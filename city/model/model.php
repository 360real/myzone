<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>City Manager</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Country</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_COUNTRY,"country_id","id","country",$rec["country_id"],"id",$site['CMSURL'],"countryDiv","countryLabelppDiv")?></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>City</strong></td>
  <td><input name="city" type="text" title="City" lang='MUST' value="<?php echo($rec['city'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Slogan</strong></td>
  <td><input name="slogan" type="text" title="Slogan" lang='MUST' value="<?php echo $rec['slogan']?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow2)?>">
    <td align="right" ><strong>Select City Banner</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['city_banner']=='') {?>  title="Select Image" <?php } ?> name="city_banner" id="city_banner" /></td>
          <td width="62%"><?php if($rec['city_banner']):?>
            <br />
            <img src="<?php echo  ($staticContentUrl."city/".$rec['id']."/".$rec['city_banner'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_city_banner" value="<?php echo ($rec['city_banner'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>


<tr class="<?php echo($clsRow1)?>">
<td align="right"><strong>Description</strong></td>
<td><textarea class="ckeditor" name="description"  id="description" rows="8" cols="70" title="Residential Content" ><?php echo $rec['description']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Meta Title</strong></td>
<td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Meta Keywords</strong></td>
<td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Meta Description</strong></td>
<td><textarea  name="meta_description" id="meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
</tr>


<!-- Start: Residential Projects Meta Detail -->

<tr class="<?php echo($clsRow1)?>">
<td align="right"><strong>Residential Description</strong></td>
<td><textarea class="ckeditor" name="residential_description"  id="residential_description" rows="8" cols="70" title="Residential Content" ><?php echo $rec['residential_description']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Residential Meta Title</strong></td>
<td><textarea  name="residential_meta_title" id="residential_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['residential_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Residential Meta Keywords</strong></td>
<td><textarea  name="residential_meta_keywords" id="residential_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['residential_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Residential Meta Description</strong></td>
<td><textarea  name="residential_meta_description" id="residential_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['residential_meta_description'])?></textarea></td>
</tr>

<!-- End:Residential Projects Meta Detail -->


<!-- Start: Commercial Projects Meta Detail -->

<tr class="<?php echo($clsRow1)?>">
<td align="right"><strong>Commercial Description</strong></td>
<td><textarea class="ckeditor" name="commercial_description"  id="commercial_description" rows="8" cols="70" title="Commercial Content" ><?php echo $rec['commercial_description']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Meta Title</strong></td>
<td><textarea  name="commercial_meta_title" id="commercial_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['commercial_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Meta Keywords</strong></td>
<td><textarea  name="commercial_meta_keywords" id="commercial_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['commercial_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Meta Description</strong></td>
<td><textarea  name="commercial_meta_description" id="commercial_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['commercial_meta_description'])?></textarea></td>
</tr>

<!-- End: Commercial Projects Meta Detail -->

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Top Script</strong></td>
<td><textarea  name="top_div" id="top_div" rows="6" cols="50" ><?php echo stripslashes($rec['top_div'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Middle Script</strong></td>
<td><textarea  name="middle_div" id="middle_div" rows="6" cols="50" ><?php echo stripslashes($rec['middle_div'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Right Script</strong></td>
<td><textarea  name="right_div" id="right_div" rows="6" cols="50" ><?php echo stripslashes($rec['right_div'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Right 2 Script</strong></td>
<td><textarea  name="right_div2" id="right_div2" rows="6" cols="50" ><?php echo stripslashes($rec['right_div2'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Bottom Script</strong></td>
<td><textarea  name="bottom_div" id="bottom_div" rows="6" cols="50" ><?php echo stripslashes($rec['bottom_div'])?></textarea></td>
</tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>City Ordering</strong></td>
  <td><input name="ordering" autocomplete="off" id="ordering" type="text"  value="<?php if($_GET["action"]=="Update" && $rec['ordering']>0) echo $rec['ordering']; else echo "1000"; ?>" size="80%" maxlength="120" /></td>
</tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Drop Down Ordering</strong></td>
  <td><input name="dropdown_ordering" autocomplete="off" id="dropdown_ordering" type="text"  value="<?php if($_GET["action"]=="Update" && $rec['dropdown_ordering']>0) echo $rec['dropdown_ordering']; else echo "1000"; ?>" size="80%" maxlength="120" /></td>
</tr>


<!-- *************************** new code added *****************************-->

<?php 
if ($action=="Update") {
  //$sql="SELECT splproj.*, prop.property_name from ".TABLE_SPECIALS_PROJECTS." splproj  inner join ".TABLE_PROPERTIES." prop on splproj.proj_id=prop.id where splproj.special_id=".$_GET['id']." " ;

  $sql="SELECT devord.*, devlpr.name from ".TABLE_DEVELOPERS_ORDERING." devord inner join ".TABLE_DEVELOPERS." devlpr on devlpr.id=devord.devlpr_id where devord.cty_id=".$_GET['id']."  ";
  
  $reca=($obj_mysql->getAllData($sql));
   $countOfrec=count($reca);

 

 if($reca!="")
 {
   $upCount=1;
   ?>

<?php 
foreach($reca as $pecialproj)
{

if($upCount==1)
{
  ?>


<tr class="<?php echo($clsRow1)?>" id="replacetr">
  <td align="right"><strong><font color="#FF0000">*</font>Developer <?php echo $upCount; ?></strong></td>
  <td><input onkeydown="test(this.id);" id="text_<?php echo $upCount; ?>" data=""  type="text" title="project1" value="<?php echo($pecialproj['name'])?>" size="50%" maxlength="120" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Ordering<input type="text" name="developer_ordering[]" value="<?php echo($pecialproj['ordering'])?>"> 
<button  class="add" onclick="addMoreRows(this); return false;"></button>
  <div id="hiddenProjId"><input  type="hidden" name="project_id[]" value="<?php echo($pecialproj['devlpr_id'])?>"></div>

  </tr>

<?php } else {?>

<tr class="<?php echo($clsRow1)?>" id="replacetr">
  <td align="right"><strong><font color="#FF0000">*</font>Developer <?php echo $upCount; ?></strong></td>
  <td><input onkeydown="test(this.id);" id="text_<?php echo $upCount; ?>" data=""  type="text" title="project1"  value="<?php echo($pecialproj['name'])?>" size="50%" maxlength="120" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Ordering<input type="text" name="developer_ordering[]" value="<?php echo($pecialproj['ordering'])?>"> <button class="delete" onclick="deleteRow(<?php echo  $pecialproj['id'] ?>, this); return false;"></button>
<div id="hiddenProjId"><input  type="hidden" name="project_id[]" value="<?php echo($pecialproj['devlpr_id'])?>"></div>
  </tr>
  

<?php } ?>

 
  
<?php $upCount++;  }  ?>



<?php } else {   ?>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Developer 1</strong></td>
  <td><input onkeydown="test(this.id);" id="text_1" data=""  type="text" title="project1" value="<?php echo($rec['project1'])?>" size="50%" maxlength="120" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Ordering<input type="text" name="developer_ordering[]" value="">
  <button class="add" onclick="addMoreRows(this); return false;"></button></td>

<?php }

}


else {
?>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Developer 1</strong></td>
  <td><input onkeydown="test(this.id);" id="text_1" data=""  type="text" title="project1" value="<?php echo($rec['project1'])?>" size="50%" maxlength="120" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  Ordering<input type="text" name="developer_ordering[]" value="">
  <button class="add" onclick="addMoreRows(this); return false;"></button></td> 


<?php } 

?>

<tr class="clsRow1uuu" id="newRows"></tr>




<!-- **************************************************new code added ********************************* -->









<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Show Type</strong></td>
  <td>
  <input type="radio" name="is_show_type" value="1" id="1" checked="checked" /> 
  <label for="1">Both</label> 
  <input type="radio" name="is_show_type" value="2" id="2" <?php if ($rec['is_show_type']=="2"){ echo("checked='checked'");} ?>/> 
  <label for="2">CRM</label>
  <input type="radio" name="is_show_type" value="3" id="3" <?php if ($rec['is_show_type']=="3"){ echo("checked='checked'");} ?>/> 
  <label for="3">CMS</label>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>

 <tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Used In Commercial </strong> </td>
	<td valign="bottom">
	<input type="radio" name="in_commercial" value="1" id="1" <?php if ($rec['in_commercial'] >0){ echo("checked='checked'");} ?> /> 
	<label for="1">Yes</label> 
	<input type="radio" name="in_commercial" value="0" id="0" <?php if ($rec['in_commercial'] <=0){ echo("checked='checked'");} ?>/> 
	<label for="0">No</label>
	</td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Top Script</strong></td>
<td><textarea  name="commercial_top_div" id="commercial_top_div" rows="6" cols="50" ><?php echo stripslashes($rec['commercial_top_div'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Middle Script</strong></td>
<td><textarea  name="commercial_middle_div" id="commercial_middle_div" rows="6" cols="50" ><?php echo stripslashes($rec['commercial_middle_div'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Right Script</strong></td>
<td><textarea  name="commercial_right_div" id="commercial_right_div" rows="6" cols="50" ><?php echo stripslashes($rec['commercial_right_div'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Right 2 Script</strong></td>
<td><textarea  name="commercial_right_div2" id="commercial_right_div2" rows="6" cols="50" ><?php echo stripslashes($rec['commercial_right_div2'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Commercial Bottom Script</strong></td>
<td><textarea  name="commercial_bottom_div" id="commercial_bottom_div" rows="6" cols="50" ><?php echo stripslashes($rec['commercial_bottom_div'])?></textarea></td>
</tr>


<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>

<style>
.ui-autocomplete{height:200px; overflow-x:scroll; }
</style>
</tbody>
</table>
</form>




<?php 
if($action=="Update" && $reca!="")
{

  $count=$countOfrec+1;
}
else
{
  $count=2;
}
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>




<script type="text/javascript">



var rowCount = <?php echo $count; ?>;
function addMoreRows(row) {

  var recRow = '<tr id="newRow_'+rowCount+'" class="clsRow1"><td align="right"><strong><font color="#FF0000">*</font>Developer '+rowCount+'</strong></td><td><input  onkeydown="test(this.id);" data="" id="text_'+rowCount+'" type="text" name="" size=50% maxlength="120">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ordering<input type="text" name="developer_ordering[]" value=""><button onclick="removeRow(this); return false;">Remove</button></td></tr>';

jQuery('#newRows').before(recRow);
rowCount ++;
}

function removeRow(removeNum) {

  console.log(removeNum);
  var test=$(removeNum).parent().parent();
  console.log(test);
$(test).remove();
}
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

 <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



<script>


 function test(id)
  {

var base_url="<?php echo C_ROOT_URL ?>";

  cityId="";
//console.log(list);
    
  $(function() {
    //var projectlist=list;

      $("#"+id).autocomplete({
      minLength: 0,
      source:base_url+"/city/view/delRecord.php?cityId="+cityId,
      focus: function( event, ui ) {
        $( "#"+id ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        $( "#"+id ).val( ui.item.value );

        $( "#"+id ).val( ui.item.label );
        $("#"+id).parent().find("#hiddenProjId").remove();

        $("#"+id).parent().find("#newhidden").remove();
          //$("#"+id).find("#hiddenProjId").remove();
        // /console.log($("#"+id ).closest('td:input').next().attr('data',ui.item.value));
        $( "#"+id ).append('<input id="newhidden" type="hidden" name="project_id[]" value="'+ui.item.value+'">');
        //$( "#"+id ).attr('name',ui.item.value);
        //$( ".project-icon" ).attr( "src", "images/" + ui.item.icon );
 
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label +"</a>" )
        .appendTo( ul );
    };
  });

}
  </script>


<script>
function deleteRow(ID, thisref)
{
  
if(confirm("Are you sure you want to delete this record"))
{
var base_url="<?php echo C_ROOT_URL ?>";

$(thisref).parent().parent().remove();


  $.ajax({
        //url: "view/changepoststatus.php",
        url:base_url+"/city/view/delRecord.php",
        type: 'POST',
        data: {rec_id :ID},
        success: function(data) {
             
          // location.href=base_url+"/specials/index.php";
          //jQuery("#rec_"+recid).replaceWith(data);
            
        }
    });
}


}
</script>