<?php
//ini_set('display_errors',1);
//include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_CITIES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/city/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."city/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."city/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT cty.id, cty.country_id, cty.city, cty.slogan, cty.description ,cty.top_div,cty.middle_div,cty.right_div,cty.right_div2,cty.bottom_div,cty.commercial_top_div,cty.commercial_middle_div,cty.commercial_right_div,cty.commercial_right_div2,cty.commercial_bottom_div, cty.meta_title, cty.meta_keywords, cty.meta_description, cty.residential_description, cty.residential_meta_title, cty.residential_meta_keywords, cty.residential_meta_description, cty.commercial_description, cty.commercial_meta_title, cty.commercial_meta_keywords, cty.commercial_meta_description, cty.status, cty.modified_date, cty.created_date, cty.ordering, cty.dropdown_ordering, cty.ipaddress, cty.is_show_type,cty.city_banner FROM ".TABLE_CITIES." cty WHERE ";
		if($_GET["city_id"]!="")
			$sql.="cty.id=".$_GET["city_id"];
		if($_GET["city"]!="")
			$sql.=" cty.city like '%".trim($_GET["city"])."%'";
		if($_GET["status"]!="")
			$sql.="cty.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(cty.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(cty.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(cty.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(cty.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT cty.id, cty.country_id, cty.city, cty.slogan, cty.description, cty.top_div,cty.middle_div,cty.right_div,cty.right_div2,cty.bottom_div,cty.commercial_top_div,cty.commercial_middle_div,cty.commercial_right_div,cty.commercial_right_div2,cty.commercial_bottom_div, cty.meta_title, cty.meta_keywords, cty.meta_description, cty.residential_description, cty.residential_meta_title, cty.residential_meta_keywords, cty.residential_meta_description, cty.commercial_description, cty.commercial_meta_title, cty.commercial_meta_keywords, cty.commercial_meta_description, cty.status, cty.modified_date, cty.created_date, cty.ordering,cty.dropdown_ordering, cty.ipaddress, cty.is_show_type, cty.in_commercial,cty.city_banner FROM ".TABLE_CITIES." cty";
		
		$sql.= " WHERE cty.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
		$sql = "SELECT cty.id, cty.country_id, cty.city, cty.slogan, cty.description, cty.top_div,cty.middle_div,cty.right_div,cty.right_div2,cty.bottom_div,cty.commercial_top_div,cty.commercial_middle_div,cty.commercial_right_div,cty.commercial_right_div2,cty.commercial_bottom_div, cty.meta_title, cty.meta_keywords, cty.meta_description, cty.residential_description, cty.residential_meta_title, cty.residential_meta_keywords, cty.residential_meta_description, cty.commercial_description, cty.commercial_meta_title, cty.commercial_meta_keywords, cty.commercial_meta_description, cty.status, cty.modified_date, cty.created_date, cty.ordering, cty.ipaddress, cty.dropdown_ordering, cty.is_show_type,cty.city_banner FROM ".TABLE_CITIES." cty";
		
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
/*	print "<pre>";
	print_r($rec);
	print "<pre>";*/

	if(count($_POST)>0){

		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";

			if($arr['pd']=="1")
			$arr['author_modified_date'] = 'now()';

			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];
			if($obj_mysql->isDupUpdate($tblName,'city', $arr['city'] ,'id',$rec_id)){

				$fileBasePath ="/var/www/html/myzone/media_images/";
				// check and create folder if not exists - start code here
				$city = $fileBasePath .'/'.'city';
				if (!is_dir($city)) {
				    mkdir($city);
				    chmod($city, 0777);
				}

				$pathFileType = $city .'/'.$rec_id;
				if (!is_dir($pathFileType)) {
				    mkdir($pathFileType);
				    chmod($pathFileType, 0777);
				}
				// check and create folder if not exists - end code here

				// image upload and update in table - start end here
	            if ($_FILES['city_banner']['name'] != "") {
	                $tmpname = $_FILES['city_banner']['tmp_name'];
	                $fileName = $_FILES['city_banner']['name'];

	                @unlink($pathFileType. "/" . $_POST['old_file_city_banner']);
	                if(move_uploaded_file($tmpname, $pathFileType . "/" . $fileName))
	                {
	                	$arr['city_banner'] = $fileName;
	                }
				}
				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);

				/*********** new code added **********************/

				$delQuery="Delete from ".TABLE_DEVELOPERS_ORDERING." where cty_id=".$rec_id." ";
						//print_r($_POST);
						$test=array_combine($_POST['developer_ordering'],$_POST['project_id']);

						/*print_r($_POST['developer_ordering']);
						print_r($_POST['project_id']);	

						print_r($test);die;*/
						$updateTblName=TABLE_DEVELOPERS_ORDERING;
						//$flId="id";
						
						$obj_mysql->exec_query($delQuery, $link);	
						foreach($test as $key=>$val)
						{

						$projectArray['cty_id']=$rec_id;
						$projectArray['devlpr_id']=$val;
						$projectArray['ordering']=$key;
						$projectArray['ipaddress']=$regn_ip;
						$projectArray['status']=1;
						$obj_mysql->insert_data($updateTblName,$projectArray);
							}
				/************ new code added *******************************/

//print_r($_POST);die;


				$obj_common->redirect($page_name."?msg=update");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}else{
			$arr['created_date'] = "now()";

			if($arr['pd']=="1")
			$arr['author_modified_date'] = 'now()';

			$arr['ipaddress'] = $regn_ip;
			$arr['created_by'] = $_SESSION['login']['id'];
			if($obj_mysql->isDuplicate($tblName,'city', $arr['city'])){


				$test=array_combine($_POST['developer_ordering'],$_POST['project_id']);

						//print_r($test);
						//print_r($arr);exit;
					$setMaxId=$obj_mysql->insert_data($tblName,$arr);
					$tableProjOrdering=TABLE_DEVELOPERS_ORDERING;
					foreach($test as $key=>$val)
					{
						$projectArray['cty_id']=$setMaxId;
						$projectArray['devlpr_id']=$val;
						$projectArray['ordering']=$key;
						$projectArray['ipaddress']=$regn_ip;
						$projectArray['status']=1;

						$obj_mysql->insert_data($tableProjOrdering,$projectArray);

					}


			        $fileBasePath ="/var/www/html/myzone/media_images/";
					// check and create folder if not exists - start code here
					$city = $fileBasePath .'/'.'city';
					if (!is_dir($city)) {
					    mkdir($city);
					    chmod($city, 0777);
					}

					$pathFileType = $city .'/'.$setMaxId;
					if (!is_dir($pathFileType)) {
					    mkdir($pathFileType);
					    chmod($pathFileType, 0777);
					}
					// check and create folder if not exists - end code here


					// image upload and update in table - start end here
		            if ($_FILES['city_banner']['name'] != "") {
		                $tmpname = $_FILES['city_banner']['tmp_name'];
		                $fileName = $_FILES['city_banner']['name'];

		                @unlink($pathFileType. "/" . $_POST['old_file_city_banner']);
		                if(move_uploaded_file($tmpname, $pathFileType . "/" . $fileName))
		                {
		                	$city_banner['city_banner'] = $fileName;
		                	$obj_mysql->update_data($tblName, $fld_id, $city_banner, $setmaxid);
		                }
					}

					// image upload and update in table - code end here
			




				$obj_common->redirect($page_name."?msg=insert");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&state_id='.$rec[$i]['state_id'].'&country_id='.$rec[$i]['country_id'].'&id='.$rec[$i]['id'].'" target="_blank" class="title_a">'.$rec[$i]['city'].'</a></td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td class="center">'.$st.'</td>
					<td><a href="'.$page_name.'?action=Update&state_id='.$rec[$i]['state_id'].'&country_id='.$rec[$i]['country_id'].'&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>';
					if($_SESSION['login']['id']=='1'){
					$list.='<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>';
					}
					$list.='</td>
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Cities', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
