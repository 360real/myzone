<?php
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";


admin_login();
$message_arr = array("insert" => "Record(s) has been added successfully!",
    "update" => "Record(s) has been updated successfully!",
    "delete" => "Record(s) has been deleted successfully!",
    "cstatus" => "Record(s) status has been changed!",
    "unique" => "Name already in record!");

$categoryUrlArray = array(
    
    'cptid' => array(
        1 => 'residential',
        2 => 'commercial'
    )
          
);

$tblName = TABLE_WEBSITE_PPC_CAMPAIGN; //main table name
$fld_id = 'id'; //primery key
$fld_status = 'status'; // staus field
$fld_orderBy = 'id'; // default order by field
$page_name = C_ROOT_URL . '/generic_campaign/controller/controller.php';
$clsRow1 = 'clsRow1';
$clsRow2 = 'clsRow2';
$action = remRegFx($_REQUEST['action']); // action to perform tast like Update, Create , Delete etc.
$rec_id = remRegFx($_GET['id']);
$arr_id = $_POST['items'] ? $_POST['items'] : array($rec_id); // for radio button
$query_str = "page=$page";

function getCityNameFromId($cityid){
    
    $sql = mysql_query("select id, city from tsr_cities where id=" . $cityid);
    $recCity = mysql_fetch_array($sql);
    $relationName = $recCity['city'];
    $relationName = str_replace(" ","-", $relationName);
    return strtolower($relationName);
}

function getAllcityofdoveloper($doveloperId){
    
    $sqlSet = "SELECT pro.cty_id,cty.city   FROM tsr_properties as pro
    inner join tsr_cities as cty on pro.cty_id = cty.id 
    where pro.devlpr_id = '".$doveloperId."' and pro.status = 1 GROUP by cty_id ";
    return  $querySet = mysql_query($sqlSet);
   
}

$file_name = "";
switch ($action) {
    case 'Create':
    case 'Update':
    $file_name = ROOT_PATH . "generic_campaign/model/model.php";
    break;
    default:
    $file_name = ROOT_PATH . "generic_campaign/view/view.php";
    break;
}

if ($_GET["action"] != "" && $_GET["action"] != "Create") {

    if ($_GET["action"] == "search") {
        $sql = "SELECT tem.template_name, camp.id, camp.is_dsa , camp.template_id, camp.relation_id, camp.loc_id, camp.campaign_id, camp.campaign_type, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM " . TABLE_WEBSITE_PPC_CAMPAIGN . " as camp INNER JOIN tsr_templates tem ON camp.template_id=tem.id WHERE ";
        if ($_GET["template_id"] != "") {
            $sql .= "camp.template_id=" . $_GET["template_id"];
        }

        if ($_GET["campaign_id"] != "") {
            $sql .= " camp.campaign_id like '%" . trim($_GET["campaign_id"]) . "%'";
        }

        if ($_GET["status"] != "") {
            $sql .= "camp.status=" . $_GET["status"];
        }

        if ($_GET["startDate"] != "" && $_GET["endDate"]) {
            $sql .= " and date(camp.created_date)>='" . date('Y-m-d', strtotime($_GET["startDate"])) . "' and date(camp.created_date)<='" . date('Y-m-d', strtotime($_GET["endDate"])) . "'";
        } else
        if ($_GET["startDate"] != "") {
            $sql .= " and date(camp.created_date)='" . date('Y-m-d', strtotime($_GET["startDate"])) . "'";
        } else
        if ($_GET["endDate"] != "") {
            $sql .= " and date(camp.created_date)='" . date('Y-m-d', strtotime($_GET["endDate"])) . "'";
        }

    } else if ($_GET["action"] == "Update") {
        $sql = "SELECT `id`, `page_url`, `is_dsa`, `campaign_type`, `campaign_url`, `status`, `start_date`, `end_date`, `ipaddress`, `created_by`, `campaign_id`, `campaign_number`, `modified_by`, `modified_date`, `created_date` FROM `generic_campaign`";

        $sql .= "WHERE id=" . $_GET["id"];
        //$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
    

    } else if ($_GET["action"] == "Delete") {
        $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
        $obj_common->redirect($page_name . "?msg=delete");
    }
    /*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');
    if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
    $s = ($_GET['s'] ? 'ASC' : 'DESC');
    $sort = ($_GET['s'] ? 0 : 1);
    $f = $_GET['f'];

    /*if($s && $f)
    $sql.= " ORDER BY $f  $s";
    else
    $sql.= " ORDER BY $fld_orderBy";    */

    /*---------------------paging script start----------------------------------------*/

    $obj_paging->limit = 10;
    if ($_GET['page_no']) {
        $page_no = remRegFx($_GET['page_no']);
    } else {
        $page_no = 0;
    }

    $queryStr = $_SERVER['QUERY_STRING'];
    /*--------------------if page_no alreay exists please remove them ---------------------------*/
    $str_pos = strpos($queryStr, 'page_no');
    if ($str_pos > 0) {
        $queryStr = str_replace(substr($queryStr, ($str_pos - 1), strlen($queryStr)), "", $queryStr);
    }

    /*------------------------------------------------------------------------------------------*/
    $obj_paging->set_lower_upper($page_no);
    $total_num = $obj_mysql->get_num_rows($sql);

    $paging = $obj_paging->next_pre($page_no, $total_num, $page_name . "?$queryStr&", 'textArial11Bold', 'textArial11orgBold');
    $total_rec = $obj_paging->total_records($total_num);
    $sql .= " LIMIT $obj_paging->lower,$obj_paging->limit";
    /*---------------------paging script end----------------------------------------*/
    //echo $sql;
    $rec = ($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
} else {
    $sql = "SELECT tem.template_name,camp.CategoryIdentifier,camp.CategoryValue ,camp.is_dsa, camp.id, camp.template_id, camp.relation_id, camp.loc_id, camp.campaign_id, camp.campaign_type, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM " . TABLE_WEBSITE_PPC_CAMPAIGN . " as camp INNER JOIN tsr_templates tem ON camp.template_id=tem.id order by created_date desc";

    $rec = ($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
}


if (count($_POST) > 0) {

   //print_r($_POST);die("####");
    $arr = $_POST;
    $rec = $_POST;
    if ($rec_id) {
      
        $arr['modified_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['modified_by'] = $_SESSION['login']['id'];

        if($_POST['is_dsa'] == 1 && $_POST['campaign_type']=="Facebook"){ 
           $arr['campaign_url'] = $_POST['page_url']."?url={lpurl}&source=google&medium=adwords&campaignid={campaignid}&adgroupid={adgroupid}&feeditemid={feeditemid}&matchtype={matchtype}&network={network}&device={device}&keyword={keyword}&creative={creative}&placement={placement}&target={target}&campaigntype=dsa&utm_source=linkedin-property&ctid=" .$_POST['campaign_id']. "&dd=" . date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=linkedin-website&utm_campaign=" . $_POST['campaign_id']. "";
        }

        if($_POST['is_dsa'] == 0 && $_POST['campaign_type']=="Facebook"){ 
           $arr['campaign_url'] = $_POST['page_url']."/?utm_source=ppc&ctid=".$_POST['campaign_id']."&dd=". date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=ppc-website&utm_campaign=". $_POST['campaign_id']. ""; 
        }

        if($_POST['is_dsa'] == 1 && $_POST['campaign_type']=="PPC"){ 
            $arr['campaign_url'] = $_POST['page_url']."/?url={lpurl}&source=google&medium=adwords&campaignid={campaignid}&adgroupid={adgroupid}&feeditemid={feeditemid}&matchtype={matchtype}&network={network}&device={device}&keyword={keyword}&creative={creative}&placement={placement}&target={target}&campaigntype=dsa&utm_source=ppc&ctid=" .$_POST['campaign_id']. "&dd=" . date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=ppc&utm_campaign=" . $_POST['campaign_id']. "";
        }

        if($_POST['is_dsa'] == 0 && $_POST['campaign_type']=="PPC"){ 
          $arr['campaign_url'] = $_POST['page_url']."//?utm_source=ppc&ctid=" .$_POST['campaign_id'].  "&dd=" . date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=ppc&utm_campaign=". $_POST['campaign_id']. "";
        }

        $obj_mysql->update_data('generic_campaign', $fld_id, $arr, $rec_id);
        $obj_common->redirect($page_name . "?msg=update");

    } else {

        if (isset($arr['Category']))  {
            echo $arr['category'];
            $catArr = explode('-', $arr['Category']);
            $arr["CategoryIdentifier"] = $catArr[0];
            $arr["CategoryValue"] = $catArr[1];
        }   unset($arr["Category"]);      
                           
       
        $arr['created_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['created_by'] = $_SESSION['login']['id'];

        if($_POST['is_dsa'] == 1 && $_POST['campaign_type']=="Facebook"){ 
           $arr['campaign_url'] = $_POST['page_url']."?url={lpurl}&source=google&medium=adwords&campaignid={campaignid}&adgroupid={adgroupid}&feeditemid={feeditemid}&matchtype={matchtype}&network={network}&device={device}&keyword={keyword}&creative={creative}&placement={placement}&target={target}&campaigntype=dsa&utm_source=linkedin-property&ctid=" .$_POST['campaign_id']. "&dd=" . date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=linkedin-website&utm_campaign=" . $_POST['campaign_id']. "";
        }

        if($_POST['is_dsa'] == 0 && $_POST['campaign_type']=="Facebook"){ 
           $arr['campaign_url'] = $_POST['page_url']."/?utm_source=ppc&ctid=".$_POST['campaign_id']."&dd=". date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=ppc-website&utm_campaign=". $_POST['campaign_id']. ""; 
        }

        if($_POST['is_dsa'] == 1 && $_POST['campaign_type']=="PPC"){ 
            $arr['campaign_url'] = $_POST['page_url']."/?url={lpurl}&source=google&medium=adwords&campaignid={campaignid}&adgroupid={adgroupid}&feeditemid={feeditemid}&matchtype={matchtype}&network={network}&device={device}&keyword={keyword}&creative={creative}&placement={placement}&target={target}&campaigntype=dsa&utm_source=ppc&ctid=" .$_POST['campaign_id']. "&dd=" . date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=ppc&utm_campaign=" . $_POST['campaign_id']. "";
        }

        if($_POST['is_dsa'] == 0 && $_POST['campaign_type']=="PPC"){ 
          $arr['campaign_url'] = $_POST['page_url']."//?utm_source=ppc&ctid=" .$_POST['campaign_id'].  "&dd=" . date("dmY", strtotime($arr['created_date'])). "&st=true&ucpt=addshoot&utm_medium=ppc&utm_campaign=". $_POST['campaign_id']. "";
        }

        $obj_mysql->insert_data('generic_campaign', $arr);
        $obj_common->redirect($page_name . "?msg=insert");
    }
}


$genericsql = "SELECT `id`, `page_url`, `is_dsa`, `campaign_type`, `campaign_url`, `status`, `start_date`, `end_date`, `ipaddress`, `created_by`, `campaign_id`, `campaign_number`, `modified_by`, `modified_date`, `created_date` FROM `generic_campaign` order by created_date desc";

$genericrec = ($rec_id ? $obj_mysql->get_assoc_arr($genericsql) : $obj_mysql->getAllData($genericsql));


$list = "";
 
foreach ($genericrec as $key => $value) {

    if($value['status']==1){
        $staus ='Active';
    }
    if($value['status']==0){
        $staus ='Inactive';
    }

    $clsRow = ($i % 2 == 0) ? 'clsRowView1' : 'clsRowView2';
    $list .= '<tr class="' . $clsRow . '">
                    <td class="center">' . $value['campaign_type'] . '</td>
                    <td class="center">' . $value['campaign_id'] . '</td>
                    <td class="center">' . $value['campaign_url'] . '</td>
                    <td class="center">' . $staus . '</td>
                    <td class="center">' . $value['created_date'] . '</td>
                    <td class="center">' . $value['modified_date'] . '</td>
					<td><a href="' . $page_name . '?action=Update&id=' .$value['id'] .'&is_dsa=' . $value['is_dsa'] . '" target="_blank" class="edit">Edit</a>
					</td>
				</tr>';
}
if ($list == "") {
    $list = "<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
}

?>
<?php include LIBRARY_PATH . "includes/header.php";
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Types', $site['TITLE'], $site['URL']);
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php
if ($_GET['msg'] != "") {
    echo ('<div class="notice">' . $message_arr[$_GET['msg']] . '</div>');
}
if ($msg != "") {
    echo ('<div class="notice">' . $message_arr[$msg] . '</div>');
}
?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include $file_name;?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include LIBRARY_PATH . "includes/footer.php";?>
