<?php
include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
include_once(CONFIG_PATH);
admin_login();

/* Including common Header */
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('Luxury Properties', $site['TITLE'], $site['URL']);
/*Header Ends*/

include(LIBRARY_PATH."luxury/model/Model.php");
$modelOb = new  Model();
$pageName = C_ROOT_URL . "/luxury/controller/controller.php";
$action = $_GET['action'];

switch ($action) {

	/*-------------CRUD for Static Content---------------*/

	case 'ContentManager':
		$getStaticContent = $modelOb->getStaticContent();
		if(count($_POST)>0 && is_array($_POST))
		{
			$insertContent = $modelOb->insertContent();
			if($insertContent) { ?>
				<script>
				alert("Data inserted successfully");
				window.location.href = "";
				</script>
			<?php } 
			else { ?>
			  <script>
			  alert("Data insertion Failed");
			  window.location.href = "";
			  </script>
			<?php }
		}
		else {
			include(ROOT_PATH."luxury/view/StaticContent.php");
		}
		break;

	case 'UpdateContent':

	$getStaticContent = $modelOb->getStaticContent();
	if ( isset($_GET['id']) && $_GET['id']!="") {
		$id = $_GET['id'];
		$getStaticContentUpdate = $modelOb->getStaticContent($id);
	}
	else
	{
		echo "No ID to capture !";
	}
		
	if(isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0)
	{
		$UpdateContent = $modelOb->UpdateContent($id);
		if ($UpdateContent) {
			header('location: /luxury/controller/controller.php?action=ContentManager');
		}
		
	}
	
		include(ROOT_PATH."luxury/view/StaticContent.php");
		break;

	case 'DeleteContent': 
		$id = $_GET['id'];
		$deleteStaticContent = $modelOb->deleteStaticContent($id);
		if ($deleteStaticContent) 
		{ ?>
			<script>
			alert("Record Deleted !");
			window.location.href="<?php echo $pageName.'?action=ContentManager' ?>";
			</script>
		<?php  }
		break;

		/*------CRUD for Static Content Ends-----*/

		/*------CRUD for Top Banners Starts------*/

	case 'Banners' : 
	$getAllBannerData = $modelOb->getAllBannerData();
	if (count($_POST)>0 && is_array($_POST)) {

		$insertBanner = $modelOb->insertBanner(); //inserting to DATABASE
		$LuxuryMainBannerSavePath = LuxuryMainBannerSavePath; //Constant defined in server_config_admin
		$bannerTempName = $_FILES['banner']['tmp_name'];
        $bannerName = $_FILES['banner']['name'];
        $bannerSaveFolder = $LuxuryMainBannerSavePath.'/'.$insertBanner;

    	if(!is_dir($bannerSaveFolder)){ //Creating Folder a/c to id
        	mkdir($bannerSaveFolder);
        	chmod($bannerSaveFolder, 0777);
        }
        else {
        	echo "Unable to create Duplicate Folder";
        	return false;
        }
		$moveBannerToLocation = move_uploaded_file($bannerTempName, $bannerSaveFolder.'/'.$bannerName); //Saving Banners
			if ($moveBannerToLocation && $insertBanner) { ?>
				<script>
				alert("Data inserted successfully");
				window.location.href = "";
				</script>
			<?php } 
	}
	else {
		include(ROOT_PATH."/luxury/view/Banners.php");
	}
	break;

	case 'UpdateBanner':
	$getAllBannerData = $modelOb->getAllBannerData();
	if ( isset($_GET['id']) && $_GET['id']!="") {
		$id = $_GET['id'];
	}
	else
	{
		echo "No ID to capture !";
	}
	$status = $_GET['status'];
	if($status !="")
	{	
		$UpdateBannerData = $modelOb->UpdateBannerData($id,$status);
	
		if ($UpdateBannerData) {
			header('Location: /luxury/controller/controller.php?action=Banners');
		}	
	}
	
	include(ROOT_PATH."luxury/view/Banners.php");
	break;

	case 'DeleteBanner':

	$id = $_GET['id'];
		$deleteBanner = $modelOb->deleteBanner($id);
		if ($deleteBanner) 
		{ ?>
			<script>
			alert("Record Deleted !");
			window.location.href="<?php echo $pageName.'?action=Banners' ?>";
			</script>
		<?php  }
	
	break;

	/*------CRUD for Main Banners Ends-----*/

	/*------CRUD for Property Data Starts------*/	

	case 'Property':
		$LuxuryPropertyDetails = $modelOb->LuxuryPropertyDetails();
		$blogs = $modelOb->dropdownBlog();
		$events = $modelOb->dropdownEvents();
	
		if (isset($_POST) && count($_POST)>0) {
			$PropertyId = mysql_real_escape_string($_POST['luxuryProperty']);
			$CheckIfDuplicate = $modelOb->CheckIfDuplicate($PropertyId);
			$count = count($CheckIfDuplicate);
			if ($count>0) { ?>
				<script>
				alert("ERROR. Details of this Property Already Exists.");
				</script>
			<?php } else {
			$insertPropertyContent = $modelOb->insertPropertyContent();
			}
			if ($insertPropertyContent) {
			header('Location: '.$pageName);
			}
		}
		
		include(ROOT_PATH.'luxury/view/PropertyForm.php');
		break;
	
		case 'UpdatePropContent':

		$LuxuryPropertyDetails = $modelOb->LuxuryPropertyDetails();
		$blogs = $modelOb->dropdownBlog();
		$events = $modelOb->dropdownEvents();

		$id = mysql_real_escape_string($_GET['id']);
		$PropContent = $modelOb->PropContent($id);
		$PropertySelected = $PropContent[0]['propertyId'];
		$fetchLuxuryBannerDetails = $modelOb->fetchLuxuryBannerDetails($id);
		$deletedBannerPropertyId = mysql_real_escape_string($_POST['alldleleteFIleIds']);

		$getdataforUploadingFile = $modelOb->getdataforuploadingFiles($id);
        $FileData = $getdataforUploadingFile;


		// if ($deletedBannerPropertyId!=='') {		
		// 	if (!is_uploaded_file($_FILES['banner']['name'])) { ?>
				 	<script>
		// 				alert('ERROR. Banner Can not be empty');
		// 			</script>
		 	<?php // header('Location: '.$pageName); }
		// 	else
		// 	{
		// 		//$updateDeletedBanner = $modelOb->updateDeletedBanner($deletedBannerPropertyId);
		// 		//$insertUpdatedbanner = $modelOb->insertFiledetail();
		// 	}
		// }

		if (count($_POST)>0) {

			
			 $UpdatePropContent = $modelOb->UpdatePropContent($id);
			 header('Location: '.$pageName);
		}

		include(ROOT_PATH.'luxury/view/PropertyForm.php');
		break;

	/*-------CRUD for Top Properties--------*/

	case 'TopProperties':
		$LuxuryProperties = $modelOb->LuxuryPropertyDetails();
		$previousTopProperties =  $modelOb->previousTopProperties();
		if (count($_POST)>0) {

			$updateTable = $modelOb->udpateTopProp();
			if ($updateTable) {

			$rank = 1;
				foreach ($_POST['top'] as $propId) {
				$propertyId = mysql_real_escape_string($propId);
				$insertTopProp = $modelOb->insertTopProp($rank,$propertyId);
				$rank = $rank+1;
				}
				if ($insertTopProp) {
				header('Location: '.$pageName.'?action=TopProperties');
				}
		 	}
		}
		include(ROOT_PATH.'luxury/view/TopProperties.php');
		break;

	/*-----CRUD for Top Properties Ends------*/	
	default:
		$listingData = $modelOb->listingData();
		include(ROOT_PATH."luxury/view/listing.php");
		break;

}

?>
