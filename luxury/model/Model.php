<?php
require_once(ROOT_PATH . "library/mysql_function.php");	

class Model
{
	private $dbObj;

    function __construct() {

        $this->dbObj = new mysql_function();
       
    }

   public function insertContent()
   {
   		$AboutLuxury = mysql_real_escape_string($_POST['AboutLuxury']);
   		$status = mysql_real_escape_string($_POST['status']);
   		$createdBy = mysql_real_escape_string($_SESSION['login']['id']);

   		$sql = "INSERT INTO tsr_luxury_contents (AboutLuxury, status, createdBy) VALUES ('".$AboutLuxury."', '".$status."', '".$createdBy."')";

   		$query = $this->dbObj->insert_query($sql);
   		return $query;
   }

   public function getStaticContent($id='')
   {
   		$sql = "SELECT id, createdDate, AboutLuxury, status FROM tsr_luxury_contents WHERE status!='History'  ";
   		if ($id!='') {
   			$sql.="AND id='".$id."'";
   		}
   		$sql.="ORDER BY id desc";
		$query = $this->dbObj->getAllData($sql);
   		return $query;
   }

   public function UpdateContent($id)
   {
   	$sql = "UPDATE tsr_luxury_contents SET AboutLuxury='".mysql_real_escape_string($_POST['AboutLuxury'])."', status='".mysql_real_escape_string($_POST['status'])."' WHERE id='".$id."' ";
   	$query = $this->dbObj->update_query($sql);
   	return $query;
   }

   public function deleteStaticContent($id)
   {
   		$sql = "UPDATE tsr_luxury_contents SET status= 'History' WHERE id= '".$id."' ";
   		$query = $this->dbObj->update_query($sql);
   		return $query;
   }

   public function insertBanner()
   {
      $sql = "INSERT INTO tsr_luxury_TopBanners (bannerName, status, bannerHeading, createdBy) VALUES ('".mysql_real_escape_string($_FILES['banner']['name'])."', '".mysql_real_escape_string($_POST['status'])."', '".mysql_real_escape_string($_POST['bannerHeading'])."' , '".mysql_real_escape_string($_SESSION['login']['id'])."')";
      $query = $this->dbObj->insert_query($sql);
      return $query;
   }

   public function getAllBannerData($id='')
   {
      $sql = "SELECT id, createdDate, bannerName, bannerHeading , status FROM tsr_luxury_TopBanners WHERE status!='History' ";
      if ($id!='') {
         $sql.= "AND id='".$id."'";
      }
      $sql.= "ORDER BY id desc";
      $query = $this->dbObj->getAllData($sql);
      return $query;
   }

   public function UpdateBannerData($id,$status)
   { 
      if ($status=='Active') {
      $statusChange = 'Inactive';
      }
      elseif ($status=='Inactive') {
         $statusChange = 'Active';
      }
      $sql = "UPDATE tsr_luxury_TopBanners SET status = '".$statusChange."' WHERE id='".$id."' ";
      $query = $this->dbObj->update_query($sql);
      return $query;
   }

   public function deleteBanner($id)
   {
      $sql = "UPDATE tsr_luxury_TopBanners SET status ='History' WHERE id='".$id."' ";
      $query = $this->dbObj->update_query($sql);
      return $query;
   }

   public function LuxuryPropertyDetails()
   {
      $sql = "SELECT id, property_name FROM tsr_properties WHERE is_platinum='1' ";
     
      $query = $this->dbObj->getAllData($sql);
      return $query;
   }

   public function dropdownBlog() 
   {
        $sql = "SELECT id, title FROM tsr_blog";
        $query= $this->dbObj->getAllData($sql);
        return $query;
   }

   public function dropdownEvents()
   {
      $query = "SELECT id, EventTitle FROM tsr_events";
      $sql = $this->dbObj->getAllData($query);
      return $sql;
   }

   public function PropContent($id)
   {
      $query = "SELECT youtubeLink, overview, keyfeatures, Highlights, Location ,blogs, metaTitle, metaDescription , metaKeywords , events, propertyId FROM tsr_luxury_propertyContents WHERE propertyId = '".$id."'";
      $sql = $this->dbObj->getAllData($query);
      return $sql;
   }

   public function fetchLuxuryBannerDetails($propId)
   {

    $query = "SELECT id ,filename, propertyId FROM tsr_luxury_assets WHERE propertyId = '".$propId."' AND status='Live' and assetType = 'Banner'";
    $sql = $this->dbObj->getAllData($query);
    return $sql;
   }

   public function updateDeletedBanner($propertyID)
   {
      $query = "UPDATE tsr_luxury_assets SET status = 'History' WHERE propertyId = '".$propertyID."' ";
      $sql = $this->dbObj->update_query($query);
      return $sql;
   }

   public function UpdatePropContent($id)
   {  

      $blogs = '';
      if (is_array($_POST['blogs']) && count($_POST['blogs']) > 0) {
         $blogs = mysql_real_escape_string(implode(",", $_POST['blogs']));
      } else {
         $blogs = mysql_real_escape_string($_POST['blogs']);
      }

      $event = '';

      if (is_array($_POST['events']) && count($_POST['events']) > 0) {
        $event = mysql_real_escape_string(implode(",", $_POST['events']));
      } else {
        $event = mysql_real_escape_string($_POST['events']);
      }

      $query = "UPDATE tsr_luxury_propertyContents 
                SET propertyId = '".mysql_real_escape_string($_POST['luxuryProperty'])."', 
                    youtubeLink = '".mysql_real_escape_string($_POST['youtubeLink'])."',
                    overview = '".mysql_real_escape_string($_POST['overview'])."', 
                    keyfeatures = '".mysql_real_escape_string($_POST['keyfeatures'])."',
                    Highlights='".mysql_real_escape_string($_POST['Highlights'])."',
                    Location='".mysql_real_escape_string($_POST['Location'])."', 
                    metaTitle = '".mysql_real_escape_string($_POST['metaTitle'])."', 
                    metaDescription = '".mysql_real_escape_string($_POST['metaDescription'])."', 
                    metaKeywords = '".mysql_real_escape_string($_POST['metaKeywords'])."',
                    blogs = '".$blogs."', 
                    events= '".$event."'
                    WHERE propertyId = '".$id."' ";
      $sql = $this->dbObj->update_query($query);

      $updatefiles = $this->updatefileforLuxury();



      return $sql;
   }



   private function updatefileforLuxury() {

        $tobedelteIds = $_POST['alldleleteFIleIds'];

      

        $tounlinkFiles = $_POST['deleteFIlesPath'];

        $deleteFIleArr = explode("#", $tobedelteIds);

        $unlinkFIlearr = explode("#", $tounlinkFiles);
        if ($_POST['alldleleteFIleIds'] != "") {

            foreach ($deleteFIleArr as $value) {

                $qry = 'update tsr_luxury_assets set status = "history"  where id = ' . mysql_real_escape_string($value);

                $this->dbObj->exec_query($qry);
            }

            
        }
       $this->saveFile();
      $this->insertFileDetail();
    }
   public function insertPropertyContent()
   {
      $blogs = '';
      if (is_array($_POST['blogs']) && count($_POST['blogs']) > 0) {
         $blogs = mysql_real_escape_string(implode(",", $_POST['blogs']));
      } else {
         $blogs = mysql_real_escape_string($_POST['blogs']);
      }

      $event = '';

      if (is_array($_POST['events']) && count($_POST['events']) > 0) {
        $event = mysql_real_escape_string(implode(",", $_POST['events']));
      } else {
        $event = mysql_real_escape_string($_POST['events']);
      }

      $sql = "INSERT INTO tsr_luxury_propertyContents(propertyId, overview, keyfeatures, Highlights, Location, blogs, events, youtubeLink, metaTitle, metaDescription, metaKeywords, createdBy) VALUES ('".mysql_real_escape_string($_POST['luxuryProperty'])."', '".mysql_real_escape_string($_POST['overview'])."', '".mysql_real_escape_string($_POST['keyfeatures'])."','".mysql_real_escape_string($_POST['Highlights'])."', '".mysql_real_escape_string($_POST['Location'])."' ,'".$blogs."' ,'".$event."' ,'".mysql_real_escape_string($_POST['youtubeLink'])."', '".mysql_real_escape_string($_POST['metaTitle'])."', '".mysql_real_escape_string($_POST['metaDescription'])."', '".mysql_real_escape_string($_POST['metaKeywords'])."' ,'".mysql_real_escape_string($_SESSION['login']['id'])."')";
      
      $query = $this->dbObj->insert_query($sql);

      $this->saveFile();
      $this->insertFileDetail();
      return true;
    }

    public function saveFile()
    { 

        $FileArr['Banner'] = $_FILES['banner']['name'];
        $FileArr['Images'] = $_FILES['Images']['name'];

        $propertyId = mysql_real_escape_string($_POST['luxuryProperty']);
        $fileBasePath = LuxuryPropertyBannerSavePath ;
    
        $fileArray['Banner'] = $_FILES['banner'];
        $fileArray['Images'] = $_FILES['Images'];

        foreach ($fileArray as $key => $value) {

            foreach ($value as $key1 => $value1) {
                if ($value1[0] != '') {

                    foreach ($value1 as $key2 => $value2) {

                        $tmpname = $value['tmp_name'][$key2];

                        $fileName = $value['name'][$key2];

                        $pathEventId = $fileBasePath .'/'. $propertyId;

                        if (!is_dir($pathEventId)) {
                            mkdir($pathEventId);
                            chmod($pathEventId, 0777);
                        }

                        $pathFileType = $pathEventId . '/' . $key;
                        if (!is_dir($pathFileType)) {
                            mkdir($pathFileType);
                            chmod($pathFileType, 0777);
                        }
                        move_uploaded_file($tmpname, $pathFileType . "/" . $fileName);
                }
            }
        }
      }  
    }

    public function insertFileDetail()
    {
        $FileArr['Banner'] = $_FILES['banner']['name'];
        $FileArr['Images'] = $_FILES['Images']['name'];

        foreach ($FileArr as $key => $value) {

            if (count($value) > 0 && $value[0] != "") {

                foreach ($value as $fileNames) {

                    $qry = 'INSERT INTO tsr_luxury_assets (assetType, propertyId, filename , createdBy,status )'
                            . ' values( '
                            . '"' . mysql_real_escape_string($key) . '",'
                            . '"' . mysql_real_escape_string($_POST['luxuryProperty']) . '",'
                            . '"' . mysql_real_escape_string($fileNames) . '",'
                            . '"' . mysql_real_escape_string($_SESSION['login']['id']) . '",'
                            . '"' . mysql_real_escape_string('Live') . '")';

                    $query = $this->dbObj->insert_query($qry);
                }
            }
        }

    }
  
    public function getdataforuploadingFiles($propId) {

        $qry = 'select id,propertyId,assetType,filename from tsr_luxury_assets where propertyId = ' . mysql_real_escape_string($propId) . ' and status ="Live" ';

        $rec = $this->dbObj->getAllData($qry);

        foreach ($rec as $key => $value) {

            $filearr['name'] = $value['filename'];
            $filearr['id'] = $value['id'];

            $filedisplayArray[$value['assetType']][] = $filearr;
        }

        return $filedisplayArray;
    }

    public function listingData()
    {
      $sql = "SELECT prop.id, prop.property_name, city.city, loc.location, prop.status FROM tsr_luxury_propertyContents as luxprop
              INNER JOIN tsr_properties as prop ON luxprop.propertyId = prop.id
              INNER JOIN tsr_cities as city ON prop.cty_id = city.id
              INNER JOIN tsr_locations as loc ON prop.loc_id = loc.id
              WHERE prop.is_platinum = '1' ";
      $query = $this->dbObj->getAllData($sql);
      return $query;
   }


   public function udpateTopProp()
   {
    $query = "UPDATE tsr_luxury_TopProperties SET status='History' ";
    $sql = $this->dbObj->update_query($query);
    return $sql;
   }

   public function insertTopProp($rank, $propId)
   {
    $query = "INSERT INTO tsr_luxury_TopProperties(propertyId, ranking, status, createdBy) VALUES('".$propId."', '".$rank."', 'Live' ,'".mysql_real_escape_string($_SESSION['login']['id'])."') ";

    $sql = $this->dbObj->insert_query($query);
    return $sql;

   }

   public function previousTopProperties()
   {
    $query = "SELECT propertyId FROM tsr_luxury_TopProperties WHERE status='Live' ";
    $sql = $this->dbObj->getAllData($query);
    return $sql;
   }

   public function CheckIfDuplicate($propertyID)
   {
    $query = "SELECT id FROM tsr_luxury_propertyContents WHERE propertyId='".$propertyID."'";
    $sql = $this->dbObj->getAllData($query);
    return $sql;
   }

}
?>
