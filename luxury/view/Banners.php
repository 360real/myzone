<?php include(LIBRARY_PATH ."luxury/view/CommonIncludesLuxury.php"); ?>
<html>
<body>
	
<table width="100%" class="MainTable eventinfo">

	<tr>
	<td colspan="2" width="210" class="bannerpg"><a href="<?= $pageName ?>" class="add">GO BACK</a></td>
	</tr>
	<form method="post" enctype="multipart/form-data">
	<tr>

	<td class="input-lbl">Banner File*</td>
	<td><input type="file" name="banner" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" required></td>
	</tr>

	<td class="input-lbl">Banner Heading</td>
	<td><input type="text" maxlength="25" name="bannerHeading" placeholder="25 KeyStrokes Only"></td>

	<tr>
	<td class="input-lbl">Status*</td>
	<td> <input type="radio" name="status" value="Active" required="true"> Active
		 <input type="radio" name="status" value="Inactive"> Inactive
	</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" name="submit" class="addmorebtn">
			<input type="reset" name="reset" class="addmorebtn" >
		</td>
	</tr>
		

	</form>
</table>
<? // -------------------------LISTING STARTS----------------------------?>
<table width="100%" class="MainTable eventinfo">
	<tr>
		<td class="bnrhd">Created date</td>
		<td class="bnrhd">Banner</td>
		<td class="bnrhd">Banner Heading</td>
		<td class="bnrhd">Status</td>
		<td class="bnrhd">Action</td>
	</tr>
	
	<?php 
	if (count($getAllBannerData)>0) {

	foreach ($getAllBannerData as $value) { ?>
			
	<tr class="clsRow1">
		
	<td align="center"> <?php echo $value['createdDate']; ?> </td>
	<td align="center"> <?php echo $value['bannerName']; ?></td>
	<td align="center"> <?php echo $value['bannerHeading'];?> </td>
	<td align="center" <?php if ($value['status']=='Active') { ?> style="color: green;" <?php } else {?> style="color: red;" <?php } ?>> <?php echo $value['status']; ?> </td>
	<td>
	    	<?php if ($value['status']=='Active') {
				$reverseStatus = 'Inactive';
			} elseif($value['status']=='Inactive') {
				$reverseStatus = 'Active';
			}?>
			<a href="<?php echo $pageName.'?action=UpdateBanner&id='.$value['id'].'&status='.$value['status']; ?>" class="edit"><?php echo $reverseStatus ?></a>
			<a href="<?php echo $pageName.'?action=DeleteBanner&id='.$value['id']; ?>" class="delete">Delete</a>
	</td>
	</tr>

	<?php }
	}
	else
		echo "No data Found !";
	 ?>
</table>


</body>
</html>