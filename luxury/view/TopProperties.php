<?php include(LIBRARY_PATH ."luxury/view/CommonIncludesLuxury.php"); ?>

<!DOCTYPE html>
<html>

<body>
<table width="100%" class="MainTable eventinfo">

<tr>
<td colspan="2" width="210" class="bannerpg"><a href="<?= $pageName ?>" class="add">GO BACK</a></td>
</tr>
<form method="post">	
<tr>
	<td class="input-lbl">Property at Top-Left</td>
	<td>
		<select name="top[]">
			<?php foreach ($LuxuryProperties as $var) { ?>
			<option name="Top1" value="<?php echo $var['id']; ?>"

				<?php if ($var['id'] == $previousTopProperties[0]['propertyId']){ ?>
					selected
				<?php } ?> >

				<?php echo $var['property_name']; ?></option>
			<?php } ?>
		</select>
	</td>
</tr>

<tr>
	<td class="input-lbl">Property at Bottom-Left</td>
	<td>
		<select name="top[]">
			<?php foreach ($LuxuryProperties as $var) { ?>
			<option name="Top2" value="<?php echo $var['id']; ?>"

				<?php if ($var['id'] == $previousTopProperties[1]['propertyId']){ ?>
					selected
				<?php } ?> >


				<?php echo $var['property_name']; ?></option>
			<?php } ?>
		</select>
	</td>
</tr>
<tr>
	<td class="input-lbl">Property at Center-Focus</td>
	<td>
		<select name="top[]">
			<?php foreach ($LuxuryProperties as $var) { ?>
			<option name="Top3" value="<?php echo $var['id']; ?>"

				<?php if ($var['id'] == $previousTopProperties[2]['propertyId']){ ?>
					selected
				<?php } ?> >

				<?php echo $var['property_name']; ?></option>
			<?php } ?>
		</select>
	</td>
</tr>
<tr>
	<td class="input-lbl">Property at Top-Right</td>
	<td>
		<select name="top[]">
			<?php foreach ($LuxuryProperties as $var) { ?>
			<option name="Top4" value="<?php echo $var['id']; ?>"

				<?php if ($var['id'] == $previousTopProperties[3]['propertyId']){ ?>
					selected
				<?php } ?> >

				<?php echo $var['property_name']; ?></option>
			<?php } ?>
		</select>
	</td>
</tr>
<tr>
	<td class="input-lbl">Property at Bottom-Right</td>
	<td>
		<select name="top[]">
			<?php foreach ($LuxuryProperties as $var) { ?>
			<option name="Top5" value="<?php echo $var['id']; ?>"

				<?php if ($var['id'] == $previousTopProperties[4]['propertyId']){ ?>
					selected
				<?php } ?> >

				<?php echo $var['property_name']; ?></option>
			<?php } ?>
		</select>
	</td>
</tr>

<tr>
	<td></td>
	<td><input type="submit" name="submit" class="addmorebtn">
	</td>
</tr>

</form>

</table>

</body>
</html>