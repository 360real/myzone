<?php include(LIBRARY_PATH ."luxury/view/CommonIncludesLuxury.php"); ?>
<html>
<body>

<table width="100%" class="MainTable eventinfo">

	<tr>
	<td colspan="2" width="210" class="bannerpg"><a href="<?= $pageName ?>" class="add">GO BACK</a></td>
	</tr>

	<form method="post" enctype="multipart/form-data">
	<tr>
		<td class="input-lbl">Property *</td>
		<td>
			<select name="luxuryProperty" required>			
				<option value="">--- Select Luxury Property ---</option>
				<?php   
                foreach($LuxuryPropertyDetails as $value){ ?>
				<option value="<?php echo $value['id']; ?>"
                <?php if ($PropertySelected == $value['id']){?>
 				selected
                <?php } ?>
                >
         		<?php echo $value['property_name']; ?>
				</option>
				<?php } ?>
			</select>
		</td>
	</tr>

	<tr>
		<?php $bannerFileName = $fetchLuxuryBannerDetails[0]['filename'];
			  $propertyId = $fetchLuxuryBannerDetails[0]['propertyId'];
			   $assetId = $fetchLuxuryBannerDetails[0]['id'];
		?>
		<td class="input-lbl">Property Specific Banner</td>
		<td><input type="file" id="propertyBanner"  style="display: block;" name="banner[]" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
			<?php if (count($bannerFileName)==0) { ?> required  <?php } ?> 
                        
                       <?php if ($action == 'UpdatePropContent') { ?>
                        disabled="true"
                       <?php } ?>
                        
                        >
		<?php if (count($bannerFileName)>0) { ?>
		<div>
		<div class="filediv"> 
                <div style="float:left;">
                <img src="<?php echo STATIC_URL.'/Luxury/PropertyBanners/'.$propertyId.'/Banner/'.$bannerFileName; ?>" width="75px;" height="75px;" title="<?php echo $bannerFileName; ?>">
                </div>
                               
                <div class="deleteFIle deletediv" isSingle="yes"  file-id="<?php echo $assetId; ?>" file-path="<?php echo STATIC_URL.'/Luxury/PropertyBanners/'.$propertyId.'/Banner/'.$bannerFileName;?>">
                <img src="<?php echo C_ROOT_URL.'view/images/clse_btn.jpg'  ?>" title="Delete"> 
                </div>
        </div>
    	</div>
    	<?php } ?>
		</td>
	</tr>

	<tr>
		<td class="input-lbl">Luxury Property Images</td>
		<td >
                        <input type="file" style="display: block;" name="Images[]" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
                            <?php if (!is_array($FileData['Images']) && count($FileData['Images'] == 0)) {?>      
                               required 
                            <?php } ?>
                               multiple >
                    
                        <div>  
                            <?php
                            if (is_array($FileData['Images']) && count($FileData['Images'] > 0)){

                           foreach($FileData['Images'] as $value){
                            
                            ?>
                            
                           <div class="filediv"> 
                                <div style="float:left;">
                                     <img src="<?php echo STATIC_URL.'/Luxury/PropertyBanners/'.$propertyId.'/Images/'.$value['name'];?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                </div>
                               
                                <div  class="deleteFIle deletediv" file-id="<?php echo $value['id']; ?>">
                                   <img src="<?php echo C_ROOT_URL.'view/images/clse_btn.jpg'  ?>" title="Delete"> 
                                </div>
                            </div>
                            
                            <?php }
                            }?>
                        </div>
        </td>

	</tr>


	<tr>
		<td class="input-lbl">Overview</td>
			<td><textarea class="ckeditor" name="overview"  id="overview" rows="4" cols="70" ><?php echo $PropContent[0]["overview"]; ?></textarea></td>
	</tr>

	<tr>
		<td class="input-lbl">Key Features</td>
		<td><textarea class="ckeditor" name="keyfeatures"  id="keyfeatures" rows="4" cols="70" ><?php echo $PropContent[0]["keyfeatures"]; ?></textarea></td>
	</tr>

	<tr>
		<td class="input-lbl">Highlights</td>
		<td><textarea class="ckeditor" name="Highlights"  id="Highlights" rows="4" cols="70" ><?php echo $PropContent[0]["Highlights"]; ?></textarea></td>
	</tr>

	<tr>
		<td class="input-lbl">Location</td>
		<td><textarea class="ckeditor" name="Location"  id="Location" rows="4" cols="70"><?php echo $PropContent[0]["Location"]; ?></textarea></td>
	</tr>



	<tr>
		<td class="input-lbl">Youtube Link</td>
		<td><input type="text" rows="4" cols="70" name="youtubeLink" placeholder="Embed Youtube Link" value="<?php echo $PropContent[0]['youtubeLink']; ?>"></td>
	</tr>

	<tr>
		<td class="input-lbl">Meta Title</td>
		<td><textarea rows="7" cols="100" name="metaTitle"><?php echo $PropContent[0]['metaTitle']; ?></textarea> </td>
	</tr>

	<tr>
		<td class="input-lbl">Meta Description</td>
		<td><textarea rows="15" cols="100" name="metaDescription"><?php echo $PropContent[0]['metaDescription']; ?></textarea> </td>
	</tr>

	<tr>
		<td class="input-lbl">Meta Keywords</td>
		<td><textarea rows="15" cols="100" name="metaKeywords" required="true"><?php echo $PropContent[0]['metaKeywords']; ?></textarea> </td>
	</tr>
	
	<tr>
		<td class="input-lbl">Blogs</td>
		<td>
			<?php $blogsSelectedArray = explode(",", $PropContent[0]['blogs']); ?>
			<select name="blogs[]" multiple size="15" class="blogselectbox" style="width: 500px; height: 220px;" required>
				<option value="">--- Select ----</option>
				<?php foreach ($blogs as $blg) { ?>
                   	<option value="<?php echo $blg['id']; ?>"

                   		<?php if (in_array($blg['id'], $blogsSelectedArray)) {?> selected<?php } ?>
           				>
                        <?php echo $blg['title']; ?>
                    </option>
                <?php } ?>
            </select>
		</td>
	</tr>

	<tr>
		<td class="input-lbl">Events</td>
		<td>
			<?php $eventsSelectedArray = explode(",", $PropContent[0]['events']); ?>
			<select name="events[]" multiple size="15" class="blogselectbox" style="width: 500px; height: 220px;" required>
				<option value="">--- Select ---</option>
				<?php foreach ($events as $evnt) { ?>
                   	<option value="<?php echo $evnt['id']; ?>"

                   		<?php if (in_array($evnt['id'], $eventsSelectedArray)) { ?>
                   				selected
                   		<?php } ?>
                   		>
                        <?php echo $evnt['EventTitle']; ?>
                    </option>
                <?php } ?>
            </select>
		</td>
	</tr>
	<input type="hidden" name="alldleleteFIleIds" id="alldeleteFIlesIds" value="">
    <input type="hidden" name="deleteFIlesPath" id="deleteFIlesPath" value="">
	<tr>
		<td></td>
		<td><input type="submit" name="submit" class="addmorebtn">
		</td>
	</tr>

	</form>
	
</table>
<? // -------------------------LISTING STARTS---------------------------- ?>


</body>
</html>				
