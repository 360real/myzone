<?php
include $_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php";
include_once CONFIG_PATH;
admin_login();

/* Including common Header */
include LIBRARY_PATH . "includes/header.php";
$commonHead = new commonHead();
$commonHead->commonHeader('Homepage', $site['TITLE'], $site['URL']);
/*Header Ends*/

include LIBRARY_PATH . "library/insertimage.php";
$insertimage = new insertimage();
require_once ROOT_PATH . "library/mysql_function.php";
include LIBRARY_PATH . "home/model/Model.php";
$modelObject = new model();
$action = mysql_real_escape_string($_GET['action']);

switch ($action) {
    case 'editbnr':

    default:

        if ($_FILES['banner']['size'] > 0) {

            $bannerArr = $_FILES['banner'];
            /*
             * Function to insert SINGLE Image into folder a/c to params passed
             * @params : Array of $_FILES superglobal containing details of image,
             *           Table name if imagedetail is to be inserted into any table, OPTIONAL
             *           Folder name to be created for storing image
             */
            $insertedStatus = $insertimage->getUploadStatus($bannerArr, 'homePageBanner', 'home');
            if ($insertedStatus == 1) {
                header("Location: " . C_ROOT_URL . "home/index.php");
            }

        }
        $imagename = $modelObject->fetchLastInsertedImageData();

        include ROOT_PATH . "home/view/view.php";
}
