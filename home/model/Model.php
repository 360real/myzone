<?php

class Model
{
    private $dbObj;

    public function __construct()
    {

        $this->dbObj = new mysql_function();

    }

    public function fetchLastInsertedImageData()
    {
        $sql = "select id,name from homePageBanner order by id desc limit 1";
        $query = $this->dbObj->getAllData($sql);
        return $query;
    }
}
?>
