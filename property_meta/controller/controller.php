<?php 
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Property Name already in record!");
		
	$tblName	=	TABLE_PROPERTIES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/property_meta/controller/controller.php';
	$page_name_insert  =	C_ROOT_URL.'/property/controller/controller.php';
	$page_name_update = C_ROOT_URL.'/property/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."property_meta/model/model.php";
			break;	
		default:
			$file_name = ROOT_PATH."property_meta/model/model.php";
			break;
	}

	
	if(count($_POST)>0){
	$arr=$_POST;
	$rec=$_POST;

	/* -------------------------------------------------------------  */
	// PROPERTY , PRICES , IMAGES UPDATE Code Block Start Here
	/*--------------------------------------------------------------  */

		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $getUser['id'];

			/* -------------------------------------------------------------  */
				
		/* CODE FOR PROPERTY PRICES UPDATE BLOCK */
				$obj_mysql->update_data(TABLE_PROPERTIES,$fld_id,$arr,$rec_id);
				$obj_common->redirect($page_name_update."?action=search&pid=".$rec_id."");	
			}
			else{
				$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}


	}	
?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Property', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
 </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
