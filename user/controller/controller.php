<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	ADMIN_USER;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/user/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."user/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."user/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT admin.public_profile,admin.id, admin.name, admin.email,admin.login_id, admin.created_date,admin.ipaddress, admin.status, admin.modified_date FROM ".ADMIN_USER." as admin WHERE ";
		if($_GET["user_id"]!="")
			$sql.="admin.id=".$_GET["user_id"];
		if($_GET["name"]!="")
			$sql.=" admin.name like '%".trim($_GET["name"])."%'";
		if($_GET["status"]!="")
			$sql.="admin.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(admin.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(admin.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(admin.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(admin.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT admin.public_profile,admin.id, admin.name,admin.pwd,admin.role_id,admin.ipaddress, admin.email,admin.login_id, admin.created_date, admin.status, admin.modified_date FROM ".ADMIN_USER." as admin";
		
		$sql.= " WHERE admin.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
	$sql = "SELECT admin.public_profile,admin.id, admin.name, admin.email,admin.login_id,admin.ipaddress, admin.created_date, admin.status, admin.modified_date FROM ".ADMIN_USER." as admin" ;
		
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/
	//echo $sql;
	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
                        //print_r($arr);
                        //print_r($rec);
                        insert_update($arr,$rec_id,$obj_mysql);
			if($obj_mysql->isDupUpdate($tblName,'login_id', $arr['login_id'] ,'id',$rec_id)){

			$arr["pwd"] = sha1("360_?@Re@lTorS#".$_POST["pwd"]."((!!^&");

				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				$obj_common->redirect($page_name."?msg=update");
                                
			}else{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}else{
			$arr['created_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			if($obj_mysql->isDuplicate($tblName,'login_id', $arr['login_id'])){
			
			$arr["pwd"] = sha1("360_?@Re@lTorS#".$_POST["pwd"]."((!!^&");


				$id = $obj_mysql->insert_data($tblName,$arr);
                                insert_update($arr,$id,$obj_mysql);
				$obj_common->redirect($page_name."?msg=insert");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="title_a">'.$rec[$i]['login_id'].'</a></td>
					<td class="left">'.($rec[$i]['name']).'</td>
					<td class="left">'.($rec[$i]['email']).'</td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td class="center">'.$st.'</td>
					<td > <a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>
					<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>
					</td>
					
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Users', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php 
function insert_update($array,$rec_id,$obj){
    $sql = " select * from ".TABLE_USER_MAPPING." where user_id=".$rec_id;
    print_r($array);
    //$sql = " select * from ".TABLE_USER_MAPPING." where user_id=".$rec_id;
    $sql2 = " select * from ".TABLE_CITIES." where id=".$array['cty_id'];
    $assoc = $obj->get_assoc_arr($sql2);
    //echo $obj->get_num_rows($sql);
    //exit;
    if($obj->get_num_rows($sql) > 0){
    //$array = $obj->get_assoc_arr($sql);
        if($array['role_id'] == 20){
        $sql = "update ".TABLE_USER_MAPPING." set city_id=".$array['cty_id'].", cityname='".$assoc['city'].
                "', username='".$array['login_id']."',status=".$array['status']." where user_id=".$rec_id;
        }else{
            $sql = "update ".TABLE_USER_MAPPING." set city_id=".$array['cty_id'].", cityname='".$assoc['city'].
                "', username='".$array['login_id']."',status=0 where user_id=".$rec_id;
        }
        //echo $sql; exit;
        $obj->update_query($sql);
    }else{
        //echo'dddd';exit;
        //echo $array['role_id'];
        if($array['role_id'] == 20){
           // echo"ffff";exit;
            $sql = "insert into ".TABLE_USER_MAPPING."(city_id,cityname,username,status,user_id,date_created,user_type)";
            $sql .= "Values(".$array['cty_id'].",'".$assoc['city']."','".$array['login_id']."',".$array['status'].",".$rec_id.",'".date('Y-m-d H:i:s')."',".$array['role_id'].")";
      //echo $sql; exit;
            $obj->exec_query($sql);
        }
    }
    
}
include(LIBRARY_PATH."includes/footer.php");?>
