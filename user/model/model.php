<form  method="post"  name="form123" onsubmit="return validateForm(this);">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>User Detail</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>User Name</strong></td>
  <td><input name="login_id" type="text" title="User Name" lang='MUST' value="<?php echo($rec['login_id'])?>" size="50%" maxlength="120" /></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Password</strong></td>
  <td><input name="pwd" type="password" title="Password" lang='MUST' value="<?php echo($rec['pwd'])?>" size="50%" maxlength="120" /></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Name</strong></td>
  <td valign="bottom"><input name="name" type="text" title="Name"  value="<?php echo($rec['name'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Email </strong></td>
  <td valign="bottom"><input name="email" type="text" title="Email"  value="<?php echo($rec['email'])?>" size="50%" maxlength="120" /></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Public Profile </strong></td>
  <td valign="bottom"><input name="public_profile" type="text" title="Public Profile"  value="<?php echo($rec['public_profile'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Role</strong></td>
  <td><?php echo DropDownList(TABLE_ROLES,"role_id","id","role",$rec['role_id'],"id")?></td>
</tr>

<tr id="city_drop_down" class="<?php echo($clsRow1)?>">
  <td align="right"><strong>City</strong></td>
  <td><?php echo AjaxDropDownListAdvance(TABLE_CITIES,"cty_id","id","city",1,1,"city",$site['CMSURL']."property/populate_locations_advance.php","locationDiv","locationLabelDiv","country_id");?></td>
</tr>

<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>


<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>

<script type="text/javascript">
    var RoleId = "<?=$rec['role_id']?>";
    if(RoleId == 20){
    $('#city_drop_down').show();
    }else{
        $('#city_drop_down').hide();
    }
    $('#role_id').on('change',function(){
        if($(this).val()== 20){
            $('#city_drop_down').show();
        }else{
            $('#city_drop_down').hide();
        }
    })
</script>