<?php 
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Property Name already in record!");
		
	$tblName	=	TABLE_PROPERTIES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/property/controller/controller.php';
	$page_name_insert  =	C_ROOT_URL.'/price/controller/controller.php';
	$page_name_update = '';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";
    
	$sql2="SELECT `property_name` FROM `tsr_properties` WHERE id=".$rec_id;
	$prop_name = $obj_mysql->getAllData($sql2);
	//print_r($prop_name[0]['property_name']);

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."image_gallary/model/model.php";
			break;	
		default:
			$file_name = ROOT_PATH."image_gallary/model/model.php";
			break;
	}

	
	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		$timesimage='';
		$imageupdate='';
		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES UPDATE Code Block Start Here
		/*--------------------------------------------------------------  */
		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];

			$newPath = UPLOAD_PATH_PROPERTY_IMAGE.$_POST["property_name"];
			$oldPath = UPLOAD_PATH_PROPERTY_IMAGE.$_POST["existingPropertyName"];

				for($counterImages=0;$counterImages<4;$counterImages++){

				if($_FILES["propertyImage".$counterImages]["name"]!="" )
				{
				
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/imagegallary/".$_POST['old_file_name_propertyImage'.$counterImages]);	
				$arrayPropertyImage["img_type"] = "IMAGE_GALLARY"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
				$arrayPropertyImage["alt_title"]=$_POST['propertyImageTitle'.$counterImages];
								

				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
                    $imageupdate[]='MINI_PROPERTY_IMAGE';
				
					$handle = new upload($_FILES["propertyImage".$counterImages]);
					if ($handle->uploaded)
					{
						/*$handle->image_resize         = true;
						$handle->image_x              = 1300;
						$handle->image_y              = 1637;
						$handle->image_ratio_y        = true;*/
						$handle->file_safe_name = true;
			
					$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/imagegallary/");
					if ($handle->processed) {
					//echo 'image resized';
					$arrayPropertyImage["image"] = $handle->file_dst_name;
				
					
					$handle->clean();
					} else {
					echo 'error : ' . $handle->error;
					}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_propertyImage'.$counterImages];
				$timesimage ='';
				}
				
			
				}
				else if(trim($_POST['propertyImageTitle'.$counterImages])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
					$arrayPropertyImage["alt_title"]=$_POST['propertyImageTitle'.$counterImages];

				}
								
				
				if($arrayPropertyImage["imageId"]=="" && $_FILES["propertyImage".$counterImages]["name"]!=""){
				$imagedata = $obj_mysql->insert_data(TABLE_IMAGES_GALLAY,$arrayPropertyImage);

				}else{

					$obj_mysql->update_data(TABLE_IMAGES_GALLAY,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
					
			    }
			}
		


				$obj_common->redirect($page_name."?action=search&pid=".$rec_id."");	
			
		}
		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES UPDATE Code Block END Here
		/*--------------------------------------------------------------  */
		

	}	
?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Property', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
 </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
