<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "webstories/commonvariable.php");
include_once(LIBRARY_PATH . "webstories/model/model.php");

$modelObj = new Model();

admin_login();
if(!in_array($_SESSION['login']['role_id'],array(1,23))){
	$obj_common->redirect('/index.php');
}

include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('webstories Managemant', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/webstories/controller/controller.php";


switch ($action) {

    case 'addwebstories':
    include(ROOT_PATH . "/webstories/view/addwebstories.php");
    break;

    case 'editwebstories':
    $listdata = $modelObj->listdata($_GET['id']);
    include(ROOT_PATH . "/webstories/view/addwebstories.php");
    break;

    case 'webstorieslist':
    $listdata = $modelObj->listdata('');
    include(ROOT_PATH . "/webstories/view/listwebstories.php");
    break;

    case 'deletewebstories':
    $deletedata = $modelObj->deletedata($_GET['id']);
    $listdata = $modelObj->listdata('');
    include(ROOT_PATH . "/webstories/view/listwebstories.php");
    break;


}

?>
