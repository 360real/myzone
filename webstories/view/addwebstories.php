<?php
$timeArray =  array('00:00:00','00:30:00','01:00:00','01:30:00','02:00:00','02:30:00','03:00:00','03:30:00','04:00:00','04:30:00','05:00:00','05:30:00','06:00:00','06:30:00','07:00:00','07:30:00','08:00:00','08:30:00','09:00:00','09:30:00','10:00:00','10:30:00','11:00:00','11:30:00','12:00:00','13:00:00','13:30:00','14:00:00','14:30:00','15:00:00','15:30:00','16:00:00','16:30:00','17:00:00','17:30:00','18:00:00','18:30:00','19:00:00','19:30:00','20:00:00','20:30:00','21:00:00','21:30:00','22:00:00','22:30:00','23:00:00','23:30:00');
?>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>view/js/jquery.dropdown.css">
<script src="<?php echo $site['URL']?>view/js/jquery.dropdown.js"></script>
  <style>

  </style>

<form id="datafm"  enctype="multipart/form-data">

<input type="hidden"  name="post_id" value="<?php echo($listdata[0]['id'])?>">
<input type="hidden" name="cover_image_old" value="<?php echo($listdata[0]['event_image'])?>">
<input type="hidden" name="add_video_old" value="<?php echo($listdata[0]['logo_image'])?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="1" align="left"><h3>Webstories</h3></th><th align="right"><a class="buttons" href="/webstories/controller/controller.php?action=webstorieslist"><h3>Webstories List</h3></a></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Title</strong></td>
  <td><input name="title" type="text" title="Title" id="titletext" lang='MUST' value="<?php echo($listdata[0]['title'])?>" size="50%" maxlength="30" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Discription</strong></td>
  <td><textarea id="ckeditor" name="web_stories_post"  id="web_stories_post" rows="4" cols="70"><?php echo $listdata[0]["web_stories_post"]; ?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Cover Image  </strong></td>
  <td><input  type="file" name="cover_image" id="cover_image"/> <!-- <p class="revlbtn" onclick="removevl('cover_image_old')">Remove</p> --></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Video Type</strong></td>
  <td><input type="radio" name="videotype" value="1" <?php if($listdata[0]['video_type']==1){ echo 'checked' ; }?>> Upload Video <input type="radio" name="videotype" value="2" <?php if($listdata[0]['video_type']==2){ echo 'checked' ; }?>> Add Video Link</td>
</tr>

<tr class="<?php echo($clsRow1)?> desc" id='uploadvideo'  <?php if($listdata[0]['video_type']==1){ ?> style='display:contents;' <?php }else{ ?> style='display: none;' <?php } ?>>
  <td align="right"><strong><font color="#FF0000">*</font>Add Video</strong></td>
  <td><input  type="file" name="add_video" id="add_video" /> </td>
</tr>

<tr class="<?php echo($clsRow1)?> desc" id='videolink' <?php if($listdata[0]['video_type']==2){ ?> style='display: contents;' <?php }else{ ?> style='display: none;' <?php } ?> >
  <td align="right"><strong><font color="#FF0000">*</font>Add Video Url</strong></td>
  <td><input  type="text" name="video_url" id="video_url" value="<?php echo($listdata[0]['add_video'])?>" /> </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Oredering</strong></td>
  <td><input name="ordering" type="number" title="ordering" lang='MUST' value="<?php echo($listdata[0]['ordering'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="clsRow1">
    <td align="right"><strong>Status</strong></td>
    <td>
      <select id="status" name="status" placeholder="Select">
         <option value="">Select Status </option>
         <option value="1"  <?php if($listdata[0]['status']==1){ echo 'selected';} ?>>Active </option>
         <option value="0" <?php if($listdata[0]['status']==0){ echo 'selected';} ?>>In Active</option>
        </select>
    </td>
  </tr>

<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" />
  </td>
</tr>
</tbody>
</table>
</form>

<script type="text/javascript">
  $('.dropdown-mul-2').dropdown({
    limitCount: 500,
    searchable: true
  });
  

  function removevl(item){
   var input = $('input[name="'+item+'"]').val('');
  }

</script>




<script type="text/javascript">

  $(document).ready(function(){  
    var data = new FormData();
    $('.button').on('click', function(e){  
      e.preventDefault();
        var cover_image = $('#cover_image').prop('files')[0];
        var add_video = $('#add_video').prop('files')[0];

        var textckeditor = $('#ckeditor').val(); 
        var textlengthckeditor = textckeditor.length
        if(textlengthckeditor > 70) {
        alert('Post cannot write more then 70 characters!');
        return false;
        }

        var text = $('#titletext').val(); 
        var textlength = text.length
        if(textlength > 30) {
        alert('Title cannot write more then 30 characters!');
        return false;
        } 

        var form_data = $('#datafm').serializeArray();
        $.each(form_data, function (key, input) {
          data.append(input.name, input.value);
        });

        var file_data = $('input[name="cover_image"]')[0].files;
        data.append("cover_image", file_data[0]);

        var add_video = $('input[name="add_video"]')[0].files;
        data.append("add_video", add_video[0]);

        data.append('key', 'value');

        $.ajax({
            url: "/webstories/controller/ajaxdata.php",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
              console.log(data);
              var json=JSON.parse(data);
                if(json.status==0){
                  alert(json.msg);
                  window.location="/webstories/controller/controller.php?action=webstorieslist";
                }else{
                  alert(json.msg);
                }
            } 
        });
    });
  });

</script>

<script type="text/javascript">
$(document).ready(function() {
  //$(".desc").hide();
  $("input[name$='videotype']").click(function() {
    var test = $(this).val();
    if(test==1){
      $('#uploadvideo').show();
      $('#videolink').hide();
    }
    if(test==2){
      $('#videolink').show();
      $('#uploadvideo').hide();
    } 
  });
});
</script>


