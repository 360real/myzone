<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "faq_news/commonvariable.php");
include_once(LIBRARY_PATH . "faq_news/model/model.php");

$modelObj = new Model();

admin_login();
$rolesession = explode(',',$_SESSION['login']['role_id']);
/*$resultdev = !empty(array_intersect($rolesession ,array(21,22) ));*/
$result = !empty(array_intersect($rolesession ,array(1,23)));
if(count($result) ==0){
$obj_common->redirect('/index.php');
}
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('FAQ news Managemant', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/faq_news/controller/controller.php";


if($_GET['search']=='Submit'){
     $listdata = $modelObj->listdata($_GET['news_id']);
    include(ROOT_PATH . "/faq_news/view/listfaq_news.php");
}

switch ($action) {
    case 'addfaq_news':
    include(ROOT_PATH . "/faq_news/view/addfaq_news.php");
    break;
    case 'editfaq_news':
    $listdata = $modelObj->listeditdata($_GET['id']);
    include(ROOT_PATH . "/faq_news/view/addfaq_news.php");
    break;
    case 'faq_newslist':
    $listdata = $modelObj->listdata($_GET['news_id']);
    include(ROOT_PATH . "/faq_news/view/listfaq_news.php");
    break;
    case 'deletefaq_news':
    $deletedata = $modelObj->deletedata($_GET['id']);
    header('Location: /faq_news/controller/controller.php?news_id='.$_GET['news_id'].'&search=Submit');
    break;
}

?>
