<?php

	require_once(ROOT_PATH . "library/mysqli_function.php");
	require_once(ROOT_PATH . "banner_manager/commonvariable.php");

	// this model for making all the query for news
	class Model {

	    private $dbObj;

	    function __construct() {

	        $this->dbObj = new mysqli_function();
	    }


	    public function adddata($arr){

			$sql = 'INSERT INTO `tsr_faq_news`( `news_id`, `question`,`answer`,`status`,`ordering`,`city_id`) VALUES ("'.$arr['news_id'].'","'.mysql_escape_string($arr['question']).'","'.mysql_escape_string($arr['answer']).'","'.$arr['status'].'","'.$arr['ordering'].'","'.$arr['city_id'].'")';

			$query = $this->dbObj->insert_query($sql);

			if($query>0){
				return $last_id = $conn->insert_id;
			}else{
				return 0;
			}

	    }

	    public function listdata($id=0){
	    	if($id>0){
               $sql = "SELECT `id`, `news_id`, `question`, `answer`, `created_date`, `status`,`ordering`,`city_id` FROM `tsr_faq_news` WHERE `news_id` = ".$id;
	    	}else{
	    		$sql = "SELECT  `id`, `news_id`, `question`, `answer`, `created_date`, `status`,`ordering`,`city_id` FROM `tsr_faq_news` order by id DESC";
	    	}
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	    public function listeditdata($id=0){
	    	
                $sql = "SELECT `id`, `news_id`, `question`, `answer`, `created_date`, `status`,`ordering`,`city_id` FROM `tsr_faq_news` WHERE `id` = ".$id;
	    	
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }


	    public function deletedata($id){

	    	$sql = "delete from `tsr_faq_news` WHERE `id` = ".$id;
	    	$query =  $this->dbObj->exec_query($sql, '');

	    }

	    public function updatedata($arr){

	        if($arr['id']>0){
		       $sql = 'UPDATE `tsr_faq_news` SET `news_id` = "'.$arr['news_id'].'", `city_id` = "'.$arr['city_id'].'",`question`= "'.mysql_escape_string($arr['question']).'",`answer`= "'.mysql_escape_string($arr['answer']).'", `status`= "'.$arr['status'].'", `ordering`= "'.$arr['ordering'].'" WHERE `id`='.$arr['id'];
                $query = $this->dbObj->update_query($sql);
		    	if($query>0){
					return 1;
				}else{
					return 0;
				}
			}

	    }


	    public function checkordering($order,$locid,$qn_id,$qn){
	    	if($qn_id!='' || $qn_id >0){
				$sql = "SELECT `id` FROM `tsr_faq_news` WHERE `ordering` = '".$order."' AND `news_id` = '".$locid."' AND id !='".$qn_id."'";
	    	}else{
	    		$sql = "SELECT `id` FROM `tsr_faq_news` WHERE `ordering` = '".$order."' AND `news_id` = '".$locid."' AND question !='".mysql_escape_string($qn)."'";
	    	}
	    	return $data = $this->dbObj->getAllData($sql);
	    }
	}

 ?>
 