


$(document).ready(function () {
  
    if ($('#myTable').length > 0) {
        $('#myTable').DataTable({
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var data_showfilter = jQuery(column.footer()).attr('data-showfilter');
                    if (data_showfilter == '1') {
                        var select = $('<select><option value="">--Select--</option></select>')
                                .appendTo($(column.footer()).empty())
                               .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            },
            //     "bLengthChange": false,
            //"pageLength": 20, // for number of records per page,
//        responsive: true, // include responsive js and css
            "order": [],
            //  "bLengthChange": false,
//        responsive: true, // include responsive js and css

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true,
            "bFilter": true,
            "lengthMenu": [[10, 25, 50], [10, 25, 50]]
                    //  bSortable: true


        });
        var table = $('#myTable').DataTable();
        $('#search-title').on('keydown', function () {

            table
                    .column(0)
                    .search(this.value)
                    .draw();
        });

    }
    
    $('input[type="file"]').change(function (e) {
       
        var allowed = $(this).attr("allowed");
        var sizeAllowed = parseInt($(this).attr("size-allowed"));
        var fileName = e.target.files[0].name;
        var extn = getExtension(fileName);
        var allowedfileArray = allowed.split("/");
        var isAllowed = checkisAllowed(allowedfileArray, fileName);
        var fileSize = e.target.files[0].size;
        var fileSize1 = fileSize/1000;
        var fileSize2 = fileSize1.toFixed(0)
        
//        alert(fileSize2);
//        
////        var AllowedSize = sizeAllowed.toFixed(2);
////        
//       alert(sizeAllowed);
      
        if (fileSize2 > sizeAllowed) {
            alert('File Size Should be less then '+sizeAllowed+' KB');
            $(this).val('');
            return false;
        }
        
        if (isAllowed == false) {
            alert(extn + ' Files are not allowed !');
            $(this).val('');
            return false;
        }
    });

});


$(document).on("click", "#reset", function () {

    window.location = "index.php?action=Create";
});

$(document).on("click", "#resetTable", function () {

    window.location = "index.php";

});





function  checkisAllowed(validateArray, fileName) {

    var extn = getExtension(fileName);
    var allowed = false;
    for (var i = 0; i < validateArray.length; i++) {
        if (validateArray[i] == extn) {
            allowed = true;
            break;
        }
    }
    return allowed;
}


function getExtension(fileName) {
    var imageextnPos = parseInt(parseInt(fileName.lastIndexOf(".") + parseInt(1)));
    var extn = fileName.substr(imageextnPos);
    return extn;
}

$(document).on("click",".deletenews",function(){

var currentStaus = $(this).attr("ChangeStatus");       
var deleteConfirm = confirm("Are You Sure to "+currentStaus+" this News ?");
var NewsId = $(this).attr("NewsId");

if(deleteConfirm){
   
    window.location.href = '/media/controller/controller.php?action=Delete&newsId='+NewsId;

}

});
/**
 * [description]
 * @param  {String} ){$('input[name [description]
 * @return {[type]}                  [description]
 */
$(document).on("click","#generatecode",function(){
    var op = [];
$('input[name="emailmarketing"]:checked').each(function() {
    op.push(this.value);
});

var base_url = window.location.origin;
if(op.length>0){
$("#mainfsk").show();
  $.ajax({
        url:base_url+"/emailmarketing/view/codegenerator.php",
        type: 'POST',
        data: {op:op},
        beforeSend: function() { 
             },
        success: function(data) { 
         $("#codeshow").html(data);
         
        }
    });
}
else{   alert("Select at least one news."); }

});

$(document).on("click",".deleteResearch",function(){

var currentStaus = $(this).attr("ChangeStatus");       
var deleteConfirm = confirm("Are You Sure to "+currentStaus+" this Research ?");
var ResearchId = $(this).attr("researchId");

if(deleteConfirm){
   
    window.location.href = '/research/controller/controller.php?action=Delete&researchId='+ResearchId;



}

});


$(document).on("click","#newsSaveBtn",function(){
    
   var pageType =  $(this).attr("pageType");
    
    if ($("#Newspdf").val() == "" &&  $("#newsLink").val() == "" && pageType == 'Create'){
        
        alert(' Please Select Pdf or Enter News Link !! ');
        return false;
    }
   
    
});