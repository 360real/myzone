


$(document).ready(function () {
  
    if ($('#myTable').length > 0) {
        $('#myTable').DataTable({
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var data_showfilter = jQuery(column.footer()).attr('data-showfilter');
                    if (data_showfilter == '1') {
                        var select = $('<select><option value="">--Select--</option></select>')
                                .appendTo($(column.footer()).empty())
                               .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            },
            //     "bLengthChange": false,
            //"pageLength": 20, // for number of records per page,
//        responsive: true, // include responsive js and css
            "order": [],
            //  "bLengthChange": false,
//        responsive: true, // include responsive js and css

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true,
            "bFilter": true,
            "lengthMenu": [[10, 25, 50], [10, 25, 50]]
                    //  bSortable: true


        });
        var table = $('#myTable').DataTable();
        $('#search-title').on('keydown', function () {

            table
                    .column(0)
                    .search(this.value)
                    .draw();
        });

    }
    
    $('input[type="file"]').change(function (e) {
       
        var allowed = $(this).attr("allowed");
        var sizeAllowed = parseInt($(this).attr("size-allowed"));
        var fileName = e.target.files[0].name;
        var extn = getExtension(fileName);
        var allowedfileArray = allowed.split("/");
        var isAllowed = checkisAllowed(allowedfileArray, fileName);
        var fileSize = e.target.files[0].size;
        var fileSize1 = fileSize/1000;
        var fileSize2 = fileSize1.toFixed(0)
        
//        alert(fileSize2);
//        
////        var AllowedSize = sizeAllowed.toFixed(2);
////        
//       alert(sizeAllowed);
      
        if (fileSize2 > sizeAllowed) {
            alert('File Size Should be less then '+sizeAllowed+' KB');
            $(this).val('');
            return false;
        }
        
        if (isAllowed == false) {
            alert(extn + ' Files are not allowed !');
            $(this).val('');
            return false;
        }
    });

});


$(document).on("click", "#reset", function () {
    window.location = "controller.php?action=Create";
});

$(document).on("click", "#resetTable", function () {

    window.location = "index.php";

});


function setendmaxdate() {
    $("#eventend").attr("min", $("#eventstart").val());
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}




function  checkisAllowed(validateArray, fileName) {

    var extn = getExtension(fileName);
    var allowed = false;
    for (var i = 0; i < validateArray.length; i++) {
        if (validateArray[i] == extn) {
            allowed = true;
            break;
        }
    }
    return allowed;
}


function getExtension(fileName) {
    var imageextnPos = parseInt(parseInt(fileName.lastIndexOf(".") + parseInt(1)));
    var extn = fileName.substr(imageextnPos);
    return extn;
}



$(document).on("change", ".eventCountry", function () {

    var countryId = $(this).val();
    var thisDiv = $(this);
    
        var pageForajax = $("#pageForJs").val();
    $.ajax({
        type: 'POST',
        url: pageForajax,
        data: {
            action: 'fetchcityData',
            countryId: countryId

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

                thisDiv.parent().siblings('.eventCitydiv').html(response);

               // ckeditor.replace('#propertyDescription');

                // CKEDITOR.replace("#propertyDescription");

            }


        }

    });

})

$(document).on("change", "#bannerCountry", function () {

    var countryId = $(this).val();
    var thisDiv = $(this)
    $.ajax({
        type: 'POST',
        url: '../model/ModelForAjax.php',
        data: {
            action: 'fetchcityData',
            countryId: countryId,
            pageFrom:'banner'

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

               $("#bannerCity").html(response);

               // ckeditor.replace('#propertyDescription');

                // CKEDITOR.replace("#propertyDescription");

            }


        }

    });

})

$(document).on("change", "#nriCountry", function () {
    
    
    var countryId = $(this).val();
    var thisDiv = $(this);
    
    var pageForajax = $("#pageForJs").val();
    
   
    
    $.ajax({
        type: 'POST',
        url: pageForajax,
        data: {
            action: 'fetchcityDataForNRI',
            countryId: countryId,
          
        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

               $("#nriCityDiv").html(response);
               
              
               

               // ckeditor.replace('#propertyDescription');

                // CKEDITOR.replace("#propertyDescription");

            }


        }

    });
    
     var eventData = getEventData(countryId,0);
               
               

});


$(document).on("change","#NriCity",function(){
    
    var cityId = $(this).val();
    
    if ($(this).val()==""){
        
        cityId = 0;
    }
    
    var countryId = $("#nriCountry").val();
    
    getEventData(countryId,cityId);
    
});



function getEventData(countryId,cityId=0){
    
  
    
    var pageForajax = $("#pageForJs").val();
    
 
    
    $.ajax({
        type: 'POST',
        url: pageForajax,
        data: {
            action: 'GetDataofEvent',
            countryId: countryId,
            cityId:cityId
          
        },
        success: function (resultProp) {

            var response = JSON.parse(resultProp);
            
        

          

              $("#eventShowForNRI").html(response.cantent);
               
              
               

               // ckeditor.replace('#propertyDescription');

                // CKEDITOR.replace("#propertyDescription");

         


        }

    });

    return false;
    
}

$(document).on("change", ".showPropertyList", function () {

    var cityId = $(this).val();
    var thisDiv = $(this);
    var totalADDproperty = $(".fetchpropertyHighlight").length;
    
    
    
     var pageForajax = $("#pageForJs").val();
     
    $.ajax({
        type: 'POST',
        url: pageForajax,
        data: {
            action: 'fetchPropertyData',
            cityId: cityId,
            fieldId:totalADDproperty

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

                thisDiv.parent().siblings('.eventPropertyDiv').html(response);

            }


        }

    });

})


$(document).on("click", "#addmoreproperty", function () {

 var totalADDproperty = $(".fetchpropertyHighlight").length;
 
 var idForNewCkeditor = parseInt(totalADDproperty)+parseInt(1);
 var pageForajax = $("#pageForJs").val();
 
    $.ajax({
        type: 'POST',
        url: pageForajax,
        data: {
            action: 'fetchMorePropertyHtml',
            ckeditorId:idForNewCkeditor

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

                $("#addmorepropertyMainDIv").append(response);

                CKEDITOR.replace('propertyDescription_'+idForNewCkeditor);
                CKEDITOR.replace('highlight_'+idForNewCkeditor);
            }


        }

    });


});

$(document).on("click", ".RemoveProperty", function () {

    $(this).parent().parent().remove();

});


$(document).on("click",".deletenri",function(){

  

var deleteConfirm = confirm("Are You Sure to Delete this Nri Page ?");
var NriId = $(this).attr("NriId");

if(deleteConfirm){
   
    window.location.href = '/nri/controller/controller.php?action=Delete&nriId='+NriId;



}

});


$(document).on("click",".deleteFIle",function(){
   
    
    var fileId = $(this).attr('file-id');
    
    var FIlePath = $(this).attr('file-path');
      
    var deleteFIlesId = $("#alldeleteFIlesIds").val();
    
    if (deleteFIlesId == ""){
        
     $("#alldeleteFIlesIds").val(fileId);   
     $("#deleteFIlesPath").val(FIlePath)
    } else {
        
     $("#alldeleteFIlesIds").val($("#alldeleteFIlesIds").val()+"#"+fileId);   
     $("#deleteFIlesPath").val( $("#deleteFIlesPath").val()+'#'+FIlePath);
    }
    
    $(this).parent().remove();
    
    var bannerdeletecount = $("#bannerExists").children().length;
  
  var imagedeletecount = $("#imagesExists").children().length;
  
  if (bannerdeletecount == 0){
      
      $("#banner").attr("required",true);
  }
    
     if (imagedeletecount == 0){
      
      $("#images").attr("required",true);
  }
    
     
});


$(document).on("click",".deletepropertyFIle",function(){
   
    
    var fileId = $(this).attr('file-id');
    
  //  var FIlePath = $(this).attr('file-path');
  
  //var deleteFIletxtbox = $(this).parent().parent().siblings().val();
      
  // console.log($(this).parent().parent().siblings(".deletepropertytxt").val());
   
    var desableflasecheck = $(this).attr("isSingle");
   
   if (desableflasecheck == "yes"){
       
       $(this).parent().parent().siblings("#Proimage").attr('disabled',false);
   }
   
  var deleteFIletxtbox = $(this).parent().parent().siblings(".deletepropertytxt").val();
   
    
    if (deleteFIletxtbox == ""){
        
     $(this).parent().parent().siblings(".deletepropertytxt").val(fileId);   

    } else {
        
     $(this).parent().parent().siblings(".deletepropertytxt").val( $(this).parent().parent().siblings(".deletepropertytxt").val()+"#"+fileId);   

    }
    
    $(this).parent().remove();
    
    
//    var bannerdeletecount = $("#bannerExists").children().length;
//  
//  var imagedeletecount = $("#imagesExists").children().length;
//  
//  if (bannerdeletecount == 0){
//      
//      $("#banner").attr("required",true);
//  }
//    
//     if (imagedeletecount == 0){
//      
//      $("#images").attr("required",true);
//  }
    
     
});

$(document).on("click",".deleteBanner",function()
{
var deleteConfirm = confirm("Are You Sure to Delete this banner ?");
var BannerId = $(this).attr("BannerId");

if(deleteConfirm){

	window.location.href = '/nri/controller/controller.php?action=deleteBanner&BannerId='+BannerId;
}
});

$(document).on("click",".setPropertyTypeHoliday",function(){
    
    if ($(this).is(":checked")){
        
        console.log($(this).next('.holidayHometxt').val());
        
        $(this).next('.holidayHometxt').val("1");
        
    } else {
        
       $(this).next('.holidayHometxt').val("0");
    }  
    
});

$(document).on("click",".setPropertyTypeRoi",function(){
    
    if ($(this).is(":checked")){
        
       $(this).next('.roiHometxt').val("1");
        
    } else {
        
     $(this).next('.roiHometxt').val("0");
    }
    
});

$(document).on("change",".blogselectbox", function(){
       
         $(this).siblings().val($(this).val())
     
});

$()



$(document).on("change", ".fetchpropertyHighlight", function () {

    var propertyId = $(this).val();
    var textareaId = $(this).attr("highlight-id");
      
    var thisDiv = $(this)
    
     var pageForajax = $("#pageForJs").val();
     
     thisDiv.parent().siblings('.propertyBannerDiv').children("#banner").attr("name",'PropertybannerName_'+propertyId+'[]');
     thisDiv.parent().siblings('.propertyImageDiv').children("#Proimage").attr("name",'PropertyImageName_'+propertyId+'[]');
    
     console.log(thisDiv.parent().siblings('.propertyBannerDiv').children("#banner").attr("name"));
    $.ajax({
        type: 'POST',
        url: pageForajax,
        data: {
            action: 'fetchPropertyHighlight',
            propertyId: propertyId

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {
                
              //  console.log(thisDiv.parent().siblings('.highlight'));
              
              console.log(response);
              
            
                
              //  $("#propertyHighlight_"+textareaId).html(response);
                
//              console.log(thisDiv.parent().siblings('.highlight').children(".highlighdiv").children().attr("id"));
              var ckeditorId = thisDiv.parent().siblings('.highlight').children(".highlighdiv").children().attr("id");


               CKEDITOR.instances[ckeditorId].setData(response);
//      heighlight         if (!CKEDITOR.instances[ckeditorId]){
//                CKEDITOR.replace(ckeditorId);
//            }

              //  thisDiv.parent().siblings('.heighlight').child(".heighlightdiv").child(".hlight").val(response);

            }


        }

    });

})
