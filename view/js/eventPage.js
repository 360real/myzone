

$(document).ready(function () {

    if ($('#myTable')) {
        $('#myTable').DataTable({
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    var data_showfilter = jQuery(column.footer()).attr('data-showfilter');
                    if (data_showfilter == '1') {
                        var select = $('<select><option value="">--Select--</option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                });
                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            },
            //     "bLengthChange": false,
            //"pageLength": 20, // for number of records per page,
//        responsive: true, // include responsive js and css
            "order": [],
            //  "bLengthChange": false,
//        responsive: true, // include responsive js and css

            rowReorder: {
                selector: 'td:nth-child(2)'
            },
            responsive: true,
            "bFilter": true,
            "lengthMenu": [[10, 25, 50], [10, 25, 50]]
                    //  bSortable: true


        });
        var table = $('#myTable').DataTable();
        $('#search-title').on('keydown', function () {

            table
                    .column(0)
                    .search(this.value)
                    .draw();
        });

    }
    
    $('input[type="file"]').change(function (e) {
        var allowed = $(this).attr("allowed");
        var fileName = e.target.files[0].name;
        var extn = getExtension(fileName);
        var allowedfileArray = allowed.split("/");
        var isAllowed = checkisAllowed(allowedfileArray, fileName);

        if (isAllowed == false) {
            alert(extn + ' Files are not allowed !');
            $(this).val('');
            return false;
        }
    });

});


$(document).on("click", "#reset", function () {
    window.location = "controller.php?action=Create";
});

$(document).on("click", "#resetTable", function () {

    window.location = "index.php";

});


function setendmaxdate() {
    $("#eventend").attr("min", $("#eventstart").val());
}


function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


$(document).on("click", "#addReqirements", function () {

    $.ajax({
        type: 'POST',
        url: '../model/ModelForAjax.php',
        data: {
            action: 'fetchRequirmentsData',

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

                $("#addmoreRequirment").append(response);
            }


        }

    });

});

function  checkisAllowed(validateArray, fileName) {

    var extn = getExtension(fileName);
    var allowed = false;
    for (var i = 0; i < validateArray.length; i++) {
        if (validateArray[i] == extn) {
            allowed = true;
            break;
        }
    }
    return allowed;
}


function getExtension(fileName) {
    var imageextnPos = parseInt(parseInt(fileName.lastIndexOf(".") + parseInt(1)));
    var extn = fileName.substr(imageextnPos);
    return extn;
}

$(document).on("click", ".RemoveRequirments", function () {
    $(this).parent().parent().remove();
});


$(document).on("change", ".eventCountry", function () {

    var countryId = $(this).val();
    var thisDiv = $(this)
    $.ajax({
        type: 'POST',
        url: '../model/ModelForAjax.php',
        data: {
            action: 'fetchcityData',
            countryId: countryId

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

                thisDiv.parent().siblings('.eventCitydiv').html(response);

                ckeditor.replace('#propertyDescription');

                // CKEDITOR.replace("#propertyDescription");

            }


        }

    });

})

$(document).on("change", "#bannerCountry", function () {

    var countryId = $(this).val();
    var thisDiv = $(this)
    $.ajax({
        type: 'POST',
        url: '../model/ModelForAjax.php',
        data: {
            action: 'fetchcityData',
            countryId: countryId,
            pageFrom:'banner'

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

               $("#bannerCity").html(response);

               // ckeditor.replace('#propertyDescription');

                // CKEDITOR.replace("#propertyDescription");

            }


        }

    });

})
$(document).on("change", ".showPropertyList", function () {

    var cityId = $(this).val();
    var thisDiv = $(this)
    $.ajax({
        type: 'POST',
        url: '../model/ModelForAjax.php',
        data: {
            action: 'fetchPropertyData',
            cityId: cityId

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

                thisDiv.parent().siblings('.eventPropertyDiv').html(response);

            }


        }

    });

})


$(document).on("click", "#addmoreproperty", function () {

 var totalADDproperty = $("#addmorepropertyMainDIv").children().length;
 
 var idForNewCkeditor = parseInt(totalADDproperty)+parseInt(1);
 
    $.ajax({
        type: 'POST',
        url: '../model/ModelForAjax.php',
        data: {
            action: 'fetchMorePropertyHtml',
            ckeditorId:idForNewCkeditor

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

                $("#addmorepropertyMainDIv").append(response);

                CKEDITOR.replace('propertyDescription_'+idForNewCkeditor);
            }


        }

    });


});

$(document).on("click", ".RemoveProperty", function () {

    $(this).parent().parent().remove();

});


$(document).on("click",".deleteEvent",function(){

  

var deleteConfirm = confirm("Are You Sure to Delete this banner ?");
var eventId = $(this).attr("eventId");

if(deleteConfirm){
   
    window.location.href = '/events/controller/controller.php?action=Delete&eventId='+eventId;



}

});


$(document).on("click",".deleteFIle",function(){
   
    
    var fileId = $(this).attr('file-id');
    
    var FIlePath = $(this).attr('file-path');
    
  
  
  
  
    
    var deleteFIlesId = $("#alldeleteFIlesIds").val();
    
    if (deleteFIlesId == ""){
        
     $("#alldeleteFIlesIds").val(fileId);   
     $("#deleteFIlesPath").val(FIlePath)
    } else {
        
     $("#alldeleteFIlesIds").val($("#alldeleteFIlesIds").val()+"#"+fileId);   
     $("#deleteFIlesPath").val( $("#deleteFIlesPath").val()+'#'+FIlePath);
    }
    
    $(this).parent().remove();
    
    var bannerdeletecount = $("#bannerExists").children().length;
  
  var imagedeletecount = $("#imagesExists").children().length;
  
  if (bannerdeletecount == 0){
      
      $("#banner").attr("required",true);
  }
    
     if (imagedeletecount == 0){
      
      $("#images").attr("required",true);
  }
    
     
});

$(document).on("click",".deleteBanner",function()
{
var deleteConfirm = confirm("Are You Sure to Delete this banner ?");
var BannerId = $(this).attr("BannerId");

if(deleteConfirm){

	window.location.href = '/events/controller/controller.php?action=deleteBanner&BannerId='+BannerId;
}
});

window.addEventListener('load', function() { 

    var countryId = $('#bannerCountry').val();

    var thisDiv = $(this)
    $.ajax({
        type: 'POST',
        url: '../model/ModelForAjax.php',
        data: {
            action: 'fetchcityData',
            countryId: countryId,
            pageFrom:'banner'

        },
        success: function (resultProp) {

            var response = resultProp;

            if (response != "") {

               $("#bannerCity").html(response);


            }


        }

    });

})