/* The following function creates an XMLHttpRequest object... */
var elementId = "";
var loadingMessage = "";
var loading_layer_in = "";
function createRequestObject() {
    var request_o; //declare the variable to hold the object.
    var browser = navigator.appName; //find the browser name
    if (browser == "Microsoft Internet Explorer") { /* Create the object using MSIE's method */
        request_o = new ActiveXObject("Microsoft.XMLHTTP");
    } else { /* Create the object using other browser's method */
        request_o = new XMLHttpRequest();
    }
    return request_o; //return the object
}

var http = createRequestObject();

function getRequest(url, id, loadingMessage,loading_layer,parameterId) {


    url = url + "?"+parameterId+"="+document.getElementById(parameterId).value;
	elementId = id;
	loading_layer_in = loading_layer;
        if (loading_layer.length > 0){
	document.getElementById(loading_layer_in).style.display="block";
    }
    http.open('get', url);
	/* Define a function to call once a response has been received. This will be our handleProductCategories function that we define below. */
    http.onreadystatechange = ManipulateRequest;
	/* Send the data. We use something other than null when we are sending using the POST method. */
    http.send(null);
}

function ManipulateRequest() {
/* Make sure that the transaction has finished. The XMLHttpRequest object
		has a property called readyState with several states:
		0: Uninitialized
		1: Loading
		2: Loaded
		3: Interactive
		4: Finished */
    if (loadingMessage == ""){ 
		var loadingMessage = "<FONT><B>Loading please wait.........</B></FONT>";
	}else{ 
		msg = loadingMessage;
	}
	/*if (document.getElementById(loading_layer_in)) {
            document.getElementById(loading_layer_in).style.display ='block';
    }*/
	//alert(http.readyState);
    if (http.readyState == 1) {
	/*if(document.getElementById(elementId))
		document.getElementById(elementId).innerHTML=loadingMessage;*/
    }
    else if (http.readyState == 4) { //Finished loading the response
	/* We have got the response from the server-side script,
			let's see just what it was. using the responseText property of
			the XMLHttpRequest object. */
		
        var response = http.responseText;
       /* And now we want to change the product_categories <div> content.
			we do this using an ability to get/change the content of a page element
			that we can find: innerHTML. */
		//alert(document.getElementById(elementId).id);
        if (document.getElementById(elementId)) {
            document.getElementById(elementId).innerHTML = response;
        }
		/*if (document.getElementById(loading_layer_in)) {
            document.getElementById(loading_layer_in).style.display ='none';
        }*/
    }
}

function getEffectOnText(subscript)
{
		var sizeSet = document.getElementById("getSize"+subscript).value;
		var priceSet = document.getElementById("getPrice"+subscript).value;
		document.getElementById("getAmount"+subscript).value = Math.abs(sizeSet*priceSet).toFixed(2);
		//document.getElementById("getBookingAmount"+subscript).value = Math.abs((document.getElementById("getAmount"+subscript).value * (10/100))).toFixed(2);
}