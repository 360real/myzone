function validateForm()
{
	
	if(document.frm_admin_login.user_login.value=="" || document.frm_admin_login.user_login.value==null || document.frm_admin_login.user_login.value=='User name')
	{
		alert("Please enter your User Name");
		document.frm_admin_login.user_login.focus();
		return false;
	}

	if(document.frm_admin_login.user_pass.value=="" || document.frm_admin_login.user_pass.value==null || document.frm_admin_login.user_pass.value=='Password')
	{
		alert("Please enter your Password");
		document.frm_admin_login.user_pass.focus();
		return false;
	}

	return true;
}