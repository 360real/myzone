<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
admin_login();
$message_arr = array("insert" => "Record(s) has been added successfully!",
    "update" => "Record(s) has been updated successfully!",
    "delete" => "Record(s) has been deleted successfully!",
    "cstatus" => "Record(s) status has been changed!",
    "unique" => "Name already in record!");

$tblName = TABLE_PPC_CAMPAIGN; //main table name
$fld_id = 'id'; //primery key
$fld_status = 'status'; // staus field
$fld_orderBy = 'id'; // default order by field
$page_name = C_ROOT_URL . '/ppc_campaign/controller/controller.php';
$clsRow1 = 'clsRow1';
$clsRow2 = 'clsRow2';
$action = remRegFx($_REQUEST['action']); // action to perform tast like Update, Create , Delete etc.
$rec_id = remRegFx($_GET['id']);
$arr_id = $_POST['items'] ? $_POST['items'] : array($rec_id); // for radio button
$query_str = "page=$page";

$file_name = "";
switch ($action) {
    case 'Create':
    case 'Update':
        $file_name = ROOT_PATH . "ppc_campaign/model/model.php";
        break;
    default:
        $file_name = ROOT_PATH . "ppc_campaign/view/view.php";
        break;
}

if ($_GET["action"] != "" && $_GET["action"] != "Create") {
    if ($_GET["action"] == "search") {
        $sql = "SELECT camp.id, micro.site_name, camp.is_dsa, camp.template_id, micro.domain_name, camp.relation_id, camp.campaign_id, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM " . TABLE_PPC_CAMPAIGN . " as camp INNER JOIN " . TABLE_MICROSITES . " micro ON camp.relation_id=micro.id WHERE ";
        if ($_GET["micro_id"] != "") {
            $sql .= "micro.id=" . $_GET["micro_id"];
        }

        if ($_GET["site_name"] != "") {
            $sql .= " micro.site_name like '%" . trim($_GET["site_name"]) . "%'";
        }

        if ($_GET["status"] != "") {
            $sql .= "micro.status=" . $_GET["status"];
        }

        if ($_GET["startDate"] != "" && $_GET["endDate"]) {
            $sql .= " and date(micro.created_date)>='" . date('Y-m-d', strtotime($_GET["startDate"])) . "' and date(micro.created_date)<='" . date('Y-m-d', strtotime($_GET["endDate"])) . "'";
        } else
        if ($_GET["startDate"] != "") {
            $sql .= " and date(micro.created_date)='" . date('Y-m-d', strtotime($_GET["startDate"])) . "'";
        } else
        if ($_GET["endDate"] != "") {
            $sql .= " and date(micro.created_date)='" . date('Y-m-d', strtotime($_GET["endDate"])) . "'";
        }

    } else if ($_GET["action"] == "Update") {
        $sql = "SELECT camp.id,micro.site_name, camp.is_dsa, camp.template_id, micro.domain_name, camp.relation_id, camp.campaign_id, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM " . TABLE_PPC_CAMPAIGN . " as camp INNER JOIN " . TABLE_MICROSITES . " micro ON camp.relation_id=micro.id";

        $sql .= " WHERE camp.id=" . $_GET["id"];
        //$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
    } else if ($_GET["action"] == "Delete") {
        $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
        $obj_common->redirect($page_name . "?msg=delete");
    }
    /*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');
    if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
    $s = ($_GET['s'] ? 'ASC' : 'DESC');
    $sort = ($_GET['s'] ? 0 : 1);
    $f = $_GET['f'];

    /*if($s && $f)
    $sql.= " ORDER BY $f  $s";
    else
    $sql.= " ORDER BY $fld_orderBy";    */

    /*---------------------paging script start----------------------------------------*/

    $obj_paging->limit = 10;
    if ($_GET['page_no']) {
        $page_no = remRegFx($_GET['page_no']);
    } else {
        $page_no = 0;
    }

    $queryStr = $_SERVER['QUERY_STRING'];
    /*--------------------if page_no alreay exists please remove them ---------------------------*/
    $str_pos = strpos($queryStr, 'page_no');
    if ($str_pos > 0) {
        $queryStr = str_replace(substr($queryStr, ($str_pos - 1), strlen($queryStr)), "", $queryStr);
    }

    /*------------------------------------------------------------------------------------------*/
    $obj_paging->set_lower_upper($page_no);
    $total_num = $obj_mysql->get_num_rows($sql);

    $paging = $obj_paging->next_pre($page_no, $total_num, $page_name . "?$queryStr&", 'textArial11Bold', 'textArial11orgBold');
    $total_rec = $obj_paging->total_records($total_num);
    $sql .= " LIMIT $obj_paging->lower,$obj_paging->limit";
    /*---------------------paging script end----------------------------------------*/
    //echo $sql;
    $rec = ($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
} else {
    $sql = "SELECT camp.id, camp.is_dsa, micro.site_name, micro.domain_name, camp.template_id, camp.relation_id, camp.campaign_id, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM " . TABLE_PPC_CAMPAIGN . " as camp INNER JOIN " . TABLE_MICROSITES . " micro ON camp.relation_id=micro.id order by camp.created_date desc";

    $rec = ($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
}
/*print "<pre>";
print_r($rec);
print "<pre>";*/

if (count($_POST) > 0) {
    $arr = $_POST;
    $rec = $_POST;
    if ($rec_id) {
        $arr["start_date"] = date('Y-m-d', strtotime($_POST["start_date"]));
        $arr["end_date"] = date('Y-m-d', strtotime($_POST["end_date"]));

        $arr['modified_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['modified_by'] = $_SESSION['login']['id'];
        if ($obj_mysql->isDupUpdate($tblName, 'campaign_id', $arr['campaign_id'], 'id', $rec_id)) {
            $obj_mysql->update_data($tblName, $fld_id, $arr, $rec_id);
            $obj_common->redirect($page_name . "?msg=update");
        } else {
            //$obj_common->redirect($page_name."?msg=unique");
            $msg = 'unique';
        }
    } else {

        $arr["start_date"] = date('Y-m-d', strtotime($_POST["start_date"]));
        $arr["end_date"] = date('Y-m-d', strtotime($_POST["end_date"]));

        $arr['created_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['created_by'] = $_SESSION['login']['id'];
        if ($obj_mysql->isDuplicate($tblName, 'campaign_id', $arr['campaign_id'])) {
            $obj_mysql->insert_data($tblName, $arr);
            $obj_common->redirect($page_name . "?msg=insert");
        } else {
            //$obj_common->redirect($page_name."?msg=unique");
            $msg = 'unique';
        }
    }
}
//print_r($rec[0]);
$list = "";
for ($i = 0; $i < count($rec); $i++) {
    if ($rec[$i]['status'] == "1") {
        $st = '<font color=green>Active</font>';
    } else {
        $st = '<font color=red>Inactive</font>';
    }

    // $domainName = "https://www.".$rec[$i]['domain_name'];
    $domainName = getDomainName($rec[$i]['domain_name'], $rec[$i]['template_id']);
    $createdDateSet = date('dmY', strtotime($rec[$i]['created_date']));

    if ($rec[$i]['is_dsa'] == 1) {
        $liveUrl = $domainName . "?url={lpurl}&source=google&medium=adwords&keyword={keyword}&matchtype={matchtype}&device={device}&campaigntype=dsa&utm_source=ppc-micro&ctid=" . $rec[$i]['campaign_id'] . "&dd=" . $createdDateSet . "&st=true&ucpt=addshoot";
    } else {
        $liveUrl = $domainName . "?utm_source=ppc-micro&ctid=" . $rec[$i]['campaign_id'] . "&dd=26112015&st=true&ucpt=addshoot";
    }

    // }
    $clsRow = ($i % 2 == 0) ? 'clsRowView1' : 'clsRowView2';
    $list .= '<tr class="' . $clsRow . '">
					<td><a href="' . $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&template_id=' . $rec[$i]['template_id'] . '&relation_id=' . $rec[$i]['relation_id'] . '&is_dsa=' . $rec[$i]['is_dsa'] . '" target="_blank" class="title_a">' . $rec[$i]['site_name'] . '</a></td>
					<td class="center">' . $rec[$i]['campaign_id'] . '</td>
					<td class="center">' . $liveUrl . '</td>
					<td class="center">' . $rec[$i]['created_date'] . '</td>
					<td class="center">' . $rec[$i]['modified_date'] . '</td>
					<td class="center">' . $st . '</td>
					<td><a href="' . $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&template_id=' . $rec[$i]['template_id'] . '&relation_id=' . $rec[$i]['relation_id'] . '&is_dsa=' . $rec[$i]['is_dsa'] . '" target="_blank" class="edit">Edit</a>

					</td>
				</tr>';
}
if ($list == "") {
    $list = "<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
}

?>
<?php include LIBRARY_PATH . "includes/header.php";
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Types', $site['TITLE'], $site['URL']);
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php
if ($_GET['msg'] != "") {
    echo ('<div class="notice">' . $message_arr[$_GET['msg']] . '</div>');
}
if ($msg != "") {
    echo ('<div class="notice">' . $message_arr[$msg] . '</div>');
}
?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include $file_name;?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include LIBRARY_PATH . "includes/footer.php";?>
<?php
function getDomainName($domain, $type)
{
    switch ($type) {
        case '2':
            return $domain;
        case '3':
            return $domain . '/nri';
        case '4':
            return $domain . '/domestic';
        default:
            return $domain;
    }
}

?>
