<?php
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_DEVELOPER_TEMPLATES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/developer_template/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."developer_template/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."developer_template/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT dt.favicon_image,dt.aboutus_banner,dt.residential_banner,dt.top_div,dt.bottom_div,dt.commercial_banner,dt.contactus_banner, dt.expire_date, dt.google_analytics_code, dt.is_crawlable, dt.sef_url,dev.name, tm.template_name, dt.id, dt.devlpr_id, dt.template_id, dt.overview, dt.display_email, dt.display_mobile1, dt.display_mobile2, dt.propty_id, dt.meta_title, dt.meta_keywords, dt.meta_description, dt.status, dt.modified_date, dt.created_date, dt.ipaddress FROM ".TABLE_DEVELOPER_TEMPLATES." dt INNER JOIN ".TABLE_DEVELOPERS." dev ON dt.devlpr_id=dev.id INNER JOIN ".TABLE_TEMPLATES." tm ON dt.template_id=tm.id WHERE ";
		if($_GET["developer_id"]!="")
			$sql.="dt.devlpr_id=".$_GET["developer_id"];
		if($_GET["developer"]!="")
			$sql.=" dev.name like '%".trim($_GET["developer"])."%'";
		if($_GET["status"]!="")
			$sql.="dt.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(dt.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(dt.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(dt.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(dt.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT dt.favicon_image,dt.aboutus_banner,dt.residential_banner,dt.top_div,dt.bottom_div,dt.commercial_banner,dt.contactus_banner,dt.residential_overview,dt.commercial_overview,dt.nri_overview,dt.aboutus_overview,residential_meta_title,commercial_meta_title,nri_meta_title,aboutus_meta_title, residential_meta_keywords,commercial_meta_keywords,nri_meta_keywords,aboutus_meta_keywords,dt.current_overview,dt.current_meta_title,dt.current_meta_description,dt.font_familyy_class,dt.current_meta_keywords,residential_meta_description,commercial_meta_description,nri_meta_description,aboutus_meta_description,dt.expire_date, dt.google_analytics_code, 
			dt.google_conversion_code, dt.google_remarking_code,  dt.google_verification_code, dt.is_crawlable, dt.sef_url,dev.name, tm.template_name, dt.id, dt.devlpr_id, dt.template_id, dt.overview, dt.display_email, dt.display_mobile1, dt.display_mobile2, dt.propty_id, dt.meta_title, dt.meta_keywords, dt.meta_description, dt.status, dt.modified_date, dt.created_date, dt.ipaddress, dt.banner_link1, dt.banner_link2, dt.banner_link3 FROM ".TABLE_DEVELOPER_TEMPLATES." dt INNER JOIN ".TABLE_DEVELOPERS." dev ON dt.devlpr_id=dev.id INNER JOIN ".TABLE_TEMPLATES." tm ON dt.template_id=tm.id";
		
		$sql.= " WHERE dt.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
	$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
		$sql = "SELECT dt.favicon_image,dt.aboutus_banner,dt.residential_banner,dt.top_div,dt.bottom_div,dt.commercial_banner,dt.contactus_banner, dt.expire_date, dt.google_analytics_code, dt.is_crawlable, dt.sef_url,dev.name, tm.template_name, dt.id, dt.devlpr_id, dt.template_id, dt.overview, dt.display_email,dt.font_familyy_class, dt.display_mobile1, dt.display_mobile2, dt.propty_id, dt.meta_title, dt.meta_keywords, dt.meta_description, dt.status, dt.modified_date, dt.created_date, dt.ipaddress FROM ".TABLE_DEVELOPER_TEMPLATES." dt INNER JOIN ".TABLE_DEVELOPERS." dev ON dt.devlpr_id=dev.id INNER JOIN ".TABLE_TEMPLATES." tm ON dt.template_id=tm.id";
		
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/	
	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			//$arr['propty_id'] = implode(",",$_POST["propty_id"]);
			$arr["expire_date"] = date('Y-m-d', strtotime($_POST["expire_date"]));


			/*======================= START: FAVICON Image ========================*/
			if($_FILES['favicon_image']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/favicon/".$_POST['old_file_name']);
				$handle = new upload($_FILES['favicon_image']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 16;
					$handle->image_y              = 16;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/favicon/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['favicon_image'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: FAVICON Image ========================*/

		 /*======================= START: About US Image ========================*/
			if($_FILES['aboutus_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/aboutus/".$_POST['old_file_name']);
				$handle = new upload($_FILES['aboutus_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/aboutus/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['aboutus_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: About US Image ========================*/

		 /*======================= START: Residential Banner Image ========================*/
			if($_FILES['residential_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/residential/".$_POST['old_file_name']);
				$handle = new upload($_FILES['residential_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/residential/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['residential_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Residential Banner Image ========================*/

		 /*======================= START: Commercial Banner Image ========================*/
			if($_FILES['commercial_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/commercial/".$_POST['old_file_name']);
				$handle = new upload($_FILES['commercial_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/commercial/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['commercial_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Commercial Banner Image ========================*/

		 /*======================= START: Contact Us Banner Image ========================*/
			if($_FILES['contactus_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/contactus/".$_POST['old_file_name']);
				$handle = new upload($_FILES['contactus_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/contactus/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['contactus_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}
			$arr['banner_link1']=$_POST['banner_proj_id'];
		 		$arr['banner_link2']=$_POST['banner_proj_id2'];
		 		$arr['banner_link3']=$_POST['banner_proj_id3'];

		 /*======================= END: Contact Us Banner Image ========================*/


				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);

				/*============== Property relation ==================*/
				/*
				$count=sizeof($_POST["propty_id"]);
				
				for($counter=0;$counter<$count;$counter++){
				if($_POST["propty_id"][$counter]!="")
				{
			
				$arrPrices["dev_template_id"] = $rec_id;

				$arrPrices["propty_id"] = $_POST["propty_id"][$counter];

				$arrPrices["devTemplateId"] = $_POST["developer_template_id"][$counter];

				if($arrPrices["devTemplateId"]=="" && $arrPrices["propty_id"]!="")
				$obj_mysql->insert_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$arrPrices);
				else
				$obj_mysql->update_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$fld_id,$arrPrices,$arrPrices["devTemplateId"]);
				}
				}*/

				$delQuery="Delete from ".TABLE_DEVELOPER_TEMPLATE_PROPERTY." where dev_template_id=".$rec_id." and flag=1";
						
						$test=array_combine($_POST['ordering'],$_POST['project_id']);
						$updateTblName=TABLE_DEVELOPER_TEMPLATE_PROPERTY;
						//$flId="id";
						
						$obj_mysql->exec_query($delQuery, $link);	
						foreach($test as $key=>$val)
					{
						$projectArray['dev_template_id']=$rec_id;
						$projectArray['propty_id']=$val;
						$projectArray['ordering']=$key;
						$projectArray['flag']=1;


						$obj_mysql->insert_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$projectArray);

					}	

				$delQuery2="Delete from ".TABLE_DEVELOPER_TEMPLATE_PROPERTY." where dev_template_id=".$rec_id." and flag=2";
						
						$test=array_combine($_POST['ordering1'],$_POST['project_id1']);
						$updateTblName=TABLE_DEVELOPER_TEMPLATE_PROPERTY;
						//$flId="id";
						
						$obj_mysql->exec_query($delQuery2, $link);	
						foreach($test as $key=>$val)
					{
						$projectArray1['dev_template_id']=$rec_id;
						$projectArray1['propty_id']=$val;
						$projectArray1['ordering']=$key;
						$projectArray1['flag']=2;

						$obj_mysql->insert_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$projectArray1);

					}	
	

				/*============== Property relation ==================*/

				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */
				for($counterImages=0;$counterImages<3;$counterImages++){
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/".$_POST['old_file_name_propertyImage'.$counterImages]);	
				$arrayPropertyImage["img_type"] = "DEVELOPER_TEMPLATE_IMAGE"; 
				$arrayPropertyImage["builder_template_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				$handle = new upload($_FILES["propertyImage".$counterImages]);
				if ($handle->uploaded)
				{
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_propertyImage'.$counterImages];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["propertyImage".$counterImages]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */


				$obj_common->redirect($page_name."?msg=update");	
			
		}else{

			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			//$arr['propty_id'] = implode(",",$_POST["propty_id"]);
			$arr["expire_date"] = date('Y-m-d', strtotime($_POST["expire_date"]));

			$sql = mysql_query("SELECT MAX(id) as maxId FROM tsr_developer_templates");
			$rec = mysql_fetch_array($sql);
			$setMaxId = $rec["maxId"];

			/*======================= START: FAVICON Image ========================*/
			if($_FILES['favicon_image']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/favicon/".$_POST['old_file_name']);
				$handle = new upload($_FILES['favicon_image']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 16;
					$handle->image_y              = 16;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/favicon/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['favicon_image'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: FAVICON Image ========================*/

		 /*======================= START: About Us Image ========================*/
			if($_FILES['aboutus_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/aboutus/".$_POST['old_file_name']);
				$handle = new upload($_FILES['aboutus_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/aboutus/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['aboutus_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: About Us Image ========================*/

		 /*======================= START: Residential Banner Image ========================*/
			if($_FILES['residential_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/residential/".$_POST['old_file_name']);
				$handle = new upload($_FILES['residential_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/residential/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['residential_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Residential Banner Image ========================*/

		 /*======================= START: Commercial Banner Image ========================*/
			if($_FILES['commercial_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/commercial/".$_POST['old_file_name']);
				$handle = new upload($_FILES['commercial_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/commercial/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['commercial_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Commercial Banner Image ========================*/

		 /*======================= START: Contact Us Banner Image ========================*/
			if($_FILES['contactus_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/contactus/".$_POST['old_file_name']);
				$handle = new upload($_FILES['contactus_banner']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$setMaxId."/contactus/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['contactus_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Contact Us Banner Image ========================*/

		 		$arr['banner_link1']=$_POST['banner_proj_id'];
		 		$arr['banner_link2']=$_POST['banner_proj_id2'];
		 		$arr['banner_link3']=$_POST['banner_proj_id3'];
				$rec_id = $obj_mysql->insert_data($tblName,$arr);

				/*============== Property relation ==================*/
				/*$count=sizeof($_POST["propty_id"]);
				
				for($counter=0;$counter<$count;$counter++){
				if($_POST["propty_id"][$counter]!="")
				{
			
				$arrPrices["dev_template_id"] = $rec_id;

				$arrPrices["propty_id"] = $_POST["propty_id"][$counter];
				
				$arrPrices["devTemplateId"] = $_POST["developer_template_id"][$counter];

				if($arrPrices["devTemplateId"]=="" && $arrPrices["propty_id"]!="")
				$obj_mysql->insert_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$arrPrices);
				else
				$obj_mysql->update_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$fld_id,$arrPrices,$arrPrices["devTemplateId"]);
				}
				}*/
				$test=array_combine($_POST['ordering'],$_POST['project_id']);
				foreach($test as $key=>$val)
					{
						$projectArray['dev_template_id']=$rec_id;
						$projectArray['propty_id']=$val;
						$projectArray['ordering']=$key;
						$projectArray['flag']="1";

						$obj_mysql->insert_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$projectArray);

					}	

					$test1=array_combine($_POST['ordering1'],$_POST['project_id1']);
				

					foreach($test1 as $key1=>$val1)
					{
						$projectArray1['dev_template_id']=$rec_id;
						$projectArray1['propty_id']=$val1;
						$projectArray1['ordering']=$key1;
						$projectArray1['flag']="2";

						$obj_mysql->insert_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$projectArray1);

					}	

					$test1=array_combine($_POST['ordering1'],$_POST['project_id1']);
				

					foreach($test1 as $key1=>$val1)
					{
						$projectArray1['dev_template_id']=$rec_id;
						$projectArray1['propty_id']=$val1;
						$projectArray1['ordering']=$key1;
						$projectArray1['flag']="2";

						$obj_mysql->insert_data(TABLE_DEVELOPER_TEMPLATE_PROPERTY,$projectArray1);

					}	
				/*============== Property relation ==================*/


				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */
				for($counterImages=0;$counterImages<3;$counterImages++){
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				@unlink(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/".$_POST['old_file_name_propertyImage'.$counterImages]);	
				$arrayPropertyImage["img_type"] = "DEVELOPER_TEMPLATE_IMAGE"; 
				$arrayPropertyImage["builder_template_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				$handle = new upload($_FILES["propertyImage".$counterImages]);
				if ($handle->uploaded)
				{
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE.$rec_id."/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_propertyImage'.$counterImages];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["propertyImage".$counterImages]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */


				
				$obj_common->redirect($page_name."?msg=insert");	


		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="title_a">'.$rec[$i]['name'].'</a></td>
					<td class="center">'.$rec[$i]['template_name'].'</td>
					<td class="center">'.$st.'</td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td >
					<a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>
						<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>
					</td> 
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Developer Templates', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
