<?php
include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	'';	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'created_date';			// default order by field
	$page_name  =	C_ROOT_URL.'/leads/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."leads/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."leads/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT lds.id, lds.fname, lds.mobile1, lds.email_primary, lds.source, lds.comments, lds.href_url, lds.referer_url, lds.created_date FROM tsr_leads lds WHERE date(lds.created_date) BETWEEN  DATE_SUB(date(NOW()), INTERVAL 7 day) and NOW() and ";
		if($_GET["city_id"]!="")
			$sql.="lds.id=".$_GET["city_id"];
		if($_GET["href_url"]!="")
			$sql.=" lds.href_url like '%".trim($_GET["href_url"])."%'";

		if($_SESSION["bpLoginUserId"]=="5")
			$sql.=" lds.href_url like '%www.carsonakoyabydamac.in%' and ";


		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" date(lds.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(lds.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" date(lds.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" date(lds.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT cty.id, cty.country_id, cty.city, cty.description, cty.meta_title, cty.meta_keywords, cty.meta_description, cty.status, cty.modified_date, cty.created_date, cty.ordering, cty.ipaddress FROM ".TABLE_CITIES." cty";
		
		$sql.= " WHERE cty.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s desc";
	else
		$sql.= " ORDER BY lds.created_date desc";	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=50;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
		if($_SESSION["bpLoginUserId"]=="5")
			$sql.=" SELECT lds.id, lds.fname, lds.mobile1, lds.email_primary, lds.source, lds.comments, lds.href_url, lds.referer_url, lds.created_date FROM tsr_leads lds WHERE date(lds.created_date) BETWEEN  DATE_SUB(date(NOW()), INTERVAL 7 day) and NOW() and lds.href_url like '%www.carsonakoyabydamac.in%' order by created_date desc";
		else
		$sql = "SELECT lds.id, lds.fname, lds.mobile1, lds.email_primary, lds.source, lds.comments, lds.href_url, lds.referer_url, lds.browser_source, lds.ipaddress, lds.created_date FROM tsr_leads lds where date(lds.created_date) BETWEEN  DATE_SUB(date(NOW()), INTERVAL 7 day) and NOW() order by created_date desc";
		
		
		/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=50;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/

	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";

			if($arr['pd']=="1")
			$arr['author_modified_date'] = 'now()';

			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];
			if($obj_mysql->isDupUpdate($tblName,'city', $arr['city'] ,'id',$rec_id)){
				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				$obj_common->redirect($page_name."?msg=update");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}else{
			$arr['created_date'] = "now()";

			if($arr['pd']=="1")
			$arr['author_modified_date'] = 'now()';

			$arr['ipaddress'] = $regn_ip;
			$arr['created_by'] = $_SESSION['login']['id'];
			if($obj_mysql->isDuplicate($tblName,'city', $arr['city'])){
				$obj_mysql->insert_data($tblName,$arr);
				$obj_common->redirect($page_name."?msg=insert");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}

if($rec[$i]['source']==4)
{
$setLeadType = "Website";
} else {

if($rec[$i]['referer_url']=='DIRECT' || $rec[$i]['referer_url']=='' || $rec[$i]['referer_url']=='0')
{
$setLeadType = "DIRECT";
} else if(($rec[$i]['referer_url']=='https://www.google.co.in/' || $rec[$i]['referer_url']=='https://www.google.com/') && strpos($rec[$i]['referer_url'],"aclk?")<1)
{
$setLeadType = "SEO";
} else if(strpos($rec[$i]['referer_url'],"aclk?")>0 || strpos($rec[$i]['referer_url'],"tpc.googlesyndication.com")>0 || strpos($rec[$i]['referer_url'],"utm_medium=banner")>0 || strpos($rec[$i]['referer_url'],"afs/ads?q=")>0 )
{
$setLeadType = "PPC";
} else {
$setLeadType = "SEO";
}

}


  

if($_SESSION['login']['role_id']=="4")
		{
	
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td class="center">'.$rec[$i]['href_url'].'</td>
					<td class="center">'.$setLeadType.'</td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['referer_url'].'</td>
					
				</tr>';
		} else {

		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td>'.$rec[$i]['fname'].'</td>
					<td class="center">'.$rec[$i]['mobile1'].'</td>
					<td class="center">'.$rec[$i]['email_primary'].'</td>
					<td class="center">'.$rec[$i]['comments'].'</td>
					<td class="center">'.$rec[$i]['href_url'].'</td>


					<td class="center">'.$setLeadType.'</td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					
				</tr>';
		}

	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Leads', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
