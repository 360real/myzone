<?php date_default_timezone_set('Asia/Kolkata');
	include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
	//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
	include_once (LIBRARY_PATH."library/server_config_admin.php");
	require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
	admin_login();
	$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
	$tblName	=	TABLE_INTERNAL_NEWS;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/induction_tracking/controller/controller.php';
	$clsRow1	=	'clsRow1';
	$clsRow2	=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform task like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	


	/* choices to where page is redirect
		if action=create then goes to v
		iew  	
			action =update then model
			action default to view i.e index.php
	*/

	$file_name = ROOT_PATH."/induction_tracking/view/view.php" ;
	
	/***********************************************************************************/
			//Code  for search a record from database on click submit  button

	/***********************************************************************************/
	
		if($_GET["action"]=="search")
		{
			  if($_GET['emailid']!="")
			  $sql = "SELECT induction_tracking2.*,welcomemail_tracking.sendondate,welcomemail_tracking.cc_mail ,welcomemail_tracking.emailread FROM induction_tracking2 LEFT JOIN welcomemail_tracking ON welcomemail_tracking.emailid=induction_tracking2.user_email WHERE user_email like '%".trim($_GET["emailid"])."%' ORDER BY welcomemail_tracking.sendondate DESC";
				else if($_GET['induction']!="")
					$sql = "SELECT induction_tracking2.*,welcomemail_tracking.sendondate,welcomemail_tracking.cc_mail,welcomemail_tracking.emailread FROM induction_tracking2 LEFT JOIN welcomemail_tracking ON welcomemail_tracking.emailid=induction_tracking2.user_email WHERE welcomemail_tracking.username like '%".trim($_GET["induction"])."%' ORDER BY welcomemail_tracking.sendondate DESC";
				else if(($_GET['startDate']!=""))
				{

					// $sql = "SELECT induction_tracking2.*,welcomemail_tracking.sendondate,welcomemail_tracking.cc_mail,welcomemail_tracking.emailread FROM induction_tracking2 LEFT JOIN welcomemail_tracking ON welcomemail_tracking.emailid=induction_tracking2.user_email WHERE  date(welcomemail_tracking.sendondate)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' ORDER BY welcomemail_tracking.sendondate DESC";
					$sql = "SELECT induction_tracking2.*,welcomemail_tracking.sendondate,welcomemail_tracking.cc_mail,welcomemail_tracking.emailread FROM welcomemail_tracking  LEFT JOIN induction_tracking2 ON welcomemail_tracking.emailid=induction_tracking2.user_email WHERE  date(welcomemail_tracking.sendondate)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' ORDER BY welcomemail_tracking.sendondate DESC";
				}
		}
		else{
			 $sql = "SELECT induction_tracking2.*,welcomemail_tracking.sendondate,welcomemail_tracking.cc_mail,welcomemail_tracking.emailread FROM induction_tracking2 LEFT JOIN welcomemail_tracking ON welcomemail_tracking.emailid=induction_tracking2.user_email ORDER BY welcomemail_tracking.sendondate DESC";
		}
		//$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	

	/*---------------------paging script start----------------------------------------*/
		$obj_paging->limit=10;
		if($_GET['page_no'])
			$page_no=remRegFx($_GET['page_no']);
			else
			$page_no=0;
		$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
		$str_pos=strpos($queryStr,'page_no');
		if($str_pos>0)
			$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
		$obj_paging->set_lower_upper($page_no);
		$total_num=$obj_mysql->get_num_rows($sql);
		$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
		$total_rec=$obj_paging->total_records($total_num);
		$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";
	/*---------------------paging script end----------------------------------------*/
	    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));

	/************************************************************************************************/
			// code for search and pagination ends here

	/************************************************************************************************/	

	

	$list="";
	 
	for($i=0;$i< count($rec);$i++)
	{
		
		if($rec[$i]['emailread']=='')
			$rec[$i]['emailread']="BY CRM";
		else if($rec[$i]['emailread']=='0000-00-00 00:00:00')
			$rec[$i]['emailread']="NOT";
		else $rec[$i]['emailread']="YES";
		

		if($rec[$i]['result']==1)
			$st="YES";
		else $st="NO";
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';

		if($rec[$i]['m1']>=8)
			$rec[$i]['m1']="Y";
		else if(($rec[$i]['m1']==0.000))
			$rec[$i]['m1']="";
		else if(($rec[$i]['m1']>0.000)&&($rec[$i]['m1']<8))
			$rec[$i]['m1']="N";
		else $rec[$i]['m1']="";

		if($rec[$i]['m2']>=2.5)
			$rec[$i]['m2']="Y";
		else if(($rec[$i]['m2']==0.000))
			$rec[$i]['m2']="";
		else if(($rec[$i]['m2']>0.000)&&($rec[$i]['m2']<2.5))
			$rec[$i]['m2']="N";
		else $rec[$i]['m2']="";

		if($rec[$i]['m3']>=9)
			$rec[$i]['m3']="Y";
		else if(($rec[$i]['m3']==0.000))
			$rec[$i]['m3']="";
		else if(($rec[$i]['m3']>0.000)&&($rec[$i]['m3']<9))
			$rec[$i]['m3']="N";
		else $rec[$i]['m3']="";

		if($rec[$i]['m4']>=3)
			$rec[$i]['m4']="Y";
		else if(($rec[$i]['m4']==0.000))
			$rec[$i]['m4']="";
		else if(($rec[$i]['m4']>0.000)&&($rec[$i]['m4']<3))
			$rec[$i]['m4']="N";
		else $rec[$i]['m4']="";

		if($rec[$i]['m5']>=8)
			$rec[$i]['m5']="Y";
		else if(($rec[$i]['m5']==0.000))
			$rec[$i]['m5']="";
		else if(($rec[$i]['m5']>0.000)&&($rec[$i]['m5']<8))
			$rec[$i]['m5']="N";
		else $rec[$i]['m5']="";

		if($rec[$i]['m6']>=2)
			$rec[$i]['m6']="Y";
		else if(($rec[$i]['m6']==0.000))
			$rec[$i]['m6']="";
		else if(($rec[$i]['m6']>0.000)&&($rec[$i]['m6']<2))
			$rec[$i]['m6']="N";
		else $rec[$i]['m6']="";

		if($rec[$i]['m8']>=7)
			$rec[$i]['m8']="Y";
		else if(($rec[$i]['m8']==0.000))
			$rec[$i]['m8']="";
		else if(($rec[$i]['m8']>0.000)&&($rec[$i]['m8']<7))
			$rec[$i]['m8']="N";
		else $rec[$i]['m8']="";

		
		$list.='<tr class="'.$clsRow.'">
    <td class="center">'.$rec[$i]['user_email'].'</td>
    <td class="center">'.$rec[$i]['cc_mail'].'</td>
    <td class="center">'.$rec[$i]['sendondate'].'</td>
    <td class="center">'.$rec[$i]['emailread'].'</td>
    <td class="center">'.$rec[$i]['m1'].'</td>
    <td class="center">'.$rec[$i]['m2'].'</td>
    <td class="center">'.$rec[$i]['m3'].'</td>
    <td class="center">'.$rec[$i]['m4'].'</td>
    <td class="center">'.$rec[$i]['m5'].'</td>
    <td class="center">'.$rec[$i]['m6'].'</td>
    <td class="center">'.$rec[$i]['m8'].'</td>
    <td class="center">'.$rec[$i]['total'].'</td>
    <td class="center">'.$st.'</td>
    </tr>';


		$j++;
	}

	if($list=="")
	{
		$list="<tr><td colspan=10 align='center' height=50>No Record(s) Found.</td></tr>";
	}
?>

<!--************************************** code to include common header ***************************************/-->
<?php include(LIBRARY_PATH."includes/header.php");
	$commonHead = new CommonHead();
	$commonHead->commonHeader('Internal News Manager', $site['TITLE'], $site['URL']); 
?>
<!--************************************** End of code to include header ***************************************/-->
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
	<tr>
	  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr>
					<td>
						<?php 
						if($_GET['msg']!="")
						{
							echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
						}
						if($msg!="")
						{
							echo('<div class="notice">'.$message_arr[$msg].'</div>');
						}
						?>
					</td>
		        </tr>
		    </table>
	    </td>
	</tr>

	<tr>
	  <td colspan="2" valign="top"><?php include($file_name	);?></td> <!-- include file_name to get the page on controller page -->
	</tr>

	<tr>
	  <td colspan="2" valign="top"></td>
	</tr>
</table>
<!-- Right Ends -->


<?php include(LIBRARY_PATH."includes/footer.php");?>
