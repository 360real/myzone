<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script>
function richTextBox($fieldname, $value = "") {

   
   $CKEditor = new CKEditor();
   $config['toolbar'] = array(
    array( 'Bold', 'Italic', 'Underline', 'Strike'),
    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
    array('Format'),
    array( 'Font', 'FontSize', 'FontColor', 'TextColor'),
    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ),
    array('Image', 'Table', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'),
    array('Subscript','Superscript'),
    array( 'Link', 'Unlink' ),
    array('Source')
   );
   $CKEditor->basePath = '/ckeditor/';
   $CKEditor->config['width'] = 975;
   $CKEditor->config['height'] = 400;
   
   $CKEditor->editor($fieldname, $value, $config);
}
</script>
<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Blog Detail</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>" id="setCloseType">
<td align="right" ><strong><font color="#FF0000">*</font>Domain</strong></td>
  <td valign="bottom"><?php echo AjaxDropDownList(TABLE_MICROSITES,"relation_id","id","site_name",$_GET["relation_id"],$_GET["site_name"],"id",$site['CMSURL'],"locationDiv","locationLabelDiv")?></td>
</tr>


<tr class="<?php echo($clsRow1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Microsite Category</strong></td>
  <td><select id="<?php echo ($rec['project_status_id'])?>" class="drop_down" name="project_status_id" title="Name" style="width:234px;">
<option value="">-- Select --</option>
<option value="1" <?php if ($rec['project_status_id']=="1"){ echo("selected='selected'");} ?>>New Project</option>
<option value="2" <?php if ($rec['project_status_id']=="2"){ echo("selected='selected'");} ?>>Ready to Move Projects</option>
<option value="3" <?php if ($rec['project_status_id']=="3"){ echo("selected='selected'");} ?>>Under Construction Projects</option>
<option value="4" <?php if ($rec['project_status_id']=="4"){ echo("selected='selected'");} ?>>Pre- Launch</option>
<option value="5" <?php if ($rec['project_status_id']=="5"){ echo("selected='selected'");} ?>>Ongoing Project</option>
<option value="6" <?php if ($rec['project_status_id']=="6"){ echo("selected='selected'");} ?>>Near Possesion</option>
<option value="8" <?php if ($rec['project_status_id']=="8"){ echo("selected='selected'");} ?>>New Residential Projects</option>
</select>
<?php 
   // echo DropDownListWithoutMust("tsr_project_status","project_status_id","id","name",$_GET["project_status_id"],"id",$site['CMSURL'],"SubauthDiv","SubauthLabelDiv")?>
</td>
</tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Content</strong></td>
  <td><textarea name="content" class="ckeditor"  id="content" rows="8" cols="70"><?php echo($rec['content'])?></textarea>  </td>
</tr>


<tr class="<?php echo($clsRow1)?>">
    <td align="right" ><strong>Select Image</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['imgname']=='') {?> title="Select Image" <?php } ?> name="imgname" id="imgname" /></td>
          <td width="62%"><?php if($rec['imgname']):?>
            <br />
            <img src="<?php echo  ($site['MICROSITESECTIONIMAGEURL'].$rec['id']."/".$rec['imgname'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['imgname'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Banner Url</strong></td>
  <td><input name="banner_url" type="text" title="banner_url" value="<?php echo($rec['banner_url'])?>" size="50%" maxlength="120" /></td>
</tr>

  <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
           <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
           <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
           <td><textarea  name="meta_description" id="meta_description" rows="5" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
 </tr>

 <tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
  <td valign="bottom">
  <input type="radio" name="status" value="1" id="1" checked="checked" /> 
  <label for="1">Active</label> 
  <input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
  <label for="0">Inactive</label>
  </td>
 </tr>

<tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
 

</tbody>
</table>
</form>