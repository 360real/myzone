<?php
// include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
// include("C:/wamp/www/adminproapp/includes/set_main_conf.php");

include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");

include_once LIBRARY_PATH . "library/server_config_admin.php";

?>
<!DOCTYPE html>
<html>
   <head>
      <title>360Realtors commercial propertie in, Gurgaon | 360 Realtors </title>
      <meta name="keywords" content="360Realtors, 360Realtors Gurgaon, 360Realtors Price, 360Realtors Payment Plan, 360Realtors Floor Plan">
      <meta name="description" content="360Realtors - New commercial property Gurugram. Know all about 360Realtors such as size, price, location, floor plan, payment plan and guaranteed bookings."> 
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">  
      <link rel="stylesheet" href="https://staging.360realtors.com/360assets/commercial_css/bootstrap.min.css">
      <link rel="stylesheet" href="https://staging.360realtors.com/360assets/commercial_css/style.css?1586161639">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.23/owl.carousel.css">
      <link rel="stylesheet" href="https://staging.360realtors.com/360assets/commercial_css/owl.carousel.css">
      <link rel="stylesheet" href="https://staging.360realtors.com/360assets/commercial_css/owl.carousel.min.css">
      <link rel="stylesheet" href="https://staging.360realtors.com/360assets/nw_css/fonticon.css">
      <link rel="stylesheet" href="https://staging.360realtors.com/360assets/commercial_css/responsive.css?1586161639">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

      <script src="https://staging.360realtors.com/360assets/nw_js/commercial/bootstrap.min.js"></script>
            <!-- Google Analytics -->
      <script type="text/javascript"  media="nope!" onload="this.media='all'" async>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
          ga('create', 'UA-67871365-2', 'auto');
          ga('send', 'pageview');
      </script>

      <!-- Enquiry forms -->
      <script type="text/javascript"  media="nope!" onload="this.media='all'" async>
          window.addEventListener('load',function(){
          jQuery('button:contains(" Request For Callback")').click(function(){
          jQuery('body').append('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/941104915/?label=ViZQCJGYmXMQk77gwAM&amp;guid=ON&amp;script=0"/>')
          ga('send','event','button','click','request for callback');
          })
          });
      </script>

	<!-- Facebook Pixel Code -->
      <script type="text/javascript"  media="nope!" onload="this.media='all'" async>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window,document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '288293788427669');
      fbq('track', 'PageView');
      </script>
      <noscript>
      <img height="1" width="1"
      src="https://www.facebook.com/tr?id=288293788427669&ev=PageView
      &noscript=1"/>
      </noscript>
      <!-- End Facebook Pixel Code -->
      <!-- Global site tag (gtag.js) - Google Ads: 941104915 -->

      <script async src="https://www.googletagmanager.com/gtag/js?id=AW-941104915"  media="nope!" onload="this.media='all'"></script>
      <script type="text/javascript" async  media="nope!" onload="this.media='all'">
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'AW-941104915');

      </script>
      		        <meta property="fb:admins" content="240978709438113"/> <!-- It will be same on all pages -->
		<meta property="og:url" content="https://staging.360realtors.com/a-advani-paras-downtown-sector-53-gurgaon-pid-499"/> <!--url of that page(dynamic) -->
		<meta property="og:title" content="360Realtors commercial propertie in, Gurgaon | 360 Realtors"/> <!--Title of that page(dynamic) -->
		<meta property="og:site_name" content="360 realtors"/> <!-- It will be same on all pages -->
		<meta property="og:description" content="360Realtors - New commercial property Gurugram. Know all about 360Realtors such as size, price, location, floor plan, payment plan and guaranteed bookings."/> <!-- Meta Description of that page(dynamic) -->
		<meta property="og:type" content="website"/> <!-- It will be same on all pages -->
		<meta property="og:image" content="https://stagingstatic.360realtors.ws/commercial/499/mini/Tata_Codename_Infinium_Golf_Course_Road_3.jpg"/> <!--Banner image of that page(dynamic) -->
		<meta property="og:url" content="https://staging.360realtors.com/a-advani-paras-downtown-sector-53-gurgaon-pid-499"/>  <!--url of that page(dynamic) -->
		<meta property="og:locale" content="en_us"/>  <!-- It will be same on all pages -->

		<meta name="twitter:card" content="summary_large_image" />  <!-- It will be same on all pages -->
		<meta name="twitter:site" content="@360Realtors" />  <!-- It will be same on all pages -->
		<meta name="twitter:title" content="360Realtors commercial propertie in, Gurgaon | 360 Realtors" /> <!--Title of that page(dynamic) -->
		<meta name="twitter:description" content="360Realtors - New commercial property Gurugram. Know all about 360Realtors such as size, price, location, floor plan, payment plan and guaranteed bookings." /> <!-- Meta Description of that page(dynamic) -->
		<meta name="twitter:image" content="https://stagingstatic.360realtors.ws/commercial/499/mini/Tata_Codename_Infinium_Golf_Course_Road_3.jpg" /> <!---Banner image of that page(dynamic) -->
      <script type="text/javascript" > ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js"); /* custom configuration goes here (www.olark.com/documentation) */ olark.identify('5677-609-10-5760');</script>
      <style>
         /* Note: Try to remove the following lines to see the effect of CSS positioning */
         .affix {
         top: 0;
         width: 100%;
         z-index: 9999 !important;
         }
         .affix + .container-fluid {
         padding-top: 70px;
         }
      </style>
   </head>
   <body>
        
<style type="text/css">
    .slideweb .heart_icon {
    position: static !important;
    bottom: 10px !important;
    right: 15px !important;
    color: #fff !important;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
<link href="https://staging.360realtors.com/360assets/commercial_css/ion.rangeSlider.min.css" rel="stylesheet" media="screen">

<?php 
if(isset($_GET['id']))
{
	$id = base64_decode($_GET['id']);
	$sql = " SELECT * from ".TABLE_COMMERCIAL_PROPERTIES."  WHERE id=".$id;
	$rec = $obj_mysql->get_assoc_arr($sql);


	// city name
	$city_name = "";
	if($rec['cty_id']!='')
	{
		$sql_city = "SELECT city from ".TABLE_CITIES." where id=".$rec['cty_id'];
		$rec_city = $obj_mysql->get_assoc_arr($sql_city);
		$city_name = $rec_city['city'];
	}
	// city name

	// cluster name
	$cluster_name = "";
	if($rec['cluster_id']!='')
	{
		$sql_cluster = "SELECT cluster as name from ".TABLE_CLUSTER." where id=".$rec['cluster_id'];
		$rec_cluster = $obj_mysql->get_assoc_arr($sql_cluster);
		$cluster_name = $rec_cluster['name'];
	}
	// cluster name

	// developer name
	$developer_name = "";
	if($rec['devlpr_id']!='')
	{
		$sql_developer = "SELECT name as name from ".TABLE_DEVELOPERS." where id=".$rec['devlpr_id'];
		$rec_developer = $obj_mysql->get_assoc_arr($sql_developer);
		$developer_name = $rec_developer['name'];
	}
	// developer name

	// location name
	$location_name = "";
	if($rec['loc_id']!='')
	{
		$sql_location = "SELECT location as name from ".TABLE_LOCATIONS." where id=".$rec['loc_id'];
		$rec_location = $obj_mysql->get_assoc_arr($sql_location);
		$location_name = $rec_location['name'];
	}
	// location name

	// property type name
	$property_type_name = "";
	if($rec['property_type']!='' && $rec['property_type']>0 )
	{
		$sql_property_type = "SELECT name from ".TABLE_PROPERTY_TYPE." where id=".$rec['property_type'];
		$rec_property_type = $obj_mysql->get_assoc_arr($sql_property_type);
		$property_type_name = $rec_property_type['name'];
	}
	// property type name

	// possession type name
	$project_status_name = "";
	if($rec['project_status_id']!='' && $rec['project_status_id']>0 )
	{
		$sql_project_status = "SELECT name from tsr_project_status where id=".$rec['project_status_id'];
		$rec_project_status = $obj_mysql->get_assoc_arr($sql_project_status);
		$project_status_name = $rec_project_status['name'];
	}
	// property type name

	// sale type name
	$sale_type_name = "";
	if($rec['sale_type']!='' && $rec['sale_type']>0 )
	{
		$sql_sale_type = "SELECT name from ".TABLE_SALE_TYPE." where id=".$rec['sale_type'];
		$rec_sale_type = $obj_mysql->get_assoc_arr($sql_sale_type);
		$sale_type_name = $rec_sale_type['name'];
	}
	// property type name

}
?>
<div class="details_section">
    <div class="container details_cover">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrum_cover">
                    <p>	<a href="#">Home</a> &gt;
                        	<a href="#"><?= $city_name ?> </a>&gt;
                            	<a href="#"><?= $cluster_name ?></a>&gt;
                                	<span class="activebrdcame"> <?= $rec['property_name'] ?> </span> 
                    </p>
               </div>
            </div>
                <div class="detail_img_covr">
                    <div class="detailpage_left">
                        <div class="well-none">
                            <div id="myCarouselproductdetail" class="carousel slide">
                                <div class="carousel-inner photos">
                                	<?php 
										$sql_banner = "SELECT image_url from ".TABLE_COMMERCIAL_IMAGE." where image_category='mini' and property_id=".$id;
										$rec_banner = $obj_mysql->getAllData($sql_banner);
										$i=0;
										foreach ($rec_banner as $key => $banner) {
                                			$img_src= $webSiteURL.'media_images/commercial/'.$id.'/mini/'.$banner['image_url'];
											?>
											<div class="item <?php if($i==0){ echo 'active';}?>">
		                                        <a href="<?= $img_src ?>" data-lightbox="photos">
		                                            <img src="<?= $img_src ?>" alt="Paras Downtown" class="img-responsive img-fluid">
		                                        </a>
		                                    </div>
											<?php
											$i++;
										}
										// exit;


                                	?>
                                    
                                </div>
                                <a class="right carousel-control innerarrow" href="#myCarouselproductdetail" data-slide="next">
                                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                                </a>

                                <a class="left carousel-control innerarrow" href="#myCarouselproductdetail" data-slide="prev">
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                </a>

                                <div class="heart_icon"><a href="javascript:void(0)" class="list-icon">
                                    <i class="fa fa-heart-o" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Add to Favorites" id="499" onclick="addtocompare(this.id, this)" data-original-title="Add to Favorites!"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="Property_detail">

                        <h1 class="detailtitle text-left col-lg-6 padding0"><?= $rec['property_name'] ?></h1>
                        <h2 class="col-lg-6 padding0" style="margin: 0;"><span class="detailprice text-right "> ₹  <?php  echo no_to_words($rec['price_to_display']); ?>.*                         
                            <img class="exclamationicon" src="https://staging.360realtors.com/360assets/nw_images/exclamation.svg">
                            <span>
                            </span></span></h2>

                            <div class="clearfix"></div>
                            <h6 class="propertydetailstwo">By <a href="https://staging.360realtors.com/a-advani-realty-new-projects-blid-1426"><?= $developer_name ?></a>
                                <span class="pull-right text-right"><?= $rec['covered_area'] ?> <?= $rec['covered_area_unit'] ?> <img class="exclamationicon" src="https://staging.360realtors.com/360assets/nw_images/exclamation.svg">
                                </span>
                            </h6>

                            <h6 class="propertydetailstwo">
                                <span class="map_icon"></span><?= $location_name ?>, <?= $city_name ?>  <a class="showmap" href="#locations">Show on Map</a>
                                
                            </h6>
                        </div>
                        <div class="spacication whitebackground spacication_fixed_top">
                            <ul class="spacicationmenu">
                                <li><a class="active" class="" href="#Overviewmainbox">Overview</a></li>
                                <li><a class="scroll" href="#floor_planemainbox">Floor Plan</a></li>
                                <li><a class="scroll" href="#amentiesmainbox">Amenities</a></li>
                                <li><a class="scroll" href="#Specificationmainbox">Specification</a></li>
                              <!--   <li><a class="scroll" href="#facility">Facility</a></li> -->
                                <!-- <li><a class="scroll" href="#paymentmainbox">Payment</a></li>
                                <li class="emicalculated"><a class="scroll" href="#emi">EMI</a></li>
                                <li><a class="scroll" href="#locationsmainbox">Location</a></li>
                                <li><a class="scroll" href="#reatingmainbox">Rating</a></li> -->

                            </ul>
                        </div>
                        <div class="Property_detailstwo" id="Overviewmainbox">
                            <span class="anchr" id="Overview"></span>
                            <h4 class="overviewtitle">Overview of <?= $rec['property_name'] ?></h4>
                            <div class="col-md-4 col-sm-12 col-xs-12 padding0">
                                <div class="overview-details">
                                    <h2> Project Area</h2>
                                    <p><?= $rec['covered_area'] ?> <?= $rec['covered_area_unit'] ?></p>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 padding0">
                                <div class="overview-details">
                                    <h2>Project Type</h2>
                                    <p><?= $property_type_name ?></p>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 padding0">
                                <div class="overview-details">
                                    <h2> Project Status</h2>
                                    <p><?= $project_status_name ?></p>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 padding0">
                                <div class="overview-details">
                                    <h2> Possession on</h2>
                                    <p><?= $rec['possession_date'] ?></p>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 padding0">
                                <div class="overview-details">
                                    <h2>Configurations</h2>
                                    <p><?= $sale_type_name ?></p>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 col-xs-12 padding0">
                                <div class="overview-details">
                                    <p></p>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                                      <div class="text_dis">
                                                                                <div class="row">
                                        	<div class="col-xs-12">
		                                        <div id="lessdetail">
		                                        	<?= substr($rec['new_overview'],0,500) ?>...

	<span class="about_read"><a href="javascript:void(0)" id="morebtndetail" onclick="readmorefns();">Read More</a></span></div>
		                                        <div class="show-read-more" id="moredetail" style="display:none">
		                                        	<?= $rec['new_overview'] ?>
<span class="about_read"><a href="javascript:void(0)" id="lessbtndetail" onclick="readlessfns();" style="display: block;">Read Less</a></span>
		                                        </div>
		                                    </div>
                                        </div>
                                                                                </div>

                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="Property_detailstwo" id="floor_planemainbox">
                                        <h4 class="overviewtitle">Floor Plan of Paras Downtown</h4>
                                        <br>
                                        <div id="floor-plantwo" class="owl-carousel">
	                                                                                </div>
                                    </div>
                                        <div class="Property_detailstwo" id="amentiesmainbox">
                                            <h4 class="overviewtitle">Amenities</h4>
                                                <?php
                                                	$new_amenities =  $rec['new_amenities'];
													if($new_amenities!='' && $new_amenities!=null)
													{
														$amenities_arr = explode(",",$new_amenities);
														foreach ($amenities_arr as $amenities) {
															$sql_amenities = "SELECT amenities,css_class from ".TABLE_COMMERCIAL_AMENITIES." where id=".$amenities;
															$rec_amenities = $obj_mysql->get_assoc_arr($sql_amenities);
															$amenities_name = $rec_amenities['amenities'];
															$css_class = $rec_amenities['css_class'];
															?>
															<div class="col-md-4 col-sm-6 col-xs-6 amenties"><i class="fa <?= $css_class;?>" aria-hidden="true"></i><?= $amenities_name;?></div>
															<?php
														}
													}
                                                ?>
                                                
                                                                                        <div class="clearfix"></div>
                                        </div>
                                        <div class="Property_detailstwo" id="Specificationmainbox">
                                            <h4 class="overviewtitle">Specification</h4>
                                                    <?php
                                                	$specification =  $rec['specification'];
													if($specification!='' && $specification!=null)
													{
														$specification_arr = explode(",",$specification);
														foreach ($specification_arr as $specification) {
															$sql_specification = "SELECT name from ".TABLE_COMMERCIAL_SPECIFICATION." where id=".$specification;
															$rec_specification = $obj_mysql->get_assoc_arr($sql_specification);
															$specification_name = $rec_specification['name'];
															?>
															<div class="col-md-4 col-sm-6 col-xs-6 amenties"><?= $specification_name;?></div>
															<?php
														}
													}
                                                ?>
                                                                                        <div class="clearfix"></div>
                                        </div>

                                        <!-- <div class="Property_detailstwo" id="facility">
                                            <h4 class="overviewtitle">facility</h4>
                                                                                                                                                                                
                                            <div class="clearfix"></div>
                                        </div> -->
                                        <div class="Property_detailstwo" id="paymentmainbox" style="display: none;">
                                            <h4 class="overviewtitle">Payment</h4>
                                            <div class="showmore pay_tbe" id="showtable" style="height: auto;">
                                                <div class="table-responsive ">
                                                    <table class="payment-area">
                                                        <thead>
                                                            <tr>
                                                                <th>Unit Type</th>
                                                                <th>Size (SQ. FT.)</th>
                                                                <th>Price (SQ. FT.)</th>
                                                                <th>Amount</th>
                                                                <th>Booking Amt</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                                                                                     

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>    
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="Property_detailstwo" id="emi" style="display: none;"> 
                                            <h4 class="overviewtitle">EMI Calulator</h4>
                                            <br>
                                            <form name='emi_form'>
                                            <div class="emicover">
                                                <div class="type gridfix type_mb">
                                                    <p>type</p>
                                                </div>
                                                <div class="showroom_dropdown gridfix type_dropmb">
                                                    <!--- -->
                                                    <select id="ddn_type" name='amount'   class="form-control input-lg">
                                                                                                           </select>
                                                    <i class="fa fa-chevron-down"></i>
                                                </div>
                                                <div class="type gridfix">
                                                    <p class="incometext text-center">Annual Income</p>
                                                </div>
                                                <div class="annual_price gridfix">
                                                    <div class="annual_pricecvr firstbox">
                                                        <p><input type="text" name="annual_income" class="form-control" value=''></p>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="downpaymentcover">
                                                <div class="type gridfix">
                                                    <p>Down Payment</p>
                                                </div>
                                                <div class="annual_price gridfix">
                                                    <div class="annual_pricecvr">
                                                        <p><span class="priceicon"></span> <span class="priceinner"><input type="" name="down_payment" class="form-control" value='0.00' id="down_payment"></span></p>
                                                    </div>
                                                </div>
                                                <div class="type gridfix">
                                                    <p class="incometext text-center">Interest Rate</p>
                                                </div>
                                                <div class="annual_price gridfix">
                                                    <div class="annual_pricecvr">
                                                        <p><span class="priceicon"></span> <span class="priceinner"><input type="" name="interest" class="form-control" placeholder="%" value='10'></span></p>
                                                    </div>
                                                </div>
                                                <div class="type gridfix">
                                                    <p class="incometext text-center">Tenure</p>
                                                </div>
                                                <div class="showroom_dropdown gridfix">
                                                    <!--- -->
                                                   <select id="emiTerm" name='term' class="select form-control">
                                                                                                    <option value='30' >30 Years</option>
                                                                                                    <option value='29' >29 Years</option>
                                                                                                    <option value='28' >28 Years</option>
                                                                                                    <option value='27' >27 Years</option>
                                                                                                    <option value='26' >26 Years</option>
                                                                                                    <option value='25' >25 Years</option>
                                                                                                    <option value='24' >24 Years</option>
                                                                                                    <option value='23' >23 Years</option>
                                                                                                    <option value='22' >22 Years</option>
                                                                                                    <option value='21' >21 Years</option>
                                                                                                    <option value='20' >20 Years</option>
                                                                                                    <option value='19' >19 Years</option>
                                                                                                    <option value='18' >18 Years</option>
                                                                                                    <option value='17' >17 Years</option>
                                                                                                    <option value='16' >16 Years</option>
                                                                                                    <option value='15' >15 Years</option>
                                                                                                    <option value='14' >14 Years</option>
                                                                                                    <option value='13' >13 Years</option>
                                                                                                    <option value='12' >12 Years</option>
                                                                                                    <option value='11' >11 Years</option>
                                                                                                    <option value='10' >10 Years</option>
                                                                                                    <option value='9' >9 Years</option>
                                                                                                    <option value='8' >8 Years</option>
                                                                                                    <option value='7' >7 Years</option>
                                                                                                    <option value='6' >6 Years</option>
                                                                                                    <option value='5' >5 Years</option>
                                                                                                    <option value='4' >4 Years</option>
                                                                                                    <option value='3' >3 Years</option>
                                                                                                    <option value='2' >2 Years</option>
                                                                                                    <option value='1' >1 Years</option>
                                                
                                                </select>
                                                    <i class="fa fa-chevron-down"></i>
                                                </div>

                                            </div> 
                                            <div><hr></div>
                                            <div class="totalemi_payment">
                                                <div class="payment_grid">
                                                    <h3>Total Down payment</h3>
                                                     <p class="emi-amt totalPaid">₹ <span id='totalAmountLabel'></span></p>
                                                    <p class="emi-amt">₹ <span id='amountPaid'></span></p>
                                                </div>
                                                <div class="payment_grid">
                                                    <h3>Interest to be paid</h3>
                                                    <p class="emi-amt">₹ <span id='totalInterestToBePaid'></span></p>
                                                </div>
                                                <div class="payment_grid total_emi">
                                                    <h3>EMI</h3>
                                                    <p class="totalpayment text-right">₹ <span id='emiValue'></span></p>
                                                </div>
                                            </div>
                                            </form>
                                            <div class="clearfix"></div>
                                        </div>
                                                      <div class="Property_detailstwo location_cover  location" id="locationsmainbox" style="display: none;">
                            <span class="anchr" id="locations"></span>
                            <h4 class="overviewtitle">Location of <?= $rec['property_name']; ?></h4>
                            <br>
                                <div style="width:100%;height:400px;background:url('/360assets/nw_images/map.png')no-repeat 50% #0000001a">
                                <div id="maps" style="width:100%;height:400px;opacity:"></div>
                                </div>
                            <div class="col-sm-12 col-xs-12 padding0 overflow_scroll">
                            <div class="col-xs-5 verticaltabsmenu padding0">
                                <ul class="nav nav-tabs tabs-left" id='locFilter'>
                                    <li type='airport' radius='25000' zoom='11' class="active"><a href="#transpotaion" data-toggle="tab">Airport<span class="arrowspan_left"></span></a></li>
                                    <li type='bank' radius='10000' zoom='13'><a href="#health" data-toggle="tab">ATM<span class="arrowspan_left"></span></a></li>
                                    <li type='hospital' radius='15000' zoom='11'><a href="#business" data-toggle="tab">Hospital<span class="arrowspan_left"></span></a></li>
                                    <li type='bus_station' radius='7000' zoom='14'><a href="#hotel" data-toggle="tab">Bus Station<span class="arrowspan_left"></span></a></li>
                                    <li type='shopping_mall' radius='10000' zoom='13'><a href="#shooping" data-toggle="tab">Shopping & Fun<span class="arrowspan_left"></span></a></li>
                                    <li type='school' radius='10000' zoom='13'><a href="#school" data-toggle="tab">Schools<span class="arrowspan_left"></span></a></li>
                                </ul>
                            </div>
                            <div class="col-xs-7">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                <br>
                                    <div class="tab-pane active">

                                        <table id='distanceTable'>
                                            <tbody></tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                                                </div>
                            <div class="clearfix"></div>
                        </div>
                                                            <div class="Property_detailstwo" id="reatingmainbox" style="display: none;">
                                            <h4 class="overviewtitle">Ratings</h4>
                                            <br>
                                            <div class="reatingscroller ratingcvr">
                                                        <table>
                                                                           <tr>
                                        <td>
                                            <div class="reatingtabsection">
                                                <div class="progress-radial progress-100 setsize">
                                                    <div class="overlay setsize">
                                                        <p>5</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <p class="text-center">Location</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="reatingtabsection">
                                                <div class="progress-radial progress-70 setsize">
                                                    <div class="overlay setsize">
                                                        <p>3</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <p class="text-center">Safety</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="reatingtabsection">
                                                <div class="progress-radial progress-80 setsize">
                                                    <div class="overlay setsize">
                                                        <p>4</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <p class="text-center">Transport</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="reatingtabsection">
                                                <div class="progress-radial progress-80 setsize">
                                                    <div class="overlay setsize">
                                                        <p>4</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <p class="text-center">Entertainment</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="reatingtabsection">
                                                <div class="progress-radial progress-100 setsize">
                                                    <div class="overlay setsize">
                                                        <p>5</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <p class="text-center">Connectivity</p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="reatingtabsection">
                                                <div class="progress-radial progress-80 setsize">
                                                    <div class="overlay setsize">
                                                        <p>4</p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <br>
                                                <p class="text-center">Shopping</p>
                                            </div>
                                        </td>
                                    </tr>
                                                                </table>
                                            </div>   
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="information_area">
                                            


                            </div>
                        </div>
                    </div>
                </div>
            </div>
     
     
    <!-- Modal -->
<div class="modal fade thankupopup_newletter" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close close-thankupopup_newletter" data-dismiss="modal">&times;</button>
        <div class="text-center">
            <div class="checkclass">
                <i class="fa fa-check" aria-hidden="true"></i>
            </div>
            <div class="clearfix"></div>
            <div class="footerpopupcontent">
                <p>Thank you for subscribing to our newsletter.</p>
                <p>Your subscription now is completed</p>

                <p class="textnumber">1800-1200-360</p>
            </div>
            <div class="clearfix"></div>
            <ul class="popuplinkicon">
                <li class="facebooklink"><a href="https://www.facebook.com/360Realtors"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li class="linkedinlink"><a href="https://www.linkedin.com/company/360-realtors-llp"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                <li class="linkedinlink"><a href="https://www.instagram.com/360realtors/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                
            </ul>
        </div>
      </div>
    </div>
  </div>
</div>
      <input type="hidden" class="campaign_name" value="Direc_2785_360We">


<input type="hidden" class="curr_href_url" value="https://staging.360realtors.com/a-advani-paras-downtown-sector-53-gurgaon-pid-499">
<input type="hidden" class="referer_path" value="">
<input type="hidden" class="residenceCity" value="1">

<!-- <div id="enquire" class="modal fade" role="dialog"></div> for Popup Enquiry Forms -->
<div class="modal fade" id="enquire" role="dialog"></div>
      <input type="hidden" name="cityId" class="cityId" value="1">
      <div id="comparepopset" class="modal fade " role="dialog"></div>
                          <div class="modal fade signin-up" id="login" role="dialog" data-keyboard="true" aria-hidden="true" tabindex='-1' >
            <div class="vertical-alignment-helper">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" id="loginCloseClick" onclick="clearLoginErrorDiv();" data-dismiss="modal">&times;</button>
                            <div class="clearfix"></div>
                            <div class="row margin0">
                                <div class="signup-points col-sm-6">
                                    <div class="content">
                                        <ul class="res_text" style="display:block">
                                            <h2>New here?</h2>
                                            <p>Register for features like favorites, compare, comprehensive advice, consultation and more.</p>
                                            <a href="javascript:void(0)" id="btnClick--1" class="btn_reg">Register</a>
                                        </ul>
                                        <ul class="login_text" style="display:none">
                                            <h2>Already a member?</h2>
                                            <p>Login to retrieve your previously saved properties and more</p>
                                            <a href="javascript:void(0)" id="btnClick" class="btn_reg">Login</a>
                        </ul>
                                    </div>

                                </div>

                                <div class="col-md-6 frombox">
                                    <div class="frmsign">
                                        <div class="logSgn">
                                            <ul class="nav view_mbre">
                                                <li id="loginhd"><a href="javascript:void(0)" id="btnClick" class="sclas">Login</a>
                                                </li>
                                                 <li id="signup"><a href="javascript:void(0)" id="btnClick--1">Sign Up</a></li>

                                            </ul>
                                        </div>


                                        <div id="singUp">
                                        <h2 class="heading">Login</h2>
                                            <form id="user-login-form-360">

                                                <div id="validation-error-r-login"></div>

                                                <input type="hidden" name="action" id="frm_action" value="login">
                                                <input type="hidden" name="url" id="frm_action_url" value="">
                                                <input type="hidden" name="val" id="frm_action_val" value="">

                                                <input type="hidden" id="frm_title_name" name="frm_title_name" value="">
                                                <input type="hidden" id="frm_title_id" name="frm_title_id" value="">
                                                <input type="hidden" id="frm_cat_id" name="frm_cat_id" value="">
                                                <input type="hidden" id="frm_p_id" name="frm_p_id" value="">
                                                <input type="hidden" id="frm_type_id" name="frm_type_id" value="">

                                                <div class="group">
                                                    <input type="text" id="log_email" required>
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label>Email</label>
                                                    <font style="display: none;" id="log-email-error">
                                                        </font>
                                                </div>


                                                <div class="group">
                                                    <input type="password" id="log_password" required>
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label>Password</label>
                                                    <span class="eye_icon" ></span>
                                                    <font id="log-password-error"></font>
                                                </div>



                                                <input type="button" value="Submit" class="floatL"
                                                       onclick="UserLoginSubmitProcess();"/>

                                                       <div class="spinner" style="display: none;">
                                                                <img src="https://staging.360realtors.com/360assets/images/loading.gif">
                                                            </div>
                                                <div class="floatL btmLogin"></div>

                                                <div id="signUpdddtrmwww" class="signUptrm forget-password"
                                                     onclick="aaa()">Forgot Password ?
                                                </div>
                                                <span id="log-check-error"></span>
                                                <div class="cl"></div>
                                            </form>

                                            <div class="cl"></div>


                                            <div class="orIconsection pd-or-sec">
                                                <div class="border_line"></div>
                                                <font>or login with</font>
                                                <div class="border_line pull-right"></div>
                                                <div class="or_h"></div>
                                            </div>
                                            <div class="row clearfix social">
                                                <div class="col-md-6">
                                                    <a href="javascript:void(0);" onclick="FBLogin();" class="fb-btn btn btn-block">
                                                    <i class="fa fa-facebook-square" aria-hidden="true"></i>Facebook</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="javascript:void(0);" class="gplus-btn btn btn-block" onclick="openPopGoogle();">
                                                       <span class="google_icon"></span>Google</a>
                                                </div>

                                            </div>
                                            <p class="cpy_text">By continuing, you agree to 360 Realtors’ Terms & Conditions, Privacy Policy</p>

                                        </div>
                                        <div id="forgot" style="display: none;">
                                            <h2 class="heading">Enter Your Mail ID</h2>
                                            <form>

                                                <div class="row">
                                                    <div class="col-md-12 paddingLR5">
                                                        <div class="group">
                                                            <input type="text" id="tsr_pass_forget" name="tsr_pass_forget_email" required>
                                                            <span class="highlight"></span>
                                                            <span class="bar"></span>
                                                            <label>Enter Email ID</label>
                                                            <font id="log-forget-error"></font>
                                                        </div>
                                                    </div>
                                                    <input type="button" value="Submit" onclick="submitUserForgetPassword();" class="floatL"/>
                                                    <div class="spinner" style="display: none;">
                                                                <img src="https://staging.360realtors.com/360assets/images/loading.gif">
                                                            </div>
                                                </div>
                                            </form>
                                            <div class="cl"></div>


                                            <div class="row clearfix">


                                            </div>

                                        </div>
                                        <div id="logiN" class="frmsign signup" style="display: none;">
                                        <h2 class="heading marbtm">Register</h2>
                                            <font id="validation-error-r-register"></font>
                                            <form>
                                                <span id="account-verification-email-message" class="success-text"></span>
                                                <div class="row">

                                                    <div class="col-md-12 paddingLR5">
                                                        <div class="group">
                                                            <input class="name" type="text" id="tsr_username" required>
                                                            <span class="highlight"></span>
                                                            <span class="bar"></span>
                                                            <label>Name</label>
                                                            <span id="tsr-uname-error"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 paddingLR5">
                                                        <div class="group">
                                                            <input class="email" type="text" id="tsr_emailId" required>
                                                            <span class="highlight"></span>
                                                            <span class="bar"></span>
                                                            <label>Email</label>
                                                            <span id="tsr-email-error"></span>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-12 paddingLR5">
                                                    
                                                            <div class="countycode">
                                                            <select class="selectpicker ccode" data-live-search="true">
                                                                                                                <option>+91 India</option>
                                                                                                                <option>+971 Dubai</option>
                                                                                                                <option>+86 China</option>
                                                                                                                <option>+44 United Kingdom</option>
                                                                                                                <option>+1 United States of America</option>
                                                                                                                <option>+66 Thailand</option>
                                                                                                                <option>+65 Singapore</option>
                                                                                                                <option>+41 Switzerland</option>
                                                                                                                <option>+94 Sri Lanka</option>
                                                                                                                <option>+966 Saudi Arabia</option>
                                                                                                                <option>+92 Pakistan</option>
                                                                                                                <option>+971 UAE</option>
                                                                                                                <option>+93 Afghanistan</option>
                                                                                                                <option>+355 Albania</option>
                                                                                                                <option>+213 Algeria</option>
                                                                                                                <option>+1684 American Samoa</option>
                                                                                                                <option>+376 Andorra</option>
                                                                                                                <option>+244 Angola</option>
                                                                                                                <option>+1264 Anguilla</option>
                                                                                                                <option>+672 Antarctica/Cocos (keeling) Islands/Norfo</option>
                                                                                                                <option>+1268 Antigua And Barbuda</option>
                                                                                                                <option>+54 Argentina</option>
                                                                                                                <option>+374 Armenia</option>
                                                                                                                <option>+297 Aruba</option>
                                                                                                                <option>+61 Australia/Christmas Island</option>
                                                                                                                <option>+43 Austria</option>
                                                                                                                <option>+994 Azerbaijan</option>
                                                                                                                <option>+1242 Bahamas</option>
                                                                                                                <option>+973 Bahrain</option>
                                                                                                                <option>+880 Bangladesh</option>
                                                                                                                <option>+1246 Barbados</option>
                                                                                                                <option>+375 Belarus</option>
                                                                                                                <option>+32 Belgium</option>
                                                                                                                <option>+501 Belize</option>
                                                                                                                <option>+229 Benin</option>
                                                                                                                <option>+1441 Bermuda</option>
                                                                                                                <option>+975 Bhutan</option>
                                                                                                                <option>+591 Bolivia</option>
                                                                                                                <option>+387 Bosnia And Herzegovina</option>
                                                                                                                <option>+267 Botswana</option>
                                                                                                                <option>+0 Bouvet Island</option>
                                                                                                                <option>+55 Brazil</option>
                                                                                                                <option>+246 British Indian Ocean Territory</option>
                                                                                                                <option>+673 Brunei Darussalam</option>
                                                                                                                <option>+359 Bulgaria</option>
                                                                                                                <option>+226 Burkina Faso</option>
                                                                                                                <option>+257 Burundi</option>
                                                                                                                <option>+855 Cambodia</option>
                                                                                                                <option>+237 Cameroon</option>
                                                                                                                <option>+238 Cape Verde</option>
                                                                                                                <option>+1345 Cayman Islands</option>
                                                                                                                <option>+236 Central African Republic</option>
                                                                                                                <option>+235 Chad</option>
                                                                                                                <option>+56 Chile</option>
                                                                                                                <option>+57 Colombia</option>
                                                                                                                <option>+269 Comoros</option>
                                                                                                                <option>+242 Congo/Congo, The Democratic Republic Of </option>
                                                                                                                <option>+682 Cook Islands</option>
                                                                                                                <option>+506 Costa Rica</option>
                                                                                                                <option>+225 Cote Divoire</option>
                                                                                                                <option>+385 Croatia</option>
                                                                                                                <option>+53 Cuba</option>
                                                                                                                <option>+357 Cyprus</option>
                                                                                                                <option>+420 Czech Republic</option>
                                                                                                                <option>+45 Denmark</option>
                                                                                                                <option>+253 Djibouti</option>
                                                                                                                <option>+1767 Dominica</option>
                                                                                                                <option>+1809 Dominican Republic</option>
                                                                                                                <option>+593 Ecuador</option>
                                                                                                                <option>+20 Egypt</option>
                                                                                                                <option>+503 El Salvador</option>
                                                                                                                <option>+240 Equatorial Guinea</option>
                                                                                                                <option>+291 Eritrea</option>
                                                                                                                <option>+372 Estonia</option>
                                                                                                                <option>+251 Ethiopia</option>
                                                                                                                <option>+500 Falkland Islands (malvinas)</option>
                                                                                                                <option>+298 Faroe Islands</option>
                                                                                                                <option>+679 Fiji</option>
                                                                                                                <option>+358 Finland</option>
                                                                                                                <option>+33 France</option>
                                                                                                                <option>+594 French Guiana</option>
                                                                                                                <option>+689 French Polynesia</option>
                                                                                                                <option>+0 French Southern Territories</option>
                                                                                                                <option>+241 Gabon</option>
                                                                                                                <option>+220 Gambia</option>
                                                                                                                <option>+995 Georgia</option>
                                                                                                                <option>+49 Germany</option>
                                                                                                                <option>+233 Ghana</option>
                                                                                                                <option>+350 Gibraltar</option>
                                                                                                                <option>+30 Greece</option>
                                                                                                                <option>+299 Greenland</option>
                                                                                                                <option>+1473 Grenada</option>
                                                                                                                <option>+590 Guadeloupe</option>
                                                                                                                <option>+1671 Guam</option>
                                                                                                                <option>+502 Guatemala</option>
                                                                                                                <option>+224 Guinea</option>
                                                                                                                <option>+245 Guinea-bissau</option>
                                                                                                                <option>+592 Guyana</option>
                                                                                                                <option>+509 Haiti</option>
                                                                                                                <option>+0 Heard Island And Mcdonald Islands</option>
                                                                                                                <option>+39 Holy See (vatican City State)</option>
                                                                                                                <option>+504 Honduras</option>
                                                                                                                <option>+852 Hong Kong</option>
                                                                                                                <option>+36 Hungary</option>
                                                                                                                <option>+354 Iceland</option>
                                                                                                                <option>+62 Indonesia</option>
                                                                                                                <option>+98 Iran, Islamic Republic Of</option>
                                                                                                                <option>+964 Iraq</option>
                                                                                                                <option>+353 Ireland</option>
                                                                                                                <option>+972 Israel</option>
                                                                                                                <option>+39 Italy</option>
                                                                                                                <option>+1876 Jamaica</option>
                                                                                                                <option>+81 Japan</option>
                                                                                                                <option>+962 Jordan</option>
                                                                                                                <option>+7 Kazakhstan</option>
                                                                                                                <option>+254 Kenya</option>
                                                                                                                <option>+686 Kiribati</option>
                                                                                                                <option>+850 Korea  Democratic Peoples Republic Of</option>
                                                                                                                <option>+82 Korea, Republic Of</option>
                                                                                                                <option>+965 Kuwait</option>
                                                                                                                <option>+996 Kyrgyzstan</option>
                                                                                                                <option>+856 Lao Peoples Democratic Republic</option>
                                                                                                                <option>+371 Oman</option>
                                                                                                                <option>+961 Lebanon</option>
                                                                                                                <option>+266 Lesotho</option>
                                                                                                                <option>+231 Liberia</option>
                                                                                                                <option>+218 Libyan Arab Jamahiriya</option>
                                                                                                                <option>+423 Liechtenstein</option>
                                                                                                                <option>+370 Lithuania</option>
                                                                                                                <option>+352 Luxembourg</option>
                                                                                                                <option>+853 Macao</option>
                                                                                                                <option>+389 Macedonia, The Former Yugoslav Republic </option>
                                                                                                                <option>+261 Madagascar</option>
                                                                                                                <option>+265 Malawi</option>
                                                                                                                <option>+60 Malaysia</option>
                                                                                                                <option>+960 Maldives</option>
                                                                                                                <option>+223 Mali</option>
                                                                                                                <option>+356 Malta</option>
                                                                                                                <option>+692 Marshall Islands</option>
                                                                                                                <option>+596 Martinique</option>
                                                                                                                <option>+222 Mauritania</option>
                                                                                                                <option>+230 Mauritius</option>
                                                                                                                <option>+262 Mayotte/Reunion</option>
                                                                                                                <option>+52 Mexico</option>
                                                                                                                <option>+691 Micronesia, Federated States Of</option>
                                                                                                                <option>+373 Moldova, Republic Of</option>
                                                                                                                <option>+377 Monaco</option>
                                                                                                                <option>+976 Mongolia</option>
                                                                                                                <option>+1664 Montserrat</option>
                                                                                                                <option>+212 Morocco/Western Sahara</option>
                                                                                                                <option>+258 Mozambique</option>
                                                                                                                <option>+95 Myanmar</option>
                                                                                                                <option>+264 Namibia</option>
                                                                                                                <option>+674 Nauru</option>
                                                                                                                <option>+977 Nepal</option>
                                                                                                                <option>+31 Netherlands</option>
                                                                                                                <option>+599 Netherlands Antilles</option>
                                                                                                                <option>+687 New Caledonia</option>
                                                                                                                <option>+64 New Zealand</option>
                                                                                                                <option>+505 Nicaragua</option>
                                                                                                                <option>+227 Niger</option>
                                                                                                                <option>+234 Nigeria</option>
                                                                                                                <option>+683 Niue</option>
                                                                                                                <option>+1670 Northern Mariana Islands</option>
                                                                                                                <option>+47 Norway/Svalbard And Jan Mayen</option>
                                                                                                                <option>+968 Oman UAE</option>
                                                                                                                <option>+680 Palau</option>
                                                                                                                <option>+970 Palestinian Territory, Occupied</option>
                                                                                                                <option>+507 Panama</option>
                                                                                                                <option>+675 Papua New Guinea</option>
                                                                                                                <option>+595 Paraguay</option>
                                                                                                                <option>+51 Peru</option>
                                                                                                                <option>+63 Philippines</option>
                                                                                                                <option>+0 Pitcairn</option>
                                                                                                                <option>+48 Poland</option>
                                                                                                                <option>+351 Portugal</option>
                                                                                                                <option>+1787 Puerto Rico</option>
                                                                                                                <option>+974 Qatar</option>
                                                                                                                <option>+40 Romania</option>
                                                                                                                <option>+70 Russian Federation</option>
                                                                                                                <option>+250 Rwanda</option>
                                                                                                                <option>+290 Saint Helena</option>
                                                                                                                <option>+1869 Saint Kitts And Nevis</option>
                                                                                                                <option>+1758 Saint Lucia</option>
                                                                                                                <option>+508 Saint Pierre And Miquelon</option>
                                                                                                                <option>+1784 Saint Vincent And The Grenadines</option>
                                                                                                                <option>+684 Samoa</option>
                                                                                                                <option>+378 San Marino</option>
                                                                                                                <option>+239 Sao Tome And Principe</option>
                                                                                                                <option>+221 Senegal</option>
                                                                                                                <option>+381 Serbia And Montenegro</option>
                                                                                                                <option>+248 Seychelles</option>
                                                                                                                <option>+232 Sierra Leone</option>
                                                                                                                <option>+421 Slovakia</option>
                                                                                                                <option>+386 Slovenia</option>
                                                                                                                <option>+677 Solomon Islands</option>
                                                                                                                <option>+252 Somalia</option>
                                                                                                                <option>+27 South Africa</option>
                                                                                                                <option>+0 South Georgia And The South Sandwich Isl</option>
                                                                                                                <option>+34 Spain</option>
                                                                                                                <option>+249 Sudan</option>
                                                                                                                <option>+597 Suriname</option>
                                                                                                                <option>+268 Swaziland</option>
                                                                                                                <option>+46 Sweden</option>
                                                                                                                <option>+963 Syrian Arab Republic</option>
                                                                                                                <option>+886 Taiwan, Province Of China</option>
                                                                                                                <option>+992 Tajikistan</option>
                                                                                                                <option>+255 Tanzania, United Republic Of</option>
                                                                                                                <option>+670 Timor-leste</option>
                                                                                                                <option>+228 Togo</option>
                                                                                                                <option>+690 Tokelau</option>
                                                                                                                <option>+676 Tonga</option>
                                                                                                                <option>+1868 Trinidad And Tobago</option>
                                                                                                                <option>+216 Tunisia</option>
                                                                                                                <option>+90 Turkey</option>
                                                                                                                <option>+7370 Turkmenistan</option>
                                                                                                                <option>+1649 Turks And Caicos Islands</option>
                                                                                                                <option>+688 Tuvalu</option>
                                                                                                                <option>+256 Uganda</option>
                                                                                                                <option>+380 Ukraine</option>
                                                                                                                <option>+598 Uruguay</option>
                                                                                                                <option>+998 Uzbekistan</option>
                                                                                                                <option>+678 Vanuatu</option>
                                                                                                                <option>+58 Venezuela</option>
                                                                                                                <option>+84 Viet Nam</option>
                                                                                                                <option>+1284 Virgin Islands, British</option>
                                                                                                                <option>+1340 Virgin Islands, U.s.</option>
                                                                                                                <option>+681 Wallis And Futuna</option>
                                                                                                                <option>+967 Yemen</option>
                                                                                                                <option>+260 Zambia</option>
                                                                                                                <option>+263 Zimbabwe</option>
                                                                                                                </select>
                                                        </div>

                                                        <div class="group">
                                                            <input class="mobile phone_numbereg" type="text" id="tsr_mobile" required>
                                                            <span class="highlight"></span>
                                                            <span class="bar"></span>
                                                            <label>Phone</label>
                                                            <span id="tsr-mobile-error"></span>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12 paddingLR5">
                                                        <div class="group">
                                                            <input type="password" id="tsr_password" 
                                                                   name="login[password]" required>
                                                            <span class="highlight"></span>
                                                            <span class="bar"></span>
                                                            <label>Password</label>
                                                            <span id="tsr-password-error"></span>
                                                            <div class="hide-show">
                                                                <small id="changeshowhide"> <span class="eye_icon" ></span>
                                                                </small>

                                                            </div>
                                                        </div>
                                                    </div>

                                              <!--       <div class="col-md-12 paddingLR5">
                                                        <div class="group">
                                                            <input type="password" id="confirmPassword" required
                                                                   name="login[password]">
                                                            <span class="highlight"></span>
                                                            <span class="bar"></span>
                                                            <label>Confirm Password</label>
                                                            <span id="tsr-password-errors" style="color: red"></span>
                                                            <div class="hide-show">
                                                                <small id="changeshowhide"><i class="eye_icon" aria-hidden="true"></i>
                                                                </small>

                                                            </div>
                                                        </div>
                                                    </div> -->

                                                    <div class="clearfix"></div>
                                                     <div class="spinner" style="display: none;">
                                                                <img src="https://staging.360realtors.com/360assets/images/loading.gif">
                                                            </div>
                                                    <input type="button" value="Submit"
                                                           onclick="submitUserRegistration();"/>


                                                    <!-- <div class="signUptrm"> By clicking Submit i agree with
                                                        <a href="disclaimer" target="_blank">Terms And conditions</a>
                                                    </div> -->

                                                </div>
                                            </form>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
      <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://staging.360realtors.com/360assets/nw_js/commercial/owl.carousel.min.js?1586161639"></script>
      <script src="https://staging.360realtors.com/360assets/nw_js/common_script.js?1586161639"></script>
      <script src="https://staging.360realtors.com/360assets/nw_js/enquiryForms.js?1586161639"></script>
      <script src="https://staging.360realtors.com/360assets/nw_js/common.js?1586161639"></script>
      <script src="https://staging.360realtors.com/360assets/js/xhome.js?1586161639"></script>
      <script src="https://staging.360realtors.com/360assets/js/tokeninput.js?1586161639"></script>
      <script src="https://staging.360realtors.com/360assets/nw_js/commercialcommonElasticSearch.js?1586161639" ></script>

      <script>
      var BASE_URL='https://staging.360realtors.com/';  
      var SEARCH_PATHDATA = "https://staging.360realtors.com/homesearch/Homesearch/commercialSearch";
      var cityId = "1";
      POPUP_ENQUIRY_AJAX_PATH = "https://staging.360realtors.com/common-Enquiryform-formPopupcommercial";
      LEAD_ENQUIRY_AJAX_PATH = "https://staging.360realtors.com/common/LeadCommercialEnquiryForm";
      $(document).ready(function(){

      function alignModal(){
      var modalDialog = $(this).find(".modal-dialog");
      /* Applying the top margin on modal dialog to align it vertically center */
      modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
      }
      // Align modal when it is displayed
      $(".modal").on("shown.bs.modal", alignModal);

      // Align modal when user resize the window
      $(window).on("resize", function(){
      $(".modal:visible").each(alignModal);
      });   
      });
      </script>
 <script type="text/javascript">
        $(".call-to-bt").click(function(){
            $("#footersticID .formfive").css({"display":"block"});
            $(".call-to-bt").css({"display":"none"});
        });

         $(".close-bt").click(function(){
            $("#footersticID .formfive").css({"display":"none"});
            $(".call-to-bt").css({"display":"block"});
        });

          /*$(".bottom-close-btn").click(function(){
            $("#footersticID").css({"visibility":"hidden"});
           
        });*/
    </script>
    <script type="text/javascript">
            $(document).ready(function(scrollheight){
                $(window).scroll(function(){
                    var commonfooterpostion= '';
                    if($(window).scrollTop() > commonfooterpostion){
                      $("#footersticID").css({"display":"block"});
                    }else{
                        $("#footersticID").css({"display":"none"});
                    }
                })
            });
    </script>
    <script type="text/javascript">
    function openForm() {
       document.getElementById("myForm").style.display = "block";
    }

    function closeForm() {
       document.getElementById("myForm").style.display = "none";
    }

    function olarksection(){
        document.getElementById("myForm").style.display = "none";
        olark('api.box.expand');
    } 
    </script>


      </body>
</html>
 
   
            <script type="text/javascript">
            var propertyName =  'Paras Downtown';    
            var propLat = 28.460644;
            var propLon = 77.095099;
            </script> 
            <script src="https://staging.360realtors.com/360assets/nw_js/mapsPropView.js?v1"></script>
            <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuVY4BpmVVrddSj_ktAJgIj101f6INEuU&callback=initMapDiv"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
           <!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.23/owl.carousel.min.js"></script>  -->
            <script>
                $(function() {
                    $(window).on("scroll", function() {
                        if($(window).scrollTop() >50) {
                            $(".spacication").addClass("spacication_fixed_top");
                            $(".inter_lead_form").addClass("inter_lead_form_top");
                        } else {
						//remove the background property so it comes transparent again (defined in your css)
						$(".spacication").removeClass("spacication_fixed_top");
						$(".inter_lead_form").removeClass("inter_lead_form_top");

						// $(".affix-top").css("top","0px");
						}
						});
                });
            </script> 
            
<script>        	
    $('#floor-plantwo').owlCarousel({
       loop:true,
       margin:10,
       items:1,
       responsiveClass:true,
       responsive:{
           0:{
               items:1,
               nav:true
           },
           768:{
               items:1,
               nav:true
           },
           1000:{
               items:1,
               nav:true,
               loop:false
           }
       }
    });
</script>
 <script src="https://staging.360realtors.com/360assets/nw_js/emiCal.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
        var interestAmount = showEmi('emiValue');
        $('#totalInterestToBePaid').text(interestAmount);
        });
    </script>

    <script type="text/javascript">
    function changeAmountLabel() {
        var amt = $('#ddn_type').val();
        $('#totalAmountLabel').text(amt);
    }
    changeAmountLabel();
    </script>

    <script type="text/javascript">
    $('#ddn_type, #emiTerm').change(function() {
        var ints = showEmi('emiValue');
        $('#totalInterestToBePaid').text(ints);
        changeAmountLabel();
    });
    </script>
    <script>
    $('input[name=down_payment], input[name=interest]').keyup(function() {
        var intrs = showEmi('emiValue');
        $('#totalInterestToBePaid').text(intrs);

    })
    </script>
    <script type="text/javascript">
    $(document).ready(function() {

    var moreDiv = document.getElementById('showtable');
    if ($('#showtable tr').length > 9) { 
        console.log('true'); 
        moreDiv.style.height = '377px'
        }
        else{
            console.log('false');
            moreDiv.style.height = 'auto'
        }

    
    });

</script>
<script>
    $(document).ready(function () {
        $(document).on("scroll", onScroll);
        $('a[href^="#"]').click(function(){
            var target = $(this).attr('href');
            $('html, body').animate({scrollTop: $(target).offset().top - 130}, 500);
            return false;
            $(".spacicationmenu a").click(function () {
                $(".spacicationmenu a").removeClass('active');
            })
            $(this).addClass('active');
            var target = this.hash,
                menu = target;
            $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $(target).offset().top - 130 }, 500, 'swing', function () {
                window.location.hash = target;
                $(document).on("scroll", onScroll);
            });
        });
    });
    function onScroll(event){
        var scrollPos = $(document).scrollTop();
        $('.spacicationmenu a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top - 200 <= scrollPos && refElement.position().top - 200 + refElement.height() > scrollPos) {
               $('.spacicationmenu ul li a').removeClass("active");
                currLink.addClass("active");
            }
            else{
                currLink.removeClass("active");
            }
        });
    }
</script>

<?php 
function no_to_words($no)
{
	
if($no == 0) {
return ' ';
}else {
$n = strlen($no); // 7
}

switch ($n) {
case 6:
$val = $no/100000;
$val = round($val, 2);
$finalval = $val ." Lac";
break;
case 7:
$val = $no/100000;
$val = round($val, 2);
$finalval = $val ." Lac";
break;
case 8:
$val = $no/10000000;
$val = round($val, 2);
$finalval = $val ." Cr";
break;
case 9:
$val = $no/10000000;
$val = round($val, 2);
$finalval = $val ." Cr";
break;

default:
$finalval= "";
}
return $finalval;

}
?>