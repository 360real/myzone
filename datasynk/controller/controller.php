<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "datasynk/commonvariable.php");
include_once(LIBRARY_PATH . "datasynk/model/model.php");

$modelObj = new Model();

admin_login();
if(!in_array($_SESSION['login']['role_id'],array(1,23))){
  $obj_common->redirect('/index.php');
}

include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('datasynk Managemant', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/datasynk/controller/controller.php";

switch ($action) {
    case 'test':
    $propid = 13;
    $crmurl = 'https://timesproperty.360realtors.com/property/property/testapi/'.$propid;
    $chcrm = curl_init($crmurl);
    //$payload = json_encode($arrPricesTimes);
    curl_setopt($chcrm, CURLOPT_POSTFIELDS);
    curl_setopt($chcrm, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($chcrm, CURLOPT_RETURNTRANSFER, true);
    $resultcrm = curl_exec($chcrm);
    print_r($resultcrm);die;

    break;
    case 'adddatasynk':
    include(ROOT_PATH . "/datasynk/view/adddatasynk.php");
    break;
    case 'adddatasynk2':
    $question=$modelObj->listdata($_POST['question']);
    
    for ($i=0; $i <count($question) ; $i++) { 

      $question[$i]['real_id']=$question[$i]['id'];
      $rera=$modelObj->reralistdata($question[$i]['id']);
      $rera_no='';
      if(!empty($rera)){
        foreach ($rera as $value) {
          $rera_no[]=$value['rera_no'];
        }
       
       // print_r($question[$i]['rera_no']);
      }
      
      $datefn = explode(',', $question[$i]['possession_date']);
      $question[$i]['pdMonth'] = $datefn[1];
      $question[$i]['pdYear'] = $datefn[0];
      $datefnlaunch_date = explode(',', $question[$i]['launch_date']);
      $question[$i]['ldMonth'] = $datefnlaunch_date[1];
      $question[$i]['ldYear'] = $datefnlaunch_date[0];

      $question[$i]['rera_no']=$rera_no;   
      $crmurl = 'https://timespropertymyzone.360realtors.com/newproject/controller/propertyinfoApi.php';
      $chcrm = curl_init($crmurl);
      $payload = json_encode($question[$i]);
      curl_setopt($chcrm, CURLOPT_POSTFIELDS, $payload);
      curl_setopt($chcrm, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
      curl_setopt($chcrm, CURLOPT_RETURNTRANSFER, true);
      $resultcrm = curl_exec($chcrm);

      $imagelistdata=$modelObj->imagelistdata($question[$i]['id']);
      $base64='';
      $imagetype='';
      $type='';
      for ($j=0; $j <count($imagelistdata) ; $j++) { 

        if($imagelistdata[$j]['img_type']=='MINI_PROPERTY_IMAGE'){
          $imagetype='mini';
        }
        if($imagelistdata[$j]['img_type']=='CONSTRUCTION_IMAGE'){
          $imagetype='construction';
        }
        if($imagelistdata[$j]['img_type']=='MASTERPLAN_IMAGE'){
          $imagetype='masterplan';
        }
        if($imagelistdata[$j]['img_type']=='LOCATIONMAP_IMAGE'){
          $imagetype='locationmap';
        }
        if($imagelistdata[$j]['img_type']=='SITEPLAN_IMAGE'){
          $imagetype='siteplan';
        }
        if($imagelistdata[$j]['img_type']=='PAYMENTPLAN_IMAGE'){
          $imagetype='paymentplan';
        }
        if($imagelistdata[$j]['img_type']=='SPECIFICATION_IMAGE'){
          $imagetype='specification';
        }
        if($imagelistdata[$j]['img_type']=='BOOKING_PROCEDURE_IMAGE'){
          $imagetype='bookingprocedure';
        }

     /*   $imagedata = file_get_contents('https://static.360realtors.ws/properties/photos/'.$question[$i]['id'].'/'.$imagetype.'/'.$imagelistdata[$j]['image']);
        echo $base64 = base64_encode($imagedata);*/

         $path= 'https://static.360realtors.ws/properties/photos/'.$question[$i]['id'].'/'.$imagetype.'/'.$imagelistdata[$j]['image'];
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $dataload[$j]["img_type"]   = $imagelistdata[$j]['img_type']; 
        $dataload[$j]["propty_id"]  = $question[$i]['id'];  
        $dataload[$j]["prop_name"]  = $question[$i]['property_name']; 
        $dataload[$j]["imageId"]    = $imagelistdata[$j]['id'];
        $dataload[$j]["alt_title"]  = $imagelistdata[$j]['alt_title'];
        $dataload[$j]["image"]      = $base64;
        $dataload[$j]["type"]      = $type;
        $crmurl                 = 'https://timespropertymyzone.360realtors.com/image/controller/propertyinfoApi.php';
        $chcrm                  = curl_init($crmurl);
        $payload                = json_encode($dataload[$j]);
        curl_setopt($chcrm, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chcrm, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($chcrm, CURLOPT_RETURNTRANSFER, true);
        $resultcrm = curl_exec($chcrm);
      }

      $pricelistdata=$modelObj->pricelistdata($question[$i]['id']);

      $pricepath='';
      $pricetype='';
      $pricedata='';
      $pricebase64='';

      for ($k=0; $k <count($pricelistdata) ; $k++) { 

          $pricepath= 'https://static.360realtors.ws/properties/photos/'.$question[$i]['id'].'/floorplans/'.$pricelistdata[$k]['floor_plan'];
          $pricetype = pathinfo($path, PATHINFO_EXTENSION);
          $pricedata = file_get_contents($path);
          $pricebase64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

          $arrPricesTimes[$k]["real_id"] = $question[$k]['id'];
          $arrPricesTimes[$k]["priceId"] = $pricelistdata[$k]["priceId"];
          $arrPricesTimes[$k]["type"] = $pricelistdata[$k]["type"];
          $arrPricesTimes[$k]["size"] = $pricelistdata[$k]["size"];
          $arrPricesTimes[$k]["price"] = $pricelistdata[$k]["price"];
          $arrPricesTimes[$k]["amount"] = $pricelistdata[$k]["amount"];
          $arrPricesTimes[$k]["booking_amount"] = $pricelistdata[$k]["booking_amount"];
          $arrPricesTimes[$k]["property_booking_status"] = $pricelistdata[$k]["property_booking_status"];
          $arrPricesTimes[$k]["status"] = $pricelistdata[$k]["typestatus"];
          $arrPricesTimes[$k]["imagetype"]=$pricetype;
          $arrPricesTimes[$k]["floor_plan"]=$pricebase64;
          $crmurl = 'https://timespropertymyzone.360realtors.com/price/controller/propertyinfoApi.php';
          $chcrm = curl_init($crmurl);
          $payload = json_encode($arrPricesTimes[$k]);
          curl_setopt($chcrm, CURLOPT_POSTFIELDS, $payload);
          curl_setopt($chcrm, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
          curl_setopt($chcrm, CURLOPT_RETURNTRANSFER, true);
          $resultcrm = curl_exec($chcrm);
      }

    }
die;
    include(ROOT_PATH . "/datasynk/view/adddatasynk.php");
    break;

}

?>
