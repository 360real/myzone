<?php

date_default_timezone_set('Asia/Kolkata');
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
	include_once (LIBRARY_PATH."library/server_config_admin.php");
	require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
	admin_login();
	$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
	$tblName	=	TABLE_INTERNAL_NEWS;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/weekly_news/controller/controller.php';
	$clsRow1	=	'clsRow1';
	$clsRow2	=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform task like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	


	/* choices to where page is redirect
		if action=create then goes to v
		iew  	
			action =update then model
			action default to view i.e index.php
	*/

	$file_name = ROOT_PATH."/weekly_news/view/view.php" ;
	
	/***********************************************************************************/
			//Code  for search a record from database on click submit  button

	/***********************************************************************************/
	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
		if($_GET["action"]=="search")
		{
			  // $sql = "SELECT NEW.* FROM ".TABLE_INTERNAL_NEWS." NEW LEFT JOIN ".TABLE_NEWS_TAGS_ID." as TAGID ON NEW.id=TAGID.news_id LEFT JOIN ".TABLE_NEWS_TAGS." as TAGS ON TAGS.id=TAGID.tags_id WHERE ";

			$sql = "SELECT *
					FROM (
					(

					SELECT id, title,created_date,modified_date,status, 'news' AS
					type FROM `tsr_web_news`
					)
					UNION (

					SELECT id, title ,created_date,modified_date,status, 'blog' AS
					type FROM `tsr_blog`
					)
					UNION (

					SELECT id, title,created_date,modified_date,status, 'research' AS
					type FROM tsr_research
					)
					) AS t where ";

			// if($_GET["user_id"]!="")
			// 	$sql.="NEW.id=".$_GET["user_id"];



			if($_GET["status"]!="")
			{   if(($_GET["status"]==1)||($_GET["status"]==0))
				$sql.="t.status=".$_GET["status"];
				else
				$sql.="t.status in (0,1)";
				// $sql.=" (t.status=1 OR t.status=0) ";
			}
			else
			{
				$sql.="t.status in (0,1)";
			}
			
			if($_GET["title"]!="")
				$sql.=" and t.title like '%".trim($_GET["title"])."%'";
			
			if($_GET["startDate"]!="" && $_GET["endDate"]!="")
			{
				$sql.=" and date(t.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(t.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
			}
			else
			if($_GET["startDate"]!="")
			{
				$sql.=" and date(t.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
			}
			else
			if($_GET["endDate"]!="")
			{
				$sql.=" and date(t.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
			}
		}


		$s=($_GET['s'] ? 'ASC' : 'DESC');
		$sort=($_GET['s'] ? 0 : 1 );
			$f=$_GET['f'];
		// if($s && $f)
		// $sql.= " GROUP BY t.id ORDER BY $f  $s";
		// else
		// $sql.= " GROUP BY t.id ORDER BY t.created_date DESC";
		if($s && $f)
		$sql.= " ORDER BY $f  $s";
		else
		$sql.= " ORDER BY t.created_date DESC";
	}

	else
	{
		// $sql = "SELECT NEW.*, 'news' as list_type FROM ".TABLE_INTERNAL_NEWS." NEW order by NEW.datetimestamp DESC , NEW.mail_order ASC";

		$sql = "SELECT *
					FROM (
					(

					SELECT id, title,created_date,modified_date,status, 'news' AS
					type FROM `tsr_web_news`
					)
					UNION (

					SELECT id, title ,created_date,modified_date,status , 'blog' AS
					type FROM `tsr_blog`
					)
					UNION (

					SELECT id, title,created_date,modified_date,status, 'research' AS
					type FROM tsr_research
					)
					) AS t";
	}


	/*---------------------paging script start----------------------------------------*/
		$obj_paging->limit=10;
		if($_GET['page_no'])
			$page_no=remRegFx($_GET['page_no']);
			else
			$page_no=0;
		$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
		$str_pos=strpos($queryStr,'page_no');
		if($str_pos>0)
			$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
		$obj_paging->set_lower_upper($page_no);
		$total_num=$obj_mysql->get_num_rows($sql);
		$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold'); 
		$total_rec=$obj_paging->total_records($total_num);
		$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";

	/*---------------------paging script end----------------------------------------*/
	    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));

	/************************************************************************************************/
			// code for search and pagination ends here

	/************************************************************************************************/	

	
	$list="";
	for($i=0;$i< count($rec);$i++)
	{
		if($rec[$i]['status']=="1")
		{
			$st='<font color=green>Active</font>';
		}
		else
		{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='
		<tr class="'.$clsRow.'">
			<td class="center"><input type="checkbox" name="location[]" value="'.$rec[$i]['id'].'|'.$rec[$i]['type'].'"></td>
			<td class="center">'.$rec[$i]['title'].'</td>
			<td class="center">'.$rec[$i]['type'].'</td>
			<td class="center">'.$st.'</td>
			<td class="center">'.$rec[$i]['created_date'].'</td>
			<td class="center">'.$rec[$i]['modified_date'].'</td>
		</tr>';
	}

	if($list=="")
	{
		$list="<tr><td colspan=10 align='center' height=50>No Record(s) Found.</td></tr>";
	}
?>

<!--************************************** code to include common header ***************************************/-->
<?php include(LIBRARY_PATH."includes/header.php");
	$commonHead = new CommonHead();
	$commonHead->commonHeader('Weekly News Manager', $site['TITLE'], $site['URL']); 
?>
<!--************************************** End of code to include header ***************************************/-->
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
	<tr>
	  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr>
					<td>
						<?php 
						if($_GET['msg']!="")
						{
							echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
						}
						if($msg!="")
						{
							echo('<div class="notice">'.$message_arr[$msg].'</div>');
						}
						?>
					</td>
		        </tr>
		    </table>
	    </td>
	</tr>

	<tr>
	  <td colspan="2" valign="top"><?php include($file_name	);?></td> <!-- include file_name to get the page on controller page -->
	</tr>

	<tr>
	  <td colspan="2" valign="top"></td>
	</tr>
</table>
<!-- Right Ends -->


<?php include(LIBRARY_PATH."includes/footer.php");?>
