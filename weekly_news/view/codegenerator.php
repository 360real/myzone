<?php

include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");

include_once (LIBRARY_PATH."library/server_config_admin.php");
require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
admin_login();

$vals=mysql_real_escape_string($_POST['vals']);
$news_id = "";
$blog_id = "";
$research_id = "";
$id_array = explode(",",$vals);

foreach ($id_array as $value) {
  $id="";
  $type="";
  $temp_id = explode("|",$value);
  $id = $temp_id[0];
  $type = $temp_id[1];
    if($type=="news")
    {
      $news_id.= $id.",";
    }
    else if($type=="blog")
    {
      $blog_id.= $id.",";
    }
    else if($type=="research")
    {
      $research_id.= $id.",";
    }
}

if($news_id!="")
$news_id = rtrim($news_id, ",");

if($blog_id!="")
$blog_id = rtrim($blog_id, ",");

if($research_id!="")
$research_id = rtrim($research_id, ",");

$msg="";


$msg.='<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title></title>
      <style type="text/css">
          @import url("https://fonts.googleapis.com/css2?family=Jost&display=swap");
 
         @font-face {
         font-family: \'Gotham-Ultra\';
         src: url("'.C_ROOT_URL.'weekly_news/view/fonts/Gotham-Ultra.otf");
    }
     @font-face {
         font-family: \'gotham-black\';
         src: url("'.C_ROOT_URL.'weekly_news/view/fonts/GOTHAM-BLACK.TTF");
    }
    @font-face {
         font-family: \'Gotham-Medium\';
         src: url("'.C_ROOT_URL.'weekly_news/view/fonts/Gotham-Medium.otf");
    }
    @font-face {
         font-family: \'Gotham-Bold\';
         src: url("'.C_ROOT_URL.'weekly_news/view/fonts/Gotham-Bold.otf");
    }
 
  
         .bottoms1 {
         width:calc(33.3% - 20px);
         -webkit-width:calc(33.3% - 20px);
         -ms-width:calc(33.3% - 20px);
         -moz-width:calc(33.3% - 20px);
         float:left;
         font-size:60% !important
         }
         #outlook a {
         padding:0;
         }
         body {
         width:100% !important;
         -webkit-text-size-adjust:100%;
         -ms-text-size-adjust:100%;
         margin:0;
         padding:0;
         color:#38373f;
         }
         .ExternalClass {
         width:100%;
         }
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
         line-height: 100%;
         }
         #backgroundTable {
         margin:0;
         padding:0;
         width:100% !important;
         line-height: 100% !important;
         }
         img {
         outline:none;
         text-decoration:none;
         border:none;
         -ms-interpolation-mode: bicubic;
         }
         a img {
         border:none;
         }
         .image_fix {
         display:block;
         }
         p {
         margin: 0px 0px !important;
         }
         table td {
         border-collapse: collapse;
         }
         table {
         border-collapse:collapse;
         mso-table-lspace:0pt;
         mso-table-rspace:0pt;
         }
         table[class=full] {
         width: 100%;
         clear: both;
         }
         .hide { height:25px !important;}         
         /*IPAD STYLES*/
         @media only screen and (max-width: 640px) {
         .hide { height:0px !important;}  
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #ffffff; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #ffffff !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {
         width: 440px!important;
         text-align:center!important;
         }
         table[class=devicewidthinner] {
         width: 420px!important;
         text-align:center!important;
         }
         table[class="sthide"] {
         }
         display: none!important;
         }
         img[class="bigimage"] {
         width: 420px!important;
         height:297px!important;
         }
         img[class="col2img"] {
         width: 420px!important;
         height:258px!important;
         }
         img[class="image-banner"] {
         width: 440px!important;
         height:106px!important;
         }
         td[class="menu"] {
         text-align:center !important;
         padding: 0 0 10px 0 !important;
         }
         td[class="logo"] {
         padding:10px 0 5px 0!important;
         margin: 0 auto !important;
         }
         img[class="logo"] {
         padding:0!important;
         margin: 0 auto !important;
         }
         .ttle_main {
         font-size:25px;
         line-height:35px;
         }
         .bottoms1 {
         width:calc(100% - 20px) !important;
         }
         }
         /*IPHONE STYLES*/
         @media only screen and (max-width: 480px) {
         a[href^="tel"], a[href^="sms"] {
         text-decoration: none;
         color: #ffffff; /* or whatever your want */
         pointer-events: none;
         cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
         text-decoration: default;
         color: #ffffff !important;
         pointer-events: auto;
         cursor: default;
         }
         table[class=devicewidth] {
         width: 280px!important;
         text-align:center!important;
         }
         table[class=devicewidthinner] {
         width: 260px!important;
         text-align:center!important;
         }
         table[class="sthide"] {
         display: none!important;
         }
         img[class="bigimage"] {
         width: 260px!important;
         height:184px!important;
         }
         img[class="col2img"] {
         width: 260px!important;
         height:146px!important;
         }
         img[class="image-banner"] {
         width: 280px!important;
         height:68px!important;
         }
         .ttle_main {
         font-size:25px !important;
         line-height:35px !important;
         }
         }
         .bottoms1 {
         width:calc(33.3% - 20px);
         -webkit-width:calc(33.3% - 20px);
         -ms-width:calc(33.3% - 20px);
         -moz-width:calc(33.3% - 20px);
         float:left;
         font-size:60% !important
         }
      </style>
   </head>
   <body>
      <div class="block"> </div>
      <div class="block">
         <table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
            <tbody>
               <tr>
                  <td> </td>
               </tr>
            </tbody>
         </table>
      </div>
      <div class="block">
         <table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage" class="devicewidth">
            <tbody>
               <tr>
                  <td>
                     <table bgcolor="#a04682" width="584" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                        <tbody>
                           <tr>
                              <td>
                                 <table width="584" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner" >


                                   
                                    <tbody>
                                       <tr>
                                          <td align="center"> <img width="584" height="400" src="'.C_ROOT_URL.'weekly_news/view/images/top-banner1.png"> </td>
                                       </tr>';

//print_r($rec);

if($research_id!="")
{

$sql1="SELECT * from tsr_research where id IN(".$research_id.")";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));


foreach($rec as $newsData)
{
  $title1=$newsData["Title"];
  $title = str_replace("â€™", "'", $title1);
  $newsId=$newsData["id"];
  $description=$newsData["description"];
  $description = str_replace("â€™", "'", $description);
  $image=$newsData["image_path"];

  $pdf_file_path=$newsData["pdf_file_path"];

  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $title);
  $name = trim($name);
  $newsName = str_replace("--","-",str_replace(" ","-",$name));
  $newsName=preg_replace('/-+/', '-', strtolower($newsName)); 

  $newsTitledot = "";
  $newtitle_count = strlen(strip_tags($title));
  if($newtitle_count >70) {
    $newsTitledot = "...";
  } 
  $newsDescriptiondot = "";
  $newsDescription_count = strlen(strip_tags($description));
  if($newsDescription_count >120) {
    $newsDescriptiondot = "...";
  }

 $realUrl= "https://static.360realtors.ws/Research/PDF/" .$newsId . "/".$pdf_file_path;

 $newsURL = $realUrl;

$msg.='
<!-- start here -->
  <tr>
            <td>
               <table width="560" align="center" cellspacing="0" cellpadding="10" border="0" class="devicewidthinner" bgcolor="#fff" style="background: #fff">
                
                <tr bgcolor="#fff" style="background: #fff">
                  <td><a href="'.$newsURL.'" style="text-decoration:none;"><img src="https://static.360realtors.ws/Research/IMAGES/'.$newsId.'/'.$image.'" height="174" width="220"></a></td>
                  <td><a href="'.$newsURL.'" style="text-decoration:none;"><p style="font-size: 15px; font-family: \'Jost\', sans-serif; color: #7C3471; font-weight:900;margin: 0;"><b>Reports/Magazines/Whitepapers</b></p></a>
                      <a href="'.$newsURL.'" style="text-decoration:none;"><h3 style="
font-family: \'Jost\', sans-serif;
font-size: 20px;
color: #181313;
font-weight: normal;margin: 5px 0 12px; font-weight:900;">'.substr(strip_tags($title),0,70).$newsTitledot.'</h3></a>
                      <a href="'.$newsURL.'"><img width="120" src="'.C_ROOT_URL.'weekly_news/view/images/btn.png"></a>
                    </td>
                </tr>
                 
 
               </table>
 
 
            </td>
           </tr>

            <tr style="height: 10px">
              <td align="center"> </td>
           </tr>
<!-- end  here -->';

}


}



if($news_id!="")
{

$sql1="SELECT * from tsr_web_news where id IN(".$news_id.")";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));


foreach($rec as $newsData)
{
  $title1=$newsData["title"];
  $title = str_replace("â€™", "'", $title1);
  $newsId=$newsData["id"];
  $description=$newsData["overview"];
  $description = str_replace("â€™", "'", $description);
  // $image=$newsData["imgname"];

  $sqlImages = "SELECT * FROM ".TABLE_IMAGES." WHERE img_type='NEWS_IMAGE' and propty_id=".$newsData['id'];
  $recImages=($obj_mysql->getAllData($sqlImages));
  $image=$recImages[0]['image'];



  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $title);
  $name = trim($name);
  $newsName = str_replace("--","-",str_replace(" ","-",$name));
  $newsName=preg_replace('/-+/', '-', strtolower($newsName)); 

  $newsTitledot = "";
  $newtitle_count = strlen(strip_tags($title));
  if($newtitle_count >70) {
    $newsTitledot = "...";
  } 
  $newsDescriptiondot = "";
  $newsDescription_count = strlen(strip_tags($description));
  if($newsDescription_count >120) {
    $newsDescriptiondot = "...";
  }

 $realUrl= "https://360realtors.com/news/" . $newsName . "-nid" . $newsId;

 $newsURL = "https://360realtors.com/realtyrounduptrack/mailClick?url=".$realUrl."&newsId=".$newsId."&mailerId=".$mailerId;


  $msg.='
<!-- start here -->
  <tr>
            <td>
               <table width="560" align="center" cellspacing="0" cellpadding="10" border="0" class="devicewidthinner" bgcolor="#fff" style="background: #fff">
                
                <tr bgcolor="#fff" style="background: #fff">
                  <td><a href="'.$newsURL.'" style="text-decoration:none;"><img src="https://static.360realtors.ws/news/'.$newsId.'/'.$image.'" height="174" width="220"></a></td>
                  <td><a href="'.$newsURL.'" style="text-decoration:none;"><p style="font-size: 15px; font-family: \'Jost\', sans-serif; color: #7C3471; font-weight:900;margin: 0;"><b>360 Realtors in Media</b></p></a>
                      <a href="'.$newsURL.'" style="text-decoration:none;"><h3 style="
font-family: \'Jost\', sans-serif;
font-size: 20px;
color: #181313;
font-weight: normal;margin: 5px 0 12px; font-weight:900;">'.substr(strip_tags($title),0,70).$newsTitledot.'</h3></a>
                      <a href="'.$newsURL.'"><img width="120" src="'.C_ROOT_URL.'weekly_news/view/images/btn.png"></a>
                    </td>
                </tr>
                 
 
               </table>
 
 
            </td>
           </tr>

            <tr style="height: 10px">
              <td align="center"> </td>
           </tr>
<!-- end  here -->';

}

}


if($blog_id!="")
{

$sql1="SELECT * from tsr_blog where id IN(".$blog_id.")  ";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));


foreach($rec as $newsData)
{
  $title1=$newsData["title"];
  $title = str_replace("â€™", "'", $title1);
  $newsId=$newsData["id"];
  $description=$newsData["content"];
  $description = str_replace("â€™", "'", $description);
  $image=$newsData["imgname"];
  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $title);
  $name = trim($name);
  $newsName = str_replace("--","-",str_replace(" ","-",$name));
  $newsName=preg_replace('/-+/', '-', strtolower($newsName)); 

  $newsTitledot = "";
  $newtitle_count = strlen(strip_tags($title));
  if($newtitle_count >70) {
    $newsTitledot = "...";
  } 
  $newsDescriptiondot = "";
  $newsDescription_count = strlen(strip_tags($description));
  if($newsDescription_count >120) {
    $newsDescriptiondot = "...";
  }

 $realUrl= "https://360realtors.com/blog/post/" . $newsName . "-blid" . $newsId;

 $newsURL = "https://360realtors.com/realtyrounduptrack/mailClick?url=".$realUrl."&newsId=".$newsId;

$msg.='
<!-- start here -->
  <tr>
            <td>
               <table width="560" align="center" cellspacing="0" cellpadding="10" border="0" class="devicewidthinner" bgcolor="#fff" style="background: #fff">
                
                <tr bgcolor="#fff" style="background: #fff">
                  <td><a href="'.$newsURL.'" style="text-decoration:none;"><img src="https://static.360realtors.ws/blog/'.$newsId.'/'.$image.'" height="174" width="220"></a></td>
                  <td><a href="'.$newsURL.'" style="text-decoration:none;"><p style="font-size: 15px; font-family: \'Jost\', sans-serif; color: #7C3471; font-weight:900;margin: 0;"><b>Blog of the Week</b></p></a>
                      <a href="'.$newsURL.'" style="text-decoration:none;"><h3 style="
font-family: \'Jost\', sans-serif;
font-size: 20px;
color: #181313;
font-weight: normal;margin: 5px 0 12px; font-weight:900;">'.substr(strip_tags($title),0,70).$newsTitledot.'</h3></a>
                      <a href="'.$newsURL.'"><img width="120" src="'.C_ROOT_URL.'weekly_news/view/images/btn.png"></a>
                    </td>
                </tr>
                 
 
               </table>
 
 
            </td>
           </tr>

            <tr style="height: 10px">
              <td align="center"> </td>
           </tr>
<!-- end  here -->';

}

}





  $msg.='
   </tbody>
                                 </table>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr>
               <tr>
                <td>
                  <table bgcolor="#282828" width="584" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                    <tr>
                                          <td align="center">&nbsp;</td>
                                       </tr>
                                       <tr>
                                          <td align="center">&nbsp;</td>
                                       </tr>
                     <tr>
                      <td align="center">
                        <strong style="font-size: 23px;color: #fff;font-family: \'Jost\', sans-serif;">Get in Touch</strong>
                      </td>

                     </tr>
                     <tr>
                                          <td align="center">&nbsp;</td>
                                       </tr>
                    <tr>
                      <td style="text-align:center">
                        <a href="https://www.facebook.com/360Realtors" target="_blank"><img width="30" src="'.C_ROOT_URL.'weekly_news/view/images/facebook.png"></a>
                        <a href="https://twitter.com/360realtors" target="_blank"><img width="30" src="'.C_ROOT_URL.'weekly_news/view/images/twitter.png"></a>
                        <a href="https://in.pinterest.com/360Realtors/" target="_blank"><img width="30" src="'.C_ROOT_URL.'weekly_news/view/images/pin.png"></a>
                        <a href="https://www.instagram.com/360realtors" target="_blank"><img width="30" src="'.C_ROOT_URL.'weekly_news/view/images/insta.png"></a>


                       </td>
                    </tr>
                    <tr>
                                          <td align="center">&nbsp;</td>
                                       </tr>
                                       <tr>
                      <td align="center"><p style="font-size: 12px;
    color: #fff;font-family: \'Jost\', sans-serif;
    ">
                        Address : 201A-201C, Global Foyer, Golf Course Road, Sector 43,<br/>
 Gurugram, Haryana 122002<br/>
Phone : 1800 1200 360<br/>
Email : weeklydigest@360realtors.com, www.360realtors.com</p>
                      </td>


                     </tr>
                      <tr>
                                          <td align="center">&nbsp;</td>
                                       </tr>
                     <tr>
                                          <td align="center">
                                            <table  width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                                                <tr>
                                          <td style="border-top: 3.5px solid #7C3471" align="center"></td>
                                       </tr>
                                            </table>
                                          </td>
                                       </tr>
                                        <tr>
                                          <td align="center">&nbsp;</td>
                                       </tr>
                                       <tr>
                      <td align="center"><p style="font-size: 12px;
    color: #fff;font-family: \'Jost\', sans-serif;
    "> Copyright 2020 - All Rights Reserved
                       </p>
                      </td>


                     </tr>
                     <tr>
                                          <td align="center">&nbsp;</td>
                                       </tr>

                  </table>

                </td>







                
              </tr>
            </tbody>
         </table>
      </div>
      <div class="block">
         <table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
            <tbody>
               <tr>
                  <td>&nbsp;</td>
               </tr>
            </tbody>
         </table>
      </div>
   </body>
</html>';

echo $msg;






?>



