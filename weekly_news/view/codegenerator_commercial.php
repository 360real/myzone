<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
admin_login();
$vals=mysql_real_escape_string($_POST['vals']);
$news_id = "";
$blog_id = "";
$research_id = "";
$id_array = explode(",",$vals);
foreach ($id_array as $value) {
  $id="";
  $type="";
  $temp_id = explode("|",$value);
  $id = $temp_id[0];
  $type = $temp_id[1];
    if($type=="news")
    {
      $news_id.= $id.",";
    }
    else if($type=="blog")
    {
      $blog_id.= $id.",";
    }
    else if($type=="research")
    {
      $research_id.= $id.",";
    }
}
if($news_id!="")
$news_id = rtrim($news_id, ",");

if($blog_id!="")
$blog_id = rtrim($blog_id, ",");

if($research_id!="")
$research_id = rtrim($research_id, ",");
$msg="";

$msg.='
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <style type="text/css">
    @font-face {
      font-family: \'Raleway\';
      src: url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-ExtraBold.woff2") format("woff2"),
      url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-ExtraBold.woff") format("woff");
      font-weight: 800;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: \'Raleway\';
      src: url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-SemiBold.woff2") format("woff2"),
      url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-SemiBold.woff") format("woff");
      font-weight: 600;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: \'Raleway\';
      src: url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-Bold.woff2") format("woff2"),
      url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-Bold.woff") format("woff");
      font-weight: bold;
      font-style: normal;
      font-display: swap;
    }

    @font-face {
      font-family: \'Raleway\';
      src: url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-Medium.woff2") format("woff2"),
      url("'.C_ROOT_URL.'weekly_news/view/comm_fonts/Raleway-Medium.woff") format("woff");
      font-weight: 500;
      font-style: normal;
      font-display: swap;
    }
</style>
</head>
<body>
  <div class="block">
    <table width="100%" bgcolor="#fff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage" class="devicewidth">
      <tbody>
        <tr>
          <td>
            <table width="584" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit" style="background: url('.C_ROOT_URL.'weekly_news/view/comm_images/pattern.png) no-repeat;background-size: cover; border:solid 1px #9a2690;">
              <tbody>
                <tr>
                  <td>
                    <table width="584" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner" style="background: url('.C_ROOT_URL.'weekly_news/view/comm_images/top-banner.png) no-repeat;background-size: contain;">
                      <tbody>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="right" style="padding-right: 30px;"><a target="_blank" href="#"><img src="'.C_ROOT_URL.'weekly_news/view/comm_images/logo.png" width="140"></a></td>
                        </tr>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="100%" height="100" align="center"></td>
                        </tr>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="left" style="padding: 35px 20px;">
                            <h2 style="font-size: 14px;font-family: Raleway; font-weight: bold;color: #000000;">Dear Readers,</h2>
                            <p style="font-size: 14px;font-family: Raleway; font-weight: 500;color: #000000;">Welcome to 360 Commercial Realty Newsletter. Here, we bring to you actionable and in-depth blogs, expert views, whitepapers and research reports on various markets and sub-sectors of CRE.</p>
                          </td>
                        </tr>';

//print_r($rec);
$pp=0;
if($research_id!="")
{

$sql1="SELECT * from tsr_research where id IN(".$research_id.")";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

$count_row = count($rec);
$ip=0;
foreach($rec as $newsData)
{
  $ip++;
  $title1=$newsData["Title"];
  $title = str_replace("â€™", "'", $title1);
  $newsId=$newsData["id"];
  $description=$newsData["description"];
  $description = str_replace("â€™", "'", $description);
  $image=$newsData["image_path"];

  $pdf_file_path=$newsData["pdf_file_path"];

  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $title);
  $name = trim($name);
  $newsName = str_replace("--","-",str_replace(" ","-",$name));
  $newsName=preg_replace('/-+/', '-', strtolower($newsName)); 

  $newsTitledot = "";
  $newtitle_count = strlen(strip_tags($title));
  if($newtitle_count >70) {
    $newsTitledot = "...";
  } 
  $newsDescriptiondot = "";
  $newsDescription_count = strlen(strip_tags($description));
  if($newsDescription_count >120) {
    $newsDescriptiondot = "...";
  }

 $realUrl= "https://static.360realtors.ws/Research/PDF/" .$newsId . "/".$pdf_file_path;

 $newsURL = $realUrl;


$msg.='
<!-- start here -->
<tr>
  <td>
    <table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
      <tr>
        <td><a href="'.$newsURL.'" target="_blank"  style="text-decoration:none;"><img src="https://static.360realtors.ws/Research/IMAGES/'.$newsId.'/'.$image.'" width="220"  style="height: 160px;"></a></td>
        <td style="padding:0 15px" valign="top"><a href="'.$newsURL.'" target="_blank"  style="text-decoration:none;"><p style="margin-top:0;font-size: 18px;font-family: Raleway;font-weight: 800;  color: #A44785;">'.substr(strip_tags($title),0,70).$newsTitledot.'</p></a>
          <h3 style="font-family: Raleway;font-size: 14px;color: #333232;font-weight: bold;margin: 5px 0 12px;">'.substr(strip_tags($description),0,70).$newsDescriptiondot.'</h3>
          <a href="'.$newsURL.'" target="_blank" style="background: linear-gradient(45deg, #9a2690, #5e3062);color: #ffffff;font-weight: 500;display: inline-block;height:37px;line-height:37px;padding: 0px 25px;text-decoration: none;text-transform: uppercase;font-size: 15px;font-family: Raleway;" href="">Read More</a>
        </td>
      </tr>
    </table>
  </td>
</tr>';

if($ip!=$count_row){
  $msg.='<tr><td style="padding: 15px 20px;" align="center"><hr></td></tr>';
}
$msg.='<!-- end  here -->';

$pp++;
}

}


if($news_id!="")
{

if($pp>0)
{
  $msg.='<tr><td style="padding: 15px 20px;" align="center"><hr></td></tr>';
  $pp=0; 
}

$sql1="SELECT * from tsr_web_news where id IN(".$news_id.")";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

$count_row = count($rec);
$ip=0;
foreach($rec as $newsData)
{
  $ip++;
  $title1=$newsData["title"];
  $title = str_replace("â€™", "'", $title1);
  $newsId=$newsData["id"];
  $description=$newsData["overview"];
  $description = str_replace("â€™", "'", $description);
  // $image=$newsData["imgname"];

  $sqlImages = "SELECT * FROM ".TABLE_IMAGES." WHERE img_type='NEWS_IMAGE' and propty_id=".$newsData['id'];
  $recImages=($obj_mysql->getAllData($sqlImages));
  $image=$recImages[0]['image'];



  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $title);
  $name = trim($name);
  $newsName = str_replace("--","-",str_replace(" ","-",$name));
  $newsName=preg_replace('/-+/', '-', strtolower($newsName)); 

  $newsTitledot = "";
  $newtitle_count = strlen(strip_tags($title));
  if($newtitle_count >70) {
    $newsTitledot = "...";
  } 
  $newsDescriptiondot = "";
  $newsDescription_count = strlen(strip_tags($description));
  if($newsDescription_count >120) {
    $newsDescriptiondot = "...";
  }

 $realUrl= "https://360realtors.com/news/" . $newsName . "-nid" . $newsId;

 $newsURL = "https://360realtors.com/realtyrounduptrack/mailClick?url=".$realUrl."&newsId=".$newsId."&mailerId=".$mailerId;


$msg.='
<!-- start here -->
<tr>
  <td>
      <table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
      <tr>
        <td><a href="'.$newsURL.'" target="_blank"  style="text-decoration:none;"><img src="https://static.360realtors.ws/news/'.$newsId.'/'.$image.'" width="220"  style="height: 160px;"></a></td>
        <td style="padding:0 15px" valign="top"><a href="'.$newsURL.'" target="_blank"  style="text-decoration:none;"><p style="margin-top:0;font-size: 18px;font-family: Raleway;font-weight: 800;  color: #A44785;">'.substr(strip_tags($title),0,70).$newsTitledot.'</p></a>
          <h3 style="font-family: Raleway;font-size: 14px;color: #333232;font-weight: bold;margin: 5px 0 12px;">'.substr(strip_tags($description),0,70).$newsDescriptiondot.'</h3>
          <a href="'.$newsURL.'" target="_blank" style="background: linear-gradient(45deg, #9a2690, #5e3062);color: #ffffff;font-weight: 500;display: inline-block;height:37px;line-height:37px;padding: 0px 25px;text-decoration: none;text-transform: uppercase;font-size: 15px;font-family: Raleway;" href="">Read More</a>
        </td>
      </tr>
    </table>
  </td>
</tr>';
if($ip!=$count_row){
  $msg.='<tr><td style="padding: 15px 20px;" align="center"><hr></td></tr>';
}
$msg.='<!-- end  here -->';
$pp++;
}

}


if($blog_id!="")
{

if($pp>0)
{
  $msg.='<tr><td style="padding: 15px 20px;" align="center"><hr></td></tr>';
  $pp=0; 
}

$sql1="SELECT * from tsr_blog where id IN(".$blog_id.")  ";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

$count_row = count($rec);
$ip=0;
foreach($rec as $newsData)
{
  $ip++;
  $title1=$newsData["title"];
  $title = str_replace("â€™", "'", $title1);
  $newsId=$newsData["id"];
  $description=$newsData["content"];
  $description = str_replace("â€™", "'", $description);
  $image=$newsData["imgname"];
  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $title);
  $name = trim($name);
  $newsName = str_replace("--","-",str_replace(" ","-",$name));
  $newsName=preg_replace('/-+/', '-', strtolower($newsName)); 

  $newsTitledot = "";
  $newtitle_count = strlen(strip_tags($title));
  if($newtitle_count >70) {
    $newsTitledot = "...";
  } 
  $newsDescriptiondot = "";
  $newsDescription_count = strlen(strip_tags($description));
  if($newsDescription_count >120) {
    $newsDescriptiondot = "...";
  }

 $realUrl= "https://360realtors.com/blog/post/" . $newsName . "-blid" . $newsId;

 $newsURL = "https://360realtors.com/realtyrounduptrack/mailClick?url=".$realUrl."&newsId=".$newsId;

$msg.='
<!-- start here -->
<tr>
  <td>
    <table width="560" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
      <tr>
        <td><a href="'.$newsURL.'" target="_blank"  style="text-decoration:none;"><img src="https://static.360realtors.ws/blog/'.$newsId.'/'.$image.'" width="220"  style="height: 160px;"></a></td>
        <td style="padding:0 15px" valign="top"><a href="'.$newsURL.'" target="_blank"  style="text-decoration:none;"><p style="margin-top:0;font-size: 18px;font-family: Raleway;font-weight: 800;  color: #A44785;">'.substr(strip_tags($title),0,70).$newsTitledot.'</p></a>
          <h3 style="font-family: Raleway;font-size: 14px;color: #333232;font-weight: bold;margin: 5px 0 12px;">'.substr(strip_tags($description),0,70).$newsDescriptiondot.'</h3>
          <a href="'.$newsURL.'" target="_blank" style="background: linear-gradient(45deg, #9a2690, #5e3062);color: #ffffff;font-weight: 500;display: inline-block;height:37px;line-height:37px;padding: 0px 25px;text-decoration: none;text-transform: uppercase;font-size: 15px;font-family: Raleway;" href="">Read More</a>
        </td>
      </tr>
    </table>
  </td>
</tr>';

if($ip!=$count_row){
  $msg.='<tr><td style="padding: 15px 20px;" align="center"><hr></td></tr>';
}
$msg.='<!-- end  here -->';
}

}





  $msg.='
   </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="584" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit" style="background: linear-gradient(0deg, #9a2690, #5e3062)">
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                  <p style="font-family: Raleway;font-size: 14px; font-weight:bold;color: #fff;padding-top: 30px;padding-bottom: 15px;">Please fill the below mentioned survey form to curate more<br> personalized and customized content for you:</p>
                </td>

              </tr>
              <tr>
                <td align="center" style="padding: 15px 20px;"><a style="background: #ffffff;color: #401d5e;font-weight: 500;display: inline-block;height:37px;line-height:37px;padding: 0px 25px;text-decoration: none;text-transform: uppercase;font-size: 15px;font-family: Raleway;" href="">Click Here</a></td>
              </tr>
              <tr>
                <td style="text-align:center;padding-top: 15px;">
                  <a href="#"><img width="35" src="'.C_ROOT_URL.'weekly_news/view/comm_images/linkedin.png" style="margin-right: 15px"></a>
                  <a href="https://www.facebook.com/360Realtors" target="_blank"><img width="35" src="'.C_ROOT_URL.'weekly_news/view/comm_images/facebook.png" style="margin-right: 15px"></a>
                  <a href="#"><img width="35" src="'.C_ROOT_URL.'weekly_news/view/comm_images/youtube.png"></a>
                </td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="center">
                  <table  width="540" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
                    <tr>
                      <td style="border-top: 1px solid #a7a7a7;" align="center"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
              <tr>
                <td align="center"><p style="font-family: Raleway;font-size: 14px;font-weight: normal;
                color: #fff;
                "> If you find our Newsletter informative and insightful then please <a href="#" style="color: #ffffff;">subscribe</a> to it
              </p>
            </td>
          </tr>
          <tr>
            <td align="center">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </tbody>
</table>
</div>
</body>
</html>';

echo $msg;






?>



