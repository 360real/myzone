<?php 
class CommonHead {
function commonHeader($headMenu, $site, $siteUrl) {  ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex, nofollow">
<meta name="robots" content="noarchive">
<title><?php echo $site?></title>
<link href="<?php echo $siteUrl?>view/css/admin-style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $siteUrl?>view/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $siteUrl?>view/css/css.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript" language="javascript">
$(function() {
$(this).bind("contextmenu", function(e) {
e.preventDefault();
alert('Right Click is not allowed');
});
});
$('body').bind('cut copy',function(e) {
    e.preventDefault(); return false; 
});
</script>

<?php 
if (strpos($_SERVER["REQUEST_URI"], 'blog') !== false)
{
echo '<script type="text/javascript" src="'.$siteUrl.'view/ckeditor/ckeditor.js"></script>';
} else if (strpos($_SERVER["REQUEST_URI"], 'news') !== false)
{
echo '<script type="text/javascript" src="'.$siteUrl.'view/ckeditor_news/ckeditor.js"></script>';
} else
{
echo '<script type="text/javascript" src="'.$siteUrl.'view/ckeditor/ckeditor.js"></script>';
}
?>

<script type="text/javascript" src="<?php echo $siteUrl?>view/js/lib.js"></script>
<script type="text/javascript" src="<?php echo $siteUrl?>view/js/ajax.js"></script>

<script src="<?php echo $siteUrl?>view/js/dimensions.js" type="text/javascript"></script>
</head>

<body class="bg">
<div class="wrapper1">
<div class="hdr">

<div class="logo">
<a href="<?php echo C_ROOT_URL; ?>index.php"><img src="<?php echo $siteUrl?>view/images/admin.png"  width="147" height="17" alt="Adminstration" class="admin_logo" align="left" /> </a>
<div class="headTitle"><?php echo "(".$headMenu.")" ?></div>
<div class="header_cms">Content Management System</div>
<img src="<?php echo $siteUrl?>view/images/logo.jpg" alt="360 Realtors" class="buy_prop_logo"  align="right" />
</div>

<div class="menu">
<div class="navi floatL">
<nav>
<ul>
<?php
$url = $_SERVER['REQUEST_URI'];
$var = explode("/",$url);
?>
<?php if($_SESSION['login']['role_id'] == "22"){ ?>
<li><a href="<?php echo $siteUrl?>jobportal/controller/controller.php?action=joblist">Job&nbsp;Portal</a></li>
<?php } ?>
<?php if($_SESSION['login']['role_id'] == "22"){ ?>
<li><a href="<?php echo $siteUrl?>jobcategory/controller/controller.php?action=joblist">Job&nbsp;Category</a></li>
<?php } ?>
<?php if($_SESSION['login']['role_id'] == "22"){ ?>
<li><a href="<?php echo $siteUrl?>jobportal/controller/controller.php?action=applicant">Job&nbsp;Applicants</a></li>
<?php } ?>


<?php if($_SESSION['login']['role_id']=="2"){?>
<!-- <li><a href="<?php echo $siteUrl?>leads_panel/index.php">Leads&nbsp;Manager</a></li> -->
<?php }else if($_SESSION['login']['role_id']=="7"){?>
<li><a href="<?php echo $siteUrl?>ppc_campaign/index.php">PPC&nbsp;Campaign&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>/microsites_ppc/controller/controller.php">New&nbsp;PPC&nbsp;Campaign&nbsp;Manager</a></li>
<?php }else if($_SESSION['login']['role_id']=="5"){?>

<li><a href="<?php echo $siteUrl?>template/index.php">Template&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>template_types/index.php">Template&nbsp;Types&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=single">Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=multiple">Multiple Microsite&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>microsite_blog/index.php">Microsite Blog&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_news/index.php">Microsite News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_comment/index.php">Microsite Comments&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_user/index.php">Microsite User&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_category_detail/index.php">Microsite&nbsp;Category&nbsp;Content&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
</ul>
</li>
<li><a href="<?php echo $siteUrl?>developer/index.php">Developer&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>sections/index.php">Blog&nbsp;Section&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>blog_category/index.php">Blog&nbsp;Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>authors/index.php">Blog&nbsp;Author&nbsp;Manager</a></li>
</ul>
</li>

<?php if($_SESSION['login']['id']=="7"){?>
<li><a href="<?php echo $siteUrl?>ppc_campaign/index.php">PPC&nbsp;Campaign&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>website_ppc_campaign/index.php">Website&nbsp;PPC&nbsp;Campaign&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>/microsites_ppc/controller/controller.php">New&nbsp;PPC&nbsp;Campaign&nbsp;Manager</a></li>

<?php }?>

<?php } else if($_SESSION['login']['role_id']=="4"){?>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=single">Microsite&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=multiple">Multiple Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_blog/index.php">Microsite Blog&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_news/index.php">Microsite News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_comment/index.php">Microsite Comments&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_user/index.php">Microsite User&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_category_detail/index.php">Microsite&nbsp;Category&nbsp;Content&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_meta/index.php">Microsite&nbsp;Meta&nbsp;Manager</a></li>
</ul>
</li>
<li><a href="<?php echo $siteUrl?>developer/index.php">Developer&nbsp;Manager</a>
<ul><li><a href="<?php echo $siteUrl?>developer_template/index.php">Developer&nbsp;Template&nbsp;Manager</a></li></ul>
</li>
<!-- <li><a href="<?php echo $siteUrl?>leads_panel/index.php">Leads&nbsp;Manager</a> -->
<li><a href="<?php echo $siteUrl?>types/index.php">Type&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>category/index.php">Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>amenities/index.php">Amenities&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>meta/index.php">Meta&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>country/index.php">Country&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>sections/index.php">Blog&nbsp;Section&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>blog_category/index.php">Blog&nbsp;Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>authors/index.php">Blog&nbsp;Author&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>news/index.php">News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>news_section/index.php">News&nbsp;Section&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>question/index.php">Question&nbsp;Manager</a></li>

</ul>
</li>

<li><a href="<?php echo $siteUrl?>specials/index.php">Special&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>banner/index.php">Banner&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>review/index.php">Review&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>comment/index.php">Comment&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>site_map/index.php">Site&nbsp;Map&nbsp;Manager</a></li>

<?php } else if($_SESSION['login']['role_id']=="3") {?>


<li><a href="<?php echo $siteUrl?>template/index.php">Template&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>template_types/index.php">Template&nbsp;Types&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=single">Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=multiple">Multiple Microsite&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>microsite_blog/index.php">Microsite Blog&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_news/index.php">Microsite News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_comment/index.php">Microsite Comments&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_user/index.php">Microsite User&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_category_detail/index.php">Microsite&nbsp;Category&nbsp;Content&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_meta/index.php">Microsite&nbsp;Meta&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>developer/index.php">Developer&nbsp;Manager</a>
<ul><li><a href="<?php echo $siteUrl?>developer_template/index.php">Developer&nbsp;Template&nbsp;Manager</a></li></ul>
</li>

<li><a href="<?php echo $siteUrl?>types/index.php">Type&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>category/index.php">Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>amenities/index.php">Amenities&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>meta/index.php">Meta&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>country/index.php">Country&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a>
<ul>

<li><a href="<?php echo $siteUrl?>luxury/index.php">Luxury&nbsp;Property</a></li>
<li><a href="<?php echo $siteUrl?>home/index.php">Homepage&nbsp;Banners</a></li>
<li><a href="<?php echo $siteUrl?>commercial/index.php">Commercial&nbsp;Property</a></li>

</ul>


</li>

<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>sections/index.php">Blog&nbsp;Section&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>blog_category/index.php">Blog&nbsp;Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>authors/index.php">Blog&nbsp;Author&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>specials/index.php">Special&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>banner/index.php">Banner&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>review/index.php">Review&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>comment/index.php">Comment&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>news/index.php">News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>news_section/index.php">News&nbsp;Section&nbsp;Manager</a></li>

</ul>
</li>
<li><a href="<?php echo $siteUrl?>events/index.php">Events Manager</a>

<ul>
	<li><a href="<?php echo $siteUrl?>events/controller/controller.php?action=BannerListing">Event&nbsp;Banner&nbsp;Manager </a></li>

</ul>


</li>

<!-- <li><a href="<?php echo $siteUrl?>nri/index.php">NRI Manager</a>

<ul>
	<li><a href="<?php echo $siteUrl?>nri/controller/controller.php?action=BannerListing">NRI&nbsp;Home&nbsp;Page </a></li>
        <li><a href="<?php echo $siteUrl?>nri/controller/controller.php?action=ContentDetailforHomePage">Main Content</a></li>
        <li><a href="<?php echo $siteUrl?>nri/controller/controller.php?action=topsixproperty">Top&nbsp;six&nbsp;Property </a></li>

</ul>

</li> -->

<?php } else if($_SESSION['login']['role_id']=="8" || $_SESSION['login']['role_id']=="9"){?>

<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
</ul>
</li>
<li><a href="<?php echo $siteUrl?>category/index.php">Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>meta/index.php">Meta&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>sections/index.php">Blog&nbsp;Section&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>blog_category/index.php">Blog&nbsp;Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>authors/index.php">Blog&nbsp;Author&nbsp;Manager</a></li>
</ul>
</li>
<li><a href="<?php echo $siteUrl?>specials/index.php">Special&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>banner/index.php">Banner&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>developer/index.php">Developer&nbsp;Manager</a></li>

<?php if($_SESSION['login']['role_id']=="8") {?>
<li><a href="<?php echo $siteUrl?>comment/index.php">Comment&nbsp;Manager</a></li>
<!-- <li><a href="<?php echo $siteUrl?>leads_panel/index.php">Leads&nbsp;Manager</a></li> -->

<?php }?>

<?php } else if($_SESSION['login']['role_id']=="10"){?>
<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a></li>

</ul>
</li>

<?php } else if($_SESSION['login']['role_id']=="11"){?>
<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>comment/index.php">Comment&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>banner/index.php">Banner&nbsp;Manager</a></li>
</ul>
</li>

<?php } else if($_SESSION['login']['role_id']=="12"){?>
<!-- <li><a href="<?php echo $siteUrl?>leads_panel/index.php">Leads&nbsp;Manager</a></li> -->
<!-- <li><a href="<?php echo $siteUrl?>project_wise_duplicate_lead/index.php">Duplicate&nbsp;Leads&nbsp;Manager</a></li> -->
<li><a href="<?php echo $siteUrl?>ppc_campaign/index.php">PPC&nbsp;Campaign&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>website_ppc_campaign/index.php">Website&nbsp;PPC&nbsp;Campaign&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>/microsites_ppc/controller/controller.php">New&nbsp;PPC&nbsp;Campaign&nbsp;Manager</a></li>
</ul>
</li>

<?php } else if($_SESSION['login']['role_id']=="13"){?>
<li><a href="<?php echo $siteUrl?>specials/index.php">Special&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a></li>
<!-- </ul>
</li> -->

<?php } else if($_SESSION['login']['role_id']=="15"){?>
<li><a href="<?php echo $siteUrl?>microsite_blog/index.php">Microsite Blog&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=single">Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=multiple">Multiple Microsite&nbsp;Manager</a></li>
</ul>
</li>

<?php } else if($_SESSION['login']['role_id']=="16") {?>
<li><a href="<?php echo $siteUrl?>country/index.php">Country&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a></li>
</ul>
</li>
<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>specials/index.php">Special&nbsp;Manager</a></li>

<?php if($_SESSION['login']["id"]==12) { ?>
<li><a href="<?php echo $siteUrl?>banner/index.php">Banner&nbsp;Manager</a></li>
<?php } ?>

<?php } else if($_SESSION['login']['role_id']=="14"){?>

<li><a href="<?php echo $siteUrl?>country/index.php">Country&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a></li>

<li><a href="<?php echo $siteUrl?>microsites/index.php?type=single">Microsite&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=multiple">Multiple Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_blog/index.php">Microsite Blog&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_news/index.php">Microsite News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_category_detail/index.php">Microsite&nbsp;Category&nbsp;Content&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_meta/index.php">Microsite&nbsp;Meta&nbsp;Manager</a></li>
</ul>
</li>


<li><a href="<?php echo $siteUrl?>developer/index.php">Developer&nbsp;Manager</a>


<?php }  else if($_SESSION['login']['role_id']=="17") {?>
<li><a href="<?php echo $siteUrl?>/mailtemplate">Send&nbsp;Custom&nbsp;Email</a></li>
<li><a href="<?php echo $siteUrl?>internal_news/index.php">Internal&nbsp;News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>news_tags/index.php">News&nbsp;Tags&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>internalNewsReport/index.php">Reports</a></li>
<?php } else if($_SESSION['login']['role_id']=="19") {?>

<li><a href="<?php echo $siteUrl?>microsites/index.php?type=single">Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=multiple">Multiple Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsite_blog/index.php">Microsite Blog&nbsp;Manager</a></li>

<?php } else if($_SESSION['login']['role_id']=="18") {?>
		<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a></li>
<?php }else if($_SESSION['login']['role_id']=="20") {?>

<li><a href="<?php echo $siteUrl?>commercial/index.php">Property&nbsp;Manager</a>
<ul>

<li><a href="<?php echo $siteUrl?>commercial/index.php">All Leads</a></li>
<!-- <li><a href="<?php echo $siteUrl?>commercial/controller/controller.php?action=pulledData">Pulled Data</a></li> -->
<li><a href="<?php echo $siteUrl?>commercial/controller/controller.php?action=BulkUpload">Data Upload</a></li>

</ul>
</li>
<li><a href="<?php echo $siteUrl?>developer/index.php">Developer&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>country/index.php">Country&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
</ul>
</li>
<?php } else if($_SESSION['login']['role_id']=="1"){?>
<li><a href="<?php echo $siteUrl?>user/index.php">User&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>role/index.php">Role&nbsp;Manager</a></li>
</ul>
</li>
<li><a href="<?php echo $siteUrl?>template/index.php">Template&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>template_types/index.php">Template&nbsp;Types&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=single">Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>microsites/index.php?type=multiple">Multiple Microsite&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>meta/index.php">Meta&nbsp;Manager</a></li>

</ul>
</li>

<li><a href="<?php echo $siteUrl?>developer/index.php">Developer&nbsp;Manager</a>
<ul><li><a href="<?php echo $siteUrl?>developer_template/index.php">Developer&nbsp;Template&nbsp;Manager</a></li></ul>
</li>

<li><a href="<?php echo $siteUrl?>types/index.php">Type&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>category/index.php">Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>amenities/index.php">Amenities&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>country/index.php">Country&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>city/index.php">City&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>location/index.php">Location&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>cluster/index.php">Cluster&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>micro_market/index.php">Micro Market&nbsp;Manager</a></li>
</ul>
</li>


<li><a href="<?php echo $siteUrl?>property/index.php">Property&nbsp;Manager</a>
<ul>

<li><a href="<?php echo $siteUrl?>luxury/index.php">Luxury&nbsp;Property</a></li>
<li><a href="<?php echo $siteUrl?>home/index.php">Homepage&nbsp;Banners</a></li>
<li><a href="<?php echo $siteUrl?>commercial/index.php">Commercial&nbsp;Property</a></li>

</ul>
</li>


<!-- <li><a href="<?php echo $siteUrl?>leads_panel/index.php">Leads&nbsp;Manager</a> -->
<ul>
<!-- <li><a href="<?php echo $siteUrl?>project_wise_duplicate_lead/index.php">Duplicate&nbsp;Leads&nbsp;Manager</a></li> -->
<li><a href="<?php echo $siteUrl?>ppc_campaign/index.php">PPC&nbsp;Campaign&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>website_ppc_campaign/index.php">Website&nbsp;PPC&nbsp;Campaign&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>/microsites_ppc/controller/controller.php">New&nbsp;PPC&nbsp;Campaign&nbsp;Manager</a></li>
</ul>
</li>

<li><a href="<?php echo $siteUrl?>blog/index.php">Blog&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>sections/index.php">Blog&nbsp;Section&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>blog_category/index.php">Blog&nbsp;Category&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>authors/index.php">Blog&nbsp;Author&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>specials/index.php">Special&nbsp;Manager</a>
</ul>
</li>
<?php if($_SESSION['login']['role_id']=="1" || $_SESSION['login']['role_id']=="3") { ?>
<li><a href="<?php echo $siteUrl?>area_converter/controller/controller.php?action=list_area_converter">Area Calculator</a></li>
<?php } ?>

<li><a href="<?php echo $siteUrl?>comment/index.php">Comment&nbsp;Manager</a>
<ul>
<li><a href="<?php echo $siteUrl?>newsletter/index.php">Newsletter&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>review/index.php">Review&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>news/index.php">News&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>news_section/index.php">News&nbsp;Section&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>question/index.php">Question&nbsp;Manager</a></li>
</ul>
</li>




<?php } ?>
<?php if($_SESSION['login']['role_id']=="1" || $_SESSION['login']['role_id']=="2" || $_SESSION['login']['role_id']=="3") { ?>

<li><a href="<?php echo $siteUrl?>events/index.php">Events Manager</a>

<ul>
	<li><a href="<?php echo $siteUrl?>events/controller/controller.php?action=BannerListing">Event&nbsp;Banner&nbsp;Manager </a></li>
</ul>
</li>

<!-- <li><a href="<?php echo $siteUrl?>nri/index.php">NRI Manager</a>
<ul>
	<li><a href="<?php echo $siteUrl?>nri/controller/controller.php?action=BannerListing">NRI&nbsp;Home&nbsp;Page </a></li>
        <li><a href="<?php echo $siteUrl?>nri/controller/controller.php?action=ContentDetailforHomePage">Main Content</a></li>
        <li><a href="<?php echo $siteUrl?>nri/controller/controller.php?action=topsixproperty">Top&nbsp;six&nbsp;Property </a></li>
</ul>
</li> -->

<?php } ?>

<?php if($_SESSION['login']['role_id']=="1" || $_SESSION['login']['role_id']=="3") { ?>

<li><a href="<?php echo $siteUrl?>commercialamenities/index.php">Commercial Management</a>
<ul>
<li><a href="<?php echo $siteUrl?>commercialamenities/index.php">Amenities</a></li>
<li><a href="<?php echo $siteUrl?>commercialsaletype/index.php">Sale Type</a></li>
<li><a href="<?php echo $siteUrl?>commercialpropertytype/index.php">Property Type</a></li>
<li><a href="<?php echo $siteUrl?>commercialspecification/index.php">Property Specification</a></li>
<li><a href="<?php echo $siteUrl?>commercial_micro_market/index.php">Micro Market</a></li>
<li><a href="<?php echo $siteUrl?>commercial_services/index.php">Commercial Services</a></li>
<li><a href="<?php echo $siteUrl?>commercial_building_type/index.php">Commercial Building Type</a></li>
</ul>
</li> 
<?php } ?>

<?php if($_SESSION['login']['role_id']=="1" || $_SESSION['login']['role_id']=="3") { ?>
<li><a href="<?php echo $siteUrl?>weekly_news/index.php">Weekly&nbsp;News&nbsp;Manager</a></li>
<?php } ?>

<!-- role id 21 for exlr8 website management start here -->
<?php if($_SESSION['login']['role_id']=="21") { ?>
<li><a href="<?php echo $siteUrl?>xlr8/mission/index.php">About&nbsp;Manager</a>
	<ul>
	<li><a href="<?php echo $siteUrl?>xlr8/webinfo/index.php">About Us&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/glance/index.php">Glance&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/mission/index.php">Mission&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/leadership/index.php">Leadership&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/testimonial/index.php">Testimonial&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/developers/index.php">Developers&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/media_section/index.php">Media&nbsp;Section&nbsp;Manager</a></li>


	</ul>
</li>


<li><a href="<?php echo $siteUrl?>xlr8/resource/index.php">Resource&nbsp;Manager</a>
	<ul>
	<li><a href="<?php echo $siteUrl?>xlr8/resource/index.php">Resource&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/resource_category/index.php">Resource&nbsp;Category&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/essential_reading/index.php">Essential&nbsp;Reading</a></li>
	</ul>
</li>

<li><a href="<?php echo $siteUrl?>xlr8/write_to_us/index.php">Contact&nbsp;Us&nbsp;Manager</a>
	<ul>
	<li><a href="<?php echo $siteUrl?>xlr8/write_to_us/index.php">Write&nbsp;To&nbsp;Us&nbsp;Manager</a></li>
	</ul>
</li>
<li><a href="<?php echo $siteUrl?>xlr8/how_it_works/index.php">How&nbsp;It&nbsp;Works&nbsp;Manager</a>
	<ul>
	<li><a href="<?php echo $siteUrl?>xlr8/service_category/index.php">Service&nbsp;Category&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/services/index.php">Service&nbsp;Manager</a></li>
	<li><a href="<?php echo $siteUrl?>xlr8/what_we_do/index.php">What&nbsp;We&nbsp;Do&nbsp;Manager</a></li>
	</ul>
</li>

<?php } ?>
<!-- role id 21 for exlr8 website management end here -->


<!-- role id 3 for 360edge website management start here -->
<?php if($_SESSION['login']['role_id']=="1" || $_SESSION['login']['role_id']=="3") { ?>
<li><a href="<?php echo $siteUrl?>360edge/franchise/index.php">Franchise&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>360edge/franchiselead/index.php">Franchise&nbsp;Lead&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>360edge/team/index.php">Team&nbsp;Manager</a></li>
<li><a href="<?php echo $siteUrl?>banner_manager/controller/controller.php?action=BannerListing">Ads&nbsp;Manager</a></li>
<?php } ?>
<!-- role id 3 for 360edge website management end here -->



</nav>
</div>

<div class="Navi_left floatR">
<ul>
<li><a href="#"><?php echo($_SESSION['login']['name'])?></a></li>
<li><a href="<?php echo $siteUrl?>index.php" <?php if($var[1]=="index.php" or $var[1]=="") {echo 'class="current"';}?>>DashBoard <img src="<?php echo $siteUrl?>view/images/dash_board_icon.jpg"  /></a></li>
<li><a href="<?php echo $siteUrl?>index.php?action=logout">Logout <img src="<?php echo $siteUrl?>view/images/logut_icon.jpg"  /></a></li>
</ul>
</div>
</div>

</div>
</div>
<?php }}?>
