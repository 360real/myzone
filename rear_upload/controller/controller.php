<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
use PhpOffice\PhpSpreadsheet\Reader\Xlsx; 
admin_login();
$rolesession = explode(',',$_SESSION['login']['role_id']);
/*$resultdev = !empty(array_intersect($rolesession ,array(21,22) ));*/
$result = !empty(array_intersect($rolesession ,array(1,23,33)));
if(count($result) ==0){
$obj_common->redirect('/index.php');
}
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_LOCATIONS;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/rear_upload/controller/controller.php';
	$page_name_insert =C_ROOT_URL.'/rear_upload/controller/controller.php?action=Create';
	$clsRow1	=	'clsRow1';
	$clsRow2	=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
			$file_name = ROOT_PATH."rear_upload/model/model.php" ;
			break;	

		case 'Upload':

		$fileMimes = array(
		'text/x-comma-separated-values',
		'text/comma-separated-values',
		'application/octet-stream',
		'application/vnd.ms-excel',
		'application/x-csv',
		'text/x-csv',
		'text/csv',
		'application/csv',
		'application/excel',
		'application/vnd.msexcel',
		'text/plain',
		'text/xls', 
		'text/xlsx',
		'application/excel',
		'application/vnd.msexcel',
		'application/vnd.ms-excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		);

		if(!empty($_FILES['rera']['name']) && in_array($_FILES['rera']['type'], $fileMimes)){
		// Open uploaded CSV file with read-only mode
		$csvFile = fopen($_FILES['rera']['tmp_name'], 'r');
		// Skip the first line
		fgetcsv($csvFile);
		// Parse data from CSV file line by line  

		$projectArray= array();  
		$projectArraynew= array();   
		$i=0;  
		//$getData = fgetcsv($csvFile, 10000, ",");
		while (($getData = fgetcsv($csvFile, 10000, ",")) !== FALSE)
		{
			$propid =  $getData[0];
			$reraid = $getData[1]; 
			$name = $getData[2];
			$url = $getData[3];    
			// $delQuery = "Delete from tsr_properties_rera where prop_id=".$propid;
			// $obj_mysql->exec_query($delQuery, $link);
			$projectArray[$i]['prop_id'] = $getData[0];
			$projectArray[$i]['rera_no'] =$getData[1]; 
			$projectArray[$i]['status'] = 1;
			$projectArray[$i]['rera_tower'] =  $getData[2];
			$projectArray[$i]['rera_url'] = $getData[3];             
		$i++;}
		fclose($csvFile);
		}

		for ($j=0; $j < count($projectArray); $j++) { 

			if($projectArray[$j]['prop_id']!='' && $projectArray[$j]['rera_no']!=''){
				$projectArraynew['prop_id'] = trim($projectArray[$j]['prop_id']);
				$projectArraynew['rera_no'] = trim($projectArray[$j]['rera_no']);
				$projectArraynew['status'] = 1;
				$projectArraynew['rera_tower'] = trim($projectArray[$j]['rera_tower']);
				$projectArraynew['rera_url'] =  trim($projectArray[$j]['rera_url']);
				$newrera=str_replace(' ', '', strip_tags(trim($projectArray[$j]['rera_no'])));
				$reraquery = "SELECT id FROM ".TABLE_PROPERTIES_RERA." as rera WHERE rera.prop_id='".trim($projectArray[$j]['prop_id'])."' AND rera.rera_no='".$newrera."'";
				$reraquery = $obj_mysql->get_row_arr($reraquery);
				if(!empty($reraquery)){
					unset($projectArraynew['prop_id']);
					$obj_mysql->update_data(TABLE_PROPERTIES_RERA, 'rera_no', $projectArraynew, trim($projectArray[$j]['rera_no']));
				}else{
					$obj_mysql->insert_data(TABLE_PROPERTIES_RERA, $projectArraynew);
				}
			}

		} 

		$obj_common->redirect($page_name_insert . "&msg=insert");
		break;
		default:
			$file_name = ROOT_PATH."rear_upload/view/model.php" ;
			break;
	}

	

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage rear_uploads', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php 
function getMicroMarket($id,$obj_mysql){
	if($id >0){
		$sql = "select name from ".TABLE_MICRO_MARKET." where id=".$id;
		$result = $obj_mysql->get_assoc_arr($sql);
		return $result['name'];
	}else{
		return '';
	}
}
include(LIBRARY_PATH."includes/footer.php");?>
