<?php
include_once(LIBRARY_PATH . "media/view/newsheader.php");

?>
<body>
    <form method="post" name="eventDetail" id="eventForm" onsubmit="validateForm(this);" enctype="multipart/form-data">
        <table width="100%" class="MainTable eventinfo">
            <thead>
                <tr>
            <h3><th align="left" colspan="2">News Details</th></h3>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="input-lbl">News Title<font color="#FF0000">*</font></td>
                    <td>
                        <input type="text" id="newsTitle" name="newsTitle" value="<?php echo $getdataforNews["Title"]; ?>" placeholder="News Title " style="width: 450px;" required="true">
                    </td>
                </tr>
                <tr> 
                    <td class="input-lbl">Images <font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="Images" id="images" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
<?php if ($action == "Create") { ?>      
                                   required = 'required';
                        <?php } ?>
                               size-allowed="200"><br/>
                        (Size Less then 200kb)

                        <div id="imagesExists">

<?php if ($getdataforNews['image_path'] != "") { ?>


                                <div class="filediv"> 
                                    <div style="float:left;">
                                        <img src="<?php echo $staticContentUrl . '/Media/IMAGES/' . $getdataforNews['id'] . "/" . $getdataforNews['image_path']; ?>" width="75px;" height="75px;" title="<?php echo $getdataforNews['image_path']; ?>">
                                    </div>

                                </div>

<?php }
?>

                        </div>
                    </td>
                </tr> 

                <tr> 
                    <td class="input-lbl">PDF File</td>
                    <td >
                        <input type="file" name="Newspdf" id="Newspdf" accept="file/pdf" allowed="pdf" >
                               <?php if ($getdataforNews['pdf_file_path'] != "") { ?>

                        <div style="padding-top: 5px;">
                                <a href="<?php echo $staticContentUrl . '/Media/PDF/' . $getdataforNews['id'] . '/' . $getdataforNews['pdf_file_path']; ?>"  target="_blank">

                                    <font color="#0000FF"> <?php echo $getdataforNews['pdf_file_path']; ?></font>

                                </a>
                            </div>


                            <?php } ?>

                        </div>
                    </td>
                </tr> 
                <tr>
                    <td class="input-lbl">News Link</td>
                    <td><input type="url" name="newsLink" id="newsLink" value="<?php echo $getdataforNews["media_link"]; ?>" placeholder="News Link " style="width: 450px;" ></td>
                </tr>
                <tr>
                    <td class="input-lbl">Status<font color="#FF0000">*</font></td>
                    <td>
                        <input type="radio" name="status" value="1"  <?php if ($getdataforNews["status"] == "1") { ?> checked <?php } ?> required="true"
                               <?php  if ($action == "Create") { ?> 
                              checked
                                <?php } ?>
                               
                               >Active
                        <input type="radio" name="status" value="2"  <?php if ($getdataforNews["status"] == "2") { ?> checked <?php } ?> required="true">Inactive

                    </td>
                </tr>

                <tr>
                    <td> </td>
                    <td> 
                        <input type="submit" name="submit" class="addmorebtn" Id="newsSaveBtn" 
                               value="<?php  if ($action == "Create") { ?>Sumbit<?php  } else {?>Update<?php } ?>"
                                <?php  if ($action == "Create") { ?> 
                               pageType="Create"
                                <?php } else { ?>
                                pageType="update"
                                <?php } ?>
                               >
                        <?php  if ($action == "Create") { ?>  
                        <input type="reset" id="reset" name="reset" class="addmorebtn" >
                        <?php }  else { ?>
                         <input type="button" onclick="window.location='<?php echo C_ROOT_URL  ?>media/index.php'" class="addmorebtn" value="Cancel">
                        <?php  } ?>
                    </td>
                </tr>
        </table>

    </form>
</body>
</html>