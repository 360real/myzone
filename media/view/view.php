<!DOCTYPE html>
<?php include_once(LIBRARY_PATH . "media/view/newsheader.php"); ?>
    <body>
        <!-- HEADING  -->
        <table width="100%" cellpadding="2" class="SearchTable">
            <thead>
            <h3><tr align="right"><th><a href="<?= $pageName ?>?action=Create" class="add">Add News</a></th></tr></h3>
        </thead>
    </table>   

    <?php
    $NewsData = $getDataForview;
    ?>
     
    <table width="100%" class="MainTable" id="myTable">
       

        <thead>
            <tr>
                <th colspan="8" class="bgwth">
                    <div>
                        <input type="text" id="search-title" placeholder="Search Title">
                    </div>
                    
                    <div id="resetTable" class="refTable"  >Reset Table</div>
                </th>
             </tr>
            <tr>
                <th>News Title</th>
                <th>Link</th>
               <th>News Pdf</th>
                <th>Status</th>
               <th style="cursor: auto;">ACTION</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th data-showfilter="0"></th>
                <th data-showfilter="0"></th>
                <th data-showfilter="0"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="0"></th>
               
            </tr>
        </tfoot>

        <tbody>

            <?php

if (count($NewsData)>0){
            foreach ($NewsData as $value) {
                ?>
                <tr>


                    <td class="center" width="13%">

                        <?php
                        if ($value['Title'] == "") {

                            $NewsTitle = 'N/A';
                        } else {
                            
                            
                            $titlelen = strlen(trim($value['Title']));
                            
                           
                            if ($titlelen < 30){
                                
                                $NewsTitle =  $value['Title'];
                                
                            } else {
                                
                                 $NewsTitle =  substr($value['Title'],0,30)." ...";
                            }
                           

                           
                        }
                        echo $NewsTitle;
                        ?>

                    </td>
                   
                 
                    <td class="center" width="13%">
                        
                        <?php
                        
                        if ($value['media_link'] == ""){
                            
                            echo 'N/A';
                            
                        } else {
                        
                        echo '<a href="'.$value['media_link'].'" target="_blank"><font color="blue">'.$value['media_link'].'</font></a>'; 
                        
                        }
                        
                
                        ?></td>
                      <td class="center" width="13%">
                        
                        <?php
                        
                        if ($value['pdf_file_path'] == ""){
                            
                            echo 'N/A';
                            
                        } else {
                        
                        echo '<a href="'.$staticContentUrl.'/Media/PDF/'.$value['id'].'/'.$value['pdf_file_path'].' " target="_blank"><font color="blue">'.$value['pdf_file_path'].'</font></a>'; 
                        
                        }
                        
                      $statusarray = array('','green','red');
                      $statusArr = array('','Active','In Active');        
                        ?></td>
                    <td class="center" width="12%" style="color:<?php echo $statusarray[$value['status']]; ?>; font-weight:bold;">
                        <?php
                      echo $statusArr[$value['status']];
                      ?>
                    
                    </td>
            <a href="../../../360com/application/config/config.php"></a>
            <td width="12%">
                <a href="<?php echo $page_name . '?action=Update&newsId=' . $value['id'] ?>"  class="edit">Edit</a>
                
                    <a href="#" class="deletenews" NewsId="<?PHP echo $value['id']; ?>"
                        <?php if ($value['status'] ==  1) {?>
                         ChangeStatus="De Activate"
                        <?php } else { ?>
                        ChangeStatus="Activate"
                        <?php } ?>
                       >
                        
                        <?php if ($value['status'] ==  1) {?>
                        <font color="red"><b> De Activate </b></font>
                        <?php } else { ?>
                        <font color="green"><b> Activate </b> </font>
                        <?php } ?>
                    
                    </a>
               

            </td>
        </tr>

    <?php } 
}
    ?>
    
</tbody>

</thead>
</table>
</body>
</html>