<?php

require_once(ROOT_PATH . "library/mysql_function.php");


// this model for making all the query for event
class Model {

    private $dbObj;
    private $filesaveingpath;

    function __construct() {

        $this->dbObj = new mysql_function();

        if ($_SERVER['HTTP_HOST'] == '360cms.com') {

            $this->FIleSavingPath = ROOT_PATH;
        } else {

            $this->FIleSavingPath = "/var/www/html/myzone/media_images";
        }
    }

    // for inserting Data for news
    public function insertIntoDb() {
     
        $title = mysql_real_escape_string($_POST['newsTitle']);
       
       $imagePath = mysql_real_escape_string($_FILES['Images']['name']);
       
       if ($_FILES['Newspdf']['tmp_name'] != ""){
           
        $pdfPath = mysql_real_escape_string($_FILES['Newspdf']['name']);
        
       } else {
           
           $pdfPath = ""; 
       }
         $link = mysql_real_escape_string($_POST['newsLink']);
        $status = mysql_real_escape_string($_POST['status']);
      
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $createdDate = date('Y-m-d H:i:s');
      

        $sql = "INSERT INTO `tsr_media` ("
                . "`Title`,"
                . " `image_path`, "
                . " `pdf_file_path`,"
               . " `media_link`, "
                  . " `status`, "
                . "`created_by`, "
                . "`created_date`,"
                . "`modified_date`)"
              
                . "VALUES('" . $title . "', "
                . "'" . $imagePath . "',"
                . "'" . $pdfPath . "', "
                . "'" . $link . "',"
                . " '" . $status . "',"
                . " '" . $CreatedBy . "', "
                . "'" . $createdDate . "',"
                . "'" . $createdDate . "'"
                . "   )";

             
        $query = $this->dbObj->insert_query($sql);

        $newsId = $query;

        $this->uploadImageforNews($newsId);
        
        if ($pdfPath != ""){
        $this->uploadPdfforNews($newsId);
        }
       
        return $query;
    }

    // this method for upload all file in folder
    private function uploadImageforNews($newsId) {
     
        $_FILES['Images'];
       $Rootpathmain = $this->FIleSavingPath."/Media" ;
        
                        if (!is_dir($Rootpathmain)) {
                            mkdir($Rootpathmain);
                            chmod($Rootpathmain, 0777);
                        }
        
               $Rootpath = $Rootpathmain."/IMAGES" ;
        
                        if (!is_dir($Rootpath)) {
                            mkdir($Rootpath);
                            chmod($Rootpath, 0777);
                        }
                         
        
        $path = $Rootpath."/".$newsId;     
                
         if (!is_dir($path)) {
             
                            mkdir($path);
                            chmod($path, 0777);
                        }
              
                        
        $tmpname =  $_FILES['Images']['tmp_name'];
      
        move_uploaded_file($tmpname, $path . '/' . $_FILES['Images']['name']);
        
        
        
        
    }


 // this method for upload all file in folder
    private function uploadPdfforNews($newsId) {
     
        $_FILES['Images'];
        $Rootpathmain = $this->FIleSavingPath."/Media" ;
        
                        if (!is_dir($Rootpathmain)) {
                            mkdir($Rootpathmain);
                            chmod($Rootpathmain, 0777);
                        }
        
               $Rootpath = $Rootpathmain."/PDF" ;
        
                        if (!is_dir($Rootpath)) {
                            mkdir($Rootpath);
                            chmod($Rootpath, 0777);
                        }
                         
                        
                        
        $path = $Rootpath."/".$newsId;     
                
         if (!is_dir($path)) {
             
                            mkdir($path);
                            chmod($path, 0777);
                        }
        
         
       
        $tmpname =  $_FILES['Newspdf']['tmp_name'];
      
        move_uploaded_file($tmpname, $path . '/' . $_FILES['Newspdf']['name']);
        
        
        
        
    }

    //select all news
   public function selectAllNews($newsId=""){
       
       
       $sql = 'SELECT id,Title,image_path,pdf_file_path,media_link,status FROM `tsr_media`';
       
          if ($newsId != "") {
                   
          $sql .= " where id = '". mysql_real_escape_string($newsId) ."'";
          
               }
            
           $sql .=   ' ORDER by  id desc';
       
        
       $query = $this->dbObj->getAllData($sql);


        return $query;
       
   }
   
   //uodate news
   public function updateNews($newsId){
       
      
       
        $title = mysql_real_escape_string($_POST['newsTitle']);
       
      
        $link = mysql_real_escape_string($_POST['newsLink']);
        $status = mysql_real_escape_string($_POST['status']);
      
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $modified_date = date('Y-m-d H:i:s');
       
       $sql = "update tsr_media set "
               . " Title = '".$title."',"
               . " media_link = '".$link."',";
       
        if ($_FILES['Images']['tmp_name'] != ""){
            
          $sql .=    " image_path = '".mysql_real_escape_string($_FILES['Images']['name'])."',"; 
        }
        
         if ($_FILES['Newspdf']['tmp_name'] != ""){
            
          $sql .=     " pdf_file_path = '".mysql_real_escape_string($_FILES['Newspdf']['name'])."',"; 
        }
        

        $sql .=  " status = '".$status."',"
               . " created_by = '".$CreatedBy."',"
               . " modified_date = '".$modified_date."' "
               . "  where id = '".$newsId."' ";
       
       
        if ($_FILES['Images']['tmp_name'] != ""){
            
          $this->uploadImageforNews($newsId);
        }
        
         if ($_FILES['Newspdf']['tmp_name'] != ""){
            
          $this->uploadPdfforNews($newsId);
        }
        
        
        
       $update = $this->dbObj->update_query($sql);
       if ($update) {
            return true;
        } else {

            return false;
        }
             
       
   }
   
   // change status active to deactive and deactive to active
   public function changestatusofNews($newsId){
       
        $sql = 'SELECT status FROM `tsr_media` where id = "'.$newsId.'" ';
       
       $query = $this->dbObj->getAllData($sql);

       $status = $query[0]['status'];
       
       if ($status == 1){
           
           $NewStatus = 2;
        } else {
            
           $NewStatus = 1;  
        }
       

      
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $modified_date = date('Y-m-d H:i:s');
       
       $sql = "update tsr_media set "
            
               . " status = '".$NewStatus."',"
                . " created_by = '".$CreatedBy."',"
               . " modified_date = '".$modified_date."' "
               . "  where id = '".$newsId."' ";
       
       
       $update = $this->dbObj->update_query($sql);
      
       return true;
             
       
   }
   

}

// end class 
?>
