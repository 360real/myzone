<?php
error_reporting(-1);
ini_set('display_errors', 1);
if ($_SERVER['HTTP_HOST'] == '360cms.com') {

    include("/var/www/html/adminproapp/includes/set_main_conf.php");
} else {
    include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
}



if ($_SERVER['HTTP_HOST'] == '360cms.com') {

    $staticContentUrl = 'http://360cms.com';
} else {

    $staticContentUrl = 'https://static.360realtors.ws';
}



include_once(LIBRARY_PATH . "library/server_config_admin.php");
include_once(LIBRARY_PATH . "library/main_common_function.php");

include_once(LIBRARY_PATH . "media/model/Model.php");
$modelObj = new Model();
admin_login();


/* Including common Header */
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('Manage Media', $site['TITLE'], $site['URL']);
$page_name = C_ROOT_URL . "/media/controller/controller.php";

$action = $_REQUEST['action'];

$commonFunction = new main_common();
$checkPostdata = false;



switch ($action) {
    case 'Create':

        if (is_array($_POST) && count($_POST) > 0) {
            $insertIntoDb = $modelObj->insertIntoDb();
            if ($insertIntoDb) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Inserted successfully!");
                    window.location.href = "/media/index.php";
                </script>
                <?php

            }
        }
        include(ROOT_PATH . "/media/view/newsDetailPage.php");
        break;

    case 'Update':

        if (isset($_GET['newsId']) && $_GET['newsId'] != "") {


          $NewsId = $_GET['newsId'];

            $getdataforNewsarr = $modelObj->selectAllNews($NewsId);

            $getdataforNews = $getdataforNewsarr[0];
            
            
        }

        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {

            $updateData = $modelObj->updateNews($_GET['newsId']);


            if ($updateData) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Updated successfully!");
                    window.location.href = "/media/index.php";
                </script>
                <?php

            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';
            }
        }
        include(ROOT_PATH . "/media/view/newsDetailPage.php");
        break;

    case 'Delete':

        if (isset($_GET['newsId']) && $_GET['newsId'] != "") {
            $newsId = $_GET['newsId'];
            $changeStartus = $modelObj->changestatusofNews($newsId);


            if ($changeStartus) {
                ?>
                <script type="text/javascript">
                    alert("Status has been changed successfully!");
                    window.location.href = "/media/index.php";
                </script>
                <?php

            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';

                header('location: /media/index.php');
            }
        }
        break;

    default :

        $getDataForview = $modelObj->selectAllNews();

        include(ROOT_PATH . "media/view/view.php");
        break;
}
?>
