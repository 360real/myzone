<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script>
function richTextBox($fieldname, $value = "") {

   
   $CKEditor = new CKEditor();
   $config['toolbar'] = array(
    array( 'Bold', 'Italic', 'Underline', 'Strike'),
    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
    array('Format'),
    array( 'Font', 'FontSize', 'FontColor', 'TextColor'),
    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ),
    array('Image', 'Table', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'),
    array('Subscript','Superscript'),
    array( 'Link', 'Unlink' ),
    array('Source')
   );
   $CKEditor->basePath = '/ckeditor/';
   $CKEditor->config['width'] = 975;
   $CKEditor->config['height'] = 400;
   
   $CKEditor->editor($fieldname, $value, $config);
}
</script>
<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Blog Detail</h3></th></tr>
</thead>
<tbody>


<tr class="<?php echo($clsRow1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Author Name</strong></td>
  <td><?php 
    echo AjaxDropDownList(TABLE_AUTHORS,"auth_id","id","name",$_GET["auth_id"],"id",$site['CMSURL'],"SubauthDiv","SubauthLabelDiv")?>
</td>


<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Title</strong></td>
  <td><input name="title" type="text" title="title" lang='MUST' value="<?php echo($rec['title'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Content</strong></td>
  <td><textarea name="content" class="ckeditor"  id="content" rows="8" cols="70"><?php echo($rec['content'])?></textarea>  </td>
</tr>

  <tr class="<?php echo($clsRow1)?>">
    <td align="right" ><strong>Select Image</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['imgname']=='') {?> title="Select Image" <?php } ?> name="imgname" id="imgname" /></td>
          <td width="62%"><?php if($rec['imgname']):?>
            <br />
            <img src="<?php echo  ($site['ARTICLELOGOURL'].$rec['id']."/".$rec['imgname'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['imgname'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

    <tr class="<?php echo($clsRow1)?>">
    <td align="right" ><strong>Select Twitter card Image(800 x 418 px)</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['imgname_tc']=='') {?> title="Select Image" <?php } ?> name="imgname_tc" id="imgname_tc" /></td>
          <td width="62%"><?php if($rec['imgname_tc']):?>
            <br />
            <img src="<?php echo  ($site['ARTICLELOGOURL'].$rec['id']."/".$rec['imgname_tc'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name_tc" value="<?php echo ($rec['imgname_tc'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Tags</strong></td>
           <td><textarea  name="tags" id="tags" rows="3" cols="175" ><?php echo stripslashes($rec['tags'])?></textarea></td>
 </tr>

<!-- <tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000"></font>Video Name</strong></td>
  <td><textarea  name="video_name" id="video_name" rows="1" cols="175" ><?php echo stripslashes($rec['video_name'])?></textarea></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000"></font>Embed Video</strong></td>
  <td><textarea  name="embed_video" id="embed_video" rows="5" cols="175" ><?php echo stripslashes($rec['embed_video'])?></textarea></td>
</tr>

 -->
  <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
           <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
           <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
           <td><textarea  name="meta_description" id="meta_description" rows="5" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
 </tr>
<!--<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Ordering</strong></td>
           <td><textarea name="ordering" id="ordering" rows="1" cols="175" ><?php echo stripslashes($rec['ordering'])?></textarea></td>
 </tr>-->


<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
  
	</td>
</tr>
<!-- <?php 
  $dt = $rec['scheduledTime'];
  $dtArr = explode(' ',$dt);
  $scDate = $dtArr[0];
  $scTime = $dtArr[1];
?>
<tr  id='schDiv' class="<?php echo($clsRow1)?>"><td align="right" ><strong>Schedule Time</strong> </td>
  <td>
    <input type='date' value='<?php echo $scDate; ?>' name='schDate'>
    <input type='time' value='<?php echo $scTime; ?>' name='schTime'>
  </td>
</tr> -->

<tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
 

</tbody>
</table>
</form>
<script>

$(document).ready(function(){
  var checkedVal = $('input:radio[name="status"]:checked').val();

  if (checkedVal == 3) {
    $('#schDiv').show();
  } else {
    $('#schDiv').hide();
  }
  
});

</script>

<script type="text/javascript">
  $('input:radio[name="status"]').change(function(){

    var changedVal = $(this).val(); 

    if (changedVal == 3) {
      $('#schDiv').show();
    } else {
      $('#schDiv').hide();
    }

  });
</script>
