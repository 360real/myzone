<?php

require_once(ROOT_PATH . "library/mysql_function.php");


// this model for making all the query for event
class Model {

    private $dbObj;
    private $filesaveingpath;

    function __construct() {

        $this->dbObj = new mysql_function();

        if ($_SERVER['HTTP_HOST'] == 'myzone.360realtors.in') {

            $this->FIleSavingPath = ROOT_PATH;
        } else {

            $this->FIleSavingPath = "/var/www/html/myzone/media_images";
        }
    }

    // for inserting Data for news
    public function insertIntoDb() {
     
          $title = mysql_real_escape_string($_POST['newsTitle']);
       
          $imagePath = mysql_real_escape_string($_FILES['Images']['name']);
       
          $link = mysql_real_escape_string($_POST['newsLink']);
          $content = mysql_real_escape_string($_POST['content']);
          $status = mysql_real_escape_string($_POST['status']);
      
          $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
          $createdDate = date('Y-m-d H:i:s');
      

        $sql = "INSERT INTO `tsr_emailmarketing` ("
                . "`Title`,"
                . " `image_path`, "
                . " `content`,"
               . " `media_link`, "
                  . " `status`, "
                . "`created_by`, "
                . "`created_date`)"
              
                . "VALUES('" . $title . "', "
                . "'" . $imagePath . "',"
                . "'" . $content . "', "
                . "'" . $link . "',"
                . " '" . $status . "',"
                . " '" . $CreatedBy . "', "
                . "'" . $createdDate . "'"
             
                . "   )";

             
        $query = $this->dbObj->insert_query($sql);

        $newsId = $query;

        $this->uploadImageforNews($newsId);
        
        return $query;
    }

    // this method for upload all file in folder
    private function uploadImageforNews($newsId) {
     
        $_FILES['Images'];
       $Rootpathmain = $this->FIleSavingPath."/Emailmedia" ;
        
                        if (!is_dir($Rootpathmain)) {
                            mkdir($Rootpathmain);
                            chmod($Rootpathmain, 0777);
                        }
        
               $Rootpath = $Rootpathmain."/IMAGES" ;
        
                        if (!is_dir($Rootpath)) {
                            mkdir($Rootpath);
                            chmod($Rootpath, 0777);
                        }
                         
        
        $path = $Rootpath."/".$newsId;     
                
         if (!is_dir($path)) {
             
                            mkdir($path);
                            chmod($path, 0777);
                        }
              
                        
        $tmpname =  $_FILES['Images']['tmp_name'];
      
        move_uploaded_file($tmpname, $path . '/' . $_FILES['Images']['name']);
        
        
        
        
    }


 
    
   public function selectAllNews($newsId=""){
       
       
       $sql = 'SELECT id,Title,image_path,content,media_link,status FROM `tsr_emailmarketing`';
       
          if ($newsId != "") {
                   
          $sql .= " where id = '". mysql_real_escape_string($newsId) ."'";
          
               }
            
           $sql .=   ' ORDER by id DESC';
       
        
       $query = $this->dbObj->getAllData($sql);


        return $query;
       
   }
   
   
   public function updateNews($newsId){
       
        $title = mysql_real_escape_string($_POST['newsTitle']);
       
        $imagePath = mysql_real_escape_string($_FILES['Images']['name']);
        $link = mysql_real_escape_string($_POST['newsLink']);
        $status = mysql_real_escape_string($_POST['status']);
        $content = mysql_real_escape_string($_POST['content']);
      
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $modified_date = date('Y-m-d H:i:s');
       
       if($imagePath!=''){
       $sql = "update tsr_emailmarketing set "
               . " Title = '".$title."',"
               . " media_link = '".$link."',"
               . " content = '".$content."',"
               . " status = '".$status."',"
               . " image_path = '".$imagePath."',"
               . " created_by = '".$CreatedBy."',"
               . " modified_date = '".$modified_date."' "
               . "  where id = '".$newsId."' ";

        }
        else{
        $sql = "update tsr_emailmarketing set "
               . " Title = '".$title."',"
               . " media_link = '".$link."',"
               . " content = '".$content."',"
               . " status = '".$status."',"
               . " created_by = '".$CreatedBy."',"
               . " modified_date = '".$modified_date."' "
               . "  where id = '".$newsId."' ";          
        }
       
       
       $update = $this->dbObj->update_query($sql);
       $this->uploadImageforNews($newsId);
       if ($update) {
            return true;
        } else {

            return false;
        }
             
       
   }
   
   
   public function changestatusofNews($newsId){
       
        $sql = 'SELECT status FROM `tsr_emailmarketing` where id = "'.$newsId.'" ';
       
       $query = $this->dbObj->getAllData($sql);

       $status = $query[0]['status'];
       
       if ($status == 1){
           
           $NewStatus = 2;
        } else {
            
           $NewStatus = 1;  
        }
       

      
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $modified_date = date('Y-m-d H:i:s');
       
       $sql = "update tsr_emailmarketing set "
            
               . " status = '".$NewStatus."',"
                . " created_by = '".$CreatedBy."',"
               . " modified_date = '".$modified_date."' "
               . "  where id = '".$newsId."' ";
       
       
       $update = $this->dbObj->update_query($sql);
      
       return true;
             
       
   }
   

}

// end class 
?>
