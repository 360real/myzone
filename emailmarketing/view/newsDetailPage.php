<?php
include_once(LIBRARY_PATH . "emailmarketing/view/newsheader.php");

?>
<body>
    <form method="post" name="eventDetail" id="eventForm" onsubmit="validateForm(this);" enctype="multipart/form-data">
        <table width="100%" class="MainTable eventinfo">
            <thead>
                <tr>
            <h3><th align="left" colspan="2">News Details</th></h3>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="input-lbl">News Title<font color="#FF0000">*</font></td>
                    <td>
                        <input type="text" id="newsTitle" name="newsTitle" value="<?php echo $getdataforNews["Title"]; ?>" placeholder="News Title " style="width: 450px;" required="true">
                    </td>
                </tr>
                <tr> 
                    <td class="input-lbl">Images <font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="Images" id="images" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
<?php if ($action == "Create") { ?>      
                                   required = 'required';
                        <?php } else { ?>

                                   
<?php } ?>
                               size-allowed="900"><br/>
                        (Size Less then 900kb)

                        <div id="imagesExists">

<?php if ($getdataforNews['image_path'] != "") { ?>


                                <div class="filediv"> 
                                    <div style="float:left;">
                                        <img src="<?php echo $staticContentUrl . '/Emailmedia/IMAGES/' . $getdataforNews['id'] . "/" . $getdataforNews['image_path']; ?>" width="75px;" height="75px;" title="<?php echo $getdataforNews['image_path']; ?>">
                                    </div>

                                </div>

<?php }
?>

                        </div>
                    </td>
                </tr> 

                <tr> 
                    <td class="input-lbl">Content</td>
                    <td >
                        <textarea name="content" id="content" rows="5" cols="63"><?php echo $getdataforNews["content"]; ?></textarea>
                    </td>
                </tr> 
                <tr>
                    <td class="input-lbl">News Link</td>
                    <td><input type="text" name="newsLink" value="<?php echo $getdataforNews["media_link"]; ?>" placeholder="News Link " style="width: 450px;" ></td>
                </tr>
                <tr>
                    <td class="input-lbl">Status<font color="#FF0000">*</font></td>
                    <td>
                        <input type="radio" name="status" value="1"  <?php if ($getdataforNews["status"] == "1") { ?> checked <?php } ?> required="true">Active
                        <input type="radio" name="status" value="2"  <?php if ($getdataforNews["status"] == "2") { ?> checked <?php } ?> required="true">Inactive

                    </td>
                </tr>

                <tr>
                    <td> </td>
                    <td> 
                        <input type="submit" name="submit" class="addmorebtn" 
                               value="<?php  if ($action == "Create") { ?>Sumbit<?php  } else {?>Update<?php } ?>">
                        <?php  if ($action == "Create") { ?>  
                        <input type="reset" id="reset" name="reset" class="addmorebtn" >
                        <?php } ?>
                    </td>
                </tr>
        </table>

    </form>
</body>
</html>