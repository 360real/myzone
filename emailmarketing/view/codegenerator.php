<?php
include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
admin_login();

$vals=($_POST['op']);
$options = implode(",",$vals);

$sql1="SELECT * FROM tsr_emailmarketing where id IN(".$options.") and status=1 ORDER BY id DESC";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

$sql2="SELECT imgname_mailer as imgname_mailer FROM tsr_emailmarketingimages";
$bannerpath=($rec_id ? $obj_mysql->get_assoc_arr($sql2) : $obj_mysql->getAllData($sql2));
$campaignName = date('M-Y'); 

$msg="";
$msg.='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>360realtors</title>
</head>
<body>
<img src="https://www.google-analytics.com/collect?v=1&tid=UA-134723344-1&t=event&ec=email&ea=open&cs=newsletter&cm=email&cn='.$campaignName.'">
<table style="max-width:818px; border:1px solid #ebebeb;" align="center" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="816" bgcolor="#ffffff" align="center" valign="top">
    <table style="max-width:816px;" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="816" height="20" align="center">
            <img src="'.C_ROOT_URL.'media_images/emailmarketingbanner/'.$bannerpath[0]['imgname_mailer'].'" width="100%" alt="Monthly Newsletter" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;" /></td>
        </tr>
        <tr>
          <td align="center" height="80" style="border-bottom:#773996 solid 4px;">
            <img src="https://www.360realtors.com/events/dec-2018/06-dec-2018/images/banner.jpg" width="100%" alt="" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;" /></td>
        </tr>
        <tr>
          <td align="center" height="50"></td>
        </tr>
        <tr>
          <td align="center" height="35" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#333333; line-height:25px;">
          <strong>
            Real Estate Intelligence
          </strong></td>
        </tr>
        <tr>
          <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333;">
            Get access to all Real Estate news and views, directly in your mailbox.
          </td>
        </tr>
        <tr>
          <td align="center" height="50"></td>
        </tr>
        <tr>
          <td align="center">
            <table width="90%" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">';


 foreach($rec as $newsData)
{
  $heading=$newsData['Title'];
  $content=$newsData['content'];
  $link=$newsData['media_link'];
              $msg .='<tr>
                <td>
                  <table border="0" cellspacing="0" cellpadding="0" align="left" width="280" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                      <tr>
                        <td height="10" colspan="3"></td>
                      </tr>
                      <tr>
                        <td width="5"></td>
                        <td style="border:#333133 solid 1px;" height="128">
                        <a href="'.$link.'" target="_blank" style="color:inherit; text-decoration:none;">
                          <img src="'.C_ROOT_URL.'Emailmedia/IMAGES/'.$newsData['id'].'/'.$newsData['image_path'].'" width="270" height="128" alt="" /></a>
                        </td>
                        <td width="15"></td>
                      </tr>
                      <tr>
                        <td height="10" colspan="3"></td>
                      </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" align="left" style="max-width:300px; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                      <tr>
                        <td height="5" colspan="3"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        
                        <td  align="left" height="30" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#000000; line-height:25px;"><strong><a href="'.$link.'" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#000000; line-height:25px; text-decoration:none;">'.$heading.'</a></strong></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        
                        <td  style=" max-width:300px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; line-height:25px; text-align:left;"><a href="'.$link.'" target="_blank" style=" max-width:300px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333333; line-height:25px; text-decoration:none; text-align:left">'.$content.'</a></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td height="10" colspan="3"></td>
                      </tr>
                      <tr>
                        <td></td>
                        <td align="left">
                          <table width="100" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td height="24" align="center" bgcolor="#783a97">
                                <a href="'.$link.'" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#ffffff;text-decoration:none; line-height:25px; font-weight:bold; width:100%;">Read More...</a></td>
                              </tr>
                            </table>
                        </td>
                        <td></td>
                      </tr>
                      
                    </table>
            </td>
              </tr>';

}
            $msg .='</table>
          </td>
        </tr>
        <tr>
          <td height="40"></td>
        </tr>
        <tr>
        </tr>
        
      </table>
      </td>
  </tr>
</table>';

$msg .= '<div style="max-width:818px;margin:0 auto;background: url(https://res.cloudinary.com/mirus/image/upload/v1508339603/m1_web_273993872_wftp9s.jpg);background-size:cover;">
<div style="padding: 10px 43px 10px 43px;background: #414143e8;background-size: cover;min-height: 58px;">
     <div style="width: 50%;float: left;">
       <ul style="list-style: none;margin-left: 0;padding-left: 0;">
          <li style="display: inline;">
          <a href="https://www.facebook.com/360Realtors" target="_blank" style="color:inherit; text-decoration:none;">
          <img src="https://www.360realtors.com/events/dec-2018/06-dec-2018/images/fb-icon.jpg" width="30" height="30" alt="FB" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#ffffff;" /></a>
          </li>
          <li style="display: inline;">
          <a href="https://twitter.com/360realtors" target="_blank" style="color:inherit; text-decoration:none;">
          <img src="https://www.360realtors.com/events/dec-2018/06-dec-2018/images/tw-icon.jpg" width="30" height="30" alt="TW" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#ffffff;"/></a>
          </li>

          <li style="display: inline;">
          <a href="https://www.linkedin.com/company/360-realtors-llp" target="_blank" style="color:inherit; text-decoration:none;">
          <img src="https://www.360realtors.com/events/dec-2018/06-dec-2018/images/in-icon.jpg" width="30" height="30" alt="IN" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#ffffff;" /></a>
          </li>
       </ul>
     </div>
     <div style="width: 50%;float: left;text-align: right;line-height: 62px;">

      <a href="https://www.360realtors.com" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#ffffff; line-height:25px; text-decoration:none;"><strong>www.360realtors.com</strong></a> 
      <span style="color:#ffffff;"> | </span>
      <a href="tel:18001200360" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; line-height:25px; text-decoration:none; width: 122px;"><strong style="font-size: 16px;">1800 1200 360</strong></a>
       
     </div>
</div> 
</div>
</body>';

echo $msg;

?>
