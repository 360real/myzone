<?php

include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");


admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_TESTIMONIAL;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/testimonial/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."testimonial/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."testimonial/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT testi.id, testi.name, testi.designation, testi.country_id,testi.company,testi.cty_id,testi.type,testi.ordering,testi.home_page_ordering,testi.imgname,testi.testimonial, testi.status, testi.modified_date, testi.created_date, testi.ipaddress FROM ".TABLE_TESTIMONIAL." testi WHERE ";
		if($_GET["testi_id"]!="")
			$sql.="testi.id=".$_GET["testi_id"];
		if($_GET["name"]!="")
			$sql.=" testi.name like '%".trim($_GET["name"])."%'";
		if($_GET["status"]!="")
			$sql.="testi.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(testi.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(testi.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(testi.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(testi.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT testi.id, testi.name, testi.designation, testi.country_id,testi.company,testi.cty_id,testi.type,testi.ordering,testi.home_page_ordering,testi.imgname,testi.testimonial, testi.status, testi.modified_date, testi.created_date, testi.ipaddress FROM ".TABLE_TESTIMONIAL." testi";
		
		$sql.= " WHERE testi.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
		$sql = "SELECT testi.id, testi.name, testi.designation, testi.country_id,testi.company,testi.cty_id,testi.type,testi.ordering,testi.home_page_ordering,testi.imgname,testi.testimonial, testi.status, testi.modified_date, testi.created_date, testi.ipaddress FROM ".TABLE_TESTIMONIAL." testi";
		
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/

	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";

			if($arr['pd']=="1")
			$arr['author_modified_date'] = 'now()';

			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];
			//echo $_FILES['image']['name'];
			if($obj_mysql->isDupUpdate($tblName,'name', $arr['name'] ,'id',$rec_id))
			{
			
			if($_FILES['imgname']['name']!="" && $counterRename!="1")
							{   
								echo $_FILES['imgname']['name'];
								@unlink(UPLOAD_PATH_TESTIMONIAL_IMAGES.$rec_id."/".$_POST['old_file_name']);
								$handle = new upload($_FILES['imgname']);
								if ($handle->uploaded)
									{	
										/*$handle->image_resize         = true;
										$handle->image_x              = 200;
										$handle->image_y              = 87;
										$handle->image_ratio_y        = true;*/
										$handle->file_safe_name = true;
										$handle->process(UPLOAD_PATH_TESTIMONIAL_IMAGES.$rec_id."/");
										if ($handle->processed) 
										{
											//echo 'image resized';
											$arr['imgname'] = $handle->file_dst_name;
											$handle->clean();
				   						} 
				   						else 
				   						{
										   //echo "error";
											echo 'error : ' . $handle->error;
										}
							}
				}
			
				
				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				$obj_common->redirect($page_name."?msg=update");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}else{
						$arr['created_date'] = "now()";

			if($arr['pd']=="1")
			$arr['author_modified_date'] = 'now()';

			$arr['ipaddress'] = $regn_ip;
			$arr['created_by'] = $_SESSION['login']['id'];
		
			if($obj_mysql->isDuplicate($tblName,'name', $arr['name']))
			{	

				$setmaxid=$obj_mysql->insert_data($tblName,$arr);
				if($_FILES['imgname']['name']!="")
				{
					echo $_FILES['imgname']['name'];
					$handle = new upload($_FILES['imgname']);
					//echo $_FILES['imgname'];
					if ($handle->uploaded)
					{
						/*$handle->image_resize         = true;
						$handle->image_x              = 200;
						$handle->image_y              = 87;
						$handle->image_ratio_y        = true;*/
						$handle->file_safe_name = true;
						$handle->process(UPLOAD_PATH_TESTIMONIAL_IMAGES.$setmaxid."/");
						if ($handle->processed) 
						{
							//echo 'image resized';
							$arr['imgname'] = $handle->file_dst_name;
							@unlink(UPLOAD_PATH_TESTIMONIAL_IMAGES.$setmaxid."/".$_POST['old_file_name']);
							$handle->clean();
						} 
						else 
						{
							echo 'error : ' . $handle->error;
						}
					}
					$obj_mysql->update_data($tblName,$fld_id,$arr,$setmaxid);

				}


					$obj_common->redirect($page_name."?msg=insert");

				//$obj_mysql->insert_data($tblName,$arr);
				//$obj_common->redirect($page_name."?msg=insert");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td class="center"><a href="'.$page_name.'?action=Update&state_id='.$rec[$i]['state_id'].'&country_id='.$rec[$i]['country_id'].'&id='.$rec[$i]['id'].'" target="_blank" class="title_a">'.$rec[$i]['name'].'</a></td>
					<td class="center">
					<img src="'.$site['TESTIMONIALIMAGEURL'].$rec[$i]['id']."/".$rec[$i]['imgname'].'" border="0" width="93" height="63" /></td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td class="center">'.$st.'</td>
					<td><a href="'.$page_name.'?action=Update&state_id='.$rec[$i]['state_id'].'&country_id='.$rec[$i]['country_id'].'&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>';
					if($_SESSION['login']['id']=='1'){
					$list.='<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>';
					}
					$list.='</td>
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Testimonial', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
