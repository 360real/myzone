<form  method="post"  name="form123"  enctype="multipart/form-data" onsubmit="return validateForm(this);">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Testimonial Manager</h3></th></tr>
</thead>
<tbody>
<input type="hidden" name="type" value="CMS" title="CMS">
<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Name</strong></td>
  <td><input type="text" lang='MUST' name="name" title="name" maxlength="120" value="<?php echo($rec['name'])?>" size="40" /></td>
</tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Designation</strong></td>
  <td><input type="text" lang='MUST' name="designation" title="designation" maxlength="120" value="<?php echo($rec['designation'])?>" size="40" /></td>
</tr>

<tr class="<?php echo($cls1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Country</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_COUNTRY,"country_id","id","country",$rec["country_id"],"id",$site['CMSURL'],"countryDiv","countryLabelppDiv")?></td>
</tr>

<tr class="<?php echo($cls1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>City</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_CITIES,"cty_id","id","city",$rec["cty_id"],"id",$site['CMSURL'],"cityDiv","cityLabelppDiv")?></td>
</tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Company</strong></td>
  <td><input type="text" lang='MUST' name="company" title="company" maxlength="120" value="<?php echo($rec['company'])?>" size="40" /></td>
</tr>

<tr class="<?php echo($cls1)?>">
    <td align="right" ><strong>Select Image for testimonial</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['imgname']=='') {?> title="Select Image for testimonial" <?php } ?> name="imgname" id="imgname" /></td>
          <td width="62%"><?php if($rec['imgname']):?>
            <br />
            <img src="<?php echo  ($site['TESTIMONIALIMAGEURL'].$rec['id']."/".$rec['imgname'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['imgname'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

<tr class="<?php echo($cls2)?>">
  <td align="right" ><strong>Testimonial</strong></td>
  <td><textarea name="testimonial" lang='MUST'   id="testimonial" rows="8" cols="70"><?php echo($rec['testimonial'])?></textarea>  </td>
</tr>

<tr class="<?php echo($cls1)?>">
        <td width="15%" align="right"><strong>Ordering</strong></td>
           <td><textarea name="ordering" id="ordering" rows="1" cols="175" ><?php echo stripslashes($rec['ordering'])?></textarea></td>
 </tr>

 <tr class="<?php echo($cls1)?>">
        <td width="15%" align="right"><strong>Home Page Ordering</strong></td>
           <td><textarea name="home_page_ordering" id="home_page_ordering" rows="1" cols="175" ><?php echo stripslashes($rec['home_page_ordering'])?></textarea></td>
 </tr>

<tr class="<?php echo($cls1)?>"><td align="right" ><strong>Status</strong> </td>
  <td>
  <input type="radio" name="status" value="1" id="1" checked="checked" /> 
  <label for="1">Active</label> 
  <input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
  <label for="0">Inactive</label>  </td></tr>

<tr class="<?php echo($cls1)?>"><td align="center">&nbsp;</td>
  <td align="left"><input name="categorysubmit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>