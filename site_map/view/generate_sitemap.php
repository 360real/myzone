<?php
include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$commonSavePath=P_ROOT_URL."site_map/allSiteMaps";
$commonOpenPath=S_ROOT_URL."site_map/allSiteMaps";
$templateId=$_POST['templateId'];


if($templateId==8)
{
  $sql1 = "SELECT pro.country_id,pro.new_amenities,pro.location_map, pro.payment_plan, pro.highlights,
  ptype.type as propertyType, pro.type_id as typeId, pctype.type_category as propertyTypeCategory,city.city as cityName,
  dev.id as developerId, dev.name as developer,dev.description,dev.logo as developerLogo, 
  pro.possession_date,pro.new_overview,pro.property_name, pro.property_name_display, pro.id as propertyId,
  pro.booking_status,pro.bedrooms,loc.location,subloc.cluster as sublocation FROM tsr_properties  as pro INNER JOIN tsr_types as ptype ON pro.type_id=ptype.id INNER JOIN 
  tsr_types_categories as pctype ON pro.type_id=pctype.id INNER JOIN tsr_locations loc ON pro.loc_id=loc.id 
  INNER JOIN tsr_cluster subloc ON pro.cluster_id=subloc.id INNER JOIN tsr_cities as city ON pro.cty_id=city.id 
  INNER JOIN tsr_developers dev ON pro.devlpr_id=dev.id and dev.status=1 and loc.status=1 and loc.status=1 and pro.status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

  for($i=0;$i<=$count-1; $i++) {
  $propertyName = $rec[$i]["property_name"];
  $developerName = $rec[$i]["developer"];
  $subLocationName = $rec[$i]["sublocation"];
  $cityName = $rec[$i]["cityName"];
  $propertyId = $rec[$i]["propertyId"];
  $developerName = str_replace("&","",$developerName);
  $propertyName = strtolower($propertyName);
  $developerName = strtolower($developerName);
  $subLocationName = strtolower($subLocationName);
  $cityName = strtolower($cityName);

  $propertyName = preg_replace('/[^A-Za-z0-9\-]/', ' ', $propertyName); // Removes special chars.
  $propertyName = str_replace($developerName,"",$propertyName);
  $propertyName = ($developerName." ".$propertyName);

  $setSefUrl = str_replace(".","",str_replace("--","-",str_replace(" ","-",strtolower($propertyName))))."-".str_replace(" ","-",strtolower($subLocationName))."-".strtolower($cityName)."-uid-".$propertyId;
  $setMainSefrl = "https://www.360realtors.com/".preg_replace('/-+/', '-', $setSefUrl); 


  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>daily</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  //echo $;
  $dom->save($commonSavePath."/sitemapProperties.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapProperties.xml'."' download>Download File</a>";

}
else if($templateId==7)
{
    $sql1 = "SELECT city from tsr_cities where status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {
 
  $cityName     = $rec[$i]["city"];
  $cityName     = strtolower($cityName);
  $cityName     = preg_replace('/[^A-Za-z0-9\-]/', ' ', $cityName); // Removes special chars.
  $cityName     = str_replace(" ", "-", $cityName);
  $setSefUrl    = "property-in-".$cityName;
  $setMainSefrl = "https://www.360realtors.com/".preg_replace('/-+/', '-', $setSefUrl); 


  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapcity.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapcity.xml'."' download>Download File</a>";

}
else if($templateId==27)
{
    $sql1 = "SELECT loc.id, loc.meta_title,loc.meta_keywords,loc.meta_description, loc.location,cty.id as cityId, cty.city as cityName FROM tsr_locations loc INNER JOIN tsr_cities cty ON loc.cty_id=cty.id WHERE loc.status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {
 
$locationName=pregMatchCheck($rec[$i]["location"]);
      $cityName=pregMatchCheck($rec[$i]["cityName"]);
      $locationId=$rec[$i]["id"];
      if(strrpos($locationName, $cityName)!==false)
      {  
         $setMainSefrl="https://www.360realtors.com/property-in-".$locationName."-locid-".$locationId; 
          
       }
      else
      {
         $setMainSefrl="https://www.360realtors.com/property-in-".$locationName."-".$cityName."-locid-".$locationId;
         
       } 
  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapLocation.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapLocation.xml'."' download>Download File</a>";
}

else if($templateId==29)
{
    $sql1 = "SELECT `tsr_blog`.`id`, `tsr_blog`.`auth_id`, `tsr_blog`.`cat_id`, `tsr_blog`.`propty_id`, `tsr_blog`.`devlpr_id`, `tsr_blog`.`title`, `tsr_blog`.`sef_url`, `tsr_blog`.`content`, `tsr_blog`.`imgname`, `tsr_blog`.`video_name`, `tsr_blog`.`embed_video`, `tsr_blog`.`meta_title`, `tsr_blog`.`meta_keywords`, `tsr_blog`.`meta_description`, `tsr_blog`.`tags`, `tsr_blog`.`status`, `tsr_blog`.`copyscape_status`, `tsr_blog`.`created_date`, `tsr_blog_sections`.`name` as `section_name`, `tsr_article_authors`.`name` as `authname` FROM `tsr_blog` JOIN `tsr_blog_sections` ON `tsr_blog`.`sec_id`=`tsr_blog_sections`.`id` JOIN `tsr_article_authors` ON `tsr_blog`.`auth_id`=`tsr_article_authors`.`id` WHERE `tsr_blog`.`status` = 1 ORDER BY `tsr_blog`.`created_date` ASC ";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
  <url>
  <loc>https://www.360realtors.com/blog</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.8000</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/section/property-insights</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/section/tips-and-trends</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/section/expert-advice</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/section/lifestyle</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/section/lifestyle</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/indian-real-estate</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/real-estate-trends</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/nri</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/delhi-ncr</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/tips</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/holiday-home</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/trends</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/blog/category/expert-tips</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
  </url>';
    for($i=0;$i<=$count-1; $i++) {
 
  $blogTitle = $rec[$i]["title"];
  $blogTitle = strtolower($blogTitle);
  $blogId = $rec[$i]["id"];

  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $blogTitle);
  $name = trim($name);
  $blogname = str_replace("--","-",str_replace(" ","-",$name));
  $blogname=preg_replace('/-+/', '-', strtolower($blogname)); 

  $setMainSefrl = "https://www.360realtors.com/blog/post/".$blogname."-blid".$blogId; 


  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapblog.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapblog.xml'."' download>Download File</a>";
}

else if($templateId==26)
{
    $sql1 = "SELECT * from tsr_developers where status=1 order by created_date ASC ";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {
 
  $builderName = $rec[$i]["name"];
  $builderName = strtolower($builderName);
  $builderId = $rec[$i]["id"];

  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $builderName);
   $name = trim($name);
   $name = str_replace("--","-",str_replace(" ","-",$name));
   $name=preg_replace('/-+/', '-', strtolower($name)); 

  $setMainSefrl = "https://www.360realtors.com/".$name."-new-projects-blid-".$builderId; 


  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapbuilder.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapbuilder.xml'."' download>Download File</a>";
}

else if($templateId==28)
{
    $sql1 = "SELECT loc.id as locationId, loc.location as locationName, city.id as cityId, city.city as cityName, subloc.cluster as sublocationName, subloc.id as sublocationId FROM tsr_cluster subloc INNER JOIN tsr_locations loc ON subloc.loc_id=loc.id INNER JOIN tsr_cities as city ON loc.cty_id=city.id WHERE subloc.status=1 ";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {

$sublocationName=pregMatchCheck($rec[$i]["sublocationName"]);
   $cityName=pregMatchCheck($rec[$i]["cityName"]);
   $locationName=pregMatchCheck($rec[$i]["locationName"]);
   $subLocationId=$rec[$i]["sublocationId"];

   if(strrpos($sublocationName, $cityName)!==false)
      {  
         $setMainSefrl = "https://www.360realtors.com/property-in-".$sublocationName."-clid-".$subLocationId;
          
       }
      else
      {
         $setMainSefrl = "https://www.360realtors.com/property-in-".$sublocationName."-".$cityName."-clid-".$subLocationId;
         
       } 

  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapSubLocation.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapSubLocation.xml'."' download>Download File</a>";
}

else if($templateId==10)
{
    $sql1 = "SELECT city from tsr_cities where status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {
 
  $cityName = $rec[$i]["city"];
  $cityName = strtolower($cityName);
  $cityName = preg_replace('/[^A-Za-z0-9\-]/', ' ', $cityName); // Removes special chars.
  $setSefUrl = "residential-projects-in-".$cityName;
  $setSefUrl1 = "commercial-projects-in-".$cityName;
  $setMainSefrl = "https://www.360realtors.com/".preg_replace('/-+/', '-', $setSefUrl); 
  $setMainSefrl1 = "https://www.360realtors.com/".preg_replace('/-+/', '-', $setSefUrl1);


  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';

  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl1.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';


  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapStatusType.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapStatusType.xml'."' download>Download File</a>";
}

else if($templateId==9)
{
    $sql1 = "SELECT cty.city, cat.id, cat.type_category FROM tsr_types_categories cat INNER JOIN tsr_properties pro ON pro.cat_id=cat.id INNER JOIN tsr_cities cty ON cty.id=pro.cty_id group by cat.id,cty.city,cat.type_id,pro.cty_id";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));
//print_r($rec);die;
  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {
 
  $cityName = $rec[$i]["city"];
  $id = $rec[$i]["id"];
  $typeCategory = $rec[$i]["type_category"];
  $categoryUrlNameName=pregMatchCheck($typeCategory);
  $cityName=pregMatchCheck($cityName);
  $setMainSefrl="https://www.360realtors.com/".$categoryUrlNameName."-in-".$cityName."-ctid-".$id;

  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';

  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapCategoryType.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapCategoryType.xml'."' download>Download File</a>";
}


else if($templateId==30)
{
    $sql1 = "SELECT id, name FROM tsr_project_status WHERE status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));
//print_r($rec);
  $count=count($rec);

  $sql2 = "SELECT city from tsr_cities where status=1";
  //print_r($sql1);die;
  $rec2=($rec_id ? $obj_mysql->get_assoc_arr($sql2) : $obj_mysql->getAllData($sql2));
//print_r($rec2);die;
  $count2=count($rec2);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
   
  foreach ($rec as $projectStatus) {
    foreach ($rec2 as $cities) {
     $cityName = $cities["city"];
  $id = $projectStatus["id"];
  $typeStatus = $projectStatus["name"];
  $categoryUrlNameName=pregMatchCheck($typeStatus);
$cityName=pregMatchCheck($cityName);

$setMainSefrl="https://www.360realtors.com/".$categoryUrlNameName."-in-".$cityName."-pstid-".$id;

  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= '</url>';
}
  
}


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapProjectStatusWise.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapProjectStatusWise.xml'."' download>Download File</a>";
}

else if($templateId==31)
{
    $sql1 = "SELECT * from tsr_specials where status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));
//print_r($rec);die;
  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {
 
  $specialTitle = $rec[$i]["title"];
  $id = $rec[$i]["id"];
  $title=pregMatchCheck($specialTitle);
  $setMainSefrl="https://www.360realtors.com/specials/".$title."-sid-".$id;
  //$setMainSefrl="https://www.360realtors.com/".$categoryUrlNameName."-in-".$cityName."-ctid-".$id;

  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';

  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapSpecials.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapSpecials.xml'."' download>Download File</a>";
}

else if($templateId==32)
{
    $sql1 = "SELECT city from tsr_cities where status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    for($i=0;$i<=$count-1; $i++) {
 
  $cityName = $rec[$i]["city"];
  $cityName = strtolower($cityName);
  $cityName = preg_replace('/[^A-Za-z0-9\-]/', ' ', $cityName); // Removes special chars.
  $setSefUrl = "top-real-estate-developers-in-".$cityName;
  $setMainSefrl = "https://www.360realtors.com/".preg_replace('/-+/', '-', $setSefUrl); 


  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapCityWiseDevelopers.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapCityWiseDevelopers.xml'."' download>Download File</a>";
}

else if($templateId==15)
{
    $sql1 = "SELECT city from tsr_cities where status=1";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
  <url>
  <loc>https://www.360realtors.com/nri</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
   </url>
   ';
    for($i=0;$i<=$count-1; $i++) {
 
  $cityName = $rec[$i]["city"];
  $cityName = strtolower($cityName);
  $cityName = preg_replace('/[^A-Za-z0-9\-]/', ' ', $cityName); // Removes special chars.
  $setSefUrl = "nri/property-in-".$cityName;
  $setMainSefrl = "https://www.360realtors.com/".preg_replace('/-+/', '-', $setSefUrl); 


  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapNri.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapNri.xml'."' download>Download File</a>";
}
else if($templateId==33)
{
    $sql1 = "SELECT * from tsr_web_news where status=1 and is_hindi=0";
  //print_r($sql1);die;
  $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

  $count=count($rec);

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
  <url>
  <loc>https://www.360realtors.com/news</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>weekly</changefreq>
      <priority>0.4096</priority>
   </url>
   ';
    for($i=0;$i<=$count-1; $i++) {
        $newsTitle = $rec[$i]["title"];
      $newsId=$rec[$i]["id"];

  $name = str_replace(' ', '-', $newsTitle);
    $name = trim($name);
    $setMainSefrl = "https://www.360realtors.com/news/".$name."-nid".$newsId; 

  if($i==0)
  $output .= '  <url>';
  else
  $output .= '
  <url>';
  $output .= '
  <loc>'.$setMainSefrl.'</loc>
  ';
  $output .= '  <lastmod>'.date("c").'</lastmod>
  ';
  $output .= '    <changefreq>weekly</changefreq>
  ';
  $output .= '    <priority>0.4096</priority>
  ';
  $output .= ' </url>';
  }


  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapNews.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapNews.xml'."' download>Download File</a>";
}
else if($templateId==34)
{

  $output='<?xml version="1.0" encoding="UTF-8"?>

  <urlset
  xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xsi:schemaLocation="
  http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
  <url>
       <loc>https://www.360realtors.com/</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>1.0000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/career</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/faq</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/disclaimer</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/real-estate-consulting</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/legal-consultation</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>daily</changefreq>
      <priority>0.4096</priority>
  </url>
  <url>
  <loc>https://www.360realtors.com/home-loan-consulting</loc>
    <lastmod>'.date("c").'</lastmod>
      <changefreq>daily</changefreq>
      <priority>0.8000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/after-sales-assistance</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/about-us</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/contact-us</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>
  <url>
       <loc>https://www.360realtors.com/privacy-policy</loc>
       <lastmod>'.date("c").'</lastmod>
       <changefreq>daily</changefreq>
       <priority>0.8000</priority>
  </url>';
    
  $output .= '
  </urlset>';
  //echo $output;die; 
  $dom = new DOMDocument;
  $dom->preserveWhiteSpace = FALSE;
  $dom->loadXML($output);
  
  $dom->save($commonSavePath."/sitemapStaticPages.xml");

  echo "<a style='background: #eee none repeat scroll 0 0;
    border: 1px solid;
    border-radius: 5px;
    color: #000;
    font-size: 13px;
    padding: 4px;text-decoration: none;' href='".$commonOpenPath.'/sitemapStaticPages.xml'."' download>Download File</a>";
}

?>