<?php 
include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_WEBSITE_PPC_CAMPAIGN;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/website_ppc_campaign/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."website_ppc_campaign/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."website_ppc_campaign/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT tem.template_name, camp.id, camp.template_id, camp.relation_id, camp.campaign_id, camp.campaign_type, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM ".TABLE_WEBSITE_PPC_CAMPAIGN." as camp INNER JOIN tsr_templates tem ON camp.template_id=tem.id WHERE ";
		if($_GET["template_id"]!="")
			$sql.="camp.template_id=".$_GET["template_id"];

		if($_GET["campaign_id"]!="")
			$sql.=" camp.campaign_id like '%".trim($_GET["campaign_id"])."%'";
		if($_GET["status"]!="")
			$sql.="camp.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(camp.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(camp.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(camp.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(camp.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT camp.id, camp.template_id, camp.relation_id, camp.campaign_id, camp.campaign_type, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM ".TABLE_WEBSITE_PPC_CAMPAIGN." as camp ";
		
		$sql.= " WHERE camp.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	/*if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	*/

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
		$sql = "SELECT tem.template_name, camp.id, camp.template_id, camp.relation_id, camp.campaign_id, camp.campaign_type, camp.start_date, camp.end_date, camp.status, camp.created_date,camp.ipaddress, camp.modified_date FROM ".TABLE_WEBSITE_PPC_CAMPAIGN." as camp INNER JOIN tsr_templates tem ON camp.template_id=tem.id";
		
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/
	
	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr["start_date"] = date('Y-m-d', strtotime($_POST["start_date"]));
			$arr["end_date"] = date('Y-m-d', strtotime($_POST["end_date"]));

			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];


				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				$obj_common->redirect($page_name."?msg=update");	
				
		}else{


			$arr["start_date"] = date('Y-m-d', strtotime($_POST["start_date"]));
			$arr["end_date"] = date('Y-m-d', strtotime($_POST["end_date"]));

			$arr['created_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['created_by'] = $_SESSION['login']['id'];

				$obj_mysql->insert_data($tblName,$arr);
				$obj_common->redirect($page_name."?msg=insert");	
				
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}

		if($rec[$i]['template_id']==7)
		{
$sql = mysql_query("select city from tsr_cities where id=".$rec[$i]['relation_id']);
$recCity = mysql_fetch_array($sql);
$relationName = $recCity['city'];
$liveUrl = "http://www.360realtors.in/property-in-".strtolower($relationName)."?utm_source=ppc&ctid=".$rec[$i]['campaign_id']."&dd=".date("dmY", strtotime($rec[$i]['created_date']))."&st=true&ucpt=addshoot";
		} else if ($rec[$i]['template_id']==11)
		{
$liveUrl = "http://www.360realtors.in/?utm_source=ppc&ctid=".$rec[$i]['campaign_id']."&dd=".date("dmY", strtotime($rec[$i]['created_date']))."&st=true&ucpt=addshoot&utm_medium=ppc-website&utm_campaign=".$rec[$i]['campaign_id']."";
$relationName = "";
		} else if($rec[$i]['template_id']==8 && $rec[$i]['campaign_type']=='Facebook'){
$sql = mysql_query("select property_name from tsr_properties where id=".$rec[$i]['relation_id']);
$recCity = mysql_fetch_array($sql);
$relationName = $recCity['property_name'];
		$liveUrl = "http://www.360realtors.in/{sef_url}?utm_source=fb-property&ctid=".$rec[$i]['campaign_id']."&dd=".date("dmY", strtotime($rec[$i]['created_date']))."&st=true&ucpt=addshoot&utm_medium=fb-website&utm_campaign=".$rec[$i]['campaign_id']."";
		} else if($rec[$i]['template_id']==8 && $rec[$i]['campaign_type']!='Facebook'){
$sql = mysql_query("select property_name from tsr_properties where id=".$rec[$i]['relation_id']);
$recCity = mysql_fetch_array($sql);
$relationName = $recCity['property_name'];
		$liveUrl = "http://www.360realtors.in/{sef_url}?utm_source=ppc&ctid=".$rec[$i]['campaign_id']."&dd=".date("dmY", strtotime($rec[$i]['created_date']))."&st=true&ucpt=addshoot&utm_medium=ppc-website&utm_campaign=".$rec[$i]['campaign_id']."";
		} else if($rec[$i]['template_id']==13 && $rec[$i]['campaign_type']=='Emailer'){
$relationName = 'Emailer';
		$liveUrl = "submit.php/?utm_source=emailer&ctid=".$rec[$i]['campaign_id']."&dd=".date("dmY", strtotime($rec[$i]['created_date']))."&st=true&ucpt=addshoot&utm_medium=emailer&utm_campaign=".$rec[$i]['campaign_id']."";
		}



		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'&template_id='.$rec[$i]['template_id'].'&relation_id='.$rec[$i]['relation_id'].'" target="_blank" class="title_a">'.$rec[$i]['template_name'].'</a></td>
					<td class="center">'.$relationName.'</td>
					<td class="center">'.$rec[$i]['campaign_id'].'</td>
					<td class="center">'.$liveUrl.'</td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$st.'</td>
					<td><a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'&template_id='.$rec[$i]['template_id'].'&relation_id='.$rec[$i]['relation_id'].'" target="_blank" class="edit">Edit</a>
					<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>
					</td>
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Types', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>