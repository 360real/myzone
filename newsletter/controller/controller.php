<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
	$tblName	=	TABLE_NEWS_LETTER;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/newsletter/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform task like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	


/* choices to where page is redirect
	if action=create then goes to view  	
		action =update then model
		action default to view i.e index.php
*/

$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."newsletter/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."newsletter/view/view.php" ;
			break;
}

/***********************************************************************************/
		//Code  for search a record from database on click submit  button

/***********************************************************************************/
if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
			if($_GET["action"]=="search")
			{
				$sql = "SELECT tnl.* FROM ".TABLE_NEWS_LETTER." tnl WHERE ";
				if($_GET["recId"]!="")
					$sql.="tnl.id=".$_GET["recId"];
				if($_GET["email_id"]!="")
					$sql.="tnl.email_id like '%".trim($_GET["email_id"])."%'";
				if($_GET["status"]!="")
					$sql.="tnl.status=".$_GET["status"];
				if($_GET["startDate"]!="" && $_GET["endDate"])
				{
					$sql.=" and date(tnl.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(tnl.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
				}
				else
				if($_GET["startDate"]!="")
				{
					$sql.=" and date(tnl.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
				}
				else
					if($_GET["endDate"]!="")
					{
						$sql.=" and date(tnl.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
					}
			}
			else if($_GET["action"]=="Update")
				{

					$sql = "SELECT tnl.*  FROM ".TABLE_NEWS_LETTER." tnl ";
					$sql.= " WHERE tnl.id=".$_GET["id"]." and source=1"; 
					//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
				}
			else if($_GET["action"]=="Delete") 
				{
	    			$obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
					$obj_common->redirect($page_name."?msg=delete");
				}
		$s=($_GET['s'] ? 'ASC' : 'DESC');
		$sort=($_GET['s'] ? 0 : 1 );
   		$f=$_GET['f'];
		if($s && $f)
			$sql.= " ORDER BY $f  $s";
			else
			$sql.= " ORDER BY tnl.id desc";	

	
	}
	else
	{
		$sql = "SELECT tnl.* FROM ".TABLE_NEWS_LETTER." tnl where tnl.source=1 order by tnl.id desc";
		//$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
/*---------------------paging script start----------------------------------------*/
if(isset($_GET['rpp']))
		$obj_paging->limit=$_GET['rpp'];
		else
		$obj_paging->limit=10;
				if($_GET['page_no'])
			$page_no=remRegFx($_GET['page_no']);
			else
			$page_no=0;
		$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
		$str_pos=strpos($queryStr,'page_no');
		if($str_pos>0)
			$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
		$obj_paging->set_lower_upper($page_no);
		$total_num=$obj_mysql->get_num_rows($sql);
		$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
		$total_rec=$obj_paging->total_records($total_num);
		$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
/************************************************************************************************/
		// code for search and pagination ends here

/************************************************************************************************/	

/*if(isset($_POST['categorysubmit']))
{

				$arr=$_POST;
				$rec=$_POST;
				$obj_mysql->insert_data($tblName,$arr);
				$obj_common->redirect($page_name."?msg=insert");	
}*/

	if(count($_POST)>0)
	{
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id)
			{
				$arr['modified_date'] = "now()";
				$arr['ipaddress'] = $regn_ip;
				$arr['modified_by']=$_SESSION['login']["id"];

				if($obj_mysql->isDupUpdate($tblName,'email_id', $arr['email_id'] ,'id',$rec_id))
				{
						
					$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
					$obj_common->redirect($page_name."?msg=update");	
			}
			else
			{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
	}
	else
	{
		$sql = mysql_query("SELECT MAX(id) as maxId FROM ".TABLE_NEWS_LETTER);
		$recId = mysql_fetch_array($sql);
		$maxId = $recId["maxId"];

		if($maxId!="")
			$setmaxid = $maxId+1;
			else
			$setmaxid = 1;
			//$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['created_by']=$_SESSION['login']["id"];

			if($obj_mysql->isDuplicate($tblName,'email_id', $arr['email_id']))
			{
				$obj_mysql->insert_data($tblName,$arr);
				$obj_common->redirect($page_name."?msg=insert");	
			}
			else
			{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
//second if block ends here


	$list="";
	
		for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
								
					<td class="center">'.$rec[$i]['email_id'].'</td>
					<td class="center">'.$st.'</td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td >
					<a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>
						<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>
					</td> 
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=8 align='center' height=50>No Record(s) Found.</td></tr>";
	}



?>

<!--************************************** code to include common header ***************************************/-->
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Newsletter Manager', $site['TITLE'], $site['URL']);  
?>
<!--************************************** End of code to include header ***************************************/-->
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name	);?></td> <!-- include file_name to get the page on controller page -->
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
