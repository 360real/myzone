<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<input type="hidden" name="existingPropertyName" value="<?php echo($rec['property_name'])?>">
<input type="hidden" name="property_id" value="<?php echo($_GET['id'])?>">

<?php 
$sqlData = "SELECT headings.*  FROM ".TABLE_KEY_FEATURES_HEADING." headings  WHERE headings.property_id=".$_GET['id']." and headings.source=2 " ;
$recData=($obj_mysql->getAllData($sqlData));
if($recData)
{
  ?>
  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3 style="float:left;">Property Points</h3><a style="background: #ffffff; border-radius: 2px; color: #6d1f6a; float: right; font-size: 13px; margin-right: 50px; margin-top: -1px; padding: 4px 10px;" href="javaScript:void(0);" class="" onclick="getPointsPopUp('2','', '<?php echo $_GET['id']; ?>');">Add New Heading</a></th></tr>
</thead>
<tbody>
  <tr class="<?php echo($clsRow1)?>">
  <td align="left" style="width:20%"><strong>Key Features of Property</strong></td>

  <td>
    <div class="addkeypoints"></div>
 <input type="hidden" name="source" value="2">
<?php 
$i=0;
  foreach($recData as $keydata=>$valdata)
   {
     $clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
?>
<input type="hidden" name="pointhead_id[]" value="<?php echo $valdata["id"]; ?>">
  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<tr style='width:100%;float:left;background-color:lightgrey;' class='clsRowView1' ><td align='left' style="font-size:13px;"><strong>Heading</strong></td>
<td style="width: 400px;"><input type='text'  value='<?php echo $valdata["heading"]; ?>' name='heading[]' id="headingValue_<?php echo $valdata["id"]; ?>" style='width:96%; height: 35px; font-size: 13px; padding:0  10px;' ></td>
<td style="width: 85px;"><a href="javaScript:void(0);" onclick="getPointsPopUp('1','<?php echo $valdata["id"]; ?>','');" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 10px 10px; text-decoration: none;" class="demo_open">Edit Points</a></td>
<td style="width: 65px;"><a href="javaScript:void(0);" class="delete" onclick="forHeading('1','<?php echo $valdata["id"]; ?>');">In-Active</a></td>
<td style="width: 145px;"><a href="javaScript:void(0);" onclick="forHeading('2','<?php echo $valdata["id"]; ?>');" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 5px 10px; text-decoration: none;" class="edit demo_open">Change Heading</a></td>
</tr>
<?php
 } ?>
      </table>
<?php

  //$i++; } 
?>


 </td>
 </tr>

 </tbody>
 </table>

<?php }

else { ?>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3 style="float:left;">Property Points</h3><a style="background: #ffffff; border-radius: 2px; color: #6d1f6a; float: right; font-size: 13px; margin-right: 50px; margin-top: -1px; padding: 4px 10px;" href="javaScript:void(0);" class="" onclick="getPointsPopUp('2','', '<?php echo $_GET['id']; ?>');">Add New Heading</a></th></tr>
</thead>

</table>
</form>
<?php } ?>
<div id="demo" class="posR checkwell"> </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

 <script src="<?php echo $site['URL']?>view/js/jquery.popupoverlay.js"></script>


<style type="text/css">
  .checkwell{ display: none; background: #fff; max-width: 600px; padding: 30px; width: 100% }
  .mainfsk{ font-size: 0; }
  .sectio_1 > span{ font-size: 13px ; padding-bottom: 5px; display: inline-block;}
  .SbSec{ font:bold 25px 'calibri',arial; text-align: center; padding-bottom: 10px; text-transform: uppercase; }
  .sectio_1{ width:47%; display: inline-block; vertical-align: top; margin:0 30px 20px 0; }
  .sectio_1:nth-of-type(2n+2){ margin-right: 0; }
  .sectio_12{ width: 100%;  display: inline-block; vertical-align: top;}
  .sectio_1 input[type="text"]{ width: 100%; height: 40px;background: #fff; border: 1px solid #ddd;  }
  .sectio_12 input[type="button"]{ width: 100%; height: 42px; background: #6d1f6a; border: none; font-size: 16px; color:#fff;  }
  .sectio_123{ width: 30%;  display: inline-block; vertical-align: top;}
.sectio_123 input[type="button"]{ width: 100%; height: 42px; background: #6d1f6a; border: none; font-size: 16px; color:#fff;  }
  .demo_close {
    background: #6d1f6a none repeat scroll 0 0;
    border: medium none;
    color: #ffffff;
    height: 20px;
    position: absolute;
    right: 0;
    top: 0;
    width: 20px;
}
</style>

<script>
  function getnoOfRows(value)
  {
    var base_url="<?php echo C_ROOT_URL; ?>";

    $.ajax({
        url: base_url+"/location_map/headings.php",
        type: 'POST',
        data: {rowscount:value},
        success: function(data) {
      $(".addkeypoints").html(data); 
    }
    });

  }


  function getPointsPopUp(type, headingId, propertyId)
  {
    var base_url="<?php echo C_ROOT_URL; ?>";
    $.ajax({
        url: base_url+"/location_map/points_popup.php",
        type: 'POST',
        data: {headingId:headingId, type:type, propertyId:propertyId},
        success: function(data) {
          $("#demo").replaceWith(data);
          $('#demo').popup('show');
    }
    });
  }

function savePointsData(headingId)
{
  var points = document.getElementsByName("subjectValue");
var vals=[];
var ids=[];
for (var i=0, n=points.length;i<n;i++) {
  vals.push(points[i].value);
}
for (var i=0, n=points.length;i<n;i++) {
  ids.push(points[i].id);
}


var allPointsValues=vals.join(",");
var myarr = allPointsValues.split(",");
var allPointsId=ids.join(",");
var propertyId="<?php echo $_GET['id']; ?>";
/*alert(allPointsId);*/
var base_url="<?php echo C_ROOT_URL; ?>";
    $.ajax({
        url: base_url+"/location_map/points_update.php",
        type: 'POST',
        data: {allPointsValues:allPointsValues, allPointsId:allPointsId, headingId:headingId},
        success: function(data) {
          location.reload(); 
    }
    });


}

function forHeading(type, headingId)
{
 var base_url="<?php echo C_ROOT_URL; ?>";
 var heading=$("#headingValue_"+headingId).val();
    $.ajax({
        url: base_url+"/location_map/heading_update.php",
        type: 'POST',
        data: {headingId:headingId, type:type, heading:heading},
        success: function(data) {
          location.reload(); 
    }
    });
}

function addNewHeading(propertyId)
{
  var base_url="<?php echo C_ROOT_URL; ?>";
 var heading=$("#headingValue").val();
 var type=3;
    $.ajax({
        url: base_url+"/location_map/heading_update.php",
        type: 'POST',
        data: {propertyId:propertyId, type:type, heading:heading},
        success: function(data) {
          location.reload(); 
    }
    });
}


</script>