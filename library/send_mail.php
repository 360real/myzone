<?php
class multipartmail
{
	var $header;
	var $parts;
	var $message;
	var $subject;
	var $to_address;
	var $boundary;
function multipartmail($dest='', $src="info@jobs.com.uk", $sub=''){
	$this->to_address = $dest;
	$this->subject = $sub;
	$this->parts = array("");
	$this->boundary = "------------" . md5(uniqid(time()));
	$this->header =
	"From: $src \r\n".
	"MIME-Version: 1.0\r\n".
	"Content-type: text/html; charset=iso-8859-1\r\n".
	"X-Mailer: PHP/" . phpversion();
}
function addmessage($msg = "", $ctype = "text/plain"){
	$this->parts[0] = $msg;
	//chunk_split($msg, 68, "\n");
}
function addattachment($file, $ctype){
	$fname = substr(strrchr($file, "/"), 1);
	$data = file_get_contents($file);
	$i = count($this->parts);
	$content_id = "part$i." . sprintf("%09d", crc32($fname)) . strrchr($this->to_address, "@");
	$this->parts[$i] = "Content-Type: $ctype; name=\"$fname\"\r\n" .
	"Content-Transfer-Encoding: base64\r\n" .
	"Content-ID: <$content_id>\r\n" .
	"Content-Disposition: inline;\n" .
	" filename=\"$fname\"\r\n" .
	"\n" .
	chunk_split( base64_encode($data), 68, "\n");
	return $content_id;
}
function buildmessage(){
	$this->message = "";
	$cnt = count($this->parts);
	for($i=0; $i<$cnt; $i++){
	$this->message .= "\n" .
	$this->parts[$i];
}
}
/* to get the message body as a string */
function getmessage(){
	$this->buildmessage();
	return $this->message;
}
function sendmail(){
	$this->buildmessage();	
	mail($this->to_address, $this->subject, $this->message, $this->header);		
}
}

# Usage Example:
// $mulmail = new multipartmail("seagaltest@gmail.com", "sanjeev@seagal.org", "Testmail The yahoo server");
//// // $cid = $mulmail->addattachment("/var/www/html/img/pic.jpg", "image/jpg");
//echo $mulmail->addmessage(
// "<html>\n" .
// " <head>\n" .
// " </head>\n" .
// " <body>\n" .
// " This is text before and after\n" .
// " </body>\n" .
// "</html>\n", "text/html");
// $mulmail->sendmail();





?>