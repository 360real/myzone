<?php
class main_common extends mysqli_function
{
    function redirect($url)
    {
        if (!headers_sent()):
            header("Location: $url");
        else:
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $url . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
            echo '</noscript>';
        endif;
        exit;
    }
	function redirectSubmit($url,$other="")
    {
       		echo '<html>';
			echo '<body>';
			echo '<form name="frmSubmit" id="frmSubmit" method="post" action="'.$url.'">';
			echo $other;
			echo '</form>';
			echo '<script type="text/javascript">document.frmSubmit.submit();</script>';
			echo '</body>';
			echo '</html>';
       		exit;
    }
    function random_string($chars = 8)
    {
        $letters = 'abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        return substr(str_shuffle($letters), 0, $chars);
    }
    //---------Start------Email Functions--------------------------------------//
    function validate_email($email) //Email Validation
    {
        $regexp = "^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$";
        if (eregi($regexp, $email))
            $valid = true;
        else
            $valid = false;
        return $valid;
    }
    function set_email_message($msg_array, $message_header, $message_footer)
    {
        $message .= "<table style='font-size:9pt;font-family:verdana;font-weight:normal;Color:#000000' cellpadding=5 cellspacing=0 width=100%>";
        $message .= "<tr><td colspan=3>$message_header</td></tr>";
        if (is_array($msg_array)) {
            foreach ($msg_array as $key => $val)
                $message .= "<tr><td>$key</td><td>:</td><td>$val</td></tr>";
        }
        $message .= "<tr><td colspan=3>$message_footer</td></tr>";
        $message .= "</table>";
        return $message;
    }
    function SendEmail($subject, $to, $message, $from, $reply_to = '')
    {
        $mulmail = new multipartmail($to, $from, $subject, $reply_to);
        $mulmail->addmessage("<html>" . "<body>" . "$message</body></html>", "text/html");
        $mulmail->sendmail();
        return true;
    }
    //---------Start------Dropdown Menu Functions--------------------------------------//
    function options($sql, $id = '')
    {
        if ($sql) {
            $rec = $this->getAllRowData($sql, $link = '');
            foreach ($rec as $key => $query) {
                $str = (($id == $query[0]) ? 'selected' : '');
                $list .= "<option value=\"$query[0]\" $str>$query[1]</option>";
            }
            return $list;
        }
    }
    function arrOptions($optArray, $id = '',$kayval=true)
    {
        if (is_array($optArray)) {
            natcasesort($optArray);
            foreach ($optArray as $key => $val) {
				if($kayval==false){
					$str = (($id == $val) ? 'selected' : '');
                	$list .= "<option value=\"$val\" $str>$val</option>";
				}else{
					$str = (($id == $key) ? 'selected' : '');
					$list .= "<option value=\"$key\" $str>$val</option>";
				}
            }
            return $list;
        }
    }
	
	
	function arrOptionsMultiple($optArray, $arrid = array(),$keyval=true)
    {
        if (is_array($optArray)) {
            natcasesort($optArray);
			//die(print_r($arrid));
            foreach ($optArray as $key => $val) {
				if($keyval==false){
					$str = (in_array($val,$arrid) ? ' selected="selected"' : '');
                	$list .= "<option value=\"$val\" $str>$val</option>";
				}else{
					$str = (in_array($key,$arrid) ? ' selected="selected"' : '');
					$list .= "<option value=\"$key\" $str>$val</option>";
				}
            }
            return $list;
        }
    }
	
	
    //---------Start------Image Functions--------------------------------------//
    function targetImgSize($source, $target)
    {
        if (is_file($source)):
            $size       = getimagesize($source);
            $percentage = 1; //In case if no rounding is Required
            if (!($size[0] < $target && $size[1] < $target)) //Apply only when Image size is greater than Target
                {
                if ($size[0] > $size[1])
                    $percentage = ($target / $size[0]);
                else
                    $percentage = ($target / $size[1]);
            }
            $source           = array();
            $source['width']  = round($size[0] * $percentage);
            $source['height'] = round($size[1] * $percentage);
            return $source;
        else:
            return false;
        endif;
    }
    function upload_files($arr_allowed_extension, $file_array, $destination, $new_file_name, $size = 12097152)
    {
        $ext = explode('.', $file_array['name']); //Get the extention of image 
        $ext = strtolower(end($ext)); //To get the extention and change it lower case
        if ($file_array['size'] < $size)
        //Check the size of Image
            {
            if (is_array($arr_allowed_extension) ? (in_array($ext, $arr_allowed_extension)) : false) {
                $name = $new_file_name . ".$ext";
                if (move_uploaded_file($file_array['tmp_name'], $destination . '/' . $name)):
                    chmod($destination . '/' . $name, 0777);
                    return $name;
                else:
                    return '';
                endif;
            }
        }
    }
    function message($text)
    {
        $str .= "<table width=100% class='TblBox1' cellspacing=3 cellpadding=0>";
        $str .= "<tr><td align='center' height=30>";
        $str .= $text;
        $str .= "</td></tr></table>";
        return $str;
    }
    function createImage($string, $backgroundimage, $font, $filename, $path = "")
    {
        $im     = imagecreatefromgif($backgroundimage);
        $colour = imagecolorallocate($im, 255, 255, 255);
        imagettftext($im, 75, $angle, 85, 95, $colour, "fonts/" . $font, ucfirst($string));
        $outfile = $path . "/$filename.gif";
        imagegif($im, $outfile);
        return $outfile;
    }
    function resize_image($tmp_name, $image_type, $filename, $r_height = '500', $r_width = '330')
    {
        // This is the temporary file created by PHP 
        //$uploadedfile = $_FILES['uploadfile']['tmp_name'];
        $uploadedfile = $tmp_name;
        // Create an Image from it so we can do the resize
        //$image_type= $_FILES['uploadfile']['type'];
        ini_set("memory_limit", "100M");
        if ($image_type == 'image/jpeg')
            $src = imagecreatefromjpeg($uploadedfile);
        elseif ($image_type == 'image/gif')
            $src = imagecreatefromgif($uploadedfile);
        elseif ($image_type == 'image/x-png')
            $src = imagecreatefrompng($uploadedfile);
        elseif ($image_type == 'image/pjpeg')
            $src = imagecreatefromjpeg($uploadedfile);
        else
            $src = imagecreatefrompng($uploadedfile);
        // Capture the original size of the uploaded image
        list($width, $height) = getimagesize($uploadedfile);
        //by ------------------ akhilesh--------------------------------
        $newwidth  = $width;
        $newheight = $height;
        //by ------------------ akhilesh--------------------------------
        
        /*-------------set new width and new height ----------------------------------------------------------------- */
        /*-------------if $width is less than 330 and hieght is less than 220------------------------------------------*/
        //by ------------------ akhilesh--------------------------------
        
        /*	if($width<=$r_width && $height<=$r_height)
        {
        $newwidth=$width;
        $newheight=$height;
        }
        else
        {
        $widthRatio=($width/$r_width);
        $heightRatio=($height/$r_height);
        if($heightRatio>$widthRatio)
        {
        $newwidth=($width/$heightRatio);
        $newheight=$r_height;
        }else{
        $newwidth=$r_width;
        $newheight=($height/$widthRatio);
        }
        }
        */
        //by ------------------ akhilesh--------------------------------
        if ($height > 220) {
            $widthRatio  = ($width / $r_width);
            $heightRatio = ($height / $r_height);
            if ($heightRatio > $widthRatio) {
                $newwidth  = ($width / $heightRatio);
                $newheight = $r_height;
            } else {
                $newwidth  = $r_width;
                $newheight = ($height / $widthRatio);
            }
        }
        $tmp = imagecreatetruecolor($newwidth, $newheight);
        // this line actually does the image resizing, copying from the original
        // image into the $tmp image
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        // now write the resized image to disk. I have assumed that you want the
        // resized, uploaded image file to reside in the ./images subdirectory.
        //$filename = "images/". $_FILES['uploadfile']['name'];
        imagejpeg($tmp, $filename, 100);
        imagedestroy($src);
        imagedestroy($tmp); // NOTE: PHP will clean up the temp file it created when the request
        // has completed.
        return 1;
    }
    function create_thumb($src, $width, $maxHeight, $id = '', $mouseOver = '', $title = '')
    {
        if ($id != "")
            $showId = "id=\"$id\"";
        else
            $showId = "id=\"$id\"";
        if ($mouseOver)
            $OnMouseOver = "onMouseOver=\"$mouseOver\"";
        else
            $OnMouseOver = '';
        return "<img border=\"0\" $showId $OnMouseOver src=\"../includes/thumb.php?src=../$src&x=$width&y=$maxHeight&f=0\"/>";
    }
    function generatePassword($length = 9, $strength = 0)
    {
        $vowels     = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }
        $password = '';
        $alt      = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }
	
	
	
	function sendMail_new($to,$subject,$message,$from,$file,$type)
	{
	
	// $type = 4 = send message as (html) with attachment
	// $type = 3 = send message as (html) with (no) attachment
	// $type = 2 = send message as (text) with attachment
	// $type = 1 = send message as (text) with (no) attachment
	// $to = who the mail is going to
	// $subject = this message subject
	// $message = the mail message to send
	// $from = who is sending this message (also = Reply-To)
	// $file = path to a file to attach to this message
	
			if(($type==2)||($type==4))
			{
			$content=fread(fopen($file,"r"),filesize($file));
			$content=chunk_split(base64_encode($content));
			$uid=strtoupper(md5(uniqid(time())));
			$name=basename($file);
			}
			$header="From:$from\n";
			$header.="Reply-To:$from\n";
			$header.="X-Priority: 3 (low)\n";
			$header.="X-Mailer: <Ya-Right Mail Server>\n";
			$header.="MIME-Version: 1.0\n";
	
			if(($type==2)||($type==4))
			{
			$header.="Content-Type:multipart/mixed;boundary=$uid\n\n";
			$header.="This is a mulipart message in mime format\n\n";
			$header.="--$uid\n";
			}
	
			if(($type==1)||($type==2)){
			$header.="Content-Type: text/plain; charset=\"ISO-8859-1\"\n";
			}
	
			if(($type==3)||($type==4))
			{
			$header.="Content-Type: text/html; charset=\"ISO-8859-1\"\n";
			}
			$header.="Content-Transfer-Encoding: 8bit\n\n";
			$header.="$message\n\n";
			if(($type==2)||($type==4))
			{
			$header.="--$uid\n";
			$header.="Content-Type:application/octet-streamname=\"$name\"\n";
			$header.="Content-Transfer-Encoding: base64\n";
			$header.="Content-Disposition:attachment;filename=\"$name\"\n\n";
			$header.="$content\n\n";
			$header.="--$uid--\n";
			}
			if(mail($to,$subject,"",$header))
            {				
                return true;
			}
            else
				return false;
	}
	
	
	function ftp_upload($host,$ftpName,$ftpPass,$file_object_name,$dest_directory,$allowedTYPES = array('JPEG','JPG','AVI','MPEG','WMV','GIF','PNG')){
		set_time_limit(0);
		$conn_id = ftp_connect($host) or die("Couldn't connect FTP");
		$login_result = ftp_login($conn_id, $ftpName, $ftpPass) or die("ERROR IN FTP CONNECTION");
		$theFile = $_FILES[$file_object_name];
		$source = $theFile['tmp_name'];
		$file_destination = $_FILES[$file_object_name]['name'];
		$filetype = pathinfo($_FILES[$file_object_name]['name']);
		$filetype = strtoupper($filetype['extension']);
		if(array_search($filetype, $allowedTYPES)){	
			ftp_chdir($conn_id, $dest_directory);
			$upload = ftp_put($conn_id, $file_destination, $source, FTP_BINARY);
			if($upload){
				return ($file_destination);
			}else{
				return false;
			}
		}
	}
	
	function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	
}
?>