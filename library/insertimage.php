<?php
define('UPLOAD_FOLDER', "/var/www/html/media_images");

class insertimage
{
    /*
     * Object for Database connectivity
     */
    private $dbobject;

    /*
     * Function to insert image detail in database, Upload image in folder
     * @params : Array of image data in $_FILES, Tablename for data insertion, Folder name for storing image
     * @return : Boolean , Status of Image insertion
     */

    public function getUploadStatus($underscoreFilesArr = "", $tableName = "", $folderName)
    {
        $UploadfolderPath = UPLOAD_FOLDER . "/" . $folderName;
        $this->dbobject = new mysql_function();

        /*
         * Script if Table name is provided
         * Inserting details into table
         * @params: table name for inserting data in website database
         * @return: id of table of last column
         */

        if ($tableName != "") {
            $imgname = $underscoreFilesArr['name'];
            $userid = mysql_real_escape_string($_SESSION['login']['id']);

            $query = "insert into {$tableName} (name, createdBy) values ('{$imgname}',{$userid})";
            $lastInsertedId = $this->dbobject->insert_query($query);
        }

        /*
         * Creating Root Folder for Saving Image
         * $status : "success" if new folder is created, "folderExists" if same name folder already exists
         */

        $status = $this->createBaseFolder($UploadfolderPath);

        if ($status == "success" || $status == "folderExists") {

            $insertStatus = $this->insertSingleImage($underscoreFilesArr, $UploadfolderPath, $lastInsertedId);
        }

        return $insertStatus;
    }

    /*
     * Creating folder according to parameter i.e. Path for creating folder
     * @params: path for folder creation
     * @return: string
     */
    public function createBaseFolder($path)
    {
        if (!is_dir($path)) {
            mkdir($path);
            chmod($path, 0777);
            $status = "success";
        } else {
            $status = "folderExists";
        }
        return $status;
    }

    /*
     * Inserting Image into Folder
     * @params: Image details Array
     *          Folder path for image upload
     *          ID from table of last inserted image
     * @return: Boolean - TRUE(1) if uploaded else FALSE(0) if not
     */

    public function insertSingleImage($imageDetails, $uploadPath, $lastInsertedId)
    {
        $imageName = $imageDetails['name'];
        $imgType = $imageDetails['type'];
        $tempImageName = $imageDetails['tmp_name'];
        $imgSize = $imageDetails['size'];

        if (empty($lastInsertedId)) {
            $status = move_uploaded_file($tempImageName, $uploadPath . "/" . $imageName);
        } else {
            $path = $uploadPath . "/" . $lastInsertedId;
            $createFolder = $this->createBaseFolder($path);

            if ($createFolder == "folderExists") {
                $status = "FALSE";
            } else {
                $status = move_uploaded_file($tempImageName, $uploadPath . "/" . $lastInsertedId . "/" . $imageName);
            }
        }

        return $status;
    }

}