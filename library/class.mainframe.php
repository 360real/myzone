	<?php
	class clsMainframe
	{
	var $table_prefix;

	function clsMainframe($table_prefix=""){
	$this->table_prefix = $table_prefix;
	}
	function getVideoGalleryHome()
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."video_gallery where status='1' and isHomepage='1' ORDER By dis_order LIMIT 100;";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	$ctr=1;
	foreach($rec as $row_video){
	$strReturn .='
	
	<div style="float:left; width:173px;"><div>
	
	<a href="vplayergallery.php?id='.$row_video['vid'].'&TB_iframe=true&height=300&width=320" class="thickbox">
	<img src="uploads/stvideogallery/image/'.$row_video['file_name'].'" width="170" height="150"></a></div>
	<div style="clear:both;"><strong>'.chars($row_video['title'],1000).'</strong></div>
	</div>'	;
	$ctr ++;
	}
	
	}
	return $strReturn;
	}

	function getVideoDetail($id){
	global $obj_mysql;
	$sql = "SELECT * from ".TABLE_PREFIX."video where vid='$id';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}
	function getVideoDetailGallery($id){
	global $obj_mysql;
	$sql = "SELECT * from ".$this->table_prefix."video_gallery where vid='$id';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}

	function getVideoCategoryDetail($cid){
	global $obj_mysql;
	$sql = "SELECT * from ".TABLE_PREFIX."video_category where cid='$cid';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}

	function getVideoCategoryDescription($cid){
	global $obj_mysql;
	$sql = "SELECT * from ".TABLE_PREFIX."video_category where cid='$cid';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	$description = ($this->table_prefix=='st_en_')? $rec['description_en'] : $rec['description_fr'];			
	return $description;
	}		

	function getWebpageDetail($id){
	global $obj_mysql;
	$sql = "SELECT * from ".$this->table_prefix."webpage where id='$id' and status='1';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}

	function getNewsDetail($id){
	global $obj_mysql;
	$sql = "SELECT * from ".$this->table_prefix."news where id='$id' and status='1';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}

	function getPressDetail($id){
	global $obj_mysql;
	$sql = "SELECT * from ".$this->table_prefix."banner where bid='$id' and status='1';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}

	function getEventDetail($id){
	global $obj_mysql;
	$sql = "SELECT * from ".$this->table_prefix."event where id='$id' and status='1';";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}

	function getLearnhowtoDetail(){
	global $obj_mysql;
	$sql = "SELECT * from ".$this->table_prefix."header where 1 LIMIT 1;";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;
	}

	function getNewsListHome($limit=5){
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."news where status='1' and show_on_home_page=1 ORDER By news_date DESC,dis_order LIMIT $limit;";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	$ctr=1;

	foreach($rec as $row){
	$strReturn .='
	<tr>
	<td width="116" height="9" align="left" valign="top" class="txt62"><a href="news-detail.php?id='.$row['id'].'" class="txt3">'.DisDateFormat($row['news_date'],'F d, Y').'</a></td>
	<td width="362" align="left" valign="top" class="txt62">&ndash; '.chars($row['title'],45).' </td>
	</tr>';
	if($ctr <= $total_num){
	$strReturn .='<tr>
	<td height="0" colspan="2" align="left" valign="top" >------------------------------------------------------------------------------------------------</td>
	</tr>';
	}
	$ctr +=1;
	}
	}
	return $strReturn;
	}

	function getNewsList($limit=3)
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."news where status='1' ORDER By news_date DESC,dis_order LIMIT $limit;";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}

	function getInnerNewsList($limit=5)
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."news where status='1' ORDER By news_date DESC,dis_order LIMIT $limit;";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	$ctr=1;

	foreach($rec as $row){
	$strReturn .='
	<div class="news_text_cont">
	<div class="date_time">
	<img src="'.$site['URL'].'uploads/newspic//'.$row['imgname'].'"  border="0" width="65" height="52"/></a>
	<br/>
	'.DisDateFormat($row['news_date'],'M d, Y').'</div>

	<div class="description"><a href="news_story.php?id='.$row['id'].'" class="description2">'.$row['title'].'</a></div>
	</div>';                        
	$ctr +=1;
	}				
	}
	return $strReturn;
	}

	function getEventList($limit=10){

	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."event where status='1' ORDER By event_date asc,dis_order LIMIT $limit;";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	$ctr=1;

	foreach($rec as $row){
	$strReturn .='
	<div class="middle_mtr_cont">
	<div class="event_top_curve">
	<div class="latest_news_heading_cont">
	<div class="event_hd">'.chars($row['event_title'],30).'</div>
	<div class="event_date_time">'.DisDateFormat($row['event_date'],'M d, Y').' / '.$row['event_time'].'</div>
	</div>
	<div class="event_main_cont">
	<div class="event_photo_cont"><a href="events_story.php?id='.$row['id'].'"><img src="'.$site['URL'].'uploads/eventpic/'.$row['imgname'].'" width="207" height="111" border="0"/></a></div>
	<div class="event_txt_cont">'.chars($row['description'],575).'</div>
	</div>
	</div>
					
	<div class="latest_btm_cont">
	<div class="more"><a href="events_story.php?id='.$row['id'].'"><img src="images/more.png" border="0" /></a></div>
	<div class="more"><a href="#"><img src="images/facebook_icon.png" border="0" /></a></div>
	</div>
	</div>
	';
	$ctr +=1;
	}
	$strReturn .='</table>';
	}
	return $strReturn;
	}

	function getEventListById($id){

	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."event where status='1' and id=$id  LIMIT 1;";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	$ctr=1;

	foreach($rec as $row){
	$strReturn .='
	<div class="middle_mtr_cont">
	<div class="event_top_curve">
	<div class="latest_news_heading_cont">
	<div class="event_hd">'.chars($row['event_title'],30).'</div>
	<div class="event_date_time">'.DisDateFormat($row['event_date'],'M d, Y').' / '.$row['event_time'].'</div>
	</div>
	<div class="event_main_cont">
	<div class="event_photo_cont"><img src="'.$site['URL'].'uploads/eventpic/'.$row['imgname'].'" width="207" height="111" /></div>
	<div class="event_txt_cont">'.$row['description'].'</div>
	</div>
	</div>
					
	<div class="latest_btm_cont">
	<div class="more"></div>
	<div class="more"><a href="#"><img src="images/facebook_icon.png" border="0" /></a></div>
	</div>
	</div>
	';
	$ctr +=1;
	}
	$strReturn .='</table>';
	}
	return $strReturn;
	}



	function getPressList($limit=10){

	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."banner where status='1' ORDER By news_date DESC LIMIT $limit;";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	$ctr=1;
	if(strstr($_SERVER['SCRIPT_FILENAME'],'fr/')){
	$strReturn .='
	<table width="590" border="0"  align="center" cellpadding="1" cellspacing="1">
	<tr>
	<td width="130" align="center" valign="middle" class="uptablheader">Date</td>
	<td width="170" align="center" valign="middle" class="uptablheader">Titre</td>
	<td width="300" align="center" valign="middle" class="uptablheader">Description</td>
	</tr>';

	}else{
	$strReturn .='
	<table width="590" border="0"  align="center" cellpadding="1" cellspacing="1">
	<tr>
	<td width="130" align="center" valign="middle" class="uptablheader">Date</td>
	<td width="170" align="center" valign="middle" class="uptablheader">Title</td>
	<td width="300" align="center" valign="middle" class="uptablheader">Description</td>
	</tr>';

	}
	foreach($rec as $row){
	$strReturn .='
	<tr>
	<td height="30" align="center" valign="middle" class="upcomtxt"><a href="press-detail.php?id='.$row['bid'].'" class="txt3">'.DisDateFormat($row['news_date'],'F d, Y').'</a></td>
	<td height="30" align="center" valign="middle">'.chars($row['title'],50).'</td>
	<td height="30" align="center" valign="middle"><a href="press-detail.php?id='.$row['bid'].'" class="txt3">'.chars($row['content'],50).'</a></td>
	</tr>
	';
	$ctr +=1;
	}
	$strReturn .='</table>';
	}
	return $strReturn;
	}

	function getVideoCategoryList($limit=10){
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".TABLE_PREFIX."video_category where status='1' LIMIT $limit;";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);

	if($total_num){
	$strReturn .='<table width="621" border="0" cellspacing="0" cellpadding="0">';

	$ctr=1;
	//	$strReturn .='<table width="576" border="0" align="center" cellpadding="0" cellspacing="0">';
	foreach($rec as $row){
	$title = ($this->table_prefix=='st_en_')? $row['cname_en'] : $row['cname_fr'];
	$description = ($this->table_prefix=='st_en_')? $row['description_en'] : $row['description_fr'];

	$strReturn .='<tr>
	<td width="621" height="70" align="left" valign="top" ><table width="356" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="356"><img src="images/upcomingtopcurve.png" width="594" height="9" /></td>
	</tr>
	<tr>
	<td align="center" valign="middle" class="lognpanel"><table width="584" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="79" height="86" align="center" valign="middle" class="clspad"  ><a href="list-classes.php?cid='.$row['cid'].'">';
	if($this->table_prefix=='st_fr_'){ 
	$strReturn .='<img src="../images/'.$row['file_name'].'" width="132" height="94" border="0" />';
	} else {
	$strReturn .='<img src="images/'.$row['file_name'].'" width="132" height="94" border="0" />';
	}
	$strReturn .='</a></td>
	<td width="79" align="center" valign="middle" class="sep" >&nbsp;</td>
	<td width="426" align="center" valign="top"  class="txt63">'.$description.'</td>
	</tr>

	</table>                          </td>
	</tr>
	<tr>
	<td><img src="images/bottom_upcoming.png" width="594" height="10" /></td>
	</tr>
	</table>                      </td>
	</tr>                    <tr>
	<td height="2" align="center" valign="top" ></td>
	</tr>'; 				
	/*
	$strReturn .='
	<tr>
	<td width="198" height="31" align="center" valign="middle" style="padding:5px 0px 5px 0px;"  ><table  border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" valign="middle" class="catb"><a href="list-classes.php?cid='.$row['cid'].'">'.$title.'</a></td>
	</tr>
	</table></td>
	<td width="377" align="left" valign="middle" ><span class="txt63">'.$description.'</span></td>
	<td width="1" align="center" valign="middle" ></td>
	</tr>';
	if($ctr < $total_num){
	$strReturn .='
	<tr>
	<td height="2" colspan="3" align="center" valign="middle" bgcolor="#999999"></td>
	</tr>';
	}
	$ctr +=1;*/
	}
	$strReturn .='</table>';
	}
	return $strReturn;
	}

	function getClassesListByCatId($cid=1){
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".TABLE_PREFIX."video where status='1' and cid='$cid' and lang='".$this->table_prefix."' order by dis_order";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);

	$ext='';
	if(strstr($_SERVER['SCRIPT_FILENAME'],'fr/')){	
	$ext='../';
	}			
	if($total_num){
	$ctr=1;
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="left" valign="middle"  >';
	foreach($rec as $row){
	$tem='';
	if($row['vid']==2)
	$tem='<br /><a href="special_offer.php"><img src="'.$ext.'images/buy_dvd.png" border="0" height="35"/></a>';
	if($_SESSION['MEMID']){
	if($this->chkUserVideo($row['vid']) || $row['video_free']==1){
	$strReturn .='<div  class="bigthums" style="height:310px" >
	<div><img src="'.$ext.'images/video_curvetop.png" /></div>
	<div class="picbig" style="height:280px" >
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td align="center"><!--<a rel="{handler: \'iframe\', size: {x: 350, y: 275}}" class="modal" href="vplayer.php?key='.base64_encode($row['vid']).'">-->
	<a href="vplayer.php?key='.base64_encode($row['vid']).'">'.loadImage_script($row['file_name'],'uploads/mjvideo/image',250,188).'</a></td>
	  </tr>
	</table>
	<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td height="10" align="left" class="resumtxt"></td>
	  </tr>
	  <tr>
		<td align="left" class="resumtxt" style="color:#FFFFFF"><strong>'.chars($row['title'],1000).'</strong> <br />'.chars($row['description'],1000).$tem.'</td>
	  </tr>
	</table>
	</div>
	<div><img src="'.$ext.'images/video_curvebottom.png" /></div>
	</div>
	';
	}else{
	$strReturn .='
	<div  class="bigthums" style="height:310px"  >
	<div><img src="'.$ext.'images/video_curvetop.png" /></div>
	<div class="picbig" style="height:280px" >
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td align="center"><a href="buy-video.php?cid='.$_REQUEST['cid'].'&vid='.($row['vid']).'">'.loadImage_script($row['file_name'],'uploads/mjvideo/image',250,188).'</a></td>
	  </tr>
	</table>
	<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td height="10" align="left" class="resumtxt"></td>
	  </tr>
	  <tr>
		<td align="left" class="resumtxt"  style="color:#FFFFFF">&nbsp; <img src="'.$ext.'images/lock-icon.png" border="0" /><strong >'.chars($row['title'],1000).$tem.'</strong> <br />'.chars($row['description'],1000).'</td>
	  </tr>
	</table>
	</div>
	<div><img src="'.$ext.'images/video_curvebottom.png" /></div>
	</div>
	';
	}
	}else{
	$strReturn .='<div  class="bigthums"  style="height:310px" >
	<div><img src="'.$ext.'images/video_curvetop.png" /></div>
	<div class="picbig"  style="height:280px"  >
	<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td align="center"><a href="login.php?return='.base64_encode(curPageURL()).'">'.loadImage_script($row['file_name'],'uploads/mjvideo/image',250,188).'</a></td>
	  </tr>
	</table>
	<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
		<td height="10" align="left" class="resumtxt"></td>
	  </tr>
	  <tr>
		<td align="left" class="resumtxt" style="color:#FFFFFF"><strong>'.chars($row['title'],1000).'</strong> <br />'.chars($row['description'],1000).$tem.'</td>
	  </tr>
	</table>
	</div>
	<div><img src="'.$ext.'images/video_curvebottom.png" /></div>
	</div>';
	}
	$ctr +=1;
	}
	$strReturn .='</td>
	</tr>
	</table>';
	}else{
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="left" valign="middle">

	Coming Soon...

	</td>
	</tr>
	</table>';
	}
	return $strReturn;
	}

	function chkUserVideo($vid){
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".TABLE_PREFIX."member_video where vid='$vid' and MemId='".$_SESSION['MEMID']."' ";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	return true;
	}else{
	return false;
	}
	}

	function saveOrder(){
	global $obj_mysql;
	$orderId = 0;
	$arrPost = $_POST;
	$nme='';

	if($_POST['coupon_code_s']!=''){
	$get_s_id=mysql_fetch_array(mysql_query("SELECT id  FROM st_special_video WHERE title = '".$_POST['sid']."'"));
	$get_rec=mysql_query("SELECT * FROM st_couponcode mcc 
	WHERE cc_code ='".$_POST['coupon_code_s']."'
		  and (special_offers like '".$get_s_id['id'].",%' or special_offers like '%,".$get_s_id['id'].",%' 
		  or special_offers like '%,".$get_s_id['id']."' or special_offers ='".$get_s_id['id']."')") 
		  or die(mysql_error());
	$get_rec_fetch=mysql_fetch_array($get_rec);
	$total_coupoun=$get_rec_fetch['number_user'];
	$get_c_used=mysql_fetch_array(mysql_query("SELECT count(user_id) cud from st_coupon_user where coupon_id='".$get_rec_fetch['cc_id']."'"));

	if(mysql_num_rows($get_rec)==0 && $total_coupoun<=$get_c_used['cud']){
	echo "<script>window.location='special_offer.php?msg=1'</script>";
	}else{
	$dis=($_POST['price']*$get_rec_fetch['cc_number'])/100;
	$arrPost['price']=number_format($_POST['price']-$dis,'2','.','');
	//			  mysql_query("update st_coupon_user set coupon_used=1 where coupon_user_id='".$get_rec_fetch['coupon_user_id']."'") or die(mysql_error());
	mysql_query("insert into st_coupon_user set 
	coupon_used=1,
	user_id='".$_SESSION['USER_DETAIL']['MemUname']."',
	coupon_id ='".$get_rec_fetch['cc_id']."'") or die(mysql_error());			  

	}
	}

	if($_POST['coupon_code_o']!=''){
	$get_s_id=mysql_fetch_array(mysql_query("SELECT vid FROM st_video WHERE title = '".$_POST['sid']."'"));
	$get_rec=mysql_query("SELECT * FROM st_couponcode mcc 
	WHERE cc_code ='".$_POST['coupon_code_o']."'
		  and (online_classes like '".$get_s_id['vid'].",%' or 	online_classes like '%,".$get_s_id['vid'].",%' 
		  or online_classes like '%,".$get_s_id['vid']."' or online_classes ='".$get_s_id['vid']."')") 
		  or die(mysql_error());
	$get_rec_fetch=mysql_fetch_array($get_rec);
	$total_coupoun=$get_rec_fetch['number_user'];
	$get_c_used=mysql_fetch_array(mysql_query("SELECT count(user_id) cud from st_coupon_user where coupon_id='".$get_rec_fetch['cc_id']."'"));

	if(mysql_num_rows($get_rec)==0 && $total_coupoun<=$get_c_used['cud']){
	echo "<script>window.location='buy-video.php?cid=".$_REQUEST['cid']."&vid=".$_REQUEST['vid']."&msg=1'</script>";
	}else{
	$dis=($_POST['price']*$get_rec_fetch['cc_number'])/100;
	$arrPost['price']=number_format($_POST['price']-$dis,'2','.','');
	//			  mysql_query("update st_coupon_user set coupon_used=1 where coupon_user_id='".$get_rec_fetch['coupon_user_id']."'") or die(mysql_error());			  
	mysql_query("insert into st_coupon_user set 
	coupon_used=1,
	user_id='".$_SESSION['USER_DETAIL']['MemUname']."',
	coupon_id ='".$get_rec_fetch['cc_id']."'") or die(mysql_error());			  

	}
	}	

	if(isset($_REQUEST['cid'])){
	$get_re=mysql_fetch_array(mysql_query("SELECT cname_en FROM st_video_category WHERE cid='".$_REQUEST['cid']."'"));
	$nme=", video_title='".$get_re['cname_en']."'";
	}
	if(isset($_REQUEST['sid'])){
	$nme=", video_title='".$_REQUEST['sid']."'";
	}
	$setFields = " SET
	MemId='".$_SESSION['MEMID']."',
	price='".$arrPost['price']."',
	vid='".$arrPost['vid'].",0',
	addadDate='".time()."'
	".$nme."
	";	
	$sql = "INSERT INTO ".TABLE_PREFIX."order $setFields";
	$orderId 	 = $obj_mysql->insert_query($sql);
	return $orderId;
	}

	function saveOrder_store(){
	global $obj_mysql;
	$orderId = 0;
	$arrPost = $_POST;
	$nme='';
	$_SESSION['ext_c']=0;
	if($_POST['coupon_code_st']!=''){
	$get_rec=mysql_query("SELECT * FROM st_couponcode mcc 
	WHERE cc_code ='".$_POST['coupon_code_st']."'
		  and (store like '".$_POST['stid'].",%' or store like '%,".$_POST['stid'].",%' 
			  or store like '%,".$_POST['stid']."' or store ='".$_POST['stid']."')") 
			  or die(mysql_error());
	$GP=0;
	$get_rec_fetch=mysql_fetch_array($get_rec);
	$total_coupoun=$get_rec_fetch['number_user'];
	$get_c_used=mysql_fetch_array(mysql_query("select count(user_id) cud from st_coupon_user where coupon_id='".$get_rec_fetch['coupon_user_id']."'"));
			  
	if(mysql_num_rows($get_rec)==0){
	echo "<script>window.location='store.php?msg=1'</script>";
	exit;
	}else{
	$dis=($_POST['price']*$get_rec_fetch['cc_number'])/100;
	$arrPost['price']=number_format($_POST['price']-$dis,'2','.','');
	$_SESSION['ext_c']=$dis;
	mysql_query("insert into st_coupon_user set 
	coupon_used=1,
	user_id='".$_SESSION['USER_DETAIL']['MemUname']."',
	coupon_user_id='".$get_rec_fetch['coupon_user_id']."'") or die(mysql_error());			  
	}
	}
	if(isset($_REQUEST['sid'])){
	$nme=", video_title='".$_REQUEST['sid']."'";
	}
	$setFields = " SET
	MemId='".$_SESSION['MEMID']."',
	st_id='".$_REQUEST['stid'].",0',
	addadDate='".time()."'
	".$nme."
	";	
	$sql = "INSERT INTO ".TABLE_PREFIX."order $setFields";
	$orderId = $obj_mysql->insert_query($sql);
	return $orderId;
	}		

	function saveOrderCart(){
	global $obj_mysql;

	$strVid = '';
	$ttl_v='';
	$arrCart = getCart();
	//			print_r($arrCart);
	//			exit;
	if($arrCart && is_array($arrCart)){
	foreach($arrCart as $row_cart){
	$strVid .= $row_cart['vid'].",";
	$st_id  .= $row_cart['st_id'].",";
	$ttl_v  .= $row_cart['title'].",";
	}    
	}

	if($strVid!="")
	$strVid = str_replace(',,',',',$strVid)."0";				

	if($strVid!="")
	$st_id = $st_id."0";				

	$orderId = 0;
	$arrPost = $_POST;
	$setFields = " SET
	MemId='".$_SESSION['MEMID']."',

	video_title='".mysql_real_escape_string($ttl_v)."',
	vid='".mysql_real_escape_string($strVid)."',
	st_id='".mysql_real_escape_string($st_id)."',							
	addadDate='".time()."'
	";	
	$sql = "INSERT INTO ".TABLE_PREFIX."order $setFields";
	$orderId = $obj_mysql->insert_query($sql);

	return $orderId;
	}
	function getOrderDetail($orderId)
	{
	global $obj_mysql;
	if($orderId){
	$order_sql 	= "SELECT * from ".TABLE_PREFIX."order ord
	where orderId='".mysql_real_escape_string($orderId)."' ";
	$arrOrder 	= $obj_mysql->get_assoc_arr($order_sql);
	}
	if($arrOrder)
	return $arrOrder;
	else
	return false;
	}

	function addMemberVideoOrder($orderId,$bool){
	global $obj_mysql;
	if($orderId){
	if($bool==true){
	$arrOrder = $this->getOrderDetail($orderId);
	//die($arrOrder);
	if($arrOrder && is_array($arrOrder)){

	$arrVid = explode(",",$arrOrder['vid']);
	for($v=0;$v < count($arrVid)-1;$v++){
	$setFields = " SET
	vid='".mysql_real_escape_string($arrVid[$v])."',
	MemId='".mysql_real_escape_string($arrOrder['MemId'])."',			
	addedDate='".time()."',
	updatedDate='".time()."'
	";	
	$sql = "INSERT INTO ".TABLE_PREFIX."member_video $setFields";
	$mvid = $obj_mysql->insert_query($sql);
	}
	}
	}
	}
	}

	function updateOrder($orderId,$type=true){
	global $obj_mysql,$obj_common;
	if($orderId){
	$setFields = " SET updatedDate='".time()."' ";
	if($type==true){
	$update_sql = "UPDATE ".TABLE_PREFIX."order $setFields , status='1' WHERE orderId='$orderId' ";
	$obj_mysql->update_query($update_sql);
	}else{
	$update_sql = "UPDATE ".TABLE_PREFIX."order $setFields , status='0' WHERE orderId='$orderId' ";
	$obj_mysql->update_query($update_sql);
	}
	}
	}

	function getPictureGallery(){
	global $obj_mysql,$site;
	$strReturn = '';
	$get_rec=mysql_num_rows(mysql_query("SELECT count(*) totalImage,pg.pid,pg.title,pg.content,pg.file_name,pg.dis_order,gp.g_id,gp.status from ".$this->table_prefix."picture_gallery pg,".$this->table_prefix."gallery_pictures gp  where pg.status='1' and gp.status='1' and pg.pid=gp.g_id GROUP BY pg.pid order by dis_order "));
	
	$recordcount=$get_rec;
	$where='';
	$page=1;
	$limit=4;
	if(isset($_GET['page']) && $_GET['page']!=''){
	$page=$_GET['page'];
	}
	$start=($page-1)*$limit;
	$page_name='picture_gallery.php';

	$pge=pagination($recordcount,$page,$limit,$page_name);
	$sql = "SELECT count(*) totalImage,pg.pid,pg.title,pg.content,pg.file_name,pg.dis_order,gp.g_id,gp.status from ".$this->table_prefix."picture_gallery pg,".$this->table_prefix."gallery_pictures gp  where pg.status='1' and gp.status='1' and pg.pid=gp.g_id GROUP BY pg.pid limit $start,$limit ";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);

	if($total_num){

	$ctr=1;
	//$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	//<tr>
	//<td width="125" align="center" valign="middle" >';
	foreach($rec as $row){
		
	$strReturn .='

	
	<div style="float:left; width:173px;"><div><a href="album_gallery.php?pid='.$row['pid'].'" title="'.chars($row['title'],1000).'"><img src="uploads/picture/'.$row['file_name'].'" width="170" height="150"></a></div>
	<div style="clear:both;"><strong>'.chars($row['title'],1000).'</strong></div>
	<div style="clear:both; font-weight:normal;">No of photos:'.$row['totalImage'].'</div>
	<div style="clear:both; font-weight:normal;">Description:'.chars($row['content'],280).'</div>
	</div>


	
	
	';
	$ctr +=1;
	}
	//$strReturn .='</td>
	//</tr>';
	//if($recordcount>$limit){
	//$strReturn .='<tr><td>&nbsp;</td></tr>
	//<tr>
	//<td align="center">'.$pge[0].'&nbsp;&nbsp;&nbsp;'.$pge[1].'&nbsp;&nbsp;&nbsp;'
	//.$pge[2].'
	//</td>
	//</tr>';			
	//} 		  
	$strReturn .='</table>';
	//}else{
	//$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	//<tr>
	//<td width="125" align="left" valign="middle">

	//Coming Soon...

	//</td>
	//</tr>
	//</table>';
	}
	return $strReturn;
	}


	function getPictureAlbumGallery($gId){
	global $obj_mysql,$site;
	$strReturn = '';
	$sql="SELECT * from ".$this->table_prefix."gallery_pictures where status='1' and g_id=1 order by dis_order ";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);

	if($total_num){

	$ctr=1;
	foreach($rec as $row){

		$strReturn .='

		<div style="float:left; width:173px;"><div><a href="uploads/picture/'.$row['file_name'].'" title="'.chars($row['title'],1000).'">
		<img src="uploads/picture/'.$row['file_name'].'" width="170" height="150">
		</a></div>
		<div style="clear:both;"><strong>'.chars($row['title'],1000).'</strong></div>
		</div>
	';
	$ctr +=1;
	}

	}
	return $strReturn;
	}


	function getVideoGallery(){
	global $obj_mysql;
	$strReturn = '';
	$get_rec=mysql_num_rows(mysql_query("SELECT * from ".$this->table_prefix."video_gallery where status='1'"));
	$recordcount=$get_rec;
	$where='';
	$page=1;
	$limit=4;
	if(isset($_GET['page']) && $_GET['page']!=''){
	$page=$_GET['page'];
	}
	$start=($page-1)*$limit;

	$page_name='vplayergallery.php';
	$pge=pagination($recordcount,$page,$limit,$page_name);             			
	$sql = "SELECT * from ".$this->table_prefix."video_gallery where status='1' limit $start,$limit";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);

	if($total_num){

	$ctr=1;
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="left" valign="middle"  >';
	foreach($rec as $row){
	$strReturn .='
	<div class="picbg_picture">
	<!--<a rel="{handler: \'iframe\', size: {x: 350, y: 275}}" class="modal" href="vplayergallery.php?key='.base64_encode($row['vid']).'">-->
	<a  href="vplayergallery.php?key='.base64_encode($row['vid']).'" style="text-decoration:none">
	<div>'.loadImage($row['file_name'],'uploads/stvideogallery/image',250,219).'</div>
	<div class="pictxt"><strong>'.chars($row['title'],1000).'</strong>
	<br />'.chars($row['content'],235).'</div>
	</a>
	</div>';
	$ctr +=1;
	}
	$strReturn .='</td>
	</tr>';
	if($recordcount>$limit){
	$strReturn .='<tr><td>&nbsp;</td></tr>
	<tr>
	<td align="center">'.$pge[0].'&nbsp;&nbsp;&nbsp;'.$pge[1].'&nbsp;&nbsp;&nbsp;'
	.$pge[2].'
	</td>
	</tr>';			
	} 		  
	$strReturn .='</table>';
	}else{
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="left" valign="middle">

	Coming Soon...

	</td>
	</tr>
	</table>';
	}
	return $strReturn;
	}

	function getStoreVideoList(){
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".TABLE_PREFIX."store where status='1' order by dis_order";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if(!isset($_SESSION['USER_DETAIL'])) $sty='style="display:none"'; 			
	if($total_num){
	$ctr=1;
	if(strstr($_SERVER['SCRIPT_FILENAME'],'fr/')){	
	$strReturn .='<table width="886"  border="0" cellpadding="1" cellspacing="1">
	<tr>
	<td height="36" align="center" valign="middle" class="uptablheader" >Produit</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Taille</td>									
	<td height="36" align="center" valign="middle" class="uptablheader" >Prix</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Description</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Quantit&eacute;s</td>
	<td height="36" align="center" valign="middle" class="uptablheader" '.$sty.'>Coupon <br />Code </td>									
	<td align="center" valign="top" class="uptablheader" >Add to <br />Panier d\'achats</td>
	<td align="center" valign="middle" class="uptablheader" >Acheter Maintenant</td>
	</tr>
	';
	}else{
	$strReturn .='<table width="886"  border="0" cellpadding="1" cellspacing="1">
	<tr>
	<td height="36" align="center" valign="middle" class="uptablheader" >Product</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Size</td>									
	<td height="36" align="center" valign="middle" class="uptablheader" >Price</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Description</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Quantity </td>
	<td height="36" align="center" valign="middle" class="uptablheader" '.$sty.'>Coupon <br />Code </td>									
	<td align="center" valign="top" class="uptablheader" >Add to <br />shopping Cart </td>
	<td align="center" valign="middle" class="uptablheader" >Buy Now </td>
	</tr>
	';

	}
	$gps=1;
	foreach($rec as $row){
	$nme_st=$row['title'];
	$desc_st=$row['description'];
	if($this->table_prefix=='st_fr_'){
	$nme_st=$row['title_fr'];
	$desc_st=$row['description_fr'];	
	}	
	$strReturn .='<tr>
	<td width="125" height="31" align="center" valign="middle" >'.loadImage($row['file_name'],'uploads/mjstore/image',125,94).'</td>
	<td width="125" height="31" align="center" valign="middle" class="txt1" >'.$row['product_size'].'</td>							
	<td width="71" align="center" valign="middle" class="txt1" >'.$row['price'].'$ </td>
	<td width="374" align="center" valign="middle" class="txt1" >'.$desc_st.'</td>
	<td width="115" align="center" valign="middle" class="txt1" >1</td>
	<td width="115" align="center" valign="middle" class="txt1"  '.$sty.'>
	<input type="text" name="coupon_code_s" id="coupon_code_box_'.$gps.'" value=""  size="10"/>
	</td>								
	<td width="95" align="center" valign="middle" >
	<form name="frmAdd2Cart" id="frmAdd2Cart" action="" method="post">
	<input type="hidden" name="coupon_code_s" id="coupon_code_a_'.$gps.'" value=""  size="10"/>
	<input type="hidden" name="id" value="'.$row['id'].'">
	<input type="hidden" name="sid" value="'.$nme_st.'" />							
	<input type="hidden" name="qty" value="1">
	<input type="hidden" name="price" value="'.$row['price'].'" />
	<input type="hidden" name="storeadd2Cart" value="1" />
	<input type="image" src="images/shoppingcart.png"  onclick="return get_values_id(\'coupon_code_a_\','.$gps.',\'coupon_code_box_\')"/>
	</form>
	</td>
	<td width="99" align="center" valign="middle" >
	<form name="frmOrder" id="frmOrder" action="buy-product.php" method="post">
	<input type="hidden" name="coupon_code_st" id="coupon_code_b_'.$gps.'" value=""  size="10"/>
	<input type="hidden" name="stid" value="'.$row['id'].'" />
	<input type="hidden" name="sid" value="'.$nme_st.'" />							
	<input type="hidden" name="price" value="'.$row['price'].'" />
	<input type="hidden" name="paynow" value="1" />
	<input type="image" src="images/buynow.png" width="99" height="35"  onclick="return get_values_id(\'coupon_code_b_\','.$gps.',\'coupon_code_box_\')" />
	</form>
	</td>
	</tr>
	';
	if($ctr < $total_num){
	$strReturn .='<tr>
			<td height="10" colspan="6" align="center" valign="middle" ></td>
		  </tr>';
	}
	$ctr +=1;
	$gps++;
	}
	$strReturn .='</table>';
	}else{
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="center" valign="middle">

	Coming Soon...

	</td>
	</tr>
	</table>';
	}
	return $strReturn;
	}

	function getSpecialOfferVideoList(){
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".TABLE_PREFIX."special_video where status='1' order by dis_order";
	//			$sql = "SELECT * from ".TABLE_PREFIX."special_video where status='1' and lang='".$this->table_prefix."' order by dis_order";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);

	if($total_num){

	$ctr=1;
	if(!isset($_SESSION['USER_DETAIL'])) $sty='style="display:none"'; 
	if(strstr($_SERVER['SCRIPT_FILENAME'],'fr/')){
	$strReturn .='<table width="886"  border="0" cellpadding="1" cellspacing="1">
	<tr>
	<td height="36" align="center" valign="middle" class="uptablheader" >Produit</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Prix</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Description</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Quantit&eacute;s </td>
	<td height="36" align="center" valign="middle" class="uptablheader" '.$sty.'>Coupon <br />Code </td>									
	<td align="center" valign="top" class="uptablheader" >Add to <br />Panier d\'achats	 </td>
	<td align="center" valign="middle" class="uptablheader" >Acheter Forfait </td>
	</tr>';
	}else{
	$strReturn .='<table width="886"  border="0" cellpadding="1" cellspacing="1">
	<tr>
	<td height="36" align="center" valign="middle" class="uptablheader" >Product</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Price</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Description</td>
	<td height="36" align="center" valign="middle" class="uptablheader" >Quantity </td>
	<td height="36" align="center" valign="middle" class="uptablheader" '.$sty.'>Coupon <br />Code </td>									
	<td align="center" valign="top" class="uptablheader" >Add to <br />shopping Cart </td>
	<td align="center" valign="middle" class="uptablheader" >Buy Now </td>
	</tr>';
	}			  
	$gps=1;			  
	foreach($rec as $row){
	$nme_s=$row['title'];
	$desc_s=$row['description'];
	if($this->table_prefix=='st_fr_'){
	$nme_s=$row['title_fr'];
	$desc_s=$row['description_fr'];	
	}					
	$strReturn .='
	<tr>
	<td width="125" height="31" align="center" valign="middle" >'.loadImage($row['file_name'],'uploads/mjvideo/image',125,94).'</td>
	<td width="71" align="center" valign="middle" class="txt1" >'.$row['price'].'$ </td>
	<td width="374" align="center" valign="middle" class="txt1" ><u><strong>'.$nme_s.'</strong></u><br />'.$desc_s.'</td>
	<td width="115" align="center" valign="middle" class="txt1" >1</td>
	<td width="115" align="center" valign="middle" class="txt1"  '.$sty.'>
	<input type="text" name="coupon_code_s" id="coupon_code_box_'.$gps.'" value=""  size="10"/>
	</td>							
	<td width="95" align="center" valign="middle" >
	<form name="frmAdd2Cart" id="frmAdd2Cart" action="" method="post">
	<input type="hidden" name="coupon_code_s" id="coupon_code_a_'.$gps.'" value=""  size="10"/>
	<input type="hidden" name="vid" value="'.$row['vid'].'">
	<input type="hidden" name="qty" value="1">
	<input type="hidden" name="sid" value="'.$nme_s.'" />                            
	<input type="hidden" name="price" value="'.$row['price'].'" />
	<input type="hidden" name="add2cart" value="1" />
	<input type="image" src="images/shoppingcart.png" style="cursor:pointer;"  onclick="return get_values_id(\'coupon_code_a_\','.$gps.',\'coupon_code_box_\')"/>
	</form>
	</td>
	<td width="99" align="center" valign="middle" ><form name="frmOrder" id="frmOrder" action="buy-video.php" method="post">
	  <input type="hidden" name="coupon_code_s" id="coupon_code_b_'.$gps.'" value=""  size="10"/>
	  <input type="hidden" name="vid" value="'.$row['vid'].'" />
	  <input type="hidden" name="sid" value="'.$nme_s.'" />                                          
	  <input type="hidden" name="price" value="'.$row['price'].'" />
	  <input type="hidden" name="paynow" value="1" />
	  <input type="image" src="images/buynow.png" width="99" height="35" style="cursor:pointer;" onclick="return get_values_id(\'coupon_code_b_\','.$gps.',\'coupon_code_box_\')" />
	</form></td>
	</tr>
	';
	if($ctr < $total_num){
	$strReturn .='<tr>
			<td height="10" colspan="6" align="center" valign="middle" ></td>
		  </tr>';
	}
	$ctr +=1;
	$gps++;
	}
	$strReturn .='</table>';
	}else{
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="center" valign="middle" style="color:#FFFFFF; height:100px;">

	Coming Soon...

	</td>
	</tr>
	</table>';
	}
	return $strReturn;
	}


	function getPressReleases(){
	global $obj_mysql,$site;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."banner where status='1'";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);

	if($total_num){

	$ctr=1;
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="left" valign="middle"  >';
	foreach($rec as $row){
	$strReturn .='
	<div class="picbg">
	<a  class="modal" href="'.$site['URL'].'uploads/banner/'.($row['file_name']).'">
	<div>'.loadImage($row['file_name'],'uploads/banner',125,94).'</div>
	<div class="pictxt">'.chars($row['title'],15).'</div>
	</a>
	</div>';
	$ctr +=1;
	}
	$strReturn .='</td>
	</tr>
	</table>';
	}else{
	$strReturn .='<table width="577" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td width="125" align="left" valign="middle">

	Coming Soon...

	</td>
	</tr>
	</table>';
	}
	return $strReturn;
	}

	function SendEmail($to,$subject,$body){
	global $mail;
	####################################################################################### SMTP MAIL START 
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->Host = "mail.yuvaart.com"; // SMTP server
	$mail->SMTPAuth = true;
	$mail->Username = "shadanand@yuvaart.com";
	$mail->Password = "shadanand@yuvaart.com";

	///$body="Name: " . $cname . "\nEmail: " . $cemail . "\nComments: " . $comments;

	$mail->From = 'noreply@mjstrazzero.com';
	$mail->FromName = 'MJ Website Team';

	$mail->AddAddress($to);
	//$mail->AddAddress($to2);
	$mail->AddCC('shadanand@gmail.com','Shadanand Pandeya');
	$mail->Subject = $subject;
	$mail->IsHTML(true); 
	$mail->Body = $body;
	$mail->WordWrap = 50;

	if(!$mail->Send())
	{
	//echo 'Message was not sent.';
	//echo 'Mailer error: ' . $mail->ErrorInfo;
	}
	else
	{
	//$obj_common->redirect("login.php?msg=fgp1");
	}
	}

	function getOnlineVideoArray(){
	global $obj_mysql;
	$strReturn = array();
	$sql = "SELECT * from ".TABLE_PREFIX."video where status='1' and lang='".$this->table_prefix."'";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	foreach($rec as $row){
	$strReturn[$row['vid']] = $row['title'];
	}
	}
	return $strReturn;
	}

	function getMemberArray(){
	global $obj_mysql;
	$strReturn = array();
	$sql = "SELECT * from ".TABLE_PREFIX."members where MemStatus='1'";
	$rec = $obj_mysql->getAllData($sql);
	$total_num=$obj_mysql->get_num_rows($sql);
	if($total_num){
	foreach($rec as $row){
	$strReturn[$row['MemId']] = $row['MemUname'];
	}
	}
	return $strReturn;
	}

	/*Code by me block */
	function getUpcomingEventInfo(){
	global $obj_mysql;
	$sql = "SELECT short_description,description,imgname,id from ".$this->table_prefix."event where status='1' ORDER BY event_date asc Limit 0,1";
	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}


	/* Get Services Details */

	function getServicesList()
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."category where status='1' and id!=9 ";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}

	function getServicesListById($id)
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."category where id=".$id." and status='1' limit 1";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}
	


	
	/* Get Team Members Details */

	function getTeamMemberList()
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."team_member where status='1' ";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}

	function getTeamMemberListById($id)
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."team_member where id=".$id." and status='1' limit 1";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}
		

   function getClassScheduleListById($id)
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."class_schedule where cat_content_id=".$id." and status='1'";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}
	

	function getClassScheduleHeadingById()
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."category where id=9 and status='1' limit 1";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}
	

	function getClassScheduleWeakDays()
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."category_content where cat_id=9 and status='1'";

	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}


	function getNewsById($id)
	{
	global $obj_mysql;
	$strReturn = '';
	$sql = "SELECT * from ".$this->table_prefix."news where status='1' AND id=$id  LIMIT 1;";
	$rec = $obj_mysql->getAllData($sql);
	return $rec;
	}

	function getLatestJobOpportunities()
	{
	global $obj_mysql;
	$sql = "SELECT * from ".$this->table_prefix."job_opportunities where status='1' order by id desc limit 1";
	$rec = $obj_mysql->get_assoc_arr($sql);
	return $rec;

	}




	/* Function For Front End End */
	}
	?>