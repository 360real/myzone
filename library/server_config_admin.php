<?php 
ob_start();
session_start();
date_default_timezone_set("Asia/Calcutta"); 
//ini_set('display_errors',1);
//error_reporting(E_ALL-E_NOTICE);	
set_time_limit(0);

/*================================= MySql Connectivity Variables Configuration Block Setting  ========================================*/


//============================ Local Environment ============================================
$info['db_un']		=	'360pro';
$info['db_pw']		=	'GnYKddd$#%6789dmz()t';
define('ROOT_PATH',$_SERVER['DOCUMENT_ROOT']."/");
define('MEDIA_ROOT_PATH',$_SERVER['DOCUMENT_ROOT']."/");
define('LuxuryMainBannerSavePath',$_SERVER['DOCUMENT_ROOT'].'/media_images/Luxury/MainBanner');
define('LuxuryPropertyBannerSavePath',$_SERVER['DOCUMENT_ROOT'].'/media_images/Luxury/PropertyBanners');
define('STATIC_URL', 'https://demo.360realtors.com/');

define('COMMERCIAL_URL', 'https://staging.360realtors.com/commercial/');

//============================ Local Environment ============================================

$info['db_name']	=	'cms_website_tech_staging';

/*================================= MySql Connectivity Variables Configuration Block Setting  ========================================*/

$info['db_host']	=	'srv-db-360.360prodns.com';

/*================================= Application Variables Configuration Block Setting  ========================================*/


$_SESSION['TABLE_PREFIX'] = ($_SESSION['TABLE_PREFIX'])?  $_SESSION['TABLE_PREFIX'] : "tsr_";
$site['TITLE'] = "myzone 360realtors.com";
define('C_ROOT_PATH',$_SERVER['DOCUMENT_ROOT']."/");
define('C_ROOT_URL',"http://stagingmyzone.360realtors.com/");
define('SITE_NAME','360Realtors');
define('SITE_EMAIL','abhijeet.kumar@360realtors.com');
$webSiteURL="http://stagingmyzone.360realtors.com/";
$site['URL'] = "http://stagingmyzone.360realtors.com/";
$site['CMSURL'] = "http://stagingmyzone.360realtors.com/";
$staticContentUrl = "http://static.360realtors.ws/";

$site['BUILDERLOGOURL']		=	$staticContentUrl.'developer/';
$site['PROPERTYURL'] = $staticContentUrl.'properties/photos/';
$site['TEMPLATELOGOURL']		=	$staticContentUrl.'templates/';
$site['PROPERTYBANNERURL'] = $staticContentUrl.'properties/photos/';
$site['MICROSITEIMAGEURL']		=	$staticContentUrl.'microsite/';

$site['VERIFICATIONCODEURL']		=	$staticContentUrl.'media_images/';

$site['BANNERLOGOURL']		=	$staticContentUrl.'banner/';   

$site['AUTHORLOGOURL']		=	$staticContentUrl.'authors/'; 
$site['ARTICLELOGOURL']		=	$staticContentUrl.'blog/';

$site['DEVELOPERTEMPLATELOGOURL']        = $staticContentUrl.'developer_templates/';

$site['TESTIMONIALIMAGEURL']		=	$staticContentUrl.'media_images/testimonial/';

$site['MICROSITEBLOGIMAGEURL']		=	$staticContentUrl.'media_images/microsite_blog/';

$regn_ip = $_SERVER['REMOTE_ADDR'];

$site['MICROSITEBLOGBANNERIMAGES']		=	$staticContentUrl.'media_images/microsite/';

$site['NEWSLOGOURL'] = $staticContentUrl.'news/';

$site['MICROSITESECTIONIMAGEURL']		=	$staticContentUrl.'media_images/microsite_section/';
$site['INTERNALNEWSURL']		=	'https://static.360realtors.ws/'.'internal_news/'; 
$site['STAGINGPROPERTYURL'] = 'http://stagingmyzone.360realtors.com/media_images/commercial/';

// $staticContentUrl.'internal_news/'
/*================================= Application Variables Configuration Block Setting  ========================================*/

define('UPLOAD_PATH_DEVELOPERS_LOGO',MEDIA_ROOT_PATH.'media_images/developer/');
define('UPLOAD_PATH_PROPERTY_IMAGE',MEDIA_ROOT_PATH.'media_images/properties/photos/');
define('UPLOAD_PATH_TEMPLATE_IMAGE',MEDIA_ROOT_PATH.'media_images/templates/');
define('UPLOAD_PATH_DEVELOPER_TEMPLATE_IMAGE',MEDIA_ROOT_PATH.'media_images/developer_templates/');
define('UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE',MEDIA_ROOT_PATH.'media_images/microsite/');

define('UPLOAD_PATH_MICROSITE_VERIFICATION_PATH',MEDIA_ROOT_PATH.'media_images/');

//Blog
define('UPLOAD_PATH_AUTHOR_LOGO',MEDIA_ROOT_PATH.'media_images/authors/');
define('UPLOAD_PATH_ARTICLE_LOGO',MEDIA_ROOT_PATH.'media_images/blog/');

define('UPLOAD_PATH_BANNER_LOGO',MEDIA_ROOT_PATH.'media_images/banner/');

define('P_ROOT_URL',"/var/www/html/staging/myzone/");
define('S_ROOT_URL',"http://stagingmyzone.360realtors.com/");

define('TABLE_TESTIMONIAL',"tsr_testimonial");
define('UPLOAD_PATH_TESTIMONIAL_IMAGES',MEDIA_ROOT_PATH.'media_images/testimonial/');

define('UPLOAD_PATH_MICROSITE_BLOG_IMAGES',MEDIA_ROOT_PATH.'media_images/microsite_blog/');

define('UPLOAD_PATH_NEWS_LOGO',MEDIA_ROOT_PATH.'media_images/news/');

define('UPLOAD_PATH_MICROSITE_SECTION_IMAGES',MEDIA_ROOT_PATH.'media_images/microsite_section/');
define('UPLOAD_PATH_INTERNAL_NEWS',MEDIA_ROOT_PATH.'media_images/internal_news/');
/*================================= Tables Aliasing Configuration Block Setting  ========================================*/
define('TABLE_PROPERTIES_RERA',"tsr_properties_rera");
define('ADMIN_USER',"tsr_users");
define('TABLE_ROLES',"tsr_roles");

define('TABLE_TEMPLATES',"tsr_templates");
define('TABLE_TEMPLATES_TYPES',"tsr_template_types");

define('TABLE_DEVELOPERS',"tsr_developers");
define('TABLE_DEVELOPER_TEMPLATES',"tsr_developer_templates");
define('TABLE_DEVELOPER_TEMPLATE_PROPERTY',"tsr_developer_template_property");

define('TABLE_KEY_FEATURES_HEADING',"tsr_key_features_headings");
define('TABLE_KEY_FEATURES_POINTS',"tsr_key_features_points");

define('TABLE_TYPES',"tsr_types");
define('TABLE_TYPES_CATEGORIES',"tsr_types_categories");

define('TABLE_COUNTRY',"tsr_countries");
define('TABLE_CITIES',"tsr_cities");
define('TABLE_LOCATIONS',"tsr_locations");
define('TABLE_CLUSTER',"tsr_cluster");
define('TABLE_META',"tsr_meta");
define('TABLE_WEB_BLOG',"tsr_custome_web_blog");

define('TABLE_PROPERTIES',"tsr_properties");
define('TABLE_IMAGES',"tsr_images");
define('TABLE_IMAGES_GALLAY',"tsr_images_gallary");
define('TABLE_PRICES',"tsr_prices");

define('TABLE_AMENITIES',"tsr_amenities");

define('TABLE_MICROSITES',"tsr_microsites");

define('TABLE_PPC_CAMPAIGN',"tsr_ppc_campaign");

define('TABLE_WEBSITE_PPC_CAMPAIGN',"tsr_website_ppc_campaign");

define('TABLE_AUTHORS',"tsr_article_authors");
define('TABLE_ART_CATEGORY',"tsr_blog_categories");
define('TABLE_SECTIONS',"tsr_blog_sections");
define('TABLE_BLOG',"tsr_blog");

define('TABLE_COMMENT_POSTS',"tsr_comment_posts");
define('TABLE_COMMENT_USERS',"tsr_comment_users");
define('TABLE_COMMENT_TOPICS',"tsr_comment_topics");


define('TABLE_DIMENSION',"tsr_dimension");
define('TABLE_SIZE',"tsr_size");
define('TABLE_BANNERS',"tsr_banner");

define('TABLE_SPECIALS',"tsr_specials");
define('TABLE_SPECIALS_PROJECTS',"tsr_special_projects");

define('TABLE_NEWS_LETTER',"tsr_enquiry");

define('TABLE_REVIEW_POSTS',"tsr_review_posts");
define('TABLE_REVIEW_TOPICS',"tsr_review_topics");

define('TABLE_DEVELOPERS_ORDERING',"tsr_developer_order_citywise");

define('TABLE_MICROSITES_PROJECTS',"tsr_microsites_template_projects");

define('TABLE_MICROSITE_BLOG',"tsr_microsite_blog");
define('TABLE_MICROSITE_NEWS',"tsr_microsite_news");

define('TABLE_MICROSITE_COMMENT_POSTS',"tsr_microsite_comment_posts");
define('TABLE_MICROSITE_COMMENT_USERS',"tsr_microsite_comment_users");

define('TABLE_NEWS',"tsr_web_news");
define('TABLE_NEWS_SECTION',"tsr_news_section");
define('TABLE_QUESTION',"tsr_questions");
define('TABLE_ANSWERS',"tsr_answers");
define('TABLE_MICROSITES_DEVELOPERS',"tsr_microsites_template_developers");

define('TABLE_MICROSITES_CATEGORY_OVERVIEW',"tsr_microsite_category_overviews");
define('TABLE_MICROSITES_META_DETAILS',"tsr_meta_microsites");

define('TABLE_INTERNAL_NEWS',"tsr_internal_news");
define('TABLE_NEWS_TAGS_ID',"tsr_news_tags");
define('TABLE_NEWS_TAGS',"tsr_tags");
define('TABLE_INTERNAL_NEWS_BANNERS',"tsr_internal_news_images");
define('TABLE_EVENTS',"tsr_events");
define('TABLE_COMMERCIAL_PROPERTIES',"tbl_properties_commercial");
define('TABLE_COMMERCIAL_AMENITIES',"tsr_commercial_amenities");
define('TABLE_COMMERCIAL_BROKER',"tsr_commercial_broker");
define('TABLE_COMMERCIAL_SPECIFICATION',"tsr_commercial_specifications");
define('TABLE_AREA_UNIT',"tsr_area_unit");
define('TABLE_LANDZONE',"tsr_landzone");
define('TABLE_SALE_TYPE',"tsr_sale_type");
define('TABLE_PULLED_PROPERTY'," tsr_pulled_property");
define('TABLE_PROPERTY_TYPE',"tsr_property_type");
define('TABLE_COMMERCIAL_IMAGE_TYPE',"tsr_commercial_image_type");
define('TABLE_COMMERCIAL_IMAGE',"tsr_commercial_images");
define('TABLE_USER_MAPPING',"tsr_users_mapping_operation");

define('UPLOAD_PATH_COMMERCIAL_PROPERTY_IMAGE',MEDIA_ROOT_PATH.'media_images/commercial/');
define('UPLOAD_PATH_COMMERCIAL_PROPERTY_FILE',MEDIA_ROOT_PATH.'media_import/');

define('TABLE_MICRO_MARKET',"tsr_micro_market");
define('TABLE_COMMERCIAL_PRICES',"tsr_commercial_prices");
define('TABLE_REASSIGN_PROPERTY_HISTORY',"tsr_reassign_property_history");
define('USER_MAIL',"gaurav.singh@360realtors.com");
define('REPORT_USER_MAIL',"gaurav.singh@360realtors.com");
define('TABLE_COMMERCIAL_MICRO_MARKET',"tsr_commercial_micro_market");


define('MEDIA_SERVER_URL','https://static.360realtors.ws/');

define('TABLE_WEEKLY_NEWS',"tsr_weekly_news");
define('UPLOAD_PATH_WEEKLY_NEWS',MEDIA_ROOT_PATH.'media_images/weekly_news/');
define('TABLE_WEEKLY_NEWS_TAGS_ID',"tsr_weekly_news_tags");
$site['WEEKLYNEWSURL']		=	'https://stagingstatic.360realtors.ws/weekly_news/'; 
define('TABLE_WEEKLY_NEWS_BANNERS',"tsr_weekly_news_images");

define("VP_MARKETING","asha.singh@360realtors.com");

define("TABLE_COMMERCIAL_SERVICES","tsr_commercial_services");
define("TABLE_COMMERCIAL_BUILDING_TYPE","tsr_commercial_building_type");
define("TABLE_PROPERTIES_POSSESSION_DATE","tsr_properties_possession_date");


// xlr8 website define
define("TABLE_XLR8_MISSION","tsr_xlr8_website_mission");
define("TABLE_XLR8_IN_A_GLANCE","tsr_xlr8_website_in_a_glance");
define("TABLE_XLR8_LEADERSHIP","tsr_xlr8_website_leadership");
define("TABLE_XLR8_INFO","tsr_xlr8_website_info");
define("TABLE_XLR8_ESSENTIAL_READING", "tsr_xlr8_essential_reading");
define("TABLE_XLR8_RESOURCE","tsr_xlr8_website_resource");
define("TABLE_XLR8_RESOURCE_CATEGORY","tsr_xlr8_website_resource_category");
define("TABLE_XLR8_WRITE_TO_US","tsr_xlr8_website_write_to_us");
define("TABLE_XLR8_HOW_IT_WORKS","tsr_xlr8_website_how_it_works");
define("TABLE_XLR8_SERVICE_CATEGORY","tsr_xlr8_website_service_category");
define("TABLE_XLR8_SERVICES","tsr_xlr8_website_services");
define("TABLE_XLR8_TESTIMONIAL","tsr_xlr8_website_testimonial");
define("TABLE_XLR8_DEVELOPERS","tsr_xlr8_website_developers");
define("TABLE_XLR8_MEDIA_SECTION","tsr_xlr8_website_media_section");
define("TABLE_XLR8_WHAT_WE_DO","tsr_xlr8_website_what_we_do");



// 360edge website define
define("TABLE_360EDGE_FRANCHISE","tsr_360edge_franchise");
define("TABLE_360EDGE_ENQUIRY","tsr_edge_enquiry");
define("TABLE_360EDGE_TEAM","tsr_360edge_team");
define("TABLE_SCO","tsr_sco_countrynumber");


$site['XLR8_WEBSITE']		=	$staticContentUrl.'xlr8/';
$site['360EDGE_WEBSITE']		=	$staticContentUrl.'360edge/';



define('TABLE_COMMERCIAL_PROPERTIES_CLUB_NAME',"tbl_properties_commercial_club_name");
//define('TABLE_SUB_LOCATION',"tsr_sub_location");
//define('TABLE_GENERIC_CAMPAIGN',"tsr_internal_news_images");

/*================================= Tables Aliasing Configuration Block Setting  ========================================*/


/*================================= Common Modules Configuration Block Setting ========================================*/

require_once(ROOT_PATH."library/mysqli_function.php");
require_once(ROOT_PATH."library/function.php");
require_once(ROOT_PATH."library/class.mainframe.php");
//require_once(ROOT_PATH."library/send_mail.php");
require_once(ROOT_PATH."library/main_common_function.php");
require_once(ROOT_PATH."library/paging.php");
require_once(ROOT_PATH."library/paging_ajax.php");
require_once(ROOT_PATH."library/pager.cls.php");
require_once(ROOT_PATH."library/class.upload.php");
//require_once(INCLUDE_PATH."class.phpmailer.php");
//$mail = new PHPMailer();

/*================================= Common Modules Configuration Block Setting ========================================*/


/*================================= Mysql Functions Connection Configuration Block Setting ========================================*/

$obj_mysql		=	new mysqli_function(); 	//Object of Mysql Function
$obj_common		=	new main_common();  	//Object of Common Function
$obj_paging		=	new clsPaging(); 
$obj_paging_ajax=	new clsPagingAjax(); 		//Object for Paging
$objMainframe = new clsMainframe(TABLE_PREFIX_ENGLISH);
$obj_mysql->connect($info['db_host'],$info['db_un'],$info['db_pw'],$info['db_name']);

/*================================= Mysql Functions Connection Configuration Block Setting ========================================*/
$site['URL'] = "http://stagingmyzone.360realtors.com/";
?>
