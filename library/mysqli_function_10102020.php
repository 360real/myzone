<?php
  /*----------------*
   *Function name
   
   *exec_query()  For executeing any query
   *get_assoc_arr()  For getting a assosiative data array
   *Duplicate()    For getting whether the value is duplicate in the table or not
   *get_num_rows()      For getting a number of rows efftected
   *getAllData()      For All data as array associative array
   *-----------------*/
  class mysql_function
  {
      var $sql = '';
	  var $link = '';
      function connect($host = '', $un = '', $pwd = '', $db = '')
      {
          $link = @mysql_connect($host, $un, $pwd) or die('<br />Error No.:' . mysql_errno() . '<br />' . mysql_error() . '<br /><br />');
          mysql_select_db($db, $link);
		  $this->link = $link ;
		  
          return $link;
		  
      }
      function stripMagicQuotes($arr)
      {
          if (is_array($arr)) {
              foreach ($arr as $k => $v) {
                  $arr[$k] = is_array($v) ? stripMagicQuotes($v) : stripslashes($v);
              }
          }
          return $arr;
      }
      function rem_gbfx($str)
      {
          $str = mysql_real_escape_string($str);
          return $str;
      }
      function exec_query($sql, $link = '')
      {
          if ($link)
              $res = mysql_query($sql, $link);
          else
              $res = mysql_query($sql);
          if (mysql_error())
             die(mysql_errno() . ": " . mysql_error() . "<br /><span class='error'>$sql</span><br /><br />");
			//die("<div style='margin-top:40px;margin-auto;text-align:left;font-size:25px;'>No Result Found !</div>");
          return $res;
      }
      function insert_query($sql, $link = '')
      {
          if ($link)
              $res = mysql_query($sql, $link);
          else
              $res = mysql_query($sql);
          if (mysql_error())
              //die(mysql_errno() . ": " . mysql_error() . "<br /><span class='error'>$sql</span><br /><br />");
			   die("<div style='margin-top:40px;margin-auto;text-align:left;font-size:25px;'>No Result Found !</div>");
          return mysql_insert_id();
      }
	  function update_query($sql, $link = '')
      {
			if ($link)
			  $res = mysql_query($sql, $link);
			else
			  $res = mysql_query($sql);
			if (mysql_error())
			 // die(mysql_errno() . ": " . mysql_error() . "<br /><span class='error'>$sql</span><br /><br />");
			die("<div style='margin-top:40px;margin-auto;text-align:left;font-size:25px;'>No Result Found !</div>");
			
			if(mysql_affected_rows())
				return true;
			else
				return false;
      }
      function get_assoc_arr($sql, $link = '')
      {          
          $res = $this->exec_query($sql, $link);
          $rec = $this->stripMagicQuotes(mysql_fetch_assoc($res));
          return $rec;
      }
      function get_fetch_arr($sql, $link = '')
      {
          $res = $this->exec_query($sql, $link);
          $rec = $this->stripMagicQuotes(mysql_fetch_array($res));
          return $rec;
      }
      function get_row_arr($sql, $link = '')
      {
          $res = $this->exec_query($sql, $link);
          $rec = $this->stripMagicQuotes(mysql_fetch_row($res));
          return $rec;
      }
      function get_num_rows($sql, $link = '')
      {
          $res = $this->exec_query($sql, $link);
          $num = mysql_num_rows($res);
          return $num;
      }

      /*function insertInNewCrm($data, $action) 
      {
	unset($data['viewType']);
        $url = "https://newcrm.360crm.in/360crm/UpdateTables/$action";

        $dataPostFormValuesForLead = json_encode($data);
        $headers = array(
        'Accept: application/json',
        'Content-Type: application/json');

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $dataPostFormValuesForLead);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($curl);
        // print_r($result);
        curl_close($curl);
      }*/
      function insertInNewCrm($data, $action, $table_name='') 
      {
        
        $url = "https://www.360crm.in/UpdateTables/$action";
        
        if ($table_name == 'tsr_properties' && $action == 'AddProjectInTable' && !empty($data['loc_id'])) {
            $locId = $data['loc_id'];
            $cityId = $data['cty_id'];
            $locName = $this->getLocName($locId);
            $cityName = $this->getCityName($cityId);
            $data['locationName'] = $locName;
            $data['cityName'] = $cityName;
        } else if ($table_name == 'tsr_properties' && $action == 'UpdateProjectTable' && !empty($data[0]['loc_id'])) {
            $locId = $data[0]['loc_id'];
            $cityId = $data[0]['cty_id'];
            $locName = $this->getLocName($locId);
            $cityName = $this->getCityName($cityId);
            $data[0]['locationName'] = $locName;
            $data[0]['cityName'] = $cityName;
        }

        $dataPostFormValuesForLead = json_encode($data);
        $headers = array(
        'Accept: application/json',
        'Content-Type: application/json');

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $dataPostFormValuesForLead);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($curl);
        // print_r($result);
        curl_close($curl);
      }

      function insert_data($table_name, $data_array, $link = '')
      {
          $arrForNewCRM = array();
          $fld_str = '';
          $val_str = '';
          if ($table_name && is_array($data_array)) {
              $sql = "SHOW COLUMNS FROM `$table_name`";
              $columns_query = $this->exec_query($sql, $link);
              while ($coloumn_data = mysql_fetch_assoc($columns_query))
                  $column_name[] = $coloumn_data[Field];
              foreach ($data_array as $key => $val) {
                  if (in_array($key, $column_name)) {
                      if (!empty($val)) {
                        $arrForNewCRM[$key] = $val;
                      }
                      
                      $fld_str .= "$key,";
                      if ($val == 'now()')
                          $val_str .= mysql_real_escape_string(trim($val)) . ",";
                      else
                          $val_str .= "'" . mysql_real_escape_string(trim($val)) . "',";
                  }
              }
              $fld_str = substr($fld_str, 0, -1);
              $val_str = substr($val_str, 0, -1);

/*============== Start:Update CRM Table ============================*/
if(in_array($table_name, array('tsr_properties', 'tsr_locations', 'tsr_cluster', 'tsr_cities', 'tsr_developers')))
{
$dbh2 = @mysql_connect('srv-crmpro-360.360prodns.com', '360app', 'GmtCyf#$%!234DDYtr'); 
mysql_select_db('360gyan', $dbh2);
$setTableForCrm = str_replace('tsr_','tsr_',$table_name);
$sql2 = "INSERT INTO $table_name($fld_str) VALUES($val_str)";  			 

mysql_query($sql2, $dbh2);
}
/*============== End:Update CRM Table ============================*/

//$dbh1 = @mysql_connect('172.22.22.11', 'root', 'power@360RealTors');
$dbh1 = @mysql_connect('localhost', 'root', 'power@360RealTors'); 
mysql_select_db('cms_website_tech', $dbh1);
$sql = "INSERT INTO $table_name($fld_str) VALUES($val_str)";  			 
$this->exec_query($sql, $dbh1);

$recordId = mysql_insert_id();  

// sink with new crm
  // $this->sinkWithCRM($table_name,$sql);
/*if($table_name!="tsr_countries")
{*/
$data_array['table_name']="jos_".$table_name;
/*}
else
{
$data_array['table_name']="jos_country";
}
*/
//    $this->insertInNewCrm($data_array, "SyncWithCommercial");

// sink with new crm end
             

              $fieldarr = array(

                'tsr_properties' => 'property',

                'tsr_developers' => 'developer',

                'tsr_cities' => 'city',

                'tsr_locations' => 'location',

                'tsr_cluster' => 'sublocation'

            );

            

            if (array_key_exists($table_name, $fieldarr)) {

                $this->callUrlforIndexing($recordId);

			}
              //$sql = "INSERT INTO $table_name($fld_str) VALUES($val_str)";       
			  //echo $sql;
               //$this->exec_query($sql, $link);
	    if ($table_name == 'tsr_developers') {
                
                $this->insertInNewCrm($arrForNewCRM,'AddDeveloper',$table_name);
            }
            if ($table_name == 'tsr_properties') {

                $this->insertInNewCrm($arrForNewCRM,'AddProjectInTable',$table_name);
            }
              //return mysql_insert_id();
		return $recordId;
	
          }
      }
      function update_data($table_name, $match_fld, $data_array, $rec_id, $link = '')
      {
          $fld_str = '';
          $val_str = '';
          if ($table_name && is_array($data_array)) {
              $sql = "SHOW COLUMNS FROM `$table_name`";
              $columns_query = $this->exec_query($sql, $link);
              while ($coloumn_data = mysql_fetch_assoc($columns_query))
                  $column_name[] = $coloumn_data[Field];
              foreach ($data_array as $key => $val) {
                  if (in_array($key, $column_name)) {
                    if (!empty($val)) {
                        $arrForNewCRM[$key] = $val;
                      }
                      $fld_str .= "$key,";
                      if ($val == 'now()')
                          $val_str .= "$key=" . mysql_real_escape_string(trim($val)) . ",";
                      else
                          $val_str .= "$key='" . mysql_real_escape_string(trim($val)) . "',";
                  }
              }
              $val_str = substr($val_str, 0, -1);

			if(in_array($table_name, array('tsr_properties', 'tsr_locations', 'tsr_cluster', 'tsr_cities', 'tsr_developers')))
			  {
/*============== Start:Update CRM Table ============================*/
$dbh2 = @mysql_connect('srv-crmpro-360.360prodns.com', '360app', 'GmtCyf#$%!234DDYtr');
mysql_select_db('360gyan', $dbh2);
$setTableForCrm = str_replace('tsr_','tsr_',$table_name);
$sql2 = "UPDATE $setTableForCrm SET $val_str WHERE $match_fld ='$rec_id'";				 
mysql_query($sql2, $dbh2);

/*============== End:Update CRM Table ============================*/

//$dbh1 = @mysql_connect('172.22.22.11', 'root', 'power@360RealTors');
$dbh1 = @mysql_connect('localhost', 'root', 'power@360RealTors'); 
mysql_select_db('cms_website_tech', $dbh1);
$sql = "UPDATE `$table_name` SET $val_str WHERE $match_fld ='$rec_id'";				 
$this->exec_query($sql, $dbh1);

// sink with new crm
  // $this->sinkWithCRM($table_name,$sql);
$data_array['table_name']="jos_".$table_name;
$data_array['id']=$rec_id;
//$this->insertInNewCrm($data_array, "SyncWithCommercial");

// sink with new crm end


			} else {

              $sql = "UPDATE `$table_name` SET $val_str WHERE $match_fld ='$rec_id'";
			   echo $sql;					 
              //$this->exec_query($sql, $link);
			  $this->exec_query($sql, $link);
		// sink with new crm
	  // $this->sinkWithCRM($table_name,$sql);
	/*if($table_name!="tsr_countries")
	{*/
		$data_array['table_name']="jos_".$table_name;
	/*}
	else
	{
		$data_array['table_name']="jos_country";
	}*/

	$data_array['id']=$rec_id;

//	$this->insertInNewCrm($data_array, "SyncWithCommercial");

	// sink with new crm end
	
	if ($table_name == 'tsr_properties') {

    $arrForNewCRMup[0] = $arrForNewCRM;
    $arrForNewCRMup[1] = $rec_id;

    $this->insertInNewCrm($arrForNewCRMup,'UpdateProjectTable',$table_name);
    $this->updatePropertyData($data_array,$rec_id);
}

  	return  $query;

			
			}
$fieldarr = array(

                'tsr_properties' => 'property',

                'tsr_developers' => 'developer',

                'tsr_cities' => 'city',

                'tsr_locations' => 'location',

                'tsr_cluster' => 'sublocation'

            );



            if (array_key_exists($table_name, $fieldarr)) {

                $this->callUrlforIndexing($rec_id);

			}			

            if ($table_name == 'tsr_properties') {

                $arrForNewCRMup[0] = $arrForNewCRM;
                $arrForNewCRMup[1] = $rec_id;
                // echo "<pre>";
                // print_r($arrForNewCRMup);die;

                $this->insertInNewCrm($arrForNewCRMup,'UpdateProjectTable',$table_name);
            }

              return mysql_affected_rows();
          }
      }
      function callUrlforIndexing($projectId) {
//ini_set('display_errors',1);
//error_reporting(-1);
        $url ="https://360realtors.com/cronjob/indexSingleProject?projectId=".$projectId;

         $ch = curl_init( $url );

        # Setup request to send json via POST.
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTREDIR, 3);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $projectId );

        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        # Return response instead of printing.

        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
//header('Content-Type: image/jpeg');
 //   header("Access-Control-Allow-Origin: *");
        # Send request.
//	echo curl_error($ch);die;
        $result = curl_exec($ch);

        curl_close($ch);

    }
      function getAllData($sql, $link = '')
      {          
          $res = $this->exec_query($sql, $link);
          
          while ($row = $this->stripMagicQuotes(mysql_fetch_assoc($res))) {
              $outResult[] = $row;
          }
          
          return $outResult;
      }
      function getAllRowData($sql, $link = '')
      {
          $res = $this->exec_query($sql, $link);
          while ($row = $this->stripMagicQuotes(mysql_fetch_row($res))) {
              $outResult[] = $row;
          }
          return $outResult;
      }
      function delRecord($tbl_name, $fld_id, $fld_arr_id, $link = '')
      {
          //if (is_array($fld_arr_id)) {
              //foreach ($fld_arr_id as $key => $val) {
                  $sql = "DELETE FROM `$tbl_name` WHERE $fld_id='$fld_arr_id'";
                  $this->exec_query($sql, $link);
             // }
          //}
      }
      function changeStatus($tbl_name, $fld_id, $fld_arr_id, $fld_status, $status, $link = '')
      {
          if (is_array($fld_arr_id)) {
              foreach ($fld_arr_id as $key => $val) {
                  $sql = "UPDATE `$tbl_name` SET $fld_status='$status' WHERE md5($fld_id)='$val'";
                  $this->exec_query($sql, $link);
              }
          }
      }

	
      function isDuplicateLoc($tbl_name, $fld_name, $fld_value, $fld_cmp_name, $fld_cmp_value)
      {
          $sql = "SELECT * FROM `$tbl_name` WHERE $fld_name ='" . mysql_real_escape_string($fld_value) . "' AND $fld_cmp_name ='" . mysql_real_escape_string($fld_cmp_value) . "' LIMIT 0,1 ";
          $num = $this->get_num_rows($sql);
          if ($num == 0)
              $valid = true;
          else
              $valid = false;
          return $valid;
      }


      function isDuplicate($tbl_name, $fld_name, $fld_value)
      {
          $sql = "SELECT * FROM `$tbl_name` WHERE $fld_name ='" . mysql_real_escape_string($fld_value) . "' LIMIT 0,1 ";
          $num = $this->get_num_rows($sql);
          if ($num == 0)
              $valid = true;
          else
              $valid = false;
          return $valid;
      }
      function isDupUpdate($tbl_name, $fld_name, $fld_value, $fld_id, $id_value)
      {
          $sql = "SELECT * FROM `$tbl_name` WHERE $fld_name ='$fld_value' and $fld_id != '" . mysql_real_escape_string($id_value) . "' ";
		  echo $sql;
          $num = $this->get_num_rows($sql);
          if ($num == 0)
              return true;
          else
              return false;
      }
	  
		
      function dispaly_page_data($access_id)
      {
          $sql = "SELECT *  FROM `marks_content` WHERE accessKey='$access_id' LIMIT 0,1 ";
          $rec = $this->get_assoc_arr($sql);
          return $rec;
      }
      function get_field($table, $field, $match_field, $val, $md5 = '')
      {
          if ($md5 == '')
              $sql = "SELECT " . $field . " FROM " . $table . " WHERE " . $match_field . "='" . $val . "' ";
          else
              $sql = "SELECT " . $field . " FROM " . $table . " WHERE md5(" . $match_field . ")='" . $val . "' ";
          $result = $this->get_row_arr($sql);
          return $result[0];
      }
	  function getAllDataResultSet($sql, $link = '')
      {          
          $res = $this->exec_query($sql, $link);
          return $res;
      }
	function close()
	  {
		mysql_close($this->link);
	  }

	  function delRecordArray($tbl_name, $fld_id, $fld_arr_id, $source, $link = '')
      {
          //if (is_array($fld_arr_id)) {
              //foreach ($fld_arr_id as $key => $val) {

                  $sql = "DELETE FROM `$tbl_name` WHERE $fld_id in ($fld_arr_id) and source='$source' ";
                  print_r($sql);
                  $this->exec_query($sql, $link);
             // }
          //}
      }


      
function isDuplicateWithTwoFields($tbl_name, $fld_name, $fld_value, $fld_name2, $fld_value2)
{
$sql = "SELECT * FROM `$tbl_name` WHERE $fld_name ='" . mysql_real_escape_string($fld_value) . "' and $fld_name2='$fld_value2' LIMIT 0,1 ";
$num = $this->get_num_rows($sql);
if ($num == 0)
$valid = true;
else
$valid = false;
return $valid;
}

function isDupUpdateWithTwoFields($tbl_name, $fld_name, $fld_value, $fld_id, $id_value, $fld_name2, $fld_value2)
{
$sql = "SELECT * FROM `$tbl_name` WHERE $fld_name ='$fld_value' and $fld_name2='$fld_value2' and $fld_id != '" . mysql_real_escape_string($id_value) . "' ";
echo $sql;
$num = $this->get_num_rows($sql);
if ($num == 0)
return true;
else
return false;
}

function getLocName($locId) 
      {
        $sql = "select location from tsr_locations where id = $locId";
        $locationName = $this->getAllData($sql);
        return $locationName[0]['location'];
      }

      function getCityName($cityId)
      {
        $sql = "select city from tsr_cities where id = $cityId";
        $cityName = $this->getAllData($sql);
        return $cityName[0]['city'];
      }

function callCurl($id)

{
  
$headers = array
(
      'Accept: application/json',
     'Content-Type: application/json'
);          

 
$ch = curl_init();
 $url = "https://www.360realtors.com/search/keywordindex/?id=".$id;
curl_setopt( $ch,CURLOPT_URL,$url );


curl_setopt( $ch,CURLOPT_HTTPGET, 1);
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );

curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

//curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch,CURLOPT_VERBOSE,true);
 $result = curl_exec($ch);

// curl_error($ch);
curl_close ($ch);
}

function insert_table_data($table_name, $data_array, $link = '')
      {
    //print_r($data_array);
          $arrForNewCRM = array();
          $fld_str = '';
          $val_str = '';
          if ($table_name && is_array($data_array)) {
              $sql = "SHOW COLUMNS FROM `$table_name`";
              $columns_query = $this->exec_query($sql, $link);
             // print_r(mysql_fetch_assoc($columns_query));
              while ($coloumn_data = mysql_fetch_assoc($columns_query))
                  $column_name[] = $coloumn_data[Field];
              foreach ($data_array as $key => $val) {
                  if (in_array($key, $column_name)) {
                      /*if ($val!=="") {
                        $arrForNewCRM[$key] = $val;
                      }*/
                      
                      $fld_str .= "$key,";
                      if ($val == 'now()')
                          $val_str .= mysql_real_escape_string(trim($val)) . ",";
                      else
                          $val_str .= "'" . mysql_real_escape_string(trim($val)) . "',";
                  }
              }
              /*echo $fld_str;
              echo"<br />";
              echo $val_str;
              echo"<br />";
              exit;*/
              $fld_str = substr($fld_str, 0, -1);
              $val_str = substr($val_str, 0, -1);
 
 
 
              //$dbh1 = @mysql_connect('srv-crmpro-360.360prodns.com', '360app', 'GmtCyf#$%!234DDYtr'); 
              //mysql_select_db('cms_website_tech', $dbh1);
              $sql = "INSERT INTO $table_name($fld_str) VALUES($val_str)";       
              $this->exec_query($sql, $link);
              $recordId = mysql_insert_id();  
 
              // sink with new crm
              // $this->sinkWithCRM($table_name,$sql);
              //$data_array['table_name']="jos_".$table_name;
              $data_array['table_name']="jos_".$table_name;
              if(array_key_exists('remarks', $data_array)):
                  $data_array['remarks'] = mysql_real_escape_string($data_array['remarks']);
              endif;
              $this->insertInNewCrm($data_array, "SyncWithCommercial");
 
              // sink with new crm end
 
              return $recordId;
          }
      }
      
      
      function update_commercial_data($table_name, $match_fld, $data_array, $rec_id, $link = '')
      {
          $fld_str = '';
          $val_str = '';
          if ($table_name && is_array($data_array)) {
              $sql = "SHOW COLUMNS FROM `$table_name`";
              $columns_query = $this->exec_query($sql, $link);
              while ($coloumn_data = mysql_fetch_assoc($columns_query))
                  $column_name[] = $coloumn_data[Field];
              foreach ($data_array as $key => $val) {
                  if (in_array($key, $column_name)) {
                    if ($val!=="") {
                        $arrForNewCRM[$key] = $val;
                      }
                      $fld_str .= "$key,";
                      if ($val == 'now()')
                          $val_str .= "$key=" . mysql_real_escape_string(trim($val)) . ",";
                      else
                          $val_str .= "$key='" . mysql_real_escape_string(trim($val)) . "',";
                  }
              }
              $val_str = substr($val_str, 0, -1);
 
 
 
  $sql = "UPDATE `$table_name` SET $val_str WHERE $match_fld ='$rec_id'";
  $query = $this->exec_query($sql, $link);
  
  // sink with new crm
  // $this->sinkWithCRM($table_name,$sql);
  $data_array['table_name']="jos_".$table_name;
  if(array_key_exists('remarks', $data_array)):
  $data_array['remarks'] = mysql_real_escape_string($data_array['remarks']);
  endif;

  //$data_array['table_name']="jos_".$table_name;
  $data_array['id']=$rec_id;
  $this->insertInNewCrm($data_array, "SyncWithCommercial");
 
  // sink with new crm end
  
  return  $query; 
  return mysql_affected_rows();
}
}
  }
?>
