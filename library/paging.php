<?php
class clsPaging
{
    var $limit;
    var $lower;
    var $upper;
    var $isNext = false;
    var $isPre = false;
    var $filename;
    function set_lower_upper($page)
    {
        if (isset($page)) {
            $this->lower = $page * $this->limit;
        } else {
            $this->lower = 0;
        }
        $this->upper = $this->lower + $this->limit;
    }
    function next_pre($page, $nrows, $filename, $class = '', $sal_class = '')
    { 
        if (!isset($page))
            $page = 0;
        $pageUp = $page + 1;
        $pageDw = $page - 1;
        if ($this->upper >= ($nrows + $this->limit) || $this->lower > $nrows || $this->lower < 0 || $this->lower > $this->upper) {
        } elseif ($nrows > 0) {
            if ($nrows <= $this->limit) {
                $this->isNext = false;
                $this->isPre  = false;
            } elseif ($this->lower == 0 && $this->upper < $nrows) {
                $this->isNext = true;
                $this->isPre  = false;
            } elseif ($this->upper > $nrows && $this->lower > 0) {
                $this->isNext = false;
                $this->isPre  = true;
            } elseif ($this->upper < $nrows) {
                $this->isNext = true;
                $this->isPre  = true;
            } else {
                $this->isNext = false;
                $this->isPre  = true;
            }
            
            
            $leftlower = $this->lower + 1;
            $tPages    = ($nrows / $this->limit);
            $slot      = 15;
            if ($tPages > $slot) {
                $div = (int) ($tPages / $slot);
                $fst = ((int) ($page / $slot)) * $slot;
                $lst = $fst + $slot;
                $no  = 1;
                $str="";
       //          $str .= '<table class="smallblack" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
							// <tr><td colspan="3"></td></tr>
							 
							// <tr>
							//  <td  align="right">
							//   <table class="smallblack" width="30" border="0" cellspacing="0" cellpadding="0">
							//    <tr>
							// 	<td align="center" valign="middle">';
                if ($fst >= $slot) {
                    $imgCount = $fst - 1;
                    $str .= "<a href=\"$filename page_no=$imgCount\" class='LinkBlue1'><b>...</b></a>&nbsp;";
                }
        //         $str .= '</td>
								// <td align="center" valign="middle">';
                if ($this->isPre) {
                    $str .= "<a href=\"$filename page_no=$pageDw\" class=\"LinkBlue1\">Previous</a>&nbsp;";
                }
        //         $str .= "</td>
							 //   </tr>";
        //         $str .= '</table></td>
							 // <td align="center" width="60%">';
                if ($tPages < $slot) {
                    if ($this->isPre) {
                        $str .= "<a href=\"$filename page_no=$pageDw\" class=\"red_link1\"><strong>&laquo; Previous</strong></a>";
                    }
                    if ($this->isNext) {
                        $str .= "<a href=\"$filename";
                        $str .= "page_no=$pageUp\" class=\"red_link1\"><strong>Next &raquo;</strong></a>";
                    }
                }
                for ($i = $fst; $i < $lst; $i++) {
                    $j = $i + 1;
                    if ($i < $tPages) {
                        if ($i == $page)
                            $str .= "<font class='LinkRed1' ><b>$j</b></font>  ";
                        else {
                            $str .= "<a href=\"$filename";
                            $str .= "page_no=$i\" class=\"LinkBlue1\">$j</font></u></a>  ";
                        }
                        $no = $j;
                    }
                }
       //          $str .= '</td>
						 // <td  align="left">
						 //  <table class="smallblack" width="30" border="0" cellspacing="0" cellpadding="0">
						 //   <tr>
							// <td align="center" valign="middle">
						 //  ';
                if ($this->isNext) {
                    $str .= "<a href=\"$filename page_no=$pageUp\" class=\"LinkBlue1\">Next</a>&nbsp;";
                }
                // $str .= "</td><td align=\"right\" valign=\"middle\">";
                if ($no < $nrows && $no < $tPages) {
                    $str .= "<a href=\"$filenamepage_no=$no\" class=\"LinkBlue1\"><b>.....</b></a>";
                }
      //           $str .= "</td>
						//    </tr>
						//   </table></td></tr>
						// </table>";

                return $str;
            } else {

                if ($nrows > $this->limit) {
                    if ($tPages < $slot) {
                        if ($this->isPre) {
                            $str .= "<a href=" . $filename . "page_no=$pageDw\" class=\"$class\">&laquo; Previous</a>&nbsp;";
                        }
                        for ($i = 0; $i < $tPages; $i++) {
                            $j = $i + 1;
                            if ($page == $i)
                                $str .= "<span class=\"$sal_class\"><b>$j</b></span>  ";
                            else {
                                $str .= "<a href=\"$filename" . page_no . "=$i\" class=\"$class\">$j</a>  ";
                            }
                        }
                        if ($this->isNext) {
                            $str .= "&nbsp;<a href=\"$filename" . page_no . "=$pageUp\" class=\"$class\"> Next &raquo;</a>";
                        }
                    }
                    return $str;
                }
            }
        }
    }
    function show_paging($page, $nrows, $filename, $class = '', $sal_class = '')
    {
        if (!isset($page))
            $page = 0;
        
        if ($nrows <= $this->limit) {
            $this->isNext = false;
            $this->isPre  = false;
        } elseif ($this->lower == 0 && $this->upper < $nrows) {
            $this->isNext = true;
            $this->isPre  = false;
        } elseif ($this->upper > $nrows && $this->lower > 0) {
            $this->isNext = false;
            $this->isPre  = true;
        } elseif ($this->upper < $nrows) {
            $this->isNext = true;
            $this->isPre  = true;
        } else {
            $this->isNext = false;
            $this->isPre  = true;
        }
        $leftlower = $this->lower + 1;
        $tPages    = ($nrows / $this->limit);
        $slot      = 20;
		
		#############################
		if ($page < $tPages) {
            $pageUp = $page + 1;
        } else {
            $pageUp = $tPages;
        }
        if ($page > 0) {
            $pageDw = $page - 1;
        } else {
            $pageDw = 0;
        }
		#############################
        if ($tPages > $slot) {
            $div = (int) ($tPages / $slot);
            $fst = ((int) ($page / $slot)) * $slot;
            $lst = $fst + $slot;
            $no  = 1;
            $str = '';
            if ($fst >= $slot) {
                $imgCount = $fst - 1;
                $str .= "<a href=\"" . $filename . "page_no=$imgCount\"><span>...</span></a>&nbsp;";
            }
            $str .= '';
            /*if ($this->isPre) {*/
            $str .= "<a href=\"" . $filename . "page_no=$pageDw\"><span>Prev</span></a>&nbsp;";
            /*}*/
            $str .= "";
            $str .= '';
            if ($tPages < $slot) {
                /*if ($this->isPre) {*/
                $str .= "<a href=\"" . $filename . "page_no=$pageDw\"><span>Prev</span></a>";
                /*}*/
                /*if ($this->isNext) {*/
                $str .= "<a href=\"" . $filename . "page_no=$pageUp\"><span>Next</span></a>";
                /*}*/
            }
            for ($i = $fst; $i < $lst; $i++) {
                $j = $i + 1;
                if ($i < $tPages) {
                    if ($i == $page)
                        $str .= "<a href=\"#\" class=\"active\">$j</a>";
                    else {
                        $str .= "<a href=\"" . $filename . "page_no=$i\">$j</a>  ";
                    }
                    $no = $j;
                }
            }
            $str . '';
            /*if ($this->isNext) {*/
            $str .= "<a href=\"" . $filename . "page_no=$pageUp\"><span>Next</span></a>";
            /*}*/
            $str .= "";
            if ($no < $nrows && $no < $tPages) {
                $str .= "<a href=\"" . $filename . "page_no=$no\"><b>.....</b></a>";
            }
            $str .= "";
        } else {
            if ($nrows >= $this->limit) {
                if ($tPages < $slot) {
                    /*if ($this->isPre) {*/
                    $str .= "<a href=\"" . $filename . "page_no=$pageDw\"><span>Prev</span></a>";
                    /*}*/
                    for ($i = 0; $i < $tPages; $i++) {
                        $j = $i + 1;
                        if ($page == $i)
                            $str .= "<a href=\"#\" class=\"active\">$j</a>  ";
                        else {
                            $str .= "<a href=\"" . $filename . "page_no=$i\">$j</a>  ";
                        }
                    }
                    /*if ($this->isNext) {*/
                    $str .= "<a href=\"" . $filename . "page_no=$pageUp\"><span>Next</span></a>";
                    /*}*/
                }
            }
        }
        return $str;
    }
    function total_records($records)
    {
        if ($records > 0) {
            $str = "Total Records Found:  $records ";
        }
        return $str;
    }
}
?>