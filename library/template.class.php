<?php
class top_template
{
    //Top variables
	private $title_default = "";
	private $desc_default = "";
	private $keys_default = "";
	
	private $title = "";
	private $desc = "";
	private $keys = "";
	
	private $site_css = array();
	private $site_js  = array();
	
		function build_top()
		{
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
		echo '<html xmlns="http://www.w3.org/1999/xhtml">'."\n";
		echo '<head>'."\n";
		echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'."\n";
		echo '<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />'."\n";
		echo '<META NAME="distribution" CONTENT="global" />'."\n";
		echo '<META NAME="Author" CONTENT="InvestInNest.com" />'."\n";
		echo '<META NAME="googlebot" CONTENT="all" />'."\n";
		echo '<META NAME="revisit-after" CONTENT="2 days" />'."\n";
		echo '<link rel="shortcut icon" href="'.C_ROOT_URL.'/images/favicon.ico" />'."\n";
		echo '<META NAME="google-site-verification" CONTENT="PLd8Y4WhogCXkoIGUgpGqAsRdaAOym0Ylu_zY_wunQQ=" />'."\n";
			
			//***** For Meta Tag Code *****//
			if(!empty($this->title)){
				echo "<title>$this->title</title>"."\n";
			}else{
				echo "<title>$this->title_default</title>"."\n";
			}
			if(!empty($this->descript)){
				echo "<meta name=\"description\" content=\"$this->descript\">"."\n";
			}else{
				echo "<meta name=\"description\" content=\"$this->descript_default\">"."\n";
			}
			if(!empty($this->keys)){
				echo "<meta name=\"keywords\" content=\"$this->keys\">"."\n";
			}else{
				echo "<meta name=\"keywords\" content=\"$this->title_keys\">"."\n";
			}
			
			//***** For CSS which is declare in template page
			foreach($this->site_css as $v)
			{
				echo "<link href='".C_ROOT_URL."/css/".$v."' rel='stylesheet' type='text/css' media='all'>"."\n";
			}
		
			//***** For JS which is declare in template page
			foreach($this->site_js as $v)
			{
				echo "<script src='".C_ROOT_URL."/js/".$v."' type='text/javascript'></script>"."\n";
			}
					
		 echo "<script src='".C_ROOT_URL."/js/dropdown.js' type='text/javascript'></script>"."\n";
?>
		<script type="text/javascript" src="<?=C_ROOT_URL?>/highslide/highslide-full.js"></script>
		<script type="text/javascript" src="<?=C_ROOT_URL?>/highslide/highslide.config.js" charset="utf-8"></script>
		<link rel="stylesheet" type="text/css" href="<?=C_ROOT_URL?>/highslide/highslide.css" />
		<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" href="<?=C_ROOT_URL?>/highslide/highslide-ie6.css" />
		<![endif]-->
	<?php
		echo '</head>'."\n";
		echo '<body>'."\n";
	}
	
	function subdomain_build_top()
	{
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
		echo '<html xmlns="http://www.w3.org/1999/xhtml">'."\n";
		echo '<head>'."\n";
		echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'."\n";
		echo '<META NAME="Author" CONTENT="InvestInNest.com" />'."\n";
		echo '<META NAME="googlebot" CONTENT="all" />'."\n";
		echo '<META NAME="revisit-after" CONTENT="2 days" />'."\n";
		echo '<link rel="shortcut icon" href="'.C_ROOT_URL.'/images/favicon.ico" />'."\n";
		echo '<META NAME="google-site-verification" CONTENT="PLd8Y4WhogCXkoIGUgpGqAsRdaAOym0Ylu_zY_wunQQ=" />'."\n";
		
			//***** For Meta Tag Code *****//
			if(!empty($this->title)){
				echo "<title>$this->title</title>"."\n";
			}else{
				echo "<title>$this->title_default</title>"."\n";
			}
			if(!empty($this->descript)){
				echo "<meta name=\"description\" content=\"$this->descript\">"."\n";
			}else{
				echo "<meta name=\"description\" content=\"$this->descript_default\">"."\n";
			}
			if(!empty($this->keys)){
				echo "<meta name=\"keywords\" content=\"$this->keys\">"."\n";
			}else{
				echo "<meta name=\"keywords\" content=\"$this->title_keys\">"."\n";
			}
			
			//***** For CSS which is declare in template page
			foreach($this->site_subdomain_css as $v)
			{
				echo "<link href='".C_ROOT_SUBDOMAIN_URL."/css/".$v."' rel='stylesheet' type='text/css' media='all'>"."\n";
			}
		
			//***** For JS which is declare in template page
			foreach($this->site_subdomain_js as $v)
			{
				echo "<script src='".C_ROOT_SUBDOMAIN_URL."/js/".$v."' type='text/javascript'></script>"."\n";
			}
					
		 echo "<script src='".C_ROOT_SUBDOMAIN_URL."/js/dropdown.js' type='text/javascript'></script>"."\n";
		?>
		<script type="text/javascript" src="<?=C_ROOT_SUBDOMAIN_URL?>/highslide/highslide-full.js"></script>
		<script type="text/javascript" src="<?=C_ROOT_SUBDOMAIN_URL?>/highslide/highslide.config.js" charset="utf-8"></script>
		<link rel="stylesheet" type="text/css" href="<?=C_ROOT_SUBDOMAIN_URL?>/highslide/highslide.css" />
		<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" href="<?=C_ROOT_SUBDOMAIN_URL?>/highslide/highslide-ie6.css" />
		<![endif]-->
	<?php
		echo '</head>'."\n";
		echo '<body>'."\n";
	}
	
	function build_header()
	{
		
	?>
<div class="container">
<!--need Help start-->
<div class="needhelp_panel">
<div class="needhelp"><a href="javascript:void(0)"><img src="images/needhelp.gif" alt="Need Help" height="116" width="29"></a></div>
<div class="helpinfo">
<p>For Direct Booking Call or any questions, call us at any of below mentioned numbers</p>
<p><span>India</span> <strong>

+91 9717841117		
</strong></p>
<p><span>US</span> <strong>+1 917 338 6416</strong></p>
<p><span>UK</span> <strong>+44 (0) 208 090 4217</strong></p>
<p><span>Canada</span> <strong>+1 647 965 1133</strong></p>
</div>
</div>
<!--need Help finish-->

<div id="wrapper1">
<!--start header-->
<div class="header">
<ul>
<li><img src="images/invest-logo.jpg" class="logo" /></li>
<!--expert now div start-->
<li><div class="expert-now">
<p>connect with Expert Now</p>
<input name="" type="text" style="width:202px; height:20px; border:1px solid #dedcdc;" />
<a href="#"><img src="images/call-me.jpg" class="call-btn"></a>
</div></li>
<!--expert now div end-->
<!--number now div start-->
<li style="margin:0; padding:0;"><div class="number">
<table width="330" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="125" align="right" style="padding:0 10px 0 0px;"><img src="images/phone-icon.jpg" alt="Phone"></td>
<td width="189"  style="color:#ff8112; font-size:16px; font-family:'Myriad Pro'; text-transform:capitalize;">fast, easy and free services</td>
</tr>
</table></td>
</tr>
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:6px 0 0 0px;">
<tr>
<td width="292" align="right" style="font:bold 13px Arial, Helvetica, sans-serif; color:#404040;">+91 9717841117</td>
<td width="28" style="padding:0 0 0 10px;"><img src="images/india.jpg" alt="India"/></td>
</tr>
</table></td>
</tr>
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:3px 0 0 0px;">
<tr>
<td width="142" align="right" style="font:bold 13px Arial, Helvetica, sans-serif; color:#404040;">+1 647 965 1133</td>
<td width="28" style="padding:0 0 0 10px;"><img src="images/canada.jpg" alt="Canada"></td>
<td width="17" align="center">|</td>
<td width="115" align="right" style="font:bold 13px Arial, Helvetica, sans-serif; color:#404040; padding:0 0px 0 10px;">+1 917 338 6416</td>
<td width="28" style="padding:0 0 0 10px;"><img src="images/us.jpg" alt="Us"></td>
</tr>
</table></td>
</tr>
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:3px 0 0 0px;">
<tr>
<td width="136" align="right" style="font:bold 13px Arial, Helvetica, sans-serif; color:#404040;">+44 (0) 208 090 4217</td>
<td width="28" style="padding:0 0 0 10px;"><img src="images/uk.jpg" alt="UK"></td>
<td width="17" align="center">|</td>
<td width="121" align="right" style="font:bold 13px Arial, Helvetica, sans-serif; color:#404040;">+971 50 644 6852</td>
<td width="28" style="padding:0 0 0 10px;"><img src="images/dubai.jpg" alt="Dubai"></td>
</tr>
</table></td>
</tr>
</table>

</div></li>
<!--finish now div end-->
</ul>
<!--Navigations-->
<div class="cl"></div>
<div class="navi">
<div class="menu-top">
<ul>
<li><a href="index.php"><img src="images/home-icon.png" border="0" height="15" width="13" style="margin-top:4px;"></a></li>
<li><a href="#">Residential</a></li>
<li><a href="#">Commercial</a></li>
<li><a href="#">New&nbsp;Projects</a></li>
<li><a href="#">Post&nbsp;Your&nbsp;Requirements</a></li>
<li><a href="#">Home&nbsp;Loans</a></li>
<li><a href="#">Our&nbsp;Services</a></li>
<li><a href="#">Ask&nbsp;us</a></li>
</ul>
</div>
<div class="menu-btm">
<ul>
<li><a href="#">Delhi</a></li>
<li><a href="#">Gurgaon</a></li>
<li><a href="#">Noida</a></li>
<li><a href="#">Bangalore</a></li>
<li><a href="#">Pune</a></li>
<li><a href="#">Chennai</a></li>
<li><a href="#">Mumbai</a></li>
<li><a href="#">Chandigarh</a></li>
<li><a href="#">Manesar</a></li>
<li><a href="#">Ghaziabad</a></li>
<li><a href="#">Hyderabad</a></li>
<li><a href="#">Nashik</a></li>
<li><a href="#">Faridabad</a></li>
<li><a href="#">Agra</a></li>
<li><a onmouseout="mclosetime()" onmouseover="mopen('m1')" href="#" style="padding-left:15px;">More</a>
<div style="position: absolute; top: 51px; z-index: 1; visibility: hidden; background:#80add8; top:142px; width:96px; height:235px;" onmouseout="mclosetime()" onmouseover="mcancelclosetime()" id="m1">
<a href="#">Indore</a>
<a href="#">Aurangabad</a>
<a href="#">Solapur</a>
<a href="#">Karnal</a>
<a href="#">Kolkata</a>
<a href="#">Kochi</a>
<a href="#">Lucknow</a>
<a href="#">Ahmedabad </a>
<a href="#">Jalandhar</a>
<a href="#">Goa</a>
<a href="#">Commercial</a></div>
</li>
</ul>

</div>

</div>

<!--end navigations-->

</div>
<!--Header finish-->
<?php
	}
	
	function build_footer()
	{
?>		
	<div class="footer">
    <div class="clear"></div>
      <div class="boxcolumn">
        <ul>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/about.htm"><strong>About</strong></a>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/profile.htm">Profile</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/services.htm">Services</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/contactus.htm">Contact Us</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/career.htm">Career</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/articles.htm">Articles</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/resources.htm">Resources</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/international-agents.htm">International Agents</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/termsofuse.htm">Terms of Use</a></li>
		  
        </ul>
      </div>
      <div class="boxcolumn">
        <ul>
          <li><a href="#"><strong>City</strong></a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/delhi/delhi_property.htm">Delhi</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/mumbai/mumbai_property.htm">Mumbai</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/chennai/chennai_property.htm">Chennai</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/goa/goa_property.htm">Goa</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/bangalore/bangalore_property.htm">Bangalore</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/chandigrah/chandigrah_property.htm">Chandigarh</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/noida/noida_property.htm">Noida</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/gurgaon/gurgaon_property.htm">Gurgaon</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/faridabad/faridabad_property.htm">Faridabad</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/jaipur/jaipur_property.htm">Jaipur</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/pune/pune_property.htm">Pune</a></li>
        </ul>
      </div>
      <div class="boxcolumn">
        <ul>
          <li><a href="#"><strong>Property Rates</strong></a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/delhi-real-estate.htm">Delhi Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/mumbai-real-estate.htm">Mumbai Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/chennai-real-estate.htm">Chennai Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/hyderabad-real-estate.htm">Hyderabad Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/ahmedabad-real-estate.htm">Ahmedabad Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/bangalore-real-estate.htm">Bangalore Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/gurgaon-real-estate.htm">Gurgaon Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/jaipur-real-estate.htm">Jaipur Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/kochi-real-estate.htm">Kochi Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/kolkata-real-estate.htm">Kolkata Rates</a></li>
          <li><a href="http://propertyprices.investinnest.com/rates/pune-real-estate.htm">Pune Rates</a></li>
        </ul>
      </div>
      <div class="boxcolumn">
        <ul>
          <li><a href="#"><strong>Top Flats</strong></a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/delhi/delhi_flats_homes.htm">Delhi Flats</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/chennai/chennai_flats_homes.htm">Chennai Flats</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/bangalore/bangalore_flats_homes.htm">Bangalore Flats</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/hyderabad/hyderabad_flats_homes.htm">Hyderabad Flats</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/gurgaon/gurgaon_flats_homes.htm">Gurgaon Flats</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/chandigrah/chandigrah_flats_homes.htm">Chandigarh Flats</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/noida/noida_flats_homes.htm">Noida Flats</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/city/pune/pune_flats_homes.htm">Pune Flats</a></li>
        </ul>
      </div>
      <div class="boxcolumn">
        <ul>
          <li><a href="#"><strong>Top Apartments</strong></a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/delhi_apartments_houses.htm">Delhi Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/chennai_apartments_houses.htm">Chennai Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/bangalore_apartments_houses.htm">Bangalore Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/hyderabad_apartments_houses.htm">Hyderabad Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/gurgaon_apartments_houses.htm">Gurgaon Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/panchkula_apartments_houses.htm">Panchkula Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/chandigarh_apartments_houses.htm">Chandigarh Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/noida_apartments_houses.htm">Noida Apartments</a></li>
          <li><a href="<?=C_ROOT_SUBDOMAIN_URL?>/properties/pune_apartments_houses.htm">Pune Apartments</a></li>
        </ul>
      </div>
      <div class="newboxcolumn">
        <ul>
          <li style="font-family:'Myriad Pro'; font-size:18px; color:#ff891e;">Contact &amp; Support</li>
          <li><span style="color:#365592;">India:</span> <?php
	  	if(C_ALTS=='pune' or C_ALTS=='Pune')
		{
			$displayNumber = "+91 9860511112";
		}
		elseif(C_ALTS=='chennai' or C_ALTS=='Chennai')
		{
			$displayNumber =  "+91 9840087724";
		}
		elseif(C_ALTS=='bangalore' or C_ALTS=='Bangalore')
		{
			$displayNumber =  "+91 9686755887";
		}else{
			$displayNumber =  "+91 9717841117";
		}
		?>
		<span style="color:#363636;">
		<?=$displayNumber?>
		</span>
		</li>

          <li><span style="color:#365592;">US: </span><span style="color:#363636;">+1 917 338 6416</span></li>
          <li><span style="color:#365592;">UK: </span><span style="color:#363636;">+44 (0) 208 090 4217</span></li>
          <li><span style="color:#365592;">Canada: </span><span style="color:#363636;">+1 647 965 1133</span></li>
          <li>&nbsp;</li>
          <li><strong>Email</strong></li>
          <li><a href="mailto:mail@InvestInNest.com">mail@InvestInNest.com</a></li>
          <li>&nbsp;</li>
          <li><strong>Follow us</strong></li>
          <li><a href="#"><img src="<?=C_ROOT_SUBDOMAIN_URL?>/images/icon_facebook.gif" width="16" height="16" alt="Facebook" /></a> &nbsp; <a href="#"><img src="<?=C_ROOT_SUBDOMAIN_URL?>/images/icon_twitter.gif" width="16" height="16" alt="Twitter" /></a>&nbsp; <a href="#"><img src="<?=C_ROOT_SUBDOMAIN_URL?>/images/icon_mail.gif" width="16" height="16" alt="Mail" /></a>&nbsp; <a href="#"><img src="<?=C_ROOT_SUBDOMAIN_URL?>/images/icon_linkedin.gif" width="16" height="16" alt="Linked In" /></a>&nbsp; <a href="#"><img src="<?=C_ROOT_SUBDOMAIN_URL?>/images/icon_share.gif" width="16" height="16"/></a></li>
        </ul>
      </div>
	  
      <div class="clear"></div>
	  
	 	<div class="partnersite">
	       <p><strong>Partner Websites : </strong><a href="http://www.gurgaonhomes.net/">Gurgaon Property</a> | <a href="http://www.chennaiveedu.com/">Chennai  Property</a> | <a href="http://www.thebangaloreproperty.com/">Bangalore  Property</a> |<a href="http://www.puneproperty.net/"> Pune  Property</a> | <a href="http://www.moneylaxmi.com">MoneyLaxmi</a> | <a href="http://www.quickyards.com">QuickYards</a></p>
        <p style="background:#537ac6;color:#fff; padding:5px 0px 5px 0px;">Copyright&copy; Investinnest.com. 2011 Comprehensive India Property Portal. All Right Reserved. Terms and Use. Site Map</p>
      </div>
	 </div>
  </div>
</div>
		
		
	
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-2606765-1']);
_gaq.push(['_setDomainName', '.investinnest.com']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<?php
	}
	
	function build_bot()
	{
		echo "</body>";
		echo "</html>";
?>
<script>
$( "#keywords" ).combogrid({
url: 'http://www.investinnest.com/includes/globalsearch.php',
debug:true,
//replaceNull: true,
colModel: [{'columnName':'name','width':'40','label':''}, {'columnName':'bhk','width':'30','label':''},{'columnName':'inr','width':'20','label':''},{'columnName':'enquiry','width':'10','label':''}],
select: function( event, ui ) {
document.frm.action=ui.item.formSetAction;
setFormAction(0);
$( "#keywords" ).val( ui.item.propertyName );
return false;
}

});
function setFormAction(flagSet)
{
	if(flagSet==0)
	{
		$("#keywords").keyup(function(event){
		if(event.keyCode == 13){
        window.location.href=document.frm.action;
    }
});
}
}

</script>
<?php
	}
	
	// Show Dynamic meta Tag. Fetch from Table
	function set_meta($title, $description, $keyword, $city, $type)
	{
		if($type == 'home')
		{
			$query			= "select metaTitle, metaKey, metaDesc from ".T_HOMEPAGE;
			$query		   .= " where status=0 and isDeleted=0";
		}elseif($type == 'city'){
			$query			= "select meta_title, meta_keywords, meta_description from ".TABLE_CITIES;
			$query		   .= " where city='".$city."'";
		}elseif($type == 'property'){
			$query			= "select meta_title, meta_keywords, meta_description, meta_keyphrace, meta_classification, extra1, extra2 from ".TABLE_PROPERTIES;
			$query		   .= " where name='".$city."'";
		}elseif($type == 'propertyprice'){
			$query			= "select meta_title, meta_keywords, meta_description, meta_keyphrace, meta_classification, extra1, extra2 from iin_devs";
			$query		   .= " where builder1='propertyPrice metadata' and city='metadata' and name='Delhi'";
		}elseif($type == 'flat'){
			$query			= "select meta_title, meta_keywords, meta_description, meta_keyphrace, meta_classification, extra1, extra2 from iin_devs";
			$query		   .= " where builder1='Flat MetaData' and city='Flat MetaData' and name='".$city."'";
		}elseif($type == 'apartment'){
			$query			= "select meta_title, meta_keywords, meta_description, meta_keyphrace, meta_classification, extra1, extra2 from iin_devs";
			$query		   .= " where builder1='Apartment MetaData' and city='Apartment MetaData' and name='".$city."'";
		}elseif($type == 'builder'){
			$query			= "select meta_title, meta_keywords, meta_description, meta_keyphrace, meta_classification, extra1, extra2 from iin_devs";
			$query		   .= " where city='Developer Metadata' and builder_homepage='".$city."' and meta_data='yes'";
		}
		elseif($type == 'leasing'){
			$query			= "select meta_title, meta_keywords, meta_description, meta_keyphrace, meta_classification, extra1, extra2 from iin_devs";
			$query		   .= " where city='Leasing Metadata' and builder_homepage='".$leasing."' and meta_data='yes'";
		}
		
		
		$resul			= mysql_query($query);
		$data			= mysql_fetch_assoc($resul);
		
		$this->title 	= $data['meta_title'];
		$this->descript	= $data['meta_description'];
		$this->keys		= $data['meta_keywords'];
		$this->extra1 	= $data['extra1'];
		$this->extra2 	= $data['extra2'];
	}
	
	//Show Static meta Tag which is declare in template page
	function metaTag($title, $description, $keyword)
	{
		$this->title 	= $title;
		$this->descript	= $description;
		$this->keys		= $keyword;
		
	}
	
	//Set Meta Tag Limitation
	function setMetaLimit()
	{
		$this->title_default	= str_replace(",,", ",", $this->title_default);
		$this->des_default		= str_replace(",,", ",", $this->des_default);
		$this->keys_default		= str_replace(",,", ",", $this->keys_default);
		
		$this->title_default	= trim($this->title_default);
		$this->des_default		= trim($this->des_default);
		$this->keys_default		= trim($this->keys_default);				
		
		$this->title_default	= cropString($this->title_default, 75);
		$this->des_default 		= cropString($this->des_default, 150);
		$this->keys_default 	= cropString($this->keys_default, 400);
	}
	
	//Add CSS In Template file
	function addCSS($css_arr)
	{
		if(count($css_arr)>0)
		{
			foreach($css_arr as $v)
			{
				$this->site_css[] = $v;
			}
		}
	}
	
	//Add JS In Template file
	function addJS($js_arr)
	{
		if(count($js_arr)>0)
		{
			foreach($js_arr as $v)
			{
				$this->site_js[] = $v;
			}
		}
	}
	
	//Add CSS In Template file
	function addsubdomainCSS($css_arr)
	{
		if(count($css_arr)>0)
		{
			foreach($css_arr as $v)
			{
				$this->site_subdomain_css[] = $v;
			}
		}
	}
	
	//Add JS In Template file
	function addsubdomainJS($js_arr)
	{
		if(count($js_arr)>0)
		{
			foreach($js_arr as $v)
			{
				$this->site_subdomain_js[] = $v;
			}
		}
	}
}
?>