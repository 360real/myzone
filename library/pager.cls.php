<?php


/*--------------------------------------------------+
| CLASS: pager                                      |
| FILE: pager.cls.php                               |
+===================================================+
| Class to generate page links easily               |
+---------------------------------------------------+
| Copyright � 2005 Davis 'X-ZERO' John              |
| Scriptlance ID: davisx0                           |
| Email: davisx0@gmail.com                          |
+---------------------------[ Thu, Jun 16, 2005 ]--*/




define("PAGER_DEF_PAGELIMIT", 15);


class clsPager
{
	var $urlformat;
	var $total;
	var $perpage;
	var $totalpages;
	var $curpage;


	function clsPager($urlformat="", $total=0, $perpage=0, $curpage=0)
	{
		$this->urlformat = $urlformat;
		$this->total = $total;
		$this->perpage = $perpage;
		$this->curpage = $curpage;
		$this->totalpages = ceil($total/$perpage);
	}


	function pagelink($page)
	{
		$link = str_replace("{@PAGE}", $page, $this->urlformat);
		$link = str_replace("{@PERPAGE}", $this->perpage, $link);
		return $link;
	}


	function nextpage()
	{
		if ($this->totalpages && $this->curpage != $this->totalpages)
		{
			return $this->curpage+1;
		}
	}
	
	
	function nextlink()
	{
		if ($this->curpage != $this->totalpages)
		{
			$link = str_replace("{@PAGE}", $this->curpage+1, $this->urlformat);
			$link = str_replace("{@PERPAGE}", $this->perpage, $link);
			return $link;
		}
	}


	function prevpage()
	{
		if ($this->curpage != 1)
		{
			return $this->curpage-1;
		}
	}
	
	
	function prevlink()
	{
		if ($this->curpage != 1)
		{
			$link = $this->urlformat;
			$link = str_replace("{@PAGE}", $this->curpage-1, $link);
			$link = str_replace("{@PERPAGE}", $this->perpage, $link);
			return $link;
		}
	}


	function firstlink()
	{
		if ($this->totalpages != 0)
		{
			$link = $this->urlformat;
			$link = str_replace("{@PAGE}", 1, $link);
			$link = str_replace("{@PERPAGE}", $this->perpage, $link);
			return $link;
		}
	}


	function lastlink()
	{
		if ($this->totalpages != 0)
		{
			$link = $this->urlformat;
			$link = str_replace("{@PAGE}", $this->totalpages, $link);
			$link = str_replace("{@PERPAGE}", $this->perpage, $link);
			return $link;
		}
	}
	
	
	
	function getlinks($limit = PAGER_DEF_PAGELIMIT, $firstandlast = TRUE)
	{
		if ($this->curpage <= $limit+1)
		{
			$start = 1;
			$ellipse1 = "";
			$extra = $limit - ($this->curpage-1);
		}
		else
		{
			$start = $this->curpage - $limit;
			$ellipse1 = "<a href=\"#\">...</a>";
			$extra = 0;
		}

		if ($this->totalpages-$this->curpage <= $limit+$extra)
		{
			$end = $this->totalpages;
			$ellipse2 = "";
			$extra = $limit + $extra - ($this->totalpages-$this->curpage);
		}
		else
		{
			$end = $this->curpage + ($limit+$extra);
			$ellipse2 = "<a href=\"#\">...</a>";
			$extra = 0;
		}

		if ($extra > 0)
		{
			if ($start>$extra)
			{
				$start = $start-$extra;
				$extra = 0;
			}
			else
			{
				$extra -= $start-1;
				$start = 1;
				$ellipse1 = "";
			}
		}
		
		/*if ($extra)
		{
			if ($end+$extra >= $this->totalpages)
			{
				$end = $this->totalpages;
				$ellipse2 = "";
				$extra -= ($this->totalpages-$end);
			}
			else
			{
				$end += $extra;
				$extra = 0;
			}
		}

		if ($extra)
		{
			if ($start>$extra)
			{
				$start = $start-$extra;
				$extra = 0;
			}
			else
			{
				$extra -= $start-1;
				$start = 1;
				$ellipse1 = "";
			}
		}
		*/

		$links = "";

		/*if ($this->totalpages && $firstandlast)
			$links .= "<a href=\"".$this->firstlink()."\">First</a>\n";*/
		
		if ($this->prevpage())
			$links .= "<a href=\"".$this->prevlink()."\"><span>Prev</span></a>\n";
		
		$links .= $ellipse1;

		for ($p=$start; $p<$this->curpage; $p++)
			$links .= "<a href=\"".$this->pagelink($p)."\">&nbsp;".$p."&nbsp;</a>\n";

		if($this->totalpages > 1)
			$links .= "<a href=\"#\" class=\"active\">&nbsp;".$this->curpage."&nbsp;</a>\n";

		for ($p=$this->curpage+1; $p<=$end; $p++)
			$links .= "<a href=\"".$this->pagelink($p)."\">&nbsp;".$p."&nbsp;</a>\n";

		$links .= $ellipse2;

		if ($this->nextpage())
			$links .= "<a href=\"".$this->nextlink()."\"><span>Next</span></a>\n";

		/*if ($this->totalpages && $firstandlast)
			$links .= "<a href=\"".$this->lastlink()."\">Last</a>\n";*/

		$links .= "";

		return $links;

	}


	function outputlinks()
	{
		return $this->getlinks();
	}
}

?>