<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
	 $message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
	$tblName	=	TABLE_REVIEW_POSTS;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/review/controller/controller.php';
	$userdataviewpage	=	C_ROOT_URL.'review/view/userdata_view.php';		
	$productdetailpage	=	C_ROOT_URL.'review/view/product_details.php';		

	$clsRow1	=	'clsRow1';
	$clsRow2	=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform task like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	


/* choices to where page is redirect
	if action=create then goes to view  	
		action =update then model
		action default to view i.e index.php
*/

$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."review/model/model.php" ;
			break;	
			default:
			$file_name = ROOT_PATH."review/view/view.php" ;
			break;
}

/***********************************************************************************/
		//Code  for search a record from database on click submit  button

/***********************************************************************************/
if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
			if($_GET["action"]=="search")
			{
				$sql = "SELECT trp.*, trt.topic, trt.item_id FROM ".TABLE_REVIEW_POSTS." trp join ".TABLE_REVIEW_TOPICS." trt on trp.topic_id=trt.id WHERE ";
				if($_GET["topic_id"]!="")
					//$sql.="(trp.b_name Like '%".$_GET["user"]."%' OR trp.b_email Like'%".$_GET["user"]."%' OR trp.b_city Like'%".$_GET["user"]."%'  OR ord.b_mobile Like'%".$_GET["user"]."%')";
					$sql.="trp.topic_id=". $_GET["topic_id"];
				if($_GET["title"]!="")
					$sql.=" (trp.title Like '%".$_GET["title"]."%' OR trp.post Like'%".$_GET["title"]."%' )";
				if($_GET["status"]!="")
					$sql.="trp.status=".$_GET["status"];
				if($_GET["ctry_id"]!="")
					$sql.="trp.b_country=".$_GET["ctry_id"];
				if($_GET["startDate"]!="" && $_GET["endDate"])
				{
					$sql.=" and date(trp.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(trp.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
				}
				else
				if($_GET["startDate"]!="")
				{
					$sql.=" and date(trp.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
				}
				else
					if($_GET["endDate"]!="")
					{
						$sql.=" and date(trp.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
					}
			}
			
			else if($_GET["action"]=="Update")
				{

					$sql = "SELECT trp.*, trt.topic, trt.item_id FROM ".TABLE_REVIEW_POSTS." trp join ".TABLE_REVIEW_TOPICS." trt on trp.topic_id=trt.id";
					$sql.= " WHERE trp.id=".$_GET["id"]; 
					//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
					
				}
				
			else if($_GET["action"]=="Delete") 
				{
	    			$obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
					$obj_common->redirect($page_name."?msg=delete");
					/*$arr=array('status'=>0);
					$test=$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
					$obj_common->redirect($page_name."?msg=delete");*/
				}
		$s=($_GET['s'] ? 'ASC' : 'DESC');
		$sort=($_GET['s'] ? 0 : 1 );
   		$f=$_GET['f'];
		if($s && $f)
			$sql.= " ORDER BY $f  $s";
			else
			$sql.= " ORDER BY trp.$fld_orderBy";	

	
	}
	else
	{
		$sql = "SELECT trp.*, trt.topic, trt.item_id FROM ".TABLE_REVIEW_POSTS." trp join ".TABLE_REVIEW_TOPICS." trt on trp.topic_id=trt.id order by trt.created_date desc";
		//$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	
/*---------------------paging script start----------------------------------------*/
if(isset($_GET['rpp']))
		$obj_paging->limit=$_GET['rpp'];
		else
		$obj_paging->limit=10;
				if($_GET['page_no'])
			$page_no=remRegFx($_GET['page_no']);
			else
			$page_no=0;
		$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
		$str_pos=strpos($queryStr,'page_no');
		if($str_pos>0)
			$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
		$obj_paging->set_lower_upper($page_no);
		$total_num=$obj_mysql->get_num_rows($sql);
		$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
		$total_rec=$obj_paging->total_records($total_num);
		$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
/************************************************************************************************/
		// code for search and pagination ends here

/************************************************************************************************/	


	if(count($_POST)>0)
	{
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id)
			{
				$arr['modified_date'] = "now()";
				$arr['ipaddress'] = $regn_ip;
				$arr['modified_by']=$_SESSION['login']["id"];
		
				
							$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
							$obj_common->redirect($page_name."?msg=update");	
			}
		else
		{
		
			$arr['ipaddress'] = $regn_ip;
			//$arr['created_by']=$_SESSION['login']["id"];


			$itm_id=$_POST['item_id'];	
			$checkIteminTopics="SELECT trt.* from ".TABLE_REVIEW_TOPICS." trt  where item_id=".$itm_id." ";
			$response=$obj_mysql->getAllData($checkIteminTopics);
			$count=count($response);	
			if($count==0)
			{
				$tblName=TABLE_REVIEW_TOPICS;
				//$getitemDetail="select id,item,pdt_cat_id from ".TABLE_ITEMS." where id=".$itm_id." ";
				//$result=$obj_mysql->getAllData($getitemDetail);
				
				$arr['topic']=$result[0]['item'];
				$arr['item_id']=$result[0]['id'];
				$arr['cat_id']=$result[0]['pdt_cat_id'];
				$arr['status']=1;
				$insertRecId=$obj_mysql->insert_data($tblName,$arr);
				if(isset($insertRecId))
				{
					$tblName=TABLE_REVIEW_POSTS;
					$tblpost['title']=$_POST['title'];
					$tblpost['post']=$_POST['post'];
					$tblpost['topic_id']=$insertRecId;
					$tblpost['ipaddress'] = $regn_ip;
					$tblpost['status']=2;
					$tblPostInsert=$obj_mysql->insert_data($tblName,$tblpost);
					$obj_common->redirect($page_name."?msg=insert");

				}


			}
			elseif($count==1)
			{

					$tblName=TABLE_REVIEW_POSTS;
					$tblpost['title']=$_POST['title'];
					$tblpost['post']=$_POST['post'];
					$tblpost['topic_id']=$response[0]['id'];
					$tblpost['ipaddress'] = $regn_ip;
					$tblpost['status']=2;
					$tblPostInsert=$obj_mysql->insert_data($tblName,$tblpost);
					$obj_common->redirect($page_name."?msg=insert");
			}	


			/*if($obj_mysql->isDuplicate($tblName,'post', $arr['post']))
			{
				$setmaxid=$obj_mysql->insert_data($tblName,$arr);

				if($_FILES['user_image']['name']!="")
				{
					echo $_FILES['user_image']['name'];
					$handle = new upload($_FILES['user_image']);
					if ($handle->uploaded)
					{
						
						$handle->file_safe_name = true;
						$handle->process(UPLOAD_PATH_USERS_IMAGE.$setmaxid."/");
						if ($handle->processed) 
						{
							//echo 'image resized';
							$arr['user_image'] = $handle->file_dst_name;
							@unlink(UPLOAD_PATH_USERS_IMAGE.$setmaxid."/".$_POST['old_file_name']);
							$handle->clean();
						} 
						else 
						{
							echo 'error : ' . $handle->error;
						}
					}
					$obj_mysql->update_data($tblName,$fld_id,$arr,$setmaxid);

				}
				$obj_common->redirect($page_name."?msg=insert");	
			}
			else
			{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	*/
		}
	}	


	$list="";
		for($i=0;$i< count($rec);$i++){
		
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$discount = '0';
		if($rec[$i]['status']=="1")
		{
			$st='<font color=red>Under Review</font>';
		}
		elseif($rec[$i]['status']=="2")
		{
			$st='<font color=Green>Active</font>';
		}
		elseif($rec[$i]['status']=="0")
		{
			$st='<font color=red>Inactive</font>';	
		}
		$list.='<tr class="'.$clsRow.'" id="rec_'.$rec[$i]['id'].'">
		<!--<td><a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="title_a">'.$rec[$i]['id'].'</a></td>-->
					<!--<td class="center">'.$rec[$i]['id'].'</td>-->
					<td class="center">'.$rec[$i]['topic'].'</td>
					<td class="center">'.$rec[$i]['title'].'</td>
					<td class="center">'.substr($rec[$i]['post'],0,50).'</td>
					<td class="center">'.$st.'</td>';
					if($rec[$i]['status']=="0" || $rec[$i]['status']=="1")
					{	
						$list.='<td class="center"><button value="2" id='.$rec[$i]['id'].' onclick="makeactive(this.id, this.value); return false;">Active</button></td>';
					}
					elseif($rec[$i]['status']=="2")
					{
						$list.='<td class="center"><button value="0" id='.$rec[$i]['id'].' onclick="makeactive(this.id, this.value); return false;"> In Active</button></td>';
					}
					$list.='<td class="center">'.$rec[$i]['created_date'].'</td>
					
					<td>
					<a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>
						<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>
					
					</td> 
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=10 align='center' height=50>No Record(s) Found.</td></tr>";
	}



?>

<!--************************************** code to include common header ***************************************/-->
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Review Manager', $site['TITLE'], $site['URL']);  
?>
<!--************************************** End of code to include header ***************************************/-->
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name	);?></td> <!-- include file_name to get the page on controller page -->
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
function makeactive(recid,value)
{

if(confirm("Are you sure you want to change status"))
{
var base_url="<?php echo C_ROOT_URL ?>";

  $.ajax({
        //url: "view/changepoststatus.php",
        url:base_url+"/review/view/changepoststatus.php",
        type: 'POST',
        data: {post_recid :recid, status:value },
        success: function(data) {
               	
               	
        	jQuery("#rec_"+recid).replaceWith(data);
            
        }
    });
}
}

</script>
