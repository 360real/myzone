<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_META;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/meta/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."meta/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."meta/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT meta.id, meta.template_id, meta.type_id, meta.cat_id, tem.template_name, meta.cty_id, types.type, cat.type_category, meta.description, meta.meta_title, meta.meta_keywords, meta.meta_description, meta.status, meta.created_date, meta.modified_date, cty.city, meta.project_status_id FROM ".TABLE_META." meta LEFT JOIN ".TABLE_TEMPLATES." tem ON meta.template_id=tem.id LEFT JOIN ".TABLE_TYPES." types ON meta.type_id=types.id LEFT JOIN ".TABLE_TYPES_CATEGORIES." cat ON meta.cat_id=cat.id LEFT JOIN tsr_cities cty ON meta.cty_id=cty.id WHERE ";
		if($_GET["city_id"]!="")
			$sql.="meta.cty_id=".$_GET["city_id"];
		if($_GET["city"]!="")
			$sql.=" cty.city like '%".trim($_GET["city"])."%'";
		if($_GET["status"]!="")
			$sql.="meta.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(meta.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(meta.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(meta.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(meta.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT meta.id, meta.template_id, meta.type_id, meta.cat_id, tem.template_name, meta.cty_id, types.type, cat.type_category, meta.description, meta.meta_title, meta.meta_keywords, meta.meta_description, meta.status, meta.created_date, meta.modified_date, cty.city, meta.project_status_id FROM ".TABLE_META." meta LEFT JOIN ".TABLE_TEMPLATES." tem ON meta.template_id=tem.id LEFT JOIN ".TABLE_TYPES." types ON meta.type_id=types.id LEFT JOIN ".TABLE_TYPES_CATEGORIES." cat ON meta.cat_id=cat.id LEFT JOIN tsr_cities cty ON meta.cty_id=cty.id ";
		
		$sql.= " WHERE meta.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY meta.$fld_orderBy";	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
		$sql = "SELECT meta.id, meta.template_id, meta.type_id, meta.cat_id, tem.template_name, meta.cty_id, types.type, cat.type_category, meta.description, meta.meta_title, meta.meta_keywords, meta.meta_description, meta.status, meta.created_date, meta.modified_date, cty.city, meta.project_status_id FROM ".TABLE_META." meta LEFT JOIN ".TABLE_TEMPLATES." tem ON meta.template_id=tem.id LEFT JOIN ".TABLE_TYPES." types ON meta.type_id=types.id LEFT JOIN ".TABLE_TYPES_CATEGORIES." cat ON meta.cat_id=cat.id LEFT JOIN tsr_cities cty ON meta.cty_id=cty.id";
		
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/
	//echo $sql;

	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";

			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];

				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				$obj_common->redirect($page_name."?msg=update");	
				
		}else{
			$arr['created_date'] = "now()";


			$arr['ipaddress'] = $regn_ip;
			$arr['created_by'] = $_SESSION['login']['id'];

				$obj_mysql->insert_data($tblName,$arr);
				$obj_common->redirect($page_name."?msg=insert");	
	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&state_id='.$rec[$i]['state_id'].'&template_id='.$rec[$i]['template_id'].'&id='.$rec[$i]['id'].'&type_id='.$rec[$i]['type_id'].'&cat_id='.$rec[$i]['cat_id'].'&cty_id='.$rec[$i]['cty_id'].'&project_status_id='.$rec[$i]['project_status_id'].'" target="_blank" class="title_a">'.$rec[$i]['template_name'].'</a></td>
	<td class="center">'.$rec[$i]['city'].'</td>
	<td class="center">'.$rec[$i]['type'].'</td>
	<td class="center">'.$rec[$i]['type_category'].'</td>
	<td class="center">'.$rec[$i]['meta_title'].'</td>
	<td class="center">'.$rec[$i]['meta_keywords'].'</td>
	<td class="center">'.$rec[$i]['meta_description'].'</td>
					
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$st.'</td>
					<td><a href="'.$page_name.'?action=Update&state_id='.$rec[$i]['state_id'].'&template_id='.$rec[$i]['template_id'].'&id='.$rec[$i]['id'].'&type_id='.$rec[$i]['type_id'].'&cat_id='.$rec[$i]['cat_id'].'&cty_id='.$rec[$i]['cty_id'].'&project_status_id='.$rec[$i]['project_status_id'].'" target="_blank" class="edit">Edit</a>
					</td>
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Cities', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
