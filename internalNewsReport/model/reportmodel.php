<?php
require_once ROOT_PATH . "library/mysql_function.php";

class reportModel
{
    private $obj;

    public function __construct()
    {
        $this->obj = new mysql_function();
    }

    public function getDataForMailClick()
    {

        $query = "SELECT track.NewsID, news.title, detail.Subject, detail.ShootDate, track.URL, count(track.NewsID) as count
	FROM RealtyRoundupMailTrack as track
	INNER JOIN RealtyRoundUpMailDetails as detail on track.MailerId = detail.id
	INNER JOIN tsr_internal_news as news on news.id = track.NewsID
	WHERE track.Action = 'MailClick'
	GROUP BY track.NewsID
	ORDER BY track.id desc";
        $data = $this->obj->getAllData($query);
        return $data;

    }

    public function getDataForWebClick()
    {

        $query = "SELECT news.title, track.URL, MAX(track.OpenedDate) as latest, count(track.id) as count
	FROM  RealtyRoundupMailTrack as track
	INNER JOIN tsr_internal_news as news on news.id = track.NewsID
	WHERE track.Action = 'WebBrowse'
	GROUP BY track.NewsID
	ORDER BY track.id desc";
        $data = $this->obj->getAllData($query);
        return $data;
    }

    public function getDataforOpenMail($startDate="", $endDate="")
    {

    $query = "SELECT detail.Subject,detail.SentTo, count(track.id) as opencount
	from RealtyRoundupMailTrack track
	inner join RealtyRoundUpMailDetails detail on track.MailerId = detail.id
	where track.Action = 'Opened' ";
	
	if ($startDate !="" and $endDate !="") {
		$query .="and detail.ShootDate between '$startDate' and '$endDate' ";
	}
	$query .= "group by track.MailerId
	order by track.MailerId desc";
        $data = $this->obj->getAllData($query);
        return $data;
	}
	
	public function getCountForClicksPerMail($startDate="", $endDate="")
	{
	$query = "SELECT detail.Subject, count(track.id) as mailclick
	from RealtyRoundupMailTrack track
	inner join RealtyRoundUpMailDetails detail on track.MailerId = detail.id
	where track.Action = 'MailClick' ";
	if ($startDate !="" and $endDate !="") {
		$query .="and detail.ShootDate between '$startDate' and '$endDate' ";
	}
	$query .= "group by track.MailerId
	order by track.MailerId desc";
		$data = $this->obj->getAllData($query);
		return $data;

	}

}
