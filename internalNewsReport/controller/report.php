<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

include $_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php";
include_once CONFIG_PATH;
admin_login();

/* Including common Header */
include LIBRARY_PATH . "includes/header.php";
$commonHead = new commonHead();
$commonHead->commonHeader('RealtyRoundUp Report', $site['TITLE'], $site['URL']);
/*Header Ends*/

include LIBRARY_PATH . "internalNewsReport/model/reportmodel.php";
$modelObject = new reportModel();

$pageName = C_ROOT_URL . "/internalNewsReport/controller/report.php";
$action = $_GET['action'];

if (array_key_exists("startDate", $_POST)) {
    $startDate = mysql_real_escape_string($_POST['startDate']);
} else {
    $startDate = "";
}
if (array_key_exists("endDate",$_POST)) {
    $endDate = mysql_real_escape_string($_POST['endDate']);
} else {
    $endDate = "";
}

switch ($action) {

    case 'mailclick':
        $mailClickData = $modelObject->getDataForMailClick();
        include LIBRARY_PATH . "internalNewsReport/view/mailClick.php";
        break;

    case 'webclick':
        $WebClickData = $modelObject->getDataForWebClick();
        include LIBRARY_PATH . "internalNewsReport/view/webClick.php";
        break;

    default:
        $openedmailData = $modelObject->getDataforOpenMail($startDate, $endDate);
        $ClicksPerMail = $modelObject->getCountForClicksPerMail($startDate, $endDate);
        $data = array();

        for ($i = 0; $i < count($openedmailData); $i++) {
            $data['subject'] = $openedmailData[$i]['Subject'];
            $data['opencount'] = $openedmailData[$i]['opencount'];
            $data['sentto'] = $openedmailData[$i]['SentTo'];
            $data['mailclick'] = $ClicksPerMail[$i]['mailclick'];
            $record[] = $data;
        }
        include LIBRARY_PATH . "internalNewsReport/view/openedmail.php";
}
