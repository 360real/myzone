<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script>
function richTextBox($fieldname, $value = "") 
{
  $CKEditor = new CKEditor();
  $config['toolbar'] = array(
  array( 'Bold', 'Italic', 'Underline', 'Strike'),
  array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
  array('Format'),
  array( 'Font', 'FontSize', 'FontColor', 'TextColor'),
  array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ),
  array('Image', 'Table', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'),
  array('Subscript','Superscript'),
  array( 'Link', 'Unlink' ),
  array('Source')
  );
  $CKEditor->basePath = '/ckeditor/';
  $CKEditor->config['width'] = 975;
  $CKEditor->config['height'] = 400;
  $CKEditor->editor($fieldname, $value, $config);
}
</script>

<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">

  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
    <thead>
      <tr>
        <th colspan="2" align="left">
          <h3>Blog Detail</h3>
        </th>
      </tr>
    </thead>

    <tbody>
      <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Name</strong></td>
        <td><input name="name" type="text" title="Name" lang='MUST' value="<?php echo($rec['name'])?>" size="50%" maxlength="120" /></td>
      </tr>


      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
        <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
      </tr>


      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
        <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
      </tr>


      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
        <td><textarea  name="meta_description" id="meta_description" rows="5" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
      </tr>

      <tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
      	<td valign="bottom">
        	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
        	<label for="1">Active</label> 
        	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
        	<label for="0">Inactive</label>
      	</td>
       </tr>

      <tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
        <td align="left">
          <input name="submit" type="submit" class="button" value="Submit" />
          <input name="reset" type="reset" class="button" value="Reset" />
          <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
      </tr>

    </tbody>
  </table>
</form>