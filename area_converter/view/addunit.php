<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>view/js/jquery.dropdown.css">
<script src="<?php echo $site['URL']?>view/js/jquery.dropdown.js"></script>
  <style>
a#btnAddtoList {
    color: #6e306b!important;
}
  </style>

<form id="datafm"   enctype="multipart/form-data" >
<input type="hidden"  name="post_id" value="<?php echo($listdata[0]['id'])?>">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="1" align="left"><h3>Emi Calculator</h3></th><th align="right"><a class="buttons" href="/emi_calculator/controller/controller.php?action=emi_calculatorlist"><h3>Emi Calculator List</h3></a></th></tr>
</thead>
<tbody>
  <?php 
  $sqlunit = 'SELECT `id`,`unitname` FROM tsr_unit_converter WHERE status=1';
  $unitlist =$obj_mysql->getAllData($sqlunit);
  ?>
  <tr class="<?php echo($clsRow1)?>">
    <td align="right"><strong><font color="#FF0000"></font>State</strong></td>
    <td>
      <?php 
      $sql = 'SELECT `id`,`name` FROM tsr_state ';
      $contlist =$obj_mysql->getAllData($sql);
      ?>
       <select  id="state_id" name="state_id" placeholder="Select State">
          <option value="">Select State </option>
          <?php for ($i=0; $i <count($contlist) ; $i++) { ?>
           <option value="<?php echo $contlist[$i]['id']; ?>" <?php if($listdata[0]['state_id']== $contlist[$i]['id']):?> selected <?php endif; ?>><?php echo $contlist[$i]['name']; ?></option>
          <?php } ?>
      </select>
    </td>
  </tr> 
  <tr class="<?php echo($clsRow1)?>" id="city_section">
    <td align="right"><strong><font color="#FF0000"></font>From </strong></td>
    <td>
      <table>
        <tr>
          <td><input type="text" name="unit_from" value="<?php if($listdata[0]['unit_from']!=''){ echo $listdata[0]['unit_from']; }else{ echo '1'; } ?>"></td>
          <td>
            <select  id="unit_id" name="unit_id" placeholder="Select Unit">
            <option value="">Select Unit </option>
            <?php for ($i=0; $i <count($unitlist) ; $i++) { ?>
            <option value="<?php echo $unitlist[$i]['id']; ?>" <?php if($listdata[0]['unit_id']== $unitlist[$i]['id']):?> selected <?php endif; ?>><?php echo $unitlist[$i]['unitname']; ?></option>
            <?php } ?>
            </select>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class="<?php echo($clsRow1)?>" id="city_section">
    <td align="right"><strong><font color="#FF0000"></font>Converted figure </strong></td>
    <td>
      <table>
        <tr>
          <td><input type="text" name="range" value="<?php echo $listdata[0]['converted_range']; ?>" placeholder="Converted figure"></td>
          <td>
            <select  id="converted_unit" name="converted_unit" placeholder="Select Unit">
            <option value="">Select Converted Unit </option>
            <?php for ($i=0; $i <count($unitlist) ; $i++) { ?>
            <option value="<?php echo $unitlist[$i]['id']; ?>" <?php if($listdata[0]['converted_unitid']== $unitlist[$i]['id']):?> selected <?php endif; ?>><?php echo $unitlist[$i]['unitname']; ?></option>
            <?php } ?>
            </select>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Description</strong></td>
  <td><textarea class="ckeditor" name="emi_discription"  id="emi_discription" rows="4" cols="70"><?php echo $listdata[0]["emi_discription"]; ?></textarea></td>
  </tr>
  <tr class="<?php echo($clsRow1)?>">
    <td align="right"><strong>Meta Title</strong></td>
    <td><input name="meta_title" type="text" title="ordering" lang='MUST' value="<?php echo($listdata[0]['meta_title'])?>" size="50%" maxlength="120" /></td>
  </tr>
  <tr class="<?php echo($clsRow1)?>">
    <td align="right"><strong>Meta Keyword</strong></td>
    <td><input name="meta_keyword" type="text" title="ordering" lang='MUST' value="<?php echo($listdata[0]['meta_keyword'])?>" size="50%" maxlength="120" /></td>
  </tr>
  <tr class="<?php echo($clsRow1)?>">
    <td align="right"><strong>Meta Description</strong></td>
    <td><input name="meta_description" type="text" title="meta_description" lang='MUST' value="<?php echo($listdata[0]['meta_description'])?>" size="50%" maxlength="120" /></td>
  </tr>
  <tr class="clsRow1">
    <td align="right"><strong>Status</strong></td>
    <td>
      <select id="status" name="status" placeholder="Select">
         <option value="">Select Status </option>
         <option value="1"  <?php if($listdata[0]['status']==1){ echo 'selected';} ?>>Active </option>
         <option value="0" <?php if($listdata[0]['status']==0){ echo 'selected';} ?>>In Active</option>
      </select>
    </td>
  </tr>
<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left">
    <input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" />
  </td>
</tr>
</tbody>
</table>
</form>

<script type="text/javascript">
  $('.dropdown-mul-2').dropdown({
    limitCount: 500,
    searchable: true
  });
  

  function removevl(item){
   var input = $('input[name="'+item+'"]').val('');
  }

</script>




<script type="text/javascript">


  $(document).ready(function(){  
    var data = new FormData();
    $('.button').on('click', function(e){  
      e.preventDefault();
        var form_data = $('#datafm').serializeArray();
        $.each(form_data, function (key, input) {
          data.append(input.name, input.value);
        });

        var emi_discription = CKEDITOR.instances['emi_discription'].getData();
        data.append("emi_discription", emi_discription);

        data.append('key', 'value');
        $.ajax({
            url: "/emi_calculator/controller/ajaxdata.php",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
              console.log(data);
              var json=JSON.parse(data);
                if(json.status==0){
                  alert(json.msg);
                  window.location="/emi_calculator/controller/controller.php?action=emi_calculatorlist";
                }else{
                  alert(json.msg);
                }
            } 
        });
    });
  });

</script>


