<?php

		require_once(ROOT_PATH . "library/mysqli_function.php");

	class Model {

	    private $dbObj;

	    function __construct() {

	        $this->dbObj = new mysqli_function();
	    }

	    public function adddata($arr){
			$columns = implode(", ",array_keys($arr));
			$escaped_values = array_map('mysql_real_escape_string', array_values($arr));
			foreach ($escaped_values as $idx=>$data) $escaped_values[$idx] = "'".$data."'";
			$values  = implode(", ", $escaped_values);
			$sql = "INSERT INTO `tsr_unite_state_converter` ($columns) VALUES ($values)";
			$query = $this->dbObj->insert_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}
	    }


	    public function countlistdata($id=0,$state_id,$unit_id,$converted_unit){
	    	

	    		$state_idfilter='';
	    		$unit_idfilter='';
	    		$converted_unitfilter='';

	    		if($state_id > 0){
	    			$state_idfilter = ' AND state_id="'.$state_id.'"';
	    		}

	    		if($unit_id  > 0){
	    			$unit_idfilter = ' AND unit_id="'.$unit_id.'"';
	    		}

	    		if($converted_unit  > 0){
	    			$converted_unitfilter = ' AND converted_unitid="'.$converted_unit.'"';
	    		}

	    		$sql = "SELECT converter.*,ut.unitname,st.name FROM tsr_unite_state_converter as converter INNER JOIN tsr_state as st ON converter.state_id = st.id INNER JOIN tsr_unit_converter as ut ON converter.unit_id = ut.id WHERE converter.cat_id=1  $state_idfilter $unit_idfilter $converted_unitfilter order by converter.id DESC";
	    	
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	    public function listdata($id=0,$state_id,$unit_id,$converted_unit,$pageno){
	    	if($id>0){
                $sql = "SELECT converter.*,ut.unitname,st.name FROM tsr_unite_state_converter as converter INNER JOIN tsr_state as st ON converter.state_id = st.id INNER JOIN tsr_unit_converter as ut ON converter.unit_id = ut.id WHERE  converter.cat_id=1 AND converter.id = ".$id;
	    	}else{

	    		$page = isset($pageno) ? (int)$pageno : 1;

	    		$limit = 20;
	    		$offset = ($page-1) * $limit;

	    		$state_idfilter='';
	    		$unit_idfilter='';
	    		$converted_unitfilter='';

	    		if($state_id > 0){
	    			$state_idfilter = ' AND state_id="'.$state_id.'"';
	    		}

	    		if($unit_id  > 0){
	    			$unit_idfilter = ' AND unit_id="'.$unit_id.'"';
	    		}

	    		if($converted_unit  > 0){
	    			$converted_unitfilter = ' AND converted_unitid="'.$converted_unit.'"';
	    		}

	    		$sql = "SELECT converter.*,ut.unitname,st.name FROM tsr_unite_state_converter as converter INNER JOIN tsr_state as st ON converter.state_id = st.id INNER JOIN tsr_unit_converter as ut ON converter.unit_id = ut.id WHERE converter.cat_id=1  $state_idfilter $unit_idfilter $converted_unitfilter order by converter.id DESC LIMIT $limit OFFSET $offset";
	    	}
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }


	
	    public function deletedata($id){

	    	$sql = "delete from `tsr_unite_state_converter` WHERE `id` = ".$id;
	    	$query =  $this->dbObj->exec_query($sql, '');

	    }

	     public function updatedata($arr){

	        foreach($arr as $field=>$data)
			{
			$update[] = $field.' = \''.$data.'\'';
			}
			$sql = "UPDATE `tsr_unite_state_converter` SET ".implode(', ',$update)." WHERE id='".$arr['id']."'";
			$query = $this->dbObj->update_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}

	    }

	    public function findState($stateName){

			$sql = 'SELECT `id` FROM tsr_state WHERE  name LIKE "%'.$stateName.'%"';
			return $stateName =$this->dbObj->getAllData($sql);

	    }

	    public function findUnit($stateName){

			$sql = 'SELECT `id` FROM tsr_unit_converter WHERE unitname LIKE "%'.$stateName.'%"';
			return $stateName =$this->dbObj->getAllData($sql);

	    }

	    public function findCat($stateName){

			$sql = 'SELECT `id` FROM tsr_cal_cat WHERE name LIKE "%'.$stateName.'%"';
			return $stateName =$this->dbObj->getAllData($sql);

	    }

	}

 ?>
