<?php
ini_set('post_max_size','200M');
ini_set('upload_max_filesize','200M');
ini_set('max_execution_time', 500); //500 seconds
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "area_converter/commonvariable.php");
include_once(LIBRARY_PATH . "area_converter/model/model.php");
$modelObj = new Model();
admin_login();
$rolesession = explode(',',$_SESSION['login']['role_id']);
/*$resultdev = !empty(array_intersect($rolesession ,array(21,22) ));*/
$result = !empty(array_intersect($rolesession ,array(1,23)));
if(count($result) ==0){
$obj_common->redirect('/index.php');
}



if($_POST['state_id']==''){
	$data = array(
		'status' => 1,
		'msg'=>'Please select state'
	);
	echo json_encode($data);
	die;
}

if($_POST['title']==''){
	$data = array(
		'status' => 1,
		'msg'=>'Please select title'
	);
	echo json_encode($data);
	die;
}

if($_POST['unit_from']==''){
	$data = array(
		'status' => 1,
		'msg'=>'Please select state'
	);
	echo json_encode($data);
	die;
}


if($_POST['unit_id']==''){
	$data = array(
		'status' => 1,
		'msg'=>'Please select state unit type'
	);
	echo json_encode($data);
	die;
}


if($_POST['range']==''){
	$data = array(
		'status' => 1,
		'msg'=>'Please enter coverted input value'
	);
	echo json_encode($data);
	die;
}


if($_POST['converted_unit']==''){
	$data = array(
		'status' => 1,
		'msg'=>'Please select converted unit type '
	);
	echo json_encode($data);
	die;
}


if($_POST['post_id']>0 || $_POST['post_id']!=''){
		if($_POST['state_id']!=''){

			if($_FILES['icon']!=''){

				if (!file_exists(UPLOAD_PATH_CAL)) {
		            chmod(UPLOAD_PATH_CAL, 0777);
		            mkdir(UPLOAD_PATH_CAL, 0777, true);
		        }

		        $eventsSavePath  = UPLOAD_PATH_CAL;
		        $tempName = $_FILES['icon']['tmp_name'];
		        $eventsName = str_replace(' ','_',trim($_FILES['icon']['name']));
		        $eventsName = time().'_'.$eventsName;
		        $FinalPath = $eventsSavePath . '/' . $eventsName;

		        if(move_uploaded_file($tempName, $eventsSavePath . '/' . $eventsName)){
                   $arr['icon']=$eventsName;
		        }

			}
			$arr['title']=$_POST['title'];
			$arr['cat_id']=$_POST['cat_id'];
			$arr['state_id']=$_POST['state_id'];
			$arr['unit_id']= $_POST['unit_id'];
			$arr['meta_title']=$_POST['meta_title'];
			$arr['meta_keyword']=$_POST['meta_keyword'];
			$arr['meta_description']=$_POST['meta_description'];
			$arr['id']=$_POST['post_id'];
			$arr['status']=$_POST['status'];
			$arr['converted_range']=$_POST['range'];
			$arr['unit_from']=$_POST['unit_from'];
			$arr['converted_unitid']=$_POST['converted_unit'];
			$arr['emi_discription']=$_POST['emi_discription'];
			$arr['short_discription']=$_POST['short_discription'];
			$addpdf_upload_data = $modelObj->updatedata($arr);
			$data = array(
				'status' => 0,
				'msg'=>'data Update sucessfully'
			);
			echo json_encode($data);
			die;
		}

	}else{


		if($_POST['state_id']!=''){

			if($_FILES['icon']!=''){

				if (!file_exists(UPLOAD_PATH_CAL)) {
		            chmod(UPLOAD_PATH_CAL, 0777);
		            mkdir(UPLOAD_PATH_CAL, 0777, true);
		        }

		        $eventsSavePath  = UPLOAD_PATH_CAL;
		        $tempName = $_FILES['icon']['tmp_name'];
		        $eventsName = str_replace(' ','_',trim($_FILES['icon']['name']));
		        $eventsName = time().'_'.$eventsName;
		        $FinalPath = $eventsSavePath . '/' . $eventsName;

		        if(move_uploaded_file($tempName, $eventsSavePath . '/' . $eventsName)){
                   $arr['icon']=$eventsName;
		        }

			}

			$arr['title']=$_POST['title'];
			$arr['cat_id']=$_POST['cat_id'];
			$arr['state_id']=$_POST['state_id'];
			$arr['unit_id']= $_POST['unit_id'];
			$arr['meta_title']=$_POST['meta_title'];
			$arr['meta_keyword']=$_POST['meta_keyword'];
			$arr['meta_description']=$_POST['meta_description'];
			$arr['converted_range']=$_POST['range'];
			$arr['unit_from']=$_POST['unit_from'];
			$arr['converted_unitid']=$_POST['converted_unit'];
			$arr['emi_discription']=$_POST['emi_discription'];
			$arr['short_discription']=$_POST['short_discription'];
			$addpdf_upload_data = $modelObj->adddata($arr);
			if($addpdf_upload_data>0){
				$data = array(
					'status' => 0,
					'msg'=>'data inserted sucessfully'
				);
				echo json_encode($data);
				die;
			}else{
				$data = array(
					'status' => 1,
					'msg'=>'data not inserted sucessfully'
				);
				echo json_encode($data);
				die;
			}
		}else{
			$data = array(
				'status' => 1,
				'msg'=>'data not inserted sucessfully'
			);
			echo json_encode($data);
			die;
		}
	}

?>
