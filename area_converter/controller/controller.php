<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "area_converter/commonvariable.php");
include_once(LIBRARY_PATH . "area_converter/model/model.php");

$modelObj = new Model();

admin_login();
$rolesession = explode(',',$_SESSION['login']['role_id']);
/*$resultdev = !empty(array_intersect($rolesession ,array(21,22) ));*/
$result = !empty(array_intersect($rolesession ,array(1,23)));
if(count($result) ==0){
$obj_common->redirect('/index.php');
}

$message_arr=array( "insert"=>"Record(s) has been added successfully!",
                    "update"=>"Record(s) has been updated successfully!",
                    "delete"=>"Record(s) has been deleted successfully!",
                    "cstatus"=>"Record(s) status has been changed!",
                    "unique"=>"Name already in record!");

include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('area_converter Managemant', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/area_converter/controller/controller.php";


switch ($action) {

    case 'add_area_converter':
    include(ROOT_PATH . "/area_converter/view/addAreaConverter.php");
    break;

    case 'edit_area_converter':
    $listdata = $modelObj->listdata($_GET['id']);
    //$stroylistdata = $modelObj->stroylistdata($_GET['id']);
    include(ROOT_PATH . "/area_converter/view/addAreaConverter.php");
    break;

    case 'list_area_converter':
    $pageno = $_GET['pageno'];
    $pagename= 'list_area_converter';
    $countlistdata = $modelObj->countlistdata('',$state_id,$unit_id,$converted_unit);
    $listdata = $modelObj->listdata('',0,0,0,$pageno);
    include(ROOT_PATH . "/area_converter/view/listAreaConverter.php");
    break;


    case 'search':

        $state_id = $_GET['state_id'];
        $unit_id= $_GET['unit_id'];
        $converted_unit= $_GET['converted_unit'];
        $pageno = $_GET['pageno'];

    $pagename= 'search';
    $listdata = $modelObj->listdata('',$state_id,$unit_id,$converted_unit,$pageno);
    $countlistdata = $modelObj->countlistdata('',$state_id,$unit_id,$converted_unit);
    include(ROOT_PATH . "/area_converter/view/listAreaConverter.php");
    break;

    case 'delete_area_converter':
    $deletedata = $modelObj->deletedata($_GET['id']);
    $listdata = $modelObj->listdata('');

    include(ROOT_PATH . "/area_converter/view/listAreaConverter.php");
    break;

    case 'import_calculator':
    include(ROOT_PATH . "/area_converter/view/import_calculator.php");
    break;

    case 'Upload_calculator':

        $fileMimes = array(
        'text/x-comma-separated-values',
        'text/comma-separated-values',
        'application/octet-stream',
        'application/vnd.ms-excel',
        'application/x-csv',
        'text/x-csv',
        'text/csv',
        'application/csv',
        'application/excel',
        'application/vnd.msexcel',
        'text/plain',
        'text/xls', 
        'text/xlsx',
        'application/excel',
        'application/vnd.msexcel',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        );

        if(!empty($_FILES['importfile']['name']) && in_array($_FILES['importfile']['type'], $fileMimes)){

            $fileName = $_FILES['importfile']['tmp_name'];

            $csvFile = fopen($_FILES['importfile']['tmp_name'], 'r');
            // Skip the first line
            fgetcsv($csvFile);
            // Parse data from CSV file line by line  

            if (($handle = fopen($fileName, 'r')) !== false) {

                $projectArray= array();  
                $dataPostFormValuesForLead= '';   
                $arradata='';
                $i=0;  
                $propertyid='';
                $state='';
                $unitId='';
                $convertedUnit='';
                $catName = '';

                while (($getData = fgetcsv($csvFile, 10000, ",")) !== FALSE)
                {

                    $state = $modelObj->findState($getData[0]);
                    $catName = $modelObj->findCat($getData[1]);
                    $unitId = $modelObj->findUnit($getData[3]);
                    $convertedUnit = $modelObj->findUnit($getData[5]);
                    
                    $arraydata[$i]['state_id']=$state[0]['id'];
                    $arraydata[$i]['cat_id']=$catName[0]['id'];
                    $arraydata[$i]['unit_from']=$getData[2]['id'];
                    $arraydata[$i]['unit_id']=$unitId[0]['id'];
                    $arraydata[$i]['converted_range']=$getData[4];
                    $arraydata[$i]['converted_unitid']=$convertedUnit[0]['id'];
                    $arraydata[$i]['title']=$getData[6];
                    $arraydata[$i]['emi_discription']=$getData[7];
                    $arraydata[$i]['meta_keyword']=$getData[8];
                    $arraydata[$i]['meta_description']=$getData[9];
                    $arraydata[$i]['status']=1;

                $i++;}

                fclose($csvFile);

            }


            for ($i=0; $i < count($arraydata) ; $i++) { 

                if($arraydata[$i]['unit_id'] !='' && $arraydata[$i]['converted_range']!=''){
                     $insertSql = $modelObj->adddata($arraydata[$i]);
                }
            }

         
        }

        $obj_common->redirect("/area_converter/controller/controller.php?action=import_calculator&msg=insert");

    break;

}

?>
