  <link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>view/js/jquery.dropdown.css">
  <script src="<?php echo $site['URL']?>view/js/jquery.dropdown.js"></script>
  <script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>
  <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>view/js/jquery.dropdown.css">
  <script src="<?php echo $site['URL']?>view/js/jquery.dropdown.js"></script>
<!-- <script>
function richTextBox($fieldname, $value = "") {

   
   $CKEditor = new CKEditor();
   $config['toolbar'] = array(
    array( 'Bold', 'Italic', 'Underline', 'Strike'),
    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
    array('Format'),
    array( 'Font', 'FontSize', 'FontColor', 'TextColor'),
    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ),
    array('Image', 'Table', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'),
    array('Subscript','Superscript'),

   );
   $CKEditor->basePath = '/ckeditor/';
   $CKEditor->config['width'] = 975;
   $CKEditor->config['height'] = 400;
   
   $CKEditor->editor($fieldname, $value, $config);
}
</script> -->

<style>
  .citydata{
    width: 150px;
    float: left;
    padding: 2px;
}
  .dropdown-display {
  min-height: 20px !important;
  }
  .second-toolbar {
    display: none!important;
}

  .fr-wrapper div:nth-child(1) {
    display: none!important;
}
button#insertVideo-1 {
    display: none;
}
.dropdown-multiple,input[type="text"],input[type="email"]{
  width: 83%;
}

.block2 input{
  width: 35%;float: left;padding: 3px;margin:2px;
}

.block2 span{
 float: left;padding: 3px;margin:2px;
}
.custombtn {
    background: #e5e5e5;
    text-align: center;
    width: 100px;
    border-radius: 9px;
    padding: 2px;
}
.block,.block1{
  clear:both;
}
.block3 input{
  width: 20%;float: left;padding: 3px;margin:2px;
}

.block3 span{
 float: left;padding: 3px;margin:2px;
}
.autocomplete-items {

  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;

    width: 36%;

}

.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
  background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
</style>
<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Blog Detail</h3></th></tr>
</thead>
<tbody>


<tr class="<?php echo($cls1)?>">
<td width="15%" align="right"><strong><font color="#FF0000">*</font>Section Name</strong></td>
  <td><?php 
  echo AjaxDropDownList(TABLE_SECTIONS,"sec_id","id","name",$_GET["sec_id"],"id",$site['CMSURL'],"subcatDiv","subcatLabelDiv")?></td>
</tr>

<tr class="<?php echo($cls1)?>">
<td width="15%" align="right"><strong><font color="#FF0000">*</font>Category Name</strong></td>
  <td><?php 
  echo AjaxDropDownList(TABLE_ART_CATEGORY,"cat_id","id","category_name",$_GET["cat_id"],"id",$site['CMSURL'],"subcatDiv","subcatLabelDiv")?></td>
</tr>



<tr class="<?php echo($clsRow1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Author Name</strong></td>
  <td><?php 
    echo AjaxDropDownList(TABLE_AUTHORS,"auth_id","id","name",$_GET["auth_id"],"id",$site['CMSURL'],"SubauthDiv","SubauthLabelDiv")?>
</td>


<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Title</strong></td>
  <td><input name="title" type="text" title="title" lang='MUST' value="<?php echo($rec['title'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Intro Section</strong></td>
  <td><input name="intro_section" id ="intro_section" type="text" value="<?php echo($rec['intro_section'])?>" size="50%" maxlength="200"/></td>
</tr>

<!--<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Sef Url</strong></td>
  <td><input name="sef_url" type="text" title="title" lang='Sef Url' value="<?php echo($rec['sef_url'])?>" size="50%" maxlength="120" /></td>
</tr>-->

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Content</strong></td>
  <td><textarea name="content" class="ckeditor"  id="content" rows="8" cols="70"><?php echo($rec['content'])?></textarea>  </td>
</tr>

<?php $dd= json_decode($rec['table_of_contents']); ?>


<tr>
  <td align="right" >Table OF content</td>
  <td><div class="optionBox">
    <?php for ($i=0; $i < count($dd->name); $i++) { ?>
    <div class="block block2">
       <input type="text" name="anchor[]" value="<?php echo $dd->anchor[$i]; ?>"/> <input type="text" name="name[]" value="<?php echo $dd->name[$i]; ?>"/> <span class="remove custombtn">Remove Option</span>
    </div>
    <?php } ?>
    <div class="" style="width:100%"></div>
    <div class="block">
        <span class="add custombtn">Add Option</span>
    </div>
</div></td>
</tr>


  <tr class="<?php echo($clsRow1)?>">
  <td width="15%" align="right"><strong>Add to property position</strong></td>
  <td><input name="property_pos" type="text" title="property_pos" lang='MUST' value="<?php if($rec['property_pos']==0){ echo '4'; }else{ echo $rec['property_pos']; } ?>"  /></td>
  </tr>

  <tr class="<?php echo($clsRow1)?>">
    <td width="15%" align="right"><strong>Add to property </strong></td>
     <td>
      <?php 
      $sql = 'SELECT `id`,`property_name` FROM tsr_properties WHERE status=1';
      $prolist =$obj_mysql->getAllData($sql);
      ?>
      
      <div class="dropdown-mul-2">
        <select  id="propty_id" multiple name="propty_id[]" placeholder="Select Property">
          <?php foreach($prolist as $value){ ?>
          <option value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],explode(',',$rec["propty_id"]))){ echo 'selected';} ?>><?php echo $value['property_name']; ?> </option>
          <?php } ?>
        </select>
      </div>
    </td>
 </tr>

  <tr class="<?php echo($clsRow1)?>">
    <td align="right" ><strong>Select Image</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['imgname']=='') {?> title="Select Image" <?php } ?> name="imgname" id="imgname" /></td>
          <td width="62%"><?php if($rec['imgname']):?>
            <br />
            <img src="<?php echo  ($site['ARTICLELOGOURL'].$rec['id']."/".$rec['imgname'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['imgname'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

    <tr class="<?php echo($clsRow1)?>">
    <td align="right" ><strong>Select Twitter card Image(800 x 418 px)</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['imgname_tc']=='') {?> title="Select Image" <?php } ?> name="imgname_tc" id="imgname_tc" /></td>
          <td width="62%"><?php if($rec['imgname_tc']):?>
            <br />
            <img src="<?php echo  ($site['ARTICLELOGOURL'].$rec['id']."/".$rec['imgname_tc'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name_tc" value="<?php echo ($rec['imgname_tc'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

<!--<tr class="<?php echo($clsRow1)?>">
<td align="right"><strong><font color="#FF0000">*</font>Published Date</strong></td><td>
	<input type="text" name="published_date" />
	<script language="JavaScript">
	new tcal ({
		// form name
		'formname': 'form123',
		// input name
		'controlname': 'published_date'
	});
 
	</script>
</td>
</tr>-->

<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Tags</strong></td>
           <td><textarea  name="tags" id="tags" rows="3" cols="175" ><?php echo stripslashes($rec['tags'])?></textarea></td>
 </tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000"></font>Video Name</strong></td>
  <td><textarea  name="video_name" id="video_name" rows="1" cols="175" ><?php echo stripslashes($rec['video_name'])?></textarea></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000"></font>Embed Video</strong></td>
  <td><textarea  name="embed_video" id="embed_video" rows="5" cols="175" ><?php echo stripslashes($rec['embed_video'])?></textarea></td>
</tr>


  <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
           <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
           <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
           <td><textarea  name="meta_description" id="meta_description" rows="5" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
 </tr>
<!--<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Ordering</strong></td>
           <td><textarea name="ordering" id="ordering" rows="1" cols="175" ><?php echo stripslashes($rec['ordering'])?></textarea></td>
 </tr>-->

<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Is Home Display</strong> </td>
  <td>
  <input type="radio" name="is_home_display" value="1" id="1" <?php if (isset($rec['is_home_display'])&&$rec['is_home_display']==1){ echo("checked='checked'");}else{echo("checked='false'");} ?> /> 
  <label for="1">Yes</label> 
  <input type="radio" name="is_home_display" value="0" id="0" <?php if (!isset($rec['is_home_display']) ||$rec['is_home_display']==0){ echo("checked='checked'");} ?>/> 
  <label for="0">No</label>  </td>
</tr>
<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Web Scope</strong> </td>
  <td>
  <input type="radio" name="web_scope" value="1" id="1" <?php if (isset($rec['web_scope'])&&$rec['web_scope']==1){ echo("checked='checked'");}else{echo("checked='false'");} ?> /> 
  <label for="1">Residential</label> 
  <input type="radio" name="web_scope" value="2" id="2" <?php if (isset($rec['web_scope'])&&$rec['web_scope']==2){ echo("checked='checked'");}else{echo("checked='false'");} ?> /> 
  <label for="1">Commercial</label>
  <input type="radio" name="web_scope" value="0" id="0" <?php if (!isset($rec['web_scope']) ||$rec['web_scope']==0){ echo("checked='checked'");} ?>/> 
  <label for="0">Both</label>  </td>
</tr>
<tr class="<?php echo($clsRow2)?>"><td align="right" ><strong>360 Edge</strong> </td>
  <td>
  <input type="radio" name="360_edge" value="0" <?php if (isset($rec['360_edge'])&&$rec['360_edge']==0){ echo("checked='checked'");}else{echo("checked='false'");} ?> /> 
  <label for="0">No</label> 
  <input type="radio" name="360_edge" value="1" <?php if (isset($rec['360_edge'])&&$rec['360_edge']==1){ echo("checked='checked'");}else{echo("checked='false'");} ?> /> 
  <label for="1">Yes</label>
  </td>
</tr>
<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
  <input type="radio" name="status" value="3" id="0" <?php if ($rec['status']=="3"){ echo("checked='checked'");} ?>/> 
	<label for="3">Schedule</label>
	</td>
</tr>
<?php 
  $dt = $rec['scheduledTime'];
  $dtArr = explode(' ',$dt);
  $scDate = $dtArr[0];
  $scTime = $dtArr[1];
?>
<tr  id='schDiv' class="<?php echo($clsRow1)?>"><td align="right" ><strong>Schedule Time</strong> </td>
  <td>
    <input type='date' value='<?php echo $scDate; ?>' name='schDate'>
    <input type='time' value='<?php echo $scTime; ?>' name='schTime'>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
 

</tbody>
</table>
</form>
<script>

$(document).ready(function(){
  var checkedVal = $('input:radio[name="status"]:checked').val();

  if (checkedVal == 3) {
    $('#schDiv').show();
  } else {
    $('#schDiv').hide();
  }
  
});
  
  $('.dropdown-mul-2').dropdown({
      limitCount: 500,
      searchable: true
    });

</script>

<script type="text/javascript">
  $('input:radio[name="status"]').change(function(){

    var changedVal = $(this).val(); 

    if (changedVal == 3) {
      $('#schDiv').show();
    } else {
      $('#schDiv').hide();
    }

  });

$('.add').click(function() {
    $('.block:last').before('<div class="block block2" > <input type="text" name="anchor[]" placeholder="Enter the  id :1" />  <input type="text" name="name[]" placeholder="Enter the text " /><span class="remove custombtn">Remove Option</span></div>');
});
$('.optionBox').on('click','.remove',function() {
  $(this).parent().remove();
});

function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/

              authorfun(inp.value);
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });

    function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/
var countries = <?php echo json_encode($autrname); ?>;
autocomplete(document.getElementById("myInput"), countries);


</script>

