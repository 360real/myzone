<?php
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include $_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php";

include_once LIBRARY_PATH . "library/server_config_admin.php";
admin_login();
$message_arr = array("insert" => "Record(s) has been added successfully!",
    "update" => "Record(s) has been updated successfully!",
    "delete" => "Record(s) has been deleted successfully!",
    "cstatus" => "Record(s) status has been changed!",
    "unique" => "Name already in record!");
$tblName = TABLE_BLOG; //main table name
$fld_id = 'id'; //primery key
$fld_status = 'status'; // staus field
$fld_orderBy = 'id'; // default order by field
$page_name = C_ROOT_URL . '/blog/controller/controller.php';
$clsRow1 = 'clsRow1';
$clsRow2 = 'clsRow2';
$action = remRegFx($_REQUEST['action']); // action to perform task like Update, Create , Delete etc.
$rec_id = remRegFx($_GET['id']);
$arr_id = $_POST['items'] ? $_POST['items'] : array($rec_id); // for radio button
$query_str = "page=$page";

/* choices to where page is redirect
if action=create then goes to v
iew
action =update then model
action default to view i.e index.php
*/

$file_name = "";
switch ($action) {
    case 'Create':
    case 'Update':
        $file_name = ROOT_PATH . "/blog/model/model.php";
        break;
    default:
        $file_name = ROOT_PATH . "/blog/view/view.php";
        break;
}
// echo "<pre>"; // print_r($_POST);
// $dt = $_POST['schDate'].' '.$_POST['schTime'];
// echo  date("Y-m-d H:i:s",strtotime($dt));
// die;

/***********************************************************************************/
//Code  for search a record from database on click submit  button

/***********************************************************************************/
if ($_GET["action"] != "" && $_GET["action"] != "Create") {
    if ($_GET["action"] == "search") {
        $sql = "SELECT BLG.* FROM " . TABLE_BLOG . " BLG WHERE ";
        if ($_GET["user_id"] != "") {
            $sql .= "BLG.id=" . $_GET["user_id"];
        }

        if ($_GET["title"] != "") {
            $sql .= " BLG.title like '%" . trim($_GET["title"]) . "%'";
        }

        if ($_GET["status"] != "") {
            $sql .= "BLG.status=" . $_GET["status"];
        }

        if ($_GET["startDate"] != "" && $_GET["endDate"]) {
            $sql .= " and date(BLG.created_date)>='" . date('Y-m-d', strtotime($_GET["startDate"])) . "' and date(BLG.created_date)<='" . date('Y-m-d', strtotime($_GET["endDate"])) . "'";
        } else
        if ($_GET["startDate"] != "") {
            $sql .= " and date(BLG.created_date)='" . date('Y-m-d', strtotime($_GET["startDate"])) . "'";
        } else
        if ($_GET["endDate"] != "") {
            $sql .= " and date(BLG.created_date)='" . date('Y-m-d', strtotime($_GET["endDate"])) . "'";
        }
    } else if ($_GET["action"] == "Update") {

        $sql = "SELECT BLG.* FROM " . TABLE_BLOG . " BLG ";
        $sql .= " WHERE BLG.id=" . $_GET["id"];
        //$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
    } else if ($_GET["action"] == "Delete") {
        /******* uncomment below 2 lines two delete files as well as directory ***********/
        //array_map('unlink', glob(UPLOAD_PATH_DEVELOPERS_LOGO.$_GET['id']."/*.*")); // method to unlink an image
        //@rmdir(UPLOAD_PATH_DEVELOPERS_LOGO.$_GET['id']); // method to delete a directory
        /******* uncomment below 2 lines two delete files as well as directory ***********/
        $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
        $obj_common->redirect($page_name . "?msg=delete");
    }
    $s = ($_GET['s'] ? 'ASC' : 'DESC');
    $sort = ($_GET['s'] ? 0 : 1);
    $f = $_GET['f'];
    if ($s && $f) {
        $sql .= " ORDER BY $f  $s";
    } else {
        $sql .= " ORDER BY BLG.$fld_orderBy";
    }

} else {
    $sql = "SELECT BLG.* FROM " . TABLE_BLOG . " BLG ORDER by id  DESC";
    //$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
}

/*---------------------paging script start----------------------------------------*/
$obj_paging->limit = 10;
if ($_GET['page_no']) {
    $page_no = remRegFx($_GET['page_no']);
} else {
    $page_no = 0;
}

$queryStr = $_SERVER['QUERY_STRING'];
/*--------------------if page_no alreay exists please remove them ---------------------------*/
$str_pos = strpos($queryStr, 'page_no');
if ($str_pos > 0) {
    $queryStr = str_replace(substr($queryStr, ($str_pos - 1), strlen($queryStr)), "", $queryStr);
}

/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
$total_num = $obj_mysql->get_num_rows($sql);
$paging = $obj_paging->next_pre($page_no, $total_num, $page_name . "?$queryStr&", 'textArial11Bold', 'textArial11orgBold');
$total_rec = $obj_paging->total_records($total_num);
$sql .= " LIMIT $obj_paging->lower,$obj_paging->limit";
/*---------------------paging script end----------------------------------------*/
$rec = ($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));

/************************************************************************************************/
// code for search and pagination ends here

/************************************************************************************************/

/*if(isset($_POST['namesubmit']))
{

$arr=$_POST;
$rec=$_POST;
$obj_mysql->insert_data($tblName,$arr);
$obj_common->redirect($page_name."?msg=insert");
}*/

if (count($_POST) > 0) {
    $arr = $_POST;
    $arr['published_date'] = date('Y-m-d', strtotime($_GET["published_date"]));
    $dt = $_POST['schDate'].' '.$_POST['schTime'];
    $arr['scheduledTime'] = date("Y-m-d H:i:s",strtotime($dt));

    $data = array(
        'anchor'=>$_POST['anchor'],
        'name'=>$_POST['name']
    );
    $data2 = json_encode($data);
    $arr['table_of_contents']=$data2;
    $arr['propty_id'] = implode(",", $_POST['propty_id']);
    $rec = $_POST;
    if ($rec_id) {
        $arr['modified_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['modified_by'] = $_SESSION['login']["id"];

        if ($obj_mysql->isDupUpdate($tblName, 'title', $arr['title'], 'id', $rec_id)) {
            echo "aa-" . UPLOAD_PATH_ARTICLE_LOGO . $rec_id . "/";
            if ($_FILES['imgname']['name'] != "" && $counterRename != "1") {
                echo $_FILES['imgname']['name'];
                // @unlink(UPLOAD_PATH_ARTICLE_LOGO . $rec_id . "/" . $_POST['old_file_name']);
                $handle = new upload($_FILES['imgname']);
                if ($handle->uploaded) {
                    $handle->image_resize         = true;
                    $handle->image_x              = 1200;
                    $handle->image_y              = 900;
                    $handle->image_ratio_y        = true;
                    $handle->file_safe_name = true;
                    $handle->process(UPLOAD_PATH_ARTICLE_LOGO . $rec_id . "/");
                    if ($handle->processed) {
                        //echo 'image resized';
                        $arr['imgname'] = $handle->file_dst_name;
                        $handle->clean();
                    } else {
                        //echo "error";
                        echo 'error : ' . $handle->error;
                    }
                }
			}
			
			if ($_FILES['imgname_tc']['name'] != "" && $counterRename != "1") {
                echo $_FILES['imgname_tc']['name'];
                @unlink(UPLOAD_PATH_ARTICLE_LOGO . $rec_id . "/" . $_POST['old_file_name_tc']);
                $handle = new upload($_FILES['imgname_tc']);
                if ($handle->uploaded) {
                    $handle->image_resize         = true;
                    $handle->image_x              = 1200;
                    $handle->image_y              = 900;
                    $handle->image_ratio_y        = true;
                    $handle->file_safe_name = true;
                    $handle->process(UPLOAD_PATH_ARTICLE_LOGO . $rec_id . "/");
                    if ($handle->processed) {
                        //echo 'image resized';
                        $arr['imgname_tc'] = $handle->file_dst_name;
                        $handle->clean();
                    } else {
                        //echo "error";
                        echo 'error : ' . $handle->error;
                    }
                }
			}
			
            $obj_mysql->update_data($tblName, $fld_id, $arr, $rec_id);
            $obj_common->redirect($page_name . "?msg=update");
        } else {
            //$obj_common->redirect($page_name."?msg=unique");
            $msg = 'unique';
        }
    } else {
        /*$sql = mysql_query("SELECT MAX(id) as maxId FROM ".TABLE_ART_SUB_CATEGORY);
        $recId = mysql_fetch_array($sql);
        $maxId = $recId["maxId"];

        if($maxId!="")
        $setmaxid = $maxId+1;
        else
        $setmaxid = 1;*/

        //$arr['modified_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['created_by'] = $_SESSION['login']["id"];
        $arr['published_date'] = date('Y-m-d', strtotime($_GET["published_date"]));
        $dt = $_POST['schDate'].' '.$_POST['schTime'];
        $arr['scheduledTime'] = date("Y-m-d H:i:s",strtotime($dt));
        $data = array(
        // 'anchor'=>$_POST['anchor'],
        'name'=>$_POST['name']
        );
        $data2 = json_encode($data);
        $arr['propty_id'] = implode(",", $_POST['propty_id']);
        $arr['table_of_contents']=$data2;

        if ($obj_mysql->isDuplicate($tblName, 'title', $arr['title'])) {
            $arr['published_date'] = date('Y-m-d', strtotime($_POST["published_date"]));
            $setmaxid = $obj_mysql->insert_data($tblName, $arr);
            if ($_FILES['imgname']['name'] != "") {
                echo $_FILES['imgname']['name'];
                $handle = new upload($_FILES['imgname']);
                if ($handle->uploaded) {
                    
                    $handle->image_resize         = true;
                    $handle->image_x              = 1200;
                    $handle->image_y              = 900;
                    $handle->image_ratio_y        = true;
                    
                    $handle->file_safe_name = true;
                    $handle->process(UPLOAD_PATH_ARTICLE_LOGO . $setmaxid . "/");
                    if ($handle->processed) {
                        //echo 'image resized';
                        $arr['imgname'] = $handle->file_dst_name;
                        @unlink(UPLOAD_PATH_ARTICLE_LOGO . $setmaxid . "/" . $_POST['old_file_name']);
                        $handle->clean();
                    } else {
                        echo 'error : ' . $handle->error;
                    }
                }
                $obj_mysql->update_data($tblName, $fld_id, $arr, $setmaxid);

			}
			
			if ($_FILES['imgname_tc']['name'] != "") {
                echo $_FILES['imgname_tc']['name'];
                $handle = new upload($_FILES['imgname_tc']);
                if ($handle->uploaded) {
                    $handle->image_resize         = true;
                    $handle->image_x              = 1200;
                    $handle->image_y              = 900;
                    $handle->image_ratio_y        = true;
                    $handle->file_safe_name = true;
                    $handle->process(UPLOAD_PATH_ARTICLE_LOGO . $setmaxid . "/");
                    if ($handle->processed) {
                        //echo 'image resized';
                        $arr['imgname_tc'] = $handle->file_dst_name;
                        @unlink(UPLOAD_PATH_ARTICLE_LOGO . $setmaxid . "/" . $_POST['old_file_name_tc']);
                        $handle->clean();
                    } else {
                        echo 'error : ' . $handle->error;
                    }
                }
                $obj_mysql->update_data($tblName, $fld_id, $arr, $setmaxid);

            }

            $obj_common->redirect($page_name . "?msg=insert");
        } else {
            $obj_common->redirect($page_name . "?msg=unique");
            $msg = 'unique';
        }
    }
}

//second if block ends here

$list = "";
for ($i = 0; $i < count($rec); $i++) {
    if ($rec[$i]['status'] == "1") {
        $st = '<font color=green>Active</font>';
    } elseif($rec[$i]['status'] == "2") {
        $st = '<font color=red>Inactive</font>';
    } elseif ($rec[$i]['status'] == "3"){
        $st = '<font color=orange>Scheduled</font>';
    }
    if ($rec[$i]['web_scope'] == "0") {
        $webScope = "Both(Commercial/Residential)";
        }elseif($rec[$i]['web_scope'] == "2"){
            $webScope = "Commercial";
        }else{
            $webScope = "Residential";
        }
            $clsRow = ($i % 2 == 0) ? 'clsRowView1' : 'clsRowView2';
    //print_r($rec);

    $list .= '<tr class="' . $clsRow . '">

					<td class="center">' . $rec[$i]['title'] . '</td>
					<td class="center"><img src="' . $site['ARTICLELOGOURL'] . $rec[$i]['id'] . "/" . $rec[$i]['imgname'] . '" border="0" width="93" height="63"/></td>
                    <td class="center">' . $webScope . '</td>
                    <td class="center">' . $st . '</td>
					<td class="center">' . $rec[$i]['created_date'] . '</td>
					<td class="center">' . $rec[$i]['modified_date'] . '</td>
					<td class="center">' . $rec[$i]['ipaddress'] . '</td>
					<td >
                    <a  target="_blank" class="edit" href="/faq_news/controller/controller.php?news_id=' . $rec[$i]['id'] . '&search=Submit">Faq List</a>

					<a href="' . $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&auth_id=' . $rec[$i]['auth_id'] . '&sec_id=' . $rec[$i]['sec_id'] . '&cat_id=' . $rec[$i]['cat_id'] . '" target="_blank" class="edit">Edit</a>

						<a href="' . $page_name . '?action=Delete&id=' . $rec[$i]['id'] . '" class="delete" onclick="return conf()">Delete</a>
                        
					</td>
				</tr>';
}
if ($list == "") {
    $list = "<tr><td colspan=10 align='center' height=50>No Record(s) Found.</td></tr>";
}

?>

<!--************************************** code to include common header ***************************************/-->
<?php include LIBRARY_PATH . "includes/header.php";
$commonHead = new CommonHead();
$commonHead->commonHeader('Blog Manager', $site['TITLE'], $site['URL']);
?>
<!--************************************** End of code to include header ***************************************/-->
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php
if ($_GET['msg'] != "") {
    echo ('<div class="notice">' . $message_arr[$_GET['msg']] . '</div>');
}
if ($msg != "") {
    echo ('<div class="notice">' . $message_arr[$msg] . '</div>');
}
?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include $file_name;?></td> <!-- include file_name to get the page on controller page -->
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include LIBRARY_PATH . "includes/footer.php";?>
