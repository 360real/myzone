<?php

// include files for database connectivity
include "/var/www/html/myzone/adminproapp/includes/set_main_conf.php";
include_once LIBRARY_PATH . "library/server_config_admin.php";

// fetches all scheduled blogs
$fetchQ = "select id,status,scheduledTime from tsr_blog where status=3";
$scheduledBlogs = $obj_mysql->getAllData($fetchQ);

if (count($scheduledBlogs) > 0) {

    // array which will contain all blog Ids which were schduled for publishing on website
    $toBeUpdatedBlogs = array();

    foreach ($scheduledBlogs as $key => $val) {

        array_push($toBeUpdatedBlogs, $val['id']);
    }

    $idStr = implode(',', $toBeUpdatedBlogs);
    $updateQ = "update tsr_blog set status = 1 where id in($idStr)";
    
    // publish all blogs on website
    $obj_mysql->exec_query($updateQ);
}
