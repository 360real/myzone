<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


//include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");

include_once LIBRARY_PATH . "library/server_config_admin.php";


include_once(LIBRARY_PATH . "banner_manager/commonvariable.php");
include_once(LIBRARY_PATH . "banner_manager/model/Model.php");
$modelObj = new Model();
admin_login();

/* Including common Header */
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('Manage Banner Managemant', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/banner_manager/controller/controller.php";
$action = $_REQUEST['action'];


switch ($action) {

    case 'Create':
        if (is_array($_POST) && count($_POST) > 0) {
            $insertIntoDb = $modelObj->insertIntoDb();
            if ($insertIntoDb) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Inserted successfully!");
                    window.location.href = "/banner_manager/controller/controller.php";
                </script>
                <?php

            }
        }
        include(ROOT_PATH . "/banner_manager/view/EventDetailPage.php");
        break;

   
    case 'BannerManager':
        if (is_array($_POST) && count($_POST) > 0) {

            $imageads= '';

            if($_POST['ads_platform']!=''|| $_POST['type_advertisement']!='' || $_POST['banner_position']!='' ||$_POST['country_id']!='' ||$_POST['bannerimage']!='' ||$_POST['title']!='' ||$_POST['is_active']!='' ||$_POST['static_advertisement']!='' ||$_POST['start_date']!='' ||$_POST['end_date']!='' ||$_POST['link_url']!='' ||$_POST['page_url']!='' ||$_POST['page']!='' ||$_POST['type_advertisement']==0 || $_POST['banner_position']== 0 ){
              /*  echo '<pre>';
                print_r($_POST);
                echo '</pre>';
*/
                $bannerpos_chk = $modelObj->getbannersize($_POST['banner_position']);
               /* print_r($bannerpos_chk);
*/

                if($bannerpos_chk[0]['id']==$_POST['banner_position']) {

                    $filename = $_FILES['bannerimage']['tmp_name'];
                    list($width, $height) = getimagesize($filename);
                    if($width!=$bannerpos_chk[0]['banner_size_width'] &&  $height!=$bannerpos_chk[0]['banner_size_height']){
                      $imageads= "Please upload image width= '".$bannerpos_chk[0]['banner_size_width']."' ,height='".$bannerpos_chk[0]['banner_size_height']."'";
                    }else{
                       $insertBannerDetail = $modelObj->insertBannerDb();
                    }

                }else{

                      $imageads= "Please upload image width= '".$bannerpos_chk[0]['banner_size_width']."' ,height='".$bannerpos_chk[0]['banner_size_height']."'";
                }
              
                if (!$insertBannerDetail) {
                    echo "";
                } else {
                    ?>
                    <script type="text/javascript">
                        window.location.href = "/banner_manager/controller/controller.php?action=BannerListing";
                        alert('Data Inserted Successfully !');

                    </script>
                <?php

                }

            }

        }

        include(ROOT_PATH . "/banner_manager/view/BannerManagerView.php");
        break;

    case 'BannerListing':
         $getDataForAddBanner = $modelObj->getDataaddBanner();
        // print_r($getDataForAddBanner);die();
        include(ROOT_PATH . "/banner_manager/view/BannerListing.php");
        break; 

    case 'UpdateBanner':
        if (isset($_GET['BannerId']) && $_GET['BannerId'] != "") {
            $EventBannerId = $_GET['BannerId'];
            $getDataForBanner = $modelObj->getDataaddBanner($EventBannerId);
            $bannerData = $getDataForBanner[0];
        }


        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {

            $UpdateBannerOnEdit = $modelObj->updateBannerDetails($EventBannerId);
            if ($UpdateBannerOnEdit) {
                ?>
                <script type="text/javascript">
                    alert("Banner Details Updated Successfully !");
                    window.location.href = "/banner_manager/controller/controller.php?action=BannerListing";
                </script>
            <?php

            }
        }
        include(ROOT_PATH . "/banner_manager/view/BannerManagerView.php");
        break;       

    default :

        $getDataForview = $modelObj->getDataaddBanner();

        include(ROOT_PATH . "banner_manager/view/view.php");
        break;
}
?>
