<?php
$typeOfBanner = array('1'=>'Road Block','2'=>'Tower Banner','3'=>'Horizonal banner','4'=>'LeaderBoard');
$adspltfm = array('1'=>'Desktop','2'=>'Mobile');
$bannerPosition = array('1'=>'Left','2'=>'Right','3'=>'Top','4'=>'Bottom','5'=>'Mid-1','6'=>'Mid-2','7'=>'Mid-3');
$bannerpos_list = $modelObj->getbannersize();
$bannerposjson = json_encode($bannerpos_list);
?>
<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo $site['URL']?>view/css/admin-style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/css.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/event.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $site['URL']?>view/js/eventPage.js?>"></script>
	<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>
</head>
<body>
	<form method="post" id="eventBanner" name="testform" enctype="multipart/form-data" >
		<table width="100%" class="MainTable eventinfo">
			<thead>
				<tr>
					<th align="left">BANNER MANAGER</th>
					<th><a href="<?= $pageName ?>?action=BannerListing" class="add viewdtl">VieW Banners</a></th>
				</tr>
			</thead>
			<tbody>

				<?php if(isset($imageads)){?>
					<tr>
						<td>Please Follow this instraction</td>
						<td style="color:red"><?php echo $imageads; ?></td>
					</tr>
			    <?php } ?>

				<tr>

					<td class="input-lbl">Select Ads Platform<font color="#FF0000">*</font></td>
					<td>
						<select name="ads_platform" id="ads_platform" style="width:234px;" required>
							<option value="">Select Ads Platform</option>
							<?php foreach($adspltfm as $key=>$val){ ?>
							<option value="<?php echo $key; ?>" <?php if($bannerData["ads_platform"]==$key){ echo 'selected';} ?> ><?php echo $val; ?> </option>
							<?php } ?>
						</select>
						<span class="error" id="ads_platform_error"></span>
						
				</tr>

				<tr>

				<td class="input-lbl">Banner Type<font color="#FF0000">*</font></td>
				<td>
					<select name="type_advertisement" id="type_advertisement" style="width:234px;" required>
						<option value="">Select Banner Type </option>
						<?php foreach($typeOfBanner as $key=>$val){ ?>
						<option value="<?php echo $key ?>" <?php if($bannerData["type_advertisement"]==$key){ echo 'selected';} ?> ><?php echo $val; ?> </option>
						<?php } ?>
					</select>
					<span class="error" id="type_advertisement_error"></span>
					
			</tr>
			
			<tr>
				<td class="input-lbl">Banner Position<font color="#FF0000">*</font></td>
				<td>
					<select name="banner_position" id="banner_position" style="width:234px;" required >
						<option value="">Select Banner Position </option>
					</select>
					<span id="banner_position_size"></span>
					<span class="error" id="banner_position_error"></span>
					
			</tr>
			

				<tr>
					
					<td class="input-lbl">Country <font color="#FF0000">*</font></td>
					<td><?php $countryList = $modelObj->selectAllCountry();
					?>
					<select name="country_id" id="bannerCountry" style="width:234px;" required>
						<option value="0">select Country </option>
						<?php foreach($countryList as $value){ ?>
							<option value="<?php echo $value['id'] ?>"  <?php if($bannerData["country_id"]==$value['id']){ echo 'selected';} ?>><?php echo $value['country']; ?> </option>
						<?php } ?>
					</select>
					<span class="error" id="bannerCountry_error"></span>
				</td>
			</tr>
			<tr>
				<td class="input-lbl" ><div id="locationLabelDiv" name="locationLabelDiv">City</div></td>
				<td>
					<div  name="locationDiv" id="bannerCity">
						<select style="width:234px;" name="bannerCity" id='city_id' >
							<option value="0" >-- Select City--</option>
						</select>
					</div>
					<!-- <span class="error" id="bannerCity_error"></span> -->
				</td>
			</tr>
			<tr>
			</tr>    
			<tr>
				<td class="input-lbl">Banner<font color="#FF0000">*</font></td>
				<td> <input type="file" name="bannerimage" id="bannerads" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif"  <?php if(isset($bannerData['bannerimage'])){ echo ''; }else{ echo 'required';} ?>>
					<input type="hidden" name="bannerimage_old" id="bannerads_old" value="<?php echo $bannerData['bannerimage']; ?>">
                     <span class="error" id="bannerads_error"></span>
                     <span class="error" id="bannerads_old_error"></span>
				</td>
			</tr>
			<tr>
				<td class="input-lbl">Title<font color="#FF0000">*</font></td>
					<td><input type="text" name="title" id="title" required value="<?php echo $bannerData["title"]; ?>">
					<span class="error" id="title_error"></span>
				</td>
			</tr>
			
			<tr>
				<td class="input-lbl">Status<font color="#FF0000">*</font></td>
				<td><input type="radio" name="is_active" value="1" required <?php if ($bannerData["is_active"] == "1"){?> checked <?php } ?>> Active 
					<input type="radio" name="is_active"  value="0" <?php if ($bannerData["is_active"] == "0") { ?> checked <?php } ?>> Inactive 
				</td>
			</tr>

            <!--<tr>
				<td class="input-lbl">CMS for 1st phase of Advertisement <font color="#FF0000">*</font></td>
				<td><input type="checkbox" name="phase_advertisement" required value="1" <?php if($bannerData["phase_advertisement"]==1){ echo 'checked';} ?> ></td>
			</tr> -->
			<tr>
				<td class="input-lbl">Static Advertisement  <font color="#FF0000">*</font></td>
				<td><input type="checkbox" name="static_advertisement" required value="1" <?php if($bannerData["static_advertisement"]==1){ echo 'checked';} ?> >

				</td>
			</tr>
            
			<tr>
				<td class="input-lbl">Start Date<font color="#FF0000">*</font></td>
				<td><input type="text" name="start_date" id="startDate" required value="<?php if(isset($bannerData["start_date"])){ echo date ("m/d/yy",strtotime($bannerData["start_date"]));} ?>">

					<script language="JavaScript">
					new tcal ({
						// form name
						'formname': 'testform',
						// input name
						'controlname': 'start_date'
					});
					</script>
                  <span class="error" id="startDate_error"></span>
				</td>
			</tr>
			<tr>
				<td class="input-lbl">End Date<font color="#FF0000">*</font></td>
				<td><input type="text" name="end_date" id="endDate" required value="<?php if(isset($bannerData["start_date"])){ echo date ("m/d/yy",strtotime($bannerData["end_date"]));}?>">
                <script language="JavaScript">
					new tcal ({
						// form name
						'formname': 'testform',
						// input name
						'controlname': 'end_date'
					});
					</script>
                     <span class="error" id="endDate_error"></span>
				</td>
			</tr>
			<tr>
				<td class="input-lbl">Link Url<font color="#FF0000">*</font></td>
				<td><input type="text" name="link_url" id="linkurl" required value="<?php echo $bannerData["link_url"]?>">
					<span class="error" id="linkurl_error"></span>
				</td>
			</tr>
			<tr>
				<td class="input-lbl">Page Url<font color="#FF0000">*</font></td>
				<td><input type="text" name="page_url" id="pageurl" required value="<?php echo $bannerData["page_url"]?>">
					<span class="error" id="pageurl_error"></span>
				</td>
			</tr>
			<tr>
				<td class="input-lbl">Select page<font color="#FF0000">*</font></td>
				<td><input type="text" name="page" id="pageid" required value="<?php echo $bannerData["page"]?>">
					<span class="error" id="pageid_error"></span>
				</td>
			</tr>

			 
			<tr>
				<td></td>
				<td><input onclick="formsubmit()" type="submit" name="submit">
					<input type="reset" name="reset">
				</td>
			</tr>
		</tbody>
	</table>
</form>

<script type="text/javascript">
  
	function formsubmit(){

		let errorc_typead   = validatecm('type_advertisement','type advertisement is required');
		let errorc_bnrpo       = validatecm('banner_position','Banner position is required');
		let errorc_bannerCountry  = validatecm('bannerCountry','Banner country is required');
		let errorc_bannerads   = validatecm('bannerads','Banner is required');
		let errorc_bannerads_old   = validatecm('bannerads_old','Banner is required');
		let errorc_title     = validatecm('title','Title is required');

		/*let errorc_static_ad = validatecm('staticadvertisement','static advertisement is required');
		let errorc_isactive    = validatecm('isActive','is active is required');*/
		let errorc_startDate = validatecm('startDate','Start Date is required');
		let errorc_endDate     = validatecm('endDate','End Date is required');
		let errorc_link_url = validatecm('linkurl','Link url manager is required');
		let errorc_page_url = validatecm('pageurl','Page url is required');
        let errorc_pageid = validatecm('pageid','Page url is required');
        let errorc_devicetype = validatecm('devicetype','Required field');
        let errorc_ads_platform= validatecm('ads_platform','Required field');


	};

	function validatecm(id,msg){

/*
		let type_advertisement = $('#type_advertisement').val();
		let banner_position = $('#banner_position').val();
		let bannerCountry = $('#bannerCountry').val();
		let city_id = $('#city_id').val();
		let title = $('#title').val();
		let static_advertisement = $("input[name='static_advertisement']:checked").val();
		let is_active = $("input[name='is_active']:checked").val();
		let startDate = $('#startDate').val();
		let endDate = $('#endDate').val();
		let link_url = $('#linkurl').val();
		let page_url = $('#pageurl').val();
		let page_id = $('#pageid').val();

*/
		if($('#'+id).val()=='' || $('#'+id).val()==undefined || $('#'+id).val()==0 || $('#'+id).val()==null || ( ($('#bannerads').val()==null && $('#bannerads_old').val()==null) || ($('#bannerads').val()==undefined && $('#bannerads_old').val()==undefined) || ($('#bannerads').val()=='' && $('#bannerads_old').val()=='') ) ){
			$('#'+id+'_error').html(msg);
			$('#'+id).focus();
			return false;
	    }else{ 
	        $('#'+id+'_error').html('');
	        return true;
	    } 


	}

	$('#banner_position').on('change', function() {
		var mysearchId = this.value;
		var result= '<?php echo $bannerposjson; ?>';
		var res = JSON.parse(result);
		for(var i = 0; i < res.length; i++) {
		    if(res[i].id == mysearchId) {
		      $('#banner_position_size').html('<strong>Uploade ads image size Width:-'+res[i].banner_size_width+'px'+', Height:-'+res[i].banner_size_height+'px</strong>');
		   
		      return;
		    }
		}
	});

	$('#ads_platform').on('change', function() {
		var mysearchId = this.value;
		var result= '<?php echo $bannerposjson; ?>';
		var res = JSON.parse(result);
		var opetionht='';
		for(var i = 0; i < res.length; i++) {
		    if(res[i].device == mysearchId) {
		      opetionht +='<option value="'+res[i].id+'">'+res[i].banner_type+'</opetion>';
		    }
		}

		$('#banner_position').html(opetionht);
	});


</script>
</body>
</html>
