<?php

require_once(ROOT_PATH . "library/mysqli_function.php");
require_once(ROOT_PATH . "banner_manager/commonvariable.php");

// this model for making all the query for event
class Model {

    private $dbObj;

    function __construct() {

        $this->dbObj = new mysqli_function();
    }

 

    public function insertBannerDb() {

       /* print_r($_POST);

        echo "Hello";die();*/

        if (!file_exists('/var/www/html/myzone/media_images/banner_manager')) {
            chmod('/var/www/html/myzone/media_images/banner_manager', 0777);
            mkdir('/var/www/html/myzone/media_images/banner_manager', 0777, true);
        }

        $MainPageBannersSavePath  = '/var/www/html/myzone/media_images/banner_manager';

        $tempName = $_FILES['bannerimage']['tmp_name'];
        $MainPageBannerName = $_FILES['bannerimage']['name'];
        $FinalPath = $MainPageBannersSavePath . '/' . $MainPageBannerName;

        move_uploaded_file($tempName, $MainPageBannersSavePath . '/' . $MainPageBannerName);
        $title = mysql_real_escape_string($_POST['title']);
        $updated_by = mysql_real_escape_string($_SESSION['login']['id']);
        $bannerimage = $_FILES['bannerimage']['name'];
        $is_active = mysql_real_escape_string($_POST['is_active']);
        $start_date = date ("Y-m-d",strtotime($_POST['start_date']));
        $end_date = date ("Y-m-d",strtotime($_POST['end_date']));
        $link_url = mysql_real_escape_string($_POST['link_url']);
        $page_url = mysql_real_escape_string($_POST['page_url']);
        $phase_advertisement = mysql_real_escape_string($_POST['phase_advertisement']);
        $type_advertisement = mysql_real_escape_string($_POST['type_advertisement']);
        $static_advertisement = mysql_real_escape_string($_POST['static_advertisement']);
        $page = mysql_real_escape_string($_POST['page']);
        $banner_position = $_POST['banner_position'];
        $ads_platform= $_POST['ads_platform'];
 
        if ($_POST['country_id'] == 0){
            
            $country_id = NULL;
        } else {
            
             $country_id =$_POST['country_id'];
        }
            
          
            
          if ($_POST['bannerCity'] == 0 || $_POST['bannerCity'] == ""){
              
              $city_id = null;
          } else {
              
            $city_id = $_POST['bannerCity']; 
          }

            $sql = "INSERT INTO tsr_add_manager ("
            . "title,"
            . "country_id,"
            . "city_id,"
            . " updated_by,"
            . " bannerimage,"
            . " is_active,"
            . " start_date,"
            . " end_date,"
            . " page_url,"
            . " link_url,"
            . " phase_advertisement,"
            . " static_advertisement,"
            . " type_advertisement,"
            . " banner_position,"
            . " ads_platform,"
            . " page) "

            . "VALUES ('" . $title . "' "
            . ",'" . $country_id . "'"
            . ",'" . $city_id . "'"
            . ",'" . $updated_by . "',"
            . "'" . $bannerimage . "',"
            . "'" . $is_active . "'"
            . ",'" . $start_date . "',"
            . "'" . $end_date . "',"
            . " '" . $link_url . "', "
            . " '" . $page_url . "' "
            . ",'" . $phase_advertisement . "',"
            . "'" . $static_advertisement . "',"
            . " '" . $type_advertisement . "', "
            . " '" . $banner_position . "', "
            . " '" . $ads_platform . "', "
            . "'" . $page . "')";
         
            $query = $this->dbObj->insert_query($sql);

            return true;
      
    }

    public function selectAllCountry(){
        $sql = 'SELECT id, country  FROM tsr_countries  ';
        $sql .= ' order by id asc ';
        $data = $this->dbObj->getAllData($sql);
        return $data;
    }


    public function getDataaddBanner($bannnerId = '') {
        $sql = 'SELECT * FROM tsr_add_manager ';
        if ($bannnerId != "") {

            $sql .= ' WHERE id ="' . $bannnerId . '"';
        } else {

            $sql .= ' order by id DESC ';
        }

        $data = $this->dbObj->getAllData($sql);
        return $data;
    } 


    public function updateBannerDetails($addBannerId) {
 
        $start_date = date ("Y-m-d",strtotime($_POST['start_date']));
        $end_date = date ("Y-m-d",strtotime($_POST['end_date']));

        if(isset($_POST['bannerimage'])){
            if (!file_exists('/var/www/html/myzone/media_images/banner_manager')) {
            chmod('/var/www/html/myzone/media_images/banner_manager', 0777);
            mkdir('/var/www/html/myzone/media_images/banner_manager', 0777, true);
            }
            $MainPageBannersSavePath  = '/var/www/html/myzone/media_images/banner_manager';
            $tempName = $_FILES['bannerimage']['tmp_name'];
            $MainPageBannerName = $_FILES['bannerimage']['name'];
            $FinalPath = $MainPageBannersSavePath . '/' . $MainPageBannerName;
            move_uploaded_file($tempName, $MainPageBannersSavePath . '/' . $MainPageBannerName);
            $bannerimage = $_FILES['bannerimage']['name'];
        }else{
            $bannerimage = $_POST['bannerimage_old'];
        }

        $sql = "UPDATE tsr_add_manager 
                SET title='" . mysql_real_escape_string($_POST['title']) . "', 
                    country_id='" . mysql_real_escape_string($_POST['country_id']) . "', 
                    updated_by='" . mysql_real_escape_string($_SESSION['login']['id']) . "',
                    bannerimage= '".$bannerimage."',
                    is_active='" . mysql_real_escape_string($_POST['is_active']) . "',
                    start_date='" . mysql_real_escape_string($start_date) . "',
                    end_date='" . mysql_real_escape_string($end_date) . "',
                    page_url='" . mysql_real_escape_string($_POST['page_url']) . "',
                    link_url='" . mysql_real_escape_string($_POST['link_url']) . "',
                    phase_advertisement='" . mysql_real_escape_string($_POST['phase_advertisement']) . "',
                    static_advertisement='" . mysql_real_escape_string($_POST['static_advertisement']) . "',
                    type_advertisement='" . mysql_real_escape_string($_POST['type_advertisement']) . "',
                    banner_position='".$_POST['banner_position']."',
                    page='" . mysql_real_escape_string($_POST['page']) . "'
                WHERE tsr_add_manager.id = '" . mysql_real_escape_string($addBannerId) . "' ";
        $update = $this->dbObj->update_query($sql);
        return $update;
    }
    

    public function getbannersize($banner_type){
        $sqldata = 'SELECT `id`,`banner_type`,`banner_size_width`,`banner_size_height`,`device` FROM tsr_ads_size';
        if ($banner_type != "") {
            $sqldata .= ' WHERE id ="' . $banner_type . '"';
        } else {

            $sqldata .= ' order by id DESC ';
        }

        $getbannersize = $this->dbObj->getAllData($sqldata);
        return $getbannersize;   
    }



}

// end class 
?>
