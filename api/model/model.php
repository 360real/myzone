<?php
	require_once(ROOT_PATH . "library/mysqli_function.php");

	class Model {

	    private $dbObj;
	    function __construct() {
	        $this->dbObj = new mysqli_function();
	    }

	    /*=== Add New user start===*/

	    public function adduser($arr){
			$columns = implode(", ",array_keys($arr));
			$escaped_values = array_map('mysql_real_escape_string', array_values($arr));
			foreach ($escaped_values as $idx=>$data) $escaped_values[$idx] = "'".$data."'";
			$values  = implode(", ", $escaped_values);
			$sql = "INSERT INTO `tsr_users` ($columns) VALUES ($values)";
			$query = $this->dbObj->insert_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}
	    }

	    /*=== check email duplicate start===*/
	    public function checkduplicateuser($email){
			$sql="SELECT `email` FROM `tsr_users` WHERE `email`='".$email."'";
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	    public function checkuservalidation($userid,$password){
			$sql="SELECT `id` FROM `tsr_users` WHERE `id`='".$userid."' AND `pwd`='".$password."'";
			$data = $this->dbObj->getAllData($sql);
			if(!empty($data) || $data !=''){
			  return $data;
			}else{
			  return 0;
			}
	    }

	    

	    public function userLogin($arr){
	    	$sql="SELECT `id`,`token`,`name`,`email`,`profile_image` FROM `tsr_users` WHERE `email`='".$arr['email']."' AND `pwd` = '".$arr['pwd']."'";
			$query2 = $this->dbObj->getAllData($sql);
			if(count($query2)>0){
				$sql = "UPDATE `tsr_users` SET `token`='".$arr['token']."' WHERE id='".$query2[0]['id']."'";
				$query = $this->dbObj->update_query($sql);
			  	return $query2;
			}else{
			  return 0;
			}
	    }

	    public function Insertdata($arr,$tablename){
			$columns = implode(", ",array_keys($arr));
			$escaped_values = array_map('mysql_real_escape_string', array_values($arr));
			foreach ($escaped_values as $idx=>$data) $escaped_values[$idx] = "'".$data."'";
			$values  = implode(", ", $escaped_values);
			$sql = "INSERT INTO ".$tablename." ($columns) VALUES ($values)";
			$query = $this->dbObj->insert_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}
	    }

	    public function Updatedata($arr,$tablename){
	    	foreach($arr as $field=>$data)
			{
			$update[] = $field.' = \''.$data.'\'';
			}
			$sql = "UPDATE ".$tablename." SET ".implode(', ',$update)." WHERE id='".$arr['id']."'";
			$query = $this->dbObj->update_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}
	    }

	    public function Selectdata($tbcolm,$arr,$tablename){ 	
	    	$condetions="";
	    	$condetion="";
	    	if(!empty($arr)){
		    	foreach($arr as $field=>$data)
				{
				$condetion[] = $field.' = \''.$data.'\'';
				}
				$condetions=" WHERE status=1 AND ".implode('AND ',$condetion);	
	    	}else{
	    		$condetions=" WHERE status=1";
	    	}
    		$sql="SELECT ".$tbcolm." FROM ".$tablename." ".$condetions;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }



	}

 ?>
