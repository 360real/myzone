<?php
	require_once(ROOT_PATH . "library/mysqli_function.php");
	
	class Blogmodel {

	    private $dbObj;
	    function __construct() {
	        $this->dbObj = new mysqli_function();
	    }

	    public function Insertdata($arr,$tablename){
			$columns = implode(", ",array_keys($arr));
			$escaped_values = array_map('mysql_real_escape_string', array_values($arr));
			foreach ($escaped_values as $idx=>$data) $escaped_values[$idx] = "'".$data."'";
			$values  = implode(", ", $escaped_values);
			$sql = "INSERT INTO ".$tablename." ($columns) VALUES ($values)";
			$query = $this->dbObj->insert_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}
	    }

	    public function Updatedata($arr,$tablename){
	    	foreach($arr as $field=>$data)
			{
			$update[] = $field.' = \''.$data.'\'';
			}
			$sql = "UPDATE ".$tablename." SET ".implode(', ',$update)." WHERE id='".$arr['id']."'";
			$query = $this->dbObj->update_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}
	    }

	    public function Selectdata($tbcolm,$arr,$tablename){ 
	    	$condetions="";
	    	$condetion="";
	    	if(!empty($arr)){
		    	foreach($arr as $field=>$data)
				{
				$condetion[] = $field.' = \''.$data.'\'';
				}
				$condetions=" WHERE status=1 AND ".implode('AND ',$condetion);	
	    	}else{
	    		$condetions=" WHERE status=1";
	    	}
    		$sql="SELECT ".$tbcolm." FROM ".$tablename." ".$condetions;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	}

 ?>
