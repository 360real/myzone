<?php
error_reporting(E_ALL);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: Authorization, Content-Type, x-xsrf-token, x_csrftoken, Cache-Control, X-Requested-With');
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "api/model/model.php");
$userdata = json_decode(file_get_contents("php://input"));
$modelObj = new Model();
$action		=	remRegFx($_REQUEST['action']);
switch($action){
	case 'register':
		if($userdata->name==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the name'
			);
			echo json_encode($data);
			die;
		}
		if($userdata->email==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the email'
			);
			echo json_encode($data);
			die;
		}
		if($userdata->password==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the password'
			);
			echo json_encode($data);
			die;
		}

		

		$echeckemail = $modelObj->checkduplicateuser($userdata->email);
		if($echeckemail[0]['email']!=''){
			$data = array(
				'status' => 1,
				'msg'=>'Email already exists'
			);
			echo json_encode($data);
			die;
		}

		if (!file_exists('/var/www/html/myzone/media_images/userprofile')) {
			chmod('/var/www/html/myzone/media_images/userprofile', 0777);
			mkdir('/var/www/html/myzone/media_images/userprofile', 0777, true);
		}

		$imagepath  = '/var/www/html/myzone/media_images/userprofile';
		$image_parts = explode(";base64,", $userdata->file);

		$img = explode(',', $userdata->file);
		$ini =substr($img[0], 11);
		$type = explode(';', $ini);
		$image_type_aux = explode("image/", $image_parts[0]); 
		$image_type = $image_type_aux[1];
		$image_base64 = base64_decode($image_parts[1]);
		$filename = uniqid().'.'.$type[0];
		$file = $imagepath.'/'.$filename;
		file_put_contents($file, $image_base64);

		$arr['name']=$userdata->name;
		$arr['email']=$userdata->email;
		$arr['pwd']=sha1("360_?@Re@lTorS#".$userdata->password."((!!^&");
		$arr['role_id']=1;
		$arr['created_date'] =date("Y-m-d H:i:s");
		$arr['status']=1;
		$arr['login_id']=$userdata->name;
		$arr['password_backup'] = base64_encode($userdata->password);
		$arr['profile_image']=$filename;

		$insertdata = $modelObj->adduser($arr);
		if($insertdata>0){
			$data = array(
				'status' => 0,
				'msg'=>'data inserted sucessfully'
			);
			echo json_encode($data);
			die;
		}else{
			$data = array(
				'status' => 1,
				'msg'=>'data not inserted sucessfully'
			);
			echo json_encode($data);
			die;
		}
		break;	

	case 'login':

		if($userdata->email==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the email'
			);
			echo json_encode($data);
			die;
		}
		if($userdata->password==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the password'
			);
			echo json_encode($data);
			die;
		}

		$arr['email']=$userdata->email;
		$arr['pwd']=sha1("360_?@Re@lTorS#".$userdata->password."((!!^&");
		$arr['token']=substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,30);

		$userlogin = $modelObj->userLogin($arr);

		if($userlogin!=0){
			$data = array(
				'status' => 0,
				'msg'=>'Login Successfully',
				'data'=>$userlogin,
			);
			echo json_encode($data);
			die;
		}else{
			$data = array(
				'status' => 1,
				'msg'=>'Login Failed'
			);
			echo json_encode($data);
			die;
		}
		break;

	case 'addmicrosite':
		if($userdata->city_id==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please select the city'
			);
			echo json_encode($data);
			die;
		}

		if($userdata->location_id==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please select the locaion'
			);
			echo json_encode($data);
			die;
		}

		if($userdata->property_id==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please select the property'
			);
			echo json_encode($data);
			die;
		}

		if($userdata->view_id==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please select the View type'
			);
			echo json_encode($data);
			die; 
			$data = array(
				'status' => 1,
				'msg'=>'Please select the domain'
			);
			echo json_encode($data);
			die;
		}

		if($userdata->property_name==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the property display name'
			);
			echo json_encode($data);
			die;
		}

		if($userdata->overview_title==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the overview title'
			);
			echo json_encode($data);
			die;
		}

		if($userdata->overview_description==''){
			$data = array(
				'status' => 1,
				'msg'=>'Please enter the overview discriptions'
			);
			echo json_encode($data);
			die;
		}


		if (!file_exists('/var/www/html/myzone/media_images/newmicrosite')) {
			chmod('/var/www/html/myzone/media_images/newmicrosite', 0777);
			mkdir('/var/www/html/myzone/media_images/newmicrosite', 0777, true);
		}
		$imagepath  = '/var/www/html/myzone/media_images/newmicrosite';

		if($userdata->overview_image!=''){
			$overview_parts = explode(";base64,", $userdata->overview_image);
			$overview_img = explode(',', $userdata->overview_image);
			$overview_ini =substr($overview_img[0], 11);
			$overview_type = explode(';', $overview_ini);
			$overviewimage_type_aux = explode("image/", $overview_parts[0]); 
			$overviewimage_type = $overviewimage_type_aux[1];
			$overviewimage_base64 = base64_decode($overview_parts[1]);
			$overviewfilename = uniqid().'.'.$type[0];
			$overviewfile = $imagepath.'/'.$overviewfilename;
			file_put_contents($overviewfile , $overviewimage_base64);
		}

		if($userdata->highlights_image!=''){
			$highlights_parts = explode(";base64,", $userdata->highlights_image);
			$highlights_img = explode(',', $userdata->highlights_image);
			$highlights_ini =substr($highlights_img[0], 11);
			$highlights_type = explode(';', $highlights_ini);
			$highlightsimage_type_aux = explode("image/", $highlights_parts[0]); 
			$highlightsimage_type = $highlightsimage_type_aux[1];
			$highlightsimage_base64 = base64_decode($highlights_parts[1]);
			$highlightsfilename = uniqid().'.'.$type[0];
			$highlightsfile = $imagepath.'/'.$highlightsfilename;
			file_put_contents($highlightsfile , $highlightsimage_base64);
		}

		if($propertylogo_image!=''){
			$propertylogo_parts = explode(";base64,", $userdata->propertylogo_image);
			$propertylogo_img = explode(',', $userdata->propertylogo_image);
			$propertylogo_ini =substr($propertylogo_img[0], 11);
			$propertylogo_type = explode(';', $propertylogo_ini);
			$propertylogoimage_type_aux = explode("image/", $propertylogo_parts[0]); 
			$propertylogoimage_type = $propertylogoimage_type_aux[1];
			$propertylogoimage_base64 = base64_decode($propertylogo_parts[1]);
			$propertylogofilename = uniqid().'.'.$type[0];
			$propertylogofile = $imagepath.'/'.$propertylogofilename;
			file_put_contents($propertylogofile , $propertylogoimage_base64);
		}

		if($userdata->location_image!=''){
			$location_parts = explode(";base64,", $userdata->location_image);
			$location_img = explode(',', $userdata->location_image);
			$location_ini =substr($location_img[0], 11);
			$location_type = explode(';', $location_ini);
			$locationimage_type_aux = explode("image/", $location_parts[0]); 
			$locationimage_type = $locationimage_type_aux[1];
			$locationimage_base64 = base64_decode($location_parts[1]);
			$locationfilename = uniqid().'.'.$type[0];
			$locationfile = $imagepath.'/'.$locationfilename;
			file_put_contents($locationfile , $locationimage_base64);
		}

		if($userdata->siteplane_image!=''){
			$siteplane_parts = explode(";base64,", $userdata->siteplane_image);
			$siteplane_img = explode(',', $userdata->siteplane_image);
			$siteplane_ini =substr($siteplane_img[0], 11);
			$siteplane_type = explode(';', $siteplane_ini);
			$siteplaneimage_type_aux = explode("image/", $siteplane_parts[0]); 
			$siteplaneimage_type = $siteplaneimage_type_aux[1];
			$siteplaneimage_base64 = base64_decode($siteplane_parts[1]);
			$siteplanefilename = uniqid().'.'.$type[0];
			$siteplanefile = $imagepath.'/'.$siteplanefilename;
			file_put_contents($siteplanefile , $siteplaneimage_base64);
		}
		
		/*===Posttext array===*/
		$arr['post_id']=$userdata->post_id;
		$arr['cty_id']=$userdata->city_id;
		$arr['loc_id']=$userdata->location_id;
		$arr['propty_id']=$userdata->property_id;
		$arr['view_id']=$userdata->view_id;
		$arr['domain_name']=4;
		$arr['display_property_name']=$userdata->property_name;
		$arr['overview_title']=$userdata->overview_title;
		$arr['overview']=$userdata->overview_description;
		$arr['highlights']=$userdata->highlight_description;
		//$arr['new_amenities']=$userdata->new_amenities;
		$arr['google_analytics_code']=$userdata->google_analytics;
		$arr['google_conversion_code']=$userdata->google_conversion;
		$arr['google_verification_code']=$userdata->google_verification;
		$arr['google_remarking_code']=$userdata->google_remarking;
		//$arr['slider_nwes']=$userdata->slider_nwes;
		//$arr['display_email']=$userdata->display_email;
		//$arr['mobile_number']=$userdata->mobile_number;
		//$arr['office_address']=$userdata->office_address;
		$arr['meta_title']=$userdata->meta_title;
		$arr['meta_keyword']=$userdata->meta_keyword;
		$arr['meta_description']=$userdata->meta_description;
		$arr['location_content']=$userdata->location_description;
		$arr['site_plane']=$userdata->site_description;

		/*===Filesupload array===*/
		$arr['overview_image']=$overviewfilename;
		$arr['highlights_image']=$highlightsfilename;
		$arr['property_logo']=$propertylogofilename;
		$arr['location_image']=$locationfilename;
		$arr['site_plane_image']=$siteplanefilename;

		print_r($arr);die;

        if($userdata->post_id==''){
        	$Insertdata = $modelObj->Insertdata($arr,'tsr_newmicrosite');
        }else{
        	$Updatedata = $modelObj->Updatedata($arr,'tsr_newmicrosite');
        }

    	$data = array(
			'status' => 0,
			'msg'=>'data inserted sucessfully'
		);
		echo json_encode($data);
		die;
	break;
   	case 'getcitylist':	
		$selectdata = $modelObj->Selectdata('id,city','','tsr_cities');
		echo json_encode($selectdata);
		die;
	break;
   	case 'getlocationlist':
   		$arr['cty_id']=$userdata->cityid;
   		$selectdata = $modelObj->Selectdata('id,location',$arr,'tsr_locations');
		echo json_encode($selectdata);
		die;
	break;
   	case 'getpropertylist':
   		$arr['loc_id']=$userdata->loc_id;
   		$selectdata = $modelObj->Selectdata('id,property_name,property_name_display',$arr,'tsr_properties');
		echo json_encode($selectdata);
		die;
	break;   

	default:
		echo 'invalid url';
		break;
}		

?>