<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_XLR8_MEDIA_SECTION;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/xlr8/media_section/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";



	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."xlr8/media_section/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."xlr8/media_section/view/view.php" ;
			break;
	}

	$sql="SELECT * FROM $tblName ";	
	$sql.=($rec_id ? " WHERE $fld_id='$rec_id'" :' WHERE 1');
	
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	
	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=50;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));

		if($_GET["action"]=="Delete") {
	
			$upd['is_deleted'] = "1";
			$upd['updated_date'] = "now()";
			$upd['ipaddress'] = $regn_ip;

			$obj_mysql->update_data($tblName,$fld_id,$upd,$_GET["id"]);
			$obj_common->redirect($page_name."?msg=delete");
		}




	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['updated_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			if($obj_mysql->isDupUpdate($tblName,'title', $arr['name'] ,'id',$rec_id)){

				$fileBasePath ="/var/www/html/myzone/media_images/";
				// check and create folder if not exists - start code here
				$xlr8 = $fileBasePath."xlr8";
				if (!is_dir($xlr8)) 
				{
					mkdir($xlr8);
					chmod($xlr8, 0777);
				}
				$leadership = $xlr8 .'/'.'media_section';
				if (!is_dir($leadership)) {
				    mkdir($leadership);
				    chmod($leadership, 0777);
				}

				$pathFileType = $leadership .'/'.$rec_id;
				if (!is_dir($pathFileType)) {
				    mkdir($pathFileType);
				    chmod($pathFileType, 0777);
				}
				// check and create folder if not exists - end code here

				// image upload and update in table - start end here
	            if ($_FILES['image_path']['name'] != "") {
	                $tmpname = $_FILES['image_path']['tmp_name'];
	                $fileName = $_FILES['image_path']['name'];

	                @unlink($pathFileType. "/" . $_POST['old_file_name']);
	                if(move_uploaded_file($tmpname, $pathFileType . "/" . $fileName))
	                {
	                	$arr['image_path'] = $fileName;
	                }
				}

				// image upload and update in table - code end here
				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);

				$obj_common->redirect($page_name."?msg=update");	
			}else{
				$msg='unique';
			}	
		}else{
			$arr['created_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			if($obj_mysql->isDuplicate($tblName,'title', $arr['name'])){
			
				$setmaxid = $obj_mysql->insert_data($tblName, $arr);

		        $fileBasePath ="/var/www/html/myzone/media_images/";
				// check and create folder if not exists - start code here
				$xlr8 = $fileBasePath."xlr8";
				if (!is_dir($xlr8)) 
				{
					mkdir($xlr8);
					chmod($xlr8, 0777);
				}
				$leadership = $xlr8 .'/'.'media_section';
				if (!is_dir($leadership)) {
				    mkdir($leadership);
				    chmod($leadership, 0777);
				}

				$pathFileType = $leadership .'/'.$setmaxid;
				if (!is_dir($pathFileType)) {
				    mkdir($pathFileType);
				    chmod($pathFileType, 0777);
				}
				// check and create folder if not exists - end code here


				// image upload and update in table - start end here
	            if ($_FILES['image_path']['name'] != "") {
	                $tmpname = $_FILES['image_path']['tmp_name'];
	                $fileName = $_FILES['image_path']['name'];

	                @unlink($pathFileType. "/" . $_POST['old_file_name']);
	                if(move_uploaded_file($tmpname, $pathFileType . "/" . $fileName))
	                {
	                	$arr['image_path'] = $fileName;
	                	$obj_mysql->update_data($tblName, $fld_id, $arr, $setmaxid);
	                }
				}

				// image upload and update in table - code end here


				$obj_common->redirect($page_name."?msg=insert");	
			}else{
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['is_deleted']=="0"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="title_a">'.$rec[$i]['title'].'</a></td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['updated_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td class="center">'.$st.'</td>
					<td >
					<a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>
						<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>
					</td> 
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Media Section', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
