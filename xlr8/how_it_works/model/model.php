<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>How It Works Detail</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1) ?>">
    <td align="right"><strong><font color="#FF0000">*</font>Category</strong></td>
    <td><select name="category" id="category" title="Category"  class="drop_down" style="width:234px;" lang="MUST" required="">
          <option value="">-- Select --</option>
                <option value="Discover" <?php if ($rec['category'] == "Discover") { ?> selected <?php } ?>> Discover</option>
                <option value="Insight" <?php if ($rec['category'] == "Insight") { ?> selected <?php } ?>> Insight</option>
                <option value="Perks" <?php if ($rec['category'] == "Perks") { ?> selected <?php } ?>> Perks</option>
                <option value="Support" <?php if ($rec['category'] == "Support") { ?> selected <?php } ?>> Support</option>
        </select>
    </td>
</tr>
<tr class="<?php echo($clsRow2)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Title</strong></td>
  <td><input name="title" type="text" title="Title" lang='MUST' value="<?php echo($rec['title'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
    <td align="right" ><strong>Select Icon</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['icon_path']=='') {?> lang='MUST' title="Select Icon" <?php } ?> name="icon_path" id="icon_path" /></td>
          <td width="62%"><?php if($rec['icon_path']):?>
            <br />
            <img src="<?php echo  ($site['XLR8_WEBSITE']."how_it_works/".$rec['id']."/".$rec['icon_path'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['icon_path'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

<tr class="<?php echo($clsRow2)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Description</strong></td>
   <td><textarea class="ckeditor" name="description"  id="description" rows="4" cols="70" ><?php echo $rec['description'] ?> </textarea></td>
</tr>



<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="is_deleted" value="0" id="0" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="is_deleted" value="1" id="1" <?php if ($rec['is_deleted']=="1"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>


<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>