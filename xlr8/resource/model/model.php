<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Resource Detail</h3></th></tr>
</thead>
<tbody>

<?php
$s1 = "SELECT id,name FROM " . TABLE_XLR8_RESOURCE_CATEGORY . " where is_deleted=0 order by name";
$r1 = mysql_query($s1);
$rowCount = mysql_num_rows($r1);
?>
<tr class="<?php echo($clsRow1) ?>">
    <td align="right"><strong><font color="#FF0000">*</font>Category</strong></td>
    <td><select name="category" id="category" title="Category"  class="drop_down" style="width:234px;" lang="MUST" required="">
    			<option value="">-- Select --</option>
            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                <option value="<?= $recResult['name'] ?>" <?php if ($rec['category'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
            <?php } ?>
        </select>

    </td>
</tr>
<tr class="<?php echo($clsRow2)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Title</strong></td>
  <td><input name="title" type="text" title="Title" lang='MUST' value="<?php echo($rec['title'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Topic</strong></td>
  <td><input name="topic" type="text" title="Topic" lang='MUST' value="<?php echo($rec['topic'])?>" size="50%" maxlength="120" /></td>
</tr>



<tr class="<?php echo($clsRow2)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Published Date</strong></td>
  <td>


  <input type="text" name="published_date" value="<?php echo($rec['published_date'])?>"/>
                        <script language="JavaScript">
                      new tcal ({
                        'formname': 'form123',
                        'controlname': 'published_date'
                      });</script>
  </td>
</tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Publish By</strong></td>
  <td><input name="publish_by" type="text" title="Publish By" lang='MUST' value="<?php echo($rec['publish_by'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow2)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Author Post</strong></td>
  <td><input name="author_post" type="text" title="Author Post" lang='MUST' value="<?php echo($rec['author_post'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?> Images">
    <td align="right" ><strong>Author Image</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" name="author_image_path" id="author_image_path" /></td>
          <td width="62%"><?php if($rec['author_image_path']):?>
            <br />  
            <img src="<?php echo  ($site['XLR8_WEBSITE']."resource/".$rec['id']."/".$rec['author_image_path'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="author_old_file_name" value="<?php echo ($rec['author_image_path'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

<tr class="<?php echo($clsRow2)?> Videos">
  <td align="right"><strong>Youtube Video ID</strong></td>
  <td><input name="video_id" type="text" title="Youtube Video ID" value="<?php echo($rec['video_id'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow2)?> Images">
    <td align="right" ><strong>Select Image</strong></td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" name="image_path" id="image_path" /></td>
          <td width="62%"><?php if($rec['image_path']):?>
            <br />  
            <img src="<?php echo  ($site['XLR8_WEBSITE']."resource/".$rec['id']."/".$rec['image_path'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['image_path'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Description</strong></td>
   <td><textarea class="ckeditor" name="description"  id="description" rows="4" cols="70" ><?php echo $rec['description'] ?> </textarea></td>
</tr>


<tr class="<?php echo($clsRow2)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="is_deleted" value="0" id="0" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="is_deleted" value="1" id="1" <?php if ($rec['is_deleted']=="1"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>


<tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>

<script type="text/javascript">
  $("#category").on("click",function(){
      var value = $(this).val();
      if(value=="Videos")
      {
        $(".Videos").show();
        $(".Images").hide();
      }
      else
      {
        $(".Videos").hide();
        $(".Images").show();

      }
  });
</script>