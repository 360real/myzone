<?php
include $_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php";//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");

include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_MICROSITES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/microsites/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	
	$templateType= $_GET['type'];
	$regn_ip = $_SERVER['REMOTE_ADDR'];
	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."microsites/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."microsites/view/view.php" ;
			break;
	}

	for ($i=1; $i<6; $i++) {
		
		$fileSize = $_FILES["amenity$i"]['size'];
		$fileName = $_FILES["amenity$i"]['name'];
		$tmpname = $_FILES["amenity$i"]['tmp_name'] ;
		$amenityId = $_POST["amenities$i"];
		$propertyId = $_GET['propty_id'];
		$micrositeId = $_GET['id'];
		
		$fileName = str_replace(' ', '',$fileName);
		
		if ($fileSize > 0) {

		$arr['image_name'] = $fileName;
		$arr['amenity_id'] = $amenityId;
		$arr['amenity_id'] = $amenityId;
		$arr['property_id'] = $propertyId;
		$arr['microsite_id'] = $micrositeId;
		
		$queCheck = "select id from tsr_project_wise_amenities where property_id = $propertyId and amenity_id = $amenityId";
		$sqlChk = $obj_mysql->get_num_rows($queCheck);

		$amnAction = ($sqlChk>0)?'Update':'Create';

		if ($amnAction == 'Create') {
			$response = $obj_mysql->insert_data('tsr_project_wise_amenities',$arr);
			
		} elseif ($amnAction=='Update') {
			$query = "update tsr_project_wise_amenities set image_name = '$fileName' where property_id = $propertyId and amenity_id = $amenityId";
			$res = $obj_mysql->exec_query($query, '');	
			
		}
		$path1 = MEDIA_ROOT_PATH . 'media_images/project_wise_amenities/';
		$path2 = $path1. $propertyId.'/';
		$finalPath = $path2 . $fileName;
		if (!is_dir($path1)) {
			mkdir($path1, 0777);
		}
		if (!is_dir($path2)){
			mkdir($path2, 0777);
		}
		move_uploaded_file($tmpname, $path2.$fileName);		
		}
	}

	
	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT datediff( date( ms.expire_date ) , NOW( ) ) AS domainExpiryRemainingDays,ms.video_embed_code, ms.is_redirect, ms.redirect_url, ms.redirect_banner,ms.blog_header_banner,ms.developer_image,ms.blog_banner_url, ms.redirect_title,ms.redirect_popup_url, ms.popup_logo, ms.text_developer_popup_logo, ms.template_theme_color,ms.google_conversion_code,ms.offer_content,ms.footer_content,ms.google_verification_code, ms.sitemap_url,ms.xml_file, ms.google_remarking_code, ms.property_logo,ms.property_feature_img,ms.property_ad_banner,ms.google_analytics_code,ms.is_crawlable,ms.display_email,ms.property_name_to_display,ms.display_mobile1,ms.display_mobile2,ms.address,ms.blog_meta_title,ms.blog_meta_keywords,ms.blog_meta_description,
ms.template_type, ms.id, ms.cty_id, ms.loc_id, ms.propty_id, ms.template_id,ms.is_builderObjection,ms.is_reputed, ms.site_name, ms.domain_name, ms.overview, ms.highlight_text1, ms.highlight_text2, ms.microsite_timer ,ms.highlight_text3, ms.highlight_text4 , ms.meta_title, ms.meta_keywords, ms.meta_description, ms.developer_meta_title, ms.developer_meta_keywords, ms.developer_meta_description,ms.developer_project_meta_title, ms.developer_project_meta_keywords, ms.developer_project_meta_description, ms.developer_page_overview ,ms.status, ms.modified_date, ms.created_date, ms.expire_date, ms.ipaddress, ms.blog_footer_text,ms.media_server, ms.view360_url, ms.view360_url_status,ms.similar_project_title_1,ms.similar_project_title_2, ms.similar_project_title_3,ms.similar_project_url_1,ms.similar_project_url_2, ms.similar_project_url_3, ms.city_overview, ms.overview_title, ms.city_overview_title, ms.who_we_are, ms.who_we_are_status, ms.is_city_based, ms.multiple_project_title, ms.blog_heading, ms.sef_location_name FROM ".TABLE_MICROSITES." ms WHERE ";
		if($_GET["microsite_id"]!="")
			$sql.="ms.id=".$_GET["microsite_id"];
		if($_GET["site_name"]!="")
			$sql.=" ms.site_name like '%".trim($_GET["site_name"])."%'";
		if($_GET["status"]!="")
			$sql.="ms.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(ms.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(ms.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(ms.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(ms.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT   datediff( date( ms.expire_date ) , NOW( ) ) AS domainExpiryRemainingDays,ms.video_embed_code, ms.is_redirect, ms.redirect_url, ms.redirect_banner,ms.blog_header_banner ,ms.developer_image, ms.blog_banner_url, ms.redirect_title, ms.redirect_popup_url,  ms.popup_logo, ms.text_developer_popup_logo, ms.template_theme_color,ms.google_conversion_code,ms.offer_content,ms.footer_content,ms.google_verification_code, ms.sitemap_url, ms.xml_file, ms.google_remarking_code, ms.property_logo,ms.property_feature_img,ms.property_ad_banner,ms.google_analytics_code,ms.is_crawlable,ms.display_email,ms.property_name_to_display,ms.display_mobile1,ms.display_mobile2,ms.address,ms.blog_meta_title,ms.blog_meta_keywords,ms.blog_meta_description, ms.microsite_timer,
ms.template_type, ms.id, ms.has_nri_subdomain,ms.nri_template_id,ms.has_domestic_subdomain, ms.domestic_template_id,ms.cty_id, ms.loc_id, ms.propty_id, ms.template_id, ms.is_builderObjection,ms.is_reputed, ms.site_name, ms.domain_name, ms.overview, ms.highlight_text1, ms.highlight_text2, ms.highlight_text3,ms.highlight_text4, ms.meta_title, ms.meta_keywords, ms.meta_description, ms.developer_meta_title, ms.developer_meta_keywords, ms.developer_meta_description,ms.developer_project_meta_title, ms.developer_project_meta_keywords, ms.developer_project_meta_description, ms.developer_page_overview , ms.status, ms.modified_date, ms.created_date, ms.expire_date, ms.ipaddress, ms.media_server, ms.blog_footer_text, ms.view360_url, ms.view360_url_status,ms.similar_project_title_1,ms.similar_project_title_2, ms.similar_project_title_3,ms.similar_project_url_1,ms.similar_project_url_2, ms.similar_project_url_3, ms.city_overview, ms.overview_title, ms.city_overview_title, ms.who_we_are, ms.who_we_are_status, ms.is_city_based, ms.multiple_project_title, ms.blog_heading, ms.sef_location_name FROM ".TABLE_MICROSITES." ms";
		
		$sql.= " WHERE ms.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
		$sql = "SELECT  datediff( date( ms.expire_date ) , NOW( ) ) AS domainExpiryRemainingDays,ms.video_embed_code, ms.is_redirect, ms.redirect_url, ms.redirect_banner, ms.blog_header_banner,ms.developer_image, ms.blog_banner_url, ms.redirect_title, ms.redirect_popup_url, ms.popup_logo, ms.text_developer_popup_logo, ms.template_theme_color,ms.google_conversion_code,ms.offer_content,ms.footer_content,ms.google_verification_code, ms.sitemap_url, ms.xml_file, ms.google_remarking_code, ms.property_logo,ms.property_feature_img,ms.property_ad_banner,ms.google_analytics_code,ms.is_crawlable,ms.display_email,ms.property_name_to_display,ms.display_mobile1,ms.display_mobile2,ms.address,ms.blog_meta_title,ms.blog_meta_keywords,ms.blog_meta_description, ms.microsite_timer,
ms.template_type, ms.id, ms.cty_id, ms.loc_id, ms.propty_id, ms.template_id, ms.is_builderObjection,ms.is_reputed,ms.site_name, ms.domain_name, ms.overview, ms.highlight_text1, ms.highlight_text2, ms.highlight_text3,ms.highlight_text4 ,ms.meta_title, ms.meta_keywords, ms.meta_description, ms.developer_meta_title, ms.developer_meta_keywords, ms.developer_meta_description,ms.developer_project_meta_title, ms.developer_project_meta_keywords, ms.developer_project_meta_description, ms.developer_page_overview , ms.status, ms.modified_date, ms.created_date, ms.expire_date, ms.ipaddress, ms.blog_footer_text, ms.media_server, ms.view360_url, ms.view360_url_status,ms.similar_project_title_1,ms.similar_project_title_2, ms.similar_project_title_3,ms.similar_project_url_1,ms.similar_project_url_2, ms.similar_project_url_3, ms.city_overview, ms.overview_title, ms.city_overview_title, ms.who_we_are, ms.who_we_are_status, ms.is_city_based, ms.multiple_project_title, ms.blog_heading, ms.sef_location_name FROM ".TABLE_MICROSITES." ms";

if($templateType=="multiple")
{
	$sql.= " where ms.template_id IN (25,35,36,37)";
}
		
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/

	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";



			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];
			if($obj_mysql->isDupUpdate($tblName,'site_name', $arr['site_name'] ,'id',$rec_id)){

			$arr["expire_date"] = date('Y-m-d', strtotime($_POST["expire_date"]));

		/*======================= START:  Popup Logo ========================*/
			if($_FILES['popup_logo']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/popup_logo/".$_POST['old_file_name_popup']);
				$handle = new upload($_FILES['popup_logo']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/popup_logo/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['popup_logo'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Popup Logo ========================*/



		/*======================= START:  Redirect Banner ========================*/
			if($_FILES['redirect_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/redirect_banner/".$_POST['old_file_name_redirect_banner']);
				$handle = new upload($_FILES['redirect_banner']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/redirect_banner/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['redirect_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Redirect Banner ========================*/

		 /*======================= START:  Redirect Banner ========================*/
			if($_FILES['blog_header_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/blog_header_banner/".$_POST['old_file_name_redirect_banner']);
				$handle = new upload($_FILES['blog_header_banner']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/blog_header_banner/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['blog_header_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Redirect Banner ========================*/

		 /*======================= START:  Redirect Banner ========================*/
			if($_FILES['developer_image']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/developer_image/".$_POST['old_file_name_redirect_banner']);
				$handle = new upload($_FILES['developer_image']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/developer_image/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['developer_image'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Redirect Banner ========================*/


		 /*======================= START:  Text Popup Developer Logo ========================*/
			if($_FILES['text_developer_popup_logo']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/text_developer_popup_logo/".$_POST['old_file_name_popup']);
				$handle = new upload($_FILES['text_developer_popup_logo']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/text_developer_popup_logo/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['text_developer_popup_logo'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Text Popup Developer Logo ========================*/


          /*======================= START:  Property Logo ========================*/
			if($_FILES['property_logo']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/property_logo/".$_POST['old_file_name']);
				$handle = new upload($_FILES['property_logo']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/property_logo/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['property_logo'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Property Logo ========================*/
		           /*======================= START:  Property Feature image ========================*/
							 if($_FILES['property_feature_img']['name']!="")
							 {   
								 @unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/property_feature_img/".$_POST['old_file_name_feature']);
								 $handle = new upload($_FILES['property_feature_img']);
								 if ($handle->uploaded)
								 {	
									 /*$handle->image_resize         = true;
									 $handle->image_x              = 200;
									 $handle->image_y              = 87;
									 $handle->image_ratio_y        = true;*/
									 $handle->file_safe_name = true;
									 $handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/property_feature_img/");
									 
									 if ($handle->processed) {
										 //echo 'image resized';
										 $arr['property_feature_img'] = $handle->file_dst_name;
										 $handle->clean();
										} else {
											//echo "error";
										 echo 'error : ' . $handle->error;
									 }
								 }
									
							 }
				 
							/*======================= END: Property Feature image ========================*/

	    /*======================= START:  Property Ad banner image ========================*/
			if($_FILES['property_ad_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/property_ad_banner/".$_POST['old_file_name_feature']);
				$handle = new upload($_FILES['property_ad_banner']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/property_ad_banner/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['property_ad_banner'] = $handle->file_dst_name;
						$handle->clean();
					 } else {
						 //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Property ad banner image ========================*/


		
/*======================= START: XML Verfication Image ========================*/
			if($_FILES['sitemap_url']['name']!="")
			{  
				@unlink(UPLOAD_PATH_MICROSITE_VERIFICATION_PATH."sitemap/pm/".$rec_id."/".$_POST['old_file_name_xml']);
				$handle = new upload($_FILES['sitemap_url']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 16;
					$handle->image_y              = 16;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_VERIFICATION_PATH."sitemap/pm/".$rec_id."/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['sitemap_url'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: XML Verfication Image ========================*/

		 /*======================= START: XML FILE ========================*/
			if($_FILES['xml_file']['name']!="")
			{  
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE."sitemap/".$rec_id."/".$_POST['old_file_name_xml']);
				$handle = new upload($_FILES['xml_file']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 16;
					$handle->image_y              = 16;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE."sitemap/".$rec_id."/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['xml_file'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: XML FILE ========================*/



				
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */
				for($counterImages=0;$counterImages<3;$counterImages++){
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{ 
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/".$_POST['old_file_name_propertyImage'.$counterImages]);	
				$arrayPropertyImage["img_type"] = "MICROSITE_TEMPLATE_IMAGE"; 
				$arrayPropertyImage["microsite_template_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				$handle = new upload($_FILES["propertyImage".$counterImages]);
				if ($handle->uploaded)
				{
					// $handle->image_resize         = true;
					// $handle->image_x              = 1300;
					// $handle->image_y              = 1637;
					// $handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_propertyImage'.$counterImages];
				}
				}

				if($_FILES["propertyImage".$counterImages]["name"]!=""){
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				}
				else{
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}
				}
				
				/* CODE FOR MICROSITE -NRI- IMAGES UPDATE BLOCK */
				for($counterImages=0;$counterImages<3;$counterImages++){
					if($_FILES["propertyNriImage".$counterImages]["name"]!="")
					{ 
					@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/".$_POST['old_file_name_propertyNriImage'.$counterImages]);	
					$arrayPropertyImageNri["img_type"] = "MICROSITE_NRI_TEMPLATE_IMAGE"; 
					$arrayPropertyImageNri["microsite_template_id"] = $rec_id; 
					$arrayPropertyImageNri["imageIdNri"]=$_POST["imageIdNri"][$counterImages];
					if($_FILES["propertyNriImage".$counterImages]["name"]!="")
					{
					$handle = new upload($_FILES["propertyNriImage".$counterImages]);
					if ($handle->uploaded)
					{
						// $handle->image_resize         = true;
						// $handle->image_x              = 1300;
						// $handle->image_y              = 1637;
						// $handle->image_ratio_y        = true;
						$handle->file_safe_name = true;
			
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/");
					if ($handle->processed) {
					//echo 'image resized';
					$arrayPropertyImageNri["image"] = $handle->file_dst_name;
					
					$handle->clean();
					} else {
					echo 'error : ' . $handle->error;
					}
					}
	
					}
					else
					{
					$arrayPropertyImageNri["image"]=$_POST['old_file_name_propertyNriImage'.$counterImages];
					}
					}
	
					if($_FILES["propertyNriImage".$counterImages]["name"]!=""){
					$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImageNri);
					}
					else{
					$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImageNri,$arrayPropertyImageNri["imageIdNri"]);
					}
					}
			/*======================= START: Proprty -NRI- Logo ========================*/
			if($_FILES['property_nri_logo']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/property_logo/".$_POST['old_file_name']);
				$handle = new upload($_FILES['property_nri_logo']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/property_logo/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['property_nri_logo'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Proprty -NRI- Logo  ========================*/

				for($i=0;$i<3;$i++){
				if($_FILES["image".$i]["name"]!="")
				{
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/blog_hot_project/".$_POST['old_file_name'.$i]);	
				$arrayPropertyImage["img_type"] = "MICROSITE_TEMPLATE_BLOG_BANNER_IMAGE"; 
				$arrayPropertyImage["microsite_template_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$i];
				if($_FILES["image".$i]["name"]!="")
				{
				$handle = new upload($_FILES["image".$i]);
				if ($handle->uploaded)
				{
					// $handle->image_resize         = true;
					// $handle->image_x              = 1300;
					// $handle->image_y              = 1637;
					// $handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/blog_hot_project/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name'.$i];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["image".$i]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */
				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				//print_r($_POST);die;
				$test=array_combine($_POST['ordering'],$_POST['project_id']);

					$tableProjSpecial='tsr_microsites_template_projects';
					$delQuery="Delete from ".$tableProjSpecial." where microsite_id=".$rec_id." and template_id=".$_POST["template_id"]."";
					$obj_mysql->exec_query($delQuery, $link);	
					foreach($test as $key=>$val)
					{
						$projectArray['microsite_id']=$rec_id;
						$projectArray['template_id']=$_POST["template_id"];
						$projectArray['proj_id']=$val;
						$projectArray['ordering']=$key;

						$obj_mysql->insert_data($tableProjSpecial,$projectArray);

					}	

				$test2=array_combine($_POST['ordering1'],$_POST['dev_id']);

						//print_r($test);
						
					
					$tableProjSpecial='tsr_microsites_template_developers';
					$delQuery="Delete from ".$tableProjSpecial." where microsite_id=".$rec_id." and template_id=".$_POST["template_id"]."";
					$obj_mysql->exec_query($delQuery, $link);	
					foreach($test2 as $key=>$val)
					{
						$projectArray['microsite_id']=$rec_id;
						$projectArray['template_id']=$_POST["template_id"];
						$projectArray['dev_id']=$val;
						$projectArray['ordering']=$key;

						$obj_mysql->insert_data($tableProjSpecial,$projectArray);

					}

				$obj_common->redirect($page_name."?msg=update");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}else{
			$arr['created_date'] = "now()";


			$arr['ipaddress'] = $regn_ip;
			$arr['created_by'] = $_SESSION['login']['id'];
			if($obj_mysql->isDuplicate($tblName,'site_name', $arr['site_name'])){

			$arr["expire_date"] = date('Y-m-d', strtotime($_POST["expire_date"]));

			$sql = mysql_query("SELECT MAX(id) as maxId FROM tsr_microsites");
			$rec = mysql_fetch_array($sql);
			$setMaxId = $rec["maxId"];

			/*======================= START: Popup Logo ========================*/
			if($_FILES['popup_logo']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/popup_logo/".$_POST['old_file_name_popup']);
				$handle = new upload($_FILES['popup_logo']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/popup_logo/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['popup_logo'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Popup Logo ========================*/


		 	/*======================= START: Popup Logo ========================*/
			if($_FILES['redirect_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/redirect_banner/".$_POST['old_file_name_redirect_banner']);
				$handle = new upload($_FILES['redirect_banner']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/redirect_banner/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['redirect_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Popup Logo ========================*/

		 /*======================= START: Blog Header Banner ========================*/
			if($_FILES['blog_header_banner']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/blog_header_banner/".$_POST['old_file_name_redirect_banner']);
				$handle = new upload($_FILES['blog_header_banner']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/blog_header_banner/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['blog_header_banner'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Blog Header Banner ========================*/

		  /*======================= START: Blog Header Banner ========================*/
			if($_FILES['developer_image']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/developer_image/".$_POST['old_file_name_redirect_banner']);
				$handle = new upload($_FILES['developer_image']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/developer_image/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['developer_image'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Blog Header Banner ========================*/


		 		 /*======================= START:  Text Popup Developer Logo ========================*/
			if($_FILES['text_developer_popup_logo']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/text_developer_popup_logo/".$_POST['old_file_name_popup']);
				$handle = new upload($_FILES['text_developer_popup_logo']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/text_developer_popup_logo/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['text_developer_popup_logo'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Text Popup Developer Logo ========================*/


			/*======================= START: Proprty Logo ========================*/
			if($_FILES['property_logo']['name']!="")
			{   
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/property_logo/".$_POST['old_file_name']);
				$handle = new upload($_FILES['property_logo']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$setMaxId."/property_logo/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['property_logo'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Proprty Logo  ========================*/


		 /*======================= START: Google Verfication Image ========================*/
			if($_FILES['google_verification_url']['name']!="")
			{  
				@unlink(UPLOAD_PATH_MICROSITE_VERIFICATION_PATH."google_verification/pm/".$setMaxId."/".$_POST['old_file_name_google']);
				$handle = new upload($_FILES['google_verification_url']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 16;
					$handle->image_y              = 16;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_VERIFICATION_PATH."google_verification/pm/".$setMaxId."/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['google_verification_url'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: Google Verfication Image ========================*/


		 /*======================= START: XML Verfication Image ========================*/
			if($_FILES['sitemap_url']['name']!="")
			{  
				@unlink(UPLOAD_PATH_MICROSITE_VERIFICATION_PATH."sitemap/pm/".$setMaxId."/".$_POST['old_file_name_xml']);
				$handle = new upload($_FILES['sitemap_url']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 16;
					$handle->image_y              = 16;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_VERIFICATION_PATH."sitemap/pm/".$setMaxId."/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['sitemap_url'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: XML Verfication Image ========================*/

		 		 /*======================= START: XML FILE ========================*/
			if($_FILES['xml_file']['name']!="")
			{  
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE."sitemap/".$setMaxId."/".$_POST['old_file_name_xml']);
				$handle = new upload($_FILES['xml_file']);
				if ($handle->uploaded)
				{	
					/*$handle->image_resize         = true;
					$handle->image_x              = 16;
					$handle->image_y              = 16;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE."xml_file/".$setMaxId."/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['xml_file'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

		 /*======================= END: XML FILE ========================*/


				$rec_id = $obj_mysql->insert_data($tblName,$arr);
			
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */
				for($counterImages=0;$counterImages<3;$counterImages++){
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/".$_POST['old_file_name_propertyImage'.$counterImages]);	
				$arrayPropertyImage["img_type"] = "MICROSITE_TEMPLATE_IMAGE"; 
				$arrayPropertyImage["microsite_template_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				$handle = new upload($_FILES["propertyImage".$counterImages]);
				if ($handle->uploaded)
				{
					// $handle->image_resize         = true;
					// $handle->image_x              = 1300;
					// $handle->image_y              = 1637;
					// $handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_propertyImage'.$counterImages];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["propertyImage".$counterImages]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */


				for($i=0;$i<3;$i++){
				if($_FILES["image".$i]["name"]!="")
				{
				@unlink(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/blog_hot_project/".$_POST['old_file_name'.$i]);	
				$arrayPropertyImage["img_type"] = "MICROSITE_TEMPLATE_BLOG_BANNER_IMAGE"; 
				$arrayPropertyImage["microsite_template_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$i];
				if($_FILES["image".$i]["name"]!="")
				{
				$handle = new upload($_FILES["image".$i]);
				if ($handle->uploaded)
				{
					// $handle->image_resize         = true;
					// $handle->image_x              = 1300;
					// $handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_MICROSITE_TEMPLATE_IMAGE.$rec_id."/blog_hot_project/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name'.$i];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["image".$i]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}

				$test=array_combine($_POST['ordering'],$_POST['project_id']);

						//print_r($test);
						
					
					$tableProjSpecial='tsr_microsites_template_projects';
					foreach($test as $key=>$val)
					{
						$projectArray['microsite_id']=$rec_id;
						$projectArray['template_id']=$_POST["template_id"];
						$projectArray['proj_id']=$val;
						$projectArray['ordering']=$key;

						$obj_mysql->insert_data($tableProjSpecial,$projectArray);

					}	

				$test2=array_combine($_POST['ordering1'],$_POST['dev_id']);

						//print_r($test);
						
					
					$tableProjSpecial='tsr_microsites_template_developers';
					foreach($test2 as $key=>$val)
					{
						$projectArray['microsite_id']=$rec_id;
						$projectArray['template_id']=$_POST["template_id"];
						$projectArray['dev_id']=$val;
						$projectArray['ordering']=$key;

						$obj_mysql->insert_data($tableProjSpecial,$projectArray);

					}	

				$obj_common->redirect($page_name."?msg=insert");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		if($rec[$i]['template_id']<25)
		{
			$tempType='single';
		}
		else if($rec[$i]['template_id']>24)
		{
			$tempType='multiple';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&cty_id='.$rec[$i]['cty_id'].'&loc_id='.$rec[$i]['loc_id'].'&id='.$rec[$i]['id'].'&propty_id='.$rec[$i]['propty_id'].'&type='.$tempType.'" target="_blank" class="title_a">'.$rec[$i]['site_name'].'</a></td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['expire_date'].' <font color="green"><b>('.$rec[$i]['domainExpiryRemainingDays'].') Left<b/></font></td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td class="center">'.$st.'</td>
					<td><a href="'.$page_name.'?action=Update&cty_id='.$rec[$i]['cty_id'].'&loc_id='.$rec[$i]['loc_id'].'&id='.$rec[$i]['id'].'&propty_id='.$rec[$i]['propty_id'].'&type='.$tempType.'" target="_blank" class="edit">Edit</a>
					</td>
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Microsites', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>