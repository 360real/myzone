<?php $templateType= $_GET['type']; ?>
<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>
<script language="JavaScript" src="<?php echo $site['URL']?>view/js/jscolor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />



<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
<!-- -------------------------------------------------------------  -->
			<!-- PROPERTY DETAILS Search Code Block Start Here -->
<!-- --------------------------------------------------------------  -->
<?if($_GET["action"]!="Create" && $_GET["action"]!="Update"){?>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="SearchTable">
<thead>
<tr><th colspan="5" align="left"><h3 class="user_serch_head">Search Microsites</h3><a href="<?=$page_name?>?action=Create&type=<?php echo $templateType; ?>" class="add">Add New</a></th></tr>
</thead>
<tbody>
<tr class="search-form">
  <form  method="get"  name="searchForm"  >
    <input type="hidden" name="action" value="search">
    <td align="right"><strong>Microsites</strong></td>
    <td><?php echo AjaxDropDownList(TABLE_MICROSITES,"microsite_id","id","site_name",$_GET["microsite_id"],"id",$site['CMSURL'],"locationDiv","locationLabelDiv")?></td>
    <td><input name="submit" type="submit" class="button" value="Search" /></td>
  </form>
  <form  method="get"  name="searchForm"  >
  <input type="hidden" name="action" value="search">
  <td ><strong>Microsite Name </strong><input type="text" name="site_name" title="Site Name" lang='MUST' value="<?php echo($_GET['site_name'])?>">
  </td>
 
  <td><input name="submit" type="submit" class="button" value="Search" /></td>

  </form>
</tr>
<tr class="search-form">
<form  method="get"  name="testform"  >

<input type="hidden" name="action" value="search">
  <td><strong>Status</strong> <select name="status">
  <option value="1">Active</option>
  <option value="0">In-Active</option>	
  </select></td>
  	<td >Start Date:-
	<input type="text" name="startDate" />
	<script language="JavaScript">
	new tcal ({
		// form name
		'formname': 'testform',
		// input name
		'controlname': 'startDate'
	});

	</script>
</td>
<td>End Date:-
<input type="text" name="endDate" />
	<script language="JavaScript">
	new tcal ({
		// form name
		'formname': 'testform',
		// input name
		'controlname': 'endDate'
	});

	</script>
	</td>
  <td><?php echo AjaxDropDownListNoWidthProperty(TABLE_CITIES,"cty_id","id","city",$_GET["cty_id"],"id",$site['CMSURL']."populate_locations_advance.php","locationDiv","locationLabelDiv")?></td>
  <td></td>
  <tr/>

<tr class="search-form">
<td>

<?php 
  $sql = 'SELECT `id`,`template_name` FROM tsr_templates WHERE status=1';
  $templist =$obj_mysql->getAllData($sql);
  ?>
  <select  id="template_id" name="template_id" placeholder="Select Template">
    <option value="">Select Template</option>
    <?php foreach($templist as $value){ ?>
    <option value="<?php echo $value['id']; ?>" <?php if($value['id']==$_GET["template_id"]){ echo 'selected';} ?>><?php echo $value['template_name']; ?> </option>
    <?php } ?>
</td>
  
  <td>
    <?php 
    $sql = 'SELECT `id`,`property_name` FROM tsr_properties WHERE status=1';
    $prolist =$obj_mysql->getAllData($sql);
    ?>
    <select  id="propty_id" name="propty_id" placeholder="Select Property">
      <option value="">Select Property</option>
      <?php foreach($prolist as $value){ ?>
      <option value="<?php echo $value['id']; ?>" <?php if($value['id']==$_GET["propty_id"]){ echo 'selected';} ?>><?php echo $value['property_name']; ?> </option>
      <?php } ?>
    </select>
  </td>
  <td>

<?php 
  $sql = 'SELECT `id`,`name` FROM tsr_developers  WHERE status=1';
  $devlist =$obj_mysql->getAllData($sql);
  ?>
  <select  id="dev_id" name="dev_id" placeholder="Select Developer">
    <option value="">Select Developer</option>
    <?php foreach($devlist as $value){ ?>
    <option value="<?php echo $value['id']; ?>" <?php if($value['id']==$_GET["dev_id"]){ echo 'selected';} ?>><?php echo $value['name']; ?> </option>
    <?php } ?>
</td>
<td></td>
	<td><input name="submit" type="submit" class="button" value="Search" /></td>

</tr>
  </form>
</tr>


<?}?>
<!-- -------------------------------------------------------------  -->
			<!-- PROPERTY DETAILS Search Code Block End Here -->
<!-- --------------------------------------------------------------  -->
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="2" class="paging" >
<tr>
<td width="43" align="right" nowrap="nowrap" class="pagenumber"></td>
<td width="635" align="right" class="prevnext"><?php echo($paging);?></td>
</tr>
</table>
<form name='frm_list' method='post' action="<?=$page_name?>">
  <input type='hidden' name='action' id='Action'>
 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="MainTable">
    <thead>
      <tr>
        <th ><div>Microsites</div></th>
     	<th ><div>Added Date</div></th>
		<th ><div>Expire Date</div></th>
		<th ><div>Modified Date</div></th>
		<th ><div>IP Address</div></th>
        <th ><div>Status</div></th>
        <th ><div>Action</div></th>
      </tr>
    </thead>
    <tbody>
      <?php echo($list);  ?>
    </tbody>
  </table>
</form>
<table cellpadding="2" cellspacing="0" width="100%"  border="0" class="paging">
  <tr>
    <td colspan="2" valign="top" align="right" class="pagenumber" style="padding-right:30px;"><?php echo($total_rec);?> </td>
  </tr>
  <tr>
    <td colspan="2" valign="top"></td>
  </tr>
</table>
<script>
function conf(){
	return confirm('Are you want to delete it?');
}

$(document).ready(function () {
      $('#propty_id').selectize({
          sortField: 'text'
      });
  });
</script>
