<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype='multipart/form-data'>
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Commercial Property Type Detail</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Property Type</strong></td>
  <td><input name="name" type="text" title="Name" lang='MUST' value="<?php echo($rec['name'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Property Type Image</strong></td>
  <td><input name="image" type="file" title="Image" <?php if($rec['image']==''){?> lang='MUST' <?php } ?>  /> 

  		<?php if($rec['image']!=''){?> <input name="old_image" type="hidden" value="<?php echo($rec['image'])?>" /> <a href="<?php echo C_ROOT_URL."media_images/commercial/property_type/".$rec['image'];?>" target="_blank"><img src="<?php echo C_ROOT_URL."media_images/commercial/property_type/".$rec['image'];?>" height="100"></a> <?php } ?>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="is_deleted" value="0" id="0" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="is_deleted" value="1" id="1" <?php if ($rec['is_deleted']=="1"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>

 <tr class="<?php echo($clsRow2)?>">
   <td align="right"><strong>Description</strong></td>
  <td>
   <textarea name="description" class="ckeditor"  rows="500" cols="70"><?php echo($rec['description'])?></textarea>
    
  </td>
 </tr>


<tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>