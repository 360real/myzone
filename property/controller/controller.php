<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
error_reporting(0);
define('TABLE_PROPERTIES_RERA', 'tsr_properties_rera');
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
admin_login();
$message_arr = array("insert" => "Record(s) has been added successfully!",
    "update" => "Record(s) has been updated successfully!",
    "delete" => "Record(s) has been deleted successfully!",
    "cstatus" => "Record(s) status has been changed!",
    "unique" => "Property Name already in record!");

$tblName = TABLE_PROPERTIES; //main table name
$fld_id = 'id'; //primery key
$fld_status = 'status'; // staus field
$fld_orderBy = 'id'; // default order by field
$page_name = C_ROOT_URL . '/property/controller/controller.php';
$page_name_insert = C_ROOT_URL . '/image/controller/controller.php';
$page_name_update = '';
$clsRow1 = 'clsRow1';
$clsRow2 = 'clsRow2';
$action = remRegFx($_REQUEST['action']); // action to perform tast like Update, Create , Delete etc.
$rec_id = remRegFx($_GET['id']);
$arr_id = $_POST['items'] ? $_POST['items'] : array($rec_id); // for radio button
$query_str = "page=$page";

$file_name = "";
switch ($action) {
    case 'Create':
    case 'Update':
        $file_name = ROOT_PATH . "property/model/model.php";
        break;
    default:
        $file_name = ROOT_PATH . "property/view/view.php";
        break;
}

# Users with role '360Xlr8' can only search, modify, add properties mapped as is_xlr8
if ($_SESSION['login']['role_id']==20) {
    $is_xlr8_user = 1;
}

if ($_GET["action"] != "" && $_GET["action"] != "Create") {
    if ($_GET["action"] == "search" && $_GET["msg"] != "update") {
        $sql = "SELECT pro.*, loc.location,cty.city FROM " . TABLE_PROPERTIES . " pro INNER JOIN tsr_locations loc ON pro.loc_id=loc.id INNER JOIN tsr_cities cty ON pro.cty_id=cty.id WHERE ";

        if ($_GET["propertyName"] != "" && $_GET["cty_id"] == "" && $_GET["status"] == "" && $_GET["types"] == "") {
            $sql .= " pro.property_name like '%" . trim($_GET["propertyName"]) . "%'";
        }

        if ($_GET["cty_id"] != "" && $_GET["status"] == "" && $_GET["types"] == "") {
            $sql .= " pro.cty_id=" . $_GET["cty_id"];
        }

        if ($_GET["cty_id"] != "" && $_GET["status"] != "" && $_GET["types"] == "") {
            $sql .= " pro.cty_id=" . $_GET["cty_id"] . " and pro.status=" . $_GET["status"];
        }

        if ($_GET["cty_id"] != "" && $_GET["status"] != "" && $_GET["types"] != "") {
            $sql .= " pro.cty_id=" . $_GET["cty_id"] . " and pro.status=" . $_GET["status"] . " and pro.type_id=" . $_GET["types"];
        }

        if ($_GET["propertyName"] != "" && $_GET["cty_id"] != "" && $_GET["status"] != "" && $_GET["types"] != "") {
            $sql .= " AND pro.property_name like '%" . trim($_GET["propertyName"]) . "%'";
        }
        
        if ($is_xlr8_user == 1 ) {
            $sql .= " AND pro.is_xlr8 = 1";
        }

        if ($_GET["pid"] != "") {
            $sql .= " pro.id=" . $_GET["pid"];
        }
        
        //if($_GET["status"]=="" && $_GET["propertyName"]=="")

        //$sql.=" and pro.status=1";

        $sql .= " GROUP BY pro.id";

    } else if ($_GET["action"] == "Update") {
        $sql = "SELECT pro.*,proExt.* FROM " . TABLE_PROPERTIES . " pro";

        $sql .= " left join tsr_properties_extn proExt on pro.id = proExt.propertyId ";

        $sql .= ($rec_id ? " Where pro.$fld_id='$rec_id'" : ' ');
        /* echo $sql;
    exit;*/
    }
    $s = ($_GET['s'] ? 'ASC' : 'DESC');
    $sort = ($_GET['s'] ? 0 : 1);
    $f = $_GET['f'];

    if ($_GET["msg"] != "update") {
        if ($s && $f) {
            $sql .= " ORDER BY $f  $s";
        } else
        //$sql.= " ORDER BY $fld_orderBy desc";
        {
            $sql .= " ORDER BY pro.id desc";
        }

        /*---------------------paging script start----------------------------------------*/
        //echo $sql;
        $obj_paging->limit = 30;
        if ($_GET['page_no']) {
            $page_no = remRegFx($_GET['page_no']);
        } else {
            $page_no = 0;
        }

        $queryStr = $_SERVER['QUERY_STRING'];

        /*--------------------if page_no alreay exists please remove them ---------------------------*/
        $str_pos = strpos($queryStr, 'page_no');
        if ($str_pos > 0) {
            $queryStr = str_replace(substr($queryStr, ($str_pos - 1), strlen($queryStr)), "", $queryStr);
        }

        /*------------------------------------------------------------------------------------------*/
        $obj_paging->set_lower_upper($page_no);
        $total_num = $obj_mysql->get_num_rows($sql);

        $paging = $obj_paging->next_pre($page_no, $total_num, $page_name . "?$queryStr&", 'textArial11Bold', 'textArial11orgBold');
        $total_rec = $obj_paging->total_records($total_num);
        $sql .= " LIMIT $obj_paging->lower,$obj_paging->limit";
        /*---------------------paging script end----------------------------------------*/

        $rec = ($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
    }
}

if (count($_POST) > 0) {

    //echo count($_FILES['image_gallary']['name']);die;
    $arr = $_POST;
    $rec = $_POST;


    print_r($_POST);

    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES UPDATE Code Block Start Here
    /*--------------------------------------------------------------  */
    if ($rec_id) {
        $arr['rating'] = json_encode(array('location_rating'=>$_POST['location_rating'],'safety_rating'=>$_POST['safety_rating'],'transport_rating'=>$_POST['transport_rating'],
            'entertainment_rating'=>$_POST['entertainment_rating'],'connectivity_rating'=>$_POST['connectivity_rating'],'shopping_rating'=>$_POST['shopping_rating']));
        $arr['modified_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['price'] = $_POST["price"];
        if ($arr['pd'] == "1") {
            $arr['author_modified_date'] = 'now()';
        }
        $arr['property_usp'] = $_POST["property_usp"];
        /*--- Launch date and possession date code start here ---*/
        // 4 junee 2020
        $pdMonth = $_POST['pdMonth'];
        $pdYear = $_POST['pdYear'];
        $possession_date = $pdYear . "," . $pdMonth;
        $arr['possession_date'] = $possession_date;

        $ldMonth = $_POST['ldMonth'];
        $ldYear = $_POST['ldYear'];
        $launch_date = $ldYear . "," . $ldMonth;
        $arr['launch_date'] = $launch_date;
        /*--- Launch date and possession date code end here ---*/
        $arr['modified_by'] = $_SESSION['login']['id'];
        $arr["new_amenities"] = implode(',', $_POST["new_amenities"]);
        $arr["new_category"] = implode(',', $_POST["new_category"]);
        // $arr["bedrooms"] = implode(',', $_POST["bedrooms"]);
        $arr["bedrooms"] = $_POST["bedrooms"];

        /* -------------------------------------------------------------  */
        // BANK Part Code Block Start Here

        /*if(isset($_POST['bank']))
        {
        $getBank=$_POST["bank"];
        $allBank=implode(',',$getBank);
        //mysql_query("insert into iin_properties ('banks') values('".$allBank."')");
        $obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
        }*/

        /*--------------------------------------------------------------  */

            if($_FILES['site_plane']!=''){

                if (!file_exists(UPLOAD_PATH_PROPERTY_IMAGE.'site_plane')) {
                    chmod(UPLOAD_PATH_PROPERTY_IMAGE.'site_plane', 0777);
                    mkdir(UPLOAD_PATH_PROPERTY_IMAGE.'site_plane', 0777, true);
                }

                $eventsSavePathlogo  = UPLOAD_PATH_PROPERTY_IMAGE.'site_plane';
                $tempNamelogo = $_FILES['site_plane']['tmp_name'];
                $eventsNamelogo = str_replace(' ','_',trim($_FILES['site_plane']['name']));
                $eventsNamelogo = time().'_'.$eventsNamelogo;
                $FinalPathlogo = $eventsSavePathlogo . '/' . $eventsNamelogo;

                if(move_uploaded_file($tempNamelogo, $eventsSavePathlogo . '/' . $eventsNamelogo)){
                    //echo "dfdf";
                   $arr['site_plane']=$eventsNamelogo;
                }

            }else{

                $arr['site_plane']=$_POST['old_file_name_site_plane'];
            }

            if($_FILES['brochure']!=''){

                if (!file_exists('/var/www/html/myzone/media_images/brochure_manage')) {
                    chmod('/var/www/html/myzone/media_images/brochure_manage', 0777);
                    mkdir('/var/www/html/myzone/media_images/brochure_manage', 0777, true);
                }

                $brochureSavePath  = '/var/www/html/myzone/media_images/brochure_manage';
                $tempName = $_FILES['brochure']['tmp_name'];
                $brochureName = str_replace(' ','_',trim($_FILES['brochure']['name']));
                $brochureName = time().'_'.$brochureName;
                $FinalPath = $brochureSavePath . '/' . $brochureName;

                if(move_uploaded_file($tempName, $brochureSavePath . '/' . $brochureName)){
                    $arr['brochure']=$brochureName;
                }

            }

            

             if($_FILES['locationmap_image']!=''){

                if (!file_exists(UPLOAD_PATH_PROPERTY_IMAGE.'locationmap_image')) {
                    chmod(UPLOAD_PATH_PROPERTY_IMAGE.'locationmap_image', 0777);
                    mkdir(UPLOAD_PATH_PROPERTY_IMAGE.'locationmap_image', 0777, true);
                }

                $eventsSavePathlogo1  = UPLOAD_PATH_PROPERTY_IMAGE.'locationmap_image';
                $tempNamelogo1 = $_FILES['locationmap_image']['tmp_name'];
                $eventsNamelogo1 = str_replace(' ','_',trim($_FILES['locationmap_image']['name']));
                $eventsNamelogo1 = time().'_'.$eventsNamelogo1;
                $FinalPathlogo1 = $eventsSavePathlogo1 . '/' . $eventsNamelogo1;

                if(move_uploaded_file($tempNamelogo1, $eventsSavePathlogo1 . '/' . $eventsNamelogo1)){
                    //echo "dfdf";
                   $arr['locationmap_image']=$eventsNamelogo1;
                }

            }else{

                $arr['locationmap_image']=$_POST['old_file_name_locationmap_image'];
            }

            if($_FILES['about_image']!=''){

                if (!file_exists(UPLOAD_PATH_PROPERTY_IMAGE.'about_image')) {
                    chmod(UPLOAD_PATH_PROPERTY_IMAGE.'about_image', 0777);
                    mkdir(UPLOAD_PATH_PROPERTY_IMAGE.'about_image', 0777, true);
                }

                $eventsSavePathlogo3  = UPLOAD_PATH_PROPERTY_IMAGE.'about_image';
                $tempNamelogo3 = $_FILES['about_image']['tmp_name'];
                $eventsNamelogo3 = str_replace(' ','_',trim($_FILES['about_image']['name']));
                $eventsNamelogo3 = time().'_'.$eventsNamelogo3;
                $FinalPathlogo3 = $eventsSavePathlogo3 . '/' . $eventsNamelogo3;

                if(move_uploaded_file($tempNamelogo3, $eventsSavePathlogo3 . '/' . $eventsNamelogo3)){
                    //echo "dfdf";
                   $arr['about_image']=$eventsNamelogo3;
                }

            }else{

                $arr['about_image']=$_POST['old_file_name_about_image'];
            }

             if($_FILES['heighliight_image']!=''){

                if (!file_exists(UPLOAD_PATH_PROPERTY_IMAGE.'heighliight_image')) {
                    chmod(UPLOAD_PATH_PROPERTY_IMAGE.'heighliight_image', 0777);
                    mkdir(UPLOAD_PATH_PROPERTY_IMAGE.'heighliight_image', 0777, true);
                }

                $eventsSavePathlogo4  = UPLOAD_PATH_PROPERTY_IMAGE.'heighliight_image';
                $tempNamelogo4 = $_FILES['heighliight_image']['tmp_name'];
                $eventsNamelogo4 = str_replace(' ','_',trim($_FILES['heighliight_image']['name']));
                $eventsNamelogo4 = time().'_'.$eventsNamelogo4;
                $FinalPathlogo4 = $eventsSavePathlogo4 . '/' . $eventsNamelogo4;

                if(move_uploaded_file($tempNamelogo4, $eventsSavePathlogo4 . '/' . $eventsNamelogo4)){
                    //echo "dfdf";
                   $arr['heighliight_image']=$eventsNamelogo4;
                }

            }else{

                $arr['heighliight_image']=$_POST['old_file_name_heighliight_image'];
            }



        $newPath = UPLOAD_PATH_PROPERTY_IMAGE . $_POST["property_name"];
        $oldPath = UPLOAD_PATH_PROPERTY_IMAGE . $_POST["existingPropertyName"];
//print_r($arr);exit;
       // print_r($arr);die('Hell1');
        if ($_POST["existingPropertyName"] != $_POST["property_name"]) {
            $flagRename = rename($oldPath, $newPath);
            $counterRename = "1";
            $obj_mysql->update_data($tblName, $fld_id, $arr, $rec_id);

            $ctyquery = "SELECT  tsr_cities.city FROM tsr_cities INNER JOIN tsr_properties ON tsr_properties.cty_id=tsr_cities.id WHERE tsr_properties.id=" . $rec_id;
            $ctyresult = $obj_mysql->get_row_arr($ctyquery);

            $suggesttable = 'tsr_mynewkeyword';
            $fld = 'stringid';
            if (array_key_exists("property_name", $arr)) {
                $sug['keywordstring'] = $arr['property_name'] . " " . $ctyresult[0];
            }

            if (array_key_exists("country_id", $arr)) {
                $sug['CountryId'] = $arr['country_id'];
            }

            if (array_key_exists("cty_id", $arr)) {
                $sug['CityId'] = $arr['cty_id'];
            }

            if (array_key_exists("loc_id", $arr)) {
                $sug['LocationId'] = $arr['loc_id'];
            }

            if (array_key_exists("cluster_id", $arr)) {
                $sug['SublocationId'] = $arr['cluster_id'];
            }

            if (array_key_exists("lat", $arr)) {
                $sug['lat'] = $arr['lat'];
            }

            if (array_key_exists("lon", $arr)) {
                $sug['lng'] = $arr['lon'];
            }

            if (array_key_exists("status", $arr)) {if ($arr['status'] == 0) {
                $sug['status'] = 9;
            } else if ($arr['status'] == 1) {
                $sug['status'] = 0;
            } else {}
            } else {}
            $sug['stringid'] = $rec_id;
            $sug['property_usp'] = $_POST["property_usp"];
            $sug['keywordid'] = 'ProjectInfo.projectId';
            $sug['mainkeyword'] = 'ProjectInfo.propertyName';

            $keywordq = "SELECT id FROM tsr_mynewkeyword WHERE stringid=" . $rec_id;
            $keyword = $obj_mysql->get_row_arr($keywordq);
            if (($keyword == '') || ($keyword == 0)) {

                $sugid = $obj_mysql->insert_data($suggesttable, $sug);
                //$obj_mysql->callCurl($sugid);
            } else { $mainid = $keyword[0];
                $obj_mysql->update_data($suggesttable, $fld, $sug, $rec_id);
                
            }

        }



        // if($rec_id>0){
        //  // echo "Hello3"; die;
        //     $_POST['real_id']=$rec_id;
        //     $crmurl = 'http://timespropertymyzone.360realtors.com/newproject/controller/propertyinfoApi.php';
        //     $chcrm = curl_init($crmurl);
        //     $payload = '{"existingPropertyName":"Eminent Towers","property_id":"4198","type_id":"1","cat_id":"1","country_id":"1","cty_id":"29","loc_id":"786","cluster_id":"1080","property_name":"Eminent Towers","viewType":"0","property_name_display":"Eminent Towers","property_usp":"","devlpr_id":"106","microsite_sef_url":"","new_amenities":["1","5","6","7","9","10","12","13","14","15","16","19","23","29","35","43","47","48","50"],"new_overview":"","microsite_property_overview":"","microsite_property_overview_36":"","highlights":"","location_map":"","payment_plan":"","imageId":["",""],"old_file_name_propertyImage0":"","old_file_name_propertyImage1":"","locationimageId":[""],"old_file_name_site_plane":"","old_file_name_location_map":"","project_status_id":"1","is_nri":"0","is_platinum":"0","nri_tagline":"","nri_type":"","ldMonth":"","ldYear":"","pdMonth":"Ready To Move","pdYear":"","booking_status":"1","project_usp":"","project_area":"","bedrooms":"2,3 BHK","lon":"","lat":"","status":"1","location_rating":"0","safety_rating":"0","transport_rating":"0","entertainment_rating":"0","connectivity_rating":"0","shopping_rating":"0","hot_ordering":"1000","city_ordering":"1000","location_ordering":"1000","meta_title":"","meta_description":"","meta_keywords":"","microsite_meta_title":"","microsite_meta_description":"","microsite_meta_keywords":"","location_microsite_meta_title":"","location_microsite_meta_description":"","location_microsite_meta_keywords":"","dev_highlight_text1":"","dev_highlight_text2":"","dev_highlight_text3":"","airportDistancePriroty":"10","airport_distance":"0","airportDistanceUnit":"","airport_name":"","airport_distance1":"","airportDistanceUnit1":"","airport_name1":"","airport_distance2":"","airportDistanceUnit2":"","airport_name2":"","railwayStationPriroty":"10","railway_station_distance":"0","railwayStationDistanceUnit":"","railway_station_name":"","railway_station_distance1":"","railwayStationDistanceUnit1":"","railway_station_name1":"","railway_station_distance2":"","railwayStationDistanceUnit2":"","railway_station_name2":"","metroStationDistancePriroty":"10","metro_station_distance":"0","metroStationDistanceUnit":"","metro_station_name":"","metro_station_distance1":"","metroStationDistanceUnit1":"","metro_station_name1":"","metro_station_distance2":"","metroStationDistanceUnit2":"","metro_station_name2":"","busStopDistancePriroty":"10","busstop_distance":"0","busStopDistanceUnit":"","busstop_name":"","busstop_distance1":"","busStopDistanceUnit1":"","busstop_name1":"","busstop_distance2":"","busStopDistanceUnit2":"","busstop_name2":"","highwaydistancePriroty":"10","highwaydistance":"0","highwaydistanceUnit":"","highway_name":"","highwaydistance1":"","highwaydistanceUnit1":"","highway_name1":"","highwaydistance2":"","highwaydistanceUnit2":"","highway_name2":"","schoolCollegeDistancePriroty":"10","schoolCollege_distance":"0","schoolCollegeDistanceUnit":"","schoolCollege_name":"","schoolCollege_distance1":"","schoolCollegeDistanceUnit1":"","schoolCollege_name1":"","schoolCollege_distance2":"","schoolCollegeDistanceUnit2":"","schoolCollege_name2":"","hospitalDistancePriroty":"10","hospital_distance":"0","hospitalDistanceUnit":"","hospital_name":"","hospital_distance1":"","hospitalDistanceUnit1":"","hospital_name1":"","hospital_distance2":"","hospitalDistanceUnit2":"","hospital_name2":"","mallDistancePriroty":"10","mall_distance":"0","mallDistanceUnit":"","mall_name":"","mall_distance1":"","mallDistanceUnit1":"","mall_name1":"","mall_distance2":"","mallDistanceUnit2":"","mall_name2":"","bankAtmDistancePriroty":"10","bankAtm_distance":"0","bankAtmDistanceUnit":"","bankAtm_name":"","bankAtm_distance1":"","bankAtmDistanceUnit1":"","bankAtm_name1":"","bankAtm_distance2":"","bankAtmDistanceUnit2":"","bankAtm_name2":"","restaurantsDistancePriroty":"10","restaurants_distance":"0","restaurantsDistanceUnit":"","restaurants_name":"","restaurants_distance1":"","restaurantsDistanceUnit1":"","restaurants_name1":"","restaurants_distance2":"","restaurantsDistanceUnit2":"","restaurants_name2":"","dev_meta_title":"","dev_meta_description":"","dev_meta_keywords":"","is_show_type":"1","unit_type":"1","rera_no":[""],"project_id":[""],"total_unit":"","block":"","build_area":"","is_xlr8":"0","other":"","fittings":"","submit":"Submit","real_id":"4198"}';
        //   //  $payload =  htmlentities (print_r (json_encode($payload), true));  
        //     //echo $payload = str_replace(array("\r\n\t","\r\n", "\r", "\n", "\t"," "), '', $payload);die;
        //     curl_setopt($chcrm, CURLOPT_POSTFIELDS, $payload);
        //     curl_setopt($chcrm, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //     curl_setopt($chcrm, CURLOPT_RETURNTRANSFER, true);
        //     $resultcrm = curl_exec($chcrm);
        //    // print_r($resultcrm);
       
        //}

    if ($obj_mysql->isDupUpdateWithTwoFields($tblName, 'property_name', $arr['property_name'], 'id', $rec_id,'status','1') && $counterRename != "1"){


            /*********** new code added **********************/

            // $delQuery = "Delete from " . TABLE_PROPERTIES_RERA . " where prop_id=" . $rec_id . " ";

            // $test = $_POST['rera_no'];

            // $updateTblName = TABLE_PROPERTIES_RERA;
            // $obj_mysql->exec_query($delQuery, $link);
            // foreach ($test as $key => $val) {
            //     $projectArray['prop_id'] = $rec_id;
            //     $projectArray['rera_no'] = $val;
            //     $projectArray['ipaddress'] = $regn_ip;
            //     $projectArray['status'] = 1;
            //     $obj_mysql->insert_data($updateTblName, $projectArray);
            // }


            $delQuery = "Delete from " . TABLE_PROPERTIES_RERA . " where prop_id=" . $rec_id . " ";

            $test = $_POST['rera_no'];
            $reratower = $_POST['rera_no_tower'];
            $reraurl = $_POST['rera_no_url'];
            print_r($reratower);
            print_r($reraurl);

            $updateTblName = TABLE_PROPERTIES_RERA;
            $obj_mysql->exec_query($delQuery, $link);
            $i=0;
            foreach ($test as $key => $val) {
                $projectArray['prop_id'] = $rec_id;
                $projectArray['rera_no'] = $val;
                $projectArray['status'] = 1;
                $projectArray['rera_tower'] = $reratower[$i];
                $projectArray['rera_url'] = $reraurl[$i];
                print_r($projectArray);
                $obj_mysql->insert_data($updateTblName, $projectArray);
            $i++;}

            $delQuery = "Delete from " . TABLE_PROPERTIES_POSSESSION_DATE . " where prop_id=" . $rec_id . " ";

            $obj_mysql->exec_query($delQuery, $link);

            $pdMonth = $_POST['pdMonth'];
            $pdYear = $_POST['pdYear'];
            
            foreach ($pdMonth as $key => $val) {
                $projectArray['prop_id'] = $rec_id;
                $projectArray['pdMonth'] = $val;
                $projectArray['pdYear'] = $pdYear[$key];
                $possession_date = $pdYear[$key].",".$val;
                $projectArray['possession_date'] = $possession_date;
                $projectArray['ipaddress'] = $regn_ip;
                $obj_mysql->insert_data(TABLE_PROPERTIES_POSSESSION_DATE, $projectArray);
            }

            /************ new code added *******************************/
            $obj_mysql->update_data($tblName, $fld_id, $arr, $rec_id);


               
            
            for ($counterImages = 0; $counterImages < 2; $counterImages++) {

                if ($_FILES["propertyImage" . $counterImages]["name"] != "") {

                    $arrayPropertyImage["img_type"] = "DEVELOPER_PROPERTY_BANNER_IMAGE";
                    $arrayPropertyImage["propty_id"] = $rec_id;
                    $arrayPropertyImage["imageId"] = $_POST["imageId"][$counterImages];

                    $handle = new upload($_FILES["propertyImage" . $counterImages]);
                    if ($handle->uploaded) {
                        /*$handle->image_resize         = true;
                        $handle->image_x              = 1300;
                        $handle->image_y              = 1637;
                        $handle->image_ratio_y        = true;*/
                        $handle->file_safe_name = true;
                        $handle->process(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/");
                        if ($handle->processed) {
                            //echo 'image resized';
                            $arrayPropertyImage["image"] = $handle->file_dst_name;
                            @unlink(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/" . $_POST['old_file_name_propertyImage' . $counterImages]);
                            $handle->clean();
                        } else {
                            echo 'error : ' . $handle->error;
                        }
                    }

                } else {
                   
                    $arrayPropertyImage["image"] = $_POST['old_file_name_propertyImage' . $counterImages];
                }

                if ($arrayPropertyImage["imageId"] == "" && $_FILES["propertyImage" . $counterImages]["name"] != "") {
                    $obj_mysql->insert_data(TABLE_IMAGES, $arrayPropertyImage);
                } else {

                    $obj_mysql->update_data(TABLE_IMAGES, $fld_id, $arrayPropertyImage, $arrayPropertyImage["imageId"]);

                    $obj_common->redirect($page_name_update . "?action=search&pid=" . $rec["id"] . "&submit=Search&msg=update");
                }

              /*-----------------------*/
                   if ($_FILES["locationImage" . $counterImages]["name"] != "") {
                  /*  print_r($_FILES);
                    die('####');*/

                    $arraylocationImage["img_type"] = "DEVELOPER_PROPERTY_BANNER_IMAGE";
                    $arraylocationImage["property_id"] = $rec_id;
                    $arraylocationImage["locationimageId"] = $_POST["locationimageId"][$counterImages];

                    $handle = new upload($_FILES["locationImage" . $counterImages]);
                    if ($handle->uploaded) {
                        /*$handle->image_resize         = true;
                        $handle->image_x              = 1300;
                        $handle->image_y              = 1637;
                        $handle->image_ratio_y        = true;*/
                        $handle->file_safe_name = true;
                        $handle->process(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/");
                        if ($handle->processed) {
                            //echo 'image resized';
                            $arraylocationImage["image"] = $handle->file_dst_name;
                            @unlink(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/" . $_POST['old_file_name_propertyImage' . $counterImages]);
                            $handle->clean();
                        } else {
                            echo 'error : ' . $handle->error;
                        }
                    }

                } else {
                   
                    $arraylocationImage["image"] = $_POST['old_file_name_propertyImage' . $counterImages];
                }

                if ($arraylocationImage["locationimageId"] == "" && $_FILES["propertyImage" . $counterImages]["name"] != "") {
                    $obj_mysql->insert_data('location_map', $arraylocationImage);
                } else {

                    $obj_mysql->update_data('location_map', $fld_id, $arraylocationImage, $arraylocationImage["locationimageId"]);

                    $obj_common->redirect($page_name_update . "?action=search&pid=" . $rec["id"] . "&submit=Search&msg=update");
                }
            }


            if ($_GET["propertyName"] != "") {
                $obj_common->redirect($page_name_update . "?action=search&propertyName=" . $_GET["propertyName"] . "&submit=Search&msg=update");
            } else if ($rec["cty_id"] != "") {
                $obj_common->redirect($page_name_update . "?action=search&cty_id=" . $rec["cty_id"] . "&types=" . $_GET["types"] . "&page_no=" . $_GET["pg_no"] . "&status=" . $_GET["sta"] . "&submit=Search&msg=update");
            } else {
                $obj_common->redirect($page_name_update . "?action=search&pid=" . $rec["id"] . "&submit=Search&msg=update");
            }

        } else {
            $obj_common->redirect($page_name . "?msg=unique");
            $msg = 'unique';
        }
    }
    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES UPDATE Code Block END Here
    /*--------------------------------------------------------------  */

    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES INSERT Code Block START Here
    /*--------------------------------------------------------------  */

    else {
        $arr['created_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['price'] = $_POST["price"];
        if ($arr['pd'] == "1") {
            $arr['author_modified_date'] = 'now()';
        }

        /*--- Launch date and possession date code start here ---*/
        $pdMonth = $_POST['pdMonth'];
        $pdYear = $_POST['pdYear'];
        $possession_date = $pdYear . "," . $pdMonth;
        $arr['possession_date'] = $possession_date;

        $ldMonth = $_POST['ldMonth'];
        $ldYear = $_POST['ldYear'];
        $launch_date = $ldYear . "," . $ldMonth;
        $arr['launch_date'] = $launch_date;
        $arr['created_by'] = $_SESSION['login']['id'];
        /*--- Launch date and possession date code end here ---*/
        $arr["new_amenities"] = implode(',', $_POST["new_amenities"]);
        $arr["new_category"] = implode(',', $_POST["new_category"]);
        // $arr["bedrooms"] = implode(',', $_POST["bedrooms"]);
        $arr["bedrooms"] = $_POST["bedrooms"];
        
        /*if ($is_xlr8_user == 1) {
            $arr['is_xlr8'] = 1;
        }else{
          $arr['is_xlr8'] = 0;

        }*/
        $arr['specification'] = implode(',',$arr['specification']);
        //$arr['sub_specification']= implode(',',$arr['sub_specification']);
      if($_FILES['site_plane']!=''){

                if (!file_exists('/var/www/html/myzone/media_images/properties/site_plane')) {
                    chmod('/var/www/html/myzone/media_images/properties/site_plane', 0777);
                    mkdir('/var/www/html/myzone/media_images/properties/site_plane', 0777, true);
                }

                $eventsSavePathlogo  = '/var/www/html/myzone/media_images/properties/site_plane';
                $tempNamelogo = $_FILES['site_plane']['tmp_name'];
                $eventsNamelogo = str_replace(' ','_',trim($_FILES['site_plane']['name']));
                $eventsNamelogo = time().'_'.$eventsNamelogo;
                $FinalPathlogo = $eventsSavePathlogo . '/' . $eventsNamelogo;

                if(move_uploaded_file($tempNamelogo, $eventsSavePathlogo . '/' . $eventsNamelogo)){
                   $arr['site_plane']=$eventsNamelogo;
                }

            }else{

                $arr['site_plane']=$_POST['old_file_name_site_plane'];
            }

            //print_r($arr);die('Hell2');

        if ($obj_mysql->isDuplicateWithTwoFields($tblName, 'property_name', $arr['property_name'],'status','1')) {

            $rec_id = $obj_mysql->insert_data($tblName, $arr);
           

           if($rec_id>0){
                $_POST['real_id']=$rec_id;
                $crmurl = 'http://timespropertymyzone.360realtors.com/newproject/controller/propertyinfoApi.php';
                $chcrm = curl_init($crmurl);
               echo  $payload = json_encode($_POST);
                curl_setopt($chcrm, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($chcrm, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt($chcrm, CURLOPT_RETURNTRANSFER, true);
                $resultcrm = curl_exec($chcrm);
                print_r($resultcrm);
                die;
            }
            


            $ctyquery = "SELECT tsr_locations.location, tsr_cities.city FROM tsr_locations INNER JOIN tsr_cities ON tsr_cities.id=tsr_locations.cty_id WHERE tsr_cities.id=" . $arr['cty_id'] . " AND tsr_locations.id=" . $arr['loc_id'];
            $ctyresult = $obj_mysql->get_row_arr($ctyquery);

            $suggesttable = 'tsr_mynewkeyword';
            $sug['stringid'] = $rec_id;
            $sug['keywordid'] = 'ProjectInfo.projectId';
            $sug['keywordstring'] = $arr['property_name'] . " " . $ctyresult[1];
            $sug['mainkeyword'] = 'ProjectInfo.propertyName';
            $sug['CountryId'] = $arr['country_id'];

            $sug['CityId'] = $arr['cty_id'];
            $sug['LocationId'] = $arr['loc_id'];
            $sug['SunlocationId'] = $arr['cluster_id'];
            $sug['lat'] = $arr['lat'];
            $sug['lng'] = $arr['lon'];
            if ($arr['status'] == 0) {
                $sug['status'] = 9;
            } else {
                $sug['status'] = 0;
            }

            $sugid = $obj_mysql->insert_data($suggesttable, $sug);
            //$obj_mysql->callCurl($sugid);
          /*    print_r($_FILES);
              die('########!!');*/
            for ($counterImages = 0; $counterImages < 2; $counterImages++) {
                if ($_FILES["propertyImage" . $counterImages]["name"] != "") {
                    $arrayPropertyImage["img_type"] = "DEVELOPER_PROPERTY_BANNER_IMAGE";
                    $arrayPropertyImage["propty_id"] = $rec_id;
                    $arrayPropertyImage["imageId"] = $_POST["imageId"][$counterImages];

                    $handle = new upload($_FILES["propertyImage" . $counterImages]);
                    if ($handle->uploaded) {
                        /*$handle->image_resize         = true;
                        $handle->image_x              = 1300;
                        $handle->image_y              = 1637;
                        $handle->image_ratio_y        = true;*/
                        $handle->file_safe_name = true;
                        $handle->process(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/");
                        if ($handle->processed) {
                            //echo 'image resized';
                            $arrayPropertyImage["image"] = $handle->file_dst_name;
                            @unlink(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/" . $_POST['old_file_name_propertyImage' . $counterImages]);
                            $handle->clean();
                        } else {
                            echo 'error : ' . $handle->error;
                        }
                    }

                } else {
                    $arrayPropertyImage["image"] = $_POST['old_file_name_propertyImage' . $counterImages];
                }

                if ($arrayPropertyImage["imageId"] == "" && $_FILES["propertyImage" . $counterImages]["name"] != "") {
                    $obj_mysql->insert_data(TABLE_IMAGES, $arrayPropertyImage);
                     $obj_common->redirect($page_name."?msg=insert"); 
                } else {
                  
                    $obj_mysql->update_data(TABLE_IMAGES, $fld_id, $arrayPropertyImage, $arrayPropertyImage["imageId"]);
                   
                   // $obj_common->redirect($page_name."?msg=update");    
                }

            }

            $delQuery = "Delete from " . TABLE_PROPERTIES_RERA . " where prop_id=" . $rec_id . " ";

            $test = $_POST['rera_no'];
            $reratower = $_POST['rera_no_tower'];
            $reraurl = $_POST['rera_no_url'];
            // print_r($reratower);
            // print_r($reraurl);

            $updateTblName = TABLE_PROPERTIES_RERA;
            $obj_mysql->exec_query($delQuery, $link);
            $i=0;
            foreach ($test as $key => $val) {
                $projectArray['prop_id'] = $rec_id;
                $projectArray['rera_no'] = $val;
                $projectArray['status'] = 1;
                $projectArray['rera_tower'] = $reratower[$i];
                $projectArray['rera_url'] = $reraurl[$i];
                $obj_mysql->insert_data($updateTblName, $projectArray);
            $i++;}
            die('$$$');

            $pdMonth = $_POST['pdMonth'];
            $pdYear = $_POST['pdYear'];
            
            foreach ($pdMonth as $key => $val) {
                $projectArray['prop_id'] = $rec_id;
                $projectArray['pdMonth'] = $val;
                $projectArray['pdYear'] = $pdYear[$key];
                $possession_date = $pdYear[$key].",".$val;
                $projectArray['possession_date'] = $possession_date;
                $projectArray['ipaddress'] = $regn_ip;
                $obj_mysql->insert_data(TABLE_PROPERTIES_POSSESSION_DATE, $projectArray);
            }

            $obj_common->redirect($page_name_insert . "?msg=insert&id=" . $rec_id . "");
        } else {
            $obj_common->redirect($page_name . "?msg=unique");
            $msg = 'unique';
        }
    }

    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES INSERT Code Block END Here
    /*--------------------------------------------------------------  */
}

/* -------------------------------------------------------------  */
// PROPERTY DETAILS DISPLAY Code Block START Here
/*--------------------------------------------------------------  */
$list = "";
for ($i = 0; $i < count($rec); $i++) {
    if ($rec[$i]['status'] == "1") {
        $st = '<font color=green>Active</font>';
    } else {
        $st = '<font color=red>Inactive</font>';
    }

    if ($_GET['page_no'] != "") {
        $updateUrl = $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&country_id=' . $rec[$i]['country_id'] . '&user_id=' . $rec[$i]['user_id'] . '&loc_id=' . $rec[$i]['loc_id'] . '&cty_id=' . $rec[$i]['cty_id'] . '&devlpr_id=' . $rec[$i]['devlpr_id'] . '&cat_id=' . $rec[$i]['cat_id'] . '&type_id=' . $rec[$i]['type_id'] . '&cluster_id=' . $rec[$i]['cluster_id'] . '&pg_no=' . $_GET['page_no'] . '&types=' . $_GET['types'] . '&status=' . $_GET['status'] . '&project_status_id=' . $rec[$i]['project_status_id'];
    } else if ($_GET["propertyName"] != "") {

        $updateUrl = $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&country_id=' . $rec[$i]['country_id'] . '&user_id=' . $rec[$i]['user_id'] . '&loc_id=' . $rec[$i]['loc_id'] . '&cty_id=' . $rec[$i]['cty_id'] . '&devlpr_id=' . $rec[$i]['devlpr_id'] . '&cat_id=' . $rec[$i]['cat_id'] . '&type_id=' . $rec[$i]['type_id'] . '&cluster_id=' . $rec[$i]['cluster_id'] . '&propertyName=' . $_GET["propertyName"] . '&project_status_id=' . $rec[$i]['project_status_id'];
    } else {
        $updateUrl = $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&country_id=' . $rec[$i]['country_id'] . '&user_id=' . $rec[$i]['user_id'] . '&loc_id=' . $rec[$i]['loc_id'] . '&cty_id=' . $rec[$i]['cty_id'] . '&devlpr_id=' . $rec[$i]['devlpr_id'] . '&cat_id=' . $rec[$i]['cat_id'] . '&type_id=' . $rec[$i]['type_id'] . '&cluster_id=' . $rec[$i]['cluster_id'] . '&types=' . $_GET['types'] . '&sta=' . $_GET['status'] . '&project_status_id=' . $rec[$i]['project_status_id'];
    }

    $clsRow = ($i % 2 == 0) ? 'clsRow1' : 'clsRow2';
    $list .= '<tr class="' . $clsRow . '">';

    if (in_array($_SESSION['login']['role_id'], array("8", "9", "10"))) {
        $list .= '<td><a href="javascript:void(0)" class="title_a">' . $rec[$i]['property_name'] . '</a></td>';
    } else {
        $list .= '<td><a href="' . $updateUrl . '" class="title_a">' . $rec[$i]['property_name'] . '</a></td>';
    }

    $cluster_name = "";
    if($rec[$i]['cluster_id']!='' && $rec[$i]['cluster_id']!=null &&  $rec[$i]['cluster_id']!=0)
    {
        $sql_cluster = "SELECT cluster from tsr_cluster where id=".$rec[$i]['cluster_id'];
        $cluster_data = $obj_mysql->get_assoc_arr($sql_cluster);
        $cluster_name = $cluster_data['cluster'];
    }

    $micro_market_name = "";
    if($rec[$i]['micro_market_id']!='' && $rec[$i]['micro_market_id']!=null &&  $rec[$i]['micro_market_id']!=0)
    {
        $sql_micro_market = "SELECT name from tsr_micro_market where id=".$rec[$i]['micro_market_id'];
        $micro_market_data = $obj_mysql->get_assoc_arr($sql_micro_market);
        $micro_market_name = $micro_market_data['name'];
    }
        

    $list .= '<td class="center">' . $rec[$i]['location'] . '</td>
                    <td class="center">' . $rec[$i]['city'] . '</td>
                    <td class="center">'.$cluster_name.'</td>
                    <td class="center">'.$micro_market_name.'</td>
                    <td class="center">' . $rec[$i]['created_date'] . '</td>

                    <td class="center">' . $st . '</td>
                    <td>
                    <form name="editPropertyForm_' . $i . '" id="editPropertyForm_' . $i . '">
                    <input type="hidden" name="action" value="Update">
                    <input type="hidden" name="id" value=' . $rec[$i]['id'] . '>
                    <input type="hidden" name="loc_id" value=' . $rec[$i]['loc_id'] . '>
                    <input type="hidden" name="cty_id" value=' . $rec[$i]['cty_id'] . '>
                    <input type="hidden" name="cat_id" value=' . $rec[$i]['cat_id'] . '>
                    <input type="hidden" name="type_id" value=' . $rec[$i]['type_id'] . '>
                    <input type="hidden" name="cluster_id" value=' . $rec[$i]['cluster_id'] . '>
                    <input type="hidden" name="user_id" value=' . $rec[$i]['user_id'] . '>
                    <input type="hidden" name="country_id" value=' . $rec[$i]['country_id'] . '>
                    <input type="hidden" name="devlpr_id" value=' . $rec[$i]['devlpr_id'] . '>
                    <input type="hidden" name="project_status_id" value=' . $rec[$i]['project_status_id'] . '>';

    if ($_GET['propertyName'] != "") {
        $list .= '<input type="hidden" name="propertyName" value="' . $_GET['propertyName'] . '">';
    }

    if ($_GET['page_no'] != "") {
        $list .= '<input type="hidden" name="pg_no" value=' . $_GET['page_no'] . '>';
    }

    $list .= '<select name="editType' . $i . '" id="editType' . $i . '" onChange="editTypeAction(' . $i . ');">
                    <option value="0">--Select Edit Type --</option>';
    if (!in_array($_SESSION['login']['role_id'], array("8", "9", "10"))) {
        $list .= '
                    <option value="1">Property Detail</option>
                    <option value="2">Property Images</option>
                    <option value="3">Property Prices</option>
                    <option value="5">Property Points</option>
                    <option value="6">Property Location Map</option>
                    <option value="7">Property Payment Plan</option>
                    <option value="8">Property Payment Plan NRI</option>';
    } else {
        $list .= '<option value="4">Property Meta</option>
                    <option value="3">Property Prices</option>
                    <option value="8">Property Payment Plan NRI</option>
                    ';
    }
    $list .= '</select></form>
                    </td>
                </tr>';

}
if ($list == "") {
    $list = "<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
}
/* -------------------------------------------------------------  */
// PROPERTY DETAILS DISPLAY Code Block END Here
/*--------------------------------------------------------------  */
?>
<?php include LIBRARY_PATH . "includes/header.php";
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Property', $site['TITLE'], $site['URL']);
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <td align="center" colspan="2" valign="top">
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
                 <?php
if ($_GET['msg'] != "") {
    echo ('<div class="notice">' . $message_arr[$_GET['msg']] . '</div>');
}
if ($msg != "") {
    echo ('<div class="notice">' . $message_arr[$msg] . '</div>');
}
?>
                 </td>
                </tr>
 </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include $file_name;?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<script>
function editTypeAction(id)
{

    var arr = document.getElementById("editType"+id).value;
    var formNameSet = "editPropertyForm_"+id;

    if(arr=="1")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/property/controller/controller.php';
    }
    else if(arr=="2")
    {
       document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/image/controller/controller.php';

    }
    else if(arr=="3")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/price/controller/controller.php';
    }
    else if(arr=="4")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/property_meta/controller/controller.php';
    } else if(arr=="5")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/property_point/controller/controller.php';
    }
    else if(arr=="6")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/location_map/controller/controller.php';
    }
    else if(arr=="7")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/payment_plan/controller/controller.php';
    }
    else if(arr=="8")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/price_nri/controller/controller.php';
    }

    document.getElementById(formNameSet).submit();


}
</script>
<?php include LIBRARY_PATH . "includes/footer.php";?>
