<script src="<?php echo $site['URL']?>cms/js/jscal2.js"></script>
<script src="<?php echo $site['URL']?>cms/js/lang/en.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>cms/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>cms/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>cms/css/cal/steel.css" />
<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<input type="hidden" name="existingDeveloper" value="<?php echo($rec['developer'])?>">

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Developers Detail</h3></th></tr>
</thead>
<tbody>


<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Developer Name</strong></td>
<td><input type="text" lang='MUST' name="name" title="Developer Name" maxlength="120" value="<?php echo($rec['name'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>">
    <td align="right" ><strong>Select Developer Logo</strong></td>
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['logo']=='') {?> title="Select Developer Logo" <?php } ?> name="logo" id="logo" /></td>
          <td width="62%"><?php if($rec['logo']):?>
            <br />
            <img src="<?php echo  ($site['BUILDERLOGOURL'].$rec['id']."/".$rec['logo'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['logo'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>


<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Founder</strong></td>
<td><input type="text" name="founder"  maxlength="120" value="<?php echo($rec['founder'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Corporate Office</strong></td>
<td><input type="text"  name="corporate_office"  maxlength="200" value="<?php echo($rec['corporate_office'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Founded In</strong></td>
<td><input type="text"  name="founded_in"  maxlength="120" value="<?php echo($rec['founded_in'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Company Head</strong></td>
<td><input type="text"  name="company_head"  maxlength="120" value="<?php echo($rec['company_head'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Contact Number</strong></td>
<td><input type="text"  name="contact_number"  maxlength="200" value="<?php echo($rec['contact_number'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Company Website</strong></td>
<td><input type="text"  name="company_website"  maxlength="120" value="<?php echo($rec['company_website'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Company Email</strong></td>
<td><input type="text"  name="company_email"  maxlength="120" value="<?php echo($rec['company_email'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Customer Care Officer</strong></td>
<td><input type="text"  name="customer_care_officer"  maxlength="200" value="<?php echo($rec['customer_care_officer'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Customer Care Number</strong></td>
<td><input type="text"  name="customer_care_number"  maxlength="200" value="<?php echo($rec['customer_care_number'])?>" size="40" /></td></tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"><strong>Customer Care Email Id</strong></td>
<td>
<input type="text"  name="customer_care_email_id"  maxlength="200" value="<?php echo($rec['customer_care_email_id'])?>" size="40" /></td></tr>



<tr class="<?php echo($cls2)?>">
  <td align="right" ><strong>Description</strong></td>
  <td><textarea name="description" class="ckeditor"  id="description" rows="8" cols="70"><?php echo($rec['description'])?></textarea>  </td>
</tr>
<tr class="<?php echo($cls2)?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
           <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
 </tr>


<tr class="<?php echo($cls2)?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
           <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
 </tr>


<tr class="<?php echo($cls2)?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
           <td><textarea  name="meta_description" id="meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
 </tr>

<tr class="<?php echo($cls2)?>">
        <td width="15%" align="right"><strong>Ordering</strong></td>
           <td><textarea  name="ordering" id="ordering" rows="1" cols="175" ><?php echo stripslashes($rec['ordering'])?></textarea></td>
 </tr>

<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Home page ordering</strong> </td>
  <td><input name="home_page_ordering" type="text" title="home page ordering" lang='MUST' value="<?php if($_GET["action"]=="Update" && $rec['home_page_ordering']>0) echo $rec['home_page_ordering']; else echo "1000"; ?>" /></td>
 </tr>

<tr class="<?php echo($cls1)?>"><td align="right" ><strong>Status</strong> </td>
  <td>
  <input type="radio" name="status" value="1" id="1" checked="checked" /> 
  <label for="1">Active</label> 
  <input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
  <label for="0">Inactive</label>  </td></tr>

<tr class="<?php echo($cls1)?>"><td align="center">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>
