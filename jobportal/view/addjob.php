
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="<?php echo $site['URL']?>view/js/jquery.dropdown.js"></script>
<?php
$timeArray =  array('00:00:00','00:30:00','01:00:00','01:30:00','02:00:00','02:30:00','03:00:00','03:30:00','04:00:00','04:30:00','05:00:00','05:30:00','06:00:00','06:30:00','07:00:00','07:30:00','08:00:00','08:30:00','09:00:00','09:30:00','10:00:00','10:30:00','11:00:00','11:30:00','12:00:00','13:00:00','13:30:00','14:00:00','14:30:00','15:00:00','15:30:00','16:00:00','16:30:00','17:00:00','17:30:00','18:00:00','18:30:00','19:00:00','19:30:00','20:00:00','20:30:00','21:00:00','21:30:00','22:00:00','22:30:00','23:00:00','23:30:00');

//print_r($listdata);
?>

<style type="text/css">
  input#ads_image_text,input#ads_banner_image_text{
    width: 95%;
}
</style>
 <form id="datafm"  enctype="multipart/form-data">
  <input type="hidden" id="post_id" name="post_id" value="<?php echo $listdata[0]['id']; ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="1" align="left"><h3>Jobs</h3></th><th align="right"><a class="buttons" href="/jobportal/controller/controller.php?action=joblist"><h3>Jobs List</h3></a></th></tr>
</thead>
<tbody>


  <tr class="<?php echo($clsRow1)?>" id="ads_country" >
    <td align="right"><strong><font color="#FF0000">*</font>Job Country</strong></td>
    <td>
      <?php 
      $sql = 'SELECT `id`,`country` FROM tsr_countries WHERE status=1';
      $contlist =$obj_mysql->getAllData($sql);
      ?>
       <select  id="country_id" name="country_id" placeholder="Select Ads Country" onchange="adscountry()">
          <option value="">Select Country </option>
          <?php for ($i=0; $i <count($contlist) ; $i++) { ?>
           <option value="<?php echo $contlist[$i]['id']; ?>" <?php if($listdata[0]['country_id']== $contlist[$i]['id']):?> selected <?php endif; ?>><?php echo $contlist[$i]['country']; ?></option>
          <?php } ?>
      </select>
    </td>
  </tr> 



  <tr class="<?php echo($clsRow1)?>" id="city_section">
    <td align="right"><strong><font color="#FF0000"></font>Job City </strong></td>
    <td>
       <?php 
      $sql = 'SELECT `id`,`city`,StateID FROM  tsr_cities';
      $citylist =$obj_mysql->getAllData($sql);
      ?>
      <select  id="ads_city" name="city_id" placeholder="Select Ads City">
        <option value="">Select City </option>
        </select>
    </td>
  </tr>


 <tr class="<?php echo($clsRow1)?>" id="">
    <td align="right"><strong><font color="#FF0000"></font>Job category </strong></td>
    <td>
       <?php 
      $sql = 'SELECT `id`,`title` FROM  job_category WHERE status =1';
      $catlist =$obj_mysql->getAllData($sql);
      ?>
      <select  id="cat_id" name="cat_id" placeholder="Select Category" >
        <option value="">Select category </option>
        <?php for ($i=0; $i <count($catlist) ; $i++) { ?>
           <option value="<?php echo $catlist[$i]['id']; ?>" <?php if($listdata[0]['cat_id']== $catlist[$i]['id']):?> selected <?php endif; ?>><?php echo $catlist[$i]['title']; ?></option>
          <?php } ?>
       </select>
    </td>
  </tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Job Title</strong></td>
  <td><input id="title" name="title" type="text" title="Title" lang='MUST' value="<?php echo($listdata[0]['title'])?>" size="50%"  /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Min Work Experience</strong></td>
  <td>
      <select  id="min_exp" name="min_exp" placeholder="Min Work Experience">
      <?php for($i=0 ; $i<=50; $i++){ ?>
      <option value="<?php echo $i; ?>" <?php if($listdata[0]['min_exp']==$i){ echo 'selected'; } ?>><?php echo $i; ?> Years</option>
      <?php } ?>
      </select>
    </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Max Work Experience</strong></td>
  <td>
      <select  id="max_exp" name="max_exp" placeholder="Max Work Experience">
      <?php for($i=0 ; $i<=50; $i++){ ?>
      <option value="<?php echo $i; ?>" <?php if($listdata[0]['max_exp']==$i){ echo 'selected'; } ?>><?php echo $i; ?> Years</option>
      <?php } ?>
      </select>
    </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Budget</strong></td>
  <td><input id="budget" name="budget" type="text" title="budget" lang='MUST' value="<?php echo($listdata[0]['budget'])?>" size="50%"  /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000"></font>Number Of openings</strong></td>
  <td><input id="openings" name="openings" type="text" title="openings" lang='MUST' value="<?php echo($listdata[0]['openings'])?>" size="50%"  /></td>
</tr>

 <tr class="<?php echo($clsRow1)?>" id="">
    <td align="right"><strong><font color="#FF0000"></font>Job Post </strong></td>
    <td>
      <select  id="jobview_type" name="jobview_type" placeholder="Select Job" onchange="showDiv(this)">
        <option value="">Select category </option>
        <option value="1" <?php if($listdata[0]['jobview_type']==1){ echo 'selected'; } ?>>Job link</option>
        <option value="2" <?php if($listdata[0]['jobview_type']==2){ echo 'selected'; } ?>>Add details </option>
       </select>
    </td>
  </tr>

<tr class="<?php echo($clsRow1)?> joblink" style="display:none">
  <td align="right"><strong><font color="#FF0000">*</font>Job Link</strong></td>
  <td><input id="job_link" name="job_link" type="text" title="job_link" lang='MUST' value="<?php echo($listdata[0]['job_link'])?>" size="50%"  /></td>
</tr>

<tr class="<?php echo($clsRow1)?> jobdetail">
  <td align="right"><strong><font color="#FF0000">*</font>Education</strong></td>
  <td><input id="education" name="education" type="text" title="education" lang='MUST' value="<?php echo($listdata[0]['education'])?>" size="50%"  /></td>
</tr>

<tr class="<?php echo($clsRow1)?> jobdetail">
  <td align="right"><strong><font color="#FF0000">*</font>Job Role</strong></td>
  <td><input id="job_role" name="job_role" type="text" title="job_role" lang='MUST' value="<?php echo($listdata[0]['job_role'])?>" size="50%"  /></td>
</tr>
<!-- 

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Industry Type</strong></td>
  <td><input id="industry_type" name="industry_type" type="text" title="industry_type" lang='MUST' value="<?php echo($listdata[0]['industry_type'])?>" size="50%"  /></td>
</tr> -->

 <tr class="<?php echo($clsRow1)?> jobdetail" id="">
    <td align="right"><strong><font color="#FF0000"></font> Employment Type</strong></td>
    <td>
     
      <select  id="industry_type" name="employment_type" placeholder="Select Category" >
        <option value="">Select  Employment Type </option>
        <option value="full_time"  <?php if($listdata[0]['employment_type']=='full_time'){ echo 'selected'; } ?>>Full Time, Permanent</option>
        <option value="part_time" <?php if($listdata[0]['employment_type']=='part_time'){ echo 'selected'; } ?>>Part Time</option>
        <option value="contract" <?php if($listdata[0]['employment_type']=='contract'){ echo 'selected'; } ?>>Contract</option>
       </select>
    </td>
  </tr>

<tr class="<?php echo($clsRow1)?> jobdetail">
  <td align="right"><strong><font color="#FF0000">*</font>Department</strong></td>
  <td><input id="department" name="department" type="text" title="department" lang='MUST' value="<?php echo($listdata[0]['department'])?>" size="50%"  /></td>
</tr>

<tr class="<?php echo($clsRow1)?> jobdetail">
  <td align="right"><strong><font color="#FF0000"></font>Apply on naukri</strong></td>
  <td><input id="apply_on" name="apply_on" type="text" title="apply_on" lang='MUST' value="<?php echo($listdata[0]['apply_on'])?>" size="50%"  /></td>
</tr>

<tr class="<?php echo($clsRow1)?> ">
  <td align="right"><strong><font color="#FF0000">*</font>Industry Type</strong></td>
  <td><input id="industry_type" name="industry_type" type="text" title="industry_type" lang='MUST' value="<?php echo($listdata[0]['industry_type'])?>" size="50%"  /></td>
</tr>

<tr class="<?php echo($clsRow1)?> jobdetail">
  <td align="right"><strong><font color="#FF0000"></font>Key Skills</strong></td>
  <td><textarea  name="key_skill"  id="key_skill" rows="4" cols="70"><?php echo $listdata[0]["key_skill"]; ?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?> jobdetail">
  <td align="right"><strong><font color="#FF0000"></font>Job Highlights</strong></td>
  <td><textarea class="ckeditor" name="job_highlights"  id="job_highlights" rows="4" cols="70"><?php echo $listdata[0]["job_highlights"]; ?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?> jobdetail">
  <td align="right"><strong><font color="#FF0000">*</font>Description</strong></td>
  <td><textarea class="ckeditor" name="discription"  id="discription" rows="4" cols="70"><?php echo $listdata[0]["discription"]; ?></textarea></td>
</tr>





  <tr class="<?php echo($clsRow1)?>">
    <td align="right"><strong><font color="#FF0000"></font>Job Status</strong></td>
    <td>
      <select  id="status" name="status" placeholder="Select Ads Location">
         <option value="">Select Status </option>
         <option value="1" <?php if($listdata[0]['status']== 1):?> selected <?php endif;?>>Active </option>
         <option value="0" <?php if($listdata[0]['status']== 0):?> selected <?php endif;?>>In Active</option>
        </select>
    </td>
  </tr>

  <tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
    <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
      <input name="reset" type="reset" class="button" value="Reset" />
      <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" />
    </td>
  </tr>
</tr>

</tbody>
</table>
</form>

<script>
function adstype() {
  var ads_type = $("#ads_type").val();
  if(ads_type=='1'){
    $('#ads_image').show();
    // $('#ads_banner_image').show();
    $('#ads_text').hide();

  }
  if(ads_type=='2'){
    $('#ads_image').show();
    // $('#ads_banner_image').hide();
    $('#ads_text').show();
  }
}

$(window).on("load", function(){
  var ads_type = $("#ads_type").val();
  if(ads_type=='1'){
    $('#ads_image').show();
    $('#ads_text').hide();
  }
  if(ads_type=='2'){
    $('#ads_image').hide();
    $('#ads_text').show();
  }
});

function adscountry(){
  var ads_city = $("#country_id").val();
     $.ajax({
      url: "/jobportal/controller/citydata.php",
      type: 'POST',  
      data: {'ads_city':ads_city},
      success: function (data) {
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select City </option>';
          for (var i = 0; i < json.length; i++) {
             opetionhtml+='<option value='+json[i]['id']+'>'+json[i]['city']+'</option>';
          }
          $('#ads_city').html(opetionhtml);
      }
  });
}

function adscity() {
  var ads_city = $("#ads_city").val();
  $.ajax({
      url: "/jobportal/controller/locationdata.php",
      type: 'POST',  
      data: {'city':ads_city},
      success: function (data) {
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select Location </option>';
          for (var i = 0; i < json.length; i++) {
             opetionhtml+='<option value='+json[i]['id']+'>'+json[i]['location']+'</option>';
          }
          $('#ads_location').html(opetionhtml);
      }
  });
}

$(window).on("load", function(){
 var ads_city = '<?php echo $listdata[0]["city_id"]; ?>';
  $.ajax({
      url: "/jobportal/controller/locationdata.php",
      type: 'POST',  
      data: {'city':ads_city},
      success: function (data) {
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select Location </option>';
          for (var i = 0; i < json.length; i++) {
            var selected='';
            if(json[i]['id']=='<?php echo $listdata[0]["ads_location"]; ?>'){

             selected='selected';
            }
            opetionhtml+='<option value='+json[i]['id']+' '+selected+'>'+json[i]['location']+'</option>';
          }
          $('#ads_location').html(opetionhtml);
      }
  });
});

$(window).on("load", function(){
    var ads_city = $("#country_id").val();
     $.ajax({
      url: "/jobportal/controller/citydata.php",
      type: 'POST',  
      data: {'ads_city':ads_city},
      success: function (data) {
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select City </option>';
          for (var i = 0; i < json.length; i++) {
             var selected='';
            if(json[i]['id']=='<?php echo $listdata[0]["city_id"]; ?>'){
             selected='selected';
            }
             opetionhtml+='<option value='+json[i]['id']+' '+selected+'>'+json[i]['city']+'</option>';
          }
          $('#ads_city').html(opetionhtml);
      }
  });
});

function adsdeveloper(){
  var is_listed= $('input[name="is_listed"]:checked').val();
  var other_developer = $("#ads_developer").val();
  if(is_listed=='1'){
    $('#other_developer_div').hide();
    $('#other_developer_div_dev').show();
    $('#ads_property_div2').hide();
    $('#ads_property_div1').show();
    $.ajax({
      url: "/jobportal/controller/propertydata.php",
      type: 'POST',  
      data: {'other_developer':other_developer},
      success: function (data) {
       // console.log(data);
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select Property </option>';
        for (var i = 0; i < json.length; i++) {
        opetionhtml+='<option value='+json[i]['id']+'>'+json[i]['property_name']+'</option>';
        }
        $('#ads_property').html(opetionhtml);
      }
    });
  }
  if(is_listed=='2'){
    $('#other_developer_div').show();
    $('#other_developer_div_dev').hide();
    $('#ads_property_div1').hide();
    $('#ads_property_div2').show();
    $('#ads_property_sec2').html('<input id="ads_property_unlisted" name="ads_property_unlisted" type="text" title="Title" />');
    $('#address').attr('value',' ');
    $('#location').attr('value',' ');
  //  $('#typology').attr('value',' ');
  }

}

function adsproperty(){
  var property_id = $("#ads_property").val();
  $.ajax({
      url: "/jobportal/controller/propertydata.php",
      type: 'POST',  
      data: {'property_id':property_id},
      success: function (data) {
        //console.log(data);
        var json=JSON.parse(data);
       // console.log(json[0]['prop_address']);
        $('#addsction').html('<input id="address" name="address" type="text" title="Title" value="'+json[0]['prop_address']+'"/>');
         $('#ads_location').html('<option value='+json[0]['loc_id']+'>'+json[0]['location']+'</option>');
         $('#ads_city').html('<option value='+json[0]['cty_id']+'>'+json[0]['city']+'</option>');
         $('#property_type').html('<option value='+json[0]['cty_id']+'>'+json[0]['city']+'</option>');
         
         const typologyarray = json[0]['bedrooms'].split(",");
         console.log(typologyarray);

        // $('#configuration_sec').html('<input id="typology" name="typology" type="text" title="Title" value="'+json[0]['bedrooms']+'"/>');
      }
  });
}

$(window).on("load", function(){
  var property_id = '<?php echo $listdata[0]['project_id'];?>';
  $.ajax({
      url: "/jobportal/controller/propertydata.php",
      type: 'POST',  
      data: {'property_id':property_id},
      success: function (data) {
        //console.log(data);
        var json=JSON.parse(data);
        //console.log(json[0]['prop_address']);
        $('#addsction').html('<input id="address" name="address" type="text" title="Title" value="<?php echo($listdata[0]['address'])?>"/>');
     
      }
  });
});

$(window).on("load", function(){
  var is_listed= '<?php echo $listdata[0]['is_listed'];?>';
  var other_developer = $("#ads_developer").val();
  if(is_listed=='1'){
    $('#other_developer_div').hide();
    $('#other_developer_div_dev').show();
    $('#ads_property_div2').hide();
    $('#ads_property_div1').show();
    $.ajax({
      url: "/jobportal/controller/propertydata.php",
      type: 'POST',  
      data: {'other_developer':other_developer},
      success: function (data) {
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select Property </option>';
        for (var i = 0; i < json.length; i++) {
             var selected='';
            if(json[i]['id']=='<?php echo $listdata[0]["project_id"]; ?>'){
              selected='selected';
            }
        opetionhtml+='<option value='+json[i]['id']+' '+selected+'>'+json[i]['property_name']+'</option>';
        }
        $('#ads_property').html(opetionhtml);
      }
    });
  }
  if(is_listed=='2'){
    $('#other_developer_div').show();
    $('#other_developer_div_dev').hide();
    $('#ads_property_div1').hide();
    $('#ads_property_div2').show();
    $('#ads_property_sec2').html('<input id="ads_property_unlisted" name="ads_property_unlisted" type="text" title="Title" value="<?php echo $listdata[0]["project_id"]; ?>" />');
  }
});



$(document).ready(function(){  
    var data = new FormData();
    $('.button').on('click', function(e){  
      e.preventDefault();

        var form_data = $('#datafm').serializeArray();
        $.each(form_data, function (key, input) {
          data.append(input.name, input.value);
        });
   

    var job_highlights = CKEDITOR.instances['job_highlights'].getData();
        data.append("job_highlights", job_highlights);
        var discription = CKEDITOR.instances['discription'].getData();
        data.append("discription", discription);
        data.append('key', 'value');

        $.ajax({
            url: "/jobportal/controller/ajaxdata.php",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
              console.log(data);
              var json=JSON.parse(data);
              if(json.status==0){
                  alert(json.msg);
                  window.location="/jobportal/controller/controller.php?action=joblist";
                }else{
                  alert(json.msg);
                }
            }
        });
    });
});  


function ValidateAuthorImage(event, obj,fname) {
        var files = event.target.files;
        var valid = true;
        var height = 0;
        var width = 0;
        var _URL = window.URL || window.webkitURL;
        for (var i = 0; i < files.length; i++) {
            var img = new Image();
            img.onload = function () {
                height = img.height;
                width = img.width;
                //alert(width + " " + height);
             /*   if(fname=='ads_banner_image'){
                if (width != '1230' || height!='400') {
                    //$("#lblErrorMessageAuthorImage").html("Please upload author image in squire.");
                    alert("Please upload image 1230x400px size");
                    obj.value = "";
                    return false;
                }
               }*/

                if(fname=='ads_image'){
                if (width != '277' || height!='180') {
                    //$("#lblErrorMessageAuthorImage").html("Please upload author image in squire.");
                    alert("Please upload image 277X180px size");
                    obj.value = "";
                    return false;
                }
               }
     
            }
            img.src = _URL.createObjectURL(files[i]);
        }
    }

 function showDiv(select){
   if(select.value==1){
      $('.jobdetail').hide();
      $('.joblink').show();
   } else{
    $('.joblink').hide();
    $('.jobdetail').show();
   }
} 

$(window).on("load", function(){

  var jobtypeview= '<?php echo $listdata[0]['jobview_type']; ?>';
if(jobtypeview==1){
      $('.jobdetail').hide();
      $('.joblink').show();
   } else{
    $('.joblink').hide();
    $('.jobdetail').show();
   }
});

</script>

