<?php 

    include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
    include_once LIBRARY_PATH . "library/server_config_admin.php";
    include_once(LIBRARY_PATH . "jobportal/commonvariable.php");
    include_once(LIBRARY_PATH . "jobportal/model/model.php");

	$modelObj = new Model();

    admin_login();
if(!in_array($_SESSION['login']['role_id'], array('1','22')))
{
header("Location: http://myzone.360realtors.in/login.php");
}


    if($_POST['country_id']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please select country'
        );
        echo json_encode($data);
        die;
    }

    if($_POST['city_id']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please select city'
        );
        echo json_encode($data);
        die;
    }

    if($_POST['title']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the job title'
        );
        echo json_encode($data);
        die;
    }



     if($_POST['min_exp']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the min exp.'
        );
        echo json_encode($data);
        die;
    }

    if($_POST['max_exp']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the max exp.'
        );
        echo json_encode($data);
        die;
    }

     if($_POST['budget']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the budget'
        );
        echo json_encode($data);
        die;
    }
    if($_POST['industry_type']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the industry type'
        );
        echo json_encode($data);
        die;
    }

     if($_POST['jobview_type']==1){
        if($_POST['job_link']==''){
             $data = array(
         'status' => 1,
         'msg'=>'Please enter the job link'
        );
        echo json_encode($data);
        die;
        }
     }


    if($_POST['jobview_type']==2){
         if($_POST['education']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the education'
        );
        echo json_encode($data);
        die;
    }

    if($_POST['job_role']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the job role'
        );
        echo json_encode($data);
        die;
    }




     if($_POST['department']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the department'
        );
        echo json_encode($data);
        die;
    }


    if($_POST['employment_type']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the employment type'
        );
        echo json_encode($data);
        die;
    }

    if($_POST['discription']==''){
        $data = array(
         'status' => 1,
         'msg'=>'Please enter the discription'
        );
        echo json_encode($data);
        die;
    }
    }

   






    if($_POST['post_id']>0 || $_POST['post_id']!=''){

        $arr['id']=$_POST['post_id'];
        $arr['country_id']=$_POST['country_id'];
        $arr['city_id']=$_POST['city_id'];
        $arr['department']=$_POST['department'];
        $arr['title']=$_POST['title'];
        $arr['education']=$_POST['education'];
        $arr['job_role']=$_POST['job_role'];
        $arr['industry_type']=$_POST['industry_type'];
        $arr['employment_type']=$_POST['employment_type'];
        $arr['key_skill']=$_POST['key_skill'];
        $arr['discription']=str_replace(array("\n", "\r","/rn"), '', $_POST['discription']);
        $arr['min_exp']=$_POST['min_exp'];
        $arr['max_exp']=$_POST['max_exp'];
        $arr['budget']=$_POST['budget'];
        $arr['openings']=$_POST['openings'];
        $arr['status']=$_POST['status'];
        $arr['cat_id']=$_POST['cat_id'];
        $arr['modefied_date']=date ("Y-m-d H:i:s");
        $arr['job_highlights']=$_POST['job_highlights'];
        $arr['job_link']=$_POST['job_link'];
        $arr['jobview_type']=$_POST['jobview_type'];
        $arr['apply_on']=$_POST['apply_on'];
        $addprint_ads_data = $modelObj->updatedata($arr);

        $data = array(
         'status' => 0,
         'msg'=>'data inserted sucessfully'
        );
        echo json_encode($data);
        die;

    }else{

        $arr['country_id']=$_POST['country_id'];
        $arr['city_id']=$_POST['city_id'];
        $arr['title']=$_POST['title'];
        $arr['education']=$_POST['education'];
        $arr['department']=$_POST['department'];
        $arr['job_role']=$_POST['job_role'];
        $arr['industry_type']=$_POST['industry_type'];
        $arr['employment_type']=$_POST['employment_type'];
        $arr['key_skill']=$_POST['key_skill'];
        $arr['discription']=str_replace(array("\n", "\r","/rn"), '', $_POST['discription']);
        $arr['min_exp']=$_POST['min_exp'];
        $arr['max_exp']=$_POST['max_exp'];
        $arr['budget']=$_POST['budget'];
        $arr['openings']=$_POST['openings'];
        $arr['status']=$_POST['status'];
        $arr['cat_id']=$_POST['cat_id'];
        $arr['created_date']=date ("Y-m-d H:i:s");
        $arr['job_highlights']=$_POST['job_highlights'];
         $arr['job_link']=$_POST['job_link'];
        $arr['jobview_type']=$_POST['jobview_type'];
        $arr['apply_on']=$_POST['apply_on'];
        $addprint_ads_data = $modelObj->adddata($arr);

        if($addprint_ads_data>0){
            $data = array(
             'status' => 0,
             'msg'=>'data inserted sucessfully'
            );
            echo json_encode($data);
            die;
        }else{
            $data = array(
             'status' => 1,
             'msg'=>'data not inserted sucessfully'
            );
            echo json_encode($data);
            die;
        }

    }

?>
