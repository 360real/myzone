<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "jobportal/commonvariable.php");
include_once(LIBRARY_PATH . "jobportal/model/model.php");


$modelObj = new Model();


admin_login();
if(!in_array($_SESSION['login']['role_id'], array('1','22')))
{
header("Location: http://myzone.360realtors.in/login.php");
} else {

include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('jobs Managemant', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/jobportal/controller/controller.php";


switch ($action) {

    case 'addjob':
    include(ROOT_PATH . "/jobportal/view/addjob.php");
    break;

    case 'editjob':
    $listdata = $modelObj->listdata($_GET['id']);
    include(ROOT_PATH . "/jobportal/view/addjob.php");
    break;

    case 'joblist':
    $listdata = $modelObj->listdata('',$_GET['cat_id'],$_GET['job_title']);
    include(ROOT_PATH . "/jobportal/view/listjob.php");
    break;

    case 'deletejob':
    $deletedata = $modelObj->deletedata($_GET['id']);
    $listdata = $modelObj->listdata('');
    include(ROOT_PATH . "/jobportal/view/listjob.php");
    break;

    case 'applicant':

    $listdata = $modelObj->listuserdata($_GET['jobid'],$_GET['cat_id'],$_GET['job_title']);
    include(ROOT_PATH . "/jobportal/view/applicant.php");
    break;

}


}

?>
