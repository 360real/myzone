<?php

	require_once(ROOT_PATH . "library/mysqli_function.php");

	// this model for making all the query for event
	class Model {

	    private $dbObj;

	    function __construct() {

	        $this->dbObj = new mysqli_function();
	    }


	    public function adddata($arr){
			$columns = implode(", ",array_keys($arr));
			$escaped_values = array_map('mysql_real_escape_string', array_values($arr));
			foreach ($escaped_values as $idx=>$data) $escaped_values[$idx] = "'".$data."'";
			$values  = implode(", ", $escaped_values);
			$sql = "INSERT INTO `tsr_jobportal` ($columns) VALUES ($values)";
			$query = $this->dbObj->insert_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}

	    }

	    public function listdata($id=0,$catid,$jobtitle){
	    	if($id>0){
                $sql = "SELECT * FROM `tsr_jobportal` WHERE `id` = ".$id;
	    	}else{

	    	$cond='';

	    	if($catid!='' || $jobtitle!='' ){
	    		$cond.=' where ';
	    	} 


	    	if($catid!='' && $jobtitle!=''){
	    		$cond.=' cat_id = "'.$catid.'" AND ';
	    	}else if($catid!='' && $jobtitle==''){
	    		$cond.=' cat_id = "'.$catid.'" ';
	    	}

	    	if($jobtitle!='' ){
	    		$cond.=' title LIKE "%'.$jobtitle.'%" ';
	    	}
	    		$sql = "SELECT * FROM `tsr_jobportal` ".$cond." order by id DESC";
	    	}
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	     public function jobcattitle($id=0){
	    	if($id>0){
                $sql = "SELECT title FROM `job_category` WHERE `id` = ".$id;
                $data = $this->dbObj->getAllData($sql);
			return $data;
	    	}
			
	    }

	    public function listuserdata($id=0,$cat_id,$job_title){
            $cond='';
	    	if($cat_id!=''){
	    		$cond.=' cat_id = "'.$cat_id.'" AND ';
	    	}

	    	if($job_title!=''){
	    		$sql = "SELECT id FROM `tsr_jobportal` WHERE `title`  LIKE '%".$job_title."%'";
	    		$data = $this->dbObj->getAllData($sql);
	    		//print_r($data);
	    		$cond.=' Job_id = "'.$data[0]['id'].'" AND ';
	    	}

	    	if($id>0){
              $sql = "SELECT * FROM `tsr_career` WHERE ".$cond." `job_id` = '".$id."' and status=1 order by job_id DESC";
	    	}else{
	    	  $sql = "SELECT * FROM `tsr_career`  WHERE ".$cond." status=1  order by job_id DESC";
	    	}
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }



	    public function deletedata($id){

	    	$sql = "delete from `tsr_jobportal` WHERE `id` = ".$id;
	    	$query =  $this->dbObj->exec_query($sql, '');

	    }

	     public function updatedata($arr){

	        foreach($arr as $field=>$data)
			{
			$update[] = $field.' = \''.$data.'\'';
			}
			$sql = "UPDATE `tsr_jobportal` SET ".implode(', ',$update)." WHERE id='".$arr['id']."'";
			$query = $this->dbObj->update_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}

	    }


	    public function gettermsdata($id,$tablename){
	 
            $sql = "SELECT * FROM ".$tablename." WHERE `id` = ".$id;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	     public function getdeveloper($id){
            $sql = "SELECT `name` FROM tsr_developers WHERE `id` = ".$id;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	     public function getproperty($id){
            $sql = "SELECT `property_name` FROM tsr_properties WHERE `id` = ".$id;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }
	    
	 
	}

 ?>
