<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Template Detail</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Template Type</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_TEMPLATES_TYPES,"template_type_id","id","template_types",$rec["template_type_id"],"id",$site['CMSURL'],"locationDiv","locationLabelDiv")?></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Template Name</strong></td>
  <td><input name="template_name" type="text" title="Template Name" lang='MUST' value="<?php echo($rec['template_name'])?>" size="50%" maxlength="120" /></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>CSS Path</strong></td>
  <td><input name="css_path" type="text" title="CSS Path"  value="<?php echo($rec['css_path'])?>" size="50%" maxlength="120" /></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>JS Path</strong></td>
  <td valign="bottom"><input name="js_path" type="text" title="JS Path"  value="<?php echo($rec['js_path'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Image Path </strong></td>
  <td valign="bottom"><input name="image_path" type="text" title="Image Path"  value="<?php echo($rec['image_path'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($cls1)?>">
    <td align="right" ><strong>Select Template Image</strong></td>
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if($rec['image']=='') {?> title="Select Developer Logo" <?php } ?> name="image" id="image" /></td>
          <td width="62%"><?php if($rec['image']):?>
            <br />
            <img src="<?php echo  ($site['TEMPLATELOGOURL'].$rec['id']."/".$rec['image'])?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['image'])?>" />
            <br /></td>
        </tr>
      </table></td>
  </tr>

<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>


<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>