<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_TEMPLATES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/template/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."template/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."template/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT tep.image, tep.template_type_id, tep.id, tep.template_name, tep.css_path, tep.js_path, tep.image_path, tep.ipaddress, tep.created_date, tep.status, tep.modified_date FROM ".TABLE_TEMPLATES." as tep WHERE ";
		if($_GET["template_id"]!="")
			$sql.="tep.id=".$_GET["template_id"];
		if($_GET["template_name"]!="")
			$sql.=" tep.template_name like '%".trim($_GET["template_name"])."%'";
		if($_GET["status"]!="")
			$sql.="tep.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(tep.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(tep.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(tep.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(tep.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT tep.image, tep.template_type_id, tep.id, tep.template_name, tep.css_path, tep.js_path, tep.image_path, tep.ipaddress, tep.created_date, tep.status, tep.modified_date FROM ".TABLE_TEMPLATES." as tep ";
		
		$sql.= " WHERE tep.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	else if($_GET["action"]=="Delete") {
	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		$obj_common->redirect($page_name."?msg=delete");
	}
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=10;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	else
	{
	$sql = "SELECT tep.image, tep.template_type_id, tep.id, tep.template_name, tep.css_path, tep.js_path, tep.image_path, tep.ipaddress, tep.created_date, tep.status, tep.modified_date FROM ".TABLE_TEMPLATES." as tep" ;
		
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";	
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*print "<pre>";
	print_r($rec);
	print "<pre>";*/
	//echo $sql;
	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			if($obj_mysql->isDupUpdate($tblName,'template_name', $arr['template_name'] ,'id',$rec_id)){

			echo "ddd-".$_FILES['image']['name'];
			if($_FILES['image']['name']!="")
			{   
				
				@unlink(UPLOAD_PATH_TEMPLATE_IMAGE.$rec_id."/".$_POST['old_file_name']);
				$handle = new upload($_FILES['image']);
				if ($handle->uploaded)
				{	
					$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_TEMPLATE_IMAGE.$rec_id."/");
					
					if ($handle->processed) {
						//echo 'image resized';
						$arr['image'] = $handle->file_dst_name;
						$handle->clean();
				   } else {
					   //echo "error";
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				$obj_common->redirect($page_name."?msg=update");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}else{

			$sql = mysql_query("SELECT MAX(id) as maxId FROM tsr_templates");
			$recId = mysql_fetch_array($sql);
			$maxId = $recId["maxId"];
			if($maxId!="")
			$setmaxid = $maxId+1;
			else
			$setmaxid = 1;

			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			if($obj_mysql->isDuplicate($tblName,'template_name', $arr['template_name'])){

			if($_FILES['image']['name']!="")
			{
			echo $_FILES['image']['name'];
				$handle = new upload($_FILES['image']);
				if ($handle->uploaded)
				{
				
					$handle->image_resize         = true;
					$handle->image_x              = 200;
					$handle->image_y              = 87;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
					$handle->process(UPLOAD_PATH_TEMPLATE_IMAGE.$setmaxid."/");
					if ($handle->processed) {
						//echo 'image resized';
						$arr['image'] = $handle->file_dst_name;
						@unlink(UPLOAD_PATH_TEMPLATE_IMAGE.$setmaxid."/".$_POST['old_file_name']);
					$handle->clean();
					} else {
						echo 'error : ' . $handle->error;
					}
				}
				 
			}

				$obj_mysql->insert_data($tblName,$arr);
				$obj_common->redirect($page_name."?msg=insert");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="title_a">'.$rec[$i]['template_name'].'</a></td>
					<td class="left">'.($rec[$i]['css_path']).'</td>
					<td class="left">'.($rec[$i]['js_path']).'</td>
					<td class="center">'.$rec[$i]['image_path'].'</td>
					<td class="center">'.$rec[$i]['modified_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td class="center">'.$st.'</td>
					<td > <a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'" target="_blank" class="edit">Edit</a>
					<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'" class="delete" onclick="return conf()">Delete</a>
					</td>
					
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Templates', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
