
<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $site['URL']?>view/js/news/jquery-2.0.3.min.js?>"></script>
<script>
function richTextBox($fieldname, $value = "") 
{
  $CKEditor = new CKEditor();
  $config['toolbar'] = array(
  array( 'Bold', 'Italic', 'Underline', 'Strike'),
  array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
  array('Format'),
  array( 'Font', 'FontSize', 'FontColor', 'TextColor'),
  array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ),
  array('Image', 'Table', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'),
  array('Subscript','Superscript'),
  array( 'Link', 'Unlink' ),
  array('Source')
  );
  $CKEditor->basePath = '/ckeditor/';
  $CKEditor->config['width'] = 975;
  $CKEditor->config['height'] = 400;
  $CKEditor->editor($fieldname, $value, $config);
}
</script>

<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">

  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
    <thead>
      <tr>
        <th colspan="2" align="left">
          <h3>Question Detail</h3>
        </th>
      </tr>
    </thead>

    <tbody>

      <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Question</strong></td>
        <td><input name="question_name" type="text"  lang='MUST' title="Question" value="<?php echo($rec['question_name'])?>" size="178%" maxlength="%" /></td>
      </tr>
      
      <?php
        if($_GET["action"]=="Update")
        {
          $sqlAnswers = "SELECT * FROM ".TABLE_ANSWERS." WHERE q_id=".$_GET['id'];
          $recAnswers=($obj_mysql->getAllData($sqlAnswers));
          //print_r($recAnswers);die;
          $countAnswer = count($recAnswers);
          $initialCount = 1;
          foreach($recAnswers as $answer)
          {
            ?>
              <tr class="<?php echo($clsRow1)?>">
                <td align="right" >
                  <strong>Answer  <?php echo $initialCount;?></strong>
                  <input type="hidden" name="answerId[]" value="<?=$answer['id']?>">
                </td>
                <td valign="top">
                  <textarea name="answer[]" rows="1" cols="175"><?php echo $answer['answer'];?></textarea>
                </td>
              </tr>
            <?php
            $initialCount++;
          }
        }
        else
        {
          for($i=0;$i<4;$i++)
          {
            ?>
            <div id="addAnswer">
              <tr class="<?php echo($clsRow1)?>">
                <td align="right" >
                  <strong>Answer <?php echo $i+1?></strong>
                  <input type="hidden" name="answerId[]">
                </td>
                <td valign="top">
                  <textarea  name="answer[]" rows="1" cols="175" ></textarea>
                </td>
              </tr>
              </div>
            <?php 
          }
        }
      ?>

      <tr>
        <button style="margin-left:1128px;margin-top:77px; position: absolute;" class="add" onclick="addMoreRows(); return false;"></button>
      </tr>
      
      <tr class="clsRow1uuu" id="newRows"></tr>

      <tr class="<?php echo($clsRow1)?>">
        <td align="right" >
          <strong>Status</strong>
        </td>
      	<td valign="bottom">
        	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
        	<label for="1">Active</label> 
        	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
        	<label for="0">Inactive</label>
      	</td>
      </tr>

      <tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
        <td align="left">
          <input name="submit" type="submit" class="button" value="Submit" />
          <input name="reset" type="reset" class="button" value="Reset" />
          <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" />
        </td>
      </tr>
    </tbody>
  </table>
</form>

<?php 
if($action=="Update")
{

  $count=$countAnswer+1;
}
else
{
  $count=5;
}
?>

<script>
  var rowcount = <?php echo $count?>;
  function addMoreRows() 
  {
    //alert('dfsdf');return false;
    var moreAnswer = ('<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Answer '+rowcount+'</strong></td><td valign="top"><textarea  name="answer[]" rows="1" cols="175"></textarea><button onclick="removeRow(this); return false;" class="delete-answer"></button></td></tr>');
    $('#newRows').before(moreAnswer);
    rowcount++;
  }

  function removeRow(removeNum)
  {
    var test=$(removeNum).parent().parent();
    $(test).remove();
  }
</script>