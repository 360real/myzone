<?php 
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Property Name already in record!");
		
	$tblName	=	TABLE_COMMERCIAL_PROPERTIES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/commercialimage/controller/controller.php';
	//$page_name_insert  =	C_ROOT_URL.'/price/controller/controller.php';
	$page_name_update = '';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";


	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."commercialimage/model/model.php";
			break;	
		default:
			$file_name = ROOT_PATH."commercialimage/model/model.php";
			break;
	}

	
	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		
		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES UPDATE Code Block Start Here
		/*--------------------------------------------------------------  */
		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];

			$newPath = UPLOAD_PATH_PROPERTY_IMAGE.$_POST["property_name"];
			$oldPath = UPLOAD_PATH_PROPERTY_IMAGE.$_POST["existingPropertyName"];

			
				echo "sss-".UPLOAD_PATH_PROPERTY_IMAGE;
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */
				for($counterImages=0;$counterImages<4;$counterImages++){
				
				if($_FILES["propertyImage".$counterImages]["name"]!="" )
				{
				
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/mini/".$_POST['old_file_name_propertyImage'.$counterImages]);	
				$arrayPropertyImage["img_type"] = "MINI_PROPERTY_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
				$arrayPropertyImage["alt_title"]=$_POST['propertyImageTitle'.$counterImages];
								

				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				$handle = new upload($_FILES["propertyImage".$counterImages]);
				if ($handle->uploaded)
				{
					/*$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;*/
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/mini/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
			
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_propertyImage'.$counterImages];
				
				}
				
			
				}
				else if(trim($_POST['propertyImageTitle'.$counterImages])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
					$arrayPropertyImage["alt_title"]=$_POST['propertyImageTitle'.$counterImages];

				}
								
				

				
				if($arrayPropertyImage["imageId"]=="" && $_FILES["propertyImage".$counterImages]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				 $obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}
		
		
				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */
				


				/* CODE FOR PROPERTY CONSTRUCTION IMAGES UPDATE BLOCK */
				for($counterImages=0;$counterImages<50;$counterImages++){
				if($_FILES["constructionImage".$counterImages]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/construction/".$_POST['old_file_name_constructionImage'.$counterImages]);	
				$arrayPropertyImage["img_type"] = "CONSTRUCTION_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageconstId"][$counterImages];
				$arrayPropertyImage["alt_title"]=$_POST['propertyConstructionImageTitle'.$counterImages];

				if($_FILES["constructionImage".$counterImages]["name"]!="")
				{
				$handle = new upload($_FILES["constructionImage".$counterImages]);
				if ($handle->uploaded)
				{
					$handle->image_resize         = true;
					$handle->image_x              = 1300;
					$handle->image_y              = 1637;
					$handle->image_ratio_y        = true;
					$handle->file_safe_name = true;
		
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/construction/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_constructionImage'.$counterImages];
				}
				}

				else if(trim($_POST['propertyConstructionImageTitle'.$counterImages])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["imageconstId"][$counterImages];
					$arrayPropertyImage["alt_title"]=$_POST['propertyConstructionImageTitle'.$counterImages];
					echo $arrayPropertyImage["imageId"];
					echo "<br/>".$arrayPropertyImage["alt_title"];
					
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["constructionImage".$counterImages]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
				}
		
		
				/* CODE FOR PROPERTY CONSTRUCTION IMAGES UPDATE BLOCK */


				

				/* CODE FOR PROPERTY MASTER PLAN IMAGES UPDATE BLOCK */
				if($_FILES["masterPlan"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/masterplan/".$_POST['old_file_name_masterPlan']);
				$arrayPropertyImage["img_type"] = "MASTERPLAN_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["masterPlanImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyMasterPlanImageTitle'];
				if($_FILES["masterPlan"]["name"]!="")
				{
				$handle = new upload($_FILES["masterPlan"]);
				if ($handle->uploaded)
				{
				$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/masterplan/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_masterPlan'];
				}
				}
				else if(trim($_POST['propertyMasterPlanImageTitle'])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["masterPlanImageId"];
					$arrayPropertyImage["alt_title"]=$_POST['propertyMasterPlanImageTitle'.$counterImages];
				}
				


				if($arrayPropertyImage["imageId"]=="" && $_FILES["masterPlan"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY MASTER PLAN IMAGES UPDATE BLOCK */


				
				/* CODE FOR PROPERTY LOCATION MAP IMAGES UPDATE BLOCK */
				if($_FILES["locationMap"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/locationmap/".$_POST['old_file_name_locationMap']);
				$arrayPropertyImage["img_type"] = "LOCATIONMAP_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["locationMapImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyLocationMapImageTitle'];

				if($_FILES["locationMap"]["name"]!="")
				{
				$handle = new upload($_FILES["locationMap"]);
				if ($handle->uploaded)
				{
				/*$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;*/
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/locationmap/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_locationMap'];
				}
				}
				else if(trim($_POST['propertyLocationMapImageTitle'])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["locationMapImageId"];
					$arrayPropertyImage["alt_title"]=$_POST['propertyLocationMapImageTitle'];
				}
				

				if($arrayPropertyImage["imageId"]=="" && $_FILES["locationMap"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY LOCATION MAP IMAGES UPDATE BLOCK */

				/* CODE FOR PROPERTY SITE PLAN IMAGES UPDATE BLOCK */
				if($_FILES["sitePlan"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/siteplan/".$_POST['old_file_name_sitePlan']);
				$arrayPropertyImage["img_type"] = "SITEPLAN_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["sitePlanImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertySitePlanImageTitle'];

				if($_FILES["sitePlan"]["name"]!="")
				{
				$handle = new upload($_FILES["sitePlan"]);
				if ($handle->uploaded)
				{
				/*$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;*/
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/siteplan/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_sitePlan'];
				}
				}
				else if(trim($_POST['propertySitePlanImageTitle'])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["sitePlanImageId"];
					$arrayPropertyImage["alt_title"]=$_POST['propertySitePlanImageTitle'];
				}
				

				if($arrayPropertyImage["imageId"]=="" && $_FILES["sitePlan"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY SITE PLAN IMAGES UPDATE BLOCK */



				/* CODE FOR PROPERTY PAYMENT PLAN IMAGES UPDATE BLOCK */
				if($_FILES["paymentPlan"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/paymentplan/".$_POST['old_file_name_paymentplan']);
				$arrayPropertyImage["img_type"] = "PAYMENTPLAN_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["paymentPlanImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyPaymentPlanImageTitle'];
				if($_FILES["paymentPlan"]["name"]!="")
				{
				$handle = new upload($_FILES["paymentPlan"]);
				if ($handle->uploaded)
				{
				$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/paymentplan/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_paymentplan'];
				}
				}
				else if(trim($_POST['propertyPaymentPlanImageTitle'])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["paymentPlanImageId"];
					$arrayPropertyImage["alt_title"]=$_POST['propertyPaymentPlanImageTitle'];
				}
				

				if($arrayPropertyImage["imageId"]=="" && $_FILES["paymentPlan"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY PAYMENT PLAN IMAGES UPDATE BLOCK */



				/* CODE FOR PROPERTY SPECIFICATION IMAGES UPDATE BLOCK */
				if($_FILES["specification"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/specification/".$_POST['old_file_name_specification']);
				$arrayPropertyImage["img_type"] = "SPECIFICATION_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["specificationImageId"];
					$arrayPropertyImage["alt_title"]=$_POST['propertySpecificationImageTitle'.$counterImages];
				if($_FILES["specification"]["name"]!="")
				{
				$handle = new upload($_FILES["specification"]);
				if ($handle->uploaded)
				{
				$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/specification/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_specification'];
				}
				}
				else if(trim($_POST['propertySpecificationImageTitle'])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["specificationImageId"];
					$arrayPropertyImage["alt_title"]=$_POST['propertySpecificationImageTitle'];
				}
				


				if($arrayPropertyImage["imageId"]=="" && $_FILES["specification"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);

				/* CODE FOR PROPERTY SPECIFICATION IMAGES UPDATE BLOCK */


				/* CODE FOR PROPERTY BOOKING PROCEDURE UPDATE BLOCK */
				if($_FILES["bookingProcedure"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/bookingprocedure/".$_POST['old_file_name_bookingprocedure']);
				$arrayPropertyImage["img_type"] = "BOOKING_PROCEDURE_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["bookingProcedureImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyBookingProcedureImageTitle'.$counterImages];
				if($_FILES["bookingProcedure"]["name"]!="")
				{
				$handle = new upload($_FILES["bookingProcedure"]);
				if ($handle->uploaded)
				{
				/*$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;*/
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_id']."/bookingprocedure/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_bookingprocedure'];
				}
				}
				else if(trim($_POST['propertyBookingProcedureImageTitle'])!="")
				{
					$arrayPropertyImage["imageId"]=$_POST["bookingProcedureImageId"];
					$arrayPropertyImage["alt_title"]=$_POST['propertyBookingProcedureImageTitle'];
				}
				


				if($arrayPropertyImage["imageId"]=="" && $_FILES["bookingProcedure"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);

				/* CODE FOR PROPERTY BOOKING PROCEDURE UPDATE BLOCK */



				/* CODE FOR PROPERTY IMAGES UPDATE BLOCK */

				$obj_common->redirect($page_name."?action=search&pid=".$rec_id."");	
			
		}
		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES UPDATE Code Block END Here
		/*--------------------------------------------------------------  */
		

		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES INSERT Code Block START Here
		/*--------------------------------------------------------------  */
		
		else{
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			
			if($obj_mysql->isDuplicate($tblName,'property_name', $arr['property_name'])){
				$rec_id = $obj_mysql->insert_data($tblName,$arr);
					
						
				/* CODE FOR PROPERTY IMAGES INSERT BLOCK */
				for($counterImages=0;$counterImages<4;$counterImages++){
				if($_FILES["propertyImage".$counterImages]["name"]!="")
				{
				$arrayPropertyImage["img_type"] = "MINI_PROPERTY_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageId"][$counterImages];
				$arrayPropertyImage["alt_title"]=$_POST['propertyImageTitle'.$counterImages];

							
				$handle = new upload($_FILES["propertyImage".$counterImages]);
				if ($handle->uploaded)
				{
				/*$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;*/
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/mini/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/mini/".$_POST['old_file_name_propertyImage'.$counterImages]);
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_propertyImage'.$counterImages];
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["propertyImage".$counterImages]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			}

				/* CODE FOR PROPERTY IMAGES INSERT BLOCK */




				/* CODE FOR PROPERTY CONSTRUCTION IMAGES INSERT BLOCK */

				for($counterImages=0;$counterImages<30;$counterImages++){
				if($_FILES["constructionImage".$counterImages]["name"]!="")
				{
				$arrayPropertyImage["img_type"] = "CONSTRUCTION_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["imageconstId"][$counterImages];
				$arrayPropertyImage["alt_title"]=$_POST['propertyConstructionImageTitle'.$counterImages];
				
				$handle = new upload($_FILES["constructionImage".$counterImages]);
				if ($handle->uploaded)
				{
				$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/construction/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/construction/".$_POST['old_file_name_constructionImage'.$counterImages]);
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_constructionImage'.$counterImages];
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["constructionImage".$counterImages]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			}

				/* CODE FOR PROPERTY CONSTRUCTION IMAGES INSERT BLOCK */
				


				/* CODE FOR PROPERTY MASTER PLAN IMAGES UPDATE BLOCK */
				if($_FILES["masterPlan"]["name"]!="")
				{
				$arrayPropertyImage["img_type"] = "MASTERPLAN_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["masterPlanImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyMasterPlanImageTitle'.$counterImages];
				if($_FILES["masterPlan"]["name"]!="")
				{
				$handle = new upload($_FILES["masterPlan"]);
				if ($handle->uploaded)
				{
				$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/masterplan/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/masterplan/".$_POST['old_file_name_masterPlan']);
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_masterPlan'];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["masterPlan"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY MASTER PLAN IMAGES UPDATE BLOCK */


				
				/* CODE FOR PROPERTY LOCATION MAP IMAGES UPDATE BLOCK */
				if($_FILES["locationMap"]["name"]!="")
				{
				$arrayPropertyImage["img_type"] = "LOCATIONMAP_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["locationMapImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyLocationMapImageTitle'.$counterImages];
				if($_FILES["locationMap"]["name"]!="")
				{
				$handle = new upload($_FILES["locationMap"]);
				if ($handle->uploaded)
				{
				/*$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;*/
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/masterplan/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/masterplan/".$_POST['old_file_name_locationMap']);
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_locationMap'];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["locationMap"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY LOCATION MAP IMAGES UPDATE BLOCK */

				/* CODE FOR PROPERTY SITE PLAN IMAGES UPDATE BLOCK */
				if($_FILES["sitePlan"]["name"]!="")
				{
				$arrayPropertyImage["img_type"] = "SITEPLAN_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["sitePlanImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertySitePlanImageTitle'.$counterImages];
				if($_FILES["sitePlan"]["name"]!="")
				{
				$handle = new upload($_FILES["sitePlan"]);
				if ($handle->uploaded)
				{
				/*$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;*/
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/locationmap/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/locationmap/".$_POST['old_file_name_sitePlan']);
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_sitePlan'];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["sitePlan"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY SITE PLAN IMAGES UPDATE BLOCK */




				/* CODE FOR PROPERTY PAYMENT PLAN IMAGES UPDATE BLOCK */
				if($_FILES["paymentPlan"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/paymentplan/".$_POST['old_file_name_paymentplan']);
				$arrayPropertyImage["img_type"] = "PAYMENTPLAN_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["paymentPlanImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyPaymentPlanImageTitle'.$counterImages];
				if($_FILES["paymentPlan"]["name"]!="")
				{
				$handle = new upload($_FILES["paymentPlan"]);
				if ($handle->uploaded)
				{
				$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/paymentplan/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_paymentplan'];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["paymentPlan"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);
			

				/* CODE FOR PROPERTY PAYMENT PLAN IMAGES UPDATE BLOCK */


				/* CODE FOR PROPERTY SPECIFICATION IMAGES UPDATE BLOCK */
				if($_FILES["specification"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/specification/".$_POST['old_file_name_specification']);
				$arrayPropertyImage["img_type"] = "SPECIFICATION_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["specificationImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertySpecificationImageTitle'.$counterImages];
				if($_FILES["specification"]["name"]!="")
				{
				$handle = new upload($_FILES["specification"]);
				if ($handle->uploaded)
				{
				$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/specification/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_specification'];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["specification"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);

				/* CODE FOR PROPERTY SPECIFICATION IMAGES UPDATE BLOCK */



				/* CODE FOR PROPERTY BOOKING PROCEDURE UPDATE BLOCK */
				if($_FILES["bookingProcedure"]["name"]!="")
				{
				@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/bookingprocedure/".$_POST['old_file_name_bookingprocedure']);
				$arrayPropertyImage["img_type"] = "BOOKING_PROCEDURE_IMAGE"; 
				$arrayPropertyImage["propty_id"] = $rec_id; 
				$arrayPropertyImage["imageId"]=$_POST["bookingProcedureImageId"];
				$arrayPropertyImage["alt_title"]=$_POST['propertyBookingProcedureImageTitle'.$counterImages];
				if($_FILES["bookingProcedure"]["name"]!="")
				{
				$handle = new upload($_FILES["bookingProcedure"]);
				if ($handle->uploaded)
				{
				/*$handle->image_resize         = true;
				$handle->image_x              = 1300;
				$handle->image_y              = 1637;
				$handle->image_ratio_y        = true;*/
				$handle->file_safe_name = true;
				$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$arr['property_name']."/bookingprocedure/");
				if ($handle->processed) {
				//echo 'image resized';
				$arrayPropertyImage["image"] = $handle->file_dst_name;
				$handle->clean();
				} else {
				echo 'error : ' . $handle->error;
				}
				}

				}
				else
				{
				$arrayPropertyImage["image"]=$_POST['old_file_name_bookingprocedure'];
				}
				}

				if($arrayPropertyImage["imageId"]=="" && $_FILES["bookingProcedure"]["name"]!="")
				$obj_mysql->insert_data(TABLE_IMAGES,$arrayPropertyImage);
				else
				$obj_mysql->update_data(TABLE_IMAGES,$fld_id,$arrayPropertyImage,$arrayPropertyImage["imageId"]);

				/* CODE FOR PROPERTY BOOKING PROCEDURE UPDATE BLOCK */
				/* CODE FOR PROPERTY PRICES INSERT BLOCK */
			
				$obj_common->redirect($page_name_insert."?msg=insert&id=".$rec_id."");	
			}else{
				$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}

	/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES INSERT Code Block END Here
	/*--------------------------------------------------------------  */
	}	
?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Property', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
 </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
