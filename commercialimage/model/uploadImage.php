<?php
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
include_once ("../../library/server_config_admin.php");
$action = $_GET['action'];

if($action=="upload")
{

$ipaddress = $regn_ip;

$property_name = $_POST['property_name'];
$property_id = $_POST['property_id'];
$loc_id = $_POST['loc_id'];

$sqlLocation = "SELECT location FROM ".TABLE_LOCATIONS." WHERE id=".$loc_id;
$recLocation=($obj_mysql->getAllData($sqlLocation));
$location = $recLocation[0]['location'];


$image_full_name = UPLOAD_PATH_COMMERCIAL_PROPERTY_IMAGE;

$image_category = $_POST['image_category'];
$img_type = $_POST['img_type'];
if(!empty(array_filter($_FILES["imageFile"]['name']))){

	$countfiles = count($_FILES['imageFile']['name']);
 	for($i=0;$i<$countfiles;$i++){
   		$file_name = $_FILES['imageFile']['name'][$i];
		
		$property_id_dir = $image_full_name.$property_id."/";
    	$image_category_dir = $property_id_dir.$image_category."/";
    		if (!file_exists($property_id_dir)) {
		        mkdir($property_type_dir, 0777, true);
		    }
		    

		    if (!file_exists($image_category_dir)) {
		        	mkdir($image_category_dir, 0777, true);
		    }		    	
	    

		    // count image number
		    $sqlImageCount = "SELECT count(*) as tot FROM ".TABLE_COMMERCIAL_IMAGE." WHERE property_id=".$property_id;
			$recImageCount=($obj_mysql->getAllData($sqlImageCount));
			$count = $recImageCount[0]['tot'];
			$count++;
			

			$ext = pathinfo($file_name, PATHINFO_EXTENSION);
		    $image_name_to_upload = $property_name."_".$location."_".$count.".".$ext;
	    	$image_name_to_upload = str_replace(" ","_",$image_name_to_upload);
		    	// image resize and upload code

        		$file = $_FILES["imageFile"]["tmp_name"][$i]; 
		    	$sourceProperties = getimagesize($file);
		    	$ext = pathinfo($file_name, PATHINFO_EXTENSION);
		    	$imageType = $sourceProperties[2];

		    	if($image_category!="others_image")
		    	{

		        switch ($imageType) {


		            case IMAGETYPE_PNG:
		                $imageResourceId = imagecreatefrompng($file); 
		                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
		                imagepng($targetLayer,$image_category_dir. $image_name_to_upload. $ext);
		                break;


		            case IMAGETYPE_GIF:
		                $imageResourceId = imagecreatefromgif($file); 
		                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
		                imagegif($targetLayer,$image_category_dir. $image_name_to_upload. $ext);
		                break;


		            case IMAGETYPE_JPEG:
		                $imageResourceId = imagecreatefromjpeg($file); 
		                $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
		                imagejpeg($targetLayer,$image_category_dir. $image_name_to_upload. $ext);
		                break;


		            default:
		                break;
		        	}
		    	}

		        if(move_uploaded_file($file, $image_category_dir.$image_name_to_upload))
		        {
		        	$SQL_INSERT_IMAGE = "INSERT INTO ".TABLE_COMMERCIAL_IMAGE." (property_id,image_url,image_category,img_type,status,ipaddress) VALUES('".$property_id."','".$image_name_to_upload."','".$image_category."','".$img_type."','1','".$ipaddress."')";
		        	$obj_mysql->exec_query($SQL_INSERT_IMAGE);
		        	echo "INSERTED";
		        }
		        else
		        {
		        	echo $_FILES["imageFile"]["error"][$i];
		        }
				
	       /*
		        echo " 1 ";
		    if($_FILES["imageFile"]["name"][$i]!="")
			{
		        echo " 2 ";
					$handle = new upload($_FILES["imageFile"]["tmp_name"][$i]);
					if ($handle->uploaded)
					{
		        		echo " 3 ";
						$handle->image_resize         = true;
						$handle->image_x              = 1300;
						$handle->image_y              = 1637;
						$handle->image_ratio_y        = true;
						$handle->file_safe_name = true;
					$handle->process($image_category_dir);
					if ($handle->processed) {
					echo ' image resized ' . $handle->file_dst_name;
				
					
					$handle->clean();
					} else {
					echo 'error : ' . $handle->error;
					}
				}

			}

	    		$image_name_to_upload_url = $property_name."_".$location."_".time()."_".$file_name;
	    		$image_name_to_upload_url = str_replace(" ","_",$image_name_to_upload_url);

				$SQL_INSERT_IMAGE = "INSERT INTO ".TABLE_COMMERCIAL_IMAGE." (property_id,image_url,image_category,img_type,status,ipaddress) VALUES('".$property_id."','".$image_name_to_upload_url."','".$image_category."','".$img_type."','1','".$ipaddress."')";
		        	$obj_mysql->exec_query($SQL_INSERT_IMAGE);
		        	echo "INSERTED";
			*/
   	
   	}
}

}
else if($action == "delete")
{
	$id = $_POST['id'];
	$sqlDelete = "UPDATE ".TABLE_COMMERCIAL_IMAGE." set status='0' where id=".$id; 
	$obj_mysql->update_query($sqlDelete);
	$sqlDelete = "UPDATE ".TABLE_COMMERCIAL_IMAGE." set modified_date=current_timestamp where id=".$id; 
	$obj_mysql->update_query($sqlDelete);
	echo "deleted";
}


function imageResize($imageResourceId,$width,$height) {
	$targetWidth =1300;
    $targetHeight =1637;
    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
    return $targetLayer;
}


?>