<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Meta Detail</h3></th></tr>
</thead>
<tbody>

<input type="hidden" name="user_id" value="<?php echo $_SESSION['login']['id']?>">

<input type="hidden" name="id" id="file_sub_id" value="<?php echo $_GET["id"]?>">

<tr class="<?php echo($clsRow1)?>" id="setCloseType">
<td align="right" ><strong>Microsite Name</strong></td>
  <td valign="bottom"><?php echo AjaxDropDownList(TABLE_MICROSITES,"microsite_id","id","site_name",$_GET["microsite_id"],"id",$site['CMSURL'],"locationDiv","locationLabelDiv")?></td>
</tr>


<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Site Map Meta Title</strong></td>
<td><textarea  name="site_map_meta_title" id="site_map_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['site_map_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Site Map Meta Keywords</strong></td>
<td><textarea  name="site_map_meta_keywords" id="site_map_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['site_map_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Site Map Meta Description</strong></td>
<td><textarea  name="site_map_meta_description" id="site_map_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['site_map_meta_description'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Contact Us Meta Title</strong></td>
<td><textarea  name="contact_us_meta_title" id="contact_us_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['contact_us_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Contact Us Meta Keywords</strong></td>
<td><textarea  name="contact_us_meta_keywords" id="contact_us_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['contact_us_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Contact Us Meta Description</strong></td>
<td><textarea  name="contact_us_meta_description" id="contact_us_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['contact_us_meta_description'])?></textarea></td>
</tr>


<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>EMI Meta Title</strong></td>
<td><textarea  name="emi_meta_title" id="emi_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['emi_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>EMI Meta Keywords</strong></td>
<td><textarea  name="emi_meta_keywords" id="emi_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['emi_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>EMI Meta Description</strong></td>
<td><textarea  name="emi_meta_description" id="emi_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['emi_meta_description'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Floor Plan Meta Title</strong></td>
<td><textarea  name="floor_plan_meta_title" id="floor_plan_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['floor_plan_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Floor Plan Meta Keywords</strong></td>
<td><textarea  name="floor_plan_meta_keywords" id="floor_plan_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['floor_plan_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Floor Plan Meta Description</strong></td>
<td><textarea  name="floor_plan_meta_description" id="floor_plan_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['floor_plan_meta_description'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Location Map Meta Title</strong></td>
<td><textarea  name="location_map_meta_title" id="location_map_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['location_map_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Location Map Meta Keywords</strong></td>
<td><textarea  name="location_map_meta_keywords" id="location_map_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['location_map_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Location Map Meta Description</strong></td>
<td><textarea  name="location_map_meta_description" id="location_map_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['location_map_meta_description'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Payment Plan Meta Title</strong></td>
<td><textarea  name="payment_plan_meta_title" id="payment_plan_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['payment_plan_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Payment Plan Meta Keywords</strong></td>
<td><textarea  name="payment_plan_meta_keywords" id="payment_plan_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['payment_plan_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Payment Plan Meta Description</strong></td>
<td><textarea  name="payment_plan_meta_description" id="payment_plan_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['payment_plan_meta_description'])?></textarea></td>
</tr>


<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Price List Meta Title</strong></td>
<td><textarea  name="price_list_meta_title" id="price_list_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['price_list_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Price List Meta Keywords</strong></td>
<td><textarea  name="price_list_meta_keywords" id="price_list_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['price_list_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Price List Meta Description</strong></td>
<td><textarea  name="price_list_meta_description" id="price_list_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['price_list_meta_description'])?></textarea></td>
</tr>


<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Amenities Meta Title</strong></td>
<td><textarea  name="amenities_meta_title" id="amenities_meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['amenities_meta_title'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Amenities Meta Keywords</strong></td>
<td><textarea  name="amenities_meta_keywords" id="amenities_meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['amenities_meta_keywords'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
<td width="15%" align="right"><strong>Amenities Meta Description</strong></td>
<td><textarea  name="amenities_meta_description" id="amenities_meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['amenities_meta_description'])?></textarea></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Site Map Overview</strong></td>
  <td><textarea name="site_map_overview" class="ckeditor"  id="site_map_overview" rows="8" cols="70"><?php echo($rec['site_map_overview'])?></textarea>  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Contact Us Overview</strong></td>
  <td><textarea name="contact_us_overview" class="ckeditor"  id="contact_us_overview" rows="8" cols="70"><?php echo($rec['contact_us_overview'])?></textarea>  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>EMI Overview</strong></td>
  <td><textarea name="emi_overview" class="ckeditor"  id="emi_overview" rows="8" cols="70"><?php echo($rec['emi_overview'])?></textarea>  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Floor Plan Overview</strong></td>
  <td><textarea name="floor_plan_overview" class="ckeditor"  id="floor_plan_overview" rows="8" cols="70"><?php echo($rec['floor_plan_overview'])?></textarea>  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Location Map Overview</strong></td>
  <td><textarea name="location_map_overview" class="ckeditor"  id="location_map_overview" rows="8" cols="70"><?php echo($rec['location_map_overview'])?></textarea>  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Payment Plan Overview</strong></td>
  <td><textarea name="payment_plan_overview" class="ckeditor"  id="payment_plan_overview" rows="8" cols="70"><?php echo($rec['payment_plan_overview'])?></textarea>  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Price List Overview</strong></td>
  <td><textarea name="price_list_overview" class="ckeditor"  id="price_list_overview" rows="8" cols="70"><?php echo($rec['price_list_overview'])?></textarea>  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right" ><strong>Amenities Overview</strong></td>
  <td><textarea name="amenities_overview" class="ckeditor"  id="amenities_overview" rows="8" cols="70"><?php echo($rec['amenities_overview'])?></textarea>  </td>
</tr>


<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>


<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>

<script>

function showWebsiteMicrosite()
{
	
if(document.getElementById("file_sub_id").value!="")
	{
document.getElementById("setOpenType").style.display="";
document.getElementById("setCloseType").style.display="none";
	}

var fileId = document.getElementById("template_id").value;

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("type_name").innerHTML=xmlhttp.responseText;
}

}

xmlhttp.open("GET","http://myzone.360realtors.in/meta/showWebsiteMicrositeByType.php?tid="+fileId,true);
xmlhttp.send();
return false;
}



function showSiteByCategoryId()
{
var fileId = document.getElementById("site_cat_id").value;
var typeId = document.getElementById("type_id").value;
if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("site_name_id").innerHTML=xmlhttp.responseText;
}

}

xmlhttp.open("GET","http://localhost/aijare/showSubmissionWebsiteName.php?cid="+fileId+"&tid="+typeId,true);
xmlhttp.send();
return false;
}





function showCat()
{
var fileId = document.getElementById("type_id").value;

if (window.XMLHttpRequest)
{// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("cat_name").innerHTML=xmlhttp.responseText;
}

}
xmlhttp.open("GET","http://myzone.360realtors.in/bp_fum/showCatName.php?fid="+fileId,true);
xmlhttp.send();
return false;
}
</script>