<form  method="post"  name="form123" onsubmit="return validateForm(this);">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>Category Manager</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Type</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_TYPES,"type_id","id","type",$rec["type_id"],"id",$site['CMSURL'],"locationDiv","locationLabelDiv")?></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Category Name</strong></td>
  <td><input name="type_category" type="text" title="Type Category" lang='MUST' value="<?php echo($rec['type_category'])?>" size="50%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Description</strong></td>
  <td><textarea class="ckeditor" name="description"  id="description" rows="8" cols="70" title="Residential Content" ><?php echo $rec['description']?></textarea>
</tr>

  <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
           <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
           <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
 </tr>


<tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
           <td><textarea  name="meta_description" id="meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
 </tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Category Ordering</strong></td>
  <td><input name="ordering" autocomplete="off" id="ordering" type="text"  value="<?php if($_GET["action"]=="Update" && $rec['ordering']>0) echo $rec['ordering']; else echo "1000"; ?>" size="80%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>

<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>