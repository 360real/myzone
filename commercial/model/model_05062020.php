<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
    <input type="hidden" name="existingPropertyName" value="<?php echo($rec['property_name']) ?>">
    <input type="hidden" name="property_id" value="<?php echo($_GET['id']) ?>">
    <?php if ($_GET["action"] == "BulkUpload") { ?>
        <input type="hidden" name="is_upload" value="1">
    <?php } else { ?>
        <input type="hidden" name="is_upload" value="0">
    <?php } ?>


    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
        <thead>
            <?php if ($_GET["action"] != "BulkUpload") { ?>
                <tr><th align="left"><h3>New Project Detail</h3></th>
            <?php } else { ?>
                <tr><th align="left"><h3>Upload New Properties</h3></th>
            <?php } ?>
                <th align="right"><a href="<?php echo $webSiteURL?>commercial/index.php"><h3>Back To List </h3> </a> </th></tr>

        </thead>
        <tbody>
            <?php if ($_GET["action"] != "BulkUpload") { ?>


                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong><font color="#FF0000">*</font>First Name</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonly <?php } ?> name="user_first_name" type="text" title="user_first_name" value="<?php echo($rec['user_first_name']) ?>" size="80%" maxlength="120" required="required"  lang="MUST" /></td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Last Name</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonly <?php } ?> name="user_last_name" type="text" title="user_last_name" value="<?php echo($rec['user_last_name']) ?>" size="80%" maxlength="120" required="required"  lang="MUST"/></td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong><font color="#FF0000">*</font>E-Mail</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonly <?php } ?> name="user_email" type="email" title="user_email" value="<?php echo($rec['user_email']) ?>" size="80%" maxlength="120"   /></td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong><font color="#FF0000">*</font>Mobile No.</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonly <?php } ?> name="user_mobile" type="number" required="required" title="user_mobile" value="<?php echo($rec['user_mobile']) ?>" size="80%" maxlength="120"  lang="MUST" /></td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Alternate Number</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonly <?php } ?> name="user_alternate_mobile" type="number" title="user_alternate_mobile" value="<?php echo($rec['user_alternate_mobile']) ?>" size="80%" maxlength="120" /></td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Designation</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonly <?php } ?> name="designation" type="text" title="designation" value="<?php echo($rec['designation']) ?>" size="80%" maxlength="120" /></td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Organization Name</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonly <?php } ?> name="organization_name" type="text" title="organization_name" value="<?php echo($rec['organization_name']) ?>" size="80%" maxlength="120" /></td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_SALE_TYPE . " where is_deleted=0 order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Sale Type</strong></td>
                    <td><select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="sale_type" id="sale_type"  title="sale_type"  class="drop_down" style="width:234px;" lang="MUST" required=""> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['id'] ?>" <?php if ($rec['sale_type'] == $recResult['id']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_PROPERTY_TYPE . " where is_deleted=0 order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Property Type</strong></td>
                    <td><select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="property_type" id="property_type" title="property_type"  class="drop_down" style="width:234px;" lang="MUST" required=""> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['id'] ?>" <?php if ($rec['property_type'] == $recResult['id']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>

                <?php
                $s1 = "SELECT id,name FROM " . TABLE_COMMERCIAL_BUILDING_TYPE . " where is_deleted=0 order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Building Type</strong></td>
                    <td><select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="building_type" id="building_type" title="building_type"  class="drop_down" style="width:234px;" > <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['id'] ?>" <?php if ($rec['building_type'] == $recResult['id']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?> pre_lease_option">
                    <td align="left"><strong>Tenant Name</strong></td>
                    <td>
                        <input name="pre_lease_tenant_name" type="text" title="Tenant Name" size="80%" value="<?= $rec['pre_lease_tenant_name'] ?>"  />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?> pre_lease_option">
                    <td align="left"><strong>Security Deposite</strong></td>
                    <td><input class="allownumericwithdecimal" name="pre_lease_security_deposite" type="text" title="pre_lease_security_deposite"  size="80%" maxlength="120" value="<?= $rec['pre_lease_security_deposite'] ?>" />
                        <select name="pre_lease_security_deposite_currency" id="pre_lease_security_deposite_currency" title="pre_lease_security_deposite_currency"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                <option value="Rs." <?php if($rec['pre_lease_security_deposite_currency']=="Rs."){ echo 'selected=""';} ?>>Rs.</option>
                                <option value="$" <?php if($rec['pre_lease_security_deposite_currency']=="$"){ echo 'selected=""';} ?>>$</option>
                                <option value="Euro" <?php if($rec['pre_lease_security_deposite_currency']=="Euro"){ echo 'selected=""';} ?>>Euro</option>
                        </select>

                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?> pre_lease_option">
                    <td align="left"><strong>Rent Increment (In Percentage [%])</strong></td>
                    <td><input class="allownumericwithdecimal" name="pre_lease_rent_increment" type="text" title="pre_lease_rent_increment"  size="80%" maxlength="120" value="<?= $rec['pre_lease_rent_increment'] ?>" />
                        <select name="pre_lease_rent_increment_currency" id="pre_lease_rent_increment_currency" title="pre_lease_rent_increment_currency"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                <option value="Rs." <?php if($rec['pre_lease_rent_increment_currency']=="Rs."){ echo 'selected=""';} ?>>Rs.</option>
                                <option value="$" <?php if($rec['pre_lease_rent_increment_currency']=="$"){ echo 'selected=""';} ?>>$</option>
                                <option value="Euro" <?php if($rec['pre_lease_rent_increment_currency']=="Euro"){ echo 'selected=""';} ?>>Euro</option>
                        </select>
                        <select name="pre_lease_rent_increment_duration" id="pre_lease_rent_increment_duration" title="pre_lease_rent_increment_duration"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                <option value="Monthly" <?php if($rec['pre_lease_rent_increment_duration']=="Monthly"){ echo 'selected=""';} ?>>Monthly</option>
                                <option value="Yearly" <?php if($rec['pre_lease_rent_increment_duration']=="Yearly"){ echo 'selected=""';} ?>>Yearly</option>
                        </select>

                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?> pre_lease_option">
                    <td align="left"><strong>Lease Tenure</strong></td>
                    <td><input class="allownumericwithdecimal" name="pre_lease_lease_tenure" type="text" title="pre_lease_lease_tenure"  size="80%" maxlength="120" value="<?= $rec['pre_lease_lease_tenure'] ?>" />
                        <select name="pre_lease_lease_tenure_currency" id="pre_lease_lease_tenure_currency" title="pre_lease_lease_tenure_currency"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                <option value="Rs." <?php if($rec['pre_lease_lease_tenure_currency']=="Rs."){ echo 'selected=""';} ?>>Rs.</option>
                                <option value="$" <?php if($rec['pre_lease_lease_tenure_currency']=="$"){ echo 'selected=""';} ?>>$</option>
                                <option value="Euro" <?php if($rec['pre_lease_lease_tenure_currency']=="Euro"){ echo 'selected=""';} ?>>Euro</option>
                        </select>
                        <select name="pre_lease_lease_tenure_duration" id="pre_lease_lease_tenure_duration" title="pre_lease_lease_tenure_duration"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                <option value="Monthly" <?php if($rec['pre_lease_lease_tenure_duration']=="Monthly"){ echo 'selected=""';} ?>>Monthly</option>
                                <option value="Yearly" <?php if($rec['pre_lease_lease_tenure_duration']=="Yearly"){ echo 'selected=""';} ?>>Yearly</option>
                        </select>

                    </td>
                </tr>

                 <tr class="<?php echo($clsRow1) ?> pre_lease_option">
                    <td align="left"><strong>Lockin Period (in year)</strong></td>
                    <td><input class="allownumericwithdecimal" name="pre_lease_lockin_period" type="text" title="pre_lease_lockin_period"  size="80%" maxlength="120" value="<?= $rec['pre_lease_lockin_period'] ?>" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?> pre_lease_option">
                    <td align="left"><strong>Rent Start Date</strong></td>
                    <td>
                        <input type="text" name="pre_lease_rent_start_date" value="<?php echo($rec['pre_lease_rent_start_date'])?>"/>
                        <script language="JavaScript">
                      new tcal ({
                        'formname': 'form123',
                        'controlname': 'pre_lease_rent_start_date'
                      });</script>
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?> pre_lease_option">
                    <td align="left"><strong>ROI (Return of Investment)</strong></td>
                    <td><input class="allownumericwithdecimal" name="pre_lease_roi" type="text" title="pre_lease_roi"  size="80%" maxlength="120" value="<?= $rec['pre_lease_roi'] ?>" />
                        <select name="pre_lease_roi_currency" id="pre_lease_roi_currency" title="pre_lease_roi_currency"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                <option value="Rs." <?php if($rec['pre_lease_roi_currency']=="Rs."){ echo 'selected=""';} ?>>Rs.</option>
                                <option value="$" <?php if($rec['pre_lease_roi_currency']=="$"){ echo 'selected=""';} ?>>$</option>
                                <option value="Euro" <?php if($rec['pre_lease_roi_currency']=="Euro"){ echo 'selected=""';} ?>>Euro</option>
                        </select>
                        <select name="pre_lease_roi_duration" id="pre_lease_roi_duration" title="pre_lease_roi_duration"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                <option value="Monthly" <?php if($rec['pre_lease_roi_duration']=="Monthly"){ echo 'selected=""';} ?>>Monthly</option>
                                <option value="Yearly" <?php if($rec['pre_lease_roi_duration']=="Yearly"){ echo 'selected=""';} ?>>Yearly</option>
                        </select>

                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Tansaction Type</strong></td>
                    <td><select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="tansaction_type"  id="tansaction_type" title="tansaction_type"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <option value="New Property" <?php if ($rec['tansaction_type'] == "New Property") { ?> selected<?php } ?>>New Property</option>
                            <option value="ReSale" <?php if ($rec['tansaction_type'] == "ReSale") { ?> selected<?php } ?>>ReSale</option>
                        </select></td>
                </tr>

                <?php
                $s1 = "SELECT id,name FROM " . TABLE_AREA_UNIT . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Super Build Area</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="super_build_area" type="text" title="super_build_area" value="<?php echo($rec['super_build_area']) ?>" size="80%" maxlength="120" />
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="super_build_area_unit" id="super_build_area_unit" title="super_build_area_unit"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['name'] ?>" <?php if ($rec['super_build_area_unit'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_AREA_UNIT . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Covered Area</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="covered_area" type="text" title="covered_area" value="<?php echo($rec['covered_area']) ?>" size="80%" maxlength="120" />
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="covered_area_unit" id="covered_area_unit" title="covered_area_unit"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['name'] ?>" <?php if ($rec['covered_area_unit'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_AREA_UNIT . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Plot Area</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="plot_area" class="allownumericwithdecimal" type="text" title="plot_area" value="<?php echo($rec['plot_area']) ?>" size="80%" maxlength="120" />
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="plot_area_unit" id="plot_area_unit" title="plot_area_unit"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['name'] ?>" <?php if ($rec['plot_area_unit'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Possession Type</strong></td>
                    <td>
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="possession_type" id="possession_type" title="possession_type"   class="drop_down" style="width:234px;" lang="MUST"  required=""> 
                                <option value="">-- Select --</option>
                                <option value="Under Construction" <?php if ($rec['possession_type'] == "Under Construction") { ?> selected <?php } ?>>Under Construction</option>
                                <option value="Ready to Move" <?php if ($rec['possession_type'] == "Ready to Move") { ?> selected <?php } ?>>Ready to Move</option>
                                <option value="Hand Over" <?php if ($rec['possession_type'] == "Hand Over") { ?> selected <?php } ?>>Hand Over</option>
                        </select>

                        <!-- <input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="possession_date" type="text" id="possession_date" title="possession_date" value="<?php echo($rec['possession_date']) ?>" size="80%" maxlength="120" <?php if ($rec['possession_type'] != "Hand Over") { ?> style="display:none;" <?php } ?> /> -->

                        <input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> type="text" name="possession_date" id="possession_date" value="<?php echo($rec['possession_date'])?>"  <?php if ($rec['possession_type'] != "Hand Over") { ?> style="display:none;" <?php } ?>  />
                         
                            <script language="JavaScript">
                      new tcal ({
                        'formname': 'form123',
                        'controlname': 'possession_date'
                      });</script>
                        <?php if ($rec['possession_type'] != "Hand Over") { ?>
                            
                            <script language="JavaScript">
                            $("#tcalico_1").hide();
                        </script>
                        <?php } ?>


                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Leased Out</strong></td>
                    <td><select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="leased_out"  id="leased_out" title="leased_out"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <option value="1" <?php if ($rec['leased_out'] > 0) { ?> selected<?php } ?>>Yes</option>
                            <option value="0" <?php if ($rec['leased_out'] <= 0) { ?> selected<?php } ?>>No</option>
                        </select>
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Expected Price</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="price_to_display" type="text" title="price_to_display" value="<?php echo($rec['price_to_display']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Price</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="" name="price" type="text" title="price" value="<?php echo($rec['price']) ?>" size="80%" maxlength="120" lang="MUST" required=""/>
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Other Charges</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="other_charges" type="text" title="other_charges" value="<?php echo($rec['other_charges']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>StampDuty/Registration Charges</strong></td>
                    <td><input<?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?>  class="allownumericwithdecimal" name="stampduty_registrationcharges" type="text" title="stampduty_registrationcharges" value="<?php echo($rec['stampduty_registrationcharges']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Booking/Token Amount</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="booking_token_amount" type="text" title="booking_token_amount" value="<?php echo($rec['booking_token_amount']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_AREA_UNIT . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Maintenance Charges</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="maintenance_charges" type="text" title="maintenance_charges" value="<?php echo($rec['maintenance_charges']) ?>" size="80%" maxlength="120" />
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="maintenance_type" id="maintenance_type" title="maintenance_type"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['name'] ?>" <?php if ($rec['maintenance_type'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="maintenance_duration" id="maintenance_duration" title="maintenance_duration"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <option value="Monthly" <?php if ($rec['maintenance_duration'] == 'Monthly') { ?> selected <?php } ?>>Monthly</option>
                            <option value="Quarterly" <?php if ($rec['maintenance_duration'] == 'Quarterly') { ?> selected <?php } ?>>Quarterly</option>
                            <option value="Yearly" <?php if ($rec['maintenance_duration'] == 'Yearly') { ?> selected <?php } ?>>Yearly</option>
                        </select>

                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Brokerage</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="brokerage" type="text" title="brokerage" value="<?php echo($rec['brokerage']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Brokerage Rate in %</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="brokerage_rate" type="text" title="brokerage_rate" value="<?php echo($rec['brokerage_rate']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Office Floors</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="office_floors" type="text" title="office_floors" value="<?php echo($rec['office_floors']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Retails Floors</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?>  name="retails_floors" type="text" title="retails_floors" value="<?php echo($rec['retails_floors']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Total No Of Floors</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?>  name="no_of_floors" type="text" title="no_of_floors" value="<?php echo($rec['no_of_floors']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Maintenance Agency Name</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="maintenance_agency_name" type="text" title="maintenance_agency_name" value="<?php echo($rec['maintenance_agency_name']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_AREA_UNIT . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Maintenance Office</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="maintenance_office" type="text" title="maintenance_office" value="<?php echo($rec['maintenance_office']) ?>" size="80%" maxlength="120" />
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="maintenance_office_unit" id="super_build_area_unit" title="maintenance_office_unit"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['name'] ?>" <?php if ($rec['maintenance_office_unit'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_AREA_UNIT . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Maintenance Retails</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="maintenance_retail" type="text" title="maintenance_retail" value="<?php echo($rec['maintenance_retail']) ?>" size="80%" maxlength="120" />
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="maintenance_retail_unit" id="maintenance_retail_unit" title="maintenance_retail_unit"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['name'] ?>" <?php if ($rec['maintenance_retail_unit'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>

                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Building Mgr Name</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?>  name="building_mgr_name" type="text" title="building_mgr_name" value="<?php echo($rec['building_mgr_name']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Building Mgr Contact</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?>  name="building_mgr_contact" type="text" title="building_mgr_contact" value="<?php echo($rec['building_mgr_contact']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Office Efficiency</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?>  name="efficiency_office" type="text" title="efficiency_office" value="<?php echo($rec['efficiency_office']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Retail Efficiency</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="efficiency_retail" type="text" title="efficiency_retail" value="<?php echo($rec['efficiency_retail']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Celling Height</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="celling_height" type="text" title="celling_height" value="<?php echo($rec['celling_height']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Car Parking</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="car_parking" type="text" title="car_parking" value="<?php echo($rec['car_parking']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Power Back Up</strong></td>
                    <td><select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="power_back_up"  id="power_back_up" title="power_back_up"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <option value="1" <?php if ($rec['power_back_up'] > 0) { ?> selected<?php } ?>>Yes</option>
                            <option value="0" <?php if ($rec['power_back_up'] <= 0) { ?> selected<?php } ?>>No</option>
                        </select>
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Power Load</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="allownumericwithdecimal" name="power_load" type="text" title="power_load" value="<?php echo($rec['power_load']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_AREA_UNIT . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Available Area</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="available_area" type="text" title="available_area" value="<?php echo($rec['available_area']) ?>" size="80%" maxlength="120" />
                        <select <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="available_area_unit" id="available_area_unit" title="available_area_unit"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                            <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                <option value="<?= $recResult['name'] ?>" <?php if ($rec['available_area_unit'] == $recResult['name']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>AC Type</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="" name="ac_type" type="text" title="ac_type" value="<?php echo($rec['ac_type']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Lift Maker</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="lift_maker" type="text" title="lift_maker" value="<?php echo($rec['lift_maker']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Escalators</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="escalators" type="text" title="escalators" value="<?php echo($rec['escalators']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Number of Passanger Lift</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="passanger_lift" type="text" class="allownumericwithdecimal" title="passanger_lift" value="<?php echo($rec['passanger_lift']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Number of Service Lift</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="service_lift" type="text" class="allownumericwithdecimal" title="service_lift" value="<?php echo($rec['service_lift']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Working Hour</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="working_hour" type="text" title="working_hour" value="<?php echo($rec['working_hour']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Fitouts Scope</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="fitouts_scope" type="text" title="fitouts_scope" value="<?php echo($rec['fitouts_scope']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>F&B Brands</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> type="text" title="f_b_brands" name="f_b_brands" value="<?php echo($rec['f_b_brands']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Foodcourt</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="foodcourt" type="text" title="foodcourt" value="<?php echo($rec['foodcourt']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Health Culb/Gym</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="health_culb_gym" type="text" title="health_culb_gym" value="<?php echo($rec['health_culb_gym']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Connectivity</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="connectivity" type="text" title="connectivity" value="<?php echo($rec['connectivity']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Year Of Construction</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?>  name="year_of_construction" type="text" title="year_of_construction" value="<?php echo($rec['year_of_construction']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>OC&CC</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="oc_cc" type="text" title="oc_cc" value="<?php echo($rec['oc_cc']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Fire Noc</strong></td>
                    <td><input <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> name="fire_noc" type="text" title="fire_noc" value="<?php echo($rec['fire_noc']) ?>" size="80%" maxlength="120" />
                    </td>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Remarks</strong></td>
                    <td><textarea <?php if ($_SESSION['login']['role_id'] != "20") { ?> readonlyy <?php } ?> class="ckeditor" name="remarks"  id="remarks" rows="4" cols="70" ><?php echo $rec['remarks'] ?> </textarea>
                    </td>
                </tr>
                <?php //} ?>
                <?php
                $s1 = "SELECT id,name FROM " . TABLE_LANDZONE . " order by name";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                if ($_GET["action"] == "Update") {
                    ?>
                    <tr class="<?php echo($clsRow1) ?>">
                        <td align="left"><div  id="LandZoneLabelDiv" name="LandZoneLabelDiv"><strong>LandZone</strong></div></td>
                        <td><div name="subLandZoneDiv" id="LandZoneDiv">
                                <select name="landzone" id="landzone" title="Landzone"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                    <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                        <option value="<?= $recResult['id'] ?>" <?php if ($rec['landzone'] == $recResult['id']) { ?> selected <?php } ?>><?= $recResult['name'] ?></option>
                                    <?php } ?>
                                </select>  </div></td>
                    </tr>
                <?php } else { ?>
                    <tr class="<?php echo($clsRow1) ?>">
                        <td align="left"><div  id="LandZoneLabelDiv" name="LandZoneLabelDiv"><strong>LandZone</strong></div></td>
                        <td><div name="subLandZoneDiv" id="LandZoneDiv">
                                <select name="landzone" id="landzone" title="Landzone"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                    <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                        <option value="<?= $recResult['id'] ?>" ><?= $recResult['name'] ?></option>
                                    <?php } ?>
                                </select>  </div></td>
                    </tr>

            <?php } ?>


            <tr class="<?php echo($clsRow1) ?>">
                <td align="left"><strong>Country</strong></td>
                <td><?php echo AjaxDropDownList(TABLE_COUNTRY, "country_id", "id", "country", $_GET["country_id"], "id", $site['CMSURL'] . "commercial/populate_city_advance.php", "cityDiv", "cityLabelDiv") ?></td>
            </tr>

            <?php if ($_GET["action"] == "Update") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><div  id="cityLabelDiv" name="cityLabelDiv"><strong>City</strong></div></td>
                    <td><div  name="cityDiv" id="cityDiv">
                            <?php 
                            // echo AjaxDropDownListAdvance(TABLE_CITIES, "cty_id", "id", "city", $_GET["country_id"], $_GET["cty_id"], "id", $site['CMSURL'] . "commercial/populate_location.php", "locationDiv", "locationLabelDiv", "country_id"); 
                            echo AjaxDropDownListAdvanceCity(TABLE_CITIES,"cty_id","id","city",1,$_GET['cty_id'],"id",$site['CMSURL']."commercial/populate_location.php","locationDiv","locationLabelDiv","country_id",'in_commercial','1');
                            ?>


                        </div></td>
                </tr>
            <?php } else { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><div id="cityLabelDiv" name="cityLabelDiv"><strong>City</strong></div></td>
                    <td><div  name="cityDiv" id="cityDiv"><select  style="width:234px;"><option>-- Select --</option></select></div></td>
                </tr>

            <?php } ?>

            <!-- micro market  start-->
            <?php
            if ($_GET["action"] == "Update") {
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><div  id="microMarketLabelDiv" name="microMarketLabelDiv"><strong>Micro market</strong></div></td>
                    <td><div  name="microMarketDiv" id="microMarketDiv">
                            <?php
                            
                            if ($rec['micro_market_id'] <= 0)
                                echo AjaxDropDownListAdvance(TABLE_MICRO_MARKET,"micro_market_id","id","name",$_GET["cty_id"],-1,"name",$site['CMSURL']."commercial/populate_location_by_micro_market.php","locationDiv","locationLabelDiv","cty_id");
                            else
                                echo AjaxDropDownListAdvance(TABLE_MICRO_MARKET, "micro_market_id", "id", "name", $_GET["cty_id"], $rec["micro_market_id"], "name", $site['CMSURL'] . "commercial/populate_location_by_micro_market.php", "locationDiv", "locationLabelDiv", "cty_id");
                            ?>

                        </div></td>
                </tr>

            <?php } else { ?>
                        <tr class="<?php echo($clsRow1) ?>">
                            <td align="left"><div id="microMarketLabelDiv" name="microMarketLabelDiv"><strong>Micro Market</strong></div></td>
                            <td><div  name="microMarketDiv" id="microMarketDiv"><select name="micro_market_id" id="micro_market_id" title="Micro Market" style="width:234px;"><option>-- Select --</option></select></div></td>
                        </tr>

            <?php } ?>

            <!-- micro market end -->
            <?php
            if ($_GET["action"] == "Update") {
                // $s1 = "SELECT id,location FROM " . TABLE_LOCATIONS . " WHERE cty_id=".$_GET['cty_id']." order by location";
                // $r1 = mysql_query($s1);
                // $rowCount = mysql_num_rows($r1);
                ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><div  id="locationLabelDiv" name="locationLabelDiv"><strong>Location</strong></div></td>
                    <td><div  name="locationDiv" id="locationDiv">
                            <?php
                            /*if ($rec['location'] <= 0):echo AjaxDropDownListAdvance(TABLE_LOCATIONS, "location", "id", "location", $_GET["cty_id"], $_GET["loc_id"], "location", $site['CMSURL'] . "commercial/populate_sublocations.php", "sublocationDiv", "sublocationLabelDiv", "cty_id");
                            else:
                                ?>
                                <select name="location" id="location" title="Location"  class="drop_down" style="width:234px;"> <option value="">-- Select --</option>
                                    <?php while ($recResult = mysql_fetch_assoc($r1)) { ?>
                                        <option value="<?= $recResult['id'] ?>" <?php if ($rec['location'] == $recResult['id']) { ?> selected <?php } ?>><?= $recResult['location'] ?></option>
                                <?php } ?>
                                </select>
                            <?php endif;*/
                            if ($rec['loc_id'] <= 0)
                                echo AjaxDropDownListAdvance(TABLE_LOCATIONS,"loc_id","id","location",$_GET["cty_id"],-1,"location",$site['CMSURL']."commercial/populate_sublocations.php","sublocationDiv","sublocationLabelDiv","cty_id");
                            else
                                echo AjaxDropDownListAdvance(TABLE_LOCATIONS, "loc_id", "id", "location", $_GET["cty_id"], $_GET["loc_id"], "location", $site['CMSURL'] . "commercial/populate_sublocations.php", "sublocationDiv", "sublocationLabelDiv", "cty_id");
                            ?>

                        </div></td>
                </tr>
    <?php } else { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><div id="locationLabelDiv" name="locationLabelDiv"><strong>Location</strong></div></td>
                    <td><div  name="locationDiv" id="locationDiv"><select name="loc_id" id="loc_id" title="Location" style="width:234px;"><option>-- Select --</option></select></div></td>
                </tr>

    <?php } ?>


    <?php if ($_GET["action"] == "Update") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><div  id="sublocationLabelDiv" name="sublocationLabelDiv"><strong>Cluster</strong></div></td>
                    <td><div  name="sublocationDiv" id="sublocationDiv">
        <?php 
            if ($rec['cluster_id'] <= 0)
                echo DropDownListAdvance(TABLE_CLUSTER, "cluster_id", "id", "cluster", $_GET["loc_id"], -1, "id", "loc_id");
            else
                echo DropDownListAdvance(TABLE_CLUSTER, "cluster_id", "id", "cluster", $_GET["loc_id"], $_GET["cluster_id"], "id", "loc_id");

             ?>
                        </div></td>
                </tr>
    <?php } else { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><div id="sublocationLabelDiv" name="sublocationLabelDiv"><strong>Cluster</strong></div></td>
                    <td><div  name="sublocationDiv" id="sublocationDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
                </tr>

    <?php } ?>



            <tr class="<?php echo($clsRow1) ?>">
                <td align="left"><strong>Property Name</strong></td>
                <td><input id="zipsearch" name="property_name" type="text" onkeypress="showFResult();" value="<?php echo($rec['property_name']) ?>" size="41%" maxlength="120" lang="MUST" required="" /></td>
            </tr>
            <tr class="<?php echo($clsRow1) ?>">
                <td align="left"><strong>Unit No</strong></td>
                <td><input  name="unit_no" type="text"  value="<?php echo($rec['unit_no']) ?>" size="41%" maxlength="120"/></td>
            </tr>
            <tr class="<?php echo($clsRow1) ?>">
                <td align="left"><strong>Property On</strong></td>
                <td><input name="property_on" type="text"  value="<?php echo($rec['property_on']) ?>" size="41%" maxlength="120"/></td>
            </tr>
    <?php if ($_SESSION['login']['role_id'] != "20") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>View Type</strong></td>
                    <td>
                        <?php for ($i = 0; $i < 3; $i++) { ?>
                            <input type="radio" name="viewType" value="<?php echo $i; ?>" 
                                   <?php if ($_GET["action"] == 'Create' && $i == 0) { ?>
                                       checked
                                   <?php } else if ($i == $rec['viewType']) { ?>
                                       checked
            <?php } ?>
                                   >

                            <img src="<?php echo C_ROOT_URL . 'view/images/propertyview' . $i . '.png' ?>" width="150px" height="100px" <?php ?>>
        <?php } ?>


                    </td>
                </tr>
    <?php } if ($_SESSION['login']['role_id'] != "20") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Property Name to display</strong></td>
                    <td><input name="property_name_display" type="text" value="<?php echo($rec['property_name_display']) ?>" size="41%" maxlength="120"/></td>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Property USP</strong></td>
                    <td><input name="property_usp" type="text" value="<?php echo($rec['property_usp']) ?>" size="41%" maxlength="120"/></td>
                </tr>
    <?php } ?>
            <tr class="<?php echo($clsRow1) ?>">
                <td align="left"><strong><font color="#FF0000">*</font>Developer</strong></td>
                <td>
    <?php echo DropDownList(TABLE_DEVELOPERS, "devlpr_id", "id", "name", $_GET["devlpr_id"], "name") ?>
                </td>
            </tr>
    <?php if ($_SESSION['login']['role_id'] != "20") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Microsite Domain Name</strong></td>
                    <td><input id="microsite_sef_url" name="microsite_sef_url" type="text" value="<?php echo($rec['microsite_sef_url']) ?>" size="41%" maxlength="120"/></td>
                </tr>

    <?php } ?>
            <!-- Start: Amenities Code -->

            <tr class="<?php echo($clsRow1) ?>">
                <td align="left"><strong>Amenities :</strong></td>
                <td>
                    <?php
                    if ($_GET["action"] == "Update") {

                        $addAmenities = explode(",", $rec["new_amenities"]);
                        $s1 = "SELECT id, amenities FROM " . TABLE_COMMERCIAL_AMENITIES . " order by amenities";
                        $r1 = mysql_query($s1);
                        $rowCount = mysql_num_rows($r1);
                        while ($recResult = mysql_fetch_array($r1)) {
                            ?>
                            <div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_amenities[]" <?php
                                for ($i = 0; $i < count($addAmenities); $i++) {
                                    if ($recResult[id] == $addAmenities[$i]) {
                                        echo 'checked';
                                    }
                                }
                                ?> value="<?php echo $recResult[id] ?>" type="checkbox" align="absmiddle" style=" margin-right: 5px;
                                                                                                                                    position: relative; top: 2px;" ><?php echo $recResult[amenities] ?></div>
                                                                                                                                    <?php
                                                                                                                                }
                                                                                                                            } else {
                                                                                                                                $s1 = "SELECT id, amenities FROM " . TABLE_COMMERCIAL_AMENITIES . " order by amenities";
                                                                                                                                $r1 = mysql_query($s1);
                                                                                                                                $rowCount = mysql_num_rows($r1);
                                                                                                                                while ($recResult = mysql_fetch_array($r1)) {
                                                                                                                                    ?>
                            <div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_amenities[]" value="<?php echo $recResult[id] ?>" checked type="checkbox" align="absmiddle" style=" margin-right: 5px; position: relative; top: 2px;"  ><?php echo $recResult[amenities] ?></div>
                        <?php
                        }
                    }
                    ?>
                </td>
            </tr>

            <!-- End: Amenities Code -->
    

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Property Overview</strong></td>
                    <td><textarea class="ckeditor" name="new_overview"  id="new_overview" rows="4" cols="70" ><?php echo $rec['new_overview'] ?> </textarea>
                </tr>
    <?php if ($_SESSION['login']['role_id'] != "20") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Microsite Property Overview</strong></td>
                    <td><textarea class="ckeditor" name="microsite_property_overview"  id="microsite_property_overview" rows="4" cols="70" ><?php echo $rec['microsite_property_overview'] ?> </textarea>
                </tr>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Microsite Property Overview(36 Template)</strong></td>
                    <td><textarea class="ckeditor" name="microsite_property_overview_36"  id="microsite_property_overview_36" rows="4" cols="70" ><?php echo $rec['microsite_property_overview_36'] ?> </textarea>
                </tr>
    <?php } ?>
    <?php if ($_SESSION['login']['role_id'] != "20") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Highlights</strong></td>
                    <td><textarea class="ckeditor" name="highlights"  id="highlights" rows="4" cols="70" ><?php echo $rec['highlights'] ?></textarea>
                </tr>
    <?php } if ($_SESSION['login']['role_id'] != "20") { ?>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Location Map</strong></td>
                    <td><textarea class="ckeditor" name="location_map"  id="location_map" rows="4" cols="70" ><?php echo $rec['location_map'] ?></textarea>
                </tr>
                <tr class="<?php echo($clsRow1) ?>">
                    <td align="left"><strong>Payment Plan</strong></td>
                    <td><textarea class="ckeditor" name="payment_plan"  id="payment_plan" rows="4" cols="70" ><?php echo $rec['payment_plan'] ?></textarea>
                </tr>

                <tr class="<?php echo($cls1) ?>"><td width="15%" align="right"></td>
                    <td>

                        <?php
                        if ($_GET["action"] == "Update") {
                            $sqlImages = "SELECT * FROM " . TABLE_IMAGES . " WHERE img_type='DEVELOPER_PROPERTY_BANNER_IMAGE' and propty_id=" . $_GET['id'];
                            $recImages = ($obj_mysql->getAllData($sqlImages));
                            for ($i = 0; $i < 2; $i++) {
                                ?>

                        <tr class="<?php echo($cls1) ?>" style=" background:#fffbe4;">
                            <td align="right" ><strong>Select Banner Image <?= $i + 1 ?></strong></td>
                        <input type="hidden" name="imageId[]" value="<?= $recImages[$i]['id'] ?>">
                        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="38%"><input type="file" name="propertyImage<?= $i ?>" id="propertyImage<?= $i ?>" /></td>
                                    <td width="62%">&nbsp;&nbsp;<?php if ($recImages[$i]['image']): ?>
                                            <br />
                                            <img src="<?php echo ($site['PROPERTYBANNERURL'] . $rec['id'] . "/developer_banners/" . $recImages[$i]['image']) ?>"   height="60" width="100" border="0" /> <?php endif; ?>
                                        <input type="hidden" name="old_file_name_propertyImage<?= $i ?>" value="<?php echo ($recImages[$i]['image']) ?>" />
                                        <br /></td>

                                </tr>
                            </table></td>
                        </tr>

                        <?php
                    }
                } else {
                    for ($i = 0; $i < 2; $i++) {
                        ?>

                        <tr class="<?php echo($cls1) ?>" style=" background:#fffbe4;">
                            <td align="right" ><strong>Select Banner Image <?php echo $i + 1 ?></strong></td>
                        <input type="hidden" name="imageId[]">
                        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="38%"><input type="file" name="propertyImage<?= $i ?>" id="propertyImage<?= $i ?>" /></td>
                                <input type="hidden" name="old_file_name_propertyImage<?= $i ?>"  />
                        </td>
                        </tr>
                    </table></td>
                </tr>

                <?php
            }
        }
        ?>

        </td>
        </tr>
        <?php if ($_SESSION['login']['role_id'] != "20") { ?>
            <!-- <tr class="<?php echo($clsRow1) ?>">
                <td align="left"><strong>Project Status</strong></td>
                <td>
            <?php echo DropDownListWithoutMust("tsr_project_status", "project_status_id", "id", "name", $_GET["project_status_id"], "name") ?>
                </td>
            </tr> -->
        <?php }
    }
    ?>
   
   <!-- <tr class="<?php echo($clsRow1) ?>">
        <td align="left"><strong>Project Category :</strong></td>
        <td>
            <?php
            if ($_GET["action"] == "Update") {
                $addCategory = explode(",", $rec["new_category"]);
                $s1 = "SELECT id, name FROM tsr_project_category order by id";
                $r1 = mysql_query($s1);
                $rowCount = mysql_num_rows($r1);
                while ($recResult = mysql_fetch_array($r1)) {
                    ?>
                    <div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_category[]" <?php
                        for ($i = 0; $i < count($addCategory); $i++) {
                            if ($recResult[id] == $addCategory[$i]) {
                                echo 'checked';
                            }
                        }
                        ?> value="<?php echo $recResult[id] ?>" type="checkbox" align="absmiddle" style=" margin-right: 5px;
                                                                                                                            position: relative; top: 2px;" ><?php echo $recResult[name] ?></div>
                                                                                                                            <?php
                                                                                                                        }
                                                                                                                    } else {
                                                                                                                        $s1 = "SELECT id, name FROM tsr_project_category order by id";
                                                                                                                        $r1 = mysql_query($s1);
                                                                                                                        $rowCount = mysql_num_rows($r1);
                                                                                                                        while ($recResult = mysql_fetch_array($r1)) {
                                                                                                                            ?>
                    <div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_category[]" value="<?php echo $recResult[id] ?>" checked type="checkbox" align="absmiddle" style=" margin-right: 5px; position: relative; top: 2px;"  ><?php echo $recResult[name] ?></div>
        <?php
        }
    }
    ?>
        </td>
    </tr>-->
    <?php if ($_SESSION['login']['role_id'] != "20") { ?>
        <!-- <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Is NRI</strong></td>
        <input type='hidden' name='is_nri' value='0'>
        <td><input name="is_nri" type="checkbox"  value="1" <?php if (isset($rec['is_nri']) && $rec['is_nri'] == 1) echo "checked=checked"; ?>  size="41%" maxlength="120"/>Yes</td>

        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Is Platinum</strong></td>
        <input type='hidden' name='is_platinum' value='0'>
        <td><input name="is_platinum" type="checkbox"  value="1" <?php if (isset($rec['is_platinum']) && $rec['is_platinum'] == 1) echo "checked=checked"; ?>  size="41%" maxlength="120"/>Yes</td>

        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>NRI tag line</strong></td>

            <td><input name="nri_tagline" type="text"  value="<?php echo($rec['nri_tagline']) ?>"  size="41%" maxlength="120"/></td>

        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>NRI Type</strong></td> 
            <td><select name="nri_type">
                    <option value="">Select </option>
                    <option value="1"  <?php
                            if (isset($rec['nri_type']) && $rec['nri_type'] == 1) {
                                echo "selected=selected";
                            }
                            ?> >Holiday Home</option> 
                    <option value="2"  <?php
                            if (isset($rec['nri_type']) && $rec['nri_type'] == 2) {
                                echo "selected=selected";
                            }
                            ?> >ROI/High Return Property</option> 
                </select></td>

        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Launch Date</strong></td>
            <td><?php echo getLaunchDateMonth(TABLE_COMMERCIAL_PROPERTIES, "ldMonth", "launch_date", $_GET["launch_date"], $rec['property_name']) ?> <?php echo getLaunchDateYear(TABLE_COMMERCIAL_PROPERTIES, "ldYear", "launch_date", $_GET["launch_date"], $rec['property_name']) ?></td> 
        </tr> -->
    <?php } ?>
  <!--   

                   <?php if ($_SESSION['login']['role_id'] != "20") { ?>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Booking Status</strong></td>
            <td>
                <input type="radio" name="booking_status" value="1" id="1" checked="checked" /> 
                <label for="1">Open</label> 
                <input type="radio" name="booking_status" value="0" id="0" <?php
               if ($rec['booking_status'] == "0") {
                   echo("checked='checked'");
               }
               ?>/> 
                <label for="0">Closed</label>
            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Project USP</strong></td>
            <td><input name="project_usp" type="text" value="<?php echo($rec['project_usp']) ?>" size="80%" maxlength="200"/></td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Project Area</strong></td>
            <td><input name="project_area" type="text" title="project_area" value="<?php echo($rec['project_area']) ?>" size="80%" maxlength="120" /></td>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Bedrooms To Display </strong></td>
            <td><input name="bedrooms" type="text" title="Bedrooms To Display"  value="<?php echo($rec['bedrooms']) ?>" size="80%" maxlength="120" /> <font color="#FF0000" >  [   1,2,3 Bhk   ]</font></td>
        </tr> -->
    <?php }     ?>
    <tr class="<?php echo($clsRow1) ?>">
        <td align="left"><strong>longitude</strong></td>
        <td><input name="lon" type="text" title="lon" value="<?php echo($rec['lon']) ?>" size="80%" maxlength="120" /></td>
    </tr>

    <tr class="<?php echo($clsRow1) ?>">
        <td align="left"><strong>Latitude</strong></td>
        <td><input name="lat" type="text" title="lat" value="<?php echo($rec['lat']) ?>" size="80%" maxlength="120" /></td>
    </tr>
<?php if($_SESSION['login']['role_id']!=20){?>
    <tr class="<?php echo($clsRow1) ?>"><td align="left" ><strong>Status</strong> </td>
        <td valign="bottom">
            <input type="radio" name="status" value="1" id="1" checked="checked" /> 
            <label for="1">Active</label> 
            <input type="radio" name="status" value="0" id="0" <?php
    if ($rec['status'] == "0") {
        echo("checked='checked'");
    }
    ?>/> 
            <label for="0">Inactive</label>
        </td>
    </tr>
<?php } ?>
    <?php if($_SESSION['login']['role_id']==20){?>
    <tr class="<?php echo($clsRow1) ?>"><td align="left" ><strong>Assign To Marketing</strong> </td>
        <td valign="bottom">
            <input type="radio" <?php
    if ($rec['assign_to_marketing'] == "1") {
        echo("checked='checked'");
    }
    ?> name="assign_to_marketing" value="1" id="1" /> 
            <label for="1">Yes</label> 
            <input type="radio" name="assign_to_marketing" value="0" id="0" <?php
    if ($rec['assign_to_marketing'] == "0") {
        echo("checked='checked'");
    }
    ?> checked="checked"  /> 
            <label for="0">NO</label>
        </td>
    </tr>
<?php } ?>
    <?php if($_SESSION['login']['role_id']==20){?>
    <tr class="<?php echo($clsRow1) ?>"><td align="left" ><strong>Duplicate</strong> </td>
        <td valign="bottom">
            <input type="radio" <?php
    if ($rec['is_duplicate'] == "1") {
        echo("checked='checked'");
    }
    ?> name="is_duplicate" value="1" id="1"  /> 
            <label for="1">Yes</label> 
            <input type="radio" name="is_duplicate" value="0" id="0" <?php
    if ($rec['is_duplicate'] == "0") {
        echo("checked='checked'");
    }
    ?> checked="checked"  /> 
            <label for="0">NO</label>
        </td>
    </tr>
<?php } ?>

 

    <?php if ($_SESSION['login']['role_id'] != "20") { ?>
    
        <?php
        if(isset($rec['rating']))
            $rating = json_decode($rec['rating']);
        ?>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Location Rating</strong></td>
            <td>
                <select id="location_rating" name="location_rating">
                    <option value="0">--Rating--</option>
                    <option value="1" <?php if (is_object($rating) && $rating->location_rating == "1") { ?>selected<?php } ?>>1</option>
                    <option value="2"  <?php if (is_object($rating) && $rating->location_rating == "2") { ?>selected<?php } ?>>2</option>
                    <option value="3"  <?php if (is_object($rating) && $rating->location_rating == "3") { ?>selected<?php } ?>>3</option>
                    <option value="4"  <?php if (is_object($rating) && $rating->location_rating == "4") { ?>selected<?php } ?>>4</option>
                    <option value="5"  <?php if (is_object($rating) && $rating->location_rating == "5") { ?>selected<?php } ?>>5</option>
                </select>

            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Safety Rating</strong></td>
            <td>
                <select id="safety_rating" name="safety_rating">
                    <option value="0">--Rating--</option>
                    <option value="1" <?php if (is_object($rating) && $rating->safety_rating == "1") { ?>selected<?php } ?>>1</option>
                    <option value="2"  <?php if (is_object($rating) && $rating->safety_rating == "2") { ?>selected<?php } ?>>2</option>
                    <option value="3"  <?php if (is_object($rating) && $rating->safety_rating == "3") { ?>selected<?php } ?>>3</option>
                    <option value="4"  <?php if (is_object($rating) && $rating->safety_rating == "4") { ?>selected<?php } ?>>4</option>
                    <option value="5"  <?php if (is_object($rating) && $rating->safety_rating == "5") { ?>selected<?php } ?>>5</option>
                </select>

            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Transport Rating</strong></td>
            <td>
                <select id="transport_rating" name="transport_rating">
                    <option value="0">--Rating--</option>
                    <option value="1" <?php if (is_object($rating) && $rating->transport_rating == "1") { ?>selected<?php } ?>>1</option>
                    <option value="2"  <?php if (is_object($rating) && $rating->transport_rating == "2") { ?>selected<?php } ?>>2</option>
                    <option value="3"  <?php if (is_object($rating) && $rating->transport_rating == "3") { ?>selected<?php } ?>>3</option>
                    <option value="4"  <?php if (is_object($rating) && $rating->transport_rating == "4") { ?>selected<?php } ?>>4</option>
                    <option value="5"  <?php if (is_object($rating) && $rating->transport_rating == "5") { ?>selected<?php } ?>>5</option>
                </select>

            </td>
        </tr>
<tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Entertainment Rating</strong></td>
            <td>
                <select id="entertainment_rating" name="entertainment_rating">
                    <option value="0">--Rating--</option>
                    <option value="1" <?php if (is_object($rating) && $rating->entertainment_rating == "1") { ?>selected<?php } ?>>1</option>
                    <option value="2"  <?php if (is_object($rating) && $rating->entertainment_rating == "2") { ?>selected<?php } ?>>2</option>
                    <option value="3"  <?php if (is_object($rating) && $rating->entertainment_rating == "3") { ?>selected<?php } ?>>3</option>
                    <option value="4"  <?php if (is_object($rating) && $rating->entertainment_rating == "4") { ?>selected<?php } ?>>4</option>
                    <option value="5"  <?php if (is_object($rating) && $rating->entertainment_rating == "5") { ?>selected<?php } ?>>5</option>
                </select>

            </td>
        </tr>
<tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Connectivity Rating</strong></td>
            <td>
                <select id="connectivity_rating" name="connectivity_rating">
                    <option value="0">--Rating--</option>
                    <option value="1" <?php if (is_object($rating) && $rating->connectivity_rating == "1") { ?>selected<?php } ?>>1</option>
                    <option value="2"  <?php if (is_object($rating) && $rating->connectivity_rating == "2") { ?>selected<?php } ?>>2</option>
                    <option value="3"  <?php if (is_object($rating) && $rating->connectivity_rating == "3") { ?>selected<?php } ?>>3</option>
                    <option value="4"  <?php if (is_object($rating) && $rating->connectivity_rating == "4") { ?>selected<?php } ?>>4</option>
                    <option value="5"  <?php if (is_object($rating) && $rating->connectivity_rating == "5") { ?>selected<?php } ?>>5</option>
                </select>

            </td>
        </tr>
<tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Shopping Rating</strong></td>
            <td>
                <select id="shopping_rating" name="shopping_rating">
                    <option value="0">--Rating--</option>
                    <option value="1" <?php if (is_object($rating) && $rating->shopping_rating == "1") { ?>selected<?php } ?>>1</option>
                    <option value="2"  <?php if (is_object($rating) && $rating->shopping_rating == "2") { ?>selected<?php } ?>>2</option>
                    <option value="3"  <?php if (is_object($rating) && $rating->shopping_rating == "3") { ?>selected<?php } ?>>3</option>
                    <option value="4"  <?php if (is_object($rating) && $rating->shopping_rating == "4") { ?>selected<?php } ?>>4</option>
                    <option value="5"  <?php if (is_object($rating) && $rating->shopping_rating == "5") { ?>selected<?php } ?>>5</option>
                </select>

            </td>
        </tr>



        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Home Page Ordering</strong></td>
            <td><input name="hot_ordering" autocomplete="off" id="hot_ordering" type="text"  value="<?php if ($_GET["action"] == "Update" && $rec['hot_ordering'] > 0)
                   echo $rec['hot_ordering'];
               else
                   echo "1000";
               ?>" size="80%" maxlength="120" /></td>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>City Page Ordering</strong></td>
            <td><input name="city_ordering" autocomplete="off" id="city_ordering" type="text"  value="<?php if ($_GET["action"] == "Update" && $rec['city_ordering'] > 0)
                   echo $rec['city_ordering'];
               else
                   echo "1000";
               ?>" size="80%" maxlength="120" /></td>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Location Page Ordering</strong></td>
            <td><input name="location_ordering" autocomplete="off" id="location_ordering" type="text"  value="<?php if ($_GET["action"] == "Update" && $rec['location_ordering'] > 0)
                   echo $rec['location_ordering'];
               else
                   echo "1000";
               ?>" size="80%" maxlength="120" /></td>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Meta Title</strong></td>
            <td><textarea name="meta_title"  id="meta_title" rows="3" cols="150" ><?php echo $rec['meta_title'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Meta Description</strong></td>
            <td><textarea name="meta_description"  id="meta_description" rows="3" cols="150" ><?php echo $rec['meta_description'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Meta Keyword</strong></td>
            <td><textarea name="meta_keywords"  id="meta_keywords" rows="3" cols="150" ><?php echo $rec['meta_keywords'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Micrtosite Meta Title</strong></td>
            <td><textarea name="microsite_meta_title"  id="microsite_meta_title" rows="3" cols="150" ><?php echo $rec['microsite_meta_title'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Micrtosite Meta Description</strong></td>
            <td><textarea name="microsite_meta_description"  id="microsite_meta_description" rows="3" cols="150" ><?php echo $rec['microsite_meta_description'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Micrtosite Meta Keyword</strong></td>
            <td><textarea name="microsite_meta_keywords"  id="microsite_meta_keywords" rows="3" cols="150" ><?php echo $rec['microsite_meta_keywords'] ?></textarea>
        </tr>


        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Location Micrtosite Meta Title</strong></td>
            <td><textarea name="location_microsite_meta_title"  id="location_microsite_meta_title" rows="3" cols="150" ><?php echo $rec['location_microsite_meta_title'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Location Micrtosite Meta Description</strong></td>
            <td><textarea name="location_microsite_meta_description"  id="location_microsite_meta_description" rows="3" cols="150" ><?php echo $rec['location_microsite_meta_description'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Location Micrtosite Meta Keyword</strong></td>
            <td><textarea name="location_microsite_meta_keywords"  id="location_microsite_meta_keywords" rows="3" cols="150" ><?php echo $rec['location_microsite_meta_keywords'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Devloper Highlight Text 1</strong></td>
            <td><input name="dev_highlight_text1" type="text" value="<?php echo($rec['dev_highlight_text1']) ?>" size="41%" maxlength="120"/></td>
        <tr class="<?php echo($clsRow1) ?>">

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Devloper Highlight Text 2</strong></td>
            <td><input name="dev_highlight_text2" type="text" value="<?php echo($rec['dev_highlight_text2']) ?>" size="41%" maxlength="120"/></td>
        <tr class="<?php echo($clsRow1) ?>">

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Devloper Highlight Text 3</strong></td>
            <td><input name="dev_highlight_text3" type="text" value="<?php echo($rec['dev_highlight_text3']) ?>" size="41%" maxlength="120"/></td>

            <!--*************************** Property Distance Code ***************************-->
            <?php

            function distanceUnits($name, $rec) {
                ?>
            <select name="<?php echo $name; ?>">
                <option <?php if ($rec[$name] == '') { ?> selected <?php } ?> value="">--</option>
                <option <?php if ($rec[$name] == 'KM') { ?> selected <?php } ?> value="KM">KM</option>
                <option <?php if ($rec[$name] == 'Mtr') { ?> selected <?php } ?> value="Mtr">Mtr</option>
                <option <?php if ($rec[$name] == 'Hrs') { ?> selected <?php } ?> value="Hrs">Hrs</option>
                <option <?php if ($rec[$name] == 'Min') { ?> selected <?php } ?> value="Min">Min</option>
            </select>
                         <?php
                     }

                     function setPriority($name, $rec) {
                         ?>
            <select name="<?php echo $name; ?>">
                         <?php for ($i = 1; $i < 11; $i++) { ?>
                    <option  value="<?php echo $i; ?>"
                             <?php
                             if (count($rec) == 0) {
                                 if ($i == 10) {
                                     ?>
                                     selected       
                                     <?php
                                 }
                             } else {

                                 if ($rec[$name] == null) {

                                     $priroty = 10;
                                 } else {

                                     $priroty = $rec[$name];
                                 }

                                 if ($priroty == $i) {
                                     ?>
                                     selected  
                            <?php
                            }
                        }
                        ?>



                             ><?php echo $i; ?></option>
            <?php } ?>
            </select>
                <?php }
                ?>


      


     <!--   <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Nearest Airport </strong> </td>
            <td>
                <strong>Display Priroty</strong>:
                <?php setPriority("airportDistancePriroty", $rec); ?>
                <input name="airport_distance" type="text" value="<?php echo($rec['airport_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("airportDistanceUnit", $rec); ?>
                <input name="airport_name" type="text" value="<?php echo($rec['airport_name']) ?>" size="25%" placeholder="1st Airport Name"/>

                <input name="airport_distance1" type="text" value="<?php echo($rec['airport_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("airportDistanceUnit1", $rec); ?>
                <input name="airport_name1" type="text" value="<?php echo($rec['airport_name1']) ?>" size="25%" placeholder="2nd Airport Name"/>

                <input name="airport_distance2" type="text" value="<?php echo($rec['airport_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("airportDistanceUnit2", $rec); ?>
                <input name="airport_name2" type="text" value="<?php echo($rec['airport_name2']) ?>" size="25%" placeholder="3rd Airport Name"/>
            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Nearest Railway Station</strong></td>
            <td>    <strong>Display Priroty</strong>:
        <?php setPriority("railwayStationPriroty", $rec); ?>
                <input name="railway_station_distance" type="text" value="<?php echo($rec['railway_station_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("railwayStationDistanceUnit", $rec); ?>
                <input name="railway_station_name" type="text" value="<?php echo($rec['railway_station_name']) ?>" size="25%" placeholder="1st Railway Station Name"/>

                <input name="railway_station_distance1" type="text" value="<?php echo($rec['railway_station_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("railwayStationDistanceUnit1", $rec); ?>
                <input name="railway_station_name1" type="text" value="<?php echo($rec['railway_station_name1']) ?>" size="25%" placeholder="2nd Railway Station Name"/>


                <input name="railway_station_distance2" type="text" value="<?php echo($rec['railway_station_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("railwayStationDistanceUnit2", $rec); ?>
                <input name="railway_station_name2" type="text" value="<?php echo($rec['railway_station_name2']) ?>" size="25%" placeholder="3rd Railway Station Name"/>

            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Nearest Metro Station</strong></td>
            <td>
                <strong>Display Priroty</strong>:
                <?php setPriority("metroStationDistancePriroty", $rec); ?>
                <input name="metro_station_distance" type="text" value="<?php echo($rec['metro_station_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("metroStationDistanceUnit", $rec); ?>
                <input name="metro_station_name" type="text" value="<?php echo($rec['metro_station_name']) ?>" size="25%" placeholder="1st Metro Station Name"/>

                <input name="metro_station_distance1" type="text" value="<?php echo($rec['metro_station_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("metroStationDistanceUnit1", $rec); ?>
                <input name="metro_station_name1" type="text" value="<?php echo($rec['metro_station_name1']) ?>" size="25%" placeholder="2nd Metro Station Name"/>

                <input name="metro_station_distance2" type="text" value="<?php echo($rec['metro_station_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("metroStationDistanceUnit2", $rec); ?>
                <input name="metro_station_name2" type="text" value="<?php echo($rec['metro_station_name2']) ?>" size="25%" placeholder="3rd Metro Station Name"/>
            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Nearest Bus Stop</strong></td>
            <td>     <strong>Display Priroty</strong>:
                <?php setPriority("busStopDistancePriroty", $rec); ?>
                <input name="busstop_distance" type="text" value="<?php echo($rec['busstop_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("busStopDistanceUnit", $rec); ?>
                <input name="busstop_name" type="text" value="<?php echo($rec['busstop_name']) ?>" size="25%"  placeholder="1st Bus Stop Name"/>

                <input name="busstop_distance1" type="text" value="<?php echo($rec['busstop_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("busStopDistanceUnit1", $rec); ?>
                <input name="busstop_name1" type="text" value="<?php echo($rec['busstop_name1']) ?>" size="25%"  placeholder="2nd Bus Stop Name"/>

                <input name="busstop_distance2" type="text" value="<?php echo($rec['busstop_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("busStopDistanceUnit2", $rec); ?>
                <input name="busstop_name2" type="text" value="<?php echo($rec['busstop_name2']) ?>" size="25%"  placeholder="3rd Bus Stop Name"/>

            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Nearest Highway</strong></td>
            <td>    <strong>Display Priroty</strong>:
        <?php setPriority("highwaydistancePriroty", $rec); ?>
                <input name="highwaydistance" type="text" value="<?php echo($rec['highwaydistance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("highwaydistanceUnit", $rec); ?>
                <input name="highway_name" type="text" value="<?php echo($rec['highway_name']) ?>" size="25%"  placeholder="1st Highway Name"/>

                <input name="highwaydistance1" type="text" value="<?php echo($rec['highwaydistance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("highwaydistanceUnit1", $rec); ?>
                <input name="highway_name1" type="text" value="<?php echo($rec['highway_name1']) ?>" size="25%"  placeholder="2nd Highway Name"/>

                <input name="highwaydistance2" type="text" value="<?php echo($rec['highwaydistance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("highwaydistanceUnit2", $rec); ?>
                <input name="highway_name2" type="text" value="<?php echo($rec['highway_name2']) ?>" size="25%"  placeholder="3rd Highway Name"/>
            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left">
                <strong>Nearest Schools/Colleges</strong></td>
            <td>
                <strong>Display Priroty</strong>:
                <?php setPriority("schoolCollegeDistancePriroty", $rec); ?>
                <input name="schoolCollege_distance" type="text" value="<?php echo($rec['schoolCollege_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("schoolCollegeDistanceUnit", $rec); ?>
                <input name="schoolCollege_name" type="text" value="<?php echo($rec['schoolCollege_name']) ?>" size="25%"  placeholder="1st School/College Name"/>

                <input name="schoolCollege_distance1" type="text" value="<?php echo($rec['schoolCollege_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("schoolCollegeDistanceUnit1", $rec); ?>
                <input name="schoolCollege_name1" type="text" value="<?php echo($rec['schoolCollege_name1']) ?>" size="25%"  placeholder="2nd School/College Name"/>

                <input name="schoolCollege_distance2" type="text" value="<?php echo($rec['schoolCollege_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("schoolCollegeDistanceUnit2", $rec); ?>
                <input name="schoolCollege_name2" type="text" value="<?php echo($rec['schoolCollege_name2']) ?>" size="25%"  placeholder="3rd School/College Name"/>

            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Nearest Hospital</strong></td>
            <td>   <strong>Display Priroty</strong>:
        <?php setPriority("hospitalDistancePriroty", $rec); ?>
                <input name="hospital_distance" type="text" value="<?php echo($rec['hospital_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("hospitalDistanceUnit", $rec); ?>
                <input name="hospital_name" type="text" value="<?php echo($rec['hospital_name']) ?>" size="25%"  placeholder="1st Hospital Name"/>

                <input name="hospital_distance1" type="text" value="<?php echo($rec['hospital_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("hospitalDistanceUnit1", $rec); ?>
                <input name="hospital_name1" type="text" value="<?php echo($rec['hospital_name1']) ?>" size="25%"  placeholder="2nd Hospital Name"/>

                <input name="hospital_distance2" type="text" value="<?php echo($rec['hospital_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("hospitalDistanceUnit2", $rec); ?>
                <input name="hospital_name2" type="text" value="<?php echo($rec['hospital_name2']) ?>" size="25%"  placeholder="3rd Hospital Name"/>

            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Nearest Mall</strong></td>
            <td>   <strong>Display Priroty</strong>:
        <?php setPriority("mallDistancePriroty", $rec); ?>
                <input name="mall_distance" type="text" value="<?php echo($rec['mall_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("mallDistanceUnit", $rec); ?>
                <input name="mall_name" type="text" value="<?php echo($rec['mall_name']) ?>" size="25%"  placeholder="1st Mall Name"/>


                <input name="mall_distance1" type="text" value="<?php echo($rec['mall_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("mallDistanceUnit1", $rec); ?>
                <input name="mall_name1" type="text" value="<?php echo($rec['mall_name1']) ?>" size="25%"  placeholder="2nd Mall Name"/>

                <input name="mall_distance2" type="text" value="<?php echo($rec['mall_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("mallDistanceUnit2", $rec); ?>
                <input name="mall_name2" type="text" value="<?php echo($rec['mall_name2']) ?>" size="25%"  placeholder="3rd Mall Name"/>
            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">

            <td align="left"><strong>Nearest Bank/ATM</strong></td>
            <td>
                <strong>Display Priroty</strong>:
        <?php setPriority("bankAtmDistancePriroty", $rec); ?>
                <input name="bankAtm_distance" type="text" value="<?php echo($rec['bankAtm_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("bankAtmDistanceUnit", $rec); ?>
                <input name="bankAtm_name" type="text" value="<?php echo($rec['bankAtm_name']) ?>" size="25%"  placeholder="1st ATM/Bank Name"/>

                <input name="bankAtm_distance1" type="text" value="<?php echo($rec['bankAtm_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
                <?php distanceUnits("bankAtmDistanceUnit1", $rec); ?>
                <input name="bankAtm_name1" type="text" value="<?php echo($rec['bankAtm_name1']) ?>" size="25%"  placeholder="2nd ATM/Bank Name"/>

                <input name="bankAtm_distance2" type="text" value="<?php echo($rec['bankAtm_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("bankAtmDistanceUnit2", $rec); ?>
                <input name="bankAtm_name2" type="text" value="<?php echo($rec['bankAtm_name2']) ?>" size="25%"  placeholder="3rd ATM/Bank Name"/>

            </td>
        </tr>
        <tr class="<?php echo($clsRow1) ?>">

            <td align="left"><strong>Nearest Restaurants</strong></td>
            <td>
                <strong>Display Priroty</strong>:
        <?php setPriority("restaurantsDistancePriroty", $rec); ?>
                <input name="restaurants_distance" type="text" value="<?php echo($rec['restaurants_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("restaurantsDistanceUnit", $rec); ?>
                <input name="restaurants_name" type="text" value="<?php echo($rec['restaurants_name']) ?>" size="25%"  placeholder="1st Restaurants Name"/>

                <input name="restaurants_distance1" type="text" value="<?php echo($rec['restaurants_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("restaurantsDistanceUnit1", $rec); ?>
                <input name="restaurants_name1" type="text" value="<?php echo($rec['restaurants_name1']) ?>" size="25%"  placeholder="2nd Restaurants Name"/>

                <input name="restaurants_distance2" type="text" value="<?php echo($rec['restaurants_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("restaurantsDistanceUnit2", $rec); ?>
                <input name="restaurants_name2" type="text" value="<?php echo($rec['restaurants_name2']) ?>" size="25%"  placeholder="3rd Restaurants Name"/>


            </td>
        </tr>
        -->


        <!--******************* Property Distance Code ends **************-->
            <?php } ?>
            <?php if ($_SESSION['login']['role_id'] != "20") { ?>                              
        <tr class="<?php echo($clsRow1) ?>">

            <td align="left"><strong>Devloper Meta Title</strong></td>
            <td><textarea name="dev_meta_title"  id="dev_meta_title" rows="3" cols="150" ><?php echo $rec['dev_meta_title'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Devloper Meta Description</strong></td>
            <td><textarea name="dev_meta_description"  id="dev_meta_description" rows="3" cols="150" ><?php echo $rec['dev_meta_description'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Devloper Meta Keyword</strong></td>
            <td><textarea name="dev_meta_keywords"  id="dev_meta_keywords" rows="3" cols="150" ><?php echo $rec['dev_meta_keywords'] ?></textarea>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Show Type</strong></td>
            <td>
                <input type="radio" name="is_show_type" value="1" id="1" checked="checked" /> 
                <label for="1">Both</label> 
                <input type="radio" name="is_show_type" value="2" id="2" <?php
               if ($rec['is_show_type'] == "2") {
                   echo("checked='checked'");
               }
               ?>/> 
                <label for="2">CRM</label>
                <input type="radio" name="is_show_type" value="3" id="3" <?php
        if ($rec['is_show_type'] == "3") {
            echo("checked='checked'");
        }
        ?>/> 
                <label for="3">CMS</label>
            </td>
        </tr>

        <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Unit Type</strong></td>
            <td>
                <input type="radio" name="unit_type" value="1" id="1" checked="checked" /> 
                <label for="1">SQ.FT</label> 
                <input type="radio" name="unit_type" value="2" id="2" <?php
        if ($rec['unit_type'] == "2") {
            echo("checked='checked'");
        }
        ?>/> 
                <label for="2">SQ.YD</label>
                <input type="radio" name="unit_type" value="3" id="3" <?php
        if ($rec['unit_type'] == "3") {
            echo("checked='checked'");
        }
        ?>/> 
                <label for="3">SQ.M</label>
            </td>
        </tr>

    <?php } ?>

    <!-- *************************** new code added *****************************
     <?php if ($_SESSION['login']['role_id'] == "20") { ?>                              
        <?php
        if ($action == "Update") {
            //$sql="SELECT splproj.*, prop.property_name from ".TABLE_SPECIALS_PROJECTS." splproj  inner join ".TABLE_PROPERTIES." prop on splproj.proj_id=prop.id where splproj.special_id=".$_GET['id']." " ;

            $sql = "SELECT rera.rera_no,rera.id FROM " . TABLE_PROPERTIES_RERA . " as rera  where rera.prop_id=" . $_GET['id'] . "  ";

            $reca = ($obj_mysql->getAllData($sql));

            $countOfrec = count($reca);



            if ($reca != "") {
                $upCount = 1;
                ?>

                <?php
                foreach ($reca as $pecialproj) {

                    if ($upCount == 1) {
                        ?>


                        <tr class="<?php echo($clsRow1) ?>" id="replacetr">
                            <td align="right"><strong>Rera <?php echo $upCount; ?></strong></td>
                            <td><input name="rera_no[]" id="text_<?php echo $upCount; ?>" data=""  type="text" title="project1" value="<?php echo($pecialproj['rera_no']) ?>" size="50%" maxlength="120" />
                                <button  class="add" onclick="addMoreRows(this); return false;"></button>
                                <div id="hiddenProjId"><input  type="hidden" name="project_id[]" value="<?php echo($pecialproj['devlpr_id']) ?>"></div>

                        </tr>

                    <?php } else { ?>

                        <tr class="<?php echo($clsRow1) ?>" id="replacetr">
                            <td align="right"><strong>Rera <?php echo $upCount; ?></strong></td>
                            <td><input name="rera_no[]" id="text_<?php echo $upCount; ?>" data=""  type="text" title="project1"  value="<?php echo($pecialproj['rera_no']) ?>" size="50%" maxlength="120" /> <button class="delete" onclick="deleteRow(<?php echo $pecialproj['id'] ?>, this); return false;"></button>
                                <div id="hiddenProjId"><input  type="hidden" name="project_id[]" value="<?php echo($pecialproj['devlpr_id']) ?>"></div>
                        </tr>


                    <?php } ?>



                    <?php $upCount++;
                }
                ?>



            <?php } else { ?>

                <tr class="<?php echo($clsRow1) ?>">
                    <td align="right"><strong>Rera 1</strong></td>
                    <td><input name="rera_no[]" id="text_1" data=""  type="text" title="project1" value="<?php echo($rec['project1']) ?>" size="50%" maxlength="120" />
                        <button class="add" onclick="addMoreRows(this); return false;"></button></td>

                <?php
            }
        } else {
            ?>

            <tr class="<?php echo($clsRow1) ?>">
                <td align="right"><strong>Rera 1</strong></td>
                <td><input id="text_1" data=""  type="text" title="project1" name="rera_no[]" value="<?php echo($rec['project1']) ?>" size="50%" maxlength="120" />
                    <button class="add" onclick="addMoreRows(this); return false;"></button></td> 


        <?php }
        ?>
    <?php } ?>-->

    <?php if ($_SESSION['login']['role_id'] == "20") { ?>                              
    <tr class="<?php echo($clsRow1) ?>" id="replacetr">
                            <td align="left"><strong>Rera</strong></td>
                            <td><input name="rera_no" id="rera_no"  data=""  type="text" title="Rera Number" value="<?php echo $rec['rera_no']; ?>" size="50%" maxlength="120" />
                                
    </tr>
<?php } ?>
    
    <tr class="clsRow1uuu" id="newRows"></tr>

        <?php if ($_SESSION['login']['role_id'] != "20") { ?>                              
        <!-- =============================Chnages Start===================================== -->
        <tr>
            <td align="left"><strong>Total units</strong></td>
            <td><input type="text" name="total_unit" value="<?php echo($rec['total_unit']) ?>"></td>
        </tr>
        <tr>
            <td align="left"><strong>Block/Floor</strong></td>
            <td><input type="text" name="block" value="<?php echo($rec['block']) ?>"></td>
        </tr>

        <tr>
            <td align="left"><strong>Area</strong></td>
            <td><input type="text" name="build_area" value="<?php echo($rec['build_area']) ?>"></td>
        </tr>


        <tr>
            <td align="left"><strong>Is_xlr8</strong></td>
            <td>
                <input type="radio" name="is_xlr8" value="1" <?php if($rec['is_xlr8']=="1"){ echo 'checked';}?> >Yes  
                <input type="radio" name="is_xlr8" value="0" <?php if($rec['is_xlr8']!="1"){ echo 'checked';}?> >No
            </td>
        </tr>

         <tr>
            <td align="left"><strong>Is Curated</strong></td>
            <td>
                <input type="radio" name="is_curated" value="1" <?php if($rec['is_curated']=="1"){ echo 'checked';}?> >Yes  
                <input type="radio" name="is_curated" value="0" <?php if($rec['is_curated']!="1"){ echo 'checked';}?>>No
            </td>
        </tr>

    <?php } ?>
    <tr>
        <td align="left"><strong>Specifications</strong></td>
        <td>
            <ul class="firstul">


    <?php
    $sql = "SELECT * FROM " . TABLE_COMMERCIAL_SPECIFICATION;
    $flooring = ($obj_mysql->getAllData($sql));

    foreach ($flooring as $floor_val) {
        ?>

                    <li>
                        <input type="checkbox" name="specification[]" value="<?php echo $floor_val['id']; ?>" <?php if(in_array($floor_val['id'], explode(",",$rec['specification']))){ echo 'checked=""';} ?> ><?php echo $floor_val['name']; ?>


        <?php
    }
    ?>
            </ul>

        </td>
    </tr>




    <tr>
        <td align="left"><strong>Other</strong></td>
        <td>
            <!-- <textarea name="other"  rows="3" cols="150"></textarea> -->
            <textarea class="ckeditor" name="other"  id="other" rows="4" cols="70" ><?php echo $rec['other'] ?></textarea>

        </td>
    </tr>

    <style type="text/css">
        .firstul li{

            padding:5px;
        }

        .secondul {

            padding-left: 20px;

        }

    </style>

    <!-- =============================Chnages End===================================== -->



    <!-- **************************************************new code added ********************************* -->

    <tr class="<?php echo($clsRow1) ?>">
            <td align="left"><strong>Facilities </strong> </td>
            <td>
                <table>
                    <tr>
                        <td width="30"><input name="suttle_service_point" type="checkbox" class="facilities_checkbox" value="<?php echo $rec['suttle_service_point'] ?>" <?php if($rec['suttle_service_point']==1){ echo 'checked=""';}?> /> <strong>Suttle Service Point</strong>:</td>
                        <td><input name="suttle_service_point_distance" type="text" value="<?php echo $rec['suttle_service_point_distance'] ?>"  maxlength="5" placeholder="Distance in kilometers" class="allownumericwithdecimal" lang="DBL" title="nearest suttle service point distance in kilometers" <?php if($rec['suttle_service_point']==1){ echo 'required="required"';}else{ echo 'readonlyy="readonlyy"';}?> /></td><td>(Distance in KM)</td>
                    </tr>
                    <tr>
                        <td><input name="metro" type="checkbox" class="facilities_checkbox" value="<?php echo $rec['metro'] ?>" <?php if($rec['metro']==1){ echo 'checked=""';}?> /> <strong>Metro</strong>:</td>
                        <td><input name="metro_distance" type="text" class="allownumericwithdecimal" lang="DBL" title="nearest metro distance in kilometers" value="<?php echo $rec['metro_distance'] ?>"  maxlength="5" placeholder="Distance in kilometers" <?php if($rec['metro']==1){ echo 'required="required"';}else{ echo 'readonlyy="readonlyy"';}?> /></td><td>(Distance in KM)</td>
                    </tr>
                    <tr>
                        <td><input name="bus_stand" type="checkbox" class="facilities_checkbox" value="<?php echo $rec['bus_stand'] ?>" <?php if($rec['bus_stand']==1){ echo 'checked=""';}?> /> <strong>Bus Stand</strong>:</td>
                        <td><input name="bus_stand_distance" type="text"  class="allownumericwithdecimal" lang="DBL" title="nearest bus stand distance in kilometers" value="<?php echo $rec['bus_stand_distance'] ?>"  maxlength="5" placeholder="Distance in kilometers" <?php if($rec['bus_stand']==1){ echo 'required="required"';}else{ echo 'readonlyy="readonlyy"';}?> /></td><td>(Distance in KM)</td>
                    </tr>
                    <tr>
                        <td><input name="taxi_auto" type="checkbox" class="facilities_checkbox" value="<?php echo $rec['taxi_auto'] ?>" <?php if($rec['taxi_auto']==1){ echo 'checked=""';}?> /> <strong>Taxi/Auto Service</strong>:</td>
                        <td><input name="taxi_auto_distance" type="text"  class="allownumericwithdecimal" lang="DBL" title="nearest taxi/auto distance in kilometers" value="<?php echo $rec['taxi_auto_distance'] ?>"  maxlength="5" placeholder="Distance in kilometers" <?php if($rec['taxi_auto']==1){ echo 'required="required"';}else{ echo 'readonlyy="readonlyy"';}?> /></td><td>(Distance in KM)</td>
                    </tr>
                </table>
                
                
                
            </td>
        </tr>
    <tr class="<?php echo($clsRow2) ?>"><td align="left">&nbsp;</td>
        <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
            <input name="button" type="button" class="button" onclick="widthindow.location = '<?= $page_name ?>'" value="Cancel" /></td>
    </tr>
<?php } else { ?>
    <tr class="<?php echo($clsRow1) ?>">
        <td align="left"><strong>Upload Listing</strong></td>
        <td><select class="form-control" name="import_type">
                <option value="supply" selected>Supply</option>
                <option value="moderate_property">Moderate Property</option>
            </select></td>
    </tr>
    <tr class="<?php echo($clsRow2) ?>">
        <td align="left"><strong>Upload Listing</strong></td>
        <td><button type="button" class="btn btn-primary btn-flat"><i class="fa fa-upload"></i> Upload File
                <input type="file" class="fileUpload" id="propety_list" accept=".xls,.xlsx" name="propety_list">
            </button></td>
    </tr>
    <tr class="<?php echo($clsRow1) ?>"><td align="left">&nbsp;</td>
        <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
            <input name="button" type="button" class="button" onclick="window.location = '<?= $page_name ?>'" value="Cancel" /></td>
    </tr>
<?php } ?>




</tbody>
</table>
</form>
<link href="<? echo C_ROOT_URL ?>/view/css/auto-search.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<? echo C_ROOT_URL ?>/view/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<? echo C_ROOT_URL ?>/view/js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript">

$(".pre_lease_option").hide();
if("<?= $rec['property_type'];?>"==7)
{
    $(".pre_lease_option").show();
}


$("#property_type").on("change",function(){
    var value = $(this).val();
    if(value==7)
    {
        $(".pre_lease_option").show();
    }
    else
    {
        $(".pre_lease_option").hide();
    }
});
            var urlSent = "";
            function showFResult()
            {
                urlSent = document.getElementById("cty_id").value;

                $(function () {

                    var urlSet = "http://mypanel.buyproperty.com/property_search.php?cityId=" + urlSent;
                    $("#zipsearch").autocomplete({
                        source: urlSet,
                        minLength: 2,

                        html: true, // optional (jquery.ui.autocomplete.html.js required)

// optional (if other layers overlap autocomplete list)
                        open: function (event, ui) {

                            $(".ui-autocomplete").css("z-index", 1000);
                        }
                    });

                });
            }
</script> 


<?php
if ($action == "Update" && $reca != "") {

    $count = $countOfrec + 1;
} else {
    $count = 2;
}
?>


<script type="text/javascript">



    var rowCount = <?php echo $count; ?>;
    function addMoreRows(row) {

        var recRow = '<tr id="newRow_' + rowCount + '" class="clsRow1"><td align="right"><strong>Rera ' + rowCount + '</strong></td><td><input data="" id="text_' + rowCount + '" type="text" name="rera_no[]" size=50% maxlength="120"><button onclick="removeRow(this); return false;">Remove</button></td></tr>';

        jQuery('#newRows').before(recRow);
        rowCount++;
    }

    function removeRow(removeNum) {

        console.log(removeNum);
        var test = $(removeNum).parent().parent();
        console.log(test);
        $(test).remove();
    }
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



<script>


    function test(id)
    {

        var base_url = "<?php echo C_ROOT_URL ?>";

        cityId = "";
//console.log(list);

        $(function () {
            //var projectlist=list;

            $("#" + id).autocomplete({
                minLength: 0,
                source: base_url + "/property/view/delfRecord.php?cityId=" + cityId,
                focus: function (event, ui) {
                    $("#" + id).val(ui.item.label);
                    return false;
                },
                select: function (event, ui) {
                    $("#" + id).val(ui.item.value);

                    $("#" + id).val(ui.item.label);
                    $("#" + id).parent().find("#hiddenProjId").remove();

                    $("#" + id).parent().find("#newhidden").remove();
                    //$("#"+id).find("#hiddenProjId").remove();
                    // /console.log($("#"+id ).closest('td:input').next().attr('data',ui.item.value));
                    $("#" + id).append('<input id="newhidden" type="hidden" name="prop_id[]" value="' + ui.item.value + '">');
                    //$( "#"+id ).attr('name',ui.item.value);
                    //$( ".project-icon" ).attr( "src", "images/" + ui.item.icon );

                    return false;
                }
            })
                    .autocomplete("instance")._renderItem = function (ul, item) {
                return $("<li>")
                        .append("<a>" + item.label + "</a>")
                        .appendTo(ul);
            };
        });

    }
</script>


<script>
    function deleteRow(ID, thisref)
    {

        if (confirm("Are you sure you want to delete this record"))
        {
            var base_url = "<?php echo C_ROOT_URL ?>";

            $(thisref).parent().parent().remove();


            $.ajax({
                //url: "view/changepoststatus.php",
                url: base_url + "/property/view/delRecord.php",
                type: 'POST',
                data: {rec_id: ID},
                success: function (data) {

                    // location.href=base_url+"/specials/index.php";
                    //jQuery("#rec_"+recid).replaceWith(data);

                }
            });
        }


    }
    $(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    <?php if ($_SESSION['login']['role_id'] != "20"){?>
        // do not remove this comment 
    // $("#landzone").attr('disabled', 'disabled');
    // $("#country_id").attr('disabled', 'disabled');
    // $("#cty_id").attr('disabled', 'disabled');
    // $("#location").attr('disabled', 'disabled');
    // $("#zipsearch").attr('disabled', 'disabled');

    <?php }?>

    $(".facilities_checkbox").on("click",function(){
        if($(this).is(":checked")) 
        {
            $(this).val(1);
            $(this).closest("tr").find('input[type="text"]').attr("required", "true");
            $(this).closest("tr").find('input[type="text"]').removeAttr("readonlyy", "true");
        }
        else
        {
            $(this).val(0);
            $(this).closest("tr").find('input[type="text"]').removeAttr("required", "true");
            $(this).closest("tr").find('input[type="text"]').attr("readonlyy", "true");
            $(this).closest("tr").find('input[type="text"]').val('');
        }
    });

    $("#possession_type").on("change",function(){
        var value = $(this).val();
        if(value=="Hand Over")
        {
            $("#possession_date").show();
            $("#tcalico_1").show();
        }
        else
        {
            $("#tcalico_1").hide();
            $("#possession_date").hide();
        }
    });

    $("devlpr_id").attr("required", "true");

    $(document).on("change","#cty_id",function(){
        var cty_id = $(this).val();
            $.ajax({
                type:"GET",
                url:"<?php echo $site['CMSURL'].'commercial/populate_micro_market.php';?>",
                data:{"cty_id":cty_id},
                success:function(res)
                {
                    $("#microMarketDiv").html(res);
                }
            });
    });


</script>
