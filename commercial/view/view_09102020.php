<?php 
include_once ("../library/server_config_admin.php");
admin_login();
?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<style>
/* Popup container - can be anything you want */
.popup {
  position: relative;
  display: inline-block;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* The actual popup */
.popup .popuptext {
  visibility: hidden;
  width: 160px;
  background-color: #555;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 8px 0;
  position: absolute;
  z-index: 1;
  bottom: 125%;
  left: 50%;
  margin-left: -80px;
}

/* Popup arrow */
.popup .popuptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup .show {
  visibility: visible;
  -webkit-animation: fadeIn 1s;
  animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
  from {opacity: 0;} 
  to {opacity: 1;}
}

@keyframes fadeIn {
  from {opacity: 0;}
  to {opacity:1 ;}
}
</style>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<!-- -------------------------------------------------------------  -->
			<!-- PROPERTY DETAILS Search Code Block Start Here -->
<!-- --------------------------------------------------------------  -->
<?
if($_GET["action"]!="Create" && $_GET["action"]!="Update"){?>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="6" align="left"><h3 class="user_serch_head" <?php if($_SESSION['login']['role_id']==20){?> style="display:none;"<?php } ?>>Search New Projects</h3>
<button <?php if($_SESSION['login']['role_id']==20){?> style="display:none;"<?php } ?> id="'.$orderID.'"  onclick="generate_sitemap(); return false;" >Generate SiteMap </button>
<span id="sitedown"></span>
<?php if(in_array($_SESSION['login']['role_id'],array("20"))){?>
<a href="<?=$page_name?>?action=BulkUpload" class="add">Upload Property</a>
<a href="<?=$page_name?>?action=Create" class="add">Add New</a>
<?php }?>
</th></tr>

</thead>
<tbody> 
   <tr  class="<?php echo($clsRow1)?>">
<form  method="get"  name="searchForm"  >
<input type="hidden" name="action" value="search">
  <td align="right" style="width:80px;"><strong>Cities</strong><?php 
    $select_cty_id = isset($_GET['cty_id']) ? $_GET['cty_id']:-1;
  echo AjaxDropDownListAdvanceCity(TABLE_CITIES,"cty_id","id","city",1,$select_cty_id,"city",$site['CMSURL']."commercial/populate_location.php","locationDiv","locationLabelDiv","country_id",'in_commercial','1');?>
  
</td>
  <td style="width:425px;" <?php if ($_SESSION['login']['role_id']==20) { ?> colspan="3" <?php } ?> >

	<div style="float:right; margin:0 10px;">

<div  name="locationDiv" id="locationDiv">
    <span style="font:bold 12px arial;">Micro Market</span>
    <select name="loc_id" id="loc_id" style="width:234px;"><option value="">-- Select --</option></select>
</div>
        </div>
  </td>

  <td ><strong>Property Name </strong><input type="text" name="propertyName" title="Property Name"  value="<?php echo($_GET['propertyName'])?>">
  </td>

  <td><input name="submit" type="submit" class="button" value="Search Properties" /> 
  <input name="button" id="button" type="button" class="button" value="Reset" />
</form>

</tr>
<tr  class="<?php echo($clsRow1)?>">
 <td style="width:80px;" <?php if ($_SESSION['login']['role_id']!=20) { ?> colspan="6" <?php } ?> ><strong>Action</strong><select name="performAction" id="performAction" title="performAction" style="width:140px;">
          <option value="0">Please Select Action</option><
      <?php if ($_SESSION['login']['role_id']==20) { ?>
          <option value="1">Mark As Duplicate</option>
	         <option value="2">Assign To Marketing</option>
        <?php } ?>
  <?php if ($_SESSION['login']['role_id']!=20) { ?> 
        <option value="3">Active</option> 
        <option value="4">Inactive</option>
        <option value="5">Reassign To Commercial Team</option>
      <?php } ?> 
	</select>
</td>
<?php if ($_SESSION['login']['role_id']==20) { ?> 
<td> <span style="font:bold 12px arial;margin-right: 10%;"align="right">Pulling Requests <lable style="margin-left: 5px;" >
     <?php 
      $childsql="SELECT id from tbl_properties_commercial where";
       if ($_SESSION['login']['role_id']==20) {
        $childsql.=" assign_to=".$_SESSION['login']['id'];
        }else{
            $childsql.=" assign_to_marketing= 1";
        }
      $sql = "SELECT count(*) as pulled_data from tsr_pulled_property where property_id in ($childsql)";
      $result = mysql_query($sql);
      $row = mysql_fetch_assoc($result);
     ?>
     <lable><?php   if($row['pulled_data'] >0)
     echo'<a href="/commercial/index.php?action=pulledData" style="color:#000000;">'.$row['pulled_data'].'</a>'; else echo'0';?></span> </td>
<td> <span style="font:bold 12px arial;margin-right: 10%;"align="right">Listings expiring <lable style="margin-left: 5px;" >
     <?php 
       $sql = "SELECT count(*) as is_expire from tbl_properties_commercial where";
       if ($_SESSION['login']['role_id']==20) {
        $sql.=" assign_to=".$_SESSION['login']['id'];
        }else{
            $sql.=" assign_to_marketing= 1";
        }
 
        $sql.="  and is_expire=1";
        $result = mysql_query($sql);
        $row = mysql_fetch_assoc($result);
     ?>
     <lable><?php   if($row['is_expire'] >0)
     echo'<a href="/commercial/index.php?action=search&condition=is_expire" style="color:#000000;">'.$row['is_expire'].'</a>'; else echo'0';?><lable>
     </span></td>
  <td>
  <div style="margin-left: 55%;margin-top: -15px;">
      <span style="font:bold 12px arial;"align="right" style="width:80px;">Today's Task</span><lable style="margin-left: 5px;" id="countValue">
      <?php 
       $sql = "select count(*) as countCurrentDate,created_date from tbl_properties_commercial where";
       if ($_SESSION['login']['role_id']==20) {
        $sql.=" assign_to=".$_SESSION['login']['id'];
        }else{
            $sql.=" assign_to_marketing= 1";
        }
 
        $sql.=" DATE(created_date)=CURDATE()";
          $result = mysql_query($sql);
          $row = mysql_fetch_assoc($result);
     ?>
     <lable><?php   if($row['countCurrentDate'] >0)
     echo'<a href="/commercial/index.php?action=search&condition=today">'.$row['countCurrentDate'].'</a>'; else echo'0';?><lable>
    </div>
  </td>
  <td align="right" style="width:80px;"> 
    <div>
      <span style="font:bold 12px arial;margin-right: 10%;"align="right"> Missing Broker/Owner Info</span><lable style="margin-left: 5px;"><?php 
       
       $sql = "select count(*) as  name  from tbl_properties_commercial where";
       if ($_SESSION['login']['role_id']==20) {
        $sql.=" assign_to=".$_SESSION['login']['id'];
        }else{
            $sql.=" assign_to_marketing= 1";
        }
 
        $sql.="  and  (user_first_name='' or user_first_name is null or user_mobile='' or user_mobile is null  or user_email='' or user_email is null)";
        
          $result = mysql_query($sql);
          $row = mysql_fetch_assoc($result);
     ?><?php  if($row['name'] >0)
     echo'<a href="/commercial/index.php?action=search&condition=infomissing" style="color:#000000;">'.$row['name'].'</a>'; else echo'0';?></lable>
    
    </div>
  </td>
  <td> 
  <div>
  <span style="font:bold 12px arial;"align="right" style="width:80px;">Missing Documents</span><lable style="margin-left: 5px;" id="countValue">
     <?php 
       
       $sql = "select count(*) as fileDocument from tbl_properties_commercial where";
       if ($_SESSION['login']['role_id']==20) {
        $sql.=" assign_to=".$_SESSION['login']['id'];
        }else{
            $sql.=" assign_to_marketing= 1";
        }
 
        $sql.="  and fire_noc is null";
        
          $result = mysql_query($sql);
          $row = mysql_fetch_assoc($result);
     ?>
     <lable><?php   if($row['fileDocument'] >0)
     echo'<a href="/commercial/index.php?action=search&condition=documentmissing" style="color:#000000;">'.$row['fileDocument'].'</a>'; else echo'0';?><lable>
    </div>
  
  </td>
 <?php }?>
</tr>
<!-- -------------------------------------------------------------  -->
			<!-- PROPERTY DETAILS Search Code Block End Here -->
<!-- --------------------------------------------------------------  -->
</table>
<style>
.tooltip {
  position: relative;
  display: inline-block;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 220px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
</style>
<table width="100%" border="0" cellpadding="0" cellspacing="2" class="paging" >
<tr>
<td width="285" nowrap="nowrap">&nbsp;</td>
<td width="43" align="right" nowrap="nowrap" class="pagenumber"></td>
<td width="635" align="right" class="prevnext"><?php echo($paging);?></td>
</tr>
</table>

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="MainTable">
    <thead>
      <tr>
          <?php 
          if (in_array($_SESSION['login']['role_id'], array(20))) {
            ?> 
                <th>Notification</th>
                
          <?php } ?>
                <th ><input type="checkbox" name="checkbox_for_all"></th>
                <th ><div>Reassign To Commercial</div></th>
                <th ><div>Property Name</div></th>
        		<th ><div>Micro Market</div></th>
        		<th ><div>City Name</div></th>
        		<!--<th ><div>Meta Title</div></th>
        		<th ><div>Meta Keywords</div></th>
        		<th ><div>Meta Description</div></th>
        		<th ><div>Edited By</div></th>-->
        		<th ><div>Created Date</div></th>
        <th ><div>Status</div></th>
        <th ><div>Unit No</div></th>
        <th ><div>Property On</div></th>
        <?php 
          if (in_array($_SESSION['login']['role_id'], array(20))) {
            ?>

        <th ><div>Broker/Owner</div></th>
        <th ><div>Duplicate</div></th>
        <th ><div>Assign To Name</div></th>
        <th ><div>Assign To Email</div></th>
        <th ><div>Assign To Marketing</div></th>
        <?php } ?>

        <th ><div>Preview Property</div></th>
        <th ><div>Action</div></th>
      </tr>
    </thead>
    <tbody>
      <?php echo($list);  ?>
    </tbody>
  </table>
 <div class="modal fade" id="modalfade" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reassign Property History</h4>
        </div>
        
          <table  align="center" border="1" cellspacing="0" cellpadding="0" style="border:1px solid #dadada; /*! margin:auto; */width: 100%;">
                    <thead>
                       <tr>
                        <th style="text-align: center;">Property ID</th>
                        <th style="text-align: center;">Comment</th>
                      </tr>
                    </thead>
                    <tbody id="propertyHistory">
                      
                    </tbody>
                     
                    </table>
         
       
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- notify property -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Reassign Property</h4>
        </div>
        <form class="disp_formgrc">
        <div class="modal-body" id="modalbody">
        <div class="form-group"><label for="exampleFormControlTextarea1">Description</label>
          <textarea class="form-control" name="commdescription" rows="3"></textarea>
          <input type="hidden" name="commid" id="commid" value=''></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="submit">Send</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<!-- end notiy property-->



<table cellpadding="2" cellspacing="0" width="100%"  border="0" class="paging">
  <tr>
    <td colspan="2" valign="top" align="right" class="pagenumber" style="padding-right:30px;"><?php echo($total_rec);?> </td>
  </tr>
  <tr>
    <td colspan="2" valign="top"></td>
  </tr>
</table>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fullHeightModalRight" style="display: none;">
  Launch demo modal
</button>

   
<script type="text/javascript" src="<?php echo C_ROOT_URL ?>/commercial/lib/jquery-1.10.1.min.js"></script>

<script>
function conf(){
	return confirm('Are you want to delete it?');
}
function generate_sitemap()
{
  
if(confirm("Are you sure you want to Generate Sitemap"))
{
var base_url="<?php echo C_ROOT_URL ?>";
  $.ajax({
        //url: "view/changepoststatus.php",
        url:base_url+"/commercial/view/generate_sitemap.php",
        
        type: 'POST',
        data: {},
        success: function(data) {   
          $('#sitedown').html(data);  
        }
    });
}
}
var base_url="<?php echo C_ROOT_URL ?>";
$.noConflict();
$('#performAction').on('change',function(e){
  
if($(this).val() >0){
  var checked = $("input[name='comm_id[]']:checked").length > 0;
  if(checked)
  {
   if($(this).val()==5){
    var description='';
    $('#myModal').modal('show'); 

    if($("input[type='checkbox']:checked").val()!='')
    {
      $('#commid').val($("input[type='checkbox']:checked").val());
    }
   
  }else{
      if(confirm("Are you sure you want to Perform this Action")){
          var favorite = [];
            $.each($("input[name='comm_id[]']:checked"), function(){
                favorite.push($(this).val());
            });
            var string = "?selectAction=selectAction&type="+$(this).val()+"&comm_id="+favorite.join(",");
           
            window.location.href=base_url+"commercial/index.php"+string;
    }

  }
    
  }
  else
  {
    alert("Please select at least one property!");
  }
}
});
$("#cty_id").on("change",function(){
    $.ajax({
        url:base_url+"/commercial/populate_locations_advance.php",
        type: 'GET',
        data: {'cty_id':$(this).val()},
        success: function(data) {  
          $('#loc_id').empty(); 
          $('#loc_id').append(data);
        }
    });
});

</script>
<?php if($_GET['cty_id'] >0 ){?>
<script>
    var city_id ="<?php echo $_GET['cty_id'];?>";
    var loc_id ="<?php echo $_GET['loc_id'];?>";
$.ajax({
        url:base_url+"/commercial/populate_locations_advance.php",
        type: 'GET',
        data: {'cty_id':city_id},
        success: function(data) {   
          $('#loc_id').empty(); 
          $('#loc_id').append(data); 
          $("#loc_id").val(loc_id);
        }
    });
    </script>
<?php } ?>
<script>
var base_url="<?php echo C_ROOT_URL ?>";
  function searchDataCurrentDate()
  {
    var today = new Date().toISOString().slice(0, 10)
    $.ajax({
          url:base_url+"/commercial/controller/controller.php",
          type: 'GET',
          data: {'created_date':today,'action':'search'},
          success: function(data) {  
            
          }
      });

  }
  $('#button').on('click',function(){
    window.location.href = base_url+"commercial/index.php";
  });

  $("input[type='checkbox'][name='checkbox_for_all']").on("click",function(){
    // $("input[name='comm_id[]']    
    if($(this).is(":checked")) {
      $("input[name='comm_id[]']").prop('checked', true);  
    }
    else
    {
      $("input[name='comm_id[]']").prop('checked', false);    
    }

  });

</script>
<script>
  var base_url="<?php echo C_ROOT_URL ?>";
 $(function() {
//twitter bootstrap script
 $("#submit").click(function(){
 
  
         $.ajax({
           type: "POST",
           url: base_url+"/commercial/controller/controller.php",
           data: $('.disp_formgrc').serialize(),
          success: function(msg){
           window.location.reload(true);     
       // $("#form-content").modal('hide'); 
         },
       error: function(){
       alert("failure");
       }
      });
 });
});
</script>
<script>

$(document).ready(function(){
  var base_url="<?php echo C_ROOT_URL ?>";
  $(".faaddress").mouseover(function(){
     
     var proId=$(this).attr('data-value');
     $.ajax({
          url:base_url+"/commercial/controller/controller.php",
          type: 'GET',
          data: {'propertyID':proId},
          success: function(data) {  
            
            if(data.length>2){
              $('#modalfade').modal('show');
              $('#propertyHistory').html(data);
               
             
            }else{
              $('#modalfade').modal('hide');
          // $('#modalfade').modal('hide');
              
            }
           
          }
      });
   //$('#modalfade').modal();
  });
  
});
</script>
