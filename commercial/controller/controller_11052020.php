<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
mb_internal_encoding("8bit");
define('TABLE_PROPERTIES_RERA', 'tsr_properties_rera');
// include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");

include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once LIBRARY_PATH . "commercial/excle-lib/Excel_reader.php";
include_once LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php";
admin_login();
$message_arr = array("insert" => "Record(s) has been added successfully!",
    "update" => "Record(s) has been updated successfully!",
    "delete" => "Record(s) has been deleted successfully!",
    "cstatus" => "Record(s) status has been changed!",
    "unique" => "Property Name already in record!",
    "not_upload" => "Property file does not supported!",
    "upload" => "Property file uploaded!",
);

//exit;
$tblName = TABLE_COMMERCIAL_PROPERTIES; //main table name
$fld_id = 'id'; //primery key
$fld_status = 'status'; // staus field
$fld_orderBy = 'id'; // default order by field
$page_name = C_ROOT_URL . '/commercial/controller/controller.php';
$page_name_insert = C_ROOT_URL . '/image/controller/controller.php';
$page_name_update = '';
$clsRow1 = 'clsRow1';
$clsRow2 = 'clsRow2';
$action = remRegFx($_REQUEST['action']); // action to perform tast like Update, Create , Delete etc.
$rec_id = remRegFx($_GET['id']);
$arr_id = $_POST['items'] ? $_POST['items'] : array($rec_id); // for radio button
$query_str = "page=$page";
//print_r($_POST);

$file_name = "";
switch ($action) {
    case 'Create':
    case 'Update':
        $file_name = ROOT_PATH . "commercial/model/model.php";
        break;
    case 'BulkUpload':
        $file_name = ROOT_PATH . "commercial/model/model.php";
        break;
    default:
        $file_name = ROOT_PATH . "commercial/view/view.php";
        break;
}

//print_r($_POST);
if(isset($_GET['comm_id'])){

    $id= $_GET['comm_id'];
    $type = $_GET['type'];
    updatePropertyRecord($id,$type,$obj_mysql);
}

//echo $_SESSION['login']['email'];exit;
if(isset($_POST['commid'])){
    $commid=$_POST['commid'];
    $commdescription=$_POST['commdescription'];
    $login_userID=$_SESSION['login']['id'];
    
    $login_email=$_SESSION['login']['email'];
   $login_username= $_SESSION['login']['name'];
    propertySendEmailRecord($commid,$commdescription,$login_userID,$login_email,$login_username,$obj_mysql);

}

if(isset($_GET['propertyID']))
{

    $proId=$_GET['propertyID'];
    $login_id=$_SESSION['login']['id'];
    getCommercialCommentHistory($proId,$login_id,$obj_mysql);
    
}
//echo $_GET['created_date'];exit;
if(isset($_POST['is_upload']) && $_POST['is_upload']>0){
            //$fileName = date('Y-m-d-H-i-s').'_'.$_SESSION['login']['id'].'-'.$_FILES['propety_list']['name'];
            $fileName = $_FILES['propety_list']['name'];
            $_FILES['file']['name'] = $fileName;
            $_FILES['file']['type'] = $_FILES['propety_list']['type'];
            $_FILES['file']['tmp_name'] = $_FILES['propety_list']['tmp_name'];
            $_FILES['file']['error'] = $_FILES['propety_list']['error'];
            $_FILES['file']['size'] = $_FILES['propety_list']['size'];
           
             $uploaddir2  = UPLOAD_PATH_COMMERCIAL_PROPERTY_FILE;
             $uploadfile2 = $uploaddir2.basename($_FILES['file']['name']);
             //echo'==>'.$_FILES['file']['tmp_name'];
            // echo'<br />';
             if (!file_exists($uploaddir2)) {
		            //chmod(UPLOAD_PATH_COMMERCIAL_PROPERTY_FILE, 0777);
		            mkdir($uploaddir2, 0777, true);
		           //die("####");
                            @unlink($uploaddir2.$uploadfile2);
	       }
             
             if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile2)){
                $msg = "upload";
                ImportXlsxFile($uploaddir2, $fileName, $_POST['import_type'],$obj_mysql);
                                chmod($uploadfile2, $fileName, 0777);
             }
             else
             {
                $msg = "not_upload";
             }
            
}
if($action !="BulkUpload"){
 
# Users with role '360Xlr8' can only search, modify, add properties mapped as is_xlr8
if ($_SESSION['login']['role_id']==20) {
    //$is_xlr8_user = 1;
    $is_xlr8_user = 0;
}

//if ($_GET["action"] != "" && $_GET["action"] != "Create") {
 //if ($_GET["action"] == "search" && $_GET["msg"] != "update") {
if ($_GET["action"] != "Create") {
    //echo $_GET["created_date"];exit;
    if (($_GET["action"] == "" || $_GET['action'] == "pulledData" || $_GET["action"] == "search") && $_GET["msg"] != "update") {
        //$sql = "SELECT pro.*, loc.location,cty.city FROM " . TABLE_COMMERCIAL_PROPERTIES . " pro INNER JOIN tsr_locations loc ON pro.location=loc.id INNER JOIN tsr_cities cty ON pro.cty_id=cty.id WHERE ";
        if($_GET['action'] == "pulledData"){
            $sql = " SELECT pro.* from ".TABLE_COMMERCIAL_PROPERTIES." as pro inner join ".TABLE_PULLED_PROPERTY." as tpp on pro.id = tpp.property_id WHERE";
        }else{
        $sql = "SELECT pro.* FROM " . TABLE_COMMERCIAL_PROPERTIES . " as pro WHERE ";
        }

        if ($_SESSION['login']['role_id']==20) {
        $sql.=" pro.assign_to=".$_SESSION['login']['id'];
        }else{
            $sql.=" pro.assign_to_marketing= 1";
        }

        if ($_GET["propertyName"] != "") {
            $sql .= " and pro.property_name like '%" . trim($_GET["propertyName"]) . "%'";
        }

        if ($_GET["cty_id"] != "" ) {
            $sql .= " and pro.cty_id=" . $_GET["cty_id"];
        }

        if ($_GET["loc_id"] != "") {
            $sql .= " and  pro.loc_id=" . $_GET["loc_id"];
        }
        if($_GET["condition"] == "today"){
            $sql .= " and DATE(pro.created_date)=".date('Y-m-d');
        }
        if($_GET["condition"] == "infomissing"){
            $sql .= " and (pro.user_first_name='' or pro.user_first_name is null or pro.user_mobile='' or pro.user_mobile is null  or pro.user_email='' or pro.user_email is null)";
        }
        if ($_GET["condition"] == "fileDocument" || $_GET["condition"] == "documentmissing") {
            $sql .= " and  pro.fire_noc is null" ;
        }

        if ($_GET["condition"] == "is_expire") {
            $sql .= " and  pro.is_expire=1" ;
        }
       
        
        /*if ($is_xlr8_user == 1 ) {
            $sql .= " AND pro.is_xlr8 = 1";
        }*/

        if ($_GET["pid"] != "") {
            $sql .= " and pro.id=" . $_GET["pid"];
        }
        
        //if($_GET["status"]=="" && $_GET["propertyName"]=="")

        //$sql.=" and pro.status=1";
//$sql = "SELECT * FROM " . TABLE_COMMERCIAL_PROPERTIES . " WHERE ";

       // echo $sql .= " Order BY created_date DESC"; 

    } else if ($_GET["action"] == "Update") {
        $sql = "SELECT * FROM " . TABLE_COMMERCIAL_PROPERTIES . " Where $fld_id='$rec_id'";
       //  echo $sql;
    //exit;
    }
    $s = ($_GET['s'] ? 'ASC' : 'DESC');
    $sort = ($_GET['s'] ? 0 : 1);
    $f = $_GET['f'];

    if ($_GET["msg"] != "update") {
        if ($s && $f) {
            
            $sql .= " ORDER BY $f  $s";
        } else
        //$sql.= " ORDER BY $fld_orderBy desc";
        {
            
            $sql .= " Order BY created_date DESC";
        }

        /*---------------------paging script start----------------------------------------*/
        //echo $sql;
        $obj_paging->limit = 30;
        if ($_GET['page_no']) {
            $page_no = remRegFx($_GET['page_no']);
        } else {
            $page_no = 0;
        }

        $queryStr = $_SERVER['QUERY_STRING'];

        /*--------------------if page_no alreay exists please remove them ---------------------------*/
        $str_pos = strpos($queryStr, 'page_no');
        if ($str_pos > 0) {
            $queryStr = str_replace(substr($queryStr, ($str_pos - 1), strlen($queryStr)), "", $queryStr);
        }

        /*------------------------------------------------------------------------------------------*/
        $obj_paging->set_lower_upper($page_no);
        $total_num = $obj_mysql->get_num_rows($sql);

        $paging = $obj_paging->next_pre($page_no, $total_num, $page_name . "?$queryStr&", 'textArial11Bold', 'textArial11orgBold');
        
        $total_rec = $obj_paging->total_records($total_num);
        $sql .= " LIMIT $obj_paging->lower,$obj_paging->limit";
        /*---------------------paging script end----------------------------------------*/
        $rec = ($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
    }
}

if (count($_POST) > 0) {
    $arr = $_POST;
    $rec = $_POST;

    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES UPDATE Code Block Start Here
    /*--------------------------------------------------------------  */


    if ($rec_id) {
        $arr['modified_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['price'] = $_POST["price"];
        if ($arr['pd'] == "1") {
            $arr['author_modified_date'] = 'now()';
        }
        $arr['property_usp'] = $_POST["property_usp"];
        /*--- Launch date and possession date code start here ---*/
        // $pdMonth = $_POST['pdMonth'];
        // $pdYear = $_POST['pdYear'];
        // $possession_date = $pdYear . "," . $pdMonth;
        // $arr['possession_date'] = $possession_date;

        $ldMonth = $_POST['ldMonth'];
        $ldYear = $_POST['ldYear'];
        $launch_date = $ldYear . "," . $ldMonth;
        $arr['launch_date'] = $launch_date;
        /*--- Launch date and possession date code end here ---*/
        $arr['modified_by'] = $_SESSION['login']['id'];
        $arr["new_amenities"] = implode(',', $_POST["new_amenities"]);
        $arr["new_category"] = implode(',', $_POST["new_category"]);
        $arr["specification"] = implode(',', $_POST["specification"]);

        // check is property is duplicate start

        if (isset($arr['loc_id']) && trim($arr['loc_id']) != "" && isset($arr['unit_no']) && trim($arr['unit_no']) != "" && trim($arr['property_name']) != "" && trim($arr['property_on']) != "") {
            $isDuplicate = checkIsDuplicate($arr,$obj_mysql,$rec_id);
            $arr['is_duplicate'] = $isDuplicate;
        }
        

        // check is property is duplicate end
        if( (isset($arr['user_first_name']) && trim($arr['user_first_name'])!="") &&  (isset($arr['user_last_name']) && trim($arr['user_last_name'])!="" ) &&  (isset($arr['user_email']) && trim($arr['user_email'])!="" ) )
        {
            $broker_param['name'] = trim($arr['user_first_name'])." ".trim($arr['user_last_name']);
            $broker_param['email'] = trim($arr['user_email']);
            $broker_param['mobile'] = trim($arr['user_mobile']);
            $broker_param['alternate_mobile'] = trim($arr['user_alternate_mobile']);

            $broker_id = isBrokerExists($broker_param,$obj_mysql);
            if($broker_id<=0)
            {
                $broker_id = $obj_mysql->insert_table_data(TABLE_COMMERCIAL_BROKER, $broker_param);
            }

            $arr['broker_id'] = $broker_id;

            
        }
        
        $arr['rating'] = json_encode(array('location_rating'=>$_POST['location_rating'],'safety_rating'=>$_POST['safety_rating'],'transport_rating'=>$_POST['transport_rating'],'entertainment_rating'=>$_POST['entertainment_rating'],'connectivity_rating'=>$_POST['connectivity_rating'],'shopping_rating'=>$_POST['shopping_rating']));
       
       
        $obj_mysql->update_commercial_data(TABLE_COMMERCIAL_PROPERTIES, $fld_id, $arr, $rec_id);
        $msg = 'update';

        // $arr["bedrooms"] = implode(',', $_POST["bedrooms"]);
        //$arr["bedrooms"] = $_POST["bedrooms"];

        /* -------------------------------------------------------------  */
        // BANK Part Code Block Start Here

        /*if(isset($_POST['bank']))
        {
        $getBank=$_POST["bank"];
        $allBank=implode(',',$getBank);
        //mysql_query("insert into iin_properties ('banks') values('".$allBank."')");
        $obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
        }*/

        /*--------------------------------------------------------------  */
/*
        $newPath = UPLOAD_PATH_PROPERTY_IMAGE . $_POST["property_name"];
        $oldPath = UPLOAD_PATH_PROPERTY_IMAGE . $_POST["existingPropertyName"];

        if ($_POST["existingPropertyName"] != $_POST["property_name"]) {
            $flagRename = rename($oldPath, $newPath);
            $counterRename = "1";
            $obj_mysql->update_data($tblName, $fld_id, $arr, $rec_id);

            $ctyquery = "SELECT  tsr_cities.city FROM tsr_cities INNER JOIN tbl_properties_commercial ON tbl_properties_commercial.cty_id=tsr_cities.id WHERE tsr_properties.id=" . $rec_id;
            $ctyresult = $obj_mysql->get_row_arr($ctyquery);

            $suggesttable = 'tsr_mynewkeyword';
            $fld = 'stringid';
            if (array_key_exists("property_name", $arr)) {
                $sug['keywordstring'] = $arr['property_name'] . " " . $ctyresult[0];
            }

            if (array_key_exists("country_id", $arr)) {
                $sug['CountryId'] = $arr['country_id'];
            }

            if (array_key_exists("cty_id", $arr)) {
                $sug['CityId'] = $arr['cty_id'];
            }

            if (array_key_exists("loc_id", $arr)) {
                $sug['LocationId'] = $arr['loc_id'];
            }

            if (array_key_exists("cluster_id", $arr)) {
                $sug['SublocationId'] = $arr['cluster_id'];
            }

            if (array_key_exists("lat", $arr)) {
                $sug['lat'] = $arr['lat'];
            }

            if (array_key_exists("lon", $arr)) {
                $sug['lng'] = $arr['lon'];
            }

            if (array_key_exists("status", $arr)) {if ($arr['status'] == 0) {
                $sug['status'] = 9;
            } else if ($arr['status'] == 1) {
                $sug['status'] = 0;
            } else {}
            } else {}
            $sug['stringid'] = $rec_id;
            $sug['property_usp'] = $_POST["property_usp"];
            $sug['keywordid'] = 'ProjectInfo.projectId';
            $sug['mainkeyword'] = 'ProjectInfo.propertyName';

            $keywordq = "SELECT id FROM tsr_mynewkeyword WHERE stringid=" . $rec_id;
            $keyword = $obj_mysql->get_row_arr($keywordq);
            if (($keyword == '') || ($keyword == 0)) {

                $sugid = $obj_mysql->insert_data($suggesttable, $sug);
                //$obj_mysql->callCurl($sugid);
            } else { $mainid = $keyword[0];
                $obj_mysql->update_data($suggesttable, $fld, $sug, $rec_id);
                //$obj_mysql->callCurl($mainid);
            }

        }
*/
        /*if ($obj_mysql->isDupUpdate($tblName, 'property_name', $arr['property_name'], 'id', $rec_id) && $counterRename != "1") {

            /*********** new code added **********************/

          /*  $delQuery = "Delete from " . TABLE_PROPERTIES_RERA . " where prop_id=" . $rec_id . " ";

            $test = $_POST['rera_no'];

            $updateTblName = TABLE_PROPERTIES_RERA;
            $obj_mysql->exec_query($delQuery, $link);
            foreach ($test as $key => $val) {
                $projectArray['prop_id'] = $rec_id;
                $projectArray['rera_no'] = $val;
                $projectArray['ipaddress'] = $regn_ip;
                $projectArray['status'] = 1;
                $obj_mysql->insert_data($updateTblName, $projectArray);
            }
            /************ new code added *******************************/

            /*$obj_mysql->update_data($tblName, $fld_id, $arr, $rec_id);


            for ($counterImages = 0; $counterImages < 2; $counterImages++) {

                if ($_FILES["propertyImage" . $counterImages]["name"] != "") {

                    $arrayPropertyImage["img_type"] = "DEVELOPER_PROPERTY_BANNER_IMAGE";
                    $arrayPropertyImage["propty_id"] = $rec_id;
                    $arrayPropertyImage["imageId"] = $_POST["imageId"][$counterImages];

                    $handle = new upload($_FILES["propertyImage" . $counterImages]);
                    if ($handle->uploaded) {
                        /*$handle->image_resize         = true;
                        $handle->image_x              = 1300;
                        $handle->image_y              = 1637;
                        $handle->image_ratio_y        = true;*/
              /*          $handle->file_safe_name = true;
                        $handle->process(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/");
                        if ($handle->processed) {
                            //echo 'image resized';
                            $arrayPropertyImage["image"] = $handle->file_dst_name;
                            @unlink(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/" . $_POST['old_file_name_propertyImage' . $counterImages]);
                            $handle->clean();
                        } else {
                            echo 'error : ' . $handle->error;
                        }
                    }

                } else {
                   
                    $arrayPropertyImage["image"] = $_POST['old_file_name_propertyImage' . $counterImages];
                }

                if ($arrayPropertyImage["imageId"] == "" && $_FILES["propertyImage" . $counterImages]["name"] != "") {
                    $obj_mysql->insert_data(TABLE_IMAGES, $arrayPropertyImage);
                } else {

                    $obj_mysql->update_data(TABLE_IMAGES, $fld_id, $arrayPropertyImage, $arrayPropertyImage["imageId"]);
                    $obj_common->redirect($page_name_update . "?action=search&pid=" . $rec["id"] . "&submit=Search&msg=update");
                }
            }

            if ($_GET["propertyName"] != "") {
                $obj_common->redirect($page_name_update . "?action=search&propertyName=" . $_GET["propertyName"] . "&submit=Search&msg=update");
            } else if ($rec["cty_id"] != "") {
                $obj_common->redirect($page_name_update . "?action=search&cty_id=" . $rec["cty_id"] . "&types=" . $_GET["types"] . "&page_no=" . $_GET["pg_no"] . "&status=" . $_GET["sta"] . "&submit=Search&msg=update");
            } else {
                $obj_common->redirect($page_name_update . "?action=search&pid=" . $rec["id"] . "&submit=Search&msg=update");
            }

        } else {
            $obj_common->redirect($page_name . "?msg=unique");
            $msg = 'unique';
        }*/
    }
    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES UPDATE Code Block END Here
    /*--------------------------------------------------------------  */

    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES INSERT Code Block START Here
    /*--------------------------------------------------------------  */

    else {
        $arr['created_date'] = "now()";
        $arr['ipaddress'] = $regn_ip;
        $arr['price'] = $_POST["price"];
        if ($arr['pd'] == "1") {
            $arr['author_modified_date'] = 'now()';
        }

        /*--- Launch date and possession date code start here ---*/
        // $pdMonth = $_POST['pdMonth'];
        // $pdYear = $_POST['pdYear'];
        // $possession_date = $pdYear . "," . $pdMonth;
        // $arr['possession_date'] = $possession_date;

        //$ldMonth = $_POST['ldMonth'];
        //$ldYear = $_POST['ldYear'];
        //$launch_date = $ldYear . "," . $ldMonth;
       // $arr['launch_date'] = $launch_date;
        $arr['created_by'] = $_SESSION['login']['id'];
        $arr['assign_by'] = $_SESSION['login']['id'];
        $arr['assign_to'] = $_SESSION['login']['id'];
        /*--- Launch date and possession date code end here ---*/
        $arr["new_amenities"] = implode(',', $_POST["new_amenities"]);
        $arr["new_category"] = implode(',', $_POST["new_category"]);
        // $arr["bedrooms"] = implode(',', $_POST["bedrooms"]);
      //  $arr["bedrooms"] = $_POST["bedrooms"];
        
        /*if ($is_xlr8_user == 1) {
            $arr['is_xlr8'] = 1;
        }else{
          $arr['is_xlr8'] = 0;

        }*/
        $arr['specification'] = implode(',',$arr['specification']);
        
        // check is property is duplicate start

        if (isset($arr['loc_id']) && trim($arr['loc_id']) != "" && isset($arr['unit_no']) && trim($arr['unit_no']) != "" && trim($arr['property_name']) != "" && trim($arr['property_on']) != "") {
             $isDuplicate = checkIsDuplicate($arr,$obj_mysql);
            $arr['is_duplicate'] = $isDuplicate;
        }
        

        // check is property is duplicate end
        if( (isset($arr['user_first_name']) && trim($arr['user_first_name'])!="") &&  (isset($arr['user_last_name']) && trim($arr['user_last_name'])!="" ) &&  (isset($arr['user_email']) && trim($arr['user_email'])!="" ) )
        {
            $broker_param['name'] = trim($arr['user_first_name'])." ".trim($arr['user_last_name']);
            $broker_param['email'] = trim($arr['user_email']);
            $broker_param['mobile'] = trim($arr['user_mobile']);
            $broker_param['alternate_mobile'] = trim($arr['user_alternate_mobile']);

            $broker_id = isBrokerExists($broker_param,$obj_mysql);
            if($broker_id<=0)
            {
                $broker_id = $obj_mysql->insert_table_data(TABLE_COMMERCIAL_BROKER, $broker_param);
                //$broker_id = isBrokerExists($broker_param,$obj_mysql);
            }

            $arr['broker_id'] = $broker_id;
            
            
        }

        $sql_assign_details = "SELECT name,email from ".ADMIN_USER." where id=".$arr['assign_to'];
        $assign_details_record = $obj_mysql->get_assoc_arr($sql_assign_details);
        $arr['assign_to_name']=$assign_details_record['name'];
        $arr['assign_to_email']=$assign_details_record['email'];

        $sugid = $obj_mysql->insert_table_data(TABLE_COMMERCIAL_PROPERTIES, $arr);
         $msg = "insert";
        //$arr['sub_specification']= implode(',',$arr['sub_specification']);
      

        /* if ($obj_mysql->isDuplicate($tblName, 'property_name', $arr['property_name'])) {

            $rec_id = $obj_mysql->insert_data($tblName, $arr);



            $ctyquery = "SELECT tsr_locations.location, tsr_cities.city FROM tsr_locations INNER JOIN tsr_cities ON tsr_cities.id=tsr_locations.cty_id WHERE tsr_cities.id=" . $arr['cty_id'] . " AND tsr_locations.id=" . $arr['loc_id'];
            $ctyresult = $obj_mysql->get_row_arr($ctyquery);

            $suggesttable = 'tsr_mynewkeyword';
            $sug['stringid'] = $rec_id;
            $sug['keywordid'] = 'ProjectInfo.projectId';
            $sug['keywordstring'] = $arr['property_name'] . " " . $ctyresult[1];
            $sug['mainkeyword'] = 'ProjectInfo.propertyName';
            $sug['CountryId'] = $arr['country_id'];

            $sug['CityId'] = $arr['cty_id'];
            $sug['LocationId'] = $arr['loc_id'];
            $sug['SunlocationId'] = $arr['cluster_id'];
            $sug['lat'] = $arr['lat'];
            $sug['lng'] = $arr['lon'];
            if ($arr['status'] == 0) {
                $sug['status'] = 9;
            } else {
                $sug['status'] = 0;
            }

            $sugid = $obj_mysql->insert_data($suggesttable, $sug);
            $obj_mysql->callCurl($sugid);

            for ($counterImages = 0; $counterImages < 2; $counterImages++) {
                if ($_FILES["propertyImage" . $counterImages]["name"] != "") {
                    $arrayPropertyImage["img_type"] = "DEVELOPER_PROPERTY_BANNER_IMAGE";
                    $arrayPropertyImage["propty_id"] = $rec_id;
                    $arrayPropertyImage["imageId"] = $_POST["imageId"][$counterImages];

                    $handle = new upload($_FILES["propertyImage" . $counterImages]);
                    if ($handle->uploaded) {
                        /*$handle->image_resize         = true;
                        $handle->image_x              = 1300;
                        $handle->image_y              = 1637;
                        $handle->image_ratio_y        = true;*/
            /*            $handle->file_safe_name = true;
                        $handle->process(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/");
                        if ($handle->processed) {
                            //echo 'image resized';
                            $arrayPropertyImage["image"] = $handle->file_dst_name;
                            @unlink(UPLOAD_PATH_PROPERTY_IMAGE . $rec_id . "/developer_banners/" . $_POST['old_file_name_propertyImage' . $counterImages]);
                            $handle->clean();
                        } else {
                            echo 'error : ' . $handle->error;
                        }
                    }

                } else {
                    $arrayPropertyImage["image"] = $_POST['old_file_name_propertyImage' . $counterImages];
                }

                if ($arrayPropertyImage["imageId"] == "" && $_FILES["propertyImage" . $counterImages]["name"] != "") {
                    $obj_mysql->insert_data(TABLE_IMAGES, $arrayPropertyImage);
                     $obj_common->redirect($page_name."?msg=insert"); 
                } else {
                    $obj_mysql->update_data(TABLE_IMAGES, $fld_id, $arrayPropertyImage, $arrayPropertyImage["imageId"]);
                    $obj_common->redirect($page_name."?msg=update");    
                }

            }

            $test = $_POST['rera_no'];

            //$setMaxId=$obj_mysql->insert_data($tblName,$arr);
            $updateTblName = TABLE_PROPERTIES_RERA;
            foreach ($test as $key => $val) {
                $projectArray['prop_id'] = $rec_id;
                $projectArray['rera_no'] = $val;
                $projectArray['ipaddress'] = $regn_ip;
                $projectArray['status'] = 1;
                $obj_mysql->insert_data($updateTblName, $projectArray);
            }

            $obj_common->redirect($page_name_insert . "?msg=insert&id=" . $rec_id . "");
        } else {
            $obj_common->redirect($page_name . "?msg=unique");
            $msg = 'unique';
        }*/
    }

    /* -------------------------------------------------------------  */
    // PROPERTY , PRICES , IMAGES INSERT Code Block END Here
    /*--------------------------------------------------------------  */
}

/* -------------------------------------------------------------  */
// PROPERTY DETAILS DISPLAY Code Block START Here
/*--------------------------------------------------------------  */
$list = "";
for ($i = 0; $i < count($rec); $i++) {
    if ($rec[$i]['status'] == "1") {
        $st = '<font color=green>Active</font>';
    } else {
        $st = '<font color=red>Inactive</font>';
    }
if ($rec[$i]['is_duplicate'] <=0) {
        $duplicate = '<font color=green>No</font>';
        $assign_to_name="";
        $assign_to_email="";
    } else {
        
        $sql_assign_details = "SELECT assign_to from ".TABLE_COMMERCIAL_PROPERTIES." where loc_id='".$rec[$i]['loc_id']."' and unit_no='".$rec[$i]['unit_no']."' AND property_on ='".$rec[$i]['property_on']."' AND property_name ='".$rec[$i]['property_name']."' AND is_duplicate=0";

        $assign_details_record = $obj_mysql->get_assoc_arr($sql_assign_details);
        
        if(isset($assign_details_record['assign_to']))
        {

        $sql_assign_details = "SELECT name,email from ".ADMIN_USER." where id=".$assign_details_record['assign_to'];
        $assign_details_record = $obj_mysql->get_assoc_arr($sql_assign_details);
       
            $assign_to_name=$assign_details_record['name'];
            $assign_to_email=$assign_details_record['email'];
        }
        else
        {
            $assign_to_name="";
            $assign_to_email="";

        }
        $duplicate = '<font color=red>Yes</font>';
    }
    
    if ($rec[$i]['assign_to_marketing'] >0) {
        $assign_to_marketing = '<font color=green>Yes</font>';
    } else {
        $assign_to_marketing = '<font color=red>No</font>';
    }
    if ($_GET['page_no'] != "") {
        $updateUrl = $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&country_id=' . $rec[$i]['country_id'] . '&user_id=' . $rec[$i]['user_id'] . '&loc_id=' . $rec[$i]['loc_id'] . '&cty_id=' . $rec[$i]['cty_id'] . '&devlpr_id=' . $rec[$i]['devlpr_id'] . '&cat_id=' . $rec[$i]['cat_id'] . '&type_id=' . $rec[$i]['type_id'] . '&cluster_id=' . $rec[$i]['cluster_id'] . '&pg_no=' . $_GET['page_no'] . '&types=' . $_GET['types'] . '&status=' . $_GET['status'] . '&project_status_id=' . $rec[$i]['project_status_id'];
    } else if ($_GET["propertyName"] != "") {

        $updateUrl = $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&country_id=' . $rec[$i]['country_id'] . '&user_id=' . $rec[$i]['user_id'] . '&loc_id=' . $rec[$i]['loc_id'] . '&cty_id=' . $rec[$i]['cty_id'] . '&devlpr_id=' . $rec[$i]['devlpr_id'] . '&cat_id=' . $rec[$i]['cat_id'] . '&type_id=' . $rec[$i]['type_id'] . '&cluster_id=' . $rec[$i]['cluster_id'] . '&propertyName=' . $_GET["propertyName"] . '&project_status_id=' . $rec[$i]['project_status_id'];
    } else {
        $updateUrl = $page_name . '?action=Update&id=' . $rec[$i]['id'] . '&country_id=' . $rec[$i]['country_id'] . '&user_id=' . $rec[$i]['user_id'] . '&loc_id=' . $rec[$i]['loc_id'] . '&cty_id=' . $rec[$i]['cty_id'] . '&devlpr_id=' . $rec[$i]['devlpr_id'] . '&cat_id=' . $rec[$i]['cat_id'] . '&type_id=' . $rec[$i]['type_id'] . '&cluster_id=' . $rec[$i]['cluster_id'] . '&types=' . $_GET['types'] . '&sta=' . $_GET['status'] . '&project_status_id=' . $rec[$i]['project_status_id'];
    }

    $clsRow = ($i % 2 == 0) ? 'clsRow1' : 'clsRow2';
    
    //missing information notification start  
    $missing_notify="";
    $broker_missing  = array('user_first_name','user_email','user_mobile');
    if(isMissing($broker_missing,$obj_mysql,$rec[$i]['id'])==1)
    {
        $missing_notify.="Borker/Owner info missing<br>";
    }

    $property_type_missing  = array('property_type');
    if(isMissing($property_type_missing,$obj_mysql,$rec[$i]['id'])==1)
    {
        $missing_notify.="Property type missing<br>";
    }

    $tansaction_type_missing  = array('tansaction_type');
    if(isMissing($tansaction_type_missing,$obj_mysql,$rec[$i]['id'])==1)
    {
        $missing_notify.="Tansaction type info missing<br>";
       
    }
   
   
    //missing information notification end
    $style='';
    if($missing_notify!=""){
        $style = "<img src='".C_ROOT_URL."commercial/info.png' height='20' /><div class='tooltip'><span class='tooltiptext'>".$missing_notify."</span></div>";
    }

    $list .= '<tr class="' . $clsRow . '" >';

     if (in_array($_SESSION['login']['role_id'], array(20))) {

        $list.='<td class="center faaddress" data-value="'.$rec[$i]['id'].'">'.$style.'</td>';
        
    }
        $list.='<td class="center"><input type="checkbox" name="comm_id[]" value="'.$rec[$i]['id'].'" /></td>';

     if($rec[$i]['reassign_to_marketing']==1){
        $list .= '<td style="text-align: center;">Yes</td>';
    }else{
       $list .= '<td style="text-align: center;">No</td>';
    }   
        

    if (in_array($_SESSION['login']['role_id'], array("8", "9", "10"))) {

        $list .= '<td><a href="javascript:void(0)" class="title_a">' . $rec[$i]['property_name'] . '</a></td>';
    } else {
        $list .= '<td><a href="' . $updateUrl . '" class="title_a">' . $rec[$i]['property_name'] . '</a></td>';
    }
if($rec[$i]['loc_id']>0){
        $location = getLocationName($rec[$i]['loc_id'],$obj_mysql);
    }else{
        $location= "";
    }
    if($rec[$i]['cty_id']>0){
        $cityName = getCityName($rec[$i]['cty_id'],$obj_mysql);
    }else{
        $cityName= "";
    }


    $list .=  '<td class="center">' . $location . '</td>
                    <td class="center">'.$cityName.'</td>
                    <td class="center">' . $rec[$i]['created_date'] . '</td>
                        
                    <td class="center">' . $st . '</td>
                    <td class="center">' . $rec[$i]['unit_no'] . '</td>
                    <td class="center">' . $rec[$i]['property_on'] . '</td>';
     if (in_array($_SESSION['login']['role_id'], array(20))) {
    
        $list.='<td class="center">'.$rec[$i]['user_first_name']." ".$rec[$i]['user_last_name'].'</td>
                <td class="center">'.$duplicate.'</td>
                <td class="center">'.$assign_to_name.'</td>
                <td class="center">'.$assign_to_email.'</td>
                <td class="center">'.$assign_to_marketing.'</td>';
        }

        if (!(in_array($_SESSION['login']['role_id'], array(20)))) {
            $list.='<td class="center"><a target="_blank" href="'.C_ROOT_URL.'commercial/preview/index.php?id='.base64_encode($rec[$i]['id']).'">Preview Property</a></td>';
        }

            $list.='<td>
                    <form name="editPropertyForm_' . $i . '" id="editPropertyForm_' . $i . '">
                    <input type="hidden" name="action" value="Update">
                    <input type="hidden" name="id" value=' . $rec[$i]['id'] . '>
                    <input type="hidden" name="loc_id" value=' . $rec[$i]['loc_id'] . '>
                    <input type="hidden" name="cty_id" value=' . $rec[$i]['cty_id'] . '>
                    <input type="hidden" name="cat_id" value=' . $rec[$i]['cat_id'] . '>
                    <input type="hidden" name="type_id" value=' . $rec[$i]['type_id'] . '>
                    <input type="hidden" name="cluster_id" value=' . $rec[$i]['cluster_id'] . '>
                    <input type="hidden" name="user_id" value=' . $rec[$i]['user_id'] . '>
                    <input type="hidden" name="country_id" value=' . $rec[$i]['country_id'] . '>
                    <input type="hidden" name="devlpr_id" value=' . $rec[$i]['devlpr_id'] . '>
                    <input type="hidden" name="project_status_id" value=' . $rec[$i]['project_status_id'] . '>';

    if ($_GET['propertyName'] != "") {
        $list .= '<input type="hidden" name="propertyName" value="' . $_GET['propertyName'] . '">';
    }

    if ($_GET['page_no'] != "") {
        $list .= '<input type="hidden" name="pg_no" value=' . $_GET['page_no'] . '>';
    }

    $list .= '<select name="editType' . $i . '" id="editType' . $i . '" onChange="editTypeAction(' . $i . ');">
                    <option value="0">--Select Edit Type --</option>';
    if (in_array($_SESSION['login']['role_id'], array(20))) {
        /* $list .= '
                    <option value="1">Property Detail</option>
                    <option value="2">Property Images</option>
      //              <option value="3">Property Prices</option>
        //            <option value="5">Property Points</option>
          //          <option value="6">Property Location Map</option>
            //        <option value="7">Property Payment Plan</option>
              //      <option value="8">Property Payment Plan NRI</option>';
         */
        $list .= '
                    <option value="1">Property Detail</option>
                    <option value="12">Property Images</option>';
      
    } else {

        $list .= '<option value="1">Property Detail</option>
                    <option value="12">Property Images</option>
                    <option value="4">Property Meta</option>
                    <option value="3">Property Prices</option>';
                    //<option value="7">Property Payment Plan</option>';
                   // <option value="8">Property Payment Plan NRI</option>
                   // <option value="5">Property Points</option>
                    // <option value="6">Property Location Map</option>
                    
        
                    
    }
    $list .= '</select></form>
                    </td>
                </tr>';

}
if ($list == "") {
    $list = "<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
}
/* -------------------------------------------------------------  */
// PROPERTY DETAILS DISPLAY Code Block END Here
/*--------------------------------------------------------------  */
} 
?>
<?php include LIBRARY_PATH . "includes/header.php";
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Property', $site['TITLE'], $site['URL']);
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <td align="center" colspan="2" valign="top">
            <table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
                 <?php
if ($_GET['msg'] != "") {
    echo ('<div class="notice">' . $message_arr[$_GET['msg']] . '</div>');
}
if ($msg != "") {
    echo ('<div class="notice">' . $message_arr[$msg] . '</div>');
}

?>
                 </td>
                </tr>
 </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include $file_name;?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<script>
function editTypeAction(id)
{

    var arr = document.getElementById("editType"+id).value;
    var formNameSet = "editPropertyForm_"+id;

    if(arr=="1")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/commercial/controller/controller.php';
    }
    else if(arr=="12")
    {
       document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/commercialimage/controller/controller.php';

    }
    else if(arr=="3")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/commercial_price/controller/controller.php';
    }
    else if(arr=="4")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/property_meta/controller/controller.php';
    } else if(arr=="5")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/property_point/controller/controller.php';
    }
    else if(arr=="6")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/location_map/controller/controller.php';
    }
    else if(arr=="7")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/payment_plan/controller/controller.php';
    }
    else if(arr=="8")
    {
        document.getElementById(formNameSet).action='<?php echo C_ROOT_URL ?>/price_nri/controller/controller.php';
    }

    document.getElementById(formNameSet).submit();


}
</script>


<?php /* Puload property using xlsx*/

function mappinngArrayModerate() {
        $array = array('Inventary Type' => 'property_type','Sale Type' => 'sale_type','Sector' => 'cluster_id',
                        'City Name' => 'cty_id','City' => 'cty_id','Location' => 'location', 'Address' => 'address', 
                        'Building Name' => 'property_name', 'Building Image' => 'building_image',
                        'Floor Plan' => 'floor_plan', 'Developer Name' => 'devlpr_id', 
                        'Name of Contact Person' => 'user_first_name', 'Contact No.' => 'user_mobile',
                        'Designation' => 'designation', 'Plot Area' => 'plot_area', 
                        'Floor Plate Size' => 'covered_area', 'Total Sale/Leaseable Area' => 'super_build_area', 
                        'Office Floors' => 'office_floors', 'Retail Floors' => 'retails_floors',
                        'Total No. Floors' => 'no_of_floors', 'Maintenance Agency Name' => 'maintenance_agency_name',
                        'Maintenance Office per sq.ft' => 'maintenance_office', 'Maintenance Retail per sq.ft' => 'maintenance_retail',
                        'Building mgr Name' => 'building_mgr_name', 'Contact no.' => 'building_mgr_contact', 
                        'Efficiency Office' => 'efficiency_office', 'Efficiency Retail' => 'efficiency_retail',
                        'Celling Height' => 'celling_height', 'Car parking' => 'car_parking', 
                        'Power Back up' => 'power_back_up', 'Power Load' => 'power_load', 'AC Type' => 'ac_type',
                        'Lift Maker' => 'lift_maker', 'Escalators' => 'escalators', 'No. Passanger Lift' => 'passanger_lift', 
                        'No. Service Lift' => 'service_lift', "Working Hour's" => 'working_hour',
                        'Fitouts Scope' => 'fitouts_scope', 'F & B brands' => 'f_b_brands', 'Foodcourt' => 'foodcourt', 
                        'Health culb/gym' => 'health_culb_gym', 'Google Location' => 'google_location',
                        'Connectivity' => 'connectivity', 'Year of Construction' => 'year_of_construction',
                        'OC / CC' => 'oc_cc', 'Fire NOC' => 'fire_noc', 'Remarks' => 'remarks',
                        'Mail id' => 'mail_id');
        return $array;
    }

    function mappinngArraySupply() {
        $array = array(
                'Inventary Type' => 'property_type',
                'Sale Type' => 'sale_type',
                'City Name' => 'cty_id',
                'City' => 'cty_id',
                'Location' => 'location',
                'Sector' => 'cluster_id',
                'Building Name' => 'property_name', 
                'Floor No.' => 'property_on',
                'Unit No.' => 'unit_no', 
                'Available Area sq.ft' => 'available_area', 
                'Floor Plate Size' => 'covered_area', 
                'Name of Contact Person' => 'user_first_name', 
                'Contact No.' => 'user_mobile',
                'Designation' => 'designation', 
                'Sale Price' => 'price_to_display', 
                'Rent/Sale Price' => 'rent_sale_price', 
                'Maintenance' => 'maintenance_charges',
                'Car parking' => 'car_parking', 
                'Power Back up' => 'power_back_up', 
                'AC Type' => 'ac_type', 
                'Condition' => 'specification',
                'Availability' => 'possession_type', 
                'Fitouts Details' => 'fitouts_scope', 
                'Pre Lease Inventory' => 'amenities', 
                'Remarks' => 'remarks',
                'Mail id' => 'mail_id', 
                'Source By' => 'source_by'
            );
        return $array;
    }
    
function ImportXlsxFile($config, $fileName, $propertyType,$obj_mysql) {
        
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        if($ext=="xls")
        {

            $ExcelReader = new Excel_reader();
            $ExcelReader->setOutputEncoding('CP1251');
            $ExcelReader->read($config.$fileName);
            $worksheet = $ExcelReader->sheets[0];
            $cells = $worksheet['cells'];

        }
        else if($ext=="xlsx")
        {
            set_include_path(get_include_path() . PATH_SEPARATOR . 'PHPEXCELClasses/');
            include LIBRARY_PATH . 'commercial/PHPEXCELClasses/PHPExcel/IOFactory.php';
            try {
                $objPHPExcel = PHPExcel_IOFactory::load($config.$fileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
            $cells = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

        }else
        {
            echo "invalid file ".$fileName;
            exit;
        }

        if ($propertyType == 'moderate_property') {
                $propertyArrayGlobal = mappinngArrayModerate();
        } else {
                $propertyArrayGlobal = mappinngArraySupply();
        }
        
       
        $i = 0;
        $array = array();
        $indexArray =  array();
        foreach ($cells as $cell) {

            $params = array();
            if ($i >= 1) {
              
                foreach ($cell as $key => $val) {                   
                    $params['assign_to'] ="";

                    if(array_key_exists($key, $indexArray))
                    {

                        if (isset($propertyArrayGlobal[$indexArray[$key]]) && $propertyArrayGlobal[$indexArray[$key]] != "") {
                            if ($propertyArrayGlobal[$indexArray[$key]] == 'cty_id') {
                                $ctyId = getCity($val,$obj_mysql);
                                $params[$propertyArrayGlobal[$indexArray[$key]]] = $ctyId;
                             }elseif ($propertyArrayGlobal[$indexArray[$key]] == 'mail_id') {
                                $assignTo = getUserByEmail($val,$obj_mysql);
                                if ($assignTo && $assignTo>0):
                                    $params['assign_to'] = $assignTo;
                                else:
                                    $assignTo = leadAssignTo($params['cty_id'],$obj_mysql,$_SESSION['login']['id']);
                                    $params['assign_to'] = $assignTo;
                                endif;
                            } elseif ($propertyArrayGlobal[$indexArray[$key]] == 'devlpr_id') {
                              
                                $devId = getDeveloperId($val,$obj_mysql);
                                $params[$propertyArrayGlobal[$indexArray[$key]]] = $devId;
                            } elseif ($propertyArrayGlobal[$indexArray[$key]] == 'location') {
                                $devId = getlocation($val,$obj_mysql);
                                $params[$propertyArrayGlobal[$indexArray[$key]]] = $devId;

                            }else {
                                $params[$propertyArrayGlobal[$indexArray[$key]]] = $val;
                            }
                        }

                    }

                }

        
                if($params['assign_to']=="" || $params['assign_to']=='0'){

                    $assignTo = leadAssignTo($params['cty_id'],$obj_mysql,$_SESSION['login']['id']);
                    $params['assign_to'] = $assignTo;
                }

                // assign to name and email details
                $sql_assign_details = "SELECT name,email from ".ADMIN_USER." where id=".$params['assign_to'];
                $assign_details_record = $obj_mysql->get_assoc_arr($sql_assign_details);
                $params['assign_to_name']=$assign_details_record['name'];
                $params['assign_to_email']=$assign_details_record['email'];



                if (isset($params['loc_id']) && trim($params['loc_id']) != "" && isset($params['unit_no']) && trim($params['unit_no']) != "" && trim($params['property_name']) != "" && trim($params['property_on']) != "") {
                    $isDuplicate = checkIsDuplicate($params,$obj_mysql);
                    $params['is_duplicate'] = $isDuplicate;
                }
                $params['assign_by'] = $_SESSION['login']['id'];
                $params['created_date'] = date('Y-m-d H:i:s');
                $params['is_commercial'] = 1;
                if (isset($params['user_email']) && isset($params['user_mobile']) && isset($params['user_first_name']) && ($params['user_email'] != "" && $params['user_mobile'] != "" && $params['user_first_name'] != "")):
                    $data = array('email' => $params['user_email'], 'mobile' => $params['user_mobile'], 'name' => trim($params['user_first_name']));
                    $params['is_commercial'] = tbl_property_user($param,$obj_mysql);
                endif;
                try {

                    if( (isset($params['user_first_name']) && trim($params['user_first_name'])!="")  &&  (isset($params['user_mobile']) && trim($params['user_mobile'])!="" ) )
                    {
                        $broker_param['name'] = trim($params['user_first_name']);
                        $broker_param['email'] = trim($params['user_email']);
                        $broker_param['mobile'] = trim($params['user_mobile']);
                        $broker_id = isBrokerExists($broker_param,$obj_mysql);
                        if($broker_id<=0)
                        {
                            $broker_id = $obj_mysql->insert_table_data(TABLE_COMMERCIAL_BROKER, $broker_param);
                        }

                        $params['broker_id'] = $broker_id;
                        
                        
                    }

                    // get broker id end 
                    $sugid = $obj_mysql->insert_table_data(TABLE_COMMERCIAL_PROPERTIES, $params);
                } catch (Exception $e) {
                }
            }else{
                foreach($cell as $key=>$val){
                    if($val!="")
                    $indexArray[$key] =  $val;

                }

            }
            $i++;
        }
        header('Location:/commercial/index.php');
        
    }
    
    function getUserByEmail($email,$obj_mysql){
        $sql = "SELECT * from tsr_users where email='".$email."'";
        try{
        $result = $obj_mysql->get_assoc_arr($sql);
        }catch(Exception $e){
            print_r($e);
        }
        
        if (count($result) > 0)
            return $result['id'];
        else
            return 0;
    }
     function tbl_property_user($params,$obj_mysql) {
        $sql = "SELECT * from tsr_property_user where email='".$params['user_email']."' and name='".$params['user_first_name']."'";
        //exit;
        $result = $obj_mysql->get_assoc_arr($sql);
        if(isset($result['id']) && $result['id']):
        return $result['id'];
        else:
            $data = array('email'=>$params['user_email'],'name'=>$params['user_first_name']);
            $id = $obj_mysql->insert_table_data('tsr_property_user', $params);
             return $id;
        endif;
        
    }
   
     function checkIsDuplicate($param,$obj_mysql,$id=0) {
        if($id==0)
        {
            $sql = "SELECT id from ".TABLE_COMMERCIAL_PROPERTIES." where loc_id='".$param['loc_id']."' and unit_no='".$param['unit_no']."' AND property_on ='".$param['property_on']."' AND property_name ='".$param['property_name']."' AND is_duplicate=0";
        }
        else
        {

            $sql = "SELECT id from ".TABLE_COMMERCIAL_PROPERTIES." where loc_id='".$param['loc_id']."' and unit_no='".$param['unit_no']."' AND property_on ='".$param['property_on']."' AND property_name ='".$param['property_name']."' AND id<>'".$id."' AND is_duplicate=0";

        }


        //exit;
        $result = $obj_mysql->get_assoc_arr($sql);
        if($result)
        {
            if (count($result) > 0)
                return 1;
            else
                return 0;
        }
        else
        {
            return 0;
        }
    }

    function isBrokerExists($param,$obj_mysql) {
        
        if($param['email']=="")
        {
            $sql = "SELECT id from ".TABLE_COMMERCIAL_BROKER." where lcase(name)='".strtolower($param['name'])."' and mobile='".strtolower($param['mobile'])."'";

        }
        else
        {
            $sql = "SELECT id from ".TABLE_COMMERCIAL_BROKER." where lcase(name)='".strtolower($param['name'])."' and lcase(email)='".strtolower($param['email'])."'";
        }

        $result = $obj_mysql->get_assoc_arr($sql);
        if($result)
        {
            if (count($result) > 0)
                return $result['id'];
            else
                return 0;
        }
        else
        {
            return 0;
        }
    }


    function isMissing($param,$obj_mysql,$id) {
        
        $childsql="";
        foreach ($param as $field) {
            $childsql.=$field."='' or ".$field." is null or ";
        }
        $childsql = chop($childsql," or ");
        $sql = "SELECT id from ".TABLE_COMMERCIAL_PROPERTIES." where id='".$id."' and (".$childsql.")";

        $result = $obj_mysql->get_assoc_arr($sql);
        if($result)
        {
            if (count($result) > 0)
                return 1;
            else
                return 0;
        }
        else
        {
            return 0;
        }
    }

    
    function getDeveloperId($str,$obj_mysql) {
         
        $sql = "select id from tsr_developers where LOWER(name) ='".strtolower($str)."'";
        $result = $obj_mysql->get_assoc_arr($sql);
      
        if (isset($result['id']) && $result['id'] > 0)
            return $result['id'];
        else
            $propertyName['name']=$str;
            return $obj_mysql->insert_table_data(TABLE_DEVELOPERS, $propertyName);
    }
    
    function getlocation($str,$obj_mysql) {
       // echo $str;exit;
        $sql = "select id from tsr_locations where LOWER(location) ='".strtolower($str)."'";
        $result = $obj_mysql->get_assoc_arr($sql);
        if (count($result) > 0)
            return $result['id'];
        else
            $propertyLocation['location']=$str;
            return $obj_mysql->insert_table_data(TABLE_LOCATIONS,$propertyLocation);
    }
    function getCity($str,$obj_mysql) {
        $sql = "select id from tsr_cities where LOWER(city) ='".strtolower($str)."'";
        $result = $obj_mysql->get_assoc_arr($sql);
        if (count($result) > 0)
            return $result['id'];
        else
            return '';
    }

    function getLocationName($id,$obj_mysql){
    $sql="select location from ".TABLE_LOCATIONS." where id=".$id;
    $result = $obj_mysql->get_assoc_arr($sql);
    
    return $result['location'];
}
function getCityName($id,$obj_mysql){
    $sql="select city from ".TABLE_CITIES." where id=".$id;
    $result = $obj_mysql->get_assoc_arr($sql);
    return $result['city'];
}
function updatePropertyRecord($id,$type,$obj_mysql){

    $data_array = array();
    if($type==1){
        $data_array['is_duplicate']=1;
        $sql = "update ".TABLE_COMMERCIAL_PROPERTIES." set is_duplicate=1 where id in (".$id.")";
    }elseif($type==2){

        $data_array['assign_to_marketing']=1;
        $sql = "update ".TABLE_COMMERCIAL_PROPERTIES." set assign_to_marketing=1,reassign_to_marketing=0 where id in (".$id.")";
        
        $sqlproperty2="select pcommercial.property_name,pcommercial.id,tsr_cities.city from tbl_properties_commercial as pcommercial left join tsr_cities on pcommercial.cty_id=tsr_cities.id where pcommercial.id in(".$id.")";
      
          $rec=$obj_mysql->getAllData($sqlproperty2);
            $msg="";
            
                $msg.='
                    <table width="600" align="center" border="1" cellspacing="0" cellpadding="0" style="border:1px solid #dadada; margin:auto;">
                      <tr>
                        <th>Property ID</th>
                        <th>Property Name</th>
                        <th>City</th>
                      </tr>';

                    //print_r($rec);die();
                      
                    foreach($rec as $propertyData)
                    {
                        $id=$propertyData["id"];
                        $propertyName=$propertyData["property_name"];
                        $city=$propertyData["city"];
                        $msg.='<tr>
                        <td style="text-align: center;">'.$id.'</td>
                        <td style="text-align: center;">'.$propertyName.'</td>
                        <td style="text-align: center;">'.$city.'</td>
                        
                        </tr>';
                    }
                    $msg.='</table>';

                    $mail   = new PHPMailer();
                    $body          = $msg;
                    $mail->IsSMTP(); 
                    $mail->SMTPDebug = 1; 
                    $mail->SMTPAuth = true;
                    $mail->SMTPSecure = 'tls';
                    $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
                    $mail->Port = 587;
                    $mail->IsHTML(true);
                    $mail->SetLanguage("tr", "phpmailer/language");
                    $mail->CharSet ="utf-8";
                    $mail->Username = "AKIA6IFADMQI66NZEGOF"; 
                    $mail->Password = "BEU/c/a4Hii8FMCoQmk5srnfsESPhTAdmDt8nJZLX3Ko";
                    $mail->SetFrom($_SESSION['login']['email'],$_SESSION['login']['name']);
                    $mail->addAddress(USER_MAIL);  
                    $mail->AddReplyTo($_SESSION['login']['email'],$_SESSION['login']['name']);

                    $mail->addCustomHeader('Sender', $_SESSION['login']['email']);
                    $mail->XMailer = ' ';
                    $mail->Subject    = 'Assign Property ';
                    $mail->MsgHTML($body);
                    if(!$mail->Send()) {
                      echo "Mailer Error: " . $mail->ErrorInfo;
                    } else {
                      echo "Message sent!";
                    }
                
                    
    }
    elseif($type==3){
        $data_array['status']=1;
        $sql = "update ".TABLE_COMMERCIAL_PROPERTIES." set status=1 where id in (".$id.")";
    }
    elseif($type==4){
        $data_array['status']=0;
        $sql = "update ".TABLE_COMMERCIAL_PROPERTIES." set status=0 where id in (".$id.")";
    }
    $obj_mysql->update_query($sql); 

    // sink with new crm
    // $obj_mysql->sinkWithCRM($table_name,$sql);
    $data_array['table_name']="jos_".TABLE_COMMERCIAL_PROPERTIES;
    $data_array['id']=$id;
    $obj_mysql->insertInNewCrm($data_array, "SyncWithCommercial");
    // sink with new crm end
    header('Location:/commercial/index.php');
}

function propertySendEmailRecord($commid,$commdescription,$login_userID,$login_email,$login_username,$obj_mysql){
      
        $data_array['property_id']=$commid;
        $data_array['description']=$commdescription;
        $data_array['user_id']=$login_userID;
       
        $data['status']="";
        $sql = "update ".TABLE_COMMERCIAL_PROPERTIES." set reassign_to_marketing=1,status='".$data."' where id in (".$commid.")";
        $obj_mysql->update_query($sql); 
      
   
       $sqlproperty2="select pcommercial.property_name,pcommercial.id as pid,tsr_users.email from tbl_properties_commercial as pcommercial left join tsr_users on pcommercial.assign_to=tsr_users.id where pcommercial.id=".$commid."";
         $result = mysql_query($sqlproperty2);
          $row = mysql_fetch_assoc($result);

       
       $userlogin = "select * from tsr_users where id=".$login_userID."";
       $rec=$obj_mysql->getAllData($userlogin);
      
        $msg='<table width="600" align="center" border="1" cellspacing="0" cellpadding="0" style="border:1px solid #dadada; margin:auto;">  
           <tr>
            <th>Property ID</th>
            <th>Description</th>
           </tr>   
           <tr>
            <td style="text-align: center;">'.$row['pid'].'</td>
            <td style="text-align: center;">'.$commdescription.'</td>
            </tr>
            </table>';
            $mail   = new PHPMailer();
            $body          = $msg;
            $mail->IsSMTP(); 
            $mail->SMTPDebug = 1; 
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Host = 'email-smtp.us-east-1.amazonaws.com';
            $mail->Port = 587;
            $mail->IsHTML(true);
            $mail->SetLanguage("tr", "phpmailer/language");
            $mail->CharSet ="utf-8";
            $mail->Username = "AKIA6IFADMQI66NZEGOF"; 
            $mail->Password = "BEU/c/a4Hii8FMCoQmk5srnfsESPhTAdmDt8nJZLX3Ko";
            $mail->SetFrom(USER_MAIL,'Gaurav');
            $mail->SetFrom($rec[0]['email'],$rec[0]['name']);
            $mail->addAddress($row['email']);  
            $mail->AddReplyTo($rec[0]['email'],$rec[0]['name']);
            $mail->addCustomHeader('Sender',$rec[0]['email']);
            $mail->XMailer = ' ';
            $mail->Subject    = 'Update Required on Property';
            $mail->MsgHTML($body);
            if(!$mail->Send()) {
              echo "Mailer Error: " . $mail->ErrorInfo;
            } else {

             $obj_mysql->insert_table_data(TABLE_REASSIGN_PROPERTY_HISTORY,$data_array);

              echo "Message sent!";
            }
   header('Location:/commercial/index.php');

}




function leadAssignTo($city,$obj_mysql,$id){

   $sql = "select city_id from ".TABLE_USER_MAPPING." where user_type='20' and is_deleted='0' and status='1' and user_id=".$id;
    $result = $obj_mysql->get_assoc_arr($sql);
    if($result['city_id']!= $city){
    $sql3 = "SELECT user_id FROM ".TABLE_USER_MAPPING."  where user_type='20' and is_deleted='0' and is_ready='1'
    and status='1' and city_id='" . $city . "'  order by last_lead_assign ASC limit 0,1";
    $result3 = $obj_mysql->get_assoc_arr($sql3);
    //exit;
    $id_return = $id;    
        if(isset($result3['user_id']) && $result3['user_id']>0)
        {
            $id_return = $result3['user_id'];    
        }
           $sql2 = "update ".TABLE_USER_MAPPING." set last_lead_assign= now() where user_id = ".$id_return;
            $obj_mysql->update_query($sql2);

        return $id_return;
    }else{
        return $id;
    }
}

function getCommercialCommentHistory($proId,$login_id,$obj_mysql){
     
     $sql = "select * from tsr_reassign_property_history where property_id=".$proId;
     $rec=$obj_mysql->getAllData($sql);
     $msg='';
                    foreach($rec as $value)
                    {
                        
                     $msg .='<tr>
                        <td style="text-align: center;">'.$value['property_id'].'</td>
                        <td style="text-align: center;">'.$value['description'].'</td>
                        
                        
                        </tr>';
                    }
                  

   echo $msg;
   exit;

}

?>
<?php include LIBRARY_PATH . "includes/footer.php";?>
