
<link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script src="<?php echo $site['URL']?>view/js/news/jquery-2.0.3.min.js?>"></script>
<script>
function richTextBox($fieldname, $value = "") 
{
   $CKEditor = new CKEditor();
   $config['toolbar'] = array(
    array( 'Bold', 'Italic', 'Underline', 'Strike'),
    array('JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'),
    array('Format'),
    array( 'Font', 'FontSize', 'FontColor', 'TextColor'),
    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord' ),
    array('Image', 'Table', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'),
    array('Subscript','Superscript'),
    array( 'Link', 'Unlink' ),
    array('Source')
   );
   $CKEditor->basePath = '/ckeditor/';
   $CKEditor->config['width'] = 975;
   $CKEditor->config['height'] = 400;
   $CKEditor->editor($fieldname, $value, $config);
}

</script>

<script language="JavaScript" src="<?php echo $site['URL']?>view/js/calendar_us.js"></script>

<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">

  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
    <thead>
      <tr>
        <th colspan="2" align="left">
          <h3>News Detail</h3>
        </th>
      </tr>
    </thead>

    <tbody>

      <tr class="<?php echo($clsRow1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Section Name</strong></td>
        <td>
          <?php 
            echo AjaxDropDownList(TABLE_NEWS_SECTION,"sec_id","id","name",$_GET["sec_id"],"id",$site['CMSURL']."news/dropdown_ajax.php","subcatDiv","subcatLabelDiv")
          ?>
        </td>
      </tr>

      <tr class="<?php echo($clsRow1)?>"><td width="15%" align="right"><strong><font color="#FF0000">*</font>Author Name</strong></td>
        <td><?php 
          echo AjaxDropDownList(TABLE_AUTHORS,"auth_id","id","name",$_GET["auth_id"],"id",$site['CMSURL'],"SubauthDiv","SubauthLabelDiv")?>
        </td>
      </tr>

      <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Title</strong></td>
        <td><input name="title" type="text" title="Title" lang='MUST' value="<?php echo($rec['title'])?>" size="178%" maxlength="170" /></td>
      </tr>

      <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Sub - Title</strong></td>
        <td><input name="sub_title" type="text" title="Sub -Title" lang='MUST' value="<?php echo($rec['sub_title'])?>" size="178%" maxlength="170" /></td>
      </tr>

      <tr class="<?php echo($clsRow1)?>">
        <td align="right" ><strong>Overview</strong></td>
        <td><textarea name="overview" class="ckeditor"  id="content" rows="8" cols="175"><?php echo($rec['overview'])?></textarea>  </td>
      </tr>
      
      <?php 
        if($_GET["action"]=="Update")
        {
          $sqlImages = "SELECT * FROM ".TABLE_IMAGES." WHERE img_type='NEWS_IMAGE' and propty_id=".$_GET['id'];
          $recImages=($obj_mysql->getAllData($sqlImages));
          for($i=0;$i<10;$i++)
          {
            ?>
              <tr class="<?php echo($cls1)?>" style=" background:#fffbe4;">
                <td align="right" >
                  <strong>Select Image <?=$i+1?></strong>
                </td>
                <input type="hidden" name="imageId[]" value="<?=$recImages[$i]['id']?>">
                <td valign="top">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="38%">
                        <input type="file" name="image<?=$i?>" id="image<?=$i?>" />
                      </td>
                      <td width="62%">&nbsp;&nbsp;<?php if($recImages[$i]['image']):?>
                        <br />
                        <img src="<?php echo ($site['NEWSLOGOURL'].$rec['id']."/".$recImages[$i]['image'])?>"   height="60" width="100" border="0" /> <?php endif;?>
                        <input type="hidden" name="old_file_name<?=$i?>" value="<?php echo ($recImages[$i]['image'])?>" />
                        <br />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            <?php 
          }
        }
        
        else 
        {
          for($i=0;$i<10;$i++)
          {
            ?>
              <tr class="<?php echo($cls1)?>">
                <td align="right" >
                  <strong>Select Image <?php echo $i+1?></strong>
                </td>
                <input type="hidden" name="imageId[]">
                <td valign="top">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="38%">
                        <input type="file" name="image<?=$i?>" id="image<?=$i?>" />
                      </td>
                      <input type="hidden" name="old_file_name<?=$i?>"  />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            <?php 
          }
        }
      ?>

     <tr class="<?php echo($clsRow1)?>">
        <td align="right" ><strong>Published Date</strong></td>
        <td>
          <input type="text" name="published_date" value="<?php echo($rec['published_date'])?>"/><script language="JavaScript">
          new tcal ({
            'formname': 'form123',
            'controlname': 'published_date'
          });</script>
        </td>
      </tr>

      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Tags</strong></td>
        <td><textarea  name="tags" id="tags" rows="3" cols="175" ><?php echo stripslashes($rec['tags'])?></textarea></td>
      </tr>

      <?php 
        if($_GET["action"]=="Update")
        {
          if(!empty($rec["title_hindi"]))
          {
          ?>
            <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Hindi URL Title</strong></td>
        <td><input name="title_hindi" type="text" title="Title" lang='MUST' value="<?php echo($rec['title_hindi'])?>" size="178%" maxlength="170" /></td>
      </tr>
          <?php
        }
        if(!empty($rec["hindi_meta_title"]))
          {
          ?>
            <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Hindi Meta Title</strong></td>
        <td><input name="hindi_meta_title" type="text" title="Title" lang='MUST' value="<?php echo($rec['hindi_meta_title'])?>" size="178%" maxlength="170" /></td>
      </tr>
          <?php
        }
        if(!empty($rec["hindi_meta_description"]))
          {
          ?>
            <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Hindi Meta Description</strong></td>
        <td><input name="hindi_meta_description" type="text" title="Title" lang='MUST' value="<?php echo($rec['hindi_meta_description'])?>" size="178%" maxlength="170" /></td>
      </tr>
          <?php
        }
        if(!empty($rec["hindi_meta_keywords"]))
          {
          ?>
            <tr class="<?php echo($clsRow1)?>">
        <td align="right"><strong><font color="#FF0000">*</font>Hindi Meta Keywords</strong></td>
        <td><input name="hindi_meta_keywords" type="text" title="Title" lang='MUST' value="<?php echo($rec['hindi_meta_keywords'])?>" size="178%" maxlength="170" /></td>
      </tr>
          <?php
        }
        }
        ?>

      <tr id="addValues"></tr>



      <tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Is Hindi</strong> </td>
        <td valign="bottom">
          <input type="radio" name="is_hindi" value="1" id="1" onclick="checkHindiNews();" <?php if ($rec['is_hindi']=="1"){ echo("checked='checked'");} ?> /> 
          <label for="1">Yes</label> 
          <input type="radio" name="is_hindi" value="0"  id="0" /> 
          <label for="0">No</label>
        </td>
      </tr>



      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
        <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title'])?></textarea></td>
      </tr>


      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
        <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords'])?></textarea></td>
      </tr>


      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
        <td><textarea  name="meta_description" id="meta_description" rows="5" cols="175" ><?php echo stripslashes($rec['meta_description'])?></textarea></td>
      </tr>

      <tr class="<?php echo($clsRow1)?>">
        <td width="15%" align="right"><strong>Ordering</strong></td>
           <td><textarea name="ordering" id="ordering" rows="1" cols="175" ><?php echo stripslashes($rec['ordering'])?></textarea></td>
       </tr>

      <tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td>
      	<td valign="bottom">
        	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
        	<label for="1">Active</label> 
        	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
        	<label for="0">Inactive</label>
      	</td>
      </tr>
      <tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Web Scope</strong> </td>
  <td>
  <input type="radio" name="web_scope" value="1"  <?php if (isset($rec['web_scope'])&&$rec['web_scope']==1){ echo("checked='checked'");} ?> /> 
  <label for="1">Residential</label> 
  <input type="radio" name="web_scope" value="2"  <?php if (isset($rec['web_scope'])&&$rec['web_scope']==2){ echo("checked='checked'");} ?> /> 
  <label for="1">Commercial</label>
  <input type="radio" name="web_scope" value="0"  <?php if (!isset($rec['web_scope']) ||$rec['web_scope']==0){ echo("checked='checked'");} ?>/> 
  <label for="0">Both</label>  </td>
</tr>
<tr class="<?php echo($clsRow2)?>"><td align="right" ><strong>360 Edge</strong> </td>
  <td>
  <input type="radio" name="360_edge" value="0" <?php if (isset($rec['360_edge'])&&$rec['360_edge']==0){ echo("checked='checked'");} ?> /> 
  <label for="0">No</label> 
  <input type="radio" name="360_edge" value="1" <?php if (isset($rec['360_edge'])&&$rec['360_edge']==1){ echo("checked='checked'");} ?> /> 
  <label for="1">Yes</label>
  </td>
</tr>
      <tr class="<?php echo($clsRow1)?>"><td align="left">&nbsp;</td>
        <td align="left">
          <input name="submit" type="submit" class="button" value="Submit" />
          <input name="reset" type="reset" class="button" value="Reset" />
          <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" />
        </td>
      </tr>
    </tbody>
  </table>
</form>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
  function checkHindiNews()
  {
   
    var values = '<tr class="<?php echo($clsRow1)?>"><td align="right"><strong><font color="#FF0000">*</font>Hindi URL Title</strong></td><td><input name="title_hindi" type="text" title="Title" lang="MUST" value="<?php echo($rec["title_hindi"])?>" size="178%" maxlength="170" /></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right"><strong>Hindi Meta Title</strong></td><td><input name="hindi_meta_title" type="text" title="Title" value="<?php echo($rec["hindi_meta_title"])?>" size="178%" maxlength="170" /></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right"><strong>Hindi Meta Description</strong></td><td><input name="hindi_meta_description" type="text" title="Title" value="<?php echo($rec["hindi_meta_description"])?>" size="178%" maxlength="170" /></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right"><strong>Hindi Meta Keywords</strong></td><td><input name="hindi_meta_keywords" type="text" title="Title" value="<?php echo($rec["hindi_meta_keywords"])?>" size="178%" maxlength="170" /></td></tr>';
    jQuery("#addValues").before(values);
  }
</script>


