<?php

if ($_SERVER['HTTP_HOST'] == '360cms.com') {

    include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
} else {

    include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
}

if ($_SERVER['HTTP_HOST'] == '360cms.com') {

    $staticContentUrl = 'http://360cms.com';
} else {

    $staticContentUrl = 'https://static.360realtors.ws';
}

 //echo(ini_get('post_max_size'));
include_once(LIBRARY_PATH . "library/server_config_admin.php");
include_once(LIBRARY_PATH . "library/main_common_function.php");
#ini_set('display_errors', 1);
#ini_set('display_startup_errors', 1);
#error_reporting(E_ALL);
set_time_limit (0);
include_once(LIBRARY_PATH . "research/model/Model.php");
$modelObj = new Model();
admin_login();


/* Including common Header */
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('Manage Media', $site['TITLE'], $site['URL']);
$page_name = C_ROOT_URL . "/research/index.php";

$action = $_REQUEST['action'];

$commonFunction = new main_common();
$checkPostdata = false;



switch ($action) {
    case 'Create':

        if (is_array($_POST) && count($_POST) > 0) {
          
            $insertIntoDb = $modelObj->insertIntoDb();
            if ($insertIntoDb) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Inserted successfully!");
                    window.location.href = "/research/index.php";
                </script>
                <?php

            }
        }
        include(ROOT_PATH . "/research/view/researchDetailPage.php");
        break;

    case 'Update':

        if (isset($_GET['researchId']) && $_GET['researchId'] != "") {


          $researchId = $_GET['researchId'];

            $getdataforResearcharr = $modelObj->selectAllResearch($researchId);

            $getdataforResearch = $getdataforResearcharr[0];
            
            
        }

        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {
            
          
            $updateData = $modelObj->updateResearch($_GET['researchId']);


            if ($updateData) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Updated successfully!");
                    window.location.href = "/research/index.php";
                </script>
                <?php

            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';
            }
        }
        include(ROOT_PATH . "/research/view/researchDetailPage.php");
        break;

    case 'Delete':

        if (isset($_GET['researchId']) && $_GET['researchId'] != "") {
            $researchId = $_GET['researchId'];
            $changeStartus = $modelObj->changestatusofResearch($researchId);


            if ($changeStartus) {
                ?>
                <script type="text/javascript">
                    alert("Status has been changed successfully!");
                    window.location.href = "/research/index.php";
                </script>
                <?php

            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';

                header('location: /research/controller/controller.php');
            }
        }
        break;

    default :

        $getDataForview = $modelObj->selectAllResearch();

        include(ROOT_PATH . "research/view/view.php");
        break;
}
?>
