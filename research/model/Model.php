<?php

require_once(ROOT_PATH . "library/mysqli_function.php");


// this model for making all the query for event
class Model {

    private $dbObj;
    private $filesaveingpath;

    function __construct() {

        $this->dbObj = new mysqli_function();

        if ($_SERVER['HTTP_HOST'] == '360cms.com') {

            $this->FIleSavingPath = ROOT_PATH;
        } else {

            $this->FIleSavingPath = "/var/www/html/myzone/media_images";
        }
    }


    // for inserting Data for research
    public function insertIntoDb() {
        /*ini_set('post_max_size', '10M');
        ini_set('upload_max_filesize', '10M');*/
        ini_set('memory_limit', '32M');
        //ini_set('post_max_size', '16M');
        ini_set('upload_max_filesize', '16M');
        
        $title = mysql_real_escape_string($_POST['ResearchTitle']);
        $description = mysql_real_escape_string($_POST['description']);
       
       $imagePath = mysql_real_escape_string($_FILES['Images']['name']);
       
       if ($_FILES['ResearchPdf']['tmp_name'] != ""){
           
        $pdfPath = mysql_real_escape_string($_FILES['ResearchPdf']['name']);
        
       } else {
           
           $pdfPath = ""; 
       }
        
        $status = mysql_real_escape_string($_POST['status']);
        $in_commercial = mysql_real_escape_string($_POST['in_commercial']);

      
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $createdDate = date('Y-m-d H:i:s');
      

        $sql = "INSERT INTO `tsr_research` ("
                . "`Title`,"
                . "`description`,"
                . " `image_path`, "
                . " `pdf_file_path`,"
                . " `status`, "
                . " `in_commercial`, "
                . "`created_by`, "
                . "`created_date`)"
              
                . "VALUES('" . $title . "', "
                . "'" . $description . "',"
                . "'" . $imagePath . "',"
                . "'" . $pdfPath . "', "
                . " '" . $status . "',"
                . " '" . $in_commercial . "',"
                . " '" . $CreatedBy . "', "
                . "'" . $createdDate . "'"
             
                . "   )";

             //echo $sql; exit;
        $query = $this->dbObj->insert_query($sql);

        $researchId = $query;

        $this->uploadImageforResearch($researchId);
        
        if ($pdfPath != ""){
        $this->uploadPdfforResearch($researchId);
        }
       
        return $query;
    }

    // this method for upload all file in folder
    private function uploadImageforResearch($researchId) {
     
        $_FILES['Images'];
       $Rootpathmain = $this->FIleSavingPath."/Research" ;
        
                        if (!is_dir($Rootpathmain)) {
                            mkdir($Rootpathmain);
                            chmod($Rootpathmain, 0777);
                        }
        
               $Rootpath = $Rootpathmain."/IMAGES" ;
        
                        if (!is_dir($Rootpath)) {
                            mkdir($Rootpath);
                            chmod($Rootpath, 0777);
                        }
                         
        
        $path = $Rootpath."/".$researchId;     
                
         if (!is_dir($path)) {
             
                            mkdir($path);
                            chmod($path, 0777);
                        }
              
                        
        $tmpname =  $_FILES['Images']['tmp_name'];
      
        move_uploaded_file($tmpname, $path . '/' . $_FILES['Images']['name']);
        
        
        
        
    }


 // this method for upload all file in folder
    private function uploadPdfforResearch($researchId) {
     
      
        $Rootpathmain = $this->FIleSavingPath."/Research" ;
        
                        if (!is_dir($Rootpathmain)) {
                            mkdir($Rootpathmain);
                            chmod($Rootpathmain, 0777);
                        }
        
               $Rootpath = $Rootpathmain."/PDF" ;
        
                        if (!is_dir($Rootpath)) {
                            mkdir($Rootpath);
                            chmod($Rootpath, 0777);
                        }
                         
                        
                        
        $path = $Rootpath."/".$researchId;     
                
         if (!is_dir($path)) {
             
                            mkdir($path);
                            chmod($path, 0777);
                        }
        
         
       
        $tmpname =  $_FILES['ResearchPdf']['tmp_name'];
      
        move_uploaded_file($tmpname, $path . '/' . $_FILES['ResearchPdf']['name']);
        
        
        
        
    }

    //select all research
   public function selectAllResearch($researchId=""){
       
       
       $sql = 'SELECT id,Title,description,image_path,pdf_file_path, status, in_commercial FROM `tsr_research`';
       
          if ($researchId != "") {
                   
          $sql .= " where id = '". mysql_real_escape_string($researchId) ."'";
          
               }
            
           $sql .=   ' ORDER by id DESC';
       
        
       $query = $this->dbObj->getAllData($sql);


        return $query;
       
   }
   
   //uodate research
   public function updateResearch($researchId){
       
        $title = mysql_real_escape_string($_POST['ResearchTitle']);
        $description = mysql_real_escape_string($_POST['description']);
        $status = mysql_real_escape_string($_POST['status']);
        $in_commercial = mysql_real_escape_string($_POST['in_commercial']);
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $modified_date = date('Y-m-d H:i:s');
       
       $sql = "update tsr_research set "
               . " Title = '".$title."',"
               . " description = '".$description."',";
       
       if ($_FILES['Images']['tmp_name'] != ""){
           
             $sql .= "image_path = '".mysql_real_escape_string($_FILES['Images']['name'])."', ";
       }
        
       
         if ($_FILES['ResearchPdf']['tmp_name'] != ""){
           
             $sql .= "pdf_file_path = '".mysql_real_escape_string($_FILES['ResearchPdf']['name'])."', ";
       }
           $sql .=  " status = '".$status."',"
               . " in_commercial = '".$in_commercial."',"
               . " created_by = '".$CreatedBy."',"
               . " modified_date = '".$modified_date."' "
               . "  where id = '".$researchId."' ";
       
   
       $update = $this->dbObj->update_query($sql);
        if ($_FILES['Images']['tmp_name'] != ""){
       $this->uploadImageforResearch($researchId);
        }
        
        if ($_FILES['ResearchPdf']['tmp_name'] != ""){
        $this->uploadPdfforResearch($researchId);
        }
       
       
       
       if ($update) {
           
            return true;
            
        } else {

            return false;
        }
             
       
   }
   
   // change status active to deactive and deactive to active
   public function changestatusofResearch($researchId){
       
        $sql = 'SELECT status FROM `tsr_research` where id = "'.$researchId.'" ';
       
       $query = $this->dbObj->getAllData($sql);

       $status = $query[0]['status'];
       
       if ($status == 1){
           
           $NewStatus = 2;
        } else {
            
           $NewStatus = 1;  
        }
      
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $modified_date = date('Y-m-d H:i:s');
       
       $sql = "update tsr_research set "
            
               . " status = '".$NewStatus."',"
                . " created_by = '".$CreatedBy."',"
               . " modified_date = '".$modified_date."' "
               . "  where id = '".$researchId."' ";
       
       
       $update = $this->dbObj->update_query($sql);
      
       return true;
             
       
   }
   

}

// end class 
?>
