<?php
include_once(LIBRARY_PATH . "research/view/researchheader.php");

?>
<body>
    <form method="post" name="reseatchDetail" id="reseatchForm" onsubmit="validateForm(this);" enctype="multipart/form-data">
        <table width="100%" class="MainTable eventinfo">
            <thead>
                <tr>
            <h3><th align="left" colspan="2">Research Details</th></h3>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="input-lbl">Research Title<font color="#FF0000">*</font></td>
                    <td>
                        <input type="text" id="ResearchTitle" name="ResearchTitle" value="<?php echo $getdataforResearch["Title"]; ?>" placeholder="Research Title " style="width: 450px;" required="true">
                    </td>
                </tr>
                <tr>
                <td class="input-lbl" align="right" ><strong>Description</strong></td>
                <td>
                    <textarea name="description" class="ckeditor"  id="content" rows="8" cols="175"><?php echo($getdataforResearch['description'])?></textarea>
                </td>
              </tr>
                <tr> 
                    <td class="input-lbl">Images <font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="Images" id="images" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
<?php if ($action == "Create") { ?>      
                                   required = 'required';
                        <?php }  ?>

                                  
                            ><br/>
                        

                        <div id="imagesExists">

<?php if ($getdataforResearch['image_path'] != "") { ?>


                                <div class="filediv"> 
                                    <div style="float:left;">
                                        <img src="<?php echo $staticContentUrl . '/Research/IMAGES/' . $getdataforResearch['id'] . "/" . $getdataforResearch['image_path']; ?>" width="75px;" height="75px;" title="<?php echo $getdataforResearch['image_path']; ?>">
                                    </div>

                                </div>

<?php }
?>

                        </div>
                    </td>
                </tr> 

                <tr> 
                    <td class="input-lbl">PDF File<font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="ResearchPdf" id="ResearchPdf" accept="file/pdf" allowed="pdf" 
             <?php if ($action == "Create") { ?>      
                                   required = 'required';
                        <?php }  ?>
                               >
                               <?php if ($getdataforResearch['pdf_file_path'] != "") { ?>

                        <div style="padding-top: 5px;">
                                <a href="<?php echo $staticContentUrl . '/Research/PDF/' . $getdataforResearch['id'] . '/' . $getdataforResearch['pdf_file_path']; ?>"  target="_blank">

                                    <font color="#0000FF"> <?php echo $getdataforResearch['pdf_file_path']; ?></font>

                                </a>
                            </div>


                            <?php } ?>

                        </div>
                    </td>
                </tr> 
  
                <tr>
                    <td class="input-lbl">Status<font color="#FF0000">*</font></td>
                    <td>
                        <input type="radio" name="status" value="1"  <?php if ($getdataforResearch["status"] == "1") { ?> checked <?php } ?> required="true"
                               
                                <?php  if ($action == "Create") { ?> 
                              checked
                                <?php } ?>
                               
                               >Active
                        <input type="radio" name="status" value="2"  <?php if ($getdataforResearch["status"] == "2") { ?> checked <?php } ?> required="true">Inactive

                    </td>
                </tr>

                <tr>
                    <td class="input-lbl">In Commercial<font color="#FF0000">*</font></td>
                    <td>
                        <input type="radio" name="in_commercial" value="0"  <?php if ($getdataforResearch["in_commercial"] == "0") { ?> checked <?php } ?> required="true"
                               
                                <?php  if ($action == "Create") { ?> 
                              checked
                                <?php } ?>
                               
                               >No
                        <input type="radio" name="in_commercial" value="1"  <?php if ($getdataforResearch["in_commercial"] == "1") { ?> checked <?php } ?> required="true">Yes

                    </td>
                </tr>

                <tr>
                    <td> </td>
                    <td> 
                        <input type="submit" name="submit" class="addmorebtn" 
                               value="<?php  if ($action == "Create") { ?>Sumbit<?php  } else {?>Update<?php } ?>">
                        <?php  if ($action == "Create") { ?>  
                        <input type="reset" id="reset" name="reset" class="addmorebtn" >
                        <?php }  else { ?>
                         <input type="button" onclick="window.location='<?php echo C_ROOT_URL  ?>research/index.php'" class="addmorebtn" value="Cancel">
                        <?php  } ?> 
                    </td>
                </tr>
        </table>

    </form>
</body>
</html>