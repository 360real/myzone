<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Property Name already in record!");
	
	define(TABLE_PRICES_NRI, 'tsr_prices_nri');
	$tblName	=	TABLE_PROPERTIES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/price_nri/controller/controller.php';
	$page_name_insert  =	'meta_manager.php';
	$page_name_update = C_ROOT_URL.'/property/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";
	$regn_ip = $_SERVER['REMOTE_ADDR'];


	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."price_nri/model/model.php";
			break;	
		default:
			$file_name = ROOT_PATH."price_nri/model/model.php";
			break;
	}


	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		
		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES UPDATE Code Block Start Here
		/*--------------------------------------------------------------  */
		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['price'] = $_POST["prices"];
			$arr['modified_by'] = $_SESSION['login']['id'];

		/* -------------------------------------------------------------  */
			$newPath = UPLOAD_PATH_PROPERTY_IMAGE.$_POST["property_name"];
			$oldPath = UPLOAD_PATH_PROPERTY_IMAGE.$_POST["existingPropertyName"];

			if($_POST["existingPropertyName"]!=$_POST["property_name"])
			{
				$flagRename = rename($oldPath, $newPath);
				$counterRename="1";
				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
			}
			
			
			if($obj_mysql->isDupUpdate($tblName,'property_name', $arr['property_name'] ,'id',$rec_id) && $counterRename!="1"){
				$obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);
				
				/* CODE FOR PROPERTY PRICES UPDATE BLOCK */
				$count=sizeof($_POST["type"]);
				
				for($counter=0;$counter<$count;$counter++){
				if($_POST["type"][$counter]!="")
				{
			
				$arrPrices["propty_id"] = $_GET["id"];
				$arrPrices["priceId"] = $_POST["priceId"][$counter];
				$arrPrices["type"] = $_POST["type"][$counter];
				$arrPrices["size"] = $_POST["size"][$counter];
				$arrPrices["price"] = $_POST["price"][$counter];
				$arrPrices["amount"] = $_POST["amount"][$counter];
				$arrPrices["booking_amount"] = $_POST["booking_amount"][$counter];
				$arrPrices["property_booking_status"] = $_POST["property_booking_status"][$counter];
				$arrPrices["status"] = $_POST["typestatus"][$counter];
				
				
			// 	if($_FILES["floor_plan".$counter]["name"]!="")
			// 	{
			// 	@unlink(UPLOAD_PATH_PROPERTY_IMAGE.$_GET["id"]."/floorplans/".$_POST['old_file_name'.$counter]);	
			// 	$handle = new upload($_FILES["floor_plan".$counter]);
			// 	if ($handle->uploaded)
			// 	{
			// 		/*$handle->image_resize         = true;
			// 		$handle->image_x              = 771;
			// 		$handle->image_y              = 569;
			// 		$handle->image_ratio_y        = true;*/
			// 		$handle->file_safe_name = true;
			// 		$handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$_GET["id"]."/floorplans/");
			// 		if ($handle->processed) {
			// 			//echo 'image resized';
			// 			$arrPrices["floor_plan"] = $handle->file_dst_name;
						
			// 		$handle->clean();
			// 		} else {
			// 			echo 'error : ' . $handle->error;
			// 		}
			// 	}
				 
			// }
			// else
			// {
			// 	$arrPrices["floor_plan"]=$_POST['old_file_name'.$counter];
			// }

				if($arrPrices["priceId"]=="" && $arrPrices["type"]!="")
				$obj_mysql->insert_data(TABLE_PRICES_NRI,$arrPrices);
				else
				$obj_mysql->update_data(TABLE_PRICES_NRI,$fld_id,$arrPrices,$arrPrices["priceId"]);
				}
				}
		/* CODE FOR PROPERTY PRICES UPDATE BLOCK */

				$obj_common->redirect($page_name_update."?action=search&pid=".$rec_id."");	
			}
			else{
				$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}
		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES UPDATE Code Block END Here
		/*--------------------------------------------------------------  */
		

		/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES INSERT Code Block START Here
		/*--------------------------------------------------------------  */
		
		else{
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['price'] = $_POST["prices"];
			
	
			
			if($obj_mysql->isDuplicate($tblName,'property_name', $arr['property_name'])){
				$rec_id = $obj_mysql->insert_data($tblName,$arr);
				
				/* CODE FOR PROPERTY PRICES INSERT BLOCK */
				$count=sizeof($_POST["type"]);
				for($counter=0;$counter<$count;$counter++){
				// $arrPrices["floor_plan"]=$_FILES["floor_plan".$counter]["name"];
				$arrPrices["priceId"] = $_POST["priceId"][$counter];
				$arrPrices["propty_id"] = $rec_id ;
				$arrPrices["type"] = $_POST["type"][$counter];
				$arrPrices["size"] = $_POST["size"][$counter];
				$arrPrices["price"] = $_POST["price"][$counter];
				$arrPrices["amount"] = $_POST["amount"][$counter];
				$arrPrices["booking_amount"] = $_POST["booking_amount"][$counter];
				$arrPrices["property_booking_status"] = $_POST["property_booking_status"][$counter];
				$arrPrices["status"] = $_POST["typestatus"][$counter];

				if($_POST["type"][$counter]!="")
				{

				// if($_FILES["floor_plan".$counter]["name"]!="")
				// {
				// $handle = new upload($_FILES["floor_plan".$counter]);
				// if ($handle->uploaded)
				// {
				// /*$handle->image_resize         = true;
				// $handle->image_x              = 771;
				// $handle->image_y              = 569;
				// $handle->image_ratio_y        = true;*/
				// $handle->file_safe_name = true;
				// $handle->process(UPLOAD_PATH_PROPERTY_IMAGE.$_GET["id"]."/floorplans/");
				// if ($handle->processed) {
				// //echo 'image resized';
				// $arrPrices["floor_plan"] = $handle->file_dst_name;
				// @unlink(UPLOAD_PATH_PROPERTY_IMAGE.$_GET["id"]."/floorplans/".$_POST['old_file_name'.$counter]);
				// $handle->clean();
				// } else {
				// echo 'error : ' . $handle->error;
				// }
				// }

				// }
				// else
				// {
				// $arrPrices["floor_plan"]=$_POST['old_file_name'.$counter];
				// }
			
				if($arrPrices["priceId"]=="" && $arrPrices["type"]!="")
				$obj_mysql->insert_data(TABLE_PRICES_NRI,$arrPrices);
				else
				$obj_mysql->update_data(TABLE_PRICES_NRI,$fld_id,$arrPrices,$arrPrices["priceId"]);
				}

				}
				/* CODE FOR PROPERTY PRICES INSERT BLOCK */
			
				$obj_common->redirect($page_name_insert."?msg=insert");	
			}else{
				$obj_common->redirect($page_name_insert."?msg=unique");
				$msg='unique';	
			}	
		}

	/* -------------------------------------------------------------  */
			// PROPERTY , PRICES , IMAGES INSERT Code Block END Here
	/*--------------------------------------------------------------  */
	}	
	
	/* -------------------------------------------------------------  */
			// PROPERTY DETAILS DISPLAY Code Block START Here
	/*--------------------------------------------------------------  */
	
	/* -------------------------------------------------------------  */
			// PROPERTY DETAILS DISPLAY Code Block END Here
	/*--------------------------------------------------------------  */
?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Property', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
 </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
