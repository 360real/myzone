<?php date_default_timezone_set('Asia/Kolkata');
    include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
	//include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
	//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
	include_once (LIBRARY_PATH."library/server_config_admin.php");
	require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
	admin_login();
	$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
	$tblName	=	TABLE_INTERNAL_NEWS;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/welcome_mail/controller/controller.php';
	$clsRow1	=	'clsRow1';
	$clsRow2	=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform task like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	


	/* choices to where page is redirect
		if action=create then goes to v
		iew  	
			action =update then model
			action default to view i.e index.php
	*/

	$file_name = ROOT_PATH."/welcome_mail/view/view.php" ;
	
	/***********************************************************************************/
			//Code  for search a record from database on click submit  button

	/***********************************************************************************/
	
		if($_GET["action"]=="search")
		{
			  if($_GET['name']!="")
			  $sql = "SELECT * FROM welcomemail_tracking WHERE username like '%".trim($_GET["name"])."%' ORDER BY id DESC";
				else if($_GET['email']!="")
					$sql = "SELECT * FROM welcomemail_tracking WHERE emailid like '%".trim($_GET["email"])."%' ORDER BY id DESC";
				else if(($_GET['startDate']!=""))
					$sql = "SELECT * FROM welcomemail_tracking WHERE  date(welcomemail_tracking.sendondate)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' ORDER BY id DESC";
		}
		else{
		$sql = "SELECT * FROM welcomemail_tracking ORDER BY id DESC";
		}
		//$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	

	        /*---------------------paging script start----------------------------------------*/
        //echo $sql;
        $obj_paging->limit = 30;
        if ($_GET['page_no']) {
            $page_no = remRegFx($_GET['page_no']);
        } else {
            $page_no = 0;
        }

        $queryStr = $_SERVER['QUERY_STRING'];

        /*--------------------if page_no alreay exists please remove them ---------------------------*/
        $str_pos = strpos($queryStr, 'page_no');
        if ($str_pos > 0) {
            $queryStr = str_replace(substr($queryStr, ($str_pos - 1), strlen($queryStr)), "", $queryStr);
        }

        /*------------------------------------------------------------------------------------------*/
        $obj_paging->set_lower_upper($page_no);
        $total_num = $obj_mysql->get_num_rows($sql);

        $paging = $obj_paging->next_pre($page_no, $total_num, $page_name . "?$queryStr&", 'textArial11Bold', 'textArial11orgBold');
        $total_rec = $obj_paging->total_records($total_num);
        $sql .= " LIMIT $obj_paging->lower,$obj_paging->limit";
        /*---------------------paging script end----------------------------------------*/
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));

	/************************************************************************************************/
			// code for search and pagination ends here

	/************************************************************************************************/	

	
//print_r($rec);die;
	$list="";
	$j=1;
	for($i=0;$i< count($rec);$i++)
	{
		if($rec[$i]['status']=="1")
		{
			$st='<font color=green>Active</font>';
		}
		else
		{
			$st='<font color=red>Inactive</font>';
		}
		if(($rec[$i]['emailread']!='0000-00-00 00:00:00')&&(($rec[$i]['emailread']!=''))){
			$mailtrack = $rec[$i]['emailread'];
		}
		else{ $mailtrack = 'Not open Yet'; }
		if(($rec[$i]['inductionbegin']!='0000-00-00 00:00:00')&&(($rec[$i]['inductionbegin']!='')&&($rec[$i]['inductionbegin']!=NULL))){
			$begin = 'YES';
		}
		else{ $begin = 'No'; }
			
		$quizMailStatus = ($rec[$i]['quizmail_send'] == 1) ? '<font color=blue>Sent</font>' : '<font color=orange>Not Sent</font>';

		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='
		<tr class="'.$clsRow.'">
			
			<td class="center">'.$j.'. </td>
			<td class="center">'.$rec[$i]['username'].'</td>
			<td class="center">'.$rec[$i]['emailid'].'</td>
			<td class="center">'.$rec[$i]['cc_mail'].'</td>
			<td class="center">'.$rec[$i]['sendbyuser'].'</td>
			<td class="center">'.$rec[$i]['sendondate'].'</td>
			<td class="center">'.$rec[$i]['ipaddress'].'</td>
			<td class="center">'.$mailtrack.'</td>
			<td class="center">'.$begin.'</td>
			<td class="center">'.$rec[$i]['totalsend'].'</td>
			<td class="center">'.$quizMailStatus.'</td>
			<td class="center">'.$st.'</td>

		</tr>';
		$j++;
	}

	if($list=="")
	{
		$list="<tr><td colspan=10 align='center' height=50>No Record(s) Found.</td></tr>";
	}
?>

<!--************************************** code to include common header ***************************************/-->
<?php include(LIBRARY_PATH."includes/header.php");
	$commonHead = new CommonHead();
	$commonHead->commonHeader('Internal News Manager', $site['TITLE'], $site['URL']); 
?>
<!--************************************** End of code to include header ***************************************/-->
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
	<tr>
	  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr>
					<td>
						<?php 
						if($_GET['msg']!="")
						{
							echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
						}
						if($msg!="")
						{
							echo('<div class="notice">'.$message_arr[$msg].'</div>');
						}
						?>
					</td>
		        </tr>
		    </table>
	    </td>
	</tr>

	<tr>
	  <td colspan="2" valign="top"><?php include($file_name	);?></td> <!-- include file_name to get the page on controller page -->
	</tr>

	<tr>
	  <td colspan="2" valign="top"></td>
	</tr>
</table>
<!-- Right Ends -->
<?php
function emailtrack($data){

$a = $_SERVER['PHP_SELF'];
if (strpos($a, 'emailtracking') !== false) {
$b = $_GET['emailtracking'];
	
$uemail = base64_decode(urldecode(end($b)));

$latesttime = date('Y-m-d H:i:s');
echo $userquery ="UPDATE welcomemail_tracking SET emailread='".$latesttime."' WHERE emailid = '".$uemail."'";
$obj_mysql->get_assoc_arr($userquery); 
}
}
 ?>

<?php include(LIBRARY_PATH."includes/footer.php");?>