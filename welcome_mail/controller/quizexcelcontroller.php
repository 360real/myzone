<?php
//date_default_timezone_set('Asia/Kolkata');
// include "/var/www/html/myzone/adminproapp/includes/set_main_conf.php";
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");

include_once LIBRARY_PATH . "library/server_config_admin.php";
require_once LIBRARY_PATH . "library/PHPMailer_master/PHPMailerAutoload.php";

$userquery = "SELECT * FROM `welcomemail_tracking` WHERE `quizmail_send`= 1 ORDER BY sendondate DESC";
$data      = $obj_mysql->getAllData($userquery);
$rec       = $data;


header('Content-Type: application/octet-stream');   
header("Content-Transfer-Encoding: Binary"); 
header("Content-disposition: attachment; filename=\"welcome_mail".date("d-m-Y").".xls\""); 
date_default_timezone_set('Asia/Kolkata');

$list="";
  $j=1;
  for($i=0;$i< count($rec);$i++)
  {

    if(($rec[$i]['quizemail_open']!='0000-00-00 00:00:00')&&(($rec[$i]['quizemail_open']!='')&&($rec[$i]['quizemail_open']!=NULL))){
      $emailopen = 'YES';
    }
    else{ $emailopen = 'No'; }
 
    $clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
    $list.='
    <tr class="'.$clsRow.'">
      
      <td class="center">'.$j.'. </td>
      <td class="center">'.$rec[$i]['username'].'</td>
      <td class="center">'.$rec[$i]['emailid'].'</td>
      <td class="center">'.$rec[$i]['cc_mail'].'</td>
      <td class="center">'.$rec[$i]['sendbyuser'].'</td>
      <td class="center">'.$rec[$i]['sendondate'].'</td>
      <td class="center">'.$emailopen.'</td>
    </tr>';
    $j++;
  }

  if($list=="")
  {
    $list="<tr><td colspan=10 align='center' height=50>No Record(s) Found.</td></tr>";
  }


$a='<table width="100%" border="0" cellspacing="0" cellpadding="0" class="MainTable">
    <thead>
		<tr>
			<th><div>S.No.</div></th>
			<th><div>Name</div></th>
			<th><div>Email</div></th>
			<th><div>CC Email</div></th>
			<th><div>Send By </div></th>
			<th><div>Send On</div></th>
			<th><div>Mail Open</div></th>
		</tr>
    </thead>
    <tbody>
    '.$list.'
  </tbody>
  </table>';

echo $a;

?>