
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="<?php echo $site['URL']?>view/css/calendar.css" rel="stylesheet" type="text/css" />
  <script language="JavaScript" src="<?php echo $site['URL']?>view/email-editor/ckeditor.js"></script>
  <script language="JavaScript" src="<?php echo $site['URL']?>view/email-editor/js/sample.js"></script>
  <link href="<?php echo $site['URL']?>view/email-editor/css/samples.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $site['URL']?>view/toolbarconfigurator/email-editor/lib/codemirror/neo.css" rel="stylesheet" type="text/css" />

</head>

<div  id="progressbar" style="position: absolute;background: #000c;width: 100%;height: 100%;top: 0;bottom: 0;left: 0;right: 0;display: none;z-index: 999!important;">
<img src="<?php echo C_ROOT_URL ?>view/images/UnitedSmartBinturong-max-1mb.gif" style="position: absolute;left: 0;right: 0;bottom: 0;top: 0;margin: auto;width:100px;" />
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">

<!-- -------------------------------------------------------------  -->
			<!-- NEWS DETAILS Search Code Block Start Here -->
<!-- --------------------------------------------------------------  -->

<style type="text/css">
  /* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 30%;
  right: 0;
  margin:auto;
  width: 100%; /* Full width */
  height: auto; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
 
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>

<style>
/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 60%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
#cke_description {
    width: 100% !important;
}
</style>
<?if($_GET["action"]!="Create" && $_GET["action"]!="Update"){?>

<table width="100%" border="0" cellspacing="0" cellpadding="2" class="SearchTable">
<thead>

<tr>
  <th colspan="8" align="left">
    <h3 class="user_serch_head">Search Mail</h3> 
    <a class="welcomedemo_open" href="#welcomedemo" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 5px 7px; text-decoration: none;">Welcome Mail</a>
  </th>
</thead>
<tbody>
  <tr class="search-form">
    <td></td>
  <?php 
      $dynamicurl  = C_ROOT_URL.'welcome_mail/controller/excelcontroller.php';
      $mailsendurl = C_ROOT_URL.'welcome_mail/view/mailwithexcel.php';
      $excelsend   = C_ROOT_URL.'welcome_mail/view/excel.php';
      //$welcomeexcelsend   = C_ROOT_URL.'welcome_mail/controller/UpdateemailQuiz.php';
  ?>

  <form  method="get"  name="datainexcel"  action="<?php echo $dynamicurl; ?>"  >
  
  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
  <input type="hidden" name="name" value="<?php echo $_GET['name'] ?>">
  <input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
  <input type="hidden" name="submit" value="<?php echo $_GET['submit'] ?>">
  <input type="hidden" name="startDate" value="<?php echo $_GET['startDate'] ?>">
  <td>
  <input type="submit" name="exceldata" value="datainexcel" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;"></td>
</form>



  <form  method="get"  name="quizdatainexcel"  action="<?php echo $dynamicurl; ?>"  >
  
  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
  <input type="hidden" name="name" value="<?php echo $_GET['name'] ?>">
  <input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
  <input type="hidden" name="submit" value="<?php echo $_GET['submit'] ?>">
  <input type="hidden" name="startDate" value="<?php echo $_GET['startDate'] ?>">
  <td>
  <input type="submit" name="quizexceldata" value="quizdatainexcel" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;"></td>
</form>

<form  method="get"  name="notstarted"  action="<?php echo $dynamicurl; ?>"  >
  
  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
  <input type="hidden" name="name" value="<?php echo $_GET['name'] ?>">
  <input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
  <input type="hidden" name="submit" value="<?php echo $_GET['submit'] ?>">
  <input type="hidden" name="startDate" value="<?php echo $_GET['startDate'] ?>">
  <input type="hidden" name="status" value="1">
  <td>
  <input type="submit" name="exceldata" value="Not Started" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;"></td>
</form>


<form  method="post"  name="notstarted"  action="<?php echo $excelsend; ?>"  >
  
  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
  <input type="hidden" name="name" value="<?php echo $_GET['name'] ?>">
  <input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
  <input type="hidden" name="submit" value="<?php echo $_GET['submit'] ?>">
  <input type="hidden" name="startDate" value="<?php echo $_GET['startDate'] ?>">
  <input type="hidden" name="status" value="1">
  <td>
  <input type="submit" name="exceldata" value="Excel Not Started" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;"></td>
</form>

<form  method="post"  name="notstarted"  action="<?php echo $excelsend; ?>"  >
  
  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
  <input type="hidden" name="name" value="<?php echo $_GET['name'] ?>">
  <input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
  <input type="hidden" name="submit" value="<?php echo $_GET['submit'] ?>">
  <input type="hidden" name="startDate" value="<?php echo $_GET['startDate'] ?>">
  <input type="hidden" name="status" value="2">
  <td>
  <input type="submit" name="exceldata" value="Excel Pending Induction" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;"></td>
</form>

<form  method="post"  name="notstarted"  action="<?php echo $mailsendurl; ?>"  >
  
  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
  <input type="hidden" name="name" value="<?php echo $_GET['name'] ?>">
  <input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
  <input type="hidden" name="submit" value="<?php echo $_GET['submit'] ?>">
  <input type="hidden" name="startDate" value="<?php echo $_GET['startDate'] ?>">
  <input type="hidden" name="status" value="1">
  <td>
  <input type="submit" name="exceldata" value="Send Mail To Not Started" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;"></td>
</form>

<form  method="post"  name="notstarted"  action="<?php echo $mailsendurl; ?>"  >
  
  <input type="hidden" name="action" value="<?php echo $_GET['action'] ?>">
  <input type="hidden" name="name" value="<?php echo $_GET['name'] ?>">
  <input type="hidden" name="email" value="<?php echo $_GET['email'] ?>">
  <input type="hidden" name="submit" value="<?php echo $_GET['submit'] ?>">
  <input type="hidden" name="startDate" value="<?php echo $_GET['startDate'] ?>">
  <input type="hidden" name="status" value="2">
  <td>
  <input type="submit" name="exceldata" value="Send Mail To Pending Induction" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;"></td>
</form>
</tr>
<tr class="search-form">
<td></td>

<td>
  <button id="myBtn" style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;">Send Quiz</button>
</td>
<td>
   <a href="<?php  echo C_ROOT_URL.'welcome_mail/controller/quizexcelcontroller.php';?>">
      <button  style="background: #eeeeee; border: 1px solid #000000; border-radius: 4px; color: #000000; font-size: 12px; line-height: 25px; padding: 0px 2px; text-decoration: none;">Welcome Quiz Exel</button>
   </a>
</td>

  <form  method="get"  name="searchForm" onsubmit="return validateForm(this);" >
    <input type="hidden" name="action" value="search">
    <td ><strong>User Name</strong>
    <input type="text" name="name" title="name" lang='MUST' value="<?php echo($_GET['name'])?>">
    </td>
    <td><input name="submit" type="submit" class="button" value="Search" /></td>
  </form>

  <form  method="get"  name="searchForm" onsubmit="return validateForm(this);" >
    <input type="hidden" name="action" value="search">
    <td ><strong>User Email</strong>
    <input type="text" name="email" title="email" lang='MUST' value="<?php echo($_GET['email'])?>">
    </td>
    <td><input name="submit" type="submit" class="button" value="Search" /></td>
  </form>

</tr>
<tr class="search-form">
  <td></td>
  <td></td>
  <td></td>
  <td></td>
  <form  method="get"  name="testform" onsubmit="return validateForm(this);" >
    <td >Send Date:-
        <input type="text" name="startDate" />
        <script language="JavaScript">
          new tcal ({
          // form name
          'formname': 'testform',
          // input name
          'controlname': 'startDate'
          });
        </script>
    </td>
    <td></td>
    <td>
      <input type="hidden" name="status" value="1">
      <input type="hidden" name="action" value="search">
      <input name="submit" type="submit" class="button" value="Search" />
    </td>
  </form>
</tr>


<?}?>
<!-- -------------------------------------------------------------  -->
			<!-- NEWS DETAILS Search Code Block End Here -->
<!-- --------------------------------------------------------------  -->
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="2" class="paging" >
<tr>
<!--<td width="285" nowrap="nowrap"><a href="<?=$page_name?>?action=Create" class="add">Add New</a></td> -->
<td width="43" align="right" nowrap="nowrap" class="pagenumber"></td>
<td width="635" align="right" class="prevnext"><?php echo($paging);?></td>
</tr>
</table>
<form name='frm_list' method='post' action="<?=$page_name?>">
  <input type='hidden' name='action' id='Action'>
 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="MainTable">
    <thead>
      <tr>
      <th><div>S.No.</div></th>
  		<th><div>Name</div></th>
  		<th><div>Email</div></th>
      <th><div>CC Email</div></th>
      <th><div>Send By </div></th>
      <th><div>Send On</div></th>
      <th><div>IP Address</div></th>
      <th><div>Open At</div></th>
      <th><div>Induction Begin</div></th>
      <th><div>Total Send</div></th>
      <th><div>Quiz Mail</div></th>
      <th><div>Active</div></th>
      
      </tr>
    </thead>
    <tbody>
      <?php echo($list);  ?>
    </tbody>
  </table>
</form>
<table cellpadding="2" cellspacing="0" width="100%"  border="0" class="paging">
  <tr>
    <td colspan="2" valign="top" align="right" class="pagenumber" style="padding-right:30px;"><?php echo($total_rec);?> </td>
  </tr>
  <tr>
    <td colspan="2" valign="top"></td>
  </tr>
</table>

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<!-- <a class="demo_open" href="#demo" style="font-size: 15px;">asdf</a> -->

<!-- Start Here For welcome Mail to new joinee    -->

<div id="welcomedemo" class="posR checkwell"> 

<p class="SbSec">Email Details</p>
<div class="mainfsk">
  

<div class="section2"><input type="text" id="username" name="username" value="" placeholder="USER NAME"></div>
<div class="section2"><input type="text" id="welcomesendTo" name="welcomesendTo" value="" placeholder="EMAIL ADDRESS"></div>
<div class="section2"><input type="text" id="addCC" name="addCC" value="" placeholder="CC"></div>

<div class="sectio_12" id="welcomesendNewsSubmitButton"><input type="button" name="" value="submit" onclick="welcomesendNewsToEmployees();"></div>

</div>

    <button class="welcomedemo_close">X</button>
</div>


<!-- The Modal -->
<div id="myModal" class="modal">
  <div class="modal-content">
      <span class="close">&times;</span>
      <p class="SbSec">Email Details</p>
      <div class="mainfsk"> 
          <div class="section2" style="width: 90%">
          <input type="text" id="subject" name="subject"  placeholder="Subject" style="width: 98%;margin-bottom: 10px;">
        </div>
          <div class="section2" style="width: 90%;">
          <input type="date" id="start" name="trip-start" style="width: 47%;margin-bottom: 10px;float: left;padding: 7px;border: 1px solid #ccc;">
        
          <input type="date" id="end" name="trip-start"  style="width: 47%;margin-bottom: 10px;float: right;padding: 7px;margin-left: 8px;border: 1px solid #ccc;">
        </div>
        <div class="" style="clear:both"></div>

      <div class="section2" style="width: 90%">
        <textarea name="description"  id="description" class="ckeditor" rows="8" cols="70" style="width: 100%;margin-bottom: 10px;"></textarea> 
      </div>
      <br>
      <div class="sectio_12" id="welcomesendNewsSubmitButton">
        <input type="button" name="" value="submit" onclick="sendcustomeEmail();">
      </div>
      </div>
  </div>
</div>

<!-- End Here For welcome Mail to new joinee    -->

 <script src="<?php echo $site['URL']?>view/js/jquery.popupoverlay.js"></script>
<script>
$(document).ready(function () {

$('#welcomedemo').popup({
pagecontainer: '.container',
transition: 'all 0.3s',


});

});

$(document).ready(function () {

$('#mail_open').popup({
pagecontainer: '.container',
transition: 'all 0.3s',


});

});
</script>

<script>

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<style type="text/css">
  .checkwell{ display: none; background: #fff; max-width: 400px; padding: 30px; width: 100% }
  .mainfsk{ font-size: 0; margin: auto; text-align: center; }
  .SbSec{ font:bold 25px 'calibri',arial; text-align: center; padding-bottom: 10px; text-transform: uppercase; }
  .sectio_1, .section2{ width: 70%; display:block; vertical-align: top; margin: 10px auto; }
  .sectio_12{ width: 30%;  display: inline-block; vertical-align: top;}
  .sectio_1 input[type="text"], .section2 input[type="text"]{ width: 100%; padding: 0 5px; height: 40px;background: #fff; border: 1px solid #ddd;  }
  .sectio_12 input[type="button"]{ width: 100%; height: 42px; background: #6d1f6a; border: none; font-size: 16px; color:#fff;  }
  .demo_close , .welcomedemo_close {
    background: #6d1f6a none repeat scroll 0 0;
    border: medium none;
    color: #ffffff;
    height: 25px;
    position: absolute;
    right: 0;
    top: 0;
    width: 25px;
    font-size: 20px;
}
</style>

<script>
function conf(){
	return confirm('Are you want to Delete it?');
}



function welcomesendNewsToEmployees()
{
  
  
  var sendTo = document.getElementById('welcomesendTo').value;
  var username = document.getElementById('username').value;
  var addCC = document.getElementById('addCC').value;

  $("#sendNewsSubmitButton").html('<div style="padding:0 10px 10px 10px;">Please Wait . . . !!! <span style="font-size:14px; padding: 10px 0 0 10px; vertical-align: middle;">Please Wait..</span></div>');

  var base_url="<?php echo C_ROOT_URL ?>";

  $.ajax({
        //url: "view/changepoststatus.php",
        url:base_url+"/internal_news/view/welcomemail.php",
        
        type: 'POST',
        data: {username:username,sendTo:sendTo,addCC:addCC},
        success: function(data) {   
         alert('News Email is Sent');
         $("#sendNewsSubmitButton").html('<input type="button" name="" value="submit" onclick="sendNewsToEmployees();">')
        }
    });

}

function sendcustomeEmail(){

for (instance in CKEDITOR.instances) {
CKEDITOR.instances[instance].updateElement();
}

var description =  $('#description').val();
var subject     =  $("#subject").val();
var end         =  $("#end").val();
var start       =  $("#start").val();

if(description==''){
  alert("Please Fill Mail Field");
  return false;
}

 if(start==''){
  alert("Please Select start Date Field");
  return false;
}

 if(end==''){
  alert("Please Select End Date Field");
  return false;
}

if(subject==''){
  alert("Please Fill Subject Field");
   return false;
}else{

// AJAX Code To Submit Form.
$("#progressbar").css({"display":"block"});


$.ajax({
    type: "POST",
    url: "<?php echo C_ROOT_URL ?>/welcome_mail/view/emailQuiz.php",
    data: {'subject':subject,'end':end,'start':start,'description':description},
    cache: false,
    success: function(result){

      console.log(result);
      var obj = JSON.parse(result);

      if(obj.msg=="Success"){

        $("#progressbar").css({"display":"none"});
        $("#myModal").css({"display":"none"});
        alert("Mail Sent Successfully");
        window.location.reload();

      }else{

      alert("No data found!");
      $("#progressbar").css({"display":"none"});
     
      }
      
        
      
    
    },
    error:function(error)
    {
      console.log(error);
    }
});
}
return false;

}
</script>


