<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_MICRO_MARKET;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/micro_market/controller/controller.php';
	$page_name_update = '';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	

	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."micro_market/model/model.php" ;
			break;	
		default:
			$file_name = ROOT_PATH."micro_market/view/view.php" ;
			break;
	}

	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
	if($_GET["action"]=="search")
	{
		$sql = "SELECT cty.city, loc.id, loc.cty_id, loc.name, loc.description, loc.meta_title, loc.meta_keywords, loc.meta_description, loc.status, loc.modified_date, loc.created_date, loc.ordering, loc.ipaddress FROM ".TABLE_MICRO_MARKET." loc INNER JOIN ".TABLE_CITIES." cty ON loc.cty_id=cty.id WHERE ";
		if($_GET["cty_id"]!="")
			$sql.="loc.cty_id=".$_GET["cty_id"];
		if($_GET["name"]!="")
			$sql.=" loc.name like '%".trim($_GET["name"])."%'";
		if($_GET["status"]!="")
			$sql.="loc.status=".$_GET["status"];
		if($_GET["startDate"]!="" && $_GET["endDate"])
		{
			$sql.=" and date(loc.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(loc.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
		else
		if($_GET["startDate"]!="")
		{
			$sql.=" and date(loc.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
		}
		else
		if($_GET["endDate"]!="")
		{
			$sql.=" and date(loc.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
		}
	
	}
	else if($_GET["action"]=="Update")
	{
		$sql = "SELECT cty.city, loc.id, loc.cty_id, loc.name, loc.description, loc.meta_title, loc.meta_keywords, loc.meta_description, loc.status, loc.modified_date, loc.created_date, loc.ordering, loc.ipaddress FROM ".TABLE_MICRO_MARKET." loc INNER JOIN ".TABLE_CITIES." cty ON loc.cty_id=cty.id";
		
		$sql.= " WHERE loc.id=".$_GET["id"]; 
		//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
	}
	
	else if($_GET["action"]=="Delete") {

	    $obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
		//$obj_common->redirect($page_name."?action=search&cty_id=".$_GET["cty_id"]."");

		if($_GET["name"]!="")
				$obj_common->redirect($page_name."?action=search&name=".$_GET["name"]."&submit=Search&msg=delete");
					else
				$obj_common->redirect($page_name."?action=search&cty_id=".$_GET["cty_id"]."&submit=Search&page_no=".$_GET["pg_no"]."&msg=delete");

	}
	//echo "sdvs".$_GET["cty_id"];
	/*$cid=(($_REQUEST['cid'] > 0) ? ($_REQUEST['cid']) : '0');		
	if($action!='Update') $sql .=" AND parentid='".$cid."' ";*/
	$s=($_GET['s'] ? 'ASC' : 'DESC');
	$sort=($_GET['s'] ? 0 : 1 );
    $f=$_GET['f'];
	
	if($s && $f)
		$sql.= " ORDER BY $f  $s";
	else
		$sql.= " ORDER BY $fld_orderBy";
		//$sql.= " GROUP BY loc.id ORDER BY totalProperties desc";

	/*---------------------paging script start----------------------------------------*/
	
	$obj_paging->limit=50;
	if($_GET['page_no'])
		$page_no=remRegFx($_GET['page_no']);
	else
		$page_no=0;
	$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
	$str_pos=strpos($queryStr,'page_no');
	if($str_pos>0)
		$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
$obj_paging->set_lower_upper($page_no);
	$total_num=$obj_mysql->get_num_rows($sql);
	
	$paging=$obj_paging->next_pre($page_no,$total_num,$page_name."?$queryStr&",'textArial11Bold','textArial11orgBold');
	$total_rec=$obj_paging->total_records($total_num);
	$sql.= " LIMIT $obj_paging->lower,$obj_paging->limit";	
	/*---------------------paging script end----------------------------------------*/
	//echo $sql;	
    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}
	/*else
	{
		$sql = "SELECT loc.id, loc.cty_id, loc.name, loc.description, loc.meta_title, loc.meta_keywords, loc.meta_description, loc.status, loc.modified_date, loc.created_date, loc.ordering, loc.ipaddress FROM ".TABLE_LOCATIONS." loc";
		
		
		$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	} */
//echo "sdcd".$_GET["name"];
	/*print "<pre>";
	print_r($rec);
	print "<!-- <pre> -->";*/

	if(count($_POST)>0){
		$arr=$_POST;
		$rec=$_POST;
		if($rec_id){
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by'] = $_SESSION['login']['id'];
			
				$obj_mysql->update_commercial_data($tblName,$fld_id,$arr,$rec_id);
				if($_GET["name"]!="")
				$obj_common->redirect($page_name."?action=search&name=".$_GET["name"]."&submit=Search&msg=update");
					else
				$obj_common->redirect($page_name."?action=search&cty_id=".$rec["cty_id"]."&submit=Search&page_no=".$_GET["pg_no"]."&msg=update");
				//$obj_common->redirect($page_name."?action=search&id=".$rec_id."");	
			
		}else{
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['created_by'] = $_SESSION['login']['id'];
			if($obj_mysql->isDuplicateLoc($tblName,'name', $arr['name'],'cty_id', $arr['cty_id'])){
				$obj_mysql->insert_table_data($tblName,$arr);
				$obj_common->redirect($page_name."?msg=insert");	
			}else{
				//$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	$list="";
	for($i=0;$i< count($rec);$i++){
		if($rec[$i]['status']=="1"){
			$st='<font color=green>Active</font>';
		}else{
			$st='<font color=red>Inactive</font>';
		}
	if($_GET['page_no']!=""){
		$updateUrl = $page_name."?action=Update&id=".$rec[$i]['id']."&pg_no=".$_GET['page_no'];
		
		$deleteUrl = $page_name."?action=Delete&id=".$rec[$i]['id']."&cty_id=".$rec[$i]['cty_id']."&pg_no=".$_GET['page_no'];

	}else if($_GET["name"]!=""){
	$updateUrl = $page_name."?action=Update&id=".$rec[$i]['id']."&name=".$_GET["name"];

	$deleteUrl = $page_name."?action=Delete&id=".$rec[$i]['id']."&cty_id=".$rec[$i]['cty_id']."&name=".$_GET["name"];

	}	else {
		$updateUrl = $page_name."?action=Update&id=".$rec[$i]['id'];
		$deleteUrl = $page_name."?action=Delete&id=".$rec[$i]['id']."&cty_id=".$rec[$i]['cty_id'];
	}
		
	if($_GET["action"]!="Update")
	{



		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='<tr class="'.$clsRow.'">
					<td><a href="'.$updateUrl.'" target="_blank" class="title_a">'.$rec[$i]['name'].'</a></td>
					<td class="center">'.$rec[$i]['city'].'</td>
					<td class="center">'.$rec[$i]['created_date'].'</td>
					<td class="center">'.$rec[$i]['ipaddress'].'</td>
					<td class="center">'.$st.'</td>
					<td><a href="'.$updateUrl.'" target="_blank" class="edit">Edit</a>
					<a href="'.$deleteUrl.'" class="delete" onclick="return conf()">Delete</a>
					</td>
				</tr>';
	}
	if($list==""){
			$list="<tr><td colspan=5 align='center' height=50>No Record(s) Found.</td></tr>";
	}
	}

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Micro Market', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
