<?php include_once(LIBRARY_PATH . "events/view/eventheader.php");
?>
<body>
    <form method="post" name="eventDetail" id="eventForm" onsubmit="validateForm(this);" enctype="multipart/form-data">
        <table width="100%" class="MainTable eventinfo">
            <thead>
                <tr>
            <h3><th align="left" colspan="2">EVENT DETAILS</th></h3>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="input-lbl"  >Country<font color="#FF0000">*</font></td>
                    <td>
                        <?php echo AjaxDropDownList(TABLE_COUNTRY, "country_id", "id", "country", $getdataforEvent["countryId"], "id", $site['CMSURL'] . "cluster/populate_locations_advance.php", "locationDiv", "locationLabelDiv", "property") ?></td>
                </tr>

                <?php
                if ($_GET["action"] == "Update") {
                    ?>
                    <tr>
                        <td class="input-lbl"><div  id="locationLabelDiv" name="locationLabelDiv"><font color="#FF0000">*</font>City</div></td>
                        <td >
                            <div  name="locationDiv" id="locationDiv">
                                <?php echo AjaxDropDownListAdvance(TABLE_CITIES, "cty_id", "id", "city", $getdataforEvent["countryId"], $getdataforEvent["cityId"], "id", $site['CMSURL'] . "cluster/populate_sublocations.php", "sublocationDiv", "sublocationLabelDiv", "country_id"); ?>
                            </div>
                        </td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td class="input-lbl" ><div id="locationLabelDiv" name="locationLabelDiv">City<font color="#FF0000">*</font></div></td>
                        <td><div  name="locationDiv" id="locationDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td class="input-lbl" >Event Template<font color="#FF0000">*</font></td>
                    <td><input type="radio" name="eventTemplate" value="1" required <?php if ($getdataforEvent["EventTemplateID"] == 1) { ?> checked <?php } ?>>Country Template
                        <input type="radio" name="eventTemplate" value="2" <?php if ($getdataforEvent["EventTemplateID"] == 2) { ?> checked <?php } ?>>State/City Template </td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Start Date<font color="#FF0000">*</font></td>
                    <?php
                    $startDateValue = "";
                    
                  
                    if (isset($getdataforEvent["eventStartDate"]) && $getdataforEvent["eventStartDate"] != '0000-00-00 00:00:00') {
                       
                        $startDateValue = date('Y-m-d', strtotime($getdataforEvent["eventStartDate"]));
                        
                      
                    }
                    ?>
                    <td><input type="date" name="eventstart" id="eventstart" value="<?php echo $startDateValue; ?>" onkeydown="return false" onfocusout="setendmaxdate();" required value="<?php echo $getdataforEvent['eventStartDate']; ?>"></td>
                </tr>
                <tr> 
                    <td class="input-lbl">Event End Date<font color="#FF0000">*</font></td>
                    <?php
                    $endDateValue = "";
                    if (isset($getdataforEvent["eventEndDate"]) && $getdataforEvent["eventEndDate"] != '0000-00-00 00:00:00') {
                        
                        $endDateValue = date('Y-m-d', strtotime($getdataforEvent["eventEndDate"]));
                    }
                    
                   
                    ?>
                    <td><input type="date" name="eventend" value="<?php echo $endDateValue; ?>" id="eventend" onkeydown="return false" required></td>
                </tr>
                <tr> 
                    <td class="input-lbl">Email Id<font color="#FF0000">*</font></td>

                    <td><input type="text" name="eventEmail" value="<?php echo $getdataforEvent["eventEmail"]; ?>" required=true></td>
                </tr>
                <tr> 
                    <td class="input-lbl">Event Campaign id</td>
                    
                    <td><input type="text" name="eventCampaignId" value="<?php echo $getdataforEvent["eventCampaignId"]; ?>"  ></td>
                </tr>
                <tr> 
                    <td class="input-lbl">Event Country Campaign Id</td>
                   
                    <td><input type="text" name="eventCountryCampaignId" value="<?php echo $getdataforEvent['eventCountryCampaignId']; ?>"  ></td>
                </tr>
                <tr> 
                    <td class="input-lbl">Event City Campaign Id</td>
                    
                  
                    <td><input type="text" name="eventCityCampaignId" value="<?php echo $getdataforEvent['eventCityCampaignId']; ?>"  ></td>
                </tr>
                <tr> 
                    <td class="input-lbl">Custom URL</td>
                    
                  
                    <td><input type="text" name="customUrl" value="<?php echo $getdataforEvent['customUrl']; ?>"  required maxlength="50"></td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Type</td>
                    <?php
                    $eventArray = explode(",", $getdataforEvent["eventType"]);
                    ?>
                    <td colspan="5">
                        <input type="checkbox" name="eventtypes[]" value="HotelEvent"  <?php if (in_array("HotelEvent", $eventArray)) { ?> checked <?php } ?>>Hotel Event 
                        <input type="checkbox" name="eventtypes[]" value="CanopyServices" <?php if (in_array("CanopyServices", $eventArray)) { ?> checked <?php } ?>>Canopy Services
                        <input type="checkbox" name="eventtypes[]" value="Hoarding"  <?php if (in_array("Hoarding", $eventArray)) { ?> checked <?php } ?>>Hoardings
                        <input type="checkbox" name="eventtypes[]" value="roadshow"  <?php if (in_array("roadshow", $eventArray)) { ?> checked <?php } ?>>Road Show
                        <input type="checkbox" name="eventtypes[]" value="corporate"  <?php if (in_array("corporate", $eventArray)) { ?> checked <?php } ?>>Corporate Event
                    </td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Banner<font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="bannerName[]" id="banner" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 
                             <?php if (!is_array($eventFileData['Banner']) && count($eventFileData['Banner'] == 0)) {?>  
                               required  
                             <?php } ?> 
                             multiple  />
                          <div id="bannerExists">
                            <?php
                             $BannerStr = '';
                            if (is_array($eventFileData['Banner']) && count($eventFileData['Banner'] > 0)){
                                $bannerStr = '';
                           foreach($eventFileData['Banner'] as $key => $value){
                            
                            ?>
                            
                            <div class="filediv"> 
                                <div style="float:left;">
                                     <img src="<?php echo $staticContentUrl.'event/'.'assets/'.$eventId."/Banner/".$value['name'];  ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                </div>
                               
                                <div class="deleteFIle deletediv" file-id="<?php echo $value['id'];  ?>" file-path="<?php echo $staticContentUrl.'event/'.'assets/'.$eventId."/Banner/".$value['name'];  ?>">
                                   <img src="<?php echo C_ROOT_URL.'view/images/clse_btn.jpg'  ?>" title="Delete"> 
                                </div>
                            </div>
                            
                            <?php
                            
                          
                             
                           }
                           
                            }?>
                             
                          </div>
                       
                    </td>
                </tr>
                <tr> 
                    <td class="input-lbl">Images<font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="Images[]" id="images" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
                            <?php if (!is_array($eventFileData['Images']) && count($eventFileData['Images'] == 0)) {?>      
                               required 
                            <?php } ?>
                               multiple >
                    
                        <div id="imagesExists">
                            
                            <?php
                            if (is_array($eventFileData['Images']) && count($eventFileData['Images'] > 0)){
                           foreach($eventFileData['Images'] as $value){
                            
                            ?>
                            
                            <div class="filediv"> 
                                <div style="float:left;">
                                     <img src="<?php echo $staticContentUrl.'event/assets/'.$eventId."/Images/".$value['name'];  ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                </div>
                               
                                <div  class="deleteFIle deletediv" file-id="<?php echo $value['id'];  ?>" file-path="<?php echo $staticContentUrl.'event/assets/'.$eventId."/Images/".$value['name'];  ?>">
                                   <img src="<?php echo C_ROOT_URL.'view/images/clse_btn.jpg'  ?>" title="Delete"> 
                                </div>
                            </div>
                            
                            <?php
                              
                           }
                           
                            }?>
                            
                        </div>
                    </td>
                </tr> 
                <tr>
                    <td class="input-lbl" >Is NRI</td>
                    <td><input type="checkbox" name="isNri" value="1" <?php if ($getdataforEvent["isNri"] == 1) { ?> checked <?php } ?>>Yes</td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Title</td>
                    <td><textarea name="eventTitle"  id="eventTitle" rows="4" cols="40" maxlength="50"> <?php echo $getdataforEvent["EventTitle"]; ?> </textarea></td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Description</td>
                    <td><textarea class="ckeditor" name="eventDescription"  id="eventDescription" rows="4" cols="70"><?php echo $getdataforEvent["EventDescription"]; ?></textarea></td>
                </tr>
                <!-- <tr>
                        <td>Display Email Address</td> 
                        <td><input type="text" name="displayEmail" size="50%"></td>
                </tr> -->
                <tr>
                    <td class="input-lbl">Display Mobile No.</td>
                    <td><input type="text"  name="displayMobileNo" id="displayMobileNo" maxlength="25" value="<?php echo $getdataforEvent["PPCnumber"]; ?>" onkeypress="return isNumber(event)"/></td>
                </tr>
                <tr>
                    <td class="input-lbl">Brochure</td>
                    <td>
                        <input type="file" name="brochure[]" accept="image/jpeg,image/png,application/pdf,image/x-eps,application/doc" allowed="png/jpg/jpeg/pdf/doc" multiple>
                    
                                            <div>
                            
                            <?php
                            if (is_array($eventFileData['Brochure']) && count($eventFileData['Brochure'] > 0)){
                           foreach($eventFileData['Brochure'] as $value){
                            
                            ?>
                            
                            <div class="filediv" > 
                                <div style="float:left;">
                                    <?php
                                    $imageCheckArr = array('jpeg','png','jpg','png');
                                    
                                    $fileType = substr($value['name'],strrpos($value['name'],".")+1);
                                            
                                           
                                    
                                    ?>
                                    
                                   <?php if (in_array($fileType,$imageCheckArr)) {?> 
                                     <img src="<?php echo $staticContentUrl.'event/assets/'.$eventId."/Brochure/".$value['name'];  ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                   <?php } else {?>
                                     
                                     <a href="<?php echo $staticContentUrl.'event/assets/'.$eventId."/Brochure/".$value['name'];  ?>" style="color:black;"><?php echo $value['name'];  ?></a>
                                   <?php } ?>
                                
                                </div>
                               
                                <div class="deleteFIle deletediv" file-id="<?php echo $value['id'];  ?>" file-path="<?php  echo $staticContentUrl.'event/assets/'.$eventId."/Brochure/".$value['name'];   ?>">
                                   <img src="<?php echo C_ROOT_URL.'view/images/clse_btn.jpg'  ?>" title="Delete"> 
                                </div>
                            </div>
                            
                            <?php }
                            }?>
                        </div>
                    
                    
                    
                    </td>
                </tr>
                <tr>
                    <td class="input-lbl">Leaflet</td>	
                    <td>
                        
                        <input type="file" name="leaflet[]" accept="image/jpeg,image/png,application/pdf,image/x-eps,application/doc" allowed="png/jpg/jpeg/pdf/doc" multiple>
                          <div>
                            
                            <?php
                            if (is_array($eventFileData['leaflet']) && count($eventFileData['leaflet'] > 0)){
                           foreach($eventFileData['leaflet'] as $value){
                            
                            ?>
                            
                            <div class="filediv"> 
                                <div style="float:left;">
                                  <?php     
                                    $fileType = substr($value['name'],strrpos($value['name'],".")+1);
                                     ?>
                                    
                                   <?php if (in_array($fileType,$imageCheckArr)) {?> 
                                     <img src="<?php echo $staticContentUrl.'event/assets/'.$eventId."/leaflet/".$value['name'];  ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                <?php } else {?>
                                     
                                     <a href="<?php echo $staticContentUrl.'event/assets/'.$eventId."/leaflet/".$value['name'];  ?>" style="color:black;" ><?php echo $value['name'];  ?></a>
                                  
                                <?php } ?>
                                
                                
                                </div>
                               
                                <div class="deleteFIle deletediv" file-id="<?php echo $value['id'];  ?>" file-path=""<?php echo $staticContentUrl.'event/assets/'.$eventId."/leaflet/".$value['name'];  ?>" >
                                   <img src="<?php echo C_ROOT_URL.'view/images/clse_btn.jpg'  ?>" title="Delete This"> 
                                </div>
                            </div>
                            
                            <?php }
                            }?>
                        </div>
                        
                    </td>
                </tr>

                <tr>
                    <td class="input-lbl">Event Meta Title</td>
                    <td><textarea class="ckeditor" name="eventMetaTitle"  id="eventMetaTitle" rows="4" cols="70"><?php echo $getdataforEvent["MetaTitle"]; ?></textarea></td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Meta Description</td>
                    <td><textarea class="ckeditor" name="eventMetaDescription"  id="eventMetaDescription" rows="4" cols="70"><?php echo $getdataforEvent["MetaDescription"]; ?></textarea></td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Meta Keywords</td>
                    <td><textarea class="ckeditor" name="eventMetaKeyword"  id="eventMetaKeyword" rows="4" cols="70"><?php echo $getdataforEvent["metaKeywords"]; ?></textarea></td>
                </tr>

                <tr>
                    <td class="input-lbl">Youtube Video Link</td>
                    <td><input type="text" name="videoLink" rows="4" cols="70" value="<?php echo $getdataforEvent["youtubeVideoLink"]; ?>" placeholder="YouTube Embed Source code "></td>
                </tr>

                <tr>
                    <td class="input-lbl">Facebook Share Link</td>
                    <td><input type="text" name="fbShareLink" size="100%" value="<?php echo $getdataforEvent["facebookShareLink"]; ?>"></td>
                </tr>
                <tr>
                    <td class="input-lbl">Twitter Share Link</td>
                    <td><input type="text" name="twitterShareLink" size="100%" value="<?php echo $getdataforEvent["twitterShareLink"]; ?>"></td>
                </tr>
                <tr>
                    <td class="input-lbl">LinkedIn Share Link</td>
                    <td><input type="text" name="linkedinShareLink" size="100%" value="<?php echo $getdataforEvent["linkedInShareLink"]; ?>"></td>
                </tr>
                <tr>
                    <td class="input-lbl">Pinterest Share Link</td>
                    <td><input type="text" name="pinterestShareLink" size="100%" value="<?php echo $getdataforEvent["PinterestShareLink"]; ?>"></td>
                </tr>
                <tr>
                    <td class="input-lbl">Event Address</td>
                    <td><textarea class="ckeditor" name="eventAddress"  id="eventAddress" rows="4" cols="70" >
                            <?php echo $getdataforEvent["EventAddress"]; ?></textarea></td>
                </tr> 

                <tr>
                    <td class="input-lbl">Latitude</td>
                    <td><input type="text" name="eventLat" value="<?php echo $getdataforEvent["latitude"]; ?>"></td>
                </tr>
                <tr>
                    <td class="input-lbl">Longitude</td>
                    <td><input type="text" name="eventLon" value="<?php echo $getdataforEvent["longitude"]; ?>"></td>
                </tr>
                <tr>
                    <td class="input-lbl" style="vertical-align: top;">Property</td>
                    <td align="left">
                        <div>

                            <div class="pro-pd">

                                <?php 
                               
                                echo AjaxDropDownListforproperty(TABLE_COUNTRY, "eventPropertyCountry[]", "id", "country", $eventPropertyData[0]["country_id"], "id", $site['CMSURL'] . "cluster/populate_locations_advance.php", "eventpropertyCitydiv", "locationLabelDiv", 'Select Country','forproperty') ?>

                            </div>
                            <div class="pro-pd eventCitydiv" name="eventCitydiv" >
                                <select style="width:234px;" name="eventCity[]" class="showPropertyList">
                                     <option value="">-- Select City --</option>
                                    <?php
                                    if ($_GET["action"] == "Update") {
                                        
                                        $cityList = $modelObj->fetchCity($eventPropertyData[0]["country_id"]);
                                       
                                        foreach($cityList as $value){
                                        ?>
                                        
                                        <option value="<?php echo $value['id']; ?>"
                                                <?php if ($eventPropertyData[0]['city_id'] == $value['id']){ ?>
                                                selected
                                                <?php } ?>    >
                                                    <?php echo $value['city']; ?>
                                        </option>

                                    <?php
                                    
                                        }
                                        } else { ?>
                                        <option>-- Select City --</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="pro-pd eventPropertyDiv" name="eventPropertyDiv">
                                <select style="width:234px;" name="eventProperty[]" >
                                    <option value="">-- Select Property --</option>
                                    <?php
                                    if ($_GET["action"] == "Update") {
                                        
                                        $allPropertyList = $modelObj->fetchProperty($eventPropertyData[0]['city_id']);
                                        foreach($allPropertyList as $value){
                                        
                                        ?>

                                        <option value="<?php echo $value['id']; ?>"
                                              
                                                <?php if ($eventPropertyData[0]['property_id'] == $value['id']){?>
                                                selected
                                                <?php } ?>
                                                >
                                        <?php echo $value['property_name']; ?>
                                        
                                        </option>

                                        <?php }
                                    
                                        } else { ?>
                                        <option>-- Select Property --</option>
                                    <?php } ?>

                                </select>
                            </div>
                            <div class="pro-pd">
                                <input type="text" size="100%" name="propertyUsp[]"  placeholder="Property USB" value="<?php echo $eventPropertyData[0]['property_usp']; ?>">
                            </div>
                            <div class="pro-pd textrea">
                                <textarea class="ckeditor" id="<?php echo $propertyDecriptionId ?>"   name="propertyDescription[]"   rows="4" cols="70"  placeholder="Property Description" >

                                    <?php echo $eventPropertyData[0]['property_description']; ?>
                                    
                                </textarea>

                                <div id="addmoreproperty" class="addmorebtn pro-pdbt">Add More</div>

                            </div>

                        </div>

                        <?php
                        if (count($eventPropertyData)>1) {

                            for ($i = 1; $i < count($eventPropertyData); $i++) {
                                
                                $propertyDecriptionId = 'propertyDecriptionId_' . $i;
                                ?>

                                <div class="addmr">
                                    <div class="pro-pd">

                                        <?php echo AjaxDropDownListforproperty(TABLE_COUNTRY, "eventPropertyCountry[]", "id", "country", $eventPropertyData[$i]['country_id'], "id", $site['CMSURL'] . "cluster/populate_locations_advance.php", "eventpropertyCitydiv", "locationLabelDiv", 'Select Country') ?>

                                    </div>
                                    <div class="pro-pd eventCitydiv" name="eventCitydiv" >
                                        <select style="width:234px;"  name="eventCity[]" class="showPropertyList">
                                              <option value="">-- Select City --</option>
                                            <?php
                                          $cityList = $modelObj->fetchCity($eventPropertyData[0]["country_id"]);
                                       
                                        foreach($cityList as $value){
                                        ?>
                                        
                                        <option value="<?php echo $value['id']; ?>"
                                                <?php if ($eventPropertyData[$i]['city_id'] == $value['id']){ ?>
                                                selected
                                                <?php } ?>    >
                                                    <?php echo $value['city']; ?>
                                        </option>

                                    <?php
                                    
                                        } ?>
                                          </select>
                                    </div>
                                    <div class="pro-pd eventPropertyDiv" name="eventPropertyDiv">
                                        <select style="width:234px;" name="eventProperty[]">
                                               <option value="">-- Select Property --</option>
                                           <?php   
                                            $allPropertyList = $modelObj->fetchProperty($eventPropertyData[$i]['city_id']);
                                        foreach($allPropertyList as $value){
                                        
                                        ?>

                                        <option value="<?php echo $value['id']; ?>"
                                              
                                                <?php if ($eventPropertyData[$i]['property_id'] == $value['id']){?>
                                                selected
                                                <?php } ?>
                                                >
                                        <?php echo $value['property_name']; ?>
                                        
                                        </option>

                                        <?php } ?>
                                        </select>
                                    </div>
                                    <div class="pro-pd">
                                        <input type="text" size="100%" name="propertyUsp[]"  placeholder="Property USB" value="<?php echo $eventPropertyData[$i]['property_usp']; ?>">
                                    </div>
                                    <script type="text/javascript" src="<?php echo C_ROOT_URL  ?>view/ckeditor/ckeditor.js"></script>
                                    <div class="pro-pd textrea">
                                        <textarea class="ckeditor propertyDescription" id='<?php echo $propertyDecriptionId; ?>' name="propertyDescription[]"  rows="4" cols="70" placeholder="Property Description"  >
                               
                                            <?php echo $eventPropertyData[$i]['property_description']; ?>
                                                                   
                                        </textarea>
                                        <div class="addmorebtn pro-pdbt RemoveProperty">Remove</div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                        }
                        ?>
          <div id="addmorepropertyMainDIv"></div>
                    </td>

                </tr> 

                <tr>
                    <td class="input-lbl">Blogs to Show</td>
                    <td>
                        <?php
                        $selectBlogArray = explode(",", $getdataforEvent["BlogInterlinkedID"]);

                        $blogs = $modelObj->dropdownBlog();
                        ?>
                        <select name="blogs[]" multiple size="15" class="blogselectbox" style="width: 300px; height: 120px;">
                            <option value="">----Select----</option>
                            <?php foreach ($blogs as $blg) { ?>
                                <option value="<?php echo $blg['id']; ?>"

                                        <?php if (in_array($blg['id'], $selectBlogArray)) { ?> selected <?php } ?>

                                        >
                                            <?php echo $blg['title']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php if ($_SESSION['login']['role_id'] == '1') { /* Below two coulumns will only be visible to admin permission */ ?>
                    <tr>
                        <td class="input-lbl">Requirements</td>
                        <td>
                           <?php
                            
                 
                            $Requirementlist = $modelObj->dropdownRequirement();
                            ?>
                            <select name="eventRequirment[]">
                                <option value="0">--select Requirement--</option>
                                <?php foreach($Requirementlist as $value) {?>
                                 <option value="<?php echo $value['id']; ?>"
                                         <?php if ($getdataforEventRequirment[0]['RequirementId'] == $value['id']) { ?>
                                         
                                         selected
                                         <?php  } ?>
                                         
                                         ><?php echo $value['requirementName']; ?></option>
                                <?php } ?>
                            </select>
                            <input type="text" name="reqQuantity[]" placeholder="Quantity" value="<?php echo $getdataforEventRequirment[0]['Quantity']; ?>"> 

                            <div id="addReqirements" class="addmorebtn">Add More</div>
                            <?php  
                            if (count($getdataforEventRequirment)>1){
                             for($i=1; $i<count($getdataforEventRequirment); $i++){
                            ?>
                            <div>
                                <div  style="margin-top:5px;">
                              <?php  
                             $Requirementlist = $modelObj->dropdownRequirement();
                            ?>
                            <select name="eventRequirment[]">
                                <option value="0">--select Requirement--</option>
                                <?php 
                                echo $getdataforEventRequirment[$i]['RequirementId'];
                                foreach($Requirementlist as $value) {?>
                                 <option value="<?php echo $value['id']; ?>"
                                         <?php if ($getdataforEventRequirment[$i]['RequirementId'] == $value['id']){ ?>
                                         
                                         selected
                                         <?php } ?>
                                         
                                         ><?php echo $value['requirementName']; ?></option>
                                <?php } ?>
                            </select>
                              <input type="text" name="reqQuantity[]" placeholder="Quantity" value="<?php echo $getdataforEventRequirment[$i]['Quantity']; ?>"> 

                            <div class="RemoveRequirments addmorebtn">Remove</div>
                            
                                </div><br/>
                                
                            </div>
                            <?php 
                             }
                            
                             } ?>
                            <div id="addmoreRequirment"></div>
                        </td>
                    </tr>
                    <tr> 
                        <td valign="top" class="input-lbl">Campaigns Required</td>	
                        <?php
                        $compaignRequiredArr = array();

                        if ($getdataforEvent["campaignsRequired"] != "") {

                            $compaignRequiredArr = explode(",", $getdataforEvent["campaignsRequired"]);
                        }
                        ?>
                        <td colspan="12">
                            <table border="0" cellspacing="0" cellpadding="0">

                                <tr>
                                    <td><input type="checkbox" name="Campaigns[]" value="ppc" <?php if (in_array("ppc", $compaignRequiredArr)) { ?> checked <?php } ?>>PPC </td>
                                    <td><input type="checkbox" name="Campaigns[]" value="emailMarketing" <?php if (in_array("emailMarketing", $compaignRequiredArr)) { ?> checked <?php } ?> >Email Marketing </td>
                                    <td><input type="checkbox" name="Campaigns[]" value="smsCampaign" <?php if (in_array("smsCampaign", $compaignRequiredArr)) { ?> checked <?php } ?>>SMS </td>
                                    <td><input type="checkbox" name="Campaigns[]" value="fbCampaign" <?php if (in_array("fbCampaign", $compaignRequiredArr)) { ?> checked <?php } ?>>Facebook </td>
                                </tr>
                                <tr>
                                    <td> <input type="checkbox" name="Campaigns[]" value="linkedInCampaign" <?php if (in_array("linkedInCampaign", $compaignRequiredArr)) { ?> checked <?php } ?>>LinkedIn</td>
                                    <td>  <input type="checkbox" name="Campaigns[]" value="tvAd" <?php if (in_array("tvAd", $compaignRequiredArr)) { ?> checked <?php } ?>>TV Advertisement</td>
                                    <td><input type="checkbox" name="Campaigns[]" value="radioAd" <?php if (in_array("radioAd", $compaignRequiredArr)) { ?> checked <?php } ?>>Radio Advertisement</td>
                                    <td> <input type="checkbox" name="Campaigns[]" value="newspprAd" <?php if (in_array("newspprAd", $compaignRequiredArr)) { ?> checked <?php } ?>>Newspaper Advertisement</td></td>
                                </tr>
                                <tr>
                                    <td> <input type="checkbox" name="Campaigns[]" value="localChannelAd" <?php if (in_array("localChannelAd", $compaignRequiredArr)) { ?> checked <?php } ?>>Local Channel Advertisement</td>
                                    <td> <input type="checkbox" name="Campaigns[]" value="leafletAd" <?php if (in_array("leafletAd", $compaignRequiredArr)) { ?> checked <?php } ?>>Leaflet Advertisement</td>
                                    <td> <input type="checkbox" name="Campaigns[]" value="AffiliateMarketing" <?php if (in_array("AffiliateMarketing", $compaignRequiredArr)) { ?> checked <?php } ?>>Affiliate marketing</td>
                                    <td> <input type="checkbox" name="Campaigns[]" value="bingAd" <?php if (in_array("bingAd", $compaignRequiredArr)) { ?> checked <?php } ?>>Bing Browser Ads</td>
                                </tr>  
                            </table>
                        </td>
                    </tr>
                <?php }
                ?>
                <tr ><td align="right" ><strong>Web Scope</strong> </td>
                  <td>
                  <input type="radio" name="web_scope" value="1" id="1" <?php if (isset($getdataforEvent['web_scope'])&&$getdataforEvent['web_scope']==1){ echo("checked='checked'");}else{echo("checked='false'");} ?> /> 
                  <label for="1">Residential</label> 
                  <input type="radio" name="web_scope" value="2" id="2" <?php if (isset($getdataforEvent['web_scope'])&&$getdataforEvent['web_scope']==2){ echo("checked='checked'");}else{echo("checked='false'");} ?> /> 
                  <label for="1">Commercial</label>
                  <input type="radio" name="web_scope" value="0" id="0" <?php if (!isset($getdataforEvent['web_scope']) ||$getdataforEvent['web_scope']==0){ echo("checked='checked'");} ?>/> 
                  <label for="0">Both</label>  </td>
                </tr>
                <tr class="<?php echo($clsRow2)?>"><td align="right" ><strong>360 Edge</strong> </td>
                  <td>
                  <input type="radio" name="360_edge" value="0" <?php if (isset($getdataforEvent['360_edge'])&& $getdataforEvent['360_edge']==0){ echo("checked='checked'");} ?> /> 
                  <label for="0">No</label> 
                  <input type="radio" name="360_edge" value="1" <?php if (isset($getdataforEvent['360_edge'])&& $getdataforEvent['360_edge']==1){ echo("checked='checked'");} ?> /> 
                  <label for="1">Yes</label>
                  </td>
                </tr>

                <tr>
                    <td class="input-lbl">Status<font color="#FF0000">*</font></td>
                    <td>
                        <input type="radio" name="status" value="1" required <?php if ($getdataforEvent["status"] == "1") { ?> checked <?php } ?>>Under Review
<?php if (in_array($_SESSION['login']['role_id'], array('1', '4','3'))) { /* Only admin can access below options */ ?>
                            <input type="radio" name="status" value="2"  <?php if ($getdataforEvent["status"] == "2") { ?> checked <?php } ?>>Approved
                            <input type="radio" name="status" value="3"  <?php if ($getdataforEvent["status"] == "3") { ?> checked <?php } ?>>LIVE
                            <input type="radio" name="status" value="4"  <?php if ($getdataforEvent["status"] == "4") { ?> checked <?php } ?>>Inactive
                        <?php } ?>
                    </td>
                </tr>
            <input type="hidden" name="alldleleteFIleIds" id="alldeleteFIlesIds" value="">
            <input type="hidden" name="deleteFIlesPath" id="deleteFIlesPath" value="">
                <tr>
                    <td> </td>
                    <td> 
                        <input type="submit" name="submit" class="addmorebtn">
                        <input type="reset" id="reset" name="reset" class="addmorebtn" >
                    </td>
                </tr>
        </table>
    </form>
</body>
</html>