<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo $site['URL']?>view/css/admin-style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/css.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/event.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $site['URL']?>view/js/eventPage.js?>"></script>
</head>
<body>

<form method="post" id="eventBanner" enctype="multipart/form-data" >
	<table width="100%" class="MainTable eventinfo">
		<thead>
		<tr>
			<th align="left">BANNER MANAGER</th>
			<th><a href="<?= $pageName ?>?action=BannerListing" class="add viewdtl">VIEW EVENTS</a></th>
		</tr>
	</thead>
	<tbody>
             <tr>
                    <td class="input-lbl"  >Country</td>
                    <td><?php $countryList = $modelObj->selectAllCountry();
                   
                    
                    ?>
                    
                    
                        <select name="CountryId" id="bannerCountry" style="width:234px;">
                            <option value="0">select Country </option>
                            
                            <?php foreach($countryList as $value){ ?>
                            
                              <option value="<?php echo $value['id'] ?>"><?php echo $value['country']; ?> </option>
                            <?php } ?>
                            
                        </select>
                    
                       
                    
                    </td>
                </tr>

 
                    <tr>
                        <td class="input-lbl" ><div id="locationLabelDiv" name="locationLabelDiv">City<font color="#FF0000">*</font></div></td>
                        <td>
                            
                            <div  name="locationDiv" id="bannerCity">
                                <select style="width:234px;" name="bannerCity">
                                    
                                    <option value="0">-- Select City--</option>
                                </select>
                            </div>
                        </td>
                    </tr>
               
                    
                    <tr>
                        
                        
                    </tr>    
                    
                    
		<tr>
			<td class="input-lbl">Banner<font color="#FF0000">*</font></td>
			<td> <input type="file" name="bannerMainPage" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif"><font color="#FF0000">[* Image size Strictly preferred 1366x500 pixels]</font></td>
		</tr>
		<tr>
			<td class="input-lbl">Short Title<font color="#FF0000">*</font></td>
			<td><input type="text" name="shortTitle" required value="<?php echo $bannerData["ShortTitle"]; ?>"></td>
		</tr>
		<tr>
			<td class="input-lbl">Short Description<font color="#FF0000">*</font></td>
			<td><input type="text" name="shortDes" required value="<?php echo $bannerData["ShortDes"]?>"></td>
		</tr>
		<tr>
			<td class="input-lbl">Status<font color="#FF0000">*</font></td>
			<td><input type="radio" name="bannerStatus" value="1" required <?php if ($bannerData["status"] == "1"){?> checked <?php } ?>> Active 
				<input type="radio" name="bannerStatus" value="0" if <?php if ($bannerData["status"] == "0") { ?> checked <?php } ?>> Inactive 
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit">
				<input type="reset" name="reset">
			</td>
		</tr>
	</tbody>
	</table>
</form>
</body>
</html>