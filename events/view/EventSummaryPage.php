<?php include_once(LIBRARY_PATH . "events/view/eventheader.php");
?>
<body>
    <form method="post" name="eventDetail" id="eventForm" onsubmit="validateForm(this);" enctype="multipart/form-data">
        <table width="100%" class="MainTable eventinfo">
            <thead>
                <tr>
            <h3><th align="left" colspan="2">EVENT DETAILS</th></h3>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="input-lbl">Event Summary</td>
                    <td><textarea class="ckeditor" name="eventSummary"  id="eventSummary" rows="4" cols="70"><?php echo $getdataforEvent["eventSummary"]; ?></textarea></td>
                </tr>

               

                <tr> 
                    <td class="input-lbl">Gallery <font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="Gallery[]" id="gallery" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
                            <?php if (!is_array($eventFileData['Gallery']) && count($eventFileData['Gallery'] == 0)) {?>      
                                required
                            <?php } ?>
                               multiple >
                    
                        <div id="imagesExists">
                            
                            <?php
                            if (is_array($eventFileData['Gallery']) && count($eventFileData['Gallery'] > 0)){
                           foreach($eventFileData['Gallery'] as $value){
                            
                            ?>
                            
                            <div class="filediv"> 
                                <div style="float:left;">
                                     <img src="<?php echo $staticContentUrl.'event/assets/'.$eventId."/Gallery/".$value['name'];  ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                </div>
                               
                                <div  class="deleteFIle deletediv" file-id="<?php echo $value['id'];  ?>" file-path="<?php echo $staticContentUrl.'event/assets/'.$eventId."/Gallery/".$value['name'];  ?>">
                                   <img src="<?php echo C_ROOT_URL.'view/images/clse_btn.jpg'  ?>" title="Delete"> 
                                </div>
                            </div>
                            
                            <?php
                              
                           }
                           
                            }?>
                            
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="input-lbl">Is Event Summary Active <font color="#FF0000">*</font></td>
                    <td>
                        <input type="radio" name="isActiveEventSummary" value="0" required <?php if ($getdataforEvent["isActiveEventSummary"] == 0) { ?> checked <?php } ?>> No
                        <input type="radio" name="isActiveEventSummary" value="1" <?php if ($getdataforEvent["isActiveEventSummary"] == 1) { ?> checked <?php } ?>> Yes
                    </td>
                </tr>

                <input type="hidden" name="alldleleteFIleIds" id="alldeleteFIlesIds" value="">
                <input type="hidden" name="deleteFIlesPath" id="deleteFIlesPath" value="">
                <tr>
                    <td> </td>
                    <td> 
                        <input type="submit" name="submit" class="addmorebtn">
                        <input type="reset" id="reset" name="reset" class="addmorebtn" >
                    </td>
                </tr>
        </table>
    </form>
</body>
</html>