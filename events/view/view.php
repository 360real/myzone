<!DOCTYPE html>
<?php include_once(LIBRARY_PATH . "events/view/eventheader.php"); ?>
    <body>
        <!-- HEADING  -->
        <table width="100%" cellpadding="2" class="SearchTable">
            <thead>
            <h3><tr align="right"><th><a href="<?= $pageName ?>?action=Create" class="add">ADD EVENT</a></th></tr></h3>
        </thead>
    </table>   

    <?php
    $eventData = $getDataForview;
    ?>
     
    <table width="100%" class="MainTable" id="myTable">
       

        <thead>
            <tr>
                <th colspan="8" class="bgwth">
                    <div>
                        <input type="text" id="search-title" placeholder="Search Title">
                    </div>
                    
                    <div id="resetTable" class="refTable"  >Reset Table</div>
                </th>
             </tr>
            <tr>
                <th>Event Title</th>
                <th>Event Type</th>
                <th>Country</th>
                <th>City</th>
                <th>Start On</th>
                <th>End On</th>
                <th>Event Status</th>
                <th style="cursor: auto;">ACTION</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th data-showfilter="0"></th>
                <th data-showfilter="0"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="0"></th>
            </tr>
        </tfoot>

        <tbody>

            <?php
//echo '<pre>';
//print_r($eventData);

if (count($eventData)>0){
            foreach ($eventData as $value) {
                ?>
                <tr>


                    <td class="center" width="13%">

                        <?php
                        if ($value['EventTitle'] == "") {

                            $eventTitle = 'N/A';
                        } else {
                            
                            
                            $titlelen = strlen(trim($value['EventTitle']));
                            
                           
                            if ($titlelen < 30){
                                
                                $eventTitle =  $value['EventTitle'];
                            } else {
                                
                                 $eventTitle =  substr($value['EventTitle'],0,30)." ...";
                            }
                           

                           
                        }
                        echo $eventTitle;
                        ?>

                    </td>
                    <td class="center" width="13%"><?php 
                    $eventTypeArr = explode(",",$value['eventType']);
                    
                    
                    if($eventTypeArr[0] == ''){
                        
                        $eventType = 'N/A';
                        
                    } else {
                        
                    
                        $length = count($eventTypeArr);
                        if ($length == 1){
                            
                           $eventType =  $eventTypeArr[0]; 
                        } 
                        else if ($length == 2) {
                            
                             $eventType =  $eventTypeArr[0].", ". $eventTypeArr[1];
                        } else {
                            
                             $eventType =  $eventTypeArr[0].", ". $eventTypeArr[1]." ...";
                        }
                    }   
                     echo $eventType;
                    
                    
                    ?></td>
                    <td class="center" width="13%"><?php echo $value['country']; ?></td>
                    <td class="center" width="13%"><?php echo $value['city']; ?></td>
                    <td class="center" width="12%"><?php echo $value['eventdisplayStartDate']; ?></td>
                    <td class="center" width="12%"><?php echo $value['eventillDate']; ?></td>
                    <td class="center" width="12%"><?php echo $eventStatusArray[$value['status']]; ?></td>
            <a href="../../../360com/application/config/config.php"></a>
            <td width="12%">
                <a href="<?php echo $page_name . '?action=Update&eventId=' . $value['id'] ?>"  class="edit">Edit</a>
                <a href="<?php echo $page_name . '?action=summary&eventId=' . $value['id'] ?>"  style="color:#333;">Event Summary</a>
                <?php if ($_SESSION['login']['id'] == '1') { ?>
                    <a href="#" class="delete deleteEvent" eventId="<?PHP echo $value['id']; ?>">Delete</a>
                <?PHP } ?>

            </td>
        </tr>

    <?php } 
}
    ?>
</tbody>

</thead>
</table>
</body>
</html>