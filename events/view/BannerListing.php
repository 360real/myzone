<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo $site['URL']?>view/css/admin-style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/css.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/event.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $site['URL']?>view/js/eventPage.js?>"></script>
</head>
<body>

<?php $bannerData = $getDataForBanner; ?>

<script>

</script>

<table width="100%" class="MainTable eventinfo">
		<tr>
			<td colspan="3" class="bannerpg">BANNERS</td>
			<td width="210" class="bannerpg"><a href="<?= $pageName ?>?action=BannerManager" class="add">ADD BANNER</a></td>
		</tr>
		<tr>
			<td colspan="4" height="10"></td>
		</tr>
		<tr>
			<td class="bnrhd">SHORT TITLE</td>
			<td class="bnrhd">SHORT DESCRIPTION</td>
			<td class="bnrhd">STATUS</td>
			<td class="bnrhd">ACTION</td>
		</tr>

	<?php 
if(count($bannerData)>0){
foreach ($bannerData as $data) { ?>
		<tr>
			<td><?php echo $data['ShortTitle']; ?></td>
			<td><?php echo $data['ShortDes']; ?> </td>
			<td style="color: <?php echo $bannerStatusColourArray[$data['status']]; ?>; font-weight: 600"><?php echo $bannerStatusArray[$data['status']]; ?></td>
			<td>
				<a href="<?php echo $page_name . '?action=UpdateBanner&BannerId=' . $data['id'] ?>" target="_blank" class="edit">Edit</a>
				<a href="#" class="delete deleteBanner" BannerId="<?php echo $data['id']; ?>">DELETE</a>
			</td>
		</tr>
	<?php } }?>

</table>
</body>
</html>