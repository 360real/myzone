<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);


//include("/var/www/html/myzone/adminproapp/includes/set_main_conf.php");
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");

include_once LIBRARY_PATH . "library/server_config_admin.php";


include_once(LIBRARY_PATH . "events/commonvariable.php");
include_once(LIBRARY_PATH . "events/model/Model.php");
$modelObj = new Model();
admin_login();

/* Including common Header */
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('Manage Events', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/events/controller/controller.php";
$action = $_REQUEST['action'];




// echo "<pre>";
// print_r($_POST);

switch ($action) {
    case 'Create':
        if (is_array($_POST) && count($_POST) > 0) {
            $insertIntoDb = $modelObj->insertIntoDb();
            if ($insertIntoDb) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Inserted successfully!");
                    window.location.href = "/events/controller/controller.php";
                </script>
                <?php

            }
        }
        include(ROOT_PATH . "/events/view/EventDetailPage.php");
        break;

    case 'Update':

        if (isset($_GET['eventId']) && $_GET['eventId'] != "") {

            $eventId = $_GET['eventId'];

            $getdataforEventarr = $modelObj->getdataforevent($eventId);

            $getdataforEvent = $getdataforEventarr[0];

            $getdataforEventproperty = $modelObj->getdataforeventProperty($eventId);


            $eventPropertyData = $getdataforEventproperty;

            $getdataforEventRequirment = $modelObj->getdataforeventRequirment($eventId);

            $eventRequirmentData = $getdataforEventRequirment;

            $getdataforUploadingFile = $modelObj->getdataforuploadingFiles($eventId);

            $eventFileData = $getdataforUploadingFile;
        }

        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {

            $updateData = $modelObj->udateEventData();


            if ($updateData) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Updated successfully!");
                    window.location.href = "/events/controller/controller.php";
                </script>
                <?php

            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';
            }
        }
        include(ROOT_PATH . "/events/view/EventDetailPage.php");
        break;
    case 'BannerManager':
        if (is_array($_POST) && count($_POST) > 0) {
            $insertBannerDetail = $modelObj->insertBannerDb();

            if (!$insertBannerDetail) {
                echo "Banner Name Already Exists !";
            } else {
                ?>
                <script type="text/javascript">
                    window.location.href = "/events/controller/controller.php?action=BannerListing";
                    alert('Data Inserted Successfully !');

                </script>
            <?php

            }
        }

        include(ROOT_PATH . "/events/view/EventBannerView.php");
        break;

    case 'BannerListing':
        $getDataForBanner = $modelObj->getDataForBanner();
        include(ROOT_PATH . "/events/view/BannerListing.php");
        break;

    case 'UpdateBanner':
        if (isset($_GET['BannerId']) && $_GET['BannerId'] != "") {
            $EventBannerId = $_GET['BannerId'];
            $getDataForBanner = $modelObj->getDataForBanner($EventBannerId);
            $bannerData = $getDataForBanner[0];
        }


        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {

            $UpdateBannerOnEdit = $modelObj->updateBannerDetails($EventBannerId);
            if ($UpdateBannerOnEdit) {
                ?>
                <script type="text/javascript">
                    alert("Banner Details Updated Successfully !");
                    window.location.href = "/events/controller/controller.php?action=BannerListing";
                </script>
            <?php

            }
        }
        include(ROOT_PATH . "/events/view/EventBannerView.php");
        break;

    case 'deleteBanner':

        $bannerId = $_GET['BannerId'];
        $DeleteBanner = $modelObj->deleteBanner($bannerId);
        if ($DeleteBanner) {
            header('location: /events/controller/controller.php?action=BannerListing');
        } else {
            ?>
            <script type="text/javascript">
                alert("Problem While Deleting !");
            </script>
        <?php

        }


        break;

    case 'Delete':

        if (isset($_GET['eventId']) && $_GET['eventId'] != "") {
            $eventId = $_GET['eventId'];
            $changeStartus = $modelObj->changestatusofEvent($eventId);


            if ($changeStartus) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been deleted successfully!");
                    window.location.href = "/events/controller/controller.php";
                </script>
                <?php

            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';

                header('location: /events/controller/controller.php');
            }
        }
        break;

    case 'summary':

        if (isset($_GET['eventId']) && $_GET['eventId'] != "") {

            $eventId = $_GET['eventId'];

            $getdataforEventarr = $modelObj->getdataforevent($eventId);

            $getdataforEvent = $getdataforEventarr[0];

            $getdataforUploadingFile = $modelObj->getdataforuploadingFiles($eventId);

            $eventFileData = $getdataforUploadingFile;

        }

        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {


            $updateData = $modelObj->udateEventSummaryData();


            // if ($updateData) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Updated successfully!");
                    window.location.href = "/events/controller/controller.php";
                </script>
                <?php

            // } else {
            //     echo '<script>alert(" There are some Error !!")</script>';
            // }
        }

        include(ROOT_PATH . "/events/view/EventSummaryPage.php");
        break;

    default :

        $getDataForview = $modelObj->getdataforevent();

        include(ROOT_PATH . "events/view/view.php");
        break;
}
?>
