<?php

require_once(ROOT_PATH . "library/mysqli_function.php");
require_once(ROOT_PATH . "events/commonvariable.php");
require_once(ROOT_PATH . "events/eventmail.php");

// this model for making all the query for event
class Model {

    private $dbObj;

    function __construct() {

        $this->dbObj = new mysqli_function();
    }

    // for inserting Data for event
    public function insertIntoDb() {

        $countryId = mysql_real_escape_string($_POST['country_id']);
        $cityId = mysql_real_escape_string($_POST['cty_id']);
        $eventStart = mysql_real_escape_string($_POST['eventstart']);
        $eventEnd = mysql_real_escape_string($_POST['eventend']);
        $eventTitle = mysql_real_escape_string($_POST['eventTitle']);
        $campaigns = '';

        if (is_array($_POST['Campaigns']) && count($_POST['Campaigns']) > 0) {

            $campaigns = mysql_real_escape_string(implode(",", $_POST['Campaigns']));
        }
        $eventType = '';


        if (is_array($_POST['eventtypes']) && count($_POST['eventtypes']) > 0) {

            $eventType = mysql_real_escape_string(implode(",", $_POST['eventtypes']));
        }

        $blogs = '';

        if (is_array($_POST['blogs']) && count($_POST['blogs']) > 0) {

            $blogs = mysql_real_escape_string(implode(",", $_POST['blogs']));
        }



        $eventDescription = mysql_real_escape_string($_POST['eventDescription']);
        $isNri = mysql_real_escape_string($_POST['isNri']);
        $youtubeVideoLink = mysql_real_escape_string($_POST['videoLink']);
        $displayMobileNo = mysql_real_escape_string($_POST['displayMobileNo']);
        $EventTemplateID = mysql_real_escape_string($_POST['eventTemplate']);
        $facebookShareLink = mysql_real_escape_string($_POST['fbShareLink']);
        $twitterShareLink = mysql_real_escape_string($_POST['twitterShareLink']);
        $linkedInShareLink = mysql_real_escape_string($_POST['linkedinShareLink']);
        $PinterestShareLink = mysql_real_escape_string($_POST['pinterestShareLink']);
        $eventAddress = mysql_real_escape_string($_POST['eventAddress']);
        $eventLat = mysql_real_escape_string($_POST['eventLat']);
        $eventLon = mysql_real_escape_string($_POST['eventLon']);
        $eventMetaTitle = mysql_real_escape_string($_POST['eventMetaTitle']);
        $eventMetaDes = mysql_real_escape_string($_POST['eventMetaDescription']);
        $eventMetaKeyword = mysql_real_escape_string($_POST['eventMetaKeyword']);
        $status = mysql_real_escape_string($_POST['status']);
        $web_scope = mysql_real_escape_string($_POST['web_scope']);
        $edge360 = mysql_real_escape_string($_POST['360_edge']);
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);

        $sql = "INSERT INTO `tsr_events` (`countryId`,"
                . " `cityId`,"
                . " `eventType`, "
                . " `eventEmail`, "
                . " `eventCampaignId`, "
                . " `eventCountryCampaignId`, "
                . " `eventCityCampaignId`, "
                . " `customUrl`, "
                . "`eventStartDate`, `eventEndDate`,"
                . " `EventTitle`, `EventDescription`, "
                . "`isNri`, `youtubeVideoLink`, "
                . "`PPCnumber`, "
                . "`BlogInterlinkedID`,"
                . " `EventTemplateID`, "
                . "`facebookShareLink`,"
                . " `twitterShareLink`,"
                . " `linkedInShareLink`, "
                . "`PinterestShareLink`,"
                . " `EventAddress`,"
                . " `latitude`,"
                . " `longitude`, "
                . "`MetaTitle`, "
                . "`MetaDescription`, "
                . "`metaKeywords`,"
                . " `campaignsRequired`,"
                . " `CreatedBy`,"
                . " `status`,"
                . " `web_scope`,"
                . " `360_edge`,"
                . " archives) "
                . "VALUES ('" . $countryId . "', "
                . "'" . $cityId . "',"
                . " '" . $eventType . "', "
                . " '" . mysql_real_escape_string($_POST['eventEmail']) . "', "
                . " '" . mysql_real_escape_string($_POST['eventCampaignId']) . "' ,"
                . " '" . mysql_real_escape_string($_POST['eventCountryCampaignId']) . "', "
                . " '" . mysql_real_escape_string($_POST['eventCityCampaignId']) . "', "
                . " '" . mysql_real_escape_string($_POST['customUrl']) . "' "
                . ",'" . $eventStart . "',"
                . " '" . $eventEnd . "',"
                . "'" . $eventTitle . "', "
                . "'" . $eventDescription . "',"
                . " '" . $isNri . "',"
                . " '" . $youtubeVideoLink . "',"
                . " '" . $displayMobileNo . "', "
                . "'" . $blogs . "', "
                . "'" . $EventTemplateID . "' ,"
                . "'" . $facebookShareLink . "',"
                . " '" . $twitterShareLink . "', "
                . "'" . $linkedInShareLink . "', "
                . "'" . $PinterestShareLink . "', "
                . "'" . $eventAddress . "',"
                . " '" . $eventLat . "',"
                . " '" . $eventLon . "', "
                . "'" . $eventMetaTitle . "',"
                . " '" . $eventMetaDes . "', "
                . "'" . $eventMetaKeyword . "', "
                . "'" . $campaigns . "',"
                . " '" . $CreatedBy . "',"
                . " '" . $status . "',"
                . " '" . $web_scope . "',"
                . " '" . $edge360 . "',"
                . " 'live'  )";

        $query = $this->dbObj->insert_query($sql);

        $eventId = $query;

        $this->uploadfileforevent($eventId);
        $this->savefile($eventId);
        $this->saveDataforeventproperty($eventId);
        $this->saveDataforRequirment($eventId);

        if ($query) {
            // this code for sending mail to event status
            // $this->sendMailForStatusChange('create');
        }

        return $query;
    }

        // this method for upload all file in folder
    private function uploadfileforevent($eventId) {

        $fileBasePath ="/var/www/html/myzone/media_images/event/";
        $fileArray['Banner'] = $_FILES['bannerName'];
        $fileArray['Images'] = $_FILES['Images'];
        $fileArray['brochure'] = $_FILES['brochure'];
        $fileArray['leaflet'] = $_FILES['leaflet'];
        // $fileArray['Gallery'] = $_FILES['Gallery'];

        foreach ($fileArray as $key => $value) {

            foreach ($value as $key1 => $value1) {
                if ($value1[0] != '') {

                    foreach ($value1 as $key2 => $value2) {

                        $tmpname = $value['tmp_name'][$key2];

                        $fileName = $value['name'][$key2];

                        $pathEventId = $fileBasePath . 'assets/' . $eventId;

                        if (!is_dir($pathEventId)) {
                            mkdir($pathEventId);
                            chmod($pathEventId, 0777);
                        }

                        $pathFileType = $pathEventId . '/' . $key;
                        if (!is_dir($pathFileType)) {
                            mkdir($pathFileType);
                            chmod($pathFileType, 0777);
                        }
                        move_uploaded_file($tmpname, $pathFileType . "/" . $fileName);
                    }
                }
            }
        }
    }

    // this function for saving uploading file data into db
    private function savefile($eventId) {

        $FileArr['Banner'] = $_FILES['bannerName']['name'];
        $FileArr['Images'] = $_FILES['Images']['name'];
        $FileArr['Brochure'] = $_FILES['brochure']['name'];
        $FileArr['leaflet'] = $_FILES['leaflet']['name'];
        // $FileArr['Gallery'] = $_FILES['Gallery']['name'];

        foreach ($FileArr as $key => $value) {

            if (count($value) > 0 && $value[0] != "") {

                foreach ($value as $fileNames) {

                    $qry = 'INSERT INTO `tsr_events_assets` (`AssetType`, `EventId`, `FileName`,archives)'
                            . ' values( '
                            . '"' . mysql_real_escape_string($key) . '",'
                            . '"' . mysql_real_escape_string($eventId) . '",'
                            . '"' . mysql_real_escape_string($fileNames) . '",'
                            . '"' . mysql_real_escape_string('live') . '")';

                    $query = $this->dbObj->insert_query($qry);
                }
            }
        }


        if ($query) {

            return true;
        } else {

            return false;
        }
    }

    //this function for send mail when status has been changed
    private function sendMailForStatusChange($actionType, $oldStatus = '') {

        global $recordOnePage;
        global $eventmailSubjectArr;
        global $eventmailAddreplayto;
        global $eventmailToMail;
        global $eventmailcc;
        global $eventStatusArray;

        $dataArray['subject'] = $eventmailSubjectArr['changeStatus'];
        $dataArray['adreplyto'] = $eventmailAddreplayto['changeStatus'];
        $dataArray['address'] = $eventmailToMail['changeStatus'];
        $dataArray['cc'] = $eventmailcc['changeStatus'];

        $mailData['oldStatus'] = trim($eventStatusArray[$oldStatus]);
        $mailData['status'] = trim($eventStatusArray[$_POST['status']]);
        $mailData['title'] = trim(strip_tags($_POST['eventTitle']));
        $mailData['mailType'] = 'update';

        ob_start();

        include(ROOT_PATH . "events/view/mailview.php");

        $dataArray['msg'] = ob_get_contents();

        ob_get_clean();

        sendMailChangeStatus('update', $dataArray);



        global $recordOnePage;
        global $eventmailSubjectArr;
        global $eventmailAddreplayto;
        global $eventmailToMail;
        global $eventmailcc;
        global $eventStatusArray;

        $dataArray['subject'] = $eventmailSubjectArr[$actionType];
        $dataArray['adreplyto'] = $eventmailAddreplayto[$actionType];
        $dataArray['address'] = $eventmailToMail[$actionType];
        $dataArray['cc'] = $eventmailcc[$actionType];
        $mailData['status'] = $eventStatusArray[$_POST['status']];
        $mailData['title'] = trim(strip_tags($_POST['eventTitle']));

        if ($actionType == 'changeStatus') {

            $mailData['oldStatus'] = trim($eventStatusArray[$oldStatus]);
        }
        $mailData['mailType'] = $actionType;

        ob_start();

        include(ROOT_PATH . "events/view/mailview.php");

        $dataArray['msg'] = ob_get_contents();
        ob_get_clean();

        sendMailChangeStatus($actionType, $dataArray);
    }

    // this function for for map property and event Id
    private function saveDataforeventproperty($eventId) {


        $property = $_POST['eventProperty'];

        $cities = $_POST['eventCity'];

        $country = $_POST['eventPropertyCountry'];

        $usp = $_POST['propertyUsp'];

        $description = $_POST['propertyDescription'];

        for ($i = 0; $i < count($property); $i++) {

            $qry = 'insert into  tsr_event_property (event_id,'
                    . 'property_id,'
                    . 'city_id,'
                    . 'country_id,'
                    . 'property_usp,'
                    . 'property_description,'
                    . 'created_date,'
                    . 'archives)'
                    . ' values("' . mysql_real_escape_string($eventId) . '",'
                    . '"' . mysql_real_escape_string($property[$i]) . '",'
                    . '"' . mysql_real_escape_string($cities[$i]) . '",'
                    . '"' . mysql_real_escape_string($country[$i]) . '",'
                    . '"' . mysql_real_escape_string($usp[$i]) . '",'
                    . '"' . mysql_real_escape_string($description[$i]) . '",'
                    . '"' . mysql_real_escape_string(date('Y-m-d H:i:s')) . '",'
                    . '"' . mysql_real_escape_string('live') . '")';

            $query = $this->dbObj->insert_query($qry);
        }


        if ($query) {

            return true;
        } else {

            return false;
        }
    }

    //this method for save multiple requirement
    private function saveDataforRequirment($eventId) {


        $requirments = $_POST['eventRequirment'];


        $quantity = $_POST['reqQuantity'];

        for ($i = 0; $i < count($requirments); $i++) {

            $qry = 'INSERT INTO `tsr_event_requirements` ( '
                    . '`EventId`,'
                    . ' `RequirementId`,'
                    . ' `Quantity`,'
                    . 'archives) '
                    . 'VALUES ( "' . mysql_real_escape_string($eventId) . '",'
                    . ' "' . mysql_real_escape_string($requirments[$i]) . '",'
                    . ' "' . mysql_real_escape_string($quantity[$i]) . '",'
                    . ' "live")';


            $query = $this->dbObj->insert_query($qry);
        }

        if ($query) {

            return true;
        } else {

            return false;
        }
    }

    // this function for update data into db
    public function udateEventData() {

        if (isset($_GET['eventId']) && $_GET['eventId'] != "" && $_GET['eventId'] != 0 && is_numeric($_GET['eventId'])) {
            $eventId = $_GET['eventId'];

            //for geting old status of the event

            $qry = 'select status from tsr_events where id = "' . mysql_real_escape_string($eventId) . '"';
            $rec = $this->dbObj->getAllData($qry);
            $oldStatus = $rec[0]['status'];

            $campaignRequired = "";

            if (is_array($_POST['Campaigns']) && count($_POST['Campaigns']) > 0) {

                $campaignRequired = mysql_real_escape_string(implode(",", $_POST['Campaigns']));
            }

            $eventType = '';

            if (is_array($_POST['eventtypes']) && count($_POST['eventtypes']) > 0) {

                $eventType = mysql_real_escape_string(implode(",", $_POST['eventtypes']));
            }

            $blogs = '';

            if (is_array($_POST['blogs']) && count($_POST['blogs']) > 0) {

                $blogs = mysql_real_escape_string(implode(",", $_POST['blogs']));
            }


            $qry = "UPDATE tsr_events SET "
                    . "countryId = '" . mysql_real_escape_string($_POST['country_id']) . "', "
                    . "cityId = '" . mysql_real_escape_string($_POST['cty_id']) . "',"
                    . "EventTitle = '" . mysql_real_escape_string($_POST['eventTitle']) . "',"
                    . "eventType = '" . $eventType . "',"
                    . "eventStartDate = '" . mysql_real_escape_string($_POST['eventstart']) . "', "
                    . "eventEndDate = '" . mysql_real_escape_string($_POST['eventend']) . "',"
                    . " EventDescription = '" . mysql_real_escape_string($_POST['eventDescription']) . "',"
                    . "youtubeVideoLink = '" . mysql_real_escape_string($_POST['videoLink']) . "',"
                    . " PPCnumber = '" . mysql_real_escape_string($_POST['displayMobileNo']) . "', "
                    . "BlogInterlinkedID = '" . $blogs . "', "
                    . "EventTemplateID = '" . mysql_real_escape_string($_POST['eventTemplate']) . "', "
                    . "facebookShareLink = '" . mysql_real_escape_string($_POST['fbShareLink']) . "', "
                    . "eventEmail = '" . mysql_real_escape_string($_POST['eventEmail']) . "', "
                    . "eventCampaignId = '" . mysql_real_escape_string($_POST['eventCampaignId']) . "', "
                    . "eventCountryCampaignId = '" . mysql_real_escape_string($_POST['eventCountryCampaignId']) . "', "
                    . "eventCityCampaignId = '" . mysql_real_escape_string($_POST['eventCityCampaignId']) . "', "
                    . "customUrl = '" . mysql_real_escape_string($_POST['customUrl']) . "', "
                    . "twitterShareLink = '" . mysql_real_escape_string($_POST['twitterShareLink']) . "',"
                    . "linkedInShareLink = '" . mysql_real_escape_string($_POST['linkedinShareLink']) . "', "
                    . "PinterestShareLink = '" . mysql_real_escape_string($_POST['pinterestShareLink']) . "', "
                    . "EventAddress = '" . mysql_real_escape_string($_POST['eventAddress']) . "', "
                    . "latitude = '" . mysql_real_escape_string($_POST['eventLat']) . "', "
                    . "longitude = '" . mysql_real_escape_string($_POST['eventLon']) . "',"
                    . "MetaTitle = '" . mysql_real_escape_string($_POST['eventMetaTitle']) . "',"
                    . "MetaDescription = '" . mysql_real_escape_string($_POST['eventMetaDescription']) . "',"
                    . "metaKeywords = '" . mysql_real_escape_string($_POST['eventMetaKeyword']) . "',"
                    . "campaignsRequired = '" . mysql_real_escape_string($campaignRequired) . "',"
                    . "ModifiedDate = '" . mysql_real_escape_string(date('Y-m-d H:i:s')) . "',"
                    . "CreatedBy = '" . mysql_real_escape_string($_SESSION['login']["id"]) . "',"
                    . "web_scope = '" . mysql_real_escape_string($_POST['web_scope']) . "',"
                    . "360_edge = '" . mysql_real_escape_string($_POST['360_edge']) . "',"
                    . "status = '" . mysql_real_escape_string($_POST['status']) . "' "
                    . "WHERE tsr_events.id = " . mysql_real_escape_string($eventId) . "";

            $updateData = $this->dbObj->update_query($qry);

            $this->updateEventProperty($eventId);
            $this->updateEventRequirement($eventId);
            $this->updatefileforEvent($eventId);

            if ($updateData) {

                if ($oldStatus != $_POST['status']) {

                    $this->sendMailForStatusChange('changeStatus', $oldStatus);
                }
                return true;
            } else {


                return false;
            }
        }
    }

    // this function for updetaing files
    private function updatefileforEvent($eventId) {

        $tobedelteIds = $_POST['alldleleteFIleIds'];

        $tounlinkFiles = $_POST['deleteFIlesPath'];

        $deleteFIleArr = explode("#", $tobedelteIds);

        $unlinkFIlearr = explode("#", $tounlinkFiles);
        if ($_POST['alldleleteFIleIds'] != "") {

            foreach ($deleteFIleArr as $value) {

                $qry = 'update tsr_events_assets set archives = "history" where id = ' . mysql_real_escape_string($value);

                $this->dbObj->exec_query($qry);
            }

         
        }
        $this->uploadfileforevent($eventId);
        $this->savefile($eventId);
    }

    //this function for updateing event property
    private function updateEventProperty($eventId) {

        $sql = 'update  tsr_event_property set archives= "history" where event_id = "' . mysql_real_escape_string($eventId) . '"';

        $this->dbObj->exec_query($sql);

        $this->saveDataforeventproperty($eventId);
    }

    //this function for updateing event requirement
    private function updateEventRequirement($eventId) {

        $sql = 'update  tsr_event_requirements set archives= "history" where EventId = "' . mysql_real_escape_string($eventId) . '"';

        $this->dbObj->exec_query($sql);

        $this->saveDataforRequirment($eventId);
    }

    // this function for change status for event 
    public function changestatusofEvent($eventId) {

        $qry = "Update tsr_events set status = 4 where id = '" . $eventId . "'";

        $updateStatus = $this->dbObj->update_query($qry);



        return $updateStatus;
    }

    // this function for fetch drop down list
    public function dropdownRequirement() {

        $qry = "SELECT id,requirementName FROM tsr_requirement_event_master";


        $query = $this->dbObj->getAllData($qry);


        return $query;
    }

    // this function for fetch list of blog
    public function dropdownBlog() {
        //$result = array();
        $sql = "SELECT id, title FROM tsr_blog";
        $sql1 = mysql_query($sql);
        if (mysql_num_rows($sql1)) {
            while ($result = mysql_fetch_array($sql1)) {
                $result2[] = $result;
            }
            return $result2;
        }
    }

    // this function for fetch country
    public function fetchCity($countryId) {

        $sql = 'SELECT id,city FROM tsr_cities where country_id = ' . mysql_real_escape_string($countryId);

        $rec = $this->dbObj->getAllData($sql);

        return $rec;
    }

    // this function for fetch property
    public function fetchProperty($cityId) {

        $sql = 'SELECT id,property_name FROM tsr_properties where cty_id = ' . mysql_real_escape_string($cityId) .' and status = "1"';

        $rec = $this->dbObj->getAllData($sql);

        return $rec;
    }

    // this function for fetch all data for event
    public function getdataforevent($eventId = "") {

       

        $sql = 'SELECT evnt.*, conty.country,cty.city,evnt.id,evnt.eventType, evnt.countryId,evnt.cityId,evnt.EventTitle, DATE_FORMAT(evnt.eventStartDate,"%d/%m/%Y %H:%i") as eventdisplayStartDate, DATE_FORMAT(evnt.eventEndDate,"%d/%m/%Y %H:%i") as eventillDate,evnt.status, evnt.CreatedBy,DATE_FORMAT(evnt.CreatedDate,"%d/%m/%Y %H:%i") as createdDate , evnt.EventDescription FROM tsr_events evnt
            inner join tsr_countries conty on evnt.countryId = conty.id
            inner join tsr_cities cty on evnt.cityId = cty.id  ';

        if ($eventId != "") {

            $sql .= ' where evnt.id = "' . mysql_real_escape_string($eventId) . '"';
        }
        $sql .= 'order by evnt.id desc';

        $rec = $this->dbObj->getAllData($sql);



        return $rec;
    }

    // this function for get property data of any event
    public function getdataforeventProperty($eventId) {

        $sql = 'select tep.id,cty.city,tp.property_name,tep.property_id,tep.city_id,tep.country_id,tep.property_usp,tep.property_description from tsr_event_property tep '
                . ' inner join tsr_properties tp on tep.property_id = tp.id '
                . ' inner join tsr_cities cty on tep.city_id = cty.id '
                . 'where event_id = "' . mysql_real_escape_string($eventId) . '" and archives = "live" order by id';

        $rec = $this->dbObj->getAllData($sql);


        return $rec;
    }

    // this function for get data for requirement
    public function getdataforeventRequirment($eventId) {

        $sql = 'select ter.id,ter.RequirementId,term.RequirementName,ter.Quantity from tsr_event_requirements ter '
                . 'inner join tsr_requirement_event_master term on ter.RequirementId = term.id'
                . ' where '
                . ' ter.EventId = "' . mysql_real_escape_string($eventId) . '" and archives = "live" order by id';

        $rec = $this->dbObj->getAllData($sql);


        return $rec;
    }

    //this function for geting data of uploading files
    public function getdataforuploadingFiles($eventId) {

        $qry = 'select id,AssetType,FIleName from tsr_events_assets where EventId = ' . mysql_real_escape_string($eventId) . ' and archives = "live" ';

        $rec = $this->dbObj->getAllData($qry);

        foreach ($rec as $key => $value) {

            $filearr['name'] = $value['FIleName'];
            $filearr['id'] = $value['id'];

            $filedisplayArray[$value['AssetType']][] = $filearr;
        }

        return $filedisplayArray;
    }

    public function insertBannerDb() {
        $MainPageBannersSavePath = '/var/www/html/myzone/media_images/event/MainPageBanners/';
        $tempName = $_FILES['bannerMainPage']['tmp_name'];
        $MainPageBannerName = $_FILES['bannerMainPage']['name'];
        $FinalPath = $MainPageBannersSavePath . '/' . $MainPageBannerName;
        if (file_exists($FinalPath)) {
            return false;
        } elseif (!file_exists($FinalPath)) {
            move_uploaded_file($tempName, $MainPageBannersSavePath . '/' . $MainPageBannerName);
            $ShortTitle = mysql_real_escape_string($_POST['shortTitle']);
            $ShortDes = mysql_real_escape_string($_POST['shortDes']);
            $status = mysql_real_escape_string($_POST['bannerStatus']);
            $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
            $FileName = $_FILES['bannerMainPage']['name'];
            
            if ($_POST['CountryId'] == 0){
                
                $countryId = NULL;
            } else {
                
                 $countryId =$_POST['CountryId'];
            }
            
          
            
          if ($_POST['bannerCity'] == 0 || $_POST['bannerCity'] == ""){
              
              $cityId = null;
          } else {
              
            $cityId = $_POST['bannerCity']; 
          }

            $sql = "INSERT INTO tsr_events_MainBanners ("
                    . "BannerName,"
                     . "country_id,"
                     . "city_id,"
                    . " ShortTitle,"
                    . " ShortDes,"
                    . " status,"
                    . " CreatedBy) "
 
                    . "VALUES ('" . $FileName . "' "
                      . ",'" . $countryId . "' "
                      . ",'" . $cityId . "' "
                    . ",'" . $ShortTitle . "', "
                    . "'" . $ShortDes . "',"
                    . " '" . $status . "', "
                    . "'" . $CreatedBy . "')";
            
         
            $query = $this->dbObj->insert_query($sql);
           
            return true;
        }
    }

    public function getDataForBanner($bannnerId = '') {
        $sql = 'SELECT id, ShortTitle, ShortDes, status FROM tsr_events_MainBanners WHERE status !="3" ';
        if ($bannnerId != "") {

            $sql .= ' AND id ="' . $bannnerId . '"';
        } else {

            $sql .= ' order by id DESC ';
        }

        $data = $this->dbObj->getAllData($sql);
        return $data;
    }

    public function updateBannerDetails($eventBannerId) {
        $sql = "UPDATE tsr_events_MainBanners 
                SET ShortTitle='" . mysql_real_escape_string($_POST['shortTitle']) . "', 
                    ShortDes='" . mysql_real_escape_string($_POST['shortDes']) . "', 
                    status='" . mysql_real_escape_string($_POST['bannerStatus']) . "' 
                WHERE tsr_events_MainBanners.id = '" . mysql_real_escape_string($eventBannerId) . "' ";
        $update = $this->dbObj->update_query($sql);
        return $update;
    }

    public function deleteBanner($bannerId) {
        $sql = "UPDATE tsr_events_MainBanners SET status='3' WHERE id='" . mysql_real_escape_string($bannerId) . "'";
        $delete = $this->dbObj->update_query($sql);
        if ($delete) {
            return true;
        } else {

            return false;
        }
    }

    
    public function selectAllCountry(){
        
         $sql = 'SELECT id, country  FROM tsr_countries  ';
        

            $sql .= ' order by id asc ';
      

        $data = $this->dbObj->getAllData($sql);
        return $data;
    }

    // event summary function start

    public function udateEventSummaryData()
    {
        if (isset($_GET['eventId']) && $_GET['eventId'] != "" && $_GET['eventId'] != 0 && is_numeric($_GET['eventId'])) {
            $eventId = $_GET['eventId'];

            //for geting old status of the event
            $eventSummary = mysql_real_escape_string($_POST['eventSummary']);

            $qry = "UPDATE tsr_events SET eventSummary = '" . $eventSummary . "', "
                    . "isActiveEventSummary = '" . mysql_real_escape_string($_POST['isActiveEventSummary']) . "' "
                    . "WHERE tsr_events.id = " . mysql_real_escape_string($eventId) . "";
           
            $updateData = $this->dbObj->update_query($qry);

            
            $this->updatefileforEventSummary($eventId);
            if ($updateData) {
                return true;
            } else {
               return false;
            }
        }
    }

    private function updatefileforEventSummary($eventId) {

        $tobedelteIds = $_POST['alldleleteFIleIds'];

        $tounlinkFiles = $_POST['deleteFIlesPath'];

        $deleteFIleArr = explode("#", $tobedelteIds);

        $unlinkFIlearr = explode("#", $tounlinkFiles);
        if ($_POST['alldleleteFIleIds'] != "") {

            foreach ($deleteFIleArr as $value) {

                $qry = 'update tsr_events_assets set archives = "history" where id = ' . mysql_real_escape_string($value);

                $this->dbObj->exec_query($qry);
            }

         
        }
        $this->uploadfileforeventsummary($eventId);
        $this->savefileEventSummary($eventId);
    }

    private function uploadfileforeventsummary($eventId) {

        $fileBasePath ="/var/www/html/myzone/media_images/event/";

        $fileArray['Gallery'] = $_FILES['Gallery'];

        foreach ($fileArray as $key => $value) {

            foreach ($value as $key1 => $value1) {
                if ($value1[0] != '') {

                    foreach ($value1 as $key2 => $value2) {

                        $tmpname = $value['tmp_name'][$key2];

                        $fileName = $value['name'][$key2];

                        $pathEventId = $fileBasePath . 'assets/' . $eventId;

                        if (!is_dir($pathEventId)) {
                            mkdir($pathEventId);
                            chmod($pathEventId, 0777);
                        }

                        $pathFileType = $pathEventId . '/' . $key;
                        if (!is_dir($pathFileType)) {
                            mkdir($pathFileType);
                            chmod($pathFileType, 0777);
                        }
                        move_uploaded_file($tmpname, $pathFileType . "/" . $fileName);
                    }
                }
            }
        }
    }

    // this function for saving uploading file data into db
    private function savefileEventSummary($eventId) {

        $FileArr['Gallery'] = $_FILES['Gallery']['name'];

        foreach ($FileArr as $key => $value) {

            if (count($value) > 0 && $value[0] != "") {

                foreach ($value as $fileNames) {

                    $qry = 'INSERT INTO `tsr_events_assets` (`AssetType`, `EventId`, `FileName`,archives)'
                            . ' values( '
                            . '"' . mysql_real_escape_string($key) . '",'
                            . '"' . mysql_real_escape_string($eventId) . '",'
                            . '"' . mysql_real_escape_string($fileNames) . '",'
                            . '"' . mysql_real_escape_string('live') . '")';

                    $query = $this->dbObj->insert_query($qry);
                }
            }
        }


        if ($query) {

            return true;
        } else {

            return false;
        }
    }

    // event summary function end

}

// end class 
?>
