
<?php
$viewType = $dataforAjax['viewType'];
?>
<?php
switch ($viewType) {


    case 'Requirments' :
        ?>
        <div>
            <div style="margin-top:5px;">
                <?php
                $RequirementList = $dataforAjax['reqSelectBoxData'];
                ?>       
                <span style="float:left;margin:3px;margin-left:0px;">    
                    <select  name="eventRequirment[]">
                        <option>--select Requirement--</option>
                        <?php foreach ($RequirementList as $value) { ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo $value['requirementName']; ?></option>
                        <?php } ?>
                    </select>
                </span>

                <input  type="text" style="float:left;" name="reqQuantity[]" placeholder="Quantity">
                <div class="RemoveRequirments addmorebtn">Remove</div>
            </div><br/>
        </div>
        <?php
        break;
    case 'fetchCIty' :
        ?> 
        <select style="width:234px;" class="showPropertyList" name="eventCity[]" required="true;">
            <option value="">-- Select City --</option>
            <?php
            $cityData = $dataforAjax['cityData'];

            foreach ($cityData as $value) {

                echo '<<option value="' . $value['id'] . '">' . $value['city'] . '</option>';
            }
            ?>
        </select>
        <?php
        break;
    case 'fetchCItyForNri' :
        ?> 
        <select style="width:234px;"  name="NriCity" id="NriCity">
            <option value="">-- Select City --</option>
            <?php
            $cityData = $dataforAjax['cityData'];

            foreach ($cityData as $value) {

                echo '<<option value="' . $value['id'] . '">' . $value['city'] . '</option>';
            }
            ?>
        </select>
        <?php break; ?>



    <?php case 'fetchproperty': ?>
        <select style="width:234px;" name="eventProperty[]" required="true;" class="fetchpropertyHighlight" highlight-id="<?php echo $dataforAjax['fieldId']; ?>">
            <option value="">-- Select Property --</option>
            <?php
            $propertyData = $dataforAjax['propertyData'];

            foreach ($propertyData as $value) {

                echo '<<option value="' . $value['id'] . '">' . $value['property_name'] . '</option>';
            }
            ?>
        </select>

        <?php
        break;


    case 'AddMoreProperty':
        ?>
        <div class="addmr">
            <div class="pro-pd">

                <?php echo AjaxDropDownListforproperty(TABLE_COUNTRY, "eventPropertyCountry[]", "id", "country", '', "id", $site['CMSURL'] . "cluster/populate_locations_advance.php", "eventpropertyCitydiv", "locationLabelDiv", 'Select Country') ?>

            </div>
            <div class="pro-pd eventCitydiv" name="eventCitydiv" >
                <select style="width:234px;"  name="eventCity[]" >
                    <option value="">-- Select City --</option>
                </select>
            </div>
            <div class="pro-pd eventPropertyDiv" name="eventPropertyDiv">
                <select style="width:234px;" name="eventProperty[]" class="fetchpropertyHighlight">
                    <option value="">-- Select Property --</option>
                </select>
            </div>
            <div class="pro-pd">
                <input type="text" size="100%" name="propertyUsp[]"  placeholder="Property USP">
            </div>
            <div class="pro-pd">
                <input type="text" name="PropertyvideoLink[]" rows="4" cols="70" value="<?php echo $getdataforNri["youtubeVideoLink"]; ?>" placeholder="YouTube Link" required>
            </div>
            <div class="pro-pd">
                <input type="checkbox" name="propertyTypeHoliday[]" value="1" class="setPropertyTypeHoliday"> is Holiday/Secound Home 
                <input type="hidden" name="ispropertyHolidayHome[]" value="0" class="holidayHometxt">
                <input type="checkbox" name="propertyTypeRoi[]" value="1" class="setPropertyTypeRoi"> is ROI/High Return Properties
                <input type="hidden" name="ispropertyRoiHome[]" value="0" class="roiHometxt">
            </div>
            <div class="pro-pd">
                <?php
                $blogs = $modelObj->dropdownBlog();
                ?>
                <select name="Propertyblogs[]" multiple size="15" class="blogselectbox" style="width: 300px; height: 120px;">
                    <option value="">----Select Blog----</option>
                    <?php foreach ($blogs as $blg) { ?>
                        <option value="<?php echo $blg['id']; ?>">
                            <?php echo $blg['title']; ?>
                        </option>
                    <?php } ?>
                </select>
                <input type='hidden' name='propertyselectBlog[]' value=''>
            </div>
            <div class="pro-pd propertyBannerDiv">
                <input type="file" name="PropertyImageName_[]" id="banner" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 

                       


                       multiple size-allowed="200" /><br/>
                Property Banner (Size Less then 200kb)

            </div>
      <div class="pro-pd propertyImageDiv">
                <input type="file" name="PropertybannerName[]" id="Proimage" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 

                       
                       

                        size-allowed="200" /><br/>
                Property Banner (Size Less then 200kb)

            </div>



            <script type="text/javascript" src="<?php echo C_ROOT_URL ?>view/ckeditor/ckeditor.js"></script>
            <div class="input-lbl textalignleft">Property Description : </div>
            <div class="pro-pd textrea">
                <textarea class="ckeditor propertyDescription" id='propertyDescription_<?php echo $dataforAjax['ckeditorId']; ?>' name="propertyDescription[]"  rows="4" cols="70" placeholder="Property Description" ></textarea>

            </div>

            <div class="pro-pd textrea highlight" >
                <div class="input-lbl textalignleft">Property Highlight : </div>
                <div class="highlighdiv">
                    <textarea class="ckeditor" id="highlight_<?php echo $dataforAjax['ckeditorId']; ?>"   name="property_highlight[]"   rows="4" cols="70"  placeholder="Property Description" title="Property Highlight" ></textarea>
                </div>

            </div>
            <div class="pro-pd textrea" >
                <div class="input-lbl textalignleft">Meta Title : </div>
                <textarea  name="PropertyMetaTitle[]"  id="MetaDescription" rows="4" cols="70"></textarea>
            </div>
            <div class="pro-pd textrea" >
                <div class="input-lbl textalignleft">Meta Keywords : </div>
                <textarea  name="PropertyMetaKeyword[]"  id="MetaDescription" rows="4" cols="70"></textarea>
            </div>
            <div class="pro-pd textrea" >
                <div class="input-lbl textalignleft">Meta Description : </div>
                <textarea  name="PropertyMetaDescription[]"  id="MetaDescription" rows="4" cols="70"></textarea>
                <div class="addmorebtn pro-pdbt RemoveProperty">Remove</div>
            </div>
        </div>

        <?php
        break;

    case 'Eventcantentforselect':
        ?>

        <select  class="blogselectbox" name="EventInterlinkedID[]" multiple="true" style="width: 300px; height: 120px;">
            <option value="">-- Select Event --</option>
            <?php
            $eventData = $dataforAjax['eventData'];
            if (count($eventData) > 0) {
                foreach ($eventData as $value) {

                    echo '<<option value="' . $value['id'] . '">' . $value['EventTitle'] . '</option>';
                }
            }
            ?>
        </select>
    <?php
    default :
        return '<select ><option></option></select>';
        break;
}
?>

