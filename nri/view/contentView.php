<!DOCTYPE html>
<html>
    <head>
        <link href="<?php echo $site['URL'] ?>view/css/admin-style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $site['URL'] ?>view/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $site['URL'] ?>view/css/css.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $site['URL'] ?>view/css/event.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $site['URL'] ?>view/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo $site['URL'] ?>view/js/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo $site['URL'] ?>view/js/NriPage.js?>"></script>

    </head>
    <body>
        <?php
        if ($_SERVER['HTTP_HOST'] == '360cms.com') {

            $staticContentUrl = 'http://360cms.com';
        } else {

            $staticContentUrl = 'https://static.360realtors.com';
        }
        ?>
        <form method="post" id="contentforhome" enctype="multipart/form-data" >
            <table width="100%" class="MainTable eventinfo" >
                <thead>
                    <tr>
                <h3><th align="left" colspan="2">NRI Home Page Content</th></h3>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="input-lbl "  style="vertical-align: top;">Content For Home Page<font color="#FF0000">*</font></td>
                        <td>
                            <textarea name="homecontent" id="homecontent"  class="ckeditor"   required  rows="18" cols="60" placeholder="Write Content"><?php echo $getContent['content']; ?></textarea>
                        </td>
                    </tr>   
                    <tr>
                        <td class="input-lbl "  style="vertical-align: top;">Meta Title For Home Page<font color="#FF0000">*</font></td>
                        <td>
                            <textarea name="metaTItle"   rows="18" cols="60" placeholder="Write Content"><?php echo $getContent['metaTItle']; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="input-lbl "  style="vertical-align: top;">Meta Keywords For Home Page<font color="#FF0000">*</font></td>
                        <td>
                            <textarea name="metaKeyword"   rows="18" cols="60" placeholder="Write Content"><?php echo $getContent['metaKeyword']; ?></textarea>
                        </td>
                    </tr>
                        <tr>
                        <td class="input-lbl "  style="vertical-align: top;">Meta Description For Home Page<font color="#FF0000">*</font></td>
                        <td>
                            <textarea name="metaDescription"   rows="18" cols="60" placeholder="Write Content"><?php echo $getContent['metaDescription']; ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" name="submit" class="addmorebtn">

                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </body>
</html>