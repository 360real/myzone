<!DOCTYPE html>
<?php include_once(LIBRARY_PATH . "nri/view/nriheader.php"); ?>
    <body>
        <!-- HEADING  -->
        <table width="100%" cellpadding="2" class="SearchTable">
            <thead>
            <h3><tr align="right"><th><a href="<?= $pageName ?>?action=Create" class="add">Add NRI Page</a></th></tr></h3>
        </thead>
    </table>   

    <?php
    $NriData = $getDataForview;
    ?>
     
    <table width="100%" class="MainTable" id="myTable">
       

        <thead>
            <tr>
                <th colspan="8" class="bgwth">
                    <div>
                        <input type="text" id="search-title" placeholder="Search Title">
                    </div>
                    
                    <div id="resetTable" class="refTable"  >Reset Table</div>
                </th>
             </tr>
            <tr>
                <th>NRI Title</th>
                <th>Country</th>
                <th>City</th>
                <th>NRI Page Status</th>
                <th style="cursor: auto;">ACTION</th>

            </tr>
        </thead>
        <tfoot>
            <tr>
                <th data-showfilter="0"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="1"></th>
                <th data-showfilter="0"></th>
               
            </tr>
        </tfoot>

        <tbody>

            <?php

if (count($NriData)>0){
            foreach ($NriData as $value) {
                ?>
                <tr>


                    <td class="center" width="13%">

                        <?php
                        if ($value['nriTitle'] == "") {

                            $NriTitle = 'N/A';
                        } else {
                            
                            
                            $titlelen = strlen(trim($value['nriTitle']));
                            
                           
                            if ($titlelen < 30){
                                
                                $NriTitle =  $value['nriTitle'];
                                
                            } else {
                                
                                 $NriTitle =  substr($value['nriTitle'],0,30)." ...";
                            }
                           

                           
                        }
                        echo $NriTitle;
                        ?>

                    </td>
                   
                    <td class="center" width="13%"><?php echo $value['country']; ?></td>
                    <td class="center" width="13%">
                        
                        <?php
                        
                        if ($value['city'] == ""){
                            
                            echo 'N/A';
                            
                        } else {
                        
                        echo $value['city']; 
                        
                        }
                        
                      $statusarray = array('','brown','yellow','green','red')  
                        ?></td>
                   
                    <td class="center" width="12%" style="color:<?php echo $statusarray[$value['status']]; ?>; font-weight:bold;">
                        
                       <?php echo $eventStatusArray[$value['status']]; ?>
                    
                    </td>
            <a href="../../../360com/application/config/config.php"></a>
            <td width="12%">
                <a href="<?php echo $page_name . '?action=Update&NriId=' . $value['id'] ?>"  class="edit">Edit</a>
                <?php if (in_array($_SESSION['login']['id'], array('1','2','3','4'))) { ?>
                    <a href="#" class="delete deletenri" NriId="<?PHP echo $value['id']; ?>">Delete</a>
                <?PHP } ?>

            </td>
        </tr>

    <?php } 
}
    ?>
</tbody>

</thead>
</table>
</body>
</html>