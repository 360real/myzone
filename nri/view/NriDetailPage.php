<?php
include_once(LIBRARY_PATH . "nri/view/nriheader.php");

if ($_SERVER['HTTP_HOST'] == '360cms.com') {

    $staticContentUrl = 'http://360cms.com';
} else {

    $staticContentUrl = 'https://static.360realtors.com';
}
?>
<body>
    <form method="post" name="eventDetail" id="eventForm" onsubmit="validateForm(this);" enctype="multipart/form-data">
        <table width="100%" class="MainTable eventinfo">
            <thead>
                <tr>
            <h3><th align="left" colspan="2">NRI DETAILS</th></h3>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="input-lbl">Country<font color="#FF0000">*</font></td>
                    <td><?php $countryList = $modelObj->selectAllCountry(); ?>
                        <select name="CountryId" id="nriCountry" style="width:234px;" required="true"
                                 <?php if ($action == 'Update') { ?>
                                disabled
                                 <?php } ?>
                                
                                >
                            <option value="0">select Country </option>

                            <?php foreach ($countryList as $value) { ?>

                                <option value="<?php echo $value['id'] ?>"

                                        <?php if ($getdataforNri['countryId'] == $value['id']) { ?>        
                                            selected
                                        <?php } ?>
                                        ><?php echo $value['country']; ?> </option>
                                    <?php } ?>

                        </select>

<?php if ($action == 'Update') { ?>
                        <input type="hidden" name="CountryId" value="<?php echo $getdataforNri['countryId']; ?>"
                                 <?php } ?>

                    </td>
                </tr>
            <tr>
                    <td class="input-lbl" ><div id="locationLabelDiv" name="locationLabelDiv">City</div></td>
                    <td>
                        <?php
                        if ($action == 'Update') {

                            $cityData = $modelObj->fetchCity($getdataforNri['countryId']);
                        }
                        ?>

                        <div  id="nriCityDiv">
                            <?php if ($action == 'Update') { ?>
                                <select style="width:234px;" name="NriCity" id="NriCity"
                                         <?php if ($action == 'Update') { ?>
                                disabled
                                 <?php } ?>
                                        
                                        
                                        
                                        >
                                    <option value="">-- Select City--</option>
                                    <?php
                                    if (count($cityData) > 0) {

                                        foreach ($cityData as $value) {
                                            ?>
                                            <option value="<?php echo $value['id']; ?>"
                                            <?php if ($getdataforNri['CItyId'] == $value['id']) { ?>  
                                                        selected
                                                    <?php } ?>
                                                    ><?php echo $value['city']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>

                                </select>
<?php if ($action == 'Update') { ?> 
 <input type="hidden" name="NriCity" value="<?php echo $getdataforNri['CItyId']; ?>"
        
<?php } ?>
                            <?php } else { ?>
                                <select style="width:234px;" name="NriCity" id="NriCity">

                                    <option value="">-- Select City--</option>


                                </select>
                            <?php } ?>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td class="input-lbl" >NRI Template<font color="#FF0000">*</font></td>
                    <td><input type="radio" name="nriTemplate" value="1" required <?php if ($getdataforNri["nriTemplateID"] == 1) { ?> checked <?php } ?>>Country Template
                        <input type="radio" name="nriTemplate" value="2" <?php if ($getdataforNri["nriTemplateID"] == 2) { ?> checked <?php } ?>>State/City Template </td>
                </tr>
                <tr>
                    <td class="input-lbl">NRI Banner <font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="bannerName[]" id="banner" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 
                        <?php if (!is_array($nriFiledata['Banner']) && count($nriFiledata['Banner'] == 0)) { ?>  
                                   required  
                               <?php } ?> 
                               multiple size-allowed="200" /><br/>
                        (Size Less then 200kb)
                        <div id="bannerExists">
                            <?php
                            $BannerStr = '';
                            if (is_array($nriFiledata['Banner']) && count($nriFiledata['Banner'] > 0)) {
                                $bannerStr = '';
                                foreach ($nriFiledata['Banner'] as $key => $value) {
                                    ?>

                                    <div class="filediv"> 
                                        <div style="float:left;">
                                            <img src="<?php echo $staticContentUrl . '/nri/assets/' . $value['folderId'] . "/Banner/" . $value['name']; ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                        </div>

                                        <div class="deleteFIle deletediv" file-id="<?php echo $value['id']; ?>" file-path="<?php echo $staticContentUrl . '/nri/assets/' . $value['folderId'] . "/Banner/" . $value['name']; ?>">
                                            <img src="<?php echo C_ROOT_URL . 'view/images/clse_btn.jpg' ?>" title="Delete"> 
                                        </div>
                                    </div>

                                    <?php
                                }
                            }
                            ?>

                        </div>

                    </td>
                </tr>
                <tr> 
                    <td class="input-lbl">Images <font color="#FF0000">*</font></td>
                    <td >
                        <input type="file" name="Images[]" id="images" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif" 
                        <?php if (!is_array($nriFiledata['Images']) && count($nriFiledata['Images'] == 0)) { ?>      
                                  
                               <?php } ?>
                               multiple size-allowed="100"><br/>
                        (Size Less then 100kb)

                        <div id="imagesExists">

                            <?php
                            if (is_array($nriFiledata['Images']) && count($nriFiledata['Images'] > 0)) {
                                foreach ($nriFiledata['Images'] as $value) {
                                    ?>

                                    <div class="filediv"> 
                                        <div style="float:left;">
                                            <img src="<?php echo $staticContentUrl . '/nri/assets/' . $value['folderId'] . "/Images/" . $value['name']; ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                        </div>

                                        <div  class="deleteFIle deletediv" file-id="<?php echo $value['id']; ?>" file-path="<?php echo $staticContentUrl . '/nri/assets/' . $value['folderId'] . "/Images/" . $value['name']; ?>">
                                            <img src="<?php echo C_ROOT_URL . 'view/images/clse_btn.jpg' ?>" title="Delete"> 
                                        </div>
                                    </div>

                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </td>
                </tr> 

                <tr>
                    <td class="input-lbl">NRI Title<font color="#FF0000">*</font></td>
                    <td>
                        <textarea  name="nriTitle"  id="nriTitle" rows="4" cols="40" maxlength="50" required="true"><?php echo $getdataforNri["nriTitle"]; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td class="input-lbl">NRI Description</td>
                    <td><textarea class="ckeditor" name="nriDescription"  id="nriDescription" rows="4" cols="70"><?php echo $getdataforNri["nriDescription"]; ?></textarea></td>
                </tr>
                <!-- <tr>
                        <td>Display Email Address</td> 
                        <td><input type="text" name="displayEmail" size="50%"></td>
                </tr> -->
                <tr>
                    <td class="input-lbl">Display Mobile No.</td>
                    <td><input type="text"  name="displayMobileNo" id="displayMobileNo" maxlength="25" value="<?php echo $getdataforNri["PPCnumber"]; ?>" onkeypress="return isNumber(event)"/></td>
                </tr>


                <tr>
                    <td class="input-lbl">NRI Meta Title</td>
                    <td><textarea  name="MetaTitle"  id="MetaTitle" rows="4" cols="70"><?php echo $getdataforNri["MetaTitle"]; ?></textarea></td>
                </tr>
                <tr>
                    <td class="input-lbl">NRI Meta Description</td>
                    <td><textarea  name="MetaDescription"  id="MetaDescription" rows="4" cols="70"><?php echo $getdataforNri["MetaDescription"]; ?></textarea></td>
                </tr>
                <tr>
                    <td class="input-lbl">NRI Meta Keywords</td>
                    <td><textarea  name="MetaKeyword"  id="MetaKeyword" rows="4" cols="70"><?php echo $getdataforNri["metaKeywords"]; ?></textarea></td>
                </tr>

                <tr>
                    <td class="input-lbl">Youtube Video Link</td>
                    <td><input type="text" name="videoLink" rows="4" cols="70" value="<?php echo $getdataforNri["youtubeVideoLink"]; ?>" placeholder="YouTube Embed Source code " required></td>
                </tr>

                <tr style="display: none">
                    <td class="input-lbl">Facebook Share Link</td>
                    <td><input type="text" name="fbShareLink" size="100%" value="<?php echo $getdataforNri["facebookShareLink"]; ?>"></td>
                </tr>
                <tr style="display: none">
                    <td class="input-lbl">Twitter Share Link</td>
                    <td><input type="text" name="twitterShareLink" size="100%" value="<?php echo $getdataforNri["twitterShareLink"]; ?>"></td>
                </tr>
                <tr style="display: none">
                    <td class="input-lbl">LinkedIn Share Link</td>
                    <td><input type="text" name="linkedinShareLink" size="100%" value="<?php echo $getdataforNri["linkedInShareLink"]; ?>"></td>
                </tr>
                <tr style="display: none">
                    <td class="input-lbl">Pinterest Share Link</td>
                    <td><input type="text" name="pinterestShareLink" size="100%" value="<?php echo $getdataforNri["PinterestShareLink"]; ?>"></td>
                </tr>


                <tr>
                    <td class="input-lbl" style="vertical-align: top;">Property<font color="#FF0000">*</font></td>
                    <td align="left">
                        <div>

                            <div class="pro-pd">

                                <?php
//echo '<pre>';
//print_r($NriPropertyData);

                                echo AjaxDropDownListforproperty(TABLE_COUNTRY, "eventPropertyCountry[]", "id", "country", $NriPropertyData[0]["country_id"], "id", $site['CMSURL'] . "cluster/populate_locations_advance.php", "eventpropertyCitydiv", "locationLabelDiv", 'Select Country', 'forproperty')
                                ?>

                            </div>
                            <div class="pro-pd eventCitydiv" name="eventCitydiv" >
                                <select style="width:234px;"   name="eventCity[]" class="showPropertyList">
                                    <option value="">-- Select City --</option>
                                    <?php
                                    if ($_GET["action"] == "Update") {

                                        $cityList = $modelObj->fetchCity($NriPropertyData[0]["country_id"]);
                                        foreach ($cityList as $value) {
                                            ?>

                                            <option value="<?php echo $value['id']; ?>"
                                            <?php if ($NriPropertyData[0]['city_id'] == $value['id']) { ?>
                                                        selected
                                                    <?php } ?>    >
                                                        <?php echo $value['city']; ?>
                                            </option>

                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <option>-- Select City --</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="pro-pd eventPropertyDiv" name="eventPropertyDiv">
                                <select style="width:234px;"name="eventProperty[]"  class="fetchpropertyHighlight" highlight-id="">
                                    <option value="">-- Select Property --</option>
                                    <?php
                                    if ($_GET["action"] == "Update") {

                                        $allPropertyList = $modelObj->fetchProperty($NriPropertyData[0]['city_id']);
                                        foreach ($allPropertyList as $value) {
                                            ?>

                                            <option value="<?php echo $value['id']; ?>"

                                                    <?php if ($NriPropertyData[0]['property_id'] == $value['id']) { ?>
                                                        selected
                                                    <?php } ?>
                                                    >
                                                        <?php echo $value['property_name']; ?>

                                            </option>

                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <option>-- Select Property --</option>
                                    <?php } ?>

                                </select>
                            </div>
                            <div class="pro-pd">
                                <input type="text" size="100%" name="propertyUsp[]"  placeholder="Property USP" value="<?php echo $NriPropertyData[0]["property_usp"]; ?>">
                            </div>
                            <div class="pro-pd">
                                <input type="text" name="PropertyvideoLink[]" rows="4" cols="70" value="<?php echo $NriPropertyData[0]["youtubeUrl"]; ?>" placeholder="YouTube Link" required>
                            </div>
                            <div class="pro-pd">
                                <input type="checkbox" name="propertyTypeHoliday[]" value="1" class="setPropertyTypeHoliday"
                                <?php
                                if ($NriPropertyData[0]["isholidayorSecoundHome"] == 1) {
                                    ?>
                                           checked
                                       <?php } ?>

                                       > is Holiday/Secound Home 
                                <input type="hidden" name="ispropertyHolidayHome[]" value="<?php echo $NriPropertyData[0]["isholidayorSecoundHome"]; ?>" class="holidayHometxt">
                                <input type="checkbox" name="propertyTypeRoi[]" value="1" class="setPropertyTypeRoi"
                                <?php
                                if ($NriPropertyData[0]["isRoiproperty"] == 1) {
                                    ?>
                                           checked
                                       <?php } ?>

                                       > is ROI/High Return Properties
                                <input type="hidden" name="ispropertyRoiHome[]" value="<?php echo $NriPropertyData[0]["isRoiproperty"]; ?>" class="roiHometxt">
                            </div>
                            <div class="pro-pd">
                                <?php
                                $selectBlogArray = explode(",", $NriPropertyData[0]["selectedBlogIds"]);

                                $blogs = $modelObj->dropdownBlog();
                                ?>
                                <select name="Propertyblogs[]" multiple size="15" class="blogselectbox" style="width: 300px; height: 120px;">
                                    <option value="">----Select Blog----</option>
                                    <?php foreach ($blogs as $blg) { ?>
                                        <option value="<?php echo $blg['id']; ?>"

                                                <?php if (in_array($blg['id'], $selectBlogArray)) { ?> selected <?php } ?>

                                                >
                                                    <?php echo $blg['title']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <input type='hidden' name='propertyselectBlog[]' value='<?php echo $NriPropertyData[0]["selectedBlogIds"] ?>'>
                            </div>
                            <div class="pro-pd propertyBannerDiv">
                                <input type="file" name="PropertybannerName_<?php echo $NriPropertyData[0]["property_id"]; ?>[]" id="banner" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 
                                <?php if (!is_array($nriFiledata['Banner']) && count($nriFiledata['Banner'] == 0)) { ?>  
                                           
                                       <?php } ?> 
                                       multiple size-allowed="200" /><br/>
                                Property Banner (Size Less then 200kb)
                                <div id="bannerExistsproperty"
                                    <?php
                                   if ($action == "Create"){ ?>
                                       >
                                   <?php } 
                                    
                                    
                                    $BannerStr = '';
                                    if (is_array($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['Banner']) && count($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['Banner'] > 0)) {
                                     
                                           $bannerStr = '';?>
                                            style="min-height:100px;"
                                            >
                                            <?php
                                        foreach ($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['Banner'] as $key => $value) {
                                            ?>

                                            <div class="filediv"> 
                                                <div style="float:left;">
                                                    <img src="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                                </div>

                                                <div class="deletepropertyFIle deletediv" file-id="<?php echo $value['id']; ?>" file-path="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>">
                                                    <img src="<?php echo C_ROOT_URL . 'view/images/clse_btn.jpg' ?>" title="Delete"> 
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                                <input type="hidden" name="deletePropertyImage[]" class="deletepropertytxt">
                            </div>
 

                            <div class='clearfix'></div>
  <div class="pro-pd propertyImageDiv">
                                <input type="file" name="PropertyImageName_<?php echo $NriPropertyData[0]["property_id"]; ?>[]" id="Proimage" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 
                                <?php if (is_array($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['image']) && count($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['image']) > 0) { ?>  
                                           disabled="true"
                                       <?php } ?> 
                                     size-allowed="200" /><br/>
                                Property Image (Size Less then 200kb)
                                <div id="imageExistsproperty"
                                    <?php
                                   if ($action == "Create"){ ?>
                                       >
                                   <?php } 


                                    $BannerStr = '';
                                    if (is_array($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['image']) && count($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['image'] > 0)) {
                                     
                                           $bannerStr = '';?>
                                            style="min-height:100px;"
                                            >
                                            <?php
                                        foreach ($fetchDataforPropertyimage[$NriPropertyData[0]["property_id"]]['image'] as $key => $value) {
                                            ?>

                                            <div class="filediv"> 
                                                <div style="float:left;">
                                                    <img src="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                                </div>

                                                <div class="deletepropertyFIle deletediv" isSingle="yes" file-id="<?php echo $value['id']; ?>" file-path="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>">
                                                    <img src="<?php echo C_ROOT_URL . 'view/images/clse_btn.jpg' ?>" title="Delete"> 
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                                <input type="hidden" name="deletePropertyImage[]" class="deletepropertytxt">
                            </div>
 

                            <div class='clearfix'></div>

                            <div class="pro-pd textrea">
                                <div class="input-lbl textalignleft">Property Description : </div>
                                <div>
                                    <textarea class="ckeditor" id="<?php echo $propertyDecriptionId ?>"   name="propertyDescription[]"   rows="4" cols="70"  placeholder="Property Description" title="Property Description">

                                        <?php echo $NriPropertyData[0]['property_description']; ?>
                                    
                                    </textarea>
                                </div>

                            </div>


                            <div class="pro-pd textrea highlight" >
                                <div class="input-lbl textalignleft">Property Highlight : </div>
                                <div class="highlighdiv">
                                    <textarea class="ckeditor" id="highlight_1"   name="property_highlight[]"   rows="4" cols="70"  placeholder="Property Description" title="Property Highlight" required>

                                        <?php echo $NriPropertyData[0]['property_highlight']; ?>
                                    
                                    </textarea>
                                </div>
                            </div>
                            <div class="pro-pd textrea" >
                                <div class="input-lbl textalignleft">Meta Title : </div>
                                <textarea  name="PropertyMetaTitle[]"  id="MetaDescription" rows="4" cols="70"><?php echo $NriPropertyData[0]['PropertyMetaTitle']; ?></textarea>
                            </div>
                            <div class="pro-pd textrea" >
                                <div class="input-lbl textalignleft">Meta Keywords : </div>
                                <textarea  name="PropertyMetaKeyword[]"  id="MetaDescription" rows="4" cols="70"><?php echo $NriPropertyData[0]['PropertyMetaKeyword']; ?></textarea>
                            </div>
                            <div class="pro-pd textrea" >
                                <div class="input-lbl textalignleft">Meta Description : </div>
                                <textarea  name="PropertyMetaDescription[]"  id="MetaDescription" rows="4" cols="70"><?php echo $NriPropertyData[0]['PropertyMetaDescription']; ?></textarea>
                            </div>




                        </div>

                        <?php
                        if (count($NriPropertyData) > 1) {

                            for ($i = 1; $i < count($NriPropertyData); $i++) {

                                $propertyDecriptionId = 'propertyDecriptionId_' . $i;
                                ?>

                                <div class="addmr">
                                    <div class="pro-pd">

                                        <?php echo AjaxDropDownListforproperty(TABLE_COUNTRY, "eventPropertyCountry[]", "id", "country", $NriPropertyData[$i]['country_id'], "id", $site['CMSURL'] . "cluster/populate_locations_advance.php", "eventpropertyCitydiv", "locationLabelDiv", 'Select Country') ?>

                                    </div>
                                    <div class="pro-pd eventCitydiv" name="eventCitydiv" >
                                        <select style="width:234px;"  name="eventCity[]" class="showPropertyList">
                                            <option value="">-- Select City --</option>
                                            <?php
                                            $cityList = $modelObj->fetchCity($NriPropertyData[$i]["country_id"]);

                                            foreach ($cityList as $value) {
                                                ?>

                                                <option value="<?php echo $value['id']; ?>"
                                                <?php if ($NriPropertyData[$i]['city_id'] == $value['id']) { ?>
                                                            selected
                                                        <?php } ?>    >
                                                            <?php echo $value['city']; ?>
                                                </option>

                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="pro-pd eventPropertyDiv" name="eventPropertyDiv">
                                        <select style="width:234px;"name="eventProperty[]" class="fetchpropertyHighlight" highlight-id="<?php echo $propertyDecriptionId ?>">
                                            <option value="">-- Select Property --</option>
                                            <?php
                                            $allPropertyList = $modelObj->fetchProperty($NriPropertyData[$i]['city_id']);



                                            foreach ($allPropertyList as $value) {
                                                ?>

                                                <option value="<?php echo $value['id']; ?>"

                                                        <?php if ($NriPropertyData[$i]['property_id'] == $value['id']) { ?>
                                                            selected
                                                        <?php } ?>
                                                        >
                                                            <?php echo $value['property_name']; ?>

                                                </option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="pro-pd">
                                        <input type="text" size="100%" name="propertyUsp[]"  placeholder="Property USB" value="<?php echo $NriPropertyData[$i]['property_usp']; ?>">
                                    </div>
                                    <div class="pro-pd">
                                        <input type="text" name="PropertyvideoLink[]" rows="4" cols="70" value="<?php echo $NriPropertyData[$i]["youtubeUrl"]; ?>" placeholder="YouTube Link" required>
                                    </div>
                                    <div class="pro-pd">
                                        <input type="checkbox" name="propertyTypeHoliday[]" value="1" class="setPropertyTypeHoliday"
                                        <?php
                                        if ($NriPropertyData[$i]["isholidayorSecoundHome"] == 1) {
                                            ?>
                                                   checked
                                               <?php } ?>

                                               > is Holiday/Secound Home 
                                        <input type="hidden" name="ispropertyHolidayHome[]" value="<?php echo $NriPropertyData[$i]["isholidayorSecoundHome"]; ?>" class="holidayHometxt">
                                        <input type="checkbox" name="propertyTypeRoi[]" value="1" class="setPropertyTypeRoi"
                                        <?php
                                        if ($NriPropertyData[$i]["isRoiproperty"] == 1) {
                                            ?>
                                                   checked
                                               <?php } ?>

                                               > is ROI/High Return Properties
                                        <input type="hidden" name="ispropertyRoiHome[]" value="<?php echo $NriPropertyData[$i]["isRoiproperty"]; ?>" class="roiHometxt">
                                    </div>
                                    <div class="pro-pd">
                                        <?php
                                        $selectBlogArray = explode(",", $NriPropertyData[$i]["selectedBlogIds"]);

                                        $blogs = $modelObj->dropdownBlog();
                                        ?>
                                        <select name="Propertyblogs[]" multiple size="15" class="blogselectbox" style="width: 300px; height: 120px;">
                                            <option value="">----Select Blog----</option>
                                            <?php foreach ($blogs as $blg) { ?>
                                                <option value="<?php echo $blg['id']; ?>"

                                                        <?php if (in_array($blg['id'], $selectBlogArray)) { ?> selected <?php } ?>

                                                        >
                                                            <?php echo $blg['title']; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <input type='hidden' name='propertyselectBlog[]' value='<?php echo $NriPropertyData[$i]["selectedBlogIds"]; ?>'>
                                    </div>
                                    <div class="pro-pd propertyBannerDiv" >
                                        <input type="file" name="PropertybannerName_<?php echo $NriPropertyData[$i]["property_id"]; ?>[]" id="banner" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 
                                        <?php if (!is_array($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['Banner']) && count($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['Banner'] == 0)) { ?>  
                                                  
                                               <?php } ?> 
                                               multiple size-allowed="200" /><br/>
                                        Property Banner (Size Less then 200kb)
                                        <div id="bannerExistsproperty"
                                            <?php 
                                            
                                             if (count($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['Banner']) == 0) {
                                            ?> >
                                            <?php
                                             } else {
                                            $BannerStr = '';
                                                ?>
                                            style="min-height:100px;"
                                            >
                                            <?php
                                                foreach ($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['Banner'] as $key => $value) {
                                                    ?>

                                                    <div class="filediv"> 
                                                        <div style="float:left;">
                                                            <img src="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                                        </div>

                                                        <div class="deletepropertyFIle deletediv" file-id="<?php echo $value['id']; ?>" file-path="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>">
                                                            <img src="<?php echo C_ROOT_URL . 'view/images/clse_btn.jpg' ?>" title="Delete"> 
                                                        </div>
                                                    </div>
                                                   
                                            <div class='clearfix'></div>


                                                    <?php
                                                }
                                            }
                                            ?>
                                             
                                        </div>
                                        <input type="hidden" name="deletePropertyImage[]" class="deletepropertytxt">
                                    </div>
                                      <div class='clearfix'></div>
  <div class="pro-pd propertyImageDiv">
                                <input type="file" name="PropertyImageName_<?php echo $NriPropertyData[$i]["property_id"]; ?>[]" id="Proimage" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpeg/jpg/gif" 
                                <?php if (is_array($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['image']) && count($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['image']) > 0) { ?>  
                                           disabled="true"
                                       <?php } ?> 
                                    size-allowed="200" /><br/>
                                Property Image (Size Less then 200kb)
                                <div id="imageExistsproperty"
                                    <?php
                                   if ($action == "Create"){ ?>
                                       >
                                   <?php } 
                                    $BannerStr = '';
                                    if (is_array($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['image']) && count($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['image'] > 0)) {

                                           $bannerStr = '';?>
                                            style="min-height:100px;"
                                            >
                                            <?php
                                        foreach ($fetchDataforPropertyimage[$NriPropertyData[$i]["property_id"]]['image'] as $key => $value) {
                                            ?>

                                            <div class="filediv"> 
                                                <div style="float:left;">
                                                    <img src="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>" width="75px;" height="75px;" title="<?php echo $value['name']; ?>">
                                                </div>

                                                <div class="deletepropertyFIle deletediv" isSingle="yes" file-id="<?php echo $value['id']; ?>" file-path="<?php echo $staticContentUrl . '/nri/' . $value['FileUrl']; ?>">
                                                    <img src="<?php echo C_ROOT_URL . 'view/images/clse_btn.jpg' ?>" title="Delete"> 
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                                <input type="hidden" name="deletePropertyImage[]" class="deletepropertytxt">
                            </div>
                                    <script type="text/javascript" src="<?php echo C_ROOT_URL ?>view/ckeditor/ckeditor.js"></script>
                                    <div class="pro-pd textrea">
                                        <textarea class="ckeditor propertyDescription" id='<?php echo $propertyDecriptionId; ?>' name="propertyDescription[]"  rows="4" cols="70" placeholder="Property Description" title="Property Description" >
                                                                                               
                                            <?php echo $NriPropertyData[$i]['property_description']; ?>
                                                                                                                                   
                                        </textarea>


                                    </div>
                                    <div class="pro-pd textrea highlight">
                                        <div class="input-lbl textalignleft">Property Highlight : </div>
                                        <div class="highlighdiv">
                                            <textarea  class="ckeditor hlight" id="highlight_<?php echo $i + 1; ?>"   name="property_highlight[]"   rows="4" cols="70"  placeholder="Property Description" title="Property Description" required>

                                                <?php echo $NriPropertyData[$i]['property_highlight']; ?>
                                                                            
                                            </textarea>

                                        </div>


                                    </div>
                                    <div class="pro-pd textrea" >
                                        <div class="input-lbl textalignleft">Meta Title : </div>
                                        <textarea  name="PropertyMetaTitle[]"  id="MetaDescription" rows="4" cols="70"><?php echo $NriPropertyData[$i]['PropertyMetaTitle']; ?></textarea>
                                    </div>
                                    <div class="pro-pd textrea" >
                                        <div class="input-lbl textalignleft">Meta Keywords : </div>
                                        <textarea  name="PropertyMetaKeyword[]"  id="MetaDescription" rows="4" cols="70"><?php echo $NriPropertyData[$i]['PropertyMetaKeyword']; ?></textarea>
                                    </div>
                                    <div class="pro-pd textrea" >
                                        <div class="input-lbl textalignleft">Meta Description : </div>
                                        <textarea  name="PropertyMetaDescription[]"  id="MetaDescription" rows="4" cols="70"><?php echo $NriPropertyData[$i]['PropertyMetaDescription']; ?></textarea>
                                        <div class="addmorebtn pro-pdbt RemoveProperty">Remove</div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                        }
                        ?>
                        <div id="addmorepropertyMainDIv"></div>

                        <div id="addmoreproperty" class="addmorebtn float-right" style="float:right;">Add More</div>  


                    </td>

                </tr> 

                <tr>
                    <td class="input-lbl">Blogs to Show</td>
                    <td>
                        <?php
                        $selectBlogArray = explode(",", $getdataforNri["BlogInterlinkedID"]);

                        $blogs = $modelObj->dropdownBlog();
                        ?>
                        <select name="blogs[]" multiple size="15" class="blogselectbox" style="width: 300px; height: 120px;">
                            <option value="">----Select Blog----</option>
                            <?php foreach ($blogs as $blg) { ?>
                                <option value="<?php echo $blg['id']; ?>"

                                        <?php if (in_array($blg['id'], $selectBlogArray)) { ?> selected <?php } ?>

                                        >
                                            <?php echo $blg['title']; ?>
                                </option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="input-lbl">Event to Show</td>
                    <td>
                        <?php
                        $selecteventArray = explode(",", $getdataforNri["EventInterlinkedID"]);

                        $events = $allEventDataforEdit;



                        if ($_GET["action"] == "Update") {
                            ?>
                            <div id="eventShowForNRI">
                                <select name="EventInterlinkedID[]" multiple size="15" class="blogselectbox" style="width: 300px; height: 120px;">

                                    <option value="">----Select Event----</option>
                                    <?php
                                    $enetselectBox = '';
                                    if (count($events) > 0) {
                                        foreach ($events as $value) {
                                            $enetselectBox .= '<option value="' . $value['id'] . '"';
                                            if (in_array($value['id'], $selecteventArray)) {

                                                $enetselectBox .= 'selected';
                                            }
                                            $enetselectBox .= '>' . $value['EventTitle'] . '</option>';
                                        }
                                        echo $enetselectBox;
                                    }
                                    ?>

                                </select>

                            </div>

                        <?php } else { ?>

                            <div id="eventShowForNRI">
                                <select name="EventInterlinkedID[]" multiple size="15" class="blogselectbox" style="width: 300px; height: 120px;">

                                    <option value="">----Select Event----</option>

                                </select>

                            </div>
                        <?php } ?>
                    </td>
                </tr>

                <tr>
                    <td class="input-lbl">Status<font color="#FF0000">*


                        </font></td>


                    <td>
                        <input type="radio" name="status" value="1" required <?php if ($getdataforNri["status"] == "1") { ?> checked <?php } ?>>Under Review
                        <?php if (in_array($_SESSION['login']['role_id'], array('1', '4', '3'))) { /* Only admin can access below options */ ?>
                            <input type="radio" name="status" value="2"  <?php if ($getdataforNri["status"] == "2") { ?> checked <?php } ?>>Approved
                            <input type="radio" name="status" value="3"  <?php if ($getdataforNri["status"] == "3") { ?> checked <?php } ?>>LIVE
                            <input type="radio" name="status" value="4"  <?php if ($getdataforNri["status"] == "4") { ?> checked <?php } ?>>Inactive
                        <?php } ?>
                    </td>
                </tr>
            <input type="hidden" name="alldleleteFIleIds" id="alldeleteFIlesIds" value="">
            <input type="hidden" name="deleteFIlesPath" id="deleteFIlesPath" value="">
            <tr>
                <td> </td>
                <td> 
                    <input type="submit" name="submit" class="addmorebtn">
                    <input type="reset" id="reset" name="reset" class="addmorebtn" >
                </td>
            </tr>
        </table>
        <input type="text" id="pageForJs" name="pageForJs" value="<?php echo $pathForAjax; ?>">
    </form>
</body>
</html>