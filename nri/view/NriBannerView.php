<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo $site['URL']?>view/css/admin-style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/css.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['URL']?>view/css/event.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $site['URL'] ?>view/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo $site['URL'] ?>view/js/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo $site['URL']?>view/js/NriPage.js?>"></script>

</head>
<body>
    <?php
if ($_SERVER['HTTP_HOST'] == '360cms.com'){
    
    $staticContentUrl = 'http://360cms.com';
            
} else {
    
    $staticContentUrl =  'https://static.360realtors.com';
}
?>
<form method="post" id="eventBanner" enctype="multipart/form-data" >
	<table width="100%" class="MainTable eventinfo" >
	<thead>
		<tr>
			
                    <th colspan="2"><a href="<?= $pageName ?>?action=BannerListing" class="add viewdtl">NRI HOME </a></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="input-lbl">Banner<font color="#FF0000">*</font></td>
			<td>
                            
                            <input type="file" name="bannerMainPage" accept="image/x-png,image/gif,image/jpeg,image/jpg" allowed="png/jpg/jpeg/gif"
                                   <?php if ($_GET['action'] == 'UpdateBanner'){ ?>
                                   
                                   disabled="true";
                                   <?php } ?>
                                 size-allowed="100"  ><font color="#FF0000">[* Image size Strictly preferred 1366x500 pixels]</font>
                         <?php if ($_GET['action'] == 'UpdateBanner'){ ?>
                            <br/><br/><div >
                                
                                <img src="<?php echo $staticContentUrl.'/nri/MainPageBanners/'.$bannerData["BannerName"] ?>" width="75px" height="75px" >
                                
                                
                            </div>
                            
                         <?php }  ?>
                        </td>
		</tr>
		<tr>
			<td class="input-lbl">Short Title<font color="#FF0000">*</font></td>
			<td><input type="text" name="shortTitle" required value="<?php echo $bannerData["ShortTitle"]; ?>"></td>
		</tr>
		<tr>
			<td class="input-lbl">Short Description<font color="#FF0000">*</font></td>
			<td><input type="text" name="shortDes" required value="<?php echo $bannerData["ShortDes"]?>"></td>
		</tr>
		<tr>
			<td class="input-lbl">Status<font color="#FF0000">*</font></td>
			<td><input type="radio" name="bannerStatus" value="1" required <?php if ($bannerData["status"] == "1"){?> checked <?php } ?>> Active 
				<input type="radio" name="bannerStatus" value="0" if <?php if ($bannerData["status"] == "0") { ?> checked <?php } ?>> Inactive 
			</td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit">
                            <?php if ($_GET['action'] != 'UpdateBanner'){ ?>
				<input type="reset" name="reset">
                            <?php } ?>
			</td>
		</tr>
	</tbody>
	</table>
</form>
</body>
</html>