<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once(LIBRARY_PATH . "library/server_config_admin.php");
include_once(LIBRARY_PATH . "library/main_common_function.php");
include_once(LIBRARY_PATH . "nri/commonvariable.php");
include_once(LIBRARY_PATH . "nri/model/Model.php");
$modelObj = new Model();
admin_login();
/* Including common Header */
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('Manage NRI', $site['TITLE'], $site['URL']);
$page_name = C_ROOT_URL . "/nri/controller/controller.php";
$pathForAjax = C_ROOT_URL . "/nri/model/ModelForAjax.php";
$action = $_REQUEST['action'];

$commonFunction = new main_common();
$checkPostdata = false;

/* //if (isset($_POST) && count($_POST) > 0) {
  //
  //    $_POST = $commonFunction->cleanPostData($_POST);
  //
  //    $commonFunction->pre($_POST);

  //    exit;

  //    if ($checkPostdata ==  true) {
  //
  //        $currentUrl = $_SERVER['REQUEST_URI'];

  //        ?>

  <!--        <script type="text/javascript">

  alert("Data is Trying To manuplate !!");
  window.location.href = "//<?php echo $currentUrl;  ?>";
  return false;

  </script>-->
  //<?php

  //    }

  //}
 */


switch ($action) {
    case 'Create':
        if (is_array($_POST) && count($_POST) > 0) {
            $insertIntoDb = $modelObj->insertIntoDb();
            if ($insertIntoDb) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Inserted successfully!");
                    window.location.href = "/nri/controller/controller.php";
                </script>
                <?php
            }
        }
        include(ROOT_PATH . "/nri/view/NriDetailPage.php");
        break;

    case 'Update':

        if (isset($_GET['NriId']) && $_GET['NriId'] != "") {


            $NriId = $_GET['NriId'];

            $getdataforNriarr = $modelObj->getdataforNRI($NriId);

            $getdataforNri = $getdataforNriarr[0];



            $getdataforNriproperty = $modelObj->getdataforNriProperty($NriId);

            $NriPropertyData = $getdataforNriproperty;
            
           
            
            foreach($NriPropertyData as $value){
                
                $propertyIds[] = $value['property_id'];
                
                
            }
            
            //print_r($propertyIds);
            
            $fetchDataforPropertyimage = $modelObj->getdataforNriPropertyImages($propertyIds);
            
//            echo '<pre>';
//            
//            print_r($fetchDataforPropertyimage);

            $getdataforUploadingFile = $modelObj->getdataforuploadingFiles($NriId);



            $nriFiledata = $getdataforUploadingFile;

            $allEventDataforEdit = $modelObj->getEventData($getdataforNri['countryId'], $getdataforNri['CItyId']);
        }

        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {

            $updateData = $modelObj->udateNRIData();


            if ($updateData) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Updated successfully!");
                  window.location.href = "/nri/controller/controller.php";
                </script>
                <?php
            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';
            }
        }
        include(ROOT_PATH . "/nri/view/NriDetailPage.php");
        break;
    case 'BannerManager':
        if (is_array($_POST) && count($_POST) > 0) {
            $insertBannerDetail = $modelObj->insertBannerDb();

            if (!$insertBannerDetail) {
                ?>
                <script type="text/javascript">


                    window.location.href = "<?php echo $currentUrl; ?>";

                </script>
            <?php } else {
                ?>
                <script type="text/javascript">
                    window.location.href = "/nri/controller/controller.php?action=BannerListing";
                    alert('Data Inserted Successfully !');

                </script>
                <?php
            }
        }

        include(ROOT_PATH . "/nri/view/NriBannerView.php");
        break;

    case 'BannerListing':
        $getDataForBanner = $modelObj->getDataForBanner();
        include(ROOT_PATH . "/nri/view/BannerListing.php");
        break;

    case 'UpdateBanner':
        if (isset($_GET['BannerId']) && $_GET['BannerId'] != "") {
            $EventBannerId = $_GET['BannerId'];
            $getDataForBanner = $modelObj->getDataForBanner($EventBannerId);
            $bannerData = $getDataForBanner[0];
        }


        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {

            $UpdateBannerOnEdit = $modelObj->updateBannerDetails($EventBannerId);
            if ($UpdateBannerOnEdit) {
                ?>
                <script type="text/javascript">
                    alert("Banner Details Updated Successfully !");
                    window.location.href = "/nri/controller/controller.php?action=BannerListing";
                </script>
                <?php
            }
        }
        include(ROOT_PATH . "/nri/view/NriBannerView.php");
        break;

    case 'deleteBanner':

        $bannerId = $_GET['BannerId'];
        $DeleteBanner = $modelObj->deleteBanner($bannerId);
        if ($DeleteBanner) {
            header('location: /nri/controller/controller.php?action=BannerListing');
        } else {
            ?>
            <script type="text/javascript">
                alert("Problem While Deleting !");
            </script>
            <?php
        }


        break;

    case 'Delete':

        if (isset($_GET['nriId']) && $_GET['nriId'] != "") {
            $nriId = $_GET['nriId'];
            $changeStartus = $modelObj->changestatusofNri($nriId);


            if ($changeStartus) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been deleted successfully!");
                    window.location.href = "/nri/controller/controller.php";
                </script>
                <?php
            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';

                header('location: /nri/controller/controller.php');
            }
        }
        break;
    case 'ContentDetailforHomePage':

        $getContent = $modelObj->fetchcontentforhome();

        if (isset($_POST) && is_array($_POST) && $_POST != "" && count($_POST) > 0) {

            $updateContent = $modelObj->udateHomeContent();


            if ($updateContent) {
                ?>
                <script type="text/javascript">
                    alert("Record(s) has been Updated successfully!");
                    window.location.href = "/nri/controller/controller.php?action=ContentDetailforHomePage";
                </script>
                <?php
            } else {
                //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                echo '<script>alert(" There are some Error !!")</script>';
            }
        }
        include(ROOT_PATH . "/nri/view/contentView.php");
        break;
    case 'topsixproperty':

        $TopNriProperties = $modelObj->getdataforNriProperty();


        //$previousTopProperties =  $modelOb->previousTopProperties();

        if (count($_POST) > 0) {

           
            
           // print_r($_POST['top']);
            
            $PropertyUPdate = array_unique($_POST['top']);
           
            if (count($PropertyUPdate) != 6){
                ?>
                 <script type="text/javascript">
                        alert("Please Choose Different Properties !!");
                        window.location.href = "/nri/controller/controller.php?action=topsixproperty";
                    </script>
                
          <?php  } else {
              
            $updateTable = $modelObj->udpateTopProp();
            if ($updateTable) {

                if ($updateTable) {
                    ?>
                    <script type="text/javascript">
                        alert("Record(s) has been Updated successfully!");
                        window.location.href = "/nri/controller/controller.php?action=topsixproperty";
                    </script>
                    <?php
                } else {
                    //echo 'fjgkldfjgkldjfgkldfjklgjdfklgfj';
                    echo '<script>alert(" There are some Error !!")</script>';
                }
            }
        }
        }
        include(ROOT_PATH . 'nri/view/TopProperties.php');
        break;
    
    default :

        $getDataForview = $modelObj->getdataforNRI();

        include(ROOT_PATH . "nri/view/view.php");
        break;
}
?>
