<?php

require_once(ROOT_PATH . "library/mysql_function.php");
require_once(ROOT_PATH . "nri/commonvariable.php");
require_once(ROOT_PATH . "nri/eventmail.php");

// this model for making all the query for event
class Model {

    private $dbObj;
    private $filesaveingpath;

    function __construct() {

        $this->dbObj = new mysql_function();

        if ($_SERVER['HTTP_HOST'] == '360cms.com') {

            $this->FIleSavingPath = ROOT_PATH . "/nri/";
        } else {

            $this->FIleSavingPath = "/var/www/html/myzone/media_images/nri/";
        }
    }

    // for inserting Data for nri
    public function insertIntoDb($updatefilesId = array(),$rankArr= array()) {

        $countryId = mysql_real_escape_string($_POST['CountryId']);

        $cityId = 0;
        if ($_POST['NriCity'] != "") {
            $cityId = mysql_real_escape_string($_POST['NriCity']);
        }
        $nriTemplateID = mysql_real_escape_string($_POST['nriTemplate']);
        $title = mysql_real_escape_string($_POST['nriTitle']);
        $Description = mysql_real_escape_string($_POST['nriDescription']);
        $displayMobileNo = mysql_real_escape_string($_POST['displayMobileNo']);

        $MetaTitle = mysql_real_escape_string($_POST['MetaTitle']);
        $MetaDes = mysql_real_escape_string($_POST['MetaDescription']);


        $MetaKeyword = mysql_real_escape_string($_POST['MetaKeyword']);

        $youtubeVideoLink = mysql_real_escape_string($_POST['videoLink']);


        $facebookShareLink = mysql_real_escape_string($_POST['fbShareLink']);
        $twitterShareLink = mysql_real_escape_string($_POST['twitterShareLink']);
        $linkedInShareLink = mysql_real_escape_string($_POST['linkedinShareLink']);
        $PinterestShareLink = mysql_real_escape_string($_POST['pinterestShareLink']);


        $status = mysql_real_escape_string($_POST['status']);
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);

        $blogs = '';

        if (is_array($_POST['blogs']) && count($_POST['blogs']) > 0) {

            $blogs = mysql_real_escape_string(implode(",", $_POST['blogs']));
        }

        $events = '';

        if (is_array($_POST['EventInterlinkedID']) && count($_POST['EventInterlinkedID']) > 0) {

            $events = mysql_real_escape_string(implode(",", $_POST['EventInterlinkedID']));
        }

        $createdDate = date('Y-m-d H:i:s');

        $sql = "INSERT INTO `tsr_nri` ("
                . "`countryId`,"
                . " `cityId`, "
                . " `nriTitle`,"
                . " `nriDescription`, "
                . " `youtubeVideoLink`, "
                . "`PPCnumber`, "
                . "`BlogInterlinkedID`,"
                . "EventInterlinkedID,"
                . " `nriTemplateID`, "
                . "`facebookShareLink`,"
                . " `twitterShareLink`,"
                . " `linkedInShareLink`, "
                . "`PinterestShareLink`,"
                . "`MetaTitle`, "
                . "`MetaDescription`, "
                . "`metaKeywords`,"
                . "`CreatedDate`,"
                . " `CreatedBy`,"
                . " `status`,"
                . " archives) "
                . "VALUES ('" . $countryId . "', "
                . "'" . $cityId . "',"
                . "'" . $title . "', "
                . "'" . $Description . "',"
                . " '" . $youtubeVideoLink . "',"
                . " '" . $displayMobileNo . "', "
                . "'" . $blogs . "', "
                . "'" . $events . "', "
                . "'" . $nriTemplateID . "' ,"
                . "'" . $facebookShareLink . "',"
                . " '" . $twitterShareLink . "', "
                . "'" . $linkedInShareLink . "', "
                . "'" . $PinterestShareLink . "', "
                . "'" . $MetaTitle . "',"
                . " '" . $MetaDes . "', "
                . "'" . $MetaKeyword . "', "
                . "'" . $createdDate . "',"
                . " '" . $CreatedBy . "',"
                . " '" . $status . "',"
                . " 'live'  )";


        $query = $this->dbObj->insert_query($sql);

        $nriId = $query;

        $this->uploadfileforNRI($nriId);
        $this->savefile($nriId);
        $this->saveDataforNriproperty($nriId);
        if (is_array($updatefilesId) && count($updatefilesId) > 0) {

            $this->updatefilemapwithcurrentIds($nriId, $updatefilesId);
        }
      if (is_array($rankArr) && count($rankArr) > 0) {

            $this->updateRankofProperty($rankArr);
        }


        if ($query) {
            // this code for sending mail to event status
            // $this->sendMailForStatusChange('create');
        }

        return $query;
    }

    
    //update rank of the project
    public function updateRankofProperty($rankArr){
        
        
    
        
        foreach($rankArr as $key => $value){
        
        $qry = "update  tsr_nri_property set DisplayRank = " . $value . " where property_id = " . $key . "";

        $updateRank =     $this->dbObj->update_query($qry);
            
        }

        return true;
    }
    
    
    
    //this method map old file with current nri ids
    private function updatefilemapwithcurrentIds($nriId, $toBeupdateIds) {

        if (is_array($toBeupdateIds) && $nriId != 0 && count($toBeupdateIds) > 0) {

            $updateid = implode(",", $toBeupdateIds);

            $qry = "update  tsr_nri_assets set NriId = " . $nriId . " where id in (" . $updateid . ")";

            $this->dbObj->update_query($qry);
        }

        return true;
    }

    // this method for upload all file in folder
    private function uploadfileforNRI($nriId) {



        $fileArray['Banner'] = $_FILES['bannerName'];
        $fileArray['Images'] = $_FILES['Images'];


        foreach ($fileArray as $key => $value) {

            foreach ($value as $key1 => $value1) {
                if ($value1[0] != '') {

                    foreach ($value1 as $key2 => $value2) {

                        $tmpname = $value['tmp_name'][$key2];

                        $fileName = $value['name'][$key2];

                        $pathNRIId = $this->FIleSavingPath . 'assets/' . $nriId;

                        if (!is_dir($pathNRIId)) {
                            mkdir($pathNRIId);
                            chmod($pathNRIId, 0777);
                        }

                        $pathFileType = $pathNRIId . '/' . $key;
                        if (!is_dir($pathFileType)) {
                            mkdir($pathFileType);
                            chmod($pathFileType, 0777);
                        }

                        move_uploaded_file($tmpname, $pathFileType . "/" . $fileName);
                    }
                }
            }
        }
    }

    // this function for saving uploading file data into db
    private function savefile($nriId) {


        $FileArr['Banner'] = $_FILES['bannerName']['name'];
        $FileArr['Images'] = $_FILES['Images']['name'];


        foreach ($FileArr as $key => $value) {

            if (count($value) > 0 && $value[0] != "") {

                foreach ($value as $fileNames) {

                    $qry = 'INSERT INTO `tsr_nri_assets` (`AssetType`, `NriId`, `FileName`,FIlePath,archives)'
                            . ' values( '
                            . '"' . mysql_real_escape_string($key) . '",'
                            . '"' . mysql_real_escape_string($nriId) . '",'
                            . '"' . mysql_real_escape_string($fileNames) . '",'
                            . '"' . mysql_real_escape_string($this->FIleSavingPath . 'assets/' . $nriId . "/" . $key . "/" . $fileNames) . '",'
                            . '"' . mysql_real_escape_string('live') . '")';


                    $query = $this->dbObj->insert_query($qry);
                }
            }
        }



        if ($query) {

            return true;
        } else {

            return false;
        }
    }

    //this function for send mail when status has been changed
    private function sendMailForStatusChange($actionType, $oldStatus = '') {

        global $recordOnePage;
        global $eventmailSubjectArr;
        global $eventmailAddreplayto;
        global $eventmailToMail;
        global $eventmailcc;
        global $eventStatusArray;

        $dataArray['subject'] = $eventmailSubjectArr['changeStatus'];
        $dataArray['adreplyto'] = $eventmailAddreplayto['changeStatus'];
        $dataArray['address'] = $eventmailToMail['changeStatus'];
        $dataArray['cc'] = $eventmailcc['changeStatus'];

        $mailData['oldStatus'] = trim($eventStatusArray[$oldStatus]);
        $mailData['status'] = trim($eventStatusArray[$_POST['status']]);
        $mailData['title'] = trim(strip_tags($_POST['eventTitle']));
        $mailData['mailType'] = 'update';

        ob_start();

        include(ROOT_PATH . "nri/view/mailview.php");

        $dataArray['msg'] = ob_get_contents();

        ob_get_clean();

        sendMailChangeStatus('update', $dataArray);



        global $recordOnePage;
        global $eventmailSubjectArr;
        global $eventmailAddreplayto;
        global $eventmailToMail;
        global $eventmailcc;
        global $eventStatusArray;

        $dataArray['subject'] = $eventmailSubjectArr[$actionType];
        $dataArray['adreplyto'] = $eventmailAddreplayto[$actionType];
        $dataArray['address'] = $eventmailToMail[$actionType];
        $dataArray['cc'] = $eventmailcc[$actionType];
        $mailData['status'] = $eventStatusArray[$_POST['status']];
        $mailData['title'] = trim(strip_tags($_POST['eventTitle']));

        if ($actionType == 'changeStatus') {

            $mailData['oldStatus'] = trim($eventStatusArray[$oldStatus]);
        }
        $mailData['mailType'] = $actionType;

        ob_start();

        include(ROOT_PATH . "events/view/mailview.php");

        $dataArray['msg'] = ob_get_contents();
        ob_get_clean();

        sendMailChangeStatus($actionType, $dataArray);
    }

    // this function for for map property and nri Id
    private function saveDataforNriproperty($nriId) {

        $property = $_POST['eventProperty'];

        $cities = $_POST['eventCity'];

        $country = $_POST['eventPropertyCountry'];

        $usp = $_POST['propertyUsp'];

        $description = $_POST['propertyDescription'];

        $youtubelink = $_POST['PropertyvideoLink'];

        $propertyBlogs = $_POST['propertyselectBlog'];

        $Holidays = $_POST['ispropertyHolidayHome'];

        $Rois = $_POST['ispropertyRoiHome'];

        $propertyhighLight = $_POST['property_highlight'];

        $propertymetatile = $_POST['PropertyMetaTitle'];

        $propertymetakeywords = $_POST['PropertyMetaKeyword'];

        $propertymetadescription = $_POST['PropertyMetaDescription'];


        for ($i = 0; $i < count($property); $i++) {

            $qry = 'insert into  tsr_nri_property (NriId,'
                    . 'property_id,'
                    . 'city_id,'
                    . 'country_id,'
                    . 'property_usp,'
                    . 'property_description,'
                    . 'created_date,'
                    . 'archives,'
                    . 'youtubeUrl,'
                    . 'selectedBlogIds,'
                    . 'isholidayorSecoundHome,'
                    . 'isRoiproperty,'
                    . 'property_highlight,'
                    . 'PropertyMetaTitle,'
                    . 'PropertyMetaKeyword,'
                    . 'PropertyMetaDescription'
                    . ')'
                    . ' values("' . mysql_real_escape_string($nriId) . '",'
                    . '"' . mysql_real_escape_string($property[$i]) . '",'
                    . '"' . mysql_real_escape_string($cities[$i]) . '",'
                    . '"' . mysql_real_escape_string($country[$i]) . '",'
                    . '"' . mysql_real_escape_string($usp[$i]) . '",'
                    . '"' . mysql_real_escape_string($description[$i]) . '",'
                    . '"' . mysql_real_escape_string(date('Y-m-d H:i:s')) . '",'
                    . '"' . mysql_real_escape_string('live') . '",'
                    . '"' . mysql_real_escape_string($youtubelink[$i]) . '",'
                    . '"' . mysql_real_escape_string($propertyBlogs[$i]) . '",'
                    . '"' . mysql_real_escape_string($Holidays[$i]) . '",'
                    . '"' . mysql_real_escape_string($Rois[$i]) . '",'
                    . '"' . mysql_real_escape_string($propertyhighLight[$i]) . '",'
                    . '"' . mysql_real_escape_string($propertymetatile[$i]) . '",'
                    . '"' . mysql_real_escape_string($propertymetakeywords[$i]) . '",'
                    . '"' . mysql_real_escape_string($propertymetadescription[$i]) . '"'
                    . ')';


            $query = $this->dbObj->insert_query($qry);

//            $qry = 'select id from nri_propery_images where propertyId = "' . mysql_real_escape_string($property[$i]) . '" and 	archives = "'. mysql_real_escape_string('live').'"';
//            
//            
//            
//            $checkPropertyExist = $this->dbObj->getAllData($qry);
//            
//            if (count($checkPropertyExist) == 0){
//                
            
            $this->uploadFIleForPropertyBanner($property[$i]);
            $this->uploadFIleForPropertyImage($property[$i]);
            
//            }
//            
            
    
        }



        if ($query) {

            return true;
        } else {

            return false;
        }
    }

    private function uploadFIleForPropertyBanner($propertyId) {

        $fileArray = $_FILES['PropertybannerName_' . $propertyId];


        if (count($fileArray) > 0) {
            foreach ($fileArray as $key1 => $value1) {

                if (count($value1) > 0) {

                    foreach ($value1 as $key2 => $value2) {

                        $tmpname = $fileArray['tmp_name'][$key2];

                        $fileName = $fileArray['name'][$key2];

                        $pathNRIId = $this->FIleSavingPath . 'nriproperty';

                        if (!is_dir($pathNRIId)) {
                            mkdir($pathNRIId);
                            chmod($pathNRIId, 0777);
                        }

                        $pathFileType = $pathNRIId . '/' . $propertyId;
                        if (!is_dir($pathFileType)) {
                            mkdir($pathFileType);
                            chmod($pathFileType, 0777);
                        }

                        move_uploaded_file($tmpname, $pathFileType . "/" . $fileName);

                        // 
                    }
                }
            }
        }
        $this->savefilePathForProperty($propertyId, $fileArray,'Banner');
    }

   

     private function uploadFIleForPropertyImage($propertyId) {


       $fileArray = $_FILES['PropertyImageName_'. $propertyId];


        if (count($fileArray) > 0) {
            
            
            foreach ($fileArray as $key1 => $value1) {

                if (count($value1) > 0) {

                    foreach ($value1 as $key2 => $value2) {

                        $tmpname = $fileArray['tmp_name'][$key2];

                        $fileName = $fileArray['name'][$key2];

                        $pathNRIId = $this->FIleSavingPath . 'nripropertyImage';

                        if (!is_dir($pathNRIId)) {
                            mkdir($pathNRIId);
                            chmod($pathNRIId, 0777);
                        }
   $pathFileType = $pathNRIId . '/' . $propertyId;
                       
                        if (!is_dir($pathFileType)) {
                            mkdir($pathFileType);
                            chmod($pathFileType, 0777);
                        }

                        move_uploaded_file($tmpname, $pathFileType . "/" . $fileName);

                        // 
                    }
                }
            }
        }
        $this->savefilePathForProperty($propertyId, $fileArray,'image');
    }

    
    
    
    private function savefilePathForProperty($propertyId, $filearray,$type) {

        if (count($filearray) > 0) {

            foreach ($filearray['name'] as $value) {

                if ($value != "") {
                    
                    if ($type == 'Banner'){
                        
                        $folder = 'nriproperty';
                    }
                    
                    if ($type == 'image'){
                        
                         $folder = 'nripropertyImage';
                    }
                    
                    $url = $folder."/". $propertyId . "/" . $value;
                    $qry = 'insert into  nri_propery_images (propertyId,assets_type,FileUrl,archives) '
                            . 'values("' . mysql_real_escape_string($propertyId) . '",'
                            . '"'.mysql_real_escape_string($type).'",'
                            . '"' . mysql_real_escape_string($url) . '",'
                            . '"live"'
                            . ') ';

                    $query = $this->dbObj->insert_query($qry);
                }
            }
        }
        if ($query) {

            return true;
        }
    }

    // this function for update data into db
    public function udateNRIData() {

        if (isset($_GET['NriId']) && $_GET['NriId'] != "" && $_GET['NriId'] != 0 && is_numeric($_GET['NriId'])) {
            $NriId = $_GET['NriId'];

            //for geting old status of the event

            $qry = 'select status from tsr_nri where id = "' . mysql_real_escape_string($NriId) . '"';
            $rec = $this->dbObj->getAllData($qry);
            $oldStatus = $rec[0]['status'];


            $qry = "Update tsr_nri set archives = 'history' where id = '" . mysql_real_escape_string($NriId) . "'";

            $updateStatus = $this->dbObj->update_query($qry);

            $qry = "select property_id,DisplayRank from tsr_nri_property where NriId = '" . mysql_real_escape_string($NriId) . "'";

             $rec = $this->dbObj->getAllData($qry);
             
             foreach ( $rec as $value){
                 
                 $Propertyrankarr[$value['property_id']] = $value['DisplayRank'];
                 
                 
             }

            $qry = "Update tsr_nri_property set archives = 'history' where NriId = '" . mysql_real_escape_string($NriId) . "'";

            $updateProperty = $this->dbObj->update_query($qry);

            $this->updatefileforNri($NriId);

            $updatefileId = $this->selectoldfilesIds($NriId);

            $upatePropertyImages = $this->changeStatusOfPropertyImages();


            $this->insertIntoDb($updatefileId,$Propertyrankarr);

            if ($updateStatus) {

                if ($oldStatus != $_POST['status']) {

                    //  $this->sendMailForStatusChange('changeStatus', $oldStatus);
                }
                return true;
            } else {


                return false;
            }
        }
    }

    // this function for update file when delete ..
    private function updatefileforNri($NriId) {

        $tobedelteIds = $_POST['alldleleteFIleIds'];

        $tounlinkFiles = $_POST['deleteFIlesPath'];

        $deleteFIleArr = explode("#", $tobedelteIds);

        $unlinkFIlearr = explode("#", $tounlinkFiles);

        if ($_POST['alldleleteFIleIds'] != "") {

            foreach ($deleteFIleArr as $value) {

                $qry = 'update tsr_nri_assets set archives = "history" where id = ' . mysql_real_escape_string($value);

                $qry = $this->dbObj->exec_query($qry);
            }
        }

        return $qry;
    }

    // this function for geting old file id for updateing with current id for nri
    private function selectoldfilesIds($NriId) {

        $qry = "select id from 	tsr_nri_assets where NriId = " . $NriId . " and archives = 'live'";


        $result = $this->dbObj->getAllData($qry);

        foreach ($result as $value) {

            $tobeupdateids[] = $value['id'];
        }


        return $tobeupdateids;
    }

    //this function for makeing for fething aal event for maping NRI page
    public function getEventData($countryId, $cityId) {


        $qry = 'select 	id,EventTitle from tsr_events where id > 0';

        if ($countryId != 0) {

            $qry .= ' AND countryId = "' . mysql_real_escape_string($countryId) . '"';
        }

        if ($cityId != 0) {

            $qry .= ' AND cityId = "' . mysql_real_escape_string($cityId) . '"';
        }

        $qry .= ' AND status = 3 and archives = "live" ';

        $eventData = $this->dbObj->getAllData($qry);


        return $eventData;
    }

    // this function for change status for nri page
    public function changestatusofNri($nriId) {

        $qry = "Update tsr_nri set archives = 'history' where id = '" . $nriId . "'";

        $updateStatus = $this->dbObj->update_query($qry);



        return $updateStatus;
    }

    // this function for fetch list of blog
    public function dropdownBlog() {
        //$result = array();
        $sql = "SELECT id, title FROM tsr_blog";
        $sql1 = mysql_query($sql);
        if (mysql_num_rows($sql1)) {
            while ($result = mysql_fetch_array($sql1)) {
                $result2[] = $result;
            }
            return $result2;
        }
    }

    // this function for fetch country
    public function fetchCity($countryId) {

        $sql = 'SELECT id,city FROM tsr_cities where country_id = ' . mysql_real_escape_string($countryId);

        $rec = $this->dbObj->getAllData($sql);

        return $rec;
    }

    // this function for fetch property
    public function fetchProperty($cityId) {

        $sql = 'SELECT id,property_name FROM tsr_properties where  cty_id = ' . mysql_real_escape_string($cityId);

        $rec = $this->dbObj->getAllData($sql);

        return $rec;
    }

    // this function for fetch all data for NRI PAGE
    public function getdataforNRI($NriId = "") {

        $sql = 'SELECT nri.*, conty.country,cty.city FROM tsr_nri nri
            inner join tsr_countries conty on nri.countryId = conty.id
            left join tsr_cities cty on nri.cityId = cty.id where nri.id > 0';

        if ($NriId != "") {

            $sql .= ' and nri.id = "' . mysql_real_escape_string($NriId) . '"';
        }

        $sql .= ' and archives = "live" order by nri.CItyId';


        //echo $sql;


        $rec = $this->dbObj->getAllData($sql);

        return $rec;
    }

    // this function for get property data of any nri page
    public function getdataforNriProperty($NriId = 0) {

        $sql = 'select tep.id,cty.city,tp.property_name,tep.property_id,tep.city_id,tep.DisplayRank, '
                . 'tep.country_id,tep.property_usp,tep.property_description ,tep.property_highlight,'
                . 'tep.youtubeUrl,tep.selectedBlogIds,tep.isholidayorSecoundHome,tep.isRoiproperty,'
                . 'tep.PropertyMetaTitle,tep.PropertyMetaKeyword,tep.PropertyMetaDescription from tsr_nri_property tep '
                . ' inner join tsr_properties tp on tep.property_id = tp.id '
                . ' inner join tsr_cities cty on tep.city_id = cty.id '
                . ' inner join tsr_nri nri on tep.nriId = nri.id where 1=1';

        if ($NriId != 0) {

            $sql .= ' AND tep.nriId = "' . mysql_real_escape_string($NriId) . '" ';
        }

        $sql .= ' and tep.archives = "live" and nri.archives = "live" ';

        if ($NriId == 0) {

            $sql .= ' group by tep.property_id ';
        }

        $sql .= '  order by tep.id';

        // echo $sql;

        $rec = $this->dbObj->getAllData($sql);

        //    echo '<pre>';
//print_r($rec);

        return $rec;
    }

    //this function for geting data of uploading files for nri page
    public function getdataforuploadingFiles($NriId) {

        $qry = 'select id,AssetType,FIleName,FIlePath from tsr_nri_assets where 	NriId = ' . mysql_real_escape_string($NriId) . ' and archives = "live" ';

        $rec = $this->dbObj->getAllData($qry);
if (count($rec)>0){
        foreach ($rec as $key => $value) {

            $filearr['name'] = $value['FIleName'];
            $filearr['id'] = $value['id'];

            $path = $value['FIlePath'];



            $filesarry = explode("/", $path);

            $getassstpathkey = array_search('assets', $filesarry);

            $folderIdkey = $getassstpathkey + 1;

            $folderId = $filesarry[$folderIdkey];



            $filearr['folderId'] = $folderId;

            $filedisplayArray[$value['AssetType']][] = $filearr;
        }
}
        return $filedisplayArray;
    }

    public function getdataforNriPropertyImages($propertyIds) {


        $propertyIdsStr = implode(",", $propertyIds);

        $sql = 'select id,propertyId,assets_type,FileUrl from  nri_propery_images where archives = "live" and propertyId in (' . $propertyIdsStr . ')';

        $rec = $this->dbObj->getAllData($sql);

        foreach ($rec as $value) {

            $propertyImagesData[$value['propertyId']][$value['assets_type']][] = $value;
        }

        return $propertyImagesData;
    }

    public function insertBannerDb() {
        $MainPageBannersSavePath = $this->FIleSavingPath . '/MainPageBanners';
        $tempName = $_FILES['bannerMainPage']['tmp_name'];
        $MainPageBannerName = $_FILES['bannerMainPage']['name'];
        $FinalPath = $MainPageBannersSavePath . '/' . $MainPageBannerName;
        // if (file_exists($FinalPath)) {
        //   return false;
        //} elseif (!file_exists($FinalPath)) {
        move_uploaded_file($tempName, $MainPageBannersSavePath . '/' . $MainPageBannerName);
        $ShortTitle = mysql_real_escape_string($_POST['shortTitle']);
        $ShortDes = mysql_real_escape_string($_POST['shortDes']);
        $status = mysql_real_escape_string($_POST['bannerStatus']);
        $CreatedBy = mysql_real_escape_string($_SESSION['login']['id']);
        $FileName = $_FILES['bannerMainPage']['name'];

        $sql = "INSERT INTO tsr_NRI_MainBanners (BannerName, ShortTitle, ShortDes, status, CreatedBy) VALUES ('" . $FileName . "' ,'" . $ShortTitle . "', '" . $ShortDes . "', '" . $status . "', '" . $CreatedBy . "')";

        $query = $this->dbObj->insert_query($sql);

        return true;
        // }
    }

    public function getDataForBanner($bannnerId = '') {
        $sql = 'SELECT id, ShortTitle, ShortDes,BannerName, status FROM tsr_NRI_MainBanners WHERE status !="3" ';
        if ($bannnerId != "") {

            $sql .= ' AND id ="' . mysql_real_escape_string($bannnerId) . '"';
        } else {

            $sql .= ' order by id DESC ';
        }

        $data = $this->dbObj->getAllData($sql);
        return $data;
    }

    public function updateBannerDetails($eventBannerId) {
        $sql = "UPDATE tsr_NRI_MainBanners 
                SET ShortTitle='" . mysql_real_escape_string($_POST['shortTitle']) . "', 
                    ShortDes='" . mysql_real_escape_string($_POST['shortDes']) . "', 
                    status='" . mysql_real_escape_string($_POST['bannerStatus']) . "' 
                WHERE tsr_NRI_MainBanners.id = '" . mysql_real_escape_string($eventBannerId) . "' ";
        $update = $this->dbObj->update_query($sql);
        return $update;
    }

    public function deleteBanner($bannerId) {
        $sql = "UPDATE tsr_NRI_MainBanners SET status='3' WHERE id='" . mysql_real_escape_string($bannerId) . "'";
        $delete = $this->dbObj->update_query($sql);
        if ($delete) {
            return true;
        } else {

            return false;
        }
    }

    public function selectAllCountry() {

        $sql = 'SELECT id, country  FROM tsr_countries  ';


        $sql .= ' order by id asc ';


        $data = $this->dbObj->getAllData($sql);
        return $data;
    }

    public function getEventselected($NriId) {

        $selectvalue = 'select EventInterlinkedID from tsr_nri where id = ' . mysql_real_escape_string($NriId) . '';

        $eventids = $this->dbObj->getAllData($selectvalue);



        $qry = 'select id,EventTitle from tsr_events where id in (' . $eventids[0]['EventInterlinkedID'] . ')';



        $eventData = $this->dbObj->getAllData($qry);


        return $eventData;
    }

    public function udateHomeContent() {

        $qry = "select max(id)as id from nri_home_content";

        $contentId = $this->dbObj->getAllData($qry);

        $qry = 'update nri_home_content set archives = "history" where id = "' . $contentId[0]['id'] . '"';


        $update = $this->dbObj->update_query($qry);

        $qry = 'insert into nri_home_content (content,'
                . 'metaTItle,'
                . 'metaKeyword,'
                . 'metaDescription,'
                . 'archives) '
                . 'values("' . mysql_real_escape_string($_POST['homecontent']) . '",'
                . '"' . mysql_real_escape_string($_POST['metaTItle']) . '",'
                . '"' . mysql_real_escape_string($_POST['metaKeyword']) . '",'
                . '"' . mysql_real_escape_string($_POST['metaDescription']) . '",'
                . '"live")';


        $query = $this->dbObj->insert_query($qry);

        return $query;
    }

    public function fetchcontentforhome() {

        $qry = 'select content,metaTItle,metaKeyword,metaDescription from nri_home_content where archives = "live"';

        $contentData = $this->dbObj->getAllData($qry);



        return $contentData[0];
    }


    public function udpateTopProp(){

        
        
        $sql = 'update tsr_nri_property set DisplayRank = 7 where DisplayRank in (1,2,3,4,5,6)';



        $updateold = $this->dbObj->update_query($sql);

        

        //if ($updateold) {

            for ($i = 1; $i <= 6; $i++) {

                $sql = 'update tsr_nri_property set DisplayRank = ' . $i . ' where property_id = ' . mysql_real_escape_string($_POST['top'][$i - 1]) . '';

          


                $update = $this->dbObj->update_query($sql);
            }
            return $update;
       // }
    }

    public function fetchPropertyhighlight($propertyId) {

        $qry = 'select highlights from  tsr_properties where id = "' . mysql_real_escape_string($propertyId) . '"';

        $HighlightData = $this->dbObj->getAllData($qry);



        return $HighlightData[0];
    }

    //this function for change sttus in case the property images has been deleted
    private function changeStatusOfPropertyImages() {

        $alldeletedtxt = $_POST['deletePropertyImage'];



        foreach ($alldeletedtxt as $key => $value) {

            if ($value != "") {

                $deletedIds = explode("#", $value);

                foreach ($deletedIds as $key1 => $value1) {

                    $sql = 'update nri_propery_images set archives = "history" where id = "' . mysql_real_escape_string($value1) . '" ';

                    $update = $this->dbObj->update_query($sql);
                }
            }
        }


        return $update;
    }

}

// end class 
?>
