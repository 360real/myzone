<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
include_once LIBRARY_PATH . "library/server_config_admin.php";
include_once(LIBRARY_PATH . "faq_location/commonvariable.php");
include_once(LIBRARY_PATH . "faq_location/model/model.php");

$modelObj = new Model();

admin_login();
$rolesession = explode(',',$_SESSION['login']['role_id']);
/*$resultdev = !empty(array_intersect($rolesession ,array(21,22) ));*/
$result = !empty(array_intersect($rolesession ,array(1,23)));
if(count($result) ==0){
$obj_common->redirect('/index.php');
}
include(LIBRARY_PATH . "includes/header.php");
$commonHead = new commonHead();
$commonHead->commonHeader('FAQ Location Managemant', $site['TITLE'], $site['URL']);
$pageName = C_ROOT_URL . "/faq_location/controller/controller.php";


if($_GET['search']=='Submit'){
     $listdata = $modelObj->listdata($_GET['city_id']);
    include(ROOT_PATH . "/faq_location/view/listfaq_location.php");
}

switch ($action) {
    case 'addfaq_location':
    include(ROOT_PATH . "/faq_location/view/addfaq_location.php");
    break;
    case 'editfaq_location':
    $listdata = $modelObj->listeditdata($_GET['id']);
    include(ROOT_PATH . "/faq_location/view/addfaq_location.php");
    break;
    case 'faq_locationlist':
    $listdata = $modelObj->listdata($_GET['city_id']);
    include(ROOT_PATH . "/faq_location/view/listfaq_location.php");
    break;
    case 'deletefaq_location':
    $deletedata = $modelObj->deletedata($_GET['id']);
    $listdata = $modelObj->listdata('');
    header('Location: /faq_location/controller/controller.php?city_id='.$_GET['city_id'].'&search=Submit');
    break;
}

?>
