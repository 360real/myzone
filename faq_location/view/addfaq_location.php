
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $site['URL']?>view/js/jquery.dropdown.css">
<script src="<?php echo $site['URL']?>view/js/jquery.dropdown.js"></script>
  <style>
  .dropdown-display {
  min-height: 16px !important;
  padding: 8px;
    width: 350px;
    border-radius: 0;
    border: 1px solid #548890;
  }

select, input[type=text], input[type=password] {
    border: 1px solid #548890;
    font-family: Arial;
    font-size: 12px;
    height: 20px;
    padding: 4px;
}
.dropdown-main {
    width: 368px;
}
.dropdown-multiple, .dropdown-multiple-label, .dropdown-single {
    width: 350px!important;
}
  </style>



<form id="datafm"  enctype="multipart/form-data">

  <input type="hidden" name="post_id" value="<?php echo($listdata[0]['id'])?>">
 <input type="hidden" name="location_id" value="<?php echo($_GET['loc_id'])?>">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable" id="container">
<thead>
<tr><th colspan="1" align="left"><h3>FAQ</h3></th><th>   <?php if(isset($_GET['id'])){

    }else{  ?>
    <a href='#' id="btnAddtoList">+ Add FAQ</a>
    
  <?php } ?></th><th align="right"><a class="buttons" href="/faq_location/controller/controller.php?city_id=<?php echo($_GET['city_id'])?>&search=Submit"><h3>FAQ List</h3></a></th></tr>
</thead>
<tbody id="mainsection">
<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000">*</font>Select City</strong></td>
  <td  colspan="2">
     <?php 
      $sql = 'SELECT `id`,`city` FROM tsr_cities where status=1';
      $citylist =$obj_mysql->getAllData($sql);
      ?>
      <div class="">
        <select  id="city_id"  name="city_id" placeholder="Select City" style="height: 30px;">
          <option> Select City</option>
          <?php foreach($citylist as $value){ ?>
          <option value="<?php echo $value['id']; ?>" <?php if($value['id']==$listdata[0]["city_id"]){ echo 'selected';} ?>><?php echo $value['city']; ?> </option>
          <?php } ?>
        </select>
      </div>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000"></font>Select City</strong></td>
  <td  colspan="2">
     <?php 
      $sql = 'SELECT `id`,`type_category` FROM tsr_types_categories where status=1';
      $catlist =$obj_mysql->getAllData($sql);
      ?>
      <div class="">
        <select  id="type_category"  name="type_category" placeholder="Select Category" style="height: 30px;">
          <option> Select Category</option>
          <?php foreach($catlist as $value){ ?>
          <option value="<?php echo $value['id']; ?>" <?php if($value['id']==$listdata[0]["type_category"]){ echo 'selected';} ?>><?php echo $value['type_category']; ?> </option>
          <?php } ?>
        </select>
      </div>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong><font color="#FF0000"></font>Select Bedroom</strong></td>
  <td  colspan="2">
    <select name="bedroom" id="bedroom"  style="height: 30px;">
      <option value="">Select Bedroom Type</option>
      <option value="1bhk" <?php if("1bhk"==$listdata[0]["bedroom"]){ echo 'selected';} ?>>1 BHK</option>
      <option value="1.5bhk" <?php if("1.5bhk"==$listdata[0]["bedroom"]){ echo 'selected';} ?>>1.5 BHK</option>
      <option value="2bhk" <?php if("2bhk"==$listdata[0]["bedroom"]){ echo 'selected';} ?>>2 BHK</option>
      <option value="2.5bhk" <?php if("2.5bhk"==$listdata[0]["bedroom"]){ echo 'selected';} ?>>2.5 BHK</option>
      <option value="3bhk" <?php if("3bhk"==$listdata[0]["bedroom"]){ echo 'selected';} ?>>3 BHK</option>
    </select>
  </td>
</tr>


  <tr class="<?php echo($clsRow1)?>"><td align="right"><strong><font color="#FF0000">*</font>Question</strong></td><td colspan="2"><input name="question_1" type="text" title="question" lang='MUST' value="<?php echo($listdata[0]['question'])?>" size="100%" maxlength="120" style="width:85%"/></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right"><strong><font color="#FF0000">*</font>Answer</strong></td><td colspan="2"><textarea class="ckeditor" name="answer_1"  id="answer" rows="5" cols="170"><?php echo $listdata[0]["answer"]; ?></textarea></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right"><strong><font color="#FF0000">*</font>Order</strong></td><td colspan="2"><input name="ordering_1" type="number" title="ordering" lang='MUST' value="<?php echo($listdata[0]['ordering'])?>" size="50%" maxlength="120" /></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td><td valign="bottom" colspan="2"><input type="radio" name="status_1" value="1" id="1" <?php if ($listdata[0]['status']=="1"){ echo("checked='checked'");} ?> /> <label for="1">Active</label> <input type="radio" name="status_1" value="0" id="0" <?php if ($listdata[0]['status']=="0"){ echo("checked='checked'");} ?>/> <label for="1">In-Active</label> </td></tr></tbody>
<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left" colspan="2"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" />
   
  </td>
</tr>
</table>
</form>

<script type="text/javascript">
  $('.dropdown-mul-2').dropdown({
    limitCount: 500,
    searchable: true
  });
</script>




<script type="text/javascript">
var rowCount = 2;
$(function() {
$('#btnAddtoList').click(function(){
  //alert(rowCount);
  var newDiv = $('<tr class="<?php echo($clsRow1)?>"><td align="right"><strong><font color="#FF0000">*</font>Question</strong></td><td colspan="2"><input name="question_'+rowCount+'" type="text" title="question" lang="MUST"  size="100%" maxlength="120" style="width:85%"/></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right"><strong><font color="#FF0000">*</font>Answer</strong></td><td colspan="2"><textarea class="ckeditor" name="answer_'+rowCount+'"  id="answer" rows="5" cols="170"></textarea></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right"><strong><font color="#FF0000">*</font>Order</strong></td><td colspan="2"><input name="ordering_'+rowCount+'" type="number" title="ordering" lang="MUST"  size="50%" maxlength="120" /></td></tr><tr class="<?php echo($clsRow1)?>"><td align="right" ><strong>Status</strong> </td><td valign="bottom" colspan="2"><input type="radio" name="status_'+rowCount+'" value="1" id="1"  /> <label for="1">Active</label> <input type="radio" name="status" value="0" id="0"/> <label for="1">In-Active</label> </td></tr>');
  //newDiv.style.background = "#000";
  $('#mainsection').append(newDiv);
   CKEDITOR.replace('answer_'+rowCount);
  rowCount ++;
});
});

/*function adscity() {
  var ads_city = $("#city_id").val();
  $.ajax({
      url: "/faq_location/controller/locationdata.php",
      type: 'POST',  
      data: {'city':ads_city},
      success: function (data) {
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select Location </option>';
          for (var i = 0; i < json.length; i++) {
             opetionhtml+='<option value='+json[i]['id']+'>'+json[i]['location']+'</option>';
          }
          $('#location_id').html(opetionhtml);
      }
  });
}

$(window).on("load", function(){
 var ads_city = '<?php echo $listdata[0]["city_id"]; ?>';
  $.ajax({
      url: "/faq_location/controller/locationdata.php",
      type: 'POST',  
      data: {'city':ads_city},
      success: function (data) {
        var json=JSON.parse(data);
        var opetionhtml = '<option value="">Select Location </option>';
          for (var i = 0; i < json.length; i++) {
            var selected='';
            if(json[i]['id']=='<?php echo $listdata[0]["location_id"]; ?>'){

             selected='selected';
            }
            opetionhtml+='<option value='+json[i]['id']+' '+selected+'>'+json[i]['location']+'</option>';
          }
          $('#location_id').html(opetionhtml);
      }
  });
});*/

  $(document).ready(function(){  
    var data = new FormData();
    $('.button').on('click', function(e){  
      e.preventDefault();
      var form_data = $('#datafm').serializeArray();
      $.each(form_data, function (key, input) {
        data.append(input.name, input.value);
      });
      data.append("counter", rowCount);
      data.append('key', 'value');
      $.ajax({
        url: "/faq_location/controller/ajaxdata.php",
        method: "post",
        processData: false,
        contentType: false,
        data: data,
        success: function (data) {
          //alert(data); return false;
          //console.log(data);
          var json=JSON.parse(data);
          if(json.status==0){
            alert(json.msg);
            window.location="/faq_location/controller/controller.php?loc_id=<?php echo $_GET['loc_id']; ?>&search=Submit";
          }else{
            alert(json.msg);
          }
        }
      });
    });
  });

$('#event_type').on('change',function(e){
	//alert('hi');
	var val = $(this).val();
	//alert(val);
	if(val >0){
		$('#event_div').hide();
		$('#join_url_div').show();
		$('#emb_code_div').show();
		$('#join_url_div').prop('disabled',true);
		$('#emb_code_div').prop('disabled',true);
		
		}else{
			$('#event_div').show();
			$('#join_url_div').hide();
			$('#emb_code_div').hide();
			$('#join_url_div').prop('disabled',true);
			$('#emb_code_div').prop('disabled',true);

			}
	})
</script>


<script type="text/javascript">
function ValidateAuthorImage(event, obj,fname) {
        var files = event.target.files;
        var valid = true;
        var height = 0;
        var width = 0;
        var _URL = window.URL || window.webkitURL;
        for (var i = 0; i < files.length; i++) {
            var img = new Image();
            img.onload = function () {
                height = img.height;
                width = img.width;
                //alert(width + " " + height);
                if(fname=='event_image'){
                if (width != '1024' || height!='500') {
                    //$("#lblErrorMessageAuthorImage").html("Please upload author image in squire.");
                    alert("Please upload image 1024x500px size");
                    obj.value = "";
                    return false;
                }
      }
     
            }
            img.src = _URL.createObjectURL(files[i]);
        }
    }
</script>


