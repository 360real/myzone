<?php

	require_once(ROOT_PATH . "library/mysqli_function.php");
	require_once(ROOT_PATH . "banner_manager/commonvariable.php");

	// this model for making all the query for event
	class Model {

	    private $dbObj;

	    function __construct() {

	        $this->dbObj = new mysqli_function();
	    }


	     public function adddata($arr){
			$columns = implode(", ",array_keys($arr));
			$escaped_values = array_map('mysql_real_escape_string', array_values($arr));
			foreach ($escaped_values as $idx=>$data) $escaped_values[$idx] = "'".$data."'";
			$values  = implode(", ", $escaped_values);
			$sql = "INSERT INTO `tsr_faq_location` ($columns) VALUES ($values)";
			$query = $this->dbObj->insert_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}

	    }

	    public function listdata($id=0){
	    	if($id>0){
                $sql = "SELECT * FROM `tsr_faq_location` WHERE `city_id` = ".$id;
	    	}else{
	    		$sql = "SELECT  * FROM `tsr_faq_location` order by id DESC";
	    	}
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	    public function listeditdata($id=0){
	    	
                $sql = "SELECT * FROM `tsr_faq_location` WHERE `id` = ".$id;
	    	
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }


	    public function deletedata($id){

	    	$sql = "delete from `tsr_faq_location` WHERE `id` = ".$id;
	    	$query =  $this->dbObj->exec_query($sql, '');

	    }

	      public function updatedata($arr){

	        foreach($arr as $field=>$data)
			{
			$update[] = $field.' = \''.$data.'\'';
			}
			$sql = "UPDATE `tsr_faq_location` SET ".implode(', ',$update)." WHERE id='".$arr['id']."'";
			$query = $this->dbObj->update_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}

	    }


	    public function checkordering($order,$locid,$qn_id,$qn){
	    	if($qn_id!='' || $qn_id >0){
				$sql = "SELECT `id` FROM `tsr_faq_location` WHERE `ordering` = '".$order."' AND `city_id` = '".$locid."' AND id !='".$qn_id."'";
	    	}else{
	    		$sql = "SELECT `id` FROM `tsr_faq_location` WHERE `ordering` = '".$order."' AND `city_id` = '".$locid."' AND question !='".mysql_escape_string($qn)."'";
	    	}
	    	return $data = $this->dbObj->getAllData($sql);
	    }
	}

 ?>
 