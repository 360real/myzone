<?php
// this file for declare common variable for events page
global $recordOnePage;
global $eventmailSubjectArr;
global $eventmailAddreplayto;
global $eventmailToMail;
global $eventmailcc;
global $eventStatusArray;
 

$recordOnePage = 50;


$page =1;
$short_filed = 'CreatedDate';
$orderBy = 'ASC';
$searchArray = array();



	$tblName	=	TABLE_CITIES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/events/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";
        
$eventStatusArray = array(
                           1=> 'Under Review',
                           2 => 'Approved',
                           3 => 'LIVE !',
                           4 => 'In-Active',
    
    
                            );



 $eventmailSubjectArr = array(
    
    'create' => 'New Event Created',
    'changeStatus' => 'Event Status changed'
);

 $eventmailAddreplayto = array(
    
    'create' => 'yatendra.walia@360realtors.com',
    'changeStatus' => 'yatendra.walia@360realtors.com'
);

 $eventmailToMail = array(
    
    'create' => 'kunal.dubey@360realtors.com,himanshu.sharma@360realtors.com',
    'changeStatus' => 'kunal.dubey@360realtors.com,himanshu.sharma@360realtors.com'
); 
  
 $eventmailcc = array(
    
    'create' => 'deveteam@360realtors.com',
    'changeStatus' => 'deveteam@360realtors.com'
); 
 
 $bannerStatusArray = array(0=>'Inactive',1=>'Active',3=>'Deleted'); //Deleted are still stored in tsr_events_MainBanners
$bannerStatusColourArray = array(0 =>'#DC143C' , 1=>'#006400' );
?>
