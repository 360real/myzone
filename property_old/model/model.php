<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<input type="hidden" name="existingPropertyName" value="<?php echo($rec['property_name'])?>">
<input type="hidden" name="property_id" value="<?php echo($_GET['id'])?>">


<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable">
<thead>
<tr><th colspan="2" align="left"><h3>New Project Detail</h3></th></tr>
</thead>
<tbody>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Types</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_TYPES,"type_id","id","type",$_GET["type_id"],"id",$site['CMSURL']."property/populate_subcategries.php","CategoryDiv","CategoryLabelDiv")?></td>
</tr>

<?php if($_GET["action"]=="Update"){?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div  id="CategoryLabelDiv" name="CategoryLabelDiv"><strong><font color="#FF0000">*</font>Category</strong></div></td>
  <td><div  name="subPropertyCategoryDiv" id="CategoryDiv">
  <?php echo DropDownListAdvance(TABLE_TYPES_CATEGORIES,"cat_id","id","type_category",$_GET["type_id"],$_GET["cat_id"],"id","type_id")?>
  </div></td>
</tr>
<?php } else {?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div id="CategoryLabelDiv" name="CategoryLabelDiv"><strong><font color="#FF0000">*</font>Category</strong></div></td>
  <td><div  name="CategoryDiv" id="CategoryDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
</tr>

<?php }?>


<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Country</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_COUNTRY,"country_id","id","country",$_GET["country_id"],"id",$site['CMSURL']."property/populate_city_advance.php","cityDiv","cityLabelDiv")?></td>
</tr>

<?php if($_GET["action"]=="Update"){?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div  id="cityLabelDiv" name="cityLabelDiv"><strong><font color="#FF0000">*</font>City</strong></div></td>
  <td><div  name="cityDiv" id="cityDiv">
  <?php echo AjaxDropDownListAdvance(TABLE_CITIES,"cty_id","id","city",$_GET["country_id"],$_GET["cty_id"],"id",$site['CMSURL']."property/populate_locations_advance.php","locationDiv","locationLabelDiv","country_id");?>
 
  </div></td>
</tr>
<?php } else {?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div id="cityLabelDiv" name="cityLabelDiv"><strong><font color="#FF0000">*</font>City</strong></div></td>
  <td><div  name="cityDiv" id="cityDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
</tr>

<?php }?>


<?php if($_GET["action"]=="Update"){?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div  id="locationLabelDiv" name="locationLabelDiv"><strong><font color="#FF0000">*</font>Location</strong></div></td>
  <td><div  name="locationDiv" id="locationDiv">
  <?php echo AjaxDropDownListAdvance(TABLE_LOCATIONS,"loc_id","id","location",$_GET["cty_id"],$_GET["loc_id"],"location",$site['CMSURL']."property/populate_sublocations.php","sublocationDiv","sublocationLabelDiv","cty_id");?>
 
  </div></td>
</tr>
<?php } else {?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div id="locationLabelDiv" name="locationLabelDiv"><strong><font color="#FF0000">*</font>Location</strong></div></td>
  <td><div  name="locationDiv" id="locationDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
</tr>

<?php }?>


<?php if($_GET["action"]=="Update"){?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div  id="sublocationLabelDiv" name="sublocationLabelDiv"><strong><font color="#FF0000">*</font>Cluster</strong></div></td>
  <td><div  name="sublocationDiv" id="sublocationDiv">
  <?php echo DropDownListAdvance(TABLE_CLUSTER,"cluster_id","id","cluster",$_GET["loc_id"],$_GET["cluster_id"],"id","loc_id");?>
  </div></td>
</tr>
<?php } else {?>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><div id="sublocationLabelDiv" name="sublocationLabelDiv"><strong><font color="#FF0000">*</font>Cluster</strong></div></td>
  <td><div  name="sublocationDiv" id="sublocationDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
</tr>

<?php }?>



<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Property Name</strong></td>
  <td><input id="zipsearch" name="property_name" type="text" onkeypress="showFResult();" value="<?php echo($rec['property_name'])?>" size="41%" maxlength="120"/></td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>View Type</strong></td>
  <td>
                    <?php for ($i = 0; $i < 3; $i++) { ?>
      <input type="radio" name="viewType" value="<?php echo $i; ?>" 
             <?php if ($_GET["action"] == 'Create' && $i == 0){ ?>
             checked
             <?php } else if ($i == $rec['viewType']){  ?>
             checked
             <?php } ?>
             >
     
      <img src="<?php echo C_ROOT_URL.'view/images/propertyview'.$i.'.png' ?>" width="150px" height="100px" <?php  ?>>
      <?php } ?>
      <!--      
      <input type="radio" name="viewType" value="1" >
      
      <img src="<?php echo C_ROOT_URL.'view/images/propertyview1.png' ?>" width="150px" height="100px">
     -->
<!--     <input type="radio" name="viewType" value="2" >
      
       <img src="" width="100px" height="100px">
      <input type="radio" name="viewType" value="3" >
       <img src="" width="100px" height="100px">
      
      <input type="radio" name="viewType" value="4" >
       <img src="" width="100px" height="100px">-->
  
  
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Property Name to display</strong></td>
  <td><input name="property_name_display" type="text" value="<?php echo($rec['property_name_display'])?>" size="41%" maxlength="120"/></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Developer</strong></td>
  <td>
  <?php echo DropDownList(TABLE_DEVELOPERS,"devlpr_id","id","name",$_GET["devlpr_id"],"name")?>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Microsite Domain Name</strong></td>
  <td><input id="microsite_sef_url" name="microsite_sef_url" type="text" value="<?php echo($rec['microsite_sef_url'])?>" size="41%" maxlength="120"/></td>
</tr>


<!-- Start: Amenities Code -->

<tr class="<?php echo($clsRow1)?>">
<td align="left"><strong>Amenities :</strong></td>
<td>
<?php if($_GET["action"]=="Update"){
$addAmenities = explode(",",$rec["new_amenities"]);
$s1="SELECT id, amenities FROM tsr_amenities order by id";
$r1 = mysql_query($s1);
$rowCount = mysql_num_rows($r1);
while ($recResult = mysql_fetch_array($r1)) {?>
<div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_amenities[]" <?php
for($i=0;$i<count($addAmenities);$i++){	
if($recResult[id]==$addAmenities[$i]) {echo 'checked';}}?> value="<?php echo $recResult[id]?>" type="checkbox" align="absmiddle" style=" margin-right: 5px;
position: relative; top: 2px;" ><?php echo $recResult[amenities]?></div>
<?php }
} else {
$s1="SELECT id, amenities FROM tsr_amenities order by id";
$r1 = mysql_query($s1);
$rowCount = mysql_num_rows($r1);
while ($recResult = mysql_fetch_array($r1)) {?>
<div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_amenities[]" value="<?php echo $recResult[id]?>" checked type="checkbox" align="absmiddle" style=" margin-right: 5px; position: relative; top: 2px;"  ><?php echo $recResult[amenities]?></div>
<?php }
}?>
</td>
</tr>

<!-- End: Amenities Code -->


<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Property Overview</strong></td>
  <td><textarea class="ckeditor" name="new_overview"  id="new_overview" rows="4" cols="70" ><?php echo $rec['new_overview']?> </textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Microsite Property Overview</strong></td>
  <td><textarea class="ckeditor" name="microsite_property_overview"  id="microsite_property_overview" rows="4" cols="70" ><?php echo $rec['microsite_property_overview']?> </textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Microsite Property Overview(36 Template)</strong></td>
  <td><textarea class="ckeditor" name="microsite_property_overview_36"  id="microsite_property_overview_36" rows="4" cols="70" ><?php echo $rec['microsite_property_overview_36']?> </textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Specifications</strong></td>
  <td><textarea class="ckeditor" name="highlights"  id="highlights" rows="4" cols="70" ><?php echo $rec['highlights']?></textarea>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Location Map</strong></td>
  <td><textarea class="ckeditor" name="location_map"  id="location_map" rows="4" cols="70" ><?php echo $rec['location_map']?></textarea>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Payment Plan</strong></td>
  <td><textarea class="ckeditor" name="payment_plan"  id="payment_plan" rows="4" cols="70" ><?php echo $rec['payment_plan']?></textarea>
</tr>

<tr class="<?php echo($cls1)?>"><td width="15%" align="right"></td>
<td>

<?php if($_GET["action"]=="Update"){
$sqlImages = "SELECT * FROM ".TABLE_IMAGES." WHERE img_type='DEVELOPER_PROPERTY_BANNER_IMAGE' and propty_id=".$_GET['id'];
$recImages=($obj_mysql->getAllData($sqlImages));
for($i=0;$i< 2;$i++){
?>

<tr class="<?php echo($cls1)?>" style=" background:#fffbe4;">
    <td align="right" ><strong>Select Banner Image <?=$i+1?></strong></td>
  <input type="hidden" name="imageId[]" value="<?=$recImages[$i]['id']?>">
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
          <td width="38%"><input type="file" name="propertyImage<?=$i?>" id="propertyImage<?=$i?>" /></td>
          <td width="62%">&nbsp;&nbsp;<?php if($recImages[$i]['image']):?>
            <br />
            <img src="<?php echo  ($site['PROPERTYBANNERURL'].$rec['id']."/developer_banners/".$recImages[$i]['image'])?>"   height="60" width="100" border="0" /> <?php endif;?>
            <input type="hidden" name="old_file_name_propertyImage<?=$i?>" value="<?php echo ($recImages[$i]['image'])?>" />
            <br /></td>
  
        </tr>
      </table></td>
  </tr>

    <?php
    }
} else {
for($i=0;$i< 2;$i++){
?>

<tr class="<?php echo($cls1)?>" style=" background:#fffbe4;">
    <td align="right" ><strong>Select Banner Image <?php echo $i+1?></strong></td>
  <input type="hidden" name="imageId[]">
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
          <td width="38%"><input type="file" name="propertyImage<?=$i?>" id="propertyImage<?=$i?>" /></td>
          <input type="hidden" name="old_file_name_propertyImage<?=$i?>"  />
         </td>
        </tr>
      </table></td>
  </tr>
  
<?php }
}
?>

</td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Project Status</strong></td>
  <td>
  <?php echo DropDownListWithoutMust("tsr_project_status","project_status_id","id","name",$_GET["project_status_id"],"name")?>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Is NRI</strong></td>
 <input type='hidden' name='is_nri' value='0'>
  <td><input name="is_nri" type="checkbox"  value="1" <?php if(isset($rec['is_nri']) && $rec['is_nri']==1) echo "checked=checked"; ?>  size="41%" maxlength="120"/>Yes</td>
 
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Is Platinum</strong></td>
 <input type='hidden' name='is_platinum' value='0'>
  <td><input name="is_platinum" type="checkbox"  value="1" <?php if(isset($rec['is_platinum']) && $rec['is_platinum']==1) echo "checked=checked"; ?>  size="41%" maxlength="120"/>Yes</td>
 
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>NRI tag line</strong></td>
 
  <td><input name="nri_tagline" type="text"  value="<?php echo($rec['nri_tagline'])?>"  size="41%" maxlength="120"/></td>
 
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>NRI Type</strong></td> 
  <td><select name="nri_type">
	<option value="">Select </option>
	<option value="1"  <?php if(isset($rec['nri_type']) && $rec['nri_type']==1){ echo "selected=selected"; }?> >Holiday Home</option>	
	<option value="2"  <?php if(isset($rec['nri_type']) && $rec['nri_type']==2){ echo "selected=selected"; }?> >ROI/High Return Property</option> 
  </select></td>
  
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Launch Date</strong></td>
  <td><?php echo getLaunchDateMonth(TABLE_PROPERTIES, "ldMonth", "launch_date",$_GET["launch_date"], $rec['property_name'])?> <?php echo getLaunchDateYear(TABLE_PROPERTIES, "ldYear", "launch_date",$_GET["launch_date"], $rec['property_name'])?></td> 
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Possession Date</strong></td>
  <td><?php echo getPossessionDateMonth(TABLE_PROPERTIES, "pdMonth", "possession_date",$_GET["possession_date"], $rec['property_name'])?> <?php echo getPossessionDateYear(TABLE_PROPERTIES, "pdYear", "possession_date",$_GET["possession_date"], $rec['property_name'])?></td>
</tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Booking Status</strong></td>
  <td>
  <input type="radio" name="booking_status" value="1" id="1" checked="checked" /> 
	<label for="1">Open</label> 
	<input type="radio" name="booking_status" value="0" id="0" <?php if ($rec['booking_status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Closed</label>
  </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
    <td align="left"><strong>Project USP</strong></td>
    <td><input name="project_usp" type="text" value="<?php echo($rec['project_usp']) ?>" size="80%" maxlength="200"/></td>
</tr>
<tr class="<?php echo($clsRow1) ?>">
  <td align="left"><strong>Project Area</strong></td>
  <td><input name="project_area" type="text" title="project_area" value="<?php echo($rec['project_area'])?>" size="80%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Bedrooms To Display </strong></td>
  <td><input name="bedrooms" type="text" title="Bedrooms To Display"  value="<?php echo($rec['bedrooms'])?>" size="80%" maxlength="120" /> <font color="#FF0000" >  [   1,2,3 Bhk   ]</font></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>longitude</strong></td>
  <td><input name="lon" type="text" title="lon" value="<?php echo($rec['lon'])?>" size="80%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Latitude</strong></td>
  <td><input name="lat" type="text" title="lat" value="<?php echo($rec['lat'])?>" size="80%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>"><td align="left" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" /> 
	<label for="1">Active</label> 
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status']=="0"){ echo("checked='checked'");} ?>/> 
	<label for="0">Inactive</label>
	</td>
 </tr>

 <tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Rating</strong></td>
  <td>
  <select id="rating" name="rating">
  <option value="0">--Rating--</option>
  <option value="1" <?php if($rec["rating"]=="1"){?>selected<?php } ?>>1</option>
  <option value="1.5"  <?php if($rec["rating"]=="1.5"){?>selected<?php } ?>>1.5</option>
  <option value="2"  <?php if($rec["rating"]=="2"){?>selected<?php } ?>>2</option>
  <option value="2.5"  <?php if($rec["rating"]=="2.5"){?>selected<?php } ?>>2.5</option>
  <option value="3"  <?php if($rec["rating"]=="3"){?>selected<?php } ?>>3</option>
  <option value="3.5"  <?php if($rec["rating"]=="3.5"){?>selected<?php } ?>>3.5</option>
  <option value="4"  <?php if($rec["rating"]=="4"){?>selected<?php } ?>>4</option>
  <option value="4.5"  <?php if($rec["rating"]=="4.5"){?>selected<?php } ?>>4.5</option>
  <option value="5"  <?php if($rec["rating"]=="5"){?>selected<?php } ?>>5</option>
  </select>
  
  </td>
</tr>



<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Home Page Ordering</strong></td>
  <td><input name="hot_ordering" autocomplete="off" id="hot_ordering" type="text"  value="<?php if($_GET["action"]=="Update" && $rec['hot_ordering']>0) echo $rec['hot_ordering']; else echo "1000"; ?>" size="80%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>City Page Ordering</strong></td>
  <td><input name="city_ordering" autocomplete="off" id="city_ordering" type="text"  value="<?php if($_GET["action"]=="Update" && $rec['city_ordering']>0) echo $rec['city_ordering']; else echo "1000"; ?>" size="80%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Location Page Ordering</strong></td>
  <td><input name="location_ordering" autocomplete="off" id="location_ordering" type="text"  value="<?php if($_GET["action"]=="Update" && $rec['location_ordering']>0) echo $rec['location_ordering']; else echo "1000"; ?>" size="80%" maxlength="120" /></td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Meta Title</strong></td>
  <td><textarea name="meta_title"  id="meta_title" rows="3" cols="150" ><?php echo $rec['meta_title']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Meta Description</strong></td>
  <td><textarea name="meta_description"  id="meta_description" rows="3" cols="150" ><?php echo $rec['meta_description']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Meta Keyword</strong></td>
  <td><textarea name="meta_keywords"  id="meta_keywords" rows="3" cols="150" ><?php echo $rec['meta_keywords']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Micrtosite Meta Title</strong></td>
  <td><textarea name="microsite_meta_title"  id="microsite_meta_title" rows="3" cols="150" ><?php echo $rec['microsite_meta_title']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Micrtosite Meta Description</strong></td>
  <td><textarea name="microsite_meta_description"  id="microsite_meta_description" rows="3" cols="150" ><?php echo $rec['microsite_meta_description']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Micrtosite Meta Keyword</strong></td>
  <td><textarea name="microsite_meta_keywords"  id="microsite_meta_keywords" rows="3" cols="150" ><?php echo $rec['microsite_meta_keywords']?></textarea>
</tr>


<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Location Micrtosite Meta Title</strong></td>
  <td><textarea name="location_microsite_meta_title"  id="location_microsite_meta_title" rows="3" cols="150" ><?php echo $rec['location_microsite_meta_title']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Location Micrtosite Meta Description</strong></td>
  <td><textarea name="location_microsite_meta_description"  id="location_microsite_meta_description" rows="3" cols="150" ><?php echo $rec['location_microsite_meta_description']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Location Micrtosite Meta Keyword</strong></td>
  <td><textarea name="location_microsite_meta_keywords"  id="location_microsite_meta_keywords" rows="3" cols="150" ><?php echo $rec['location_microsite_meta_keywords']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Devloper Highlight Text 1</strong></td>
  <td><input name="dev_highlight_text1" type="text" value="<?php echo($rec['dev_highlight_text1'])?>" size="41%" maxlength="120"/></td>
<tr class="<?php echo($clsRow1)?>">

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Devloper Highlight Text 2</strong></td>
  <td><input name="dev_highlight_text2" type="text" value="<?php echo($rec['dev_highlight_text2'])?>" size="41%" maxlength="120"/></td>
<tr class="<?php echo($clsRow1)?>">

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Devloper Highlight Text 3</strong></td>
  <td><input name="dev_highlight_text3" type="text" value="<?php echo($rec['dev_highlight_text3'])?>" size="41%" maxlength="120"/></td>

<!--*************************** Property Distance Code ***************************-->
<?php 
function distanceUnits($name, $rec)
{ ?>
   <select name="<?php echo $name; ?>">
    <option <?php if ($rec[$name] == '') { ?> selected <?php } ?> value="">--</option>
    <option <?php if ($rec[$name] == 'KM') { ?> selected <?php } ?> value="KM">KM</option>
    <option <?php if ($rec[$name] == 'Mtr') { ?> selected <?php } ?> value="Mtr">Mtr</option>
    <option <?php if ($rec[$name] == 'Hrs') { ?> selected <?php } ?> value="Hrs">Hrs</option>
    <option <?php if ($rec[$name] == 'Min') { ?> selected <?php } ?> value="Min">Min</option>
  </select>
        <?php
        }

        function setPriority($name, $rec) {
       
            ?>
    <select name="<?php echo $name; ?>">
    <?php for ($i = 1; $i < 11; $i++) { ?>
            <option  value="<?php echo $i; ?>"
                     <?php if (count($rec) == 0){ 
                         if ($i == 10){ ?>
                      selected       
           <?php              }
                     } else {
                         
                         if ($rec[$name] == null){
                           
                             $priroty = 10;
                             
                         } else {
                             
                              $priroty = $rec[$name];
                         }
                         
                      if ($priroty == $i) {  ?>
                       selected  
                      <?php  }         }
        ?>
                     
                     
                     
                     ><?php echo $i; ?></option>
<?php } ?>
    </select>
            <?php }
        ?>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Nearest Airport </strong> </td>
  <td>
        <strong>Display Priroty</strong>:
        <?php setPriority("airportDistancePriroty", $rec); ?>
        <input name="airport_distance" type="text" value="<?php echo($rec['airport_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("airportDistanceUnit", $rec); ?>
         <input name="airport_name" type="text" value="<?php echo($rec['airport_name']) ?>" size="25%" placeholder="1st Airport Name"/>
        
         <input name="airport_distance1" type="text" value="<?php echo($rec['airport_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("airportDistanceUnit1", $rec); ?>
         <input name="airport_name1" type="text" value="<?php echo($rec['airport_name1']) ?>" size="25%" placeholder="2nd Airport Name"/>
        
         <input name="airport_distance2" type="text" value="<?php echo($rec['airport_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("airportDistanceUnit2", $rec); ?>
         <input name="airport_name2" type="text" value="<?php echo($rec['airport_name2']) ?>" size="25%" placeholder="3rd Airport Name"/>
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Nearest Railway Station</strong></td>
  <td>    <strong>Display Priroty</strong>:
        <?php setPriority("railwayStationPriroty", $rec); ?>
        <input name="railway_station_distance" type="text" value="<?php echo($rec['railway_station_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("railwayStationDistanceUnit", $rec); ?>
        <input name="railway_station_name" type="text" value="<?php echo($rec['railway_station_name']) ?>" size="25%" placeholder="1st Railway Station Name"/>
        
        <input name="railway_station_distance1" type="text" value="<?php echo($rec['railway_station_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("railwayStationDistanceUnit1", $rec); ?>
         <input name="railway_station_name1" type="text" value="<?php echo($rec['railway_station_name1']) ?>" size="25%" placeholder="2nd Railway Station Name"/>
        
         
         <input name="railway_station_distance2" type="text" value="<?php echo($rec['railway_station_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("railwayStationDistanceUnit2", $rec); ?>
         <input name="railway_station_name2" type="text" value="<?php echo($rec['railway_station_name2']) ?>" size="25%" placeholder="3rd Railway Station Name"/>
        
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Nearest Metro Station</strong></td>
  <td>
        <strong>Display Priroty</strong>:
        <?php setPriority("metroStationDistancePriroty", $rec); ?>
        <input name="metro_station_distance" type="text" value="<?php echo($rec['metro_station_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("metroStationDistanceUnit", $rec); ?>
        <input name="metro_station_name" type="text" value="<?php echo($rec['metro_station_name']) ?>" size="25%" placeholder="1st Metro Station Name"/>
       
        <input name="metro_station_distance1" type="text" value="<?php echo($rec['metro_station_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("metroStationDistanceUnit1", $rec); ?>
        <input name="metro_station_name1" type="text" value="<?php echo($rec['metro_station_name1']) ?>" size="25%" placeholder="2nd Metro Station Name"/>
        
        <input name="metro_station_distance2" type="text" value="<?php echo($rec['metro_station_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("metroStationDistanceUnit2", $rec); ?>
        <input name="metro_station_name2" type="text" value="<?php echo($rec['metro_station_name2']) ?>" size="25%" placeholder="3rd Metro Station Name"/>
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Nearest Bus Stop</strong></td>
  <td>     <strong>Display Priroty</strong>:
        <?php setPriority("busStopDistancePriroty", $rec); ?>
         <input name="busstop_distance" type="text" value="<?php echo($rec['busstop_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("busStopDistanceUnit", $rec); ?>
         <input name="busstop_name" type="text" value="<?php echo($rec['busstop_name']) ?>" size="25%"  placeholder="1st Bus Stop Name"/>
          
         <input name="busstop_distance1" type="text" value="<?php echo($rec['busstop_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("busStopDistanceUnit1", $rec); ?>
         <input name="busstop_name1" type="text" value="<?php echo($rec['busstop_name1']) ?>" size="25%"  placeholder="2nd Bus Stop Name"/>
         
         <input name="busstop_distance2" type="text" value="<?php echo($rec['busstop_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("busStopDistanceUnit2", $rec); ?>
         <input name="busstop_name2" type="text" value="<?php echo($rec['busstop_name2']) ?>" size="25%"  placeholder="3rd Bus Stop Name"/>
    
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Nearest Highway</strong></td>
  <td>    <strong>Display Priroty</strong>:
        <?php setPriority("highwaydistancePriroty", $rec); ?>
        <input name="highwaydistance" type="text" value="<?php echo($rec['highwaydistance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("highwaydistanceUnit", $rec); ?>
         <input name="highway_name" type="text" value="<?php echo($rec['highway_name']) ?>" size="25%"  placeholder="1st Highway Name"/>
         
         <input name="highwaydistance1" type="text" value="<?php echo($rec['highwaydistance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("highwaydistanceUnit1", $rec); ?>
         <input name="highway_name1" type="text" value="<?php echo($rec['highway_name1']) ?>" size="25%"  placeholder="2nd Highway Name"/>
        
         <input name="highwaydistance2" type="text" value="<?php echo($rec['highwaydistance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("highwaydistanceUnit2", $rec); ?>
         <input name="highway_name2" type="text" value="<?php echo($rec['highway_name2']) ?>" size="25%"  placeholder="3rd Highway Name"/>
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left">
<strong>Nearest Schools/Colleges</strong></td>
  <td>
        <strong>Display Priroty</strong>:
        <?php setPriority("schoolCollegeDistancePriroty", $rec); ?>
         <input name="schoolCollege_distance" type="text" value="<?php echo($rec['schoolCollege_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("schoolCollegeDistanceUnit", $rec); ?>
        <input name="schoolCollege_name" type="text" value="<?php echo($rec['schoolCollege_name']) ?>" size="25%"  placeholder="1st School/College Name"/>
         
        <input name="schoolCollege_distance1" type="text" value="<?php echo($rec['schoolCollege_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("schoolCollegeDistanceUnit1", $rec); ?>
        <input name="schoolCollege_name1" type="text" value="<?php echo($rec['schoolCollege_name1']) ?>" size="25%"  placeholder="2nd School/College Name"/>
        
        <input name="schoolCollege_distance2" type="text" value="<?php echo($rec['schoolCollege_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("schoolCollegeDistanceUnit2", $rec); ?>
        <input name="schoolCollege_name2" type="text" value="<?php echo($rec['schoolCollege_name2']) ?>" size="25%"  placeholder="3rd School/College Name"/>
    
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Nearest Hospital</strong></td>
  <td>   <strong>Display Priroty</strong>:
        <?php setPriority("hospitalDistancePriroty", $rec); ?>
        <input name="hospital_distance" type="text" value="<?php echo($rec['hospital_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("hospitalDistanceUnit", $rec); ?>
        <input name="hospital_name" type="text" value="<?php echo($rec['hospital_name']) ?>" size="25%"  placeholder="1st Hospital Name"/>
    
        <input name="hospital_distance1" type="text" value="<?php echo($rec['hospital_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("hospitalDistanceUnit1", $rec); ?>
        <input name="hospital_name1" type="text" value="<?php echo($rec['hospital_name1']) ?>" size="25%"  placeholder="2nd Hospital Name"/>
    
        <input name="hospital_distance2" type="text" value="<?php echo($rec['hospital_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("hospitalDistanceUnit2", $rec); ?>
        <input name="hospital_name2" type="text" value="<?php echo($rec['hospital_name2']) ?>" size="25%"  placeholder="3rd Hospital Name"/>
    
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Nearest Mall</strong></td>
  <td>   <strong>Display Priroty</strong>:
        <?php setPriority("mallDistancePriroty", $rec); ?>
        <input name="mall_distance" type="text" value="<?php echo($rec['mall_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("mallDistanceUnit", $rec); ?>
        <input name="mall_name" type="text" value="<?php echo($rec['mall_name']) ?>" size="25%"  placeholder="1st Mall Name"/>
        
        
        <input name="mall_distance1" type="text" value="<?php echo($rec['mall_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("mallDistanceUnit1", $rec); ?>
        <input name="mall_name1" type="text" value="<?php echo($rec['mall_name1']) ?>" size="25%"  placeholder="2nd Mall Name"/>
        
        <input name="mall_distance2" type="text" value="<?php echo($rec['mall_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("mallDistanceUnit2", $rec); ?>
        <input name="mall_name2" type="text" value="<?php echo($rec['mall_name2']) ?>" size="25%"  placeholder="3rd Mall Name"/>
    </td>
</tr>
<tr class="<?php echo($clsRow1) ?>">
    
    <td align="left"><strong>Nearest Bank/ATM</strong></td>
    <td>
         <strong>Display Priroty</strong>:
        <?php setPriority("bankAtmDistancePriroty", $rec); ?>
         <input name="bankAtm_distance" type="text" value="<?php echo($rec['bankAtm_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("bankAtmDistanceUnit", $rec); ?>
       <input name="bankAtm_name" type="text" value="<?php echo($rec['bankAtm_name']) ?>" size="25%"  placeholder="1st ATM/Bank Name"/>
       
       <input name="bankAtm_distance1" type="text" value="<?php echo($rec['bankAtm_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("bankAtmDistanceUnit1", $rec); ?>
       <input name="bankAtm_name1" type="text" value="<?php echo($rec['bankAtm_name1']) ?>" size="25%"  placeholder="2nd ATM/Bank Name"/>
       
       <input name="bankAtm_distance2" type="text" value="<?php echo($rec['bankAtm_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("bankAtmDistanceUnit2", $rec); ?>
       <input name="bankAtm_name2" type="text" value="<?php echo($rec['bankAtm_name2']) ?>" size="25%"  placeholder="3rd ATM/Bank Name"/>
       
    </td>
</tr>
<tr class="<?php echo($clsRow1)?>">

    <td align="left"><strong>Nearest Restaurants</strong></td>
    <td>
         <strong>Display Priroty</strong>:
        <?php setPriority("restaurantsDistancePriroty", $rec); ?>
         <input name="restaurants_distance" type="text" value="<?php echo($rec['restaurants_distance']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("restaurantsDistanceUnit", $rec); ?>
       <input name="restaurants_name" type="text" value="<?php echo($rec['restaurants_name']) ?>" size="25%"  placeholder="1st Restaurants Name"/>
   
        <input name="restaurants_distance1" type="text" value="<?php echo($rec['restaurants_distance1']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("restaurantsDistanceUnit1", $rec); ?>
       <input name="restaurants_name1" type="text" value="<?php echo($rec['restaurants_name1']) ?>" size="25%"  placeholder="2nd Restaurants Name"/>
       
       <input name="restaurants_distance2" type="text" value="<?php echo($rec['restaurants_distance2']) ?>" size="10%" maxlength="5" placeholder="Distance"/>
        <?php distanceUnits("restaurantsDistanceUnit2", $rec); ?>
       <input name="restaurants_name2" type="text" value="<?php echo($rec['restaurants_name2']) ?>" size="25%"  placeholder="3rd Restaurants Name"/>
    
    
    </td>
</tr>



                              <!--******************* Property Distance Code ends **************-->

<tr class="<?php echo($clsRow1)?>">

  <td align="left"><strong>Devloper Meta Title</strong></td>
  <td><textarea name="dev_meta_title"  id="dev_meta_title" rows="3" cols="150" ><?php echo $rec['dev_meta_title']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Devloper Meta Description</strong></td>
  <td><textarea name="dev_meta_description"  id="dev_meta_description" rows="3" cols="150" ><?php echo $rec['dev_meta_description']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong>Devloper Meta Keyword</strong></td>
  <td><textarea name="dev_meta_keywords"  id="dev_meta_keywords" rows="3" cols="150" ><?php echo $rec['dev_meta_keywords']?></textarea>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Show Type</strong></td>
  <td>
  <input type="radio" name="is_show_type" value="1" id="1" checked="checked" /> 
  <label for="1">Both</label> 
  <input type="radio" name="is_show_type" value="2" id="2" <?php if ($rec['is_show_type']=="2"){ echo("checked='checked'");} ?>/> 
  <label for="2">CRM</label>
  <input type="radio" name="is_show_type" value="3" id="3" <?php if ($rec['is_show_type']=="3"){ echo("checked='checked'");} ?>/> 
  <label for="3">CMS</label>
  </td>
</tr>

<tr class="<?php echo($clsRow1)?>">
  <td align="left"><strong><font color="#FF0000">*</font>Unit Type</strong></td>
  <td>
  <input type="radio" name="unit_type" value="1" id="1" checked="checked" /> 
  <label for="1">SQ.FT</label> 
  <input type="radio" name="unit_type" value="2" id="2" <?php if ($rec['unit_type']=="2"){ echo("checked='checked'");} ?>/> 
  <label for="2">SQ.YD</label>
  <input type="radio" name="unit_type" value="3" id="3" <?php if ($rec['unit_type']=="3"){ echo("checked='checked'");} ?>/> 
  <label for="3">SQ.M</label>
  </td>
</tr>



<!-- *************************** new code added *****************************-->

<?php 
if ($action=="Update") {
  //$sql="SELECT splproj.*, prop.property_name from ".TABLE_SPECIALS_PROJECTS." splproj  inner join ".TABLE_PROPERTIES." prop on splproj.proj_id=prop.id where splproj.special_id=".$_GET['id']." " ;

  $sql="SELECT rera.rera_no,rera.id FROM ".TABLE_PROPERTIES_RERA." as rera  where rera.prop_id=".$_GET['id']."  ";
  
  $reca=($obj_mysql->getAllData($sql));

   $countOfrec=count($reca);

 

 if($reca!="")
 {
   $upCount=1;
   ?>

<?php 
foreach($reca as $pecialproj)
{

if($upCount==1)
{
  ?>


<tr class="<?php echo($clsRow1)?>" id="replacetr">
  <td align="right"><strong>Rera <?php echo $upCount; ?></strong></td>
  <td><input name="rera_no[]" id="text_<?php echo $upCount; ?>" data=""  type="text" title="project1" value="<?php echo($pecialproj['rera_no'])?>" size="50%" maxlength="120" />
<button  class="add" onclick="addMoreRows(this); return false;"></button>
  <div id="hiddenProjId"><input  type="hidden" name="project_id[]" value="<?php echo($pecialproj['devlpr_id'])?>"></div>

  </tr>

<?php } else {?>

<tr class="<?php echo($clsRow1)?>" id="replacetr">
  <td align="right"><strong>Rera <?php echo $upCount; ?></strong></td>
  <td><input name="rera_no[]" id="text_<?php echo $upCount; ?>" data=""  type="text" title="project1"  value="<?php echo($pecialproj['rera_no'])?>" size="50%" maxlength="120" /> <button class="delete" onclick="deleteRow(<?php echo  $pecialproj['id'] ?>, this); return false;"></button>
<div id="hiddenProjId"><input  type="hidden" name="project_id[]" value="<?php echo($pecialproj['devlpr_id'])?>"></div>
  </tr>
  

<?php } ?>

 
  
<?php $upCount++;  }  ?>



<?php } else {   ?>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Rera 1</strong></td>
  <td><input name="rera_no[]" id="text_1" data=""  type="text" title="project1" value="<?php echo($rec['project1'])?>" size="50%" maxlength="120" />
  <button class="add" onclick="addMoreRows(this); return false;"></button></td>

<?php }

}


else {
?>

<tr class="<?php echo($clsRow1)?>">
  <td align="right"><strong>Rera 1</strong></td>
  <td><input id="text_1" data=""  type="text" title="project1" name="rera_no[]" value="<?php echo($rec['project1'])?>" size="50%" maxlength="120" />
  <button class="add" onclick="addMoreRows(this); return false;"></button></td> 


<?php } 

?>

<tr class="clsRow1uuu" id="newRows"></tr>




<!-- **************************************************new code added ********************************* -->


<tr class="<?php echo($clsRow2)?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>





</tbody>
</table>
</form>
<link href="<?echo C_ROOT_URL?>/view/css/auto-search.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="<?echo C_ROOT_URL?>/view/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?echo C_ROOT_URL?>/view/js/jquery-ui-1.8.2.custom.min.js"></script>
<script type="text/javascript"> 
var urlSent = ""; 
function showFResult()
{
urlSent=document.getElementById("cty_id").value;

$(function() {

var urlSet = "http://mypanel.buyproperty.com/property_search.php?cityId="+urlSent;
$("#zipsearch").autocomplete({ 
source: urlSet,
minLength: 2,

html: true, // optional (jquery.ui.autocomplete.html.js required)

// optional (if other layers overlap autocomplete list)
open: function(event, ui) {
	
$(".ui-autocomplete").css("z-index", 1000);
}
});

});
}
</script> 


<?php 
if($action=="Update" && $reca!="")
{

  $count=$countOfrec+1;
}
else
{
  $count=2;
}
?>




<script type="text/javascript">



var rowCount = <?php echo $count; ?>;
function addMoreRows(row) {

  var recRow = '<tr id="newRow_'+rowCount+'" class="clsRow1"><td align="right"><strong>Rera '+rowCount+'</strong></td><td><input data="" id="text_'+rowCount+'" type="text" name="rera_no[]" size=50% maxlength="120"><button onclick="removeRow(this); return false;">Remove</button></td></tr>';

jQuery('#newRows').before(recRow);
rowCount ++;
}

function removeRow(removeNum) {

  console.log(removeNum);
  var test=$(removeNum).parent().parent();
  console.log(test);
$(test).remove();
}
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

 <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>



<script>


 function test(id)
  {

var base_url="<?php echo C_ROOT_URL ?>";

  cityId="";
//console.log(list);
    
  $(function() {
    //var projectlist=list;

      $("#"+id).autocomplete({
      minLength: 0,
      source:base_url+"/property/view/delfRecord.php?cityId="+cityId,
      focus: function( event, ui ) {
        $( "#"+id ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        $( "#"+id ).val( ui.item.value );

        $( "#"+id ).val( ui.item.label );
        $("#"+id).parent().find("#hiddenProjId").remove();

        $("#"+id).parent().find("#newhidden").remove();
          //$("#"+id).find("#hiddenProjId").remove();
        // /console.log($("#"+id ).closest('td:input').next().attr('data',ui.item.value));
        $( "#"+id ).append('<input id="newhidden" type="hidden" name="prop_id[]" value="'+ui.item.value+'">');
        //$( "#"+id ).attr('name',ui.item.value);
        //$( ".project-icon" ).attr( "src", "images/" + ui.item.icon );
 
        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label +"</a>" )
        .appendTo( ul );
    };
  });

}
  </script>


<script>
function deleteRow(ID, thisref)
{
  
if(confirm("Are you sure you want to delete this record"))
{
var base_url="<?php echo C_ROOT_URL ?>";

$(thisref).parent().parent().remove();


  $.ajax({
        //url: "view/changepoststatus.php",
        url:base_url+"/property/view/delRecord.php",
        type: 'POST',
        data: {rec_id :ID},
        success: function(data) {
             
          // location.href=base_url+"/specials/index.php";
          //jQuery("#rec_"+recid).replaceWith(data);
            
        }
    });
}


}
</script>
