<?php $templateType = $_GET['type'];?>
<link href="<?php echo $site['URL'] ?>view/css/calendar.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="<?php echo $site['URL'] ?>view/js/calendar_us.js"></script>
<script language="JavaScript" src="<?php echo $site['URL'] ?>view/js/jscolor.js"></script>
<form  method="post"  name="form123" onsubmit="return validateForm(this);" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="2" class="MainTable" >
<thead>
<tr><th colspan="2" align="left"><h3>Microsite Manager</h3></th></tr>
</thead>
<tbody>


<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><strong><font color="#FF0000">*</font>Cities</strong></td>
  <td><?php echo AjaxDropDownList(TABLE_CITIES, "cty_id", "id", "city", $_GET["cty_id"], "id", $site['CMSURL'] . "microsites_new/populate_locations_advance.php", "locationDiv", "locationLabelDiv") ?></td>
</tr>

<?php if ($_GET["action"] == "Update") {?>
<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><div  id="locationLabelDiv" name="locationLabelDiv"><strong><font color="#FF0000">*</font>Location</strong></div></td>
  <td><div  name="locationDiv" id="locationDiv">
  <?php echo AjaxDropDownListAdvance(TABLE_LOCATIONS, "loc_id", "id", "location", $_GET["cty_id"], $_GET["loc_id"], "id", $site['CMSURL'] . "microsites_new/populate_properties.php", "sublocationDiv", "sublocationLabelDiv", "cty_id"); ?>

  </div></td>
</tr>
<?php } else {?>
<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><div id="locationLabelDiv" name="locationLabelDiv"><strong><font color="#FF0000">*</font>Location</strong></div></td>
  <td><div  name="locationDiv" id="locationDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
</tr>

<?php }?>

<?php if ($_GET["action"] == "Update") {?>
<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><div  id="sublocationLabelDiv" name="sublocationLabelDiv"><strong><font color="#FF0000">*</font>Property Name</strong></div></td>
  <td><div  name="sublocationDiv" id="sublocationDiv">
  <?php echo DropDownListAdvance(TABLE_PROPERTIES, "propty_id", "id", "property_name", $_GET["loc_id"], $_GET["propty_id"], "property_name", "loc_id") ?>
  </div></td>
</tr>
<?php } else {?>
<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><div id="sublocationLabelDiv" name="sublocationLabelDiv"><strong><font color="#FF0000">*</font>Property Name</strong></div></td>
  <td><div  name="sublocationDiv" id="sublocationDiv"><select style="width:234px;"><option>-- Select --</option></select></div></td>
</tr>

<?php }?>

<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><strong><font color="#FF0000">*</font>Select Template</strong></td>
  <td></td>

</tr>

<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><strong><font color="#FF0000">*</font>Domain Name</strong></td>
  <td><input name="domain_name" type="text" title="Domain Name" lang='MUST' value="<?php echo ($rec['domain_name']) ?>" size="50%" maxlength="120" /></td>
</tr>



<tr class="<?php echo ($cls2) ?>">
  <td align="right" ><strong>Overview Title</strong></td>
  <td><textarea name="overview_title"  id="overview_title" rows="8" cols="70"><?php echo ($rec['overview_title']) ?></textarea>  </td>
</tr>

<tr class="<?php echo ($clsRow1) ?>">
  <td align="right"><strong>Overview</strong></td>
  <td><textarea class="ckeditor" name="overview"  id="overview" rows="8" cols="70" title="Residential Content" ><?php echo $rec['overview'] ?></textarea>
</tr>

<!-- Start: Amenities Code -->

<tr class="<?php echo($clsRow1)?>">
<td align="left"><strong>Amenities :</strong></td>
<td>
<?php if($_GET["action"]=="Update"){
$addAmenities = explode(",",$rec["new_amenities"]);
$s1="SELECT id, amenities FROM tsr_amenities order by id";
$r1 = mysql_query($s1);
$rowCount = mysql_num_rows($r1);
while ($recResult = mysql_fetch_array($r1)) {?>
<div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_amenities[]" <?php
for($i=0;$i<count($addAmenities);$i++){ 
if($recResult[id]==$addAmenities[$i]) {echo 'checked';}}?> value="<?php echo $recResult[id]?>" type="checkbox" align="absmiddle" style=" margin-right: 5px;
position: relative; top: 2px;" ><?php echo $recResult[amenities]?></div>
<?php }
} else {
$s1="SELECT id, amenities FROM tsr_amenities order by id";
$r1 = mysql_query($s1);
$rowCount = mysql_num_rows($r1);
while ($recResult = mysql_fetch_array($r1)) {?>
<div style="font: 12px Arial,Helvetica,sans-serif; padding-top: 10px;width: 220px; float: left;"><input name="new_amenities[]" value="<?php echo $recResult[id]?>" checked type="checkbox" align="absmiddle" style=" margin-right: 5px; position: relative; top: 2px;"  ><?php echo $recResult[amenities]?></div>
<?php }
}?>
</td>
</tr>

<!-- End: Amenities Code -->


<tr class="<?php echo ($cls2) ?>">
  <td align="right" ><strong>Google Analytics Code</strong></td>
  <td><textarea name="google_analytics_code" id="google_analytics_code" rows="8" cols="70"><?php echo ($rec['google_analytics_code']) ?></textarea>  </td>
</tr>
<tr class="<?php echo ($cls2) ?>">
  <td align="right" ><strong>Google Conversion Code</strong></td>
  <td><textarea name="google_conversion_code" id="google_conversion_code" rows="8" cols="70"><?php echo ($rec['google_conversion_code']) ?></textarea>  </td>
</tr>

<tr class="<?php echo ($cls2) ?>">
  <td align="right" ><strong>Google Verification Code</strong></td>
  <td><textarea name="google_verification_code" id="google_verification_code" rows="8" cols="70"><?php echo ($rec['google_verification_code']) ?></textarea>  </td>
</tr>

<tr class="<?php echo ($cls2) ?>">
  <td align="right" ><strong>Google Remarking Code</strong></td>
  <td><textarea name="google_remarking_code"  id="google_remarking_code" rows="8" cols="70"><?php echo ($rec['google_remarking_code']) ?></textarea>  </td>
</tr>

<tr class="<?php echo ($cls1) ?>"><td width="15%" align="right"><strong>Display Email</strong></td>
<td><input type="text" name="display_email" title="Email" maxlength="120" value="<?php echo ($rec['display_email']) ?>" size="40" /></td></tr>

<tr class="<?php echo ($cls1) ?>"><td width="15%" align="right"><strong>Display Mobile 1</strong></td>
<td><input type="text" name="display_mobile1" title="Mobile" maxlength="120" value="<?php echo ($rec['display_mobile1']) ?>" size="40" /></td></tr>



<?php if ($_GET["action"] == "Update") {
    $sqlImages = "SELECT * FROM " . TABLE_IMAGES . " WHERE img_type='MICROSITE_TEMPLATE_IMAGE' and microsite_template_id=" . $_GET['id'];
    $recImages = ($obj_mysql->getAllData($sqlImages));
    for ($i = 0; $i < 3; $i++) {
        ?>

<tr class="<?php echo ($cls1) ?>" style=" background:#fffbe4;">
    <td align="right" ><strong>Select Poperty Image</strong></td>
	<input type="hidden" name="imageId[]" value="<?=$recImages[$i]['id']?>">
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
          <td width="38%"><input type="file" name="propertyImage<?=$i?>" id="propertyImage<?=$i?>" /></td>
          <td width="62%">&nbsp;&nbsp;<?php if ($recImages[$i]['image']): ?>
            <br />
            <img src="<?php echo ($site['MICROSITEIMAGEURL'] . $rec['id'] . "/" . $recImages[$i]['image']) ?>"   height="60" width="100" border="0" /> <?php endif;?>
            <input type="hidden" name="old_file_name_propertyImage<?=$i?>" value="<?php echo ($recImages[$i]['image']) ?>" />
            <br /></td>

        </tr>
      </table></td>
  </tr>

<?php }
} else {
    for ($i = 0; $i < 3; $i++) {
        ?>

<tr class="<?php echo ($cls1) ?>" style=" background:#fffbe4;">
    <td align="right" ><strong>Select Poperty Image</strong></td>
	<input type="hidden" name="imageId[]">
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
          <td width="38%"><input type="file" name="propertyImage<?=$i?>" id="propertyImage<?=$i?>" /></td>
          <input type="hidden" name="old_file_name_propertyImage<?=$i?>"  />
         </td>
        </tr>
      </table></td>
  </tr>
<?php }
}?>

</td>
</tr>


<tr class="<?php echo ($cls1) ?>">
    <td align="right" ><strong>Select Project Logo</strong></td>
    <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="38%"><input type="file" <?php if ($rec['property_logo'] == '') {?> title="Select Project Logo" <?php }?> name="property_logo" id="property_logo" /></td>
          <td width="62%"><?php if ($rec['property_logo']): ?>
            <br />
            <img src="<?php echo ($site['MICROSITEIMAGEURL'] . $rec['id'] . "/property_logo/" . $rec['property_logo']) ?>"   border="0" height="100" width="100" />
            <?php endif;?>
            <input type="hidden" name="old_file_name" value="<?php echo ($rec['property_logo']) ?>" />
            <br /></td>
        </tr>
      </table></td>
</tr>


  <tr class="<?php echo ($clsRow1) ?>">
        <td width="15%" align="right"><strong>Meta Title</strong></td>
           <td><textarea  name="meta_title" id="meta_title" rows="1" cols="175" ><?php echo stripslashes($rec['meta_title']) ?></textarea></td>
 </tr>

<tr class="<?php echo ($clsRow1) ?>">
        <td width="15%" align="right"><strong>Meta Keywords</strong></td>
           <td><textarea  name="meta_keywords" id="meta_keywords" rows="1" cols="175" ><?php echo stripslashes($rec['meta_keywords']) ?></textarea></td>
 </tr>

<tr class="<?php echo ($clsRow1) ?>">
        <td width="15%" align="right"><strong>Meta Description</strong></td>
           <td><textarea  name="meta_description" id="meta_description" rows="1" cols="175" ><?php echo stripslashes($rec['meta_description']) ?></textarea></td>
 </tr>



<tr class="<?php echo ($clsRow1) ?>"><td align="right" ><strong>Status</strong> </td>
	<td valign="bottom">
	<input type="radio" name="status" value="1" id="1" checked="checked" />
	<label for="1">Active</label>
	<input type="radio" name="status" value="0" id="0" <?php if ($rec['status'] == "0") {echo ("checked='checked'");}?>/>
	<label for="0">Inactive</label>
	</td>
 </tr>


<tr class="<?php echo ($cls2) ?>">
  <td align="right" ><strong>Location Text</strong></td>
  <td><textarea name="location_content" class="ckeditor"  id="location_content" rows="8" cols="70"><?php echo ($rec['location_content']) ?></textarea>  </td>
</tr>

 





<tr class="<?php echo ($clsRow2) ?>"><td align="left">&nbsp;</td>
  <td align="left"><input name="submit" type="submit" class="button" value="Submit" />
    <input name="reset" type="reset" class="button" value="Reset" />
    <input name="button" type="button" class="button" onclick="window.location='<?=$page_name?>'" value="Cancel" /></td>
</tr>
</tbody>
</table>
</form>


<?php
$images = "";
for ($i = 0; $i < 3; $i++) {
    $i_plus = $i + 1;
    $images .= '<tr class="' . $clsRow1 . '"><td align="right" ><strong>Select Image ' . $i_plus . '</strong></td><input type="hidden" name="imageId[]"><td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="38%"><input type="file" name="image' . $i_plus . '" id="image' . $i_plus . '" /></td><input type="hidden" name="old_file_name' . $i_plus . '" /></td></tr></table></td></tr>';
}
?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
function selectTemplateType(value)
{
	//alert(value);
  if(value==22 || value==25 || value==36 || value==37 || value==51)
  {
    $('#getColumns').css('display','block');
    jQuery('#getColumns').replaceWith('<tr class="<?php echo ($clsRow1) ?>"><td align="right"><strong><font color="#FF0000">*</font>Project1</strong></td><td><input onkeydown="test(this.id);" id="text_1" data=""  type="text" title="project1" lang="MUST" value="<?php echo ($rec['project1']) ?>" size="50%" maxlength="120" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ordering<input type="text" name="ordering[]" value=""><button class="add" onclick="addMoreRows(this); return false;"></button></td> ');
  }

  if(value==25 || value==37)
  {
    $('#getColumns2').css('display','block');
    jQuery('#getColumns2').replaceWith('<tr class="<?php echo ($clsRow1) ?>"><td align="right"><strong><font color="#FF0000">*</font>Developer1</strong></td><td><input onkeydown="test2(this.id);" id="text1_1" data=""  type="text" title="developer1" lang="MUST" value="<?php echo ($rec['developer1']) ?>" size="50%" maxlength="120" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ordering<input type="text" name="ordering1[]" value=""><button class="add" onclick="addMoreRows2(this); return false;"></button></td> ');
  }

  if(value==23)
  {
    var image_data = '<?php echo $images ?>';
    $('#getColumns').replaceWith('<tr class="<?php echo ($clsRow1) ?>"><td align="right"><strong><font color="#FF0000">*</font>Recent Projects URL</strong></td><td><input name="similar_project_url_1" type="text" title="similar_project_url_1" value="<?php echo ($rec['similar_project_url_1']) ?>" size="50%" maxlength="120" /><input name="similar_project_url_2" type="text" title="similar_project_url_2" value="<?php echo ($rec['similar_project_url_2']) ?>" size="50%" maxlength="120" /><input name="similar_project_url_3" type="text" title="similar_project_url_3" value="<?php echo ($rec['similar_project_url_3']) ?>" size="50%" maxlength="120" /></td></tr>'+image_data);
  }

  if(value==24)
  {
    var image_data = '<?php echo $images ?>';
    $('#getColumns').replaceWith('<tr class="<?php echo ($clsRow1) ?>"><td align="right"><strong><font color="#FF0000">*</font>Recent Projects URL</strong></td><td><input name="similar_project_url_1" type="text" title="similar_project_url_1" value="<?php echo ($rec['similar_project_url_1']) ?>" size="50%" maxlength="120" /><input name="similar_project_url_2" type="text" title="similar_project_url_2" value="<?php echo ($rec['similar_project_url_2']) ?>" size="50%" maxlength="120" /><input name="similar_project_url_3" type="text" title="similar_project_url_3" value="<?php echo ($rec['similar_project_url_3']) ?>" size="50%" maxlength="120" /></td></tr>'+image_data);
  }

  if(value==16)
  {
    $('#getColumns').css('display','block');
    jQuery('#getColumns').replaceWith('<tr class="<?php echo ($clsRow1) ?>"><td align="right"><strong><font color="#FF0000">*</font>Timer(mm/dd/yy 00:00 am/pm)</strong></td><td><input name="microsite_timer" type="text" title="Timer" value="<?php echo ($rec['microsite_timer']) ?>" size="50%" maxlength="120" /></td></tr>');
  }

  if (templatesForNewAmneties.indexOf(value)<0) {
    $(".amenity").css('display','none');

  }

  if (templatesForNewAmneties.indexOf(value)>-1) {
    $(".amenity").css('display','');
  }
  //
}
</script>
<?php
if ($action == "Update" && ($rec['template_id'] == 22 || $rec['template_id'] == 25 || $rec['template_id'] == 36 || $rec['template_id'] == 37 || $rec['template_id'] == 51)) {
//echo "asas".$countOfrec;die;
    $count = $countOfrec + 1;

} else if ($action == "Update") {
    $count = 2;
} else {
    $count = 2;
}
?>
<script type="text/javascript">
        var rowCount = <?php echo $count; ?>;

function addMoreRows(row) {

if(rowCount<30)
{
  var recRow = '<tr id="newRow_'+rowCount+'" class="clsRow1"><td align="right"><strong><font color="#FF0000">*</font>Project'+rowCount+'</strong></td><td><input  onkeydown="test(this.id);" data="" id="text_'+rowCount+'" type="text" name="" size=50% maxlength="120">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ordering<input type="text" name="ordering[]" value=""><button onclick="removeRow(this); return false;">Remove</button></td></tr>';


jQuery('#newRows').before(recRow);
rowCount ++;
}
else
  alert("Maximum Number Projects Exceeds");
}
<?php
if ($action == "Update" && ($rec['template_id'] == 25 || $rec['template_id'] == 36 || $rec['template_id'] == 37 || $rec['template_id'] == 51)) {
//echo "asas".$countOfrec;die;
    $count1 = $countOfrec1 + 1;

} else if ($action == "Update") {
    $count1 = 2;
} else {
    $count1 = 2;
}
?>
var rowCount1 = <?php echo $count1; ?>;
function addMoreRows2(row) {

if(rowCount1<30)
{
  var recRow = '<tr id="newRow_'+rowCount1+'" class="clsRow1"><td align="right"><strong><font color="#FF0000">*</font>Developer'+rowCount1+'</strong></td><td><input  onkeydown="test2(this.id);" data="" id="text1_'+rowCount1+'" type="text" name="" size=50% maxlength="120">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ordering<input type="text" name="ordering1[]" value=""><button onclick="removeRow(this); return false;">Remove</button></td></tr>';


jQuery('#newRows').before(recRow);
rowCount1 ++;
}
else
  alert("Maximum Number Projects Exceeds");
}

function removeRow(removeNum) {

  //console.log(removeNum);
  var test=$(removeNum).parent().parent();
  //console.log(test);
$(test).remove();
rowCount--;
}
</script>

<script type="text/javascript">
  function test(id)
  {

var base_url="<?php echo C_ROOT_URL ?>";

//var cityId=document.getElementById("cty_id").value;

/*if(cityId!="")
  cityId=cityId;
else
  cityId=1;*/
cityId="";
//console.log(list);

  $(function() {
    //var projectlist=list;

      $("#"+id).autocomplete({
      minLength: 0,
      source:base_url+"/microsites_new/view/delRecord.php?cityId="+cityId,
      focus: function( event, ui ) {
        $( "#"+id ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        $( "#"+id ).val( ui.item.value );

        $( "#"+id ).val( ui.item.label );
        $("#"+id).parent().find("#hiddenProjId").remove();

        $("#"+id).parent().find("#newhidden").remove();
          //$("#"+id).find("#hiddenProjId").remove();
        // /console.log($("#"+id ).closest('td:input').next().attr('data',ui.item.value));
        $( "#"+id ).append('<input id="newhidden" type="hidden" name="project_id[]" value="'+ui.item.value+'">');
        //$( "#"+id ).attr('name',ui.item.value);
        //$( ".project-icon" ).attr( "src", "images/" + ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label +"</a>" )
        .appendTo( ul );
    };
  });

}
</script>

<script type="text/javascript">
  function test2(id)
  {
var base_url="<?php echo C_ROOT_URL ?>";

//var cityId=document.getElementById("cty_id").value;

/*if(cityId!="")
  cityId=cityId;
else
  cityId=1;*/
devID="";
//console.log(list);

  $(function() {
    //var projectlist=list;
      $("#"+id).autocomplete({
      minLength: 0,
      source:base_url+"/microsites_new/view/delRecord.php?devID="+devID,
      focus: function( event, ui ) {
        $( "#"+id ).val( ui.item.label );
        return false;
      },
      select: function( event, ui ) {
        $( "#"+id ).val( ui.item.value );

        $( "#"+id ).val( ui.item.label );
        $("#"+id).parent().find("#hiddenProjId1").remove();

        $("#"+id).parent().find("#newhidden1").remove();
          //$("#"+id).find("#hiddenProjId").remove();
        // /console.log($("#"+id ).closest('td:input').next().attr('data',ui.item.value));
        $( "#"+id ).append('<input id="newhidden1" type="hidden" name="dev_id[]" value="'+ui.item.value+'">');
        //$( "#"+id ).attr('name',ui.item.value);
        //$( ".project-icon" ).attr( "src", "images/" + ui.item.icon );

        return false;
      }
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<a>" + item.label +"</a>" )
        .appendTo( ul );
    };
  });

}
</script>

<script>
function deleteRow(ID, thisref)
{

if(confirm("Are you sure you want to delete this record"))
{
var base_url="<?php echo C_ROOT_URL ?>";

$(thisref).parent().parent().remove();


  $.ajax({
        //url: "view/changepoststatus.php",
        url:base_url+"/microsites_new/view/delRecord.php",
        type: 'POST',
        data: {rec_id :ID},
        success: function(data) {

          // location.href=base_url+"/specials/index.php";
          //jQuery("#rec_"+recid).replaceWith(data);

        }
    });
}


}
</script>

<script>
function deleteRow1(ID, thisref)
{

if(confirm("Are you sure you want to delete this record"))
{
var base_url="<?php echo C_ROOT_URL ?>";

$(thisref).parent().parent().remove();


  $.ajax({
        //url: "view/changepoststatus.php",
        url:base_url+"/microsites_new/view/delRecord.php",
        type: 'POST',
        data: {rec_div_id :ID},
        success: function(data) {

          // location.href=base_url+"/specials/index.php";
          //jQuery("#rec_"+recid).replaceWith(data);

        }
    });
}


}
</script>

<script type="text/javascript">
  document.getElementById('xml_file').addEventListener('change', checkFile, false);
approveletter.addEventListener('change', checkFile, false);

function checkFile(e) {

    var file_list = e.target.files;

    for (var i = 0, file; file = file_list[i]; i++) {
        var sFileName = file.name;
        var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1].toLowerCase();
        var iFileSize = file.size;
        var iConvert = (file.size / 10485760).toFixed(2);

        if (!(sFileExtension === "xml" ) || iFileSize > 10485760) {
            txt = "File type : " + sFileExtension + "\n\n";
            txt += "Size: " + iConvert + " MB \n\n";
            txt += "Please make sure your file is in xml format and less than 10 MB.\n\n";
            alert(txt);
        }
    }
}
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
var templatesForNewAmneties = ['44','45','47'];
var tempId = $("input[name='template_id']:checked").val();

var ind = templatesForNewAmneties.indexOf(tempId);

if (ind < 0) {
  $(".amenity").css('display','none');
}
</script>

<script>
function is_nri_subdomain() {
v = $('input[name="has_nri_subdomain"]:checked').val();
  if (v==1) {
    $('.nri_temp').css('display','');
  } else if(v==0) {
    $('.nri_temp').css('display','none');
  }
}
is_nri_subdomain();
</script>
<script>
function is_domestic_subdomain() {
a = $('input[name="has_domestic_subdomain"]:checked').val();
  if (a==1) {
    $('.domestic_temp').css('display','');
  } else if(a==0) {
    $('.domestic_temp').css('display','none');
  }
}
is_domestic_subdomain();
</script>

<script type="text/javascript">

$('#cty_id').change(function(){

  var base_url="<?php echo C_ROOT_URL ?>";
 // alert(base_url+"/microsites_new/micro_market.php");
  var cityid = $(this).val();
  var micro_marketdiv="";
    $.ajax({
        url:base_url+"/microsites_new/micro_market.php",
        type: 'POST',
        data: {cityid :cityid},
        success: function(data) {
          var obj = JSON.parse(data)
 
          for(var i=0; i<obj.length; i++){

            var micromarket_ids = '<?php echo $rec['micromarket_id'];?>';
            if(micromarket_ids==obj[i]['id']){
              selected= 'selected';
            }else{
              selected= '';
            }
            micro_marketdiv +='<option value="'+obj[i]['id']+'" '+selected+'>'+obj[i]['name']+'</option>';
          }
          
          $("#micromarket_id").html(micro_marketdiv);
        }
    });
});

$( document ).ready(function() {

  var cityid = $("#cty_id").val();
  var base_url="<?php echo C_ROOT_URL ?>";
 // alert(base_url+"/microsites_new/micro_market.php");
 // var cityid = $(this).val();
  var micro_marketdiv="";
    $.ajax({
        url:base_url+"/microsites_new/micro_market.php",
        type: 'POST',
        data: {cityid :cityid},
        success: function(data) {
          var obj = JSON.parse(data)
 
          for(var i=0; i<obj.length; i++){

            var micromarket_ids = '<?php echo $rec['micromarket_id'];?>';
            if(micromarket_ids==obj[i]['id']){
              selected= 'selected';
            }else{
              selected= '';
            }
            micro_marketdiv +='<option value="'+obj[i]['id']+'" '+selected+'>'+obj[i]['name']+'</option>';
          }
          
          $("#micromarket_id").html(micro_marketdiv);
        }
    });
    
});

</script>

