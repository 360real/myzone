<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
define('TABLE_MICRO_MARKET', 'tsr_micro_market'); 
admin_login();
$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
		
	$tblName	=	TABLE_NEW_MICROSITES;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/microsites_new/controller/controller.php';
	$clsRow1		=	'clsRow1';
	$clsRow2		=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform tast like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	
	$templateType= $_GET['type'];
	$regn_ip = $_SERVER['REMOTE_ADDR'];
	$file_name = "";
	switch($action){
		case 'Create':
		case 'Update':
			$file_name = ROOT_PATH."microsites_new/model/model.php" ;
			print_r($_POST);
			
			break;	
		default:
			$file_name = ROOT_PATH."microsites_new/view/view.php" ;
			break;
	}

	

?>
<?php include(LIBRARY_PATH."includes/header.php");
$commonHead = new CommonHead();
$commonHead->commonHeader('Manage Microsites', $site['TITLE'], $site['URL']);  
?>
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                  <td>
				 <?php 
				 if($_GET['msg']!=""){
					echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
				  }
				  if($msg!=""){
					echo('<div class="notice">'.$message_arr[$msg].'</div>');
				  }
				 ?>
				 </td>
                </tr>
            </table></td>
</tr>
<tr>
  <td colspan="2" valign="top"><?php include($file_name);?></td>
</tr>
<tr>
  <td colspan="2" valign="top"></td>
</tr>
</table>
<!-- Right Starts -->
<!-- Right Ends -->
<?php include(LIBRARY_PATH."includes/footer.php");?>
