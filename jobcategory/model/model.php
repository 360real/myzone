<?php

	require_once(ROOT_PATH . "library/mysqli_function.php");

	// this model for making all the query for event
	class Model {

	    private $dbObj;

	    function __construct() {

	        $this->dbObj = new mysqli_function();
	    }


	    public function adddata($arr){
			$columns = implode(", ",array_keys($arr));
			$escaped_values = array_map('mysql_real_escape_string', array_values($arr));
			foreach ($escaped_values as $idx=>$data) $escaped_values[$idx] = "'".$data."'";
			$values  = implode(", ", $escaped_values);
			$sql = "INSERT INTO `job_category` ($columns) VALUES ($values)";
			$query = $this->dbObj->insert_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}

	    }

	    public function listdata($id=0){
	    	if($id>0){
                $sql = "SELECT * FROM `job_category` WHERE `id` = ".$id;
	    	}else{
	    		$sql = "SELECT * FROM `job_category` order by id DESC";
	    	}
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }


	    public function deletedata($id){

	    	$sql = "delete from `job_category` WHERE `id` = ".$id;
	    	$query =  $this->dbObj->exec_query($sql, '');

	    }

	     public function updatedata($arr){

	        foreach($arr as $field=>$data)
			{
			$update[] = $field.' = \''.$data.'\'';
			}
			$sql = "UPDATE `job_category` SET ".implode(', ',$update)." WHERE id='".$arr['id']."'";
			$query = $this->dbObj->update_query($sql);
			if($query>0){
			  return $query;
			}else{
			  return 0;
			}

	    }


	    public function gettermsdata($id,$tablename){
	 
            $sql = "SELECT * FROM ".$tablename." WHERE `id` = ".$id;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	     public function getdeveloper($id){
            $sql = "SELECT `name` FROM tsr_developers WHERE `id` = ".$id;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }

	     public function getproperty($id){
            $sql = "SELECT `property_name` FROM tsr_properties WHERE `id` = ".$id;
			$data = $this->dbObj->getAllData($sql);
			return $data;
	    }
	    
	 
	}

 ?>
