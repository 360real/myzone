<?php
include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
include_once (LIBRARY_PATH."library/server_config_admin.php");
require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
admin_login();

$vals=mysql_real_escape_string($_POST['vals']);



$mailData['Subject'] = "Realty RoundUp ".date('jS M Y');
$mailData['NewsIDs'] = $vals;


$obj_mysql->insert_data("RealtyRoundUpMailDetails", $mailData); 
// $sql = "insert into RealtyRoundUpMailDetails(Subject) values(".$emailSubject.")";
// $rec = $obj_mysql->insert_query($sql);

$sql1="SELECT * from tsr_internal_news where id IN(".$vals.") and status=1 ORDER BY datetimestamp DESC, mail_order ASC";
$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql1) : $obj_mysql->getAllData($sql1));

$sql2="SELECT * from tsr_internal_news_images";
$rec2=($rec_id ? $obj_mysql->get_assoc_arr($sql2) : $obj_mysql->getAllData($sql2));
//print_r("http://static.360realtors.in/internal_news/banners/mailer_banner/".$rec2[0]["imgname_mailer"]);die;

$msg="";
$msg.='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>360realtors</title>
<style>
*{margin:0; padding:0;}
</style>
</head>

<body>
<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #dadada; margin:auto;">
  <tr>
    <td align="center" style="font-family:calibri; font-size:12px; color:#666666;">&#8520; If there are problems with how this message is displayed,<a href = "https://www.360realtors.com/internal-news"> click here </a>to view it in a web browser.</td>
  </tr>
  <img alt="" src="https://myzone.360realtors.ws/internal_news/view/track.php?mailerId='.$mailerId.'" width="1" height="1" border="0" />  
  <tr>
    <td>
    <img src="'.C_ROOT_URL.'internal_news/banners/mailer_banner/'.$rec2[0]["imgname_mailer"].'" /></td>
  </tr>';

//print_r($rec);
  
foreach($rec as $newsData)
{
  $title1=$newsData["title"];
  $title = str_replace("â€™", "'", $title1);
  $newsId=$newsData["id"];
  $description=$newsData["description"];
  $description = str_replace("â€™", "'", $description);
  $image=$newsData["imgname"];
  $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $title);
  $name = trim($name);
  $newsName = str_replace("--","-",str_replace(" ","-",$name));
  $newsName=preg_replace('/-+/', '-', strtolower($newsName)); 

  $newsTitledot = "";
  $newtitle_count = strlen(strip_tags($title));
  if($newtitle_count >70) {
    $newsTitledot = "...";
  } 
  $newsDescriptiondot = "";
  $newsDescription_count = strlen(strip_tags($description));
  if($newsDescription_count >120) {
    $newsDescriptiondot = "...";
  }

 $realUrl= "https://360realtors.com/internal-news/" . $newsName . "-inid-" . $newsId . "?utm_source=360realtors&utm_medium=mail&utm_campaign=internalNews&utm_content=". $newsId ;

 $newsURL = "https://360realtors.com/realtyrounduptrack/mailClick?url=".$realUrl."&newsId=".$newsId."&mailerId=".$mailerId;

  $msg.='<tr>
    <td style="padding:0 15px;">
		<table width="560" border="0" cellspacing="0" cellpadding="0" style="padding:15px 0; border-bottom:1px solid #d6d6d6;">
  <tr>
    <td>
    <a href="'.$newsURL.'">
		<img src='."https://static.360realtors.ws/internal_news/".$newsId."/".$image.' width="220" height="174"/></a>
    </td>
    <td style="padding:0 0 0 22px;">
		<table width="318" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="318" style="font:bold 18px calibri; padding:0 0 8px 0;">
		<a href="'.$newsURL.'" style="color:#222222; text-decoration: none;">'.substr(strip_tags($title),0,70).$newsTitledot.'</a>
    </td>
  </tr>
  <tr>
    <td width="318" style="font:normal 16px calibri; color:#555555; line-height:22px; padding:0 0 12px 0;">'.substr(strip_tags($description),0,120).$newsDescriptiondot.'</td>
  </tr>
  <tr>
    <td style="padding:3px 0 0px 0;">
	<a href="'.$newsURL.'" style="text-decoration:none;">
		<img src="'.C_ROOT_URL.'view/images/read-more-11.jpg" /></a>
		</td>
  </tr>
</table>
</td>
  </tr>
</table>
</td>
  </tr>';
}
  $msg.='
  <tr>
	<td height="15"></td>
	</tr>
  <tr>
  <td width="600" style="max-width:600px; font:normal 12px calibri; color:#373737; line-height:16px; background:#cbcbcb; padding:15px;">Disclaimer: The content presented in this newsletter is collated from various news websites and portals & is purely curated news. 360 Realtors claims no ownership of the content, views and opinions in the newsletter and they rest solely with the original author(s) of the article(s). 360 Realtors accepts no liability in any way, shape or form, for the content of this email, or for the consequences of any actions taken on the basis of the information provided.</td>
  </tr>
 <tr>
    <td>
		<img src="'.C_ROOT_URL.'internal_news/view/images/border.jpg" align="left" /></td>
  </tr>
</table>
</body>
</html>';
echo $msg;
?>