<?php

	date_default_timezone_set('Asia/Kolkata');
	//include("/var/www/html/main/myzone/adminproapp/includes/set_main_conf.php");
	include($_SERVER['DOCUMENT_ROOT']."/includes/set_main_conf.php");
	//include("C:/wamp/www/adminproapp/includes/set_main_conf.php");
	include_once (LIBRARY_PATH."library/server_config_admin.php");
	require_once(LIBRARY_PATH."library/PHPMailer_master/PHPMailerAutoload.php");
	admin_login();
	$message_arr=array(	"insert"=>"Record(s) has been added successfully!",
					"update"=>"Record(s) has been updated successfully!",
					"delete"=>"Record(s) has been deleted successfully!",
					"cstatus"=>"Record(s) status has been changed!",
					"unique"=>"Name already in record!");
	$tblName	=	TABLE_INTERNAL_NEWS;	//main table name
	$fld_id		=	'id';					//primery key
	$fld_status	=	'status';			// staus field
	$fld_orderBy=	'id';			// default order by field
	$page_name  =	C_ROOT_URL.'/internal_news/controller/controller.php';
	$clsRow1	=	'clsRow1';
	$clsRow2	=	'clsRow2';
	$action		=	remRegFx($_REQUEST['action']);		// action to perform task like Update, Create , Delete etc.
	$rec_id		=	remRegFx($_GET['id']);
	$arr_id		=	$_POST['items'] ? $_POST['items'] : array($rec_id);	// for radio button 
	$query_str	=	"page=$page";	


	/* choices to where page is redirect
		if action=create then goes to v
		iew  	
			action =update then model
			action default to view i.e index.php
	*/

	$file_name = "";
	switch($action)
	{
		case 'Create':
		case 'Update':
		$file_name = ROOT_PATH."/internal_news/model/model.php" ;
		break;	
		default:
		$file_name = ROOT_PATH."/internal_news/view/view.php" ;
		break;
	}

	/***********************************************************************************/
			//Code  for search a record from database on click submit  button

	/***********************************************************************************/
	if($_GET["action"]!="" && $_GET["action"]!="Create")
	{
		if($_GET["action"]=="search")
		{
			  $sql = "SELECT NEW.* FROM ".TABLE_INTERNAL_NEWS." NEW LEFT JOIN ".TABLE_NEWS_TAGS_ID." as TAGID ON NEW.id=TAGID.news_id LEFT JOIN ".TABLE_NEWS_TAGS." as TAGS ON TAGS.id=TAGID.tags_id WHERE ";
			if($_GET["user_id"]!="")
				$sql.="NEW.id=".$_GET["user_id"];
			if($_GET["title"]!="")
				$sql.=" NEW.title like '%".trim($_GET["title"])."%'";
			if($_GET["status"]!="")
				{   if(($_GET["status"]==1)||($_GET["status"]==0))
					$sql.="NEW.status=".$_GET["status"];
					else
					$sql.=" (NEW.status=1 OR NEW.status=0) ";
				}
			if($_GET["startDate"]!="" && $_GET["endDate"])
			{
				$sql.=" and date(NEW.created_date)>='".date('Y-m-d', strtotime($_GET["startDate"]))."' and date(NEW.created_date)<='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
			}
			else
			if($_GET["startDate"]!="")
			{
				$sql.=" and date(NEW.created_date)='".date('Y-m-d', strtotime($_GET["startDate"]))."'";
			}
			else
			if($_GET["endDate"]!="")
			{
				$sql.=" and date(NEW.created_date)='".date('Y-m-d', strtotime($_GET["endDate"]))."'";
			}
		}

		else if($_GET["action"]=="Update")
		{
			
			$sql = "SELECT NEW.status AS mainstatus , NEW.*,TAGID.*,TAGS.* FROM ".TABLE_INTERNAL_NEWS." NEW LEFT JOIN ".TABLE_NEWS_TAGS_ID." as TAGID ON NEW.id=TAGID.news_id LEFT JOIN ".TABLE_NEWS_TAGS." as TAGS ON TAGS.id=TAGID.tags_id ";
			$sql.= " WHERE NEW.id=".$_GET["id"];
			//$sql.=($rec_id ? " Where $tblNameJoin.$fld_id='$rec_id'" :' ');
		}

		else if($_GET["action"]=="Delete") 
		{
			/******* uncomment below 2 lines two delete files as well as directory ***********/
			//array_map('unlink', glob(UPLOAD_PATH_DEVELOPERS_LOGO.$_GET['id']."/*.*")); // method to unlink an image
			//@rmdir(UPLOAD_PATH_DEVELOPERS_LOGO.$_GET['id']); // method to delete a directory
			/******* uncomment below 2 lines two delete files as well as directory ***********/
			$obj_mysql->delRecord($tblName, $fld_id, $_GET["id"]);
			$obj_common->redirect($page_name."?msg=delete");
		}

		$s=($_GET['s'] ? 'ASC' : 'DESC');
		$sort=($_GET['s'] ? 0 : 1 );
			$f=$_GET['f'];
		if($s && $f)
		$sql.= " GROUP BY NEW.id ORDER BY $f  $s";
		else
		//$sql.= " GROUP BY NEW.id ORDER BY NEW.$fld_orderBy";	
		$sql.= " GROUP BY NEW.id ORDER BY NEW.datetimestamp DESC , NEW.mail_order ASC";
	}

	else
	{
		$sql = "SELECT NEW.*,TAGID.tags_id, TAGS.tags FROM ".TABLE_INTERNAL_NEWS." NEW LEFT JOIN ".TABLE_NEWS_TAGS_ID." as TAGID ON NEW.id=TAGID.news_id LEFT JOIN ".TABLE_NEWS_TAGS." as TAGS ON TAGS.id=TAGID.tags_id group by NEW.id order by NEW.datetimestamp DESC , NEW.mail_order ASC";
	//	$sql = "SELECT NEW.*,TAGID.tags_id, TAGS.tags FROM ".TABLE_INTERNAL_NEWS." NEW LEFT JOIN ".TABLE_NEWS_TAGS_ID." as TAGID ON NEW.id=TAGID.news_id LEFT JOIN ".TABLE_NEWS_TAGS." as TAGS ON TAGS.id=TAGID.tags_id group by NEW.id order by NEW.created_date DESC";
		//$rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));
	}


	/*---------------------paging script start----------------------------------------*/
		$obj_paging->limit=30;
		if($_GET['page_no'])
			$page_no=remRegFx($_GET['page_no']);
			else
			$page_no=0;
		$queryStr=$_SERVER['QUERY_STRING'];	
	/*--------------------if page_no alreay exists please remove them ---------------------------*/
		$str_pos=strpos($queryStr,'page_no');
		if($str_pos>0)
			$queryStr=str_replace(substr($queryStr,($str_pos-1),strlen($queryStr)),"",$queryStr); 	 		
	/*------------------------------------------------------------------------------------------*/
		
		$obj_paging->set_lower_upper($page_no);
        $total_num = $obj_mysql->get_num_rows($sql);
        $paging = $obj_paging->next_pre($page_no, $total_num, $page_name . "?$queryStr&", 'textArial11Bold', 'textArial11orgBold');

        // print_r($paging);
        $total_rec = $obj_paging->total_records($total_num);
        $sql .= " LIMIT $obj_paging->lower,$obj_paging->limit";
	/*---------------------paging script end----------------------------------------*/
	    $rec=($rec_id ? $obj_mysql->get_assoc_arr($sql) : $obj_mysql->getAllData($sql));

	/************************************************************************************************/
			// code for search and pagination ends here

	/************************************************************************************************/	

	/*if(isset($_POST['namesubmit']))
	{
		$arr=$_POST;
		$rec=$_POST;
		$obj_mysql->insert_data($tblName,$arr);
		$obj_common->redirect($page_name."?msg=insert");	
	}*/
	if(count($_POST)>0)
	{
		$arr=$_POST; 
		$rec=$_POST;
		if($rec_id)
		{
			$arr['modified_date'] = "now()";
			$arr['ipaddress'] = $regn_ip;
			$arr['modified_by']=$_SESSION['login']["id"];
			$arr['created_by_user']= $_SESSION["bpLoginUserName"];
			$arr['mail_order'] = $_POST['email_order'];
			$arr['published_date']=date('Y/m/d H:i:s', strtotime($_POST["published_date"]));
			$arr['datetimestamp'] = date("Y-m-d", strtotime($_POST["published_date"]));
			if($obj_mysql->isDupUpdate($tblName,'title', $arr['title'] ,'id',$rec_id))
			{
				//$arr['published_date']=date('m-d-Y', strtotime($_POST["published_date"]));
			//	echo "aa-".UPLOAD_PATH_INTERNAL_NEWS.$rec_id."/"; die;
						if($_FILES['imgname']['name']!="" && $counterRename!="1")
							{   
								echo $_FILES['imgname']['name'];
								@unlink(UPLOAD_PATH_INTERNAL_NEWS.$rec_id."/".$_POST['old_file_name']);
								$handle = new upload($_FILES['imgname']);
								if ($handle->uploaded)
									{	
										/*$handle->image_resize         = true;
										$handle->image_x              = 200;
										$handle->image_y              = 87;
										$handle->image_ratio_y        = true;*/
										$handle->file_safe_name = true;
										$handle->process(UPLOAD_PATH_INTERNAL_NEWS.$rec_id."/");
										if ($handle->processed) 
										{
											//echo 'image resized';
											$arr['imgname'] = $handle->file_dst_name;
											$handle->clean();
				   						} 
				   						else 
				   						{
										   //echo "error";
											echo 'error : ' . $handle->error;
										}
							}
				}
				//print_r($_POST['tags_id']);die;

				if(!empty($_POST['tags_id']))
				{
					$sql="SELECT TAGID.id, TAGID.tags_id, TAGID.status, TAGS.tags from ".TABLE_NEWS_TAGS." as TAGS INNER JOIN ".TABLE_NEWS_TAGS_ID." as TAGID ON TAGS.id=TAGID.tags_id where TAGID.news_id=".$rec_id." and TAGID.type=1 ";
					$reca=($obj_mysql->getAllData($sql));
					if(count($reca)>0)
					{
						
						$tagId=array();
						foreach ($reca as $key) {
						$tagValues=array_push($tagId, $key['tags_id']);
						}
						//print_r($tagId);die;
						$currentTagId=explode(",", $_POST['tags_id']);
						$insertionTagId=array_diff($currentTagId,$tagId);

						$sql1="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=0 where tags_id NOT IN (".$_POST['tags_id'].") and news_id=".$rec_id." and type=1 ";
						//echo $sql1;die;
						$obj_mysql->exec_query($sql1, $link);

						$sql2="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=1 where tags_id IN (".$_POST['tags_id'].") and news_id=".$rec_id." and type=1 ";
						//echo $sql1;die;
						$obj_mysql->exec_query($sql2, $link);

						if(!empty($insertionTagId))
						{
						/*$sql1="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=0 where tags_id NOT IN (".$_POST['tags_id'].") and news_id=".$rec_id." ";
						$obj_mysql->exec_query($sql1, $link);*/

						foreach($insertionTagId as $locationValues)
						{
						$projectArray['tags_id']=$locationValues;
						$projectArray['news_id']=$rec_id;
						$projectArray['status']='1';
						$projectArray['type']='1';
						$projectArray['datetimestamp'] = date("Y-m-d", strtotime($projectArray['published_date']));
						$projectArray['mail_order'] = $_POST['email_order'];
						$projectArray['created_by_user']=$_SESSION["bpLoginUserName"];
						$obj_mysql->insert_data("tsr_news_tags",$projectArray);
						}

						}

						//print_r($result);die;
						
					}
					else
					{
						$currentTagId=explode(",", $_POST['tags_id']);
						foreach($currentTagId as $locationValues)
						{
						$projectArray['tags_id']=$locationValues;
						$projectArray['news_id']=$rec_id;
						$projectArray['status']='1';
						$projectArray['type']='1';
						$projectArray['datetimestamp'] = date("Y-m-d", strtotime($projectArray['published_date']));
						$projectArray['mail_order'] = $_POST['email_order'];
						$projectArray['created_by_user']=$_SESSION["bpLoginUserName"];
						$obj_mysql->insert_data("tsr_news_tags",$projectArray);
						}
					}
					
				}
				else
				{
					//print_r($_POST);die;
					$sql1="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=0 where news_id=".$rec_id." and type=1 ";
					//echo $sql1;die;
					$obj_mysql->exec_query($sql1, $link);
				}


				/* For Segmnent Tags*/

				if(!empty($_POST['tags_id2']))
				{
					$sql="SELECT TAGID.id, TAGID.tags_id, TAGS.tags from ".TABLE_NEWS_TAGS." as TAGS INNER JOIN ".TABLE_NEWS_TAGS_ID." as TAGID ON TAGS.id=TAGID.tags_id where TAGID.news_id=".$rec_id." and TAGID.type=2 ";
					$reca=($obj_mysql->getAllData($sql));
					if(count($reca)>0)
					{
						$tagId=array();
						foreach ($reca as $key) {
						$tagValues=array_push($tagId, $key['tags_id']);
						}
						//print_r($tagId);die;
						$currentTagId=explode(",", $_POST['tags_id2']);
						$insertionTagId=array_diff($currentTagId,$tagId);

						$sql1="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=0 where tags_id NOT IN (".$_POST['tags_id2'].") and news_id=".$rec_id." and type=2 ";
						//echo $sql1;die;
						$obj_mysql->exec_query($sql1, $link);

						$sql2="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=1 where tags_id IN (".$_POST['tags_id2'].") and news_id=".$rec_id." and type=2 ";
						//echo $sql1;die;
						$obj_mysql->exec_query($sql2, $link);

						if(!empty($insertionTagId))
						{
						/*$sql1="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=0 where tags_id NOT IN (".$_POST['tags_id'].") and news_id=".$rec_id." ";
						$obj_mysql->exec_query($sql1, $link);*/

						foreach($insertionTagId as $locationValues)
						{
						$projectArray['tags_id']=$locationValues;
						$projectArray['news_id']=$rec_id;
						$projectArray['status']='1';
						$projectArray['type']='2';
						$projectArray['datetimestamp'] = date("Y-m-d", strtotime($projectArray['published_date']));
						$projectArray['mail_order'] = $_POST['email_order'];
						$projectArray['created_by_user']=$_SESSION["bpLoginUserName"];
						$obj_mysql->insert_data("tsr_news_tags",$projectArray);
						}

						}

						//print_r($result);die;
						
					}
					else
					{
						$currentTagId=explode(",", $_POST['tags_id2']);
						foreach($currentTagId as $locationValues)
						{
						$projectArray['tags_id']=$locationValues;
						$projectArray['news_id']=$rec_id;
						$projectArray['status']='1';
						$projectArray['type']='2';
						$projectArray['datetimestamp'] = date("Y-m-d", strtotime($projectArray['published_date']));
						$projectArray['mail_order'] = $_POST['email_order'];
						$projectArray['created_by_user']=$_SESSION["bpLoginUserName"];
						$obj_mysql->insert_data("tsr_news_tags",$projectArray);
						}
					}
					
				}
				else
				{
					//print_r($_POST);die;
					$sql1="UPDATE ".TABLE_NEWS_TAGS_ID." SET status=0 where news_id=".$rec_id." and type=2 ";
					//echo $sql1;die;
					$obj_mysql->exec_query($sql1, $link);
				}


				
				$query = $obj_mysql->update_data($tblName,$fld_id,$arr,$rec_id);	
				$obj_common->redirect($page_name."?msg=update");	
			}

			else
			{
				//$obj_common->redirect($page_name."?msg=unique");	
				$msg='unique';
			}	
		}

		else
		{
			$arr['ipaddress'] = $regn_ip;
			
			$arr['mail_order'] = $_POST['email_order'];
			$arr['created_by']=$_SESSION['login']["id"];
			
			$arr['created_by_user']=$_SESSION["bpLoginUserName"];
			$arr['published_date']=date('Y/m/d H:i:s', strtotime($_POST["published_date"]));
			$arr['datetimestamp'] = date("Y-m-d", strtotime($arr['published_date']));
			if($obj_mysql->isDuplicate($tblName,'title', $arr['title']))
			{
				//$arr['published_date']=date('m-d-Y', strtotime($_POST["published_date"]));
				
				$setmaxid=$obj_mysql->insert_data($tblName,$arr);

				if($_FILES['imgname']['name']!="")
				{
					echo $_FILES['imgname']['name'];
					$handle = new upload($_FILES['imgname']);
					if ($handle->uploaded)
					{
						/*$handle->image_resize         = true;
						$handle->image_x              = 200;
						$handle->image_y              = 87;
						$handle->image_ratio_y        = true;*/
						$handle->file_safe_name = true;
						$handle->process(UPLOAD_PATH_INTERNAL_NEWS.$setmaxid."/");
						if ($handle->processed) 
						{
							//echo 'image resized';
							$arr['imgname'] = $handle->file_dst_name;
							@unlink(UPLOAD_PATH_INTERNAL_NEWS.$setmaxid."/".$_POST['old_file_name']);
							$handle->clean();
						} 
						else 
						{
							echo 'error : ' . $handle->error;
						}
					}


					$obj_mysql->update_data($tblName,$fld_id,$arr,$setmaxid);

				}
				if(!empty($_POST['tags_id']))
				{
					$locationTags=explode(",",$_POST['tags_id']);
					foreach($locationTags as $locationValues)
					{
						$projectArray['tags_id']=$locationValues;
						$projectArray['news_id']=$setmaxid;
						$projectArray['status']='1';
						$projectArray['type']='1';
						$projectArray['datetimestamp'] = date("Y-m-d", strtotime($projectArray['published_date']));
						$projectArray['mail_order'] = $_POST['email_order'];
						$projectArray['created_by_user']=$_SESSION["bpLoginUserName"];
						$obj_mysql->insert_data("tsr_news_tags",$projectArray);
					}
				}

				if(!empty($_POST['tags_id2']))
				{
					$locationTags=explode(",",$_POST['tags_id2']);
					foreach($locationTags as $locationValues)
					{
						$projectArray['tags_id']=$locationValues;
						$projectArray['news_id']=$setmaxid;
						$projectArray['status']='1';
						$projectArray['type']='2';
						$projectArray['datetimestamp'] = date("Y-m-d", strtotime($projectArray['published_date']));
						$projectArray['mail_order'] = $_POST['email_order'];
						$projectArray['created_by_user']=$_SESSION["bpLoginUserName"];
						$obj_mysql->insert_data("tsr_news_tags",$projectArray);
					}
				}
				
					

				$obj_common->redirect($page_name."?msg=insert");	
			}

			else
			{
				$obj_common->redirect($page_name."?msg=unique");
				$msg='unique';	
			}	
		}
	}	
	
	//second if block ends here
	
//print_r($rec);die;
	$list="";
	for($i=0;$i< count($rec);$i++)
	{
		if($rec[$i]['status']=="1")
		{
			$st='<font color=green>Active</font>';
		}
		else
		{
			$st='<font color=red>Inactive</font>';
		}
		$clsRow = ($i%2==0)? 'clsRowView1':'clsRowView2';
		$list.='
		<tr class="'.$clsRow.'">
			<td class="center"><input type="checkbox" name="location[]" value="'.$rec[$i]['id'].'"></td>
			<td class="center">'.$rec[$i]['title'].'</td>
			<td class="center">'.$st.'</td>
			<td class="center">'.$rec[$i]['mail_order'].'</td>
			<td class="center">'.$rec[$i]['created_date'].'</td>
			<td class="center">'.$rec[$i]['published_date'].'</td>
			<td class="center">'.$rec[$i]['created_by_user'].'</td>
			<td class="center">'.$rec[$i]['modified_date'].'</td>
			<td class="center">'.$rec[$i]['ipaddress'].'</td>
			<td>
				<a href="'.$page_name.'?action=Update&id='.$rec[$i]['id'].'"  class="edit">Edit</a>
			</td> 
			<td>
				<a href="'.$page_name.'?action=Delete&id='.$rec[$i]['id'].'"  style="color:#000;" ><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
			</td> 
		</tr>';
	}

	if($list=="")
	{
		$list="<tr><td colspan=10 align='center' height=50>No Record(s) Found.</td></tr>";
	}
?>

<!--************************************** code to include common header ***************************************/-->
<?php include(LIBRARY_PATH."includes/header.php");
	$commonHead = new CommonHead();
	$commonHead->commonHeader('Internal News Manager', $site['TITLE'], $site['URL']); 
?>
<!--************************************** End of code to include header ***************************************/-->
<!-- Right Starts -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="view-table">
	<tr>
	  <td align="center" colspan="2" valign="top">
			<table width="99%" border="0" cellspacing="0" cellpadding="0" align="center">
		        <tr>
					<td>
						<?php 
						if($_GET['msg']!="")
						{
							echo('<div class="notice">'.$message_arr[$_GET['msg']].'</div>');
						}
						if($msg!="")
						{
							echo('<div class="notice">'.$message_arr[$msg].'</div>');
						}
						?>
					</td>
		        </tr>
		    </table>
	    </td>
	</tr>

	<tr>
	  <td colspan="2" valign="top"><?php include($file_name	);?></td> <!-- include file_name to get the page on controller page -->
	</tr>

	<tr>
	  <td colspan="2" valign="top"></td>
	</tr>
</table>
<!-- Right Ends -->


<?php include(LIBRARY_PATH."includes/footer.php");?>
